var VtPlugin = {
    initPlugin: function () {

        return document.getElementById('plugin0').getVersion == null ? false : true;
    },
    getVersion: function () {
        return document.getElementById('plugin0').getVersion();
    },
    getFileName: function () {
        return document.getElementById('plugin0').getFileName();
    },
    getCert: function () {
        return document.getElementById('plugin0').getCert();
    },
    getCertChain: function () {
        return document.getElementById('plugin0').getCertChain();
    },
    signHash: function (base64Hash, certSerial) {
        return document.getElementById('plugin0').signHash(base64Hash, certSerial);
    },
    signPdf: function (filePath, reason, location) {
        return document.getElementById('plugin0').signPdf(filePath, reason, location);
    },
    signPdfD: function (filePath, reason, location, fileDest) {
        return document.getElementById('plugin0').signPdf(filePath, reason, location, fileDest);
    },
    signOOXml: function (filePath) {
        return document.getElementById('plugin0').signOOXml(filePath);
    },
    signOOXml: function (filePath, fileDest) {
        return document.getElementById('plugin0').signOOXml(filePath, fileDest);
    },
    signXml: function (filePath) {
        return document.getElementById('plugin0').signOOXml(filePath);
    },
    signXml: function (filePath, fileDest) {
        return document.getElementById('plugin0').signXml(filePath, fileDest);
    }

};

function initPlugin() {
    if (!(VtPlugin.initPlugin()) || VtPlugin.getVersion() != '1.1.0.0') {
        alert('B?n c?n c�i ??t plugin');
        window.open("HTDS_Portal/plugin/ViettelCASigner.msi", "download");
        return false;
    }
    return true;
}

function signHash() {
    if (!initPlugin()) {
        return false;
    }
    var base64Cert = new String(VtPlugin.getCert());
    if (base64Cert.toString().indexOf("ERROR") < 0) {
        zAu.send(new zk.Event(zk.Widget.$('$wdicReportManagement'), 'onCreateHash', base64Cert.toString()));
    }
}

function signCA(base64Hash, certSerial, filePath) {
    var hash_array = base64Hash.split(';');
    var listSignature = '';
    for (var i = 0; i < hash_array.length; i++) {
        var base64Signature = VtPlugin.signHash(hash_array[i], certSerial);
        listSignature = listSignature + base64Signature + ';';
    }
    if (base64Signature.toString().indexOf("ERROR") < 0) {
        var json = '{"signature":"' + listSignature + '","file":"' + filePath + '"}';
        zAu.send(new zk.Event(zk.Widget.$('$wdicReportManagement'), 'onSignCA', json));
    } else if (base64Signature.toString().indexOf("ERROR: CryptAcquireCertificatePrivateKey") >= 0) {
        alert("Kh�ng th? l?y kh�a b� m?t ??i v?i ch?ng th? n�y, vui l�ng ki?m tra usb");
    } else if (base64Signature.toString().indexOf("ERROR: Sign Hash Error: get bytes") >= 0) {
    } else {
        alert("L?i " + base64Signature);
    }
}

function signHashApplet(cert) {
    zAu.send(new zk.Event(zk.Widget.$('$wdSignCAPopup'), 'onCreateHash', cert.toString()));
}

function signCAApplet(listSignature, listPath) {
    var json = '{"signature":"' + listSignature + '","file":"' + listPath + '"}';
    zAu.send(new zk.Event(zk.Widget.$('$wdSignCAPopup'), 'onSignCA', json));
}