package viettel.passport.util;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import viettel.passport.client.ObjectToken;
import viettel.passport.client.UserToken;
import viettel.passport.client.VSAValidate;

public class VsaFilter
        implements Filter
{
  private FilterConfig filterConfig;
  private static Logger logger = Logger.getLogger(VsaFilter.class);

  private static HashSet<String> casAllowedURL = new HashSet();

  private static HashSet<String> allMenuURL = new HashSet();

  private boolean timeoutToErrorPage = false;

  public void init(FilterConfig config)
          throws ServletException
  {
    try
    {
      logger.debug("Lay danh sach AllowUrl tu file config 'cas_en_US.properties'");
      if (Connector.getAllowedUrls() != null)
        getCasAllowedURL().addAll(Arrays.asList(Connector.getAllowedUrls()));
    }
    catch (Exception ex) {
      logger.error("Loi lay danh sach AllowUrl tu file config:'cas_en_US.properties'", ex);
      throw new ExceptionInInitializerError(ex);
    }
    try
    {
      setFilterConfig(config);

      VSAValidate vsa = new VSAValidate();
      ArrayList objs = vsa.getAllMenu();
      for (int i = 0; i < objs.size(); i++) {
        allMenuURL.add(((ObjectToken)objs.get(i)).getObjectUrl().split("\\?")[0]);
      }
      if (logger.isDebugEnabled())
        logger.debug("All menu URL: " + allMenuURL);
    }
    catch (Exception ex) {
      logger.error("Loi khi lay danh sach tat ca module URL", ex);
      allMenuURL.clear();
    }
  }

  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
          throws IOException, ServletException
  {
    HttpServletRequest req = null;
    HttpServletResponse res = null;

    if ((request instanceof HttpServletRequest)) {
      req = (HttpServletRequest)request;
    }
    if ((response instanceof HttpServletResponse)) {
      res = (HttpServletResponse)response;
    }

    String sTimeoutToErrorPage = this.filterConfig.getInitParameter("timeoutToErrorPage");
    this.timeoutToErrorPage = "true".equalsIgnoreCase(sTimeoutToErrorPage);
    
    if (req == null || res == null) {
        return;
    }

    Connector cnn = new Connector(req, res);
    req.setAttribute("VSA-IsPassedVSAFilter", "True");

    if (alowURL(req.getRequestURI(), Connector.getAllowedUrls()))
    {
      chain.doFilter(req, res);
    }
    else if (!cnn.isAuthenticate())
    {
      if (cnn.hadTicket())
      {
        if (!cnn.getAuthenticate())
        {
          logger.warn("Redirect to error page by authenticate failure.");
          String redirectUrl = Connector.getErrorUrl() + "?errorCode=" + "AuthenticateFailure";

          req.setAttribute("VSA-IsPassedVSAFilter", "False");

          req.setAttribute("VSA-Flag", "InPageRedirect");
          req.setAttribute("VSA-Location", redirectUrl);
          req.setAttribute("VSA-Redirect", "1");

          res.setHeader("VSA-Flag", "InPageRedirect");
          res.setHeader("VSA-Location", redirectUrl);
          res.setHeader("VSA-Redirect", "1");
          if ((req.getHeader("ZK-SID") != null) && (req.getHeader("ZK-SID").length() > 0))
          {
            chain.doFilter(request, response);
          }
          else {
            res.sendRedirect(redirectUrl);
          }
        }
        else
        {
          chain.doFilter(request, response);
        }
      }
      else {
        String redirectUrl = cnn.getPassportLoginURL() + "?appCode=" + cnn.getDomainCode() + "&service=" + URLEncoder.encode(cnn.getServiceURL(), "UTF-8");

        req.setAttribute("VSA-IsPassedVSAFilter", "False");

        req.setAttribute("VSA-Flag", "InPageRedirect");
        req.setAttribute("VSA-Location", redirectUrl);
        req.setAttribute("VSA-Redirect", "1");

        res.setHeader("VSA-Flag", "InPageRedirect");
        res.setHeader("VSA-Location", redirectUrl);
        res.setHeader("VSA-Redirect", "1");
        if ((req.getHeader("ZK-SID") != null) && (req.getHeader("ZK-SID").length() > 0))
        {
          chain.doFilter(request, response);
        }
        else {
          if (this.timeoutToErrorPage) {
            redirectUrl = Connector.getErrorUrl() + "?errorCode=" + "SessionTimeout";
          }

          res.sendRedirect(redirectUrl);
        }
      }

    }
    else if ((!allMenuURL.isEmpty()) && (allMenuURL.contains(req.getServletPath())))
    {
      if (getVsaAllowedServletPath(req).contains(req.getServletPath()))
      {
        chain.doFilter(request, response);
      }
      else {
        logger.warn("Redirect to error page by not permission to execute servlet '" + req.getServletPath() + "'.");

        String redirectUrl = Connector.getErrorUrl() + "?errorCode=" + "NotPermissionAction";
        req.setAttribute("VSA-IsPassedVSAFilter", "False");

        req.setAttribute("VSA-Flag", "InPageRedirect");
        req.setAttribute("VSA-Location", redirectUrl);
        req.setAttribute("VSA-Redirect", "1");

        res.setHeader("VSA-Flag", "NewPageRedirect");
        res.setHeader("VSA-Location", redirectUrl);
        res.setHeader("VSA-Redirect", "1");

        if ((req.getHeader("ZK-SID") != null) && (req.getHeader("ZK-SID").length() > 0))
        {
          chain.doFilter(request, response);
        }
        else {
          res.sendRedirect(redirectUrl);
        }
      }
    }
    else
      chain.doFilter(request, response);
  }

  public void destroy()
  {
  }

  private Boolean alowURL(String url, String[] listAlowUrl)
  {
    for (String str : listAlowUrl) {
      if (url.equalsIgnoreCase(str)) {
        return Boolean.valueOf(true);
      }
    }
    return Boolean.valueOf(false);
  }

  public static HashSet<String> getAllMenuURL() {
    return allMenuURL;
  }

  public static synchronized void setAllMenuURL(HashSet<String> allMenuURL2) {
    allMenuURL = allMenuURL2;
  }

  public static HashSet<String> getCasAllowedURL() {
    return casAllowedURL;
  }

  public static synchronized void setCasAllowedURL(HashSet<String> casAllowedURL2) {
    casAllowedURL = casAllowedURL2;
  }

  private HashSet<String> getVsaAllowedServletPath(HttpServletRequest request) {
    UserToken vsaUserToken = (UserToken)request.getSession().getAttribute("vsaUserToken");
    HashSet vsaAllowedURL = new HashSet();

    for (ObjectToken ot : vsaUserToken.getObjectTokens()) {
      String servletPath = ot.getObjectUrl();

      if (!"#".equals(servletPath)) {
        vsaAllowedURL.add(servletPath.split("\\?")[0]);
      }
    }
    return vsaAllowedURL;
  }

  public FilterConfig getFilterConfig() {
    return this.filterConfig;
  }

  public void setFilterConfig(FilterConfig filterConfig) {
    this.filterConfig = filterConfig;
  }
}