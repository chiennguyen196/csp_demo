package com.viettel.plc.widget;

import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.StringUtils;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.*;

/**
 * Created by tuannm33 on 1/19/2017.
 */
public class VerifyAccountSSO extends GenericForwardComposer {

    private Textbox txtAccountSSO;
    private Textbox txtPassSSO;
    private Button btnAccept;
    private Button btnCancel;
    private String id;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        id = arg.get("ID") == null ? "" : arg.get("ID").toString();
    }

    private boolean validate() {
        if (!StringUtils.isValidString(txtAccountSSO.getValue())) {
            Clients.wrongValue(txtAccountSSO, LanguageBundleUtils.getString("global.your.account.empty"));
            return false;
        }
        if (!StringUtils.isValidString(txtPassSSO.getValue())) {
            Clients.wrongValue(txtPassSSO, LanguageBundleUtils.getString("global.your.account.empty"));
            return false;
        }
        return true;
    }

    public void onClick$btnAccept() {
        if (validate()) {

        }
    }

}
