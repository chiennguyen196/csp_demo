/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.plc.widget;

import com.viettel.csp.composer.partner.PartnerComposer;
import java.util.List;
import org.apache.log4j.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.util.GenericForwardComposer;

/**
 *
 * @author GEM
 */
public class UploadPopup extends GenericForwardComposer<Component> implements IUploaderCallback {

    private static final Logger logger = Logger.getLogger(PartnerComposer.class);
    private UploadComponent uploadComponent;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        uploadComponent.setUploaderCallback(this);
    }

    @Override
    public void uploadedFilesInfoCallback(List<FileInfo> filesInfo) {
        System.out.println(filesInfo);
    }

    @Override
    public void uploadedFilesLocationCallback(List<String> filesLocation) {
        System.out.println(filesLocation);
    }

}
