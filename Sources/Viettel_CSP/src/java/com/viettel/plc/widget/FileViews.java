/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.viettel.plc.widget;

/**
 *
 * @author admin
 */
public class FileViews {
    private String fileName;
    private String location;

    public FileViews(String fileName, String location) {
        this.fileName = fileName;
        this.location = location;
    }
    
    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
    
    
}
