/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.plc.widget;

import java.io.IOException;

import groovy.util.Eval;
import org.apache.log4j.Logger;
import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.sys.ContentRenderer;
import org.zkoss.zul.Div;
import org.zkoss.zul.Iframe;

/**
 *
 * @author admin
 */
public class PdfViewerComponent extends Div {

    private static final Logger logger = Logger.getLogger(PdfViewerComponent.class);
    private static final String SERVER_URL = "http://localhost:8080";

    private String _src;
    @Wire
    private Iframe archives;

    public void setContent(AMedia media) {
        archives.setContent(media);
    }

    public PdfViewerComponent() {
        Executions.createComponents("/controls/widget/pdfViewerComponent.zul", this, null);
        Selectors.wireComponents(this, this, false);
    }

    @Override
    protected void renderProperties(ContentRenderer renderer) throws IOException {
        super.renderProperties(renderer);
        render(renderer, "src", _src);
    }
}
