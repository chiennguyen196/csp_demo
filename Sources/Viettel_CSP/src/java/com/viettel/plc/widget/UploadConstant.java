/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.plc.widget;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 *
 * @author admin
 */
public class UploadConstant {

    private UploadConstant() throws Exception {
        throw new Exception("Should never call this");
    }

    public static int MAX_NUM_FILES = 100;
    public static int MAX_FILE_SIZE = 15420 * 1024; //15MB
    public static String UPLOAD_LOCATION = "uploaded-files";//
    
    public static String getSaveLocation() {
        Path tomcatLocation = Paths.get(System.getProperty("catalina.base"));
        String path = Paths.get(tomcatLocation.getParent().toString(), UploadConstant.UPLOAD_LOCATION).toString();
        return path;
    }
}