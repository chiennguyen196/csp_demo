/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.plc.widget;

import java.util.List;

/**
 *
 * @author Gem
 */
public interface IUploaderCallback {

    default public void uploadedFilesInfoCallback(List<FileInfo> filesInfo) {
        System.out.println(filesInfo);
    }

    @Deprecated
    public void uploadedFilesLocationCallback(List<String> filesLocation);

}
