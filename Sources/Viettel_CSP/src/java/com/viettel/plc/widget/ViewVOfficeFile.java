package com.viettel.plc.widget;

import org.apache.log4j.Logger;
import org.zkoss.zk.ui.IdSpace;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.*;

import java.util.Date;

/**
 * Created by tuannm33 on 1/19/2017.
 */
public class ViewVOfficeFile extends Hlayout implements IdSpace {

    Date errorTime;
    Logger logger = Logger.getLogger("exceptionLogger");
    @Wire
    private Textbox txtVOffice;

    @Wire
    private Button btnVOffice;

    public Textbox getTxtVOffice() {
        return txtVOffice;
    }

    public void setTxtVOffice(Textbox txtVOffice) {
        this.txtVOffice = txtVOffice;
    }

    public Button getBtnVOffice() {
        return btnVOffice;
    }

    public void setBtnVOffice(Button btnVOffice) {
        this.btnVOffice = btnVOffice;
    }

}
