/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.plc.widget;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import org.apache.log4j.Logger;
import org.zkoss.io.Files;
import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.SelectEvent;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.sys.ContentRenderer;
import org.zkoss.zul.Div;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;

/**
 *
 * @author admin
 */
public class PdfFilesViewerComponent extends Div {

    private static final Logger logger = Logger.getLogger(PdfFilesViewerComponent.class);
    @Wire
    private PdfViewerComponent pdfViewerComponent;
    @Wire
    private Listbox listFiles;

    public void setFiles(List<FileViews> files) throws FileNotFoundException {
        for (FileViews file : files) {
            listFiles.getItems().add(new Listitem(file.getFileName(), file));
        }
        if (files.size() > 0) {
            listFiles.setSelectedIndex(0);
            FileViews f = files.get(0);
            java.io.File file = new java.io.File(f.getLocation() + "/" + f.getFileName());
            AMedia amedia = new AMedia(file, "application/pdf", null);
            pdfViewerComponent.setContent(amedia);
        }
    }

    public PdfFilesViewerComponent() {
        Executions.createComponents("/controls/widget/pdfFilesViewerComponent.zul", this, null);
        Selectors.wireComponents(this, this, false);
        Selectors.wireEventListeners(this, this);
    }

    @Override
    protected void renderProperties(ContentRenderer renderer) throws IOException {
        super.renderProperties(renderer);
    }

    @Listen("onSelect = #listFiles")
    public void onSelect$listFiles(SelectEvent event) {
        try {
            Object o = event.getTarget().getFellow("listFiles");
            Listbox listBox = (Listbox) o;
            FileViews f = listBox.getSelectedItem().getValue();
            java.io.File file = new java.io.File(f.getLocation() + "/" + f.getFileName());
            AMedia amedia = new AMedia(file, "application/pdf", null);
            pdfViewerComponent.setContent(amedia);
        } catch (Exception ex) {
            pdfViewerComponent.setContent(null);
            java.util.logging.Logger.getLogger(PdfFilesViewerComponent.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
