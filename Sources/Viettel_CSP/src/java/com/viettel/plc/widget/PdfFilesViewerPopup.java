/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.plc.widget;

import com.viettel.csp.composer.partner.PartnerComposer;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.ParamUtils;

import java.io.FileNotFoundException;
import java.util.List;
import org.apache.log4j.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.util.GenericForwardComposer;

/**
 *
 * @author GEM
 */
public class PdfFilesViewerPopup extends GenericForwardComposer<Component> {

    private static final Logger logger = Logger.getLogger(PartnerComposer.class);
    private PdfFilesViewerComponent pdfViewerComponentPop;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        if (this.arg.get(ParamUtils.DATA) instanceof List) {
            List<Object> files = (List<Object>) this.arg.get(ParamUtils.DATA);
            for (Object file : files) {
                if (!(file instanceof FileViews)) {
                    logger.error(LanguageBundleUtils.getMessage("global.message.pdfViewer.error.invalidDataType", FileViews.class.toString()));
                    throw new Exception(LanguageBundleUtils.getMessage("global.message.pdfViewer.error.invalidDataTypeUser"));
                }
            }
            pdfViewerComponentPop.setFiles((List<FileViews>) this.arg.get(ParamUtils.DATA));
        } else {
            logger.error(LanguageBundleUtils.getMessage("global.message.pdfViewer.error.invalidDataType", FileViews.class.toString()));
            throw new Exception(LanguageBundleUtils.getMessage("global.message.pdfViewer.error.invalidDataTypeUser"));
        }
    }

    public void setFiles(List<FileViews> files) {
        try {
            pdfViewerComponentPop.setFiles(files);
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }
}
