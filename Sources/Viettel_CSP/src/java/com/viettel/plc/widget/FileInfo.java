/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.plc.widget;

/**
 *
 * @author GEM
 */
public class FileInfo {

    private String location;
    private String description;

    public FileInfo(String location, String description) {
        this.location = location;
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public String getDescription() {
        return description;
    }
}
