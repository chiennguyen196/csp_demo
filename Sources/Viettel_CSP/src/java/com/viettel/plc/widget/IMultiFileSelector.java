/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.plc.widget;

/**
 *
 * @author luannt23
 */
public interface IMultiFileSelector {

    public boolean delFile(String fileName);

    public boolean downFile(String fileName);

    public int sizeOfFile(String fileName);

    public void invalidate();
}
