/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.plc.widget;

import com.viettel.csp.util.LanguageBundleUtils;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import org.apache.log4j.Logger;
import org.zkoss.io.Files;
import org.zkoss.util.media.Media;
import org.zkoss.zhtml.Div;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.IdSpace;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;

/**
 *
 * @author gem
 */
public class FileDisplay extends Div implements IdSpace {

    Logger logger = Logger.getLogger(FileDisplay.class);
    @Wire
    private Image btnDel;
    @Wire
    private Label lblFileName;
    private Media data = null;
    private String safeValue = "";
    private MultiFileSelector multiFileSelector;
//    private FileSelector fileSelector;

    @Wire
    private Textbox txtFileDescription;

    public String getFileDescription() {
        return this.txtFileDescription != null ? this.txtFileDescription.getValue() : "";
    }

    public Image getBtnDel() {
        return btnDel;
    }

    public void setMultiFileSelector(MultiFileSelector multiFileSelector) {
        this.multiFileSelector = multiFileSelector;
    }

//    public void setFileSelector(FileSelector fileSelector) {
//        this.fileSelector = fileSelector;
//    }

    public void setBtnDel(Image btnDel) {
        this.btnDel = btnDel;
    }

    public Label getLblFileName() {
        return lblFileName;
    }

    public void setLblFileName(Label lblFileName) {
        this.lblFileName = lblFileName;
    }

    public String getSafeValue() {
        return safeValue;
    }

    public void setSafeValue(String safeValue) {
        this.safeValue = safeValue;
    }

    public Media getData() {
        return data;
    }

    public void setData(Media data) {
        this.data = data;
    }

    public FileDisplay() {
        Executions.createComponents("/controls/widget/fileDisplay.zul", this, null);
        Selectors.wireComponents(this, this, false);
        Selectors.wireEventListeners(this, this);
    }

    @Listen("onUpload = #btnAddFiles")
    public void onUpload$btnAddFiles(UploadEvent evt) {
        multiFileSelector.onUpload$btnAddFile(evt);
        this.detach();
    }

    @Listen("onClick = #btnDel")
    public void onClick$btnDel() {
        try {
            if (multiFileSelector.getPnlMutilFile().getChildren().size() > 1) {
                this.detach();
            } else {
                Clients.showNotification(LanguageBundleUtils.getString("global.message.upload.error.atLeastOneFile"), "info", null, "middle_center", 1000);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    public boolean init(String fileName, Media data) {
        lblFileName.setValue(fileName);
        this.data = data;
        return true;
    }

    public boolean init(String fileName, String safeName, Media data) {
        lblFileName.setValue(fileName);
        this.data = data;
        this.safeValue = safeName;
        return true;
    }

    public String doUploadFile(String contextPath) {
        Execution exec = Executions.getCurrent();
        String path = exec.toAbsoluteURI(Paths.get(contextPath, safeValue).toString(), false);
        try {
            File file = new File(path);
            if (data.isBinary()) {
                Files.copy(file, data.getStreamData());
            } else {
                BufferedWriter writer = new BufferedWriter(new FileWriter(file.getAbsolutePath()));
                Files.copy(writer, data.getReaderData());
                writer.close();
            }
        } catch (IOException ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.upload.error.unknown"), "info", null, "middle_center", 1000);
        }
        return path;
    }
}
