/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.plc.widget;

import com.viettel.csp.util.LanguageBundleUtils;
import org.apache.log4j.Logger;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.IdSpace;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Button;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.Vbox;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author luannt23
 */
public class MultiFileSelector extends Hlayout implements IdSpace {

    Date errorTime;
    Logger logger = Logger.getLogger(MultiFileSelector.class);
    @Wire
    private Vbox pnlMultiFile;
    @Wire
    private Button btnAddFile;

    public Vbox getPnlMutilFile() {
        return pnlMultiFile;
    }

    public void setPnlMutilFile(Vbox pnlMutilFile) {
        this.pnlMultiFile = pnlMutilFile;
    }

    public Button getBtnAddFile() {
        return btnAddFile;
    }

    public void setBtnAddFile(Button btnAddFile) {
        this.btnAddFile = btnAddFile;
    }

    public MultiFileSelector() {
        Executions.createComponents("/controls/widget/multiFileSelector.zul", this, null);
        Selectors.wireComponents(this, this, false);
        Selectors.wireEventListeners(this, this);
        pnlMultiFile.appendChild(createFileDisplay());
    }

    private FileDisplay createFileDisplay() {
        FileDisplay result = new FileDisplay();
        result.setMultiFileSelector(this);
        return result;
    }

    public String getSafeValue() {
        String result = "";
        List<Component> coms = pnlMultiFile.getChildren();
        for (Component com : coms) {
            if (com instanceof FileDisplay) {
                FileDisplay file = (FileDisplay) com;
                result += file.getSafeValue() + ",";
            }
        }
        return result.isEmpty() ? null : (result.length() - 1 > 1000) ? null : result.substring(0, result.length() - 1);
    }

    public List<Media> getData() {

        List<Media> result = new ArrayList<Media>();
        List<Component> coms = pnlMultiFile.getChildren();
        for (Component com : coms) {
            if (com instanceof FileDisplay) {
                FileDisplay file = (FileDisplay) com;
                if (file.getData() != null) {
                    result.add(file.getData());
                }
            }
        }
        return result;
    }

    /**
     * @param contextPath Duong dan toi thu muc luu file upload
     * @return
     */
    public List<FileInfo> doUploadFile(String contextPath) {
        List<FileInfo> filesLocation = new ArrayList<>();
        List<Component> coms = pnlMultiFile.getChildren();
        for (Component com : coms) {
            if (com instanceof FileDisplay) {
                FileDisplay file = (FileDisplay) com;
                if (file.getData() != null) {
                    filesLocation.add(new FileInfo(file.doUploadFile(contextPath), file.getFileDescription()));
                }
            }
        }
        return filesLocation;
    }

    public void onUpload$btnAddFile(UploadEvent evt) {
        try {
            Media[] medias = evt.getMedias();
            if (!validateFile(medias)) {
                validateFileWithException(medias);
                return;
            }
            if ((pnlMultiFile.getChildren().size() + medias.length) > (UploadConstant.MAX_NUM_FILES + 1)) {
                Clients.showNotification(LanguageBundleUtils.getMessage("global.message.upload.error.toManyFiles", "5"));
                //because have been remove from FileDisplay
                pnlMultiFile.appendChild(createFileDisplay());
                return;
            }
            if (medias.length > 0) {
                for (Media media : medias) {
                    String name = getCorrectFileName(media);
                    FileDisplay display = new FileDisplay();
                    display.init(name, media);
                    display.setSafeValue(genFileName(name));
                    display.setMultiFileSelector(this);
                    pnlMultiFile.appendChild(display);
                }
            }
        } catch (Exception ex) {
            Clients.showNotification(ex.getMessage(), "info", null, "middle_center", 1000);
        }
    }

    private boolean validateFile(Media[] medias) {
        if (medias == null) {
            return false;
        }
        for (Media media : medias) {
            String fileName = media.getName();
            if (!isCorrectExtension(fileName.substring(fileName.lastIndexOf(".") == -1 ? fileName.length() : fileName.lastIndexOf(".")))) {
                return false;
            }
            if ((media.isBinary() && media.getByteData().length > UploadConstant.MAX_FILE_SIZE) ||
                    (!media.isBinary() && media.getStringData().getBytes().length > UploadConstant.MAX_FILE_SIZE )) {
                return false;
            }
        }
        return true;
    }

    private void validateFileWithException(Media[] medias) throws Exception {
        if (medias == null) {
            throw new Exception(LanguageBundleUtils.getString("global.message.upload.error.noFileUpload"));
        }
        for (Media media : medias) {
            String fileName = media.getName();
            if (!isCorrectExtension(fileName.substring(fileName.lastIndexOf(".") == -1 ? fileName.length() : fileName.lastIndexOf(".")))) {
                throw new Exception(LanguageBundleUtils.getString("global.message.upload.error.incorrectExtension"));
            }
            if (media.getByteData().length > UploadConstant.MAX_FILE_SIZE) {
                throw new Exception(LanguageBundleUtils.getString("global.message.upload.error.maxFileSize"));
            }
        }
    }

    private String getCorrectFileName(Media data) {
        return data.getName().replaceAll(",", "-");
    }

    private boolean isCorrectExtension(String extension) {
        return extension.length() > 1 && extension.length() < 10;
    }

    private String genFileName(String fileName) {
        int endIndex;
        endIndex = (fileName.lastIndexOf(".") == -1) ? fileName.length() : fileName.lastIndexOf(".");
        endIndex = endIndex > 200 ? 200 : endIndex;

        String name = fileName.substring(0, endIndex);
        String extension = fileName.substring(fileName.lastIndexOf(".") == -1 ? fileName.length() : fileName.lastIndexOf("."));
        return name + "_" + (new Date()).getTime() + extension;
    }

    void addNewFile() {
        FileDisplay newFileDisplay = new FileDisplay();
        newFileDisplay.setMultiFileSelector(this);
        pnlMultiFile.appendChild(newFileDisplay);
    }

    public int numOfFiles() {
        return pnlMultiFile.getChildren().size();
    }

    public void validate() throws Exception {
        if (numOfFiles() == 0) {
            throw new Exception(LanguageBundleUtils.getString("global.message.upload.error.nofileSelected"));
        }

        List<Component> coms = pnlMultiFile.getChildren();
        for (Component com : coms) {
            if (com instanceof FileDisplay) {
                FileDisplay file = (FileDisplay) com;
                if (file.getData() == null) {
                    throw new Exception(LanguageBundleUtils.getString("global.message.upload.error.noSelectedFile"));
                }
            }
        }
    }
}
