/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.plc.widget;

import com.viettel.csp.util.LanguageBundleUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import org.apache.log4j.Logger;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.IdSpace;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.sys.ContentRenderer;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Button;
import org.zkoss.zul.Vlayout;

/**
 * @author GEM
 */
public class UploadComponent extends Vlayout implements IdSpace {

    private static final Logger logger = Logger.getLogger(UploadComponent.class);
    @Wire
    private MultiFileSelector txtMultiFileAttach;
    private IUploaderCallback uploaderCallback;
    private boolean _notificationOnUploaded = false;
    @Wire
    private Button doSave;
    @Wire
    private Button addFile;

    public void setUploadIcon(boolean uploadIcon) {
        doSave.setVisible(false);
    }
    public void setVisibleAddFile(boolean uploadIcon) {
        addFile.setVisible(uploadIcon);
    }
    public void setVisibleDescription(boolean uploadIcon) {
        addFile.setVisible(uploadIcon);
    }

    public void setNotificationOnUploaded(boolean notificationOnUploaded) {
        this._notificationOnUploaded = notificationOnUploaded;
    }

    public void setUploaderCallback(IUploaderCallback uploaderCallback) {
        this.uploaderCallback = uploaderCallback;
    }

    public UploadComponent() {
        Executions.createComponents("/controls/widget/uploadComponent.zul", this, null);
        Selectors.wireComponents(this, this, false);
        Selectors.wireEventListeners(this, this);
        doSave.setVisible(false);
//        doSave.setVisible(true);
    }

    @Listen("onClick = #addFile")
    public void onClick$addFile() {
        if (txtMultiFileAttach.numOfFiles() < UploadConstant.MAX_NUM_FILES) {
            txtMultiFileAttach.addNewFile();
            txtMultiFileAttach.invalidate();
        } else {
            Clients.showNotification(LanguageBundleUtils.getMessage("global.message.upload.error.toManyFiles"), "info", null, "middle_center", 1000);
        }
    }

    @Listen("onClick = #doSave")
    public void onClick$doSave() {
        String message = (String)Sessions.getCurrent().getAttribute("upload");
        String violation = (String)Sessions.getCurrent().getAttribute("violation");
        if (message != null && violation == null )
            return;

        try {
             if (!this.validateFiles()) {
                return;
            }
            List<FileInfo> filesInfo = txtMultiFileAttach.doUploadFile(UploadConstant.getSaveLocation());
            if (_notificationOnUploaded) {
                Clients.showNotification(LanguageBundleUtils.getString("global.message.upload.successful"), "info", null, "middle_center", 1000);
            }
            if (uploaderCallback != null) {
                uploaderCallback.uploadedFilesInfoCallback(filesInfo);
                List<String> filesLocation = new ArrayList<>();
                filesInfo.forEach((FileInfo info) -> filesLocation.add(info.getLocation()));
                uploaderCallback.uploadedFilesLocationCallback(filesLocation);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            return;
        }

    }

    private boolean validateFiles() {
        try {
            txtMultiFileAttach.validate();
            return true;
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(UploadPopup.class.getName()).log(Level.SEVERE, null, ex);
            Clients.showNotification(ex.getMessage(), "info", null, "middle_center", 1000);
            return false;
        }
    }

    @Override
    protected void renderProperties(ContentRenderer renderer) throws IOException {
        super.renderProperties(renderer);
        render(renderer, "notificationOnUploaded", _notificationOnUploaded);
    }
}
