package com.viettel.plc.pdfviewer;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.DocumentException;
import com.viettel.csp.util.DateTimeUtils;

import java.io.*;
import java.net.URLDecoder;
import java.util.Date;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//@WebServlet(urlPatterns = {"/SecurityServlet"})
public class SecurityServlet extends HttpServlet {

    private org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(SecurityServlet.class);

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Kiem tra valid
        Map<String, String> params = WebUtils.getQueryMap(request.getQueryString());
        String token = params.get("token");
        String userToken = params.get("userToken");
        String fileId = URLDecoder.decode(params.get("fileId"));
        String fileView = URLDecoder.decode(params.get("fileView"));

        if (token == null || !"default".equals(token)) {
            return;
        }
        if (fileId == null) {
            return;
        }

        String uploadFolder = WebUtils.getServerPath("PATH_FILE_POLICY");
        String viewFolder = WebUtils.getServerPath("VIEW_FILE_FOLDER");

        //Chen watermark khi xem file
        PdfReader reader = null;
        PdfStamper stamper;
        try {
            reader = new PdfReader(uploadFolder + fileId);
            int n = reader.getNumberOfPages();
            stamper = new PdfStamper(reader, new FileOutputStream(viewFolder + fileView));
            // text watermark
            Font font = new Font(Font.FontFamily.HELVETICA, 20);
            Phrase p = new Phrase(userToken + "_" + DateTimeUtils.convertDateToString(new Date(), "HH:mm dd/MM/yyyy"), font);
            for (int l = 0; l < 10; l++) {
                p.add("      " + userToken + "_" + DateTimeUtils.convertDateToString(new Date(), "HH:mm dd/MM/yyyy"));
            }
            // transparency
            PdfGState gs1 = new PdfGState();
            gs1.setFillOpacity(0.3f);
            // properties
            PdfContentByte over;
            // loop over every page
            for (int i = 1; i <= n; i++) {
                over = stamper.getOverContent(i);
                over.saveState();
                over.setGState(gs1);
                ColumnText.showTextAligned(over, Element.ALIGN_CENTER, p, 300f, 500f, 55f);
                over.restoreState();
            }
            stamper.close();
        } catch (DocumentException de) {
            logger.error(de.getMessage(), de);
        } finally {
            if (reader != null) {
                reader.close();
            }
        }

        // Lay file name trong db
        String filePath = viewFolder + fileView;
        response.setContentType("application/octet-stream");
        FileInputStream fileInput = null;
        BufferedInputStream input = null;
        BufferedOutputStream output = null;
        try {
            //file is a File object or a String containing the name of the file to download
            fileInput = new FileInputStream(filePath);
            input = new BufferedInputStream(fileInput);
            output = new BufferedOutputStream(response.getOutputStream());
            //read the data from the file in chunks
            byte[] buffer = new byte[1024 * 4];
            for (int length = 0; (length = input.read(buffer)) > 0; ) {
                //copy the data from the file to the response in chunks
                output.write(buffer, 0, length);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        } finally {
            //close resources
            if (output != null) {
                try {
                    output.close();
                } catch (IOException ignore) {
                    logger.error(ignore.getMessage(), ignore);
                }
            }
            if (input != null) {
                try {
                    input.close();
                } catch (IOException ignore) {
                    logger.error(ignore.getMessage(), ignore);
                }
            }
            if (fileInput != null) {
                try {
                    fileInput.close();
                } catch (IOException ignore) {
                    logger.error(ignore.getMessage(), ignore);
                }
            }
        }

    }

//    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
//    /**
//     * Handles the HTTP <code>GET</code> method.
//     *
//     * @param request servlet request
//     * @param response servlet response
//     * @throws ServletException if a servlet-specific error occurs
//     * @throws IOException if an I/O error occurs
//     */
//    @Override
//    protected void doGet(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//        processRequest(request, response);
//    }
//
//    /**
//     * Handles the HTTP <code>POST</code> method.
//     *
//     * @param request servlet request
//     * @param response servlet response
//     * @throws ServletException if a servlet-specific error occurs
//     * @throws IOException if an I/O error occurs
//     */
//    @Override
//    protected void doPost(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//        processRequest(request, response);
//    }
//
//    /**
//     * Returns a short description of the servlet.
//     *
//     * @return a String containing servlet description
//     */
//    @Override
//    public String getServletInfo() {
//        return "Short description";
//    }// </editor-fold>

}
