package com.viettel.plc.pdfviewer;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.net.URI;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.http.client.utils.URIBuilder;

public class WebUtils {

    public static Map<String, String> getQueryMap(String query) {
        if (query == null || query.isEmpty()) {
            return new HashMap<>();
        }
        String[] params = query.split("&");
        Map<String, String> map = new HashMap<>();
        for (String param : params) {
            String name = param.split("=")[0];
            String value = param.split("=")[1];
            map.put(name, value);
        }
        return map;
    }
    
    public static String getCurrentDir() {
        String classResource = "com/viettel/restful/config";	// package
        URL urlFileDB = Thread.currentThread().getContextClassLoader().getResource(classResource);
        String path = urlFileDB.getPath();
        path = path.split(classResource)[0];
        return path;
    }
    
    public static URI getURL(String scheme, String host, int port, String path, HashMap<String, String> params) throws Exception {
        URIBuilder uriBuilder = new URIBuilder()
                .setScheme(scheme)
                .setPort(port)
                .setHost(host)
                .setPath(path);
        //.setCharset(charset);
        if (params != null) {
            for (Object key : params.keySet()) {
                uriBuilder = uriBuilder.setParameter((String) key, (String) params.get(key));
            }
        }
        return uriBuilder.build();
    }

    public static String getServerPath(String icReportDirectory) {
        ResourceBundle resbundCas = ResourceBundle.getBundle("config");
        String serverPath = resbundCas.getString(icReportDirectory);
        return serverPath;
    }

}
