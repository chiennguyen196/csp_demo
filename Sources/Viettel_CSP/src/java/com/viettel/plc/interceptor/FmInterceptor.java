/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.plc.interceptor;

import com.viettel.csp.util.ParamUtils;
import java.util.Date;
import org.zkoss.zk.ui.util.RequestInterceptor;

/**
 *
 * @author tuanna67
 */
public class FmInterceptor implements RequestInterceptor {
    @Override
    public void request(org.zkoss.zk.ui.Session sess,Object request, Object response) {    
        sess.setAttribute(ParamUtils.REQUEST_TIME, new Date());        
    }    
}
