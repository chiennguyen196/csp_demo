/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.logincsp.composer;

import com.viettel.csp.entity.UserEntity;
import com.viettel.csp.service.UserService;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.eafs.util.SpringUtil;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;

/**
 *
 * @author admin
 */
public class UserForgotPassComposer extends GenericForwardComposer<Component> {

    private Textbox txtTenTaiKhoan;
    private final UserService userService = SpringUtil.getBean("userService", UserService.class);

    final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger("ContactComposer");

    public void onClick$btnSend() {
        UserEntity userEntity = new UserEntity();
        userEntity.setUserName(txtTenTaiKhoan.getValue());
        UserEntity ue = userService.checkEntity(userEntity);
        if (null != ue) {
            if (checkHour(ue.getVerifyCodeDate().toString()) == true) {
                ue = userService.sendEmail(userEntity);
                Executions.sendRedirect("/loginlogout/convertpass.zul");
                Sessions.getCurrent().setAttribute("username", userEntity.getUserName());
            } else {
                Clients.showNotification(LanguageBundleUtils.getString("Tài khoản không tồn tại hoặc không chính xác!"), "warning", null, "middle_center", 3000);
                Messagebox.show(LanguageBundleUtils.getString("Mã code đã quá thời gian cho phép, xác nhận tài khoản để lấy thông tin code!"), "Confirm Dialog", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION, new org.zkoss.zk.ui.event.EventListener() {
                    public void onEvent(Event evt) throws Exception {
                        if ("onOK".equals(evt.getName())) {
                            UserEntity userEntity = new UserEntity();
                            userEntity.setUserName(txtTenTaiKhoan.getValue());
                            UserEntity ue = userService.checkEntity(userEntity);
                            ue = userService.sendEmail(userEntity);
                            Executions.sendRedirect("/loginlogout/convertpass.zul");
                            Sessions.getCurrent().setAttribute("username", userEntity.getUserName());
                        }
                    }
                });
            }
        } else {
            Clients.showNotification(LanguageBundleUtils.getString("Tài khoản không tồn tại hoặc không chính xác!"), "warning", null, "middle_center", 3000);
        }
    }

    public boolean checkHour(String hour) {

        java.util.Date date = new java.util.Date();
        java.sql.Timestamp sqlTimeStamp = new java.sql.Timestamp(date.getTime());
        System.out.println("util-date:" + date);
        System.out.println("sql-timestamp:" + sqlTimeStamp);
        String dateStart = hour;
        String dateStop = sqlTimeStamp.toString();

        //HH converts hour in 24 hours format (0-23), day calculation
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date d1 = null;
        Date d2 = null;

        try {
            d1 = format.parse(dateStart);
            d2 = format.parse(dateStop);
//
            //in milliseconds
            long diff = d2.getTime() - d1.getTime();

            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long diffDays = diff / (24 * 60 * 60 * 1000);
            if (diffDays > 0 || diffHours > 12) {
                return false;
            } else {
                return true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return checkHour(hour);
    }

}
