/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.logincsp.composer;

import com.viettel.csp.entity.UserEntity;
import com.viettel.csp.service.UserService;
import com.viettel.csp.util.EncryptionUtils;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.eafs.util.SpringUtil;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import oracle.sql.TIMESTAMPLTZ;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Textbox;
import org.zkoss.zk.ui.util.Clients;

/**
 *
 * @author admin
 */
public class ConvertPassComposer extends GenericForwardComposer<Component> {

    private Textbox txtVerifyCode, txtPass, txtPass2;
    private final UserService userService = SpringUtil.getBean("userService", UserService.class);
    UserEntity entity;
    String user = null;
    final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger("ContactComposer");

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp); //To change body of generated methods, choose Tools | Templates.
//        Sessions.getCurrent().getAttribute("username");
        user = (String) session.getAttribute("username");
    }

    public void onClick$btnUpdate() throws ParseException {
        UserEntity userEntity = new UserEntity();
        userEntity.setUserName(user);
        UserEntity ue = userService.checkEntity(userEntity);

        if (txtVerifyCode.getValue().equals(ue.getVerifyCode())) {
            if (txtPass.getValue().equals(txtPass2.getValue())) {
                ue.setPassword(EncryptionUtils.sha256WithSalt(txtPass.getValue(), ue.getRandomKey()));;
                ue.setPassword(EncryptionUtils.sha256WithSalt(txtPass2.getValue(), ue.getRandomKey()));
                java.util.Date date = new java.util.Date();
                java.sql.Timestamp sqlTimeStamp = new java.sql.Timestamp(date.getTime());
                ue.setVerifyCodeDate(sqlTimeStamp);
                userService.updateUser(ue);
                Clients.showNotification(LanguageBundleUtils.getString("Cập nhật mật khẩu thành công"), "warning", null, "middle_center", 3000);
                Executions.sendRedirect("/login.zul");
            } else {
                Clients.showNotification(LanguageBundleUtils.getString("Mật khẩu không trùng nhau"), "warning", null, "middle_center", 3000);
            }

        } else {
            Clients.showNotification(LanguageBundleUtils.getString("Mã code gửi email không đúng"), "warning", null, "middle_center", 3000);
        }
    }

}
