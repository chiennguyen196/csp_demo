    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.logincsp.composer;

import com.viettel.csp.DTO.BankDTO;
import com.viettel.csp.DTO.ComponentDTO;
import com.viettel.csp.DTO.PartnerBankDTO;
import com.viettel.csp.DTO.PartnerDTO;
import com.viettel.csp.composer.partner.PartnerComposer;
import com.viettel.csp.composer.partner.PartnerPopupComposer;
import com.viettel.csp.entity.PartnerBankEntity;
import com.viettel.csp.entity.PartnerEntity;
import com.viettel.csp.entity.UserEntity;
import com.viettel.csp.service.BankService;
import com.viettel.csp.service.LocationService;
import com.viettel.csp.service.MessageService;
import com.viettel.csp.service.PartnerBankService;
import com.viettel.csp.service.PartnerService;
import com.viettel.csp.service.PartnerTypeService;
import com.viettel.csp.service.UserService;
import com.viettel.csp.util.GenComponent;
import com.viettel.csp.util.KeyValueBean;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.util.StringUtils;
import com.viettel.eafs.util.SpringUtil;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;
import static jxl.biff.BaseCellFeatures.logger;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;
import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author admin
 */
public class UpdateRegisterUserComposer extends GenericForwardComposer<Component> {

    private Long totalRow = 0L;
    private String action = "";
    private Listbox lbxAtribute;
    private List<Integer> indexs = new ArrayList();
    private PartnerDTO oldDTO = null;
    private List<Component> lstInputCbbBankCtrls = new ArrayList<>();
    private List<Component> lstInputTextBankCtrls = new ArrayList<>();
    private Rows rows;
    private Row lastRow;
    private Datebox dbBusinessLicenceDate;
    private Textbox txtParnerCode, txtParnerName, txtParnerShortName, txtAddressOffice, txtAddressTransaction, txtphone, txtRepresentLaw, txtPositionLaw, txtEmailTransaction, txtTaxCode, txtBusinessLicenceNo, txtBusinessLicenceCount;
    private Combobox cboGroupPartner, cboProvince, cboTypePartner;
    private ListModelList<KeyValueBean> listBankModel = new ListModelList<KeyValueBean>();
    private ListModelList<KeyValueBean> listLocationModel = new ListModelList<KeyValueBean>();
    private ListModelList<KeyValueBean> listPartnerTypeModel = new ListModelList<KeyValueBean>();
    private DateFormat df = new SimpleDateFormat("dd/MM/yyy");
    private GenComponent genComponent = new GenComponent();
    private static final Logger log = Logger.getLogger(PartnerPopupComposer.class);

    @Autowired
    private PartnerComposer partnerComposer;
    private BankService bankService = SpringUtil.getBean("bankService", BankService.class);
    private LocationService locationService = SpringUtil.getBean("locationService", LocationService.class);
    private PartnerService partnerService = SpringUtil.getBean("partnerService", PartnerService.class);
    private PartnerTypeService partnerTypeService = SpringUtil.getBean("partnerTypeService", PartnerTypeService.class);
    private MessageService messageService = SpringUtil.getBean("messageService", MessageService.class);
    private PartnerBankService partnerBankService = SpringUtil.getBean("partnerBankService", PartnerBankService.class);
    private UserService userService = SpringUtil.getBean("userService", UserService.class);

    public PartnerComposer getPartnerComposer() {
        if (partnerComposer == null) {
            partnerComposer = new PartnerComposer();
        }
        return partnerComposer;
    }

    public void setPartnerComposer(PartnerComposer partnerComposer) {
        this.partnerComposer = partnerComposer;
    }

    public ListModelList<KeyValueBean> getListBankModel() {
        return listBankModel;
    }

    public void setListBankModel(ListModelList<KeyValueBean> listBankModel) {
        this.listBankModel = listBankModel;
    }

    public ListModelList<KeyValueBean> getListLocationModel() {
        return listLocationModel;
    }

    public void setListLocationModel(ListModelList<KeyValueBean> listLocationModel) {
        this.listLocationModel = listLocationModel;
    }

    public ListModelList<KeyValueBean> getListPartnerTypeModel() {
        return listPartnerTypeModel;
    }

    public void setListPartnerTypeModel(ListModelList<KeyValueBean> listPartnerTypeModel) {
        this.listPartnerTypeModel = listPartnerTypeModel;
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp); //To change body of generated methods, choose Tools | Templates.
        if (arg.containsKey(ParamUtils.ACTION)) {
            action = arg.get(ParamUtils.ACTION).toString();
        }
        if (arg.containsKey("LISTBOX")) {
            lbxAtribute = (Listbox) arg.get("LISTBOX");
        }
        if (arg.containsKey("LIST_ACTION_INDEX")) {
            indexs = (List<Integer>) arg.get("LIST_ACTION_INDEX");
        }

        ((Window) self).setTitle(LanguageBundleUtils.getString("global.add"));
        bindDataToCombo();
    }

    public void loadComboboxProvince() throws Exception {
        List<KeyValueBean> lstProvince = new ArrayList<KeyValueBean>();
        lstProvince = locationService.getListDataToCombobox("PROVINCE", null);
        listLocationModel = new ListModelList<KeyValueBean>();
        listLocationModel.addAll(lstProvince);
        //listLocationModel.addToSelection(lstProvince.get(0));
        cboProvince.setModel(listLocationModel);
    }

    public void loadComboboxTypePartner() throws Exception {
        List<KeyValueBean> lstPartnerType = new ArrayList<KeyValueBean>();
        lstPartnerType = partnerTypeService.getListToKeyValueBean(true);
        listPartnerTypeModel = new ListModelList<KeyValueBean>();
        listPartnerTypeModel.addAll(lstPartnerType);
        cboTypePartner.setModel(listPartnerTypeModel);
    }

    public List<PartnerBankDTO> getInfoBankByPartnerId(Long partnerId) throws Exception {
        return partnerBankService.getInfoBankByPartnerId(partnerId);
    }

    public void bindDataToCombo() throws Exception {
        loadComboboxTypePartner();
        loadComboboxProvince();

    }

    public List<PartnerBankEntity> convertToListBankEntity(List<PartnerBankDTO> lstPartnerBankDTO) {
        PartnerBankEntity partnerBankEntity = null;
        List<PartnerBankEntity> lstPartnerBankEntity = new ArrayList<PartnerBankEntity>();

        for (int i = 0; i < lstPartnerBankDTO.size(); i++) {
            partnerBankEntity = PartnerBankEntity.setEntity(lstPartnerBankDTO.get(i), partnerBankEntity);
            lstPartnerBankEntity.add(partnerBankEntity);
        }

        return lstPartnerBankEntity;
    }

    public void onClick$btnAddBankPopup() {
        Row tmpRow;
        Component tmpComp;
        Component firstComp = null;
        try {
            totalRow++;
            tmpRow = new Row();
            List<BankDTO> lstBankDTO = bankService.findAll();
            ComponentDTO componentTextDTO = new ComponentDTO();
            componentTextDTO.setAttributeType(ParamUtils.ReportDataType.STRING);
            componentTextDTO.setAttributeLength("50");
            componentTextDTO.setIsRequired(false);
            componentTextDTO.setStandardAttributeId(String.valueOf("txt" + totalRow));
            componentTextDTO.setDefaultValue("");
            componentTextDTO.setAttributeName(LanguageBundleUtils.getString("partner.accountNumber"));
            tmpComp = genComponent.createComponent(componentTextDTO);
            genComponent.createCellLabel(componentTextDTO.getAttributeName(), componentTextDTO.getIsRequired(), tmpRow);
            if (tmpComp != null) {
                tmpRow.appendChild(tmpComp);
                lstInputTextBankCtrls.add(tmpComp);
                if (firstComp == null) {
                    firstComp = tmpComp;
                }
            }
            rows.insertBefore(tmpRow, lastRow);

            tmpRow = new Row();
            ComponentDTO componentDTO = new ComponentDTO();
            componentDTO.setAttributeType(ParamUtils.ReportDataType.COMBOBOX);
            componentDTO.setAttributeType(ParamUtils.ReportDataType.COMBOVALUE);
            componentDTO.setIsRequired(false);
            componentDTO.setStandardAttributeId(String.valueOf("cbb" + totalRow));
            componentDTO.setDefaultValue(null);
            componentDTO.setAttributeName(LanguageBundleUtils.getString("partner.bank"));
            StringBuilder parValue = new StringBuilder();
            for (BankDTO itemBankDTO : lstBankDTO) {
                parValue.append(itemBankDTO.getCode());
                parValue.append(";");
                parValue.append(itemBankDTO.getName());
                parValue.append(";");
            }
            componentDTO.setInitValue(parValue.toString());
            tmpComp = genComponent.createComponent(componentDTO);
            genComponent.createCellLabel(componentDTO.getAttributeName(), componentDTO.getIsRequired(), tmpRow);
            if (tmpComp != null) {
                tmpRow.appendChild(tmpComp);
                lstInputCbbBankCtrls.add(tmpComp);
                if (firstComp == null) {
                    firstComp = tmpComp;
                }
            }
            rows.insertBefore(tmpRow, lastRow);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    public void onClick$btnRemoveBankPopup() throws Exception {
        try {
            removeComponentBank(2);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    private void removeComponentBank(int count) {
        int indexOf = rows.getChildren().indexOf(lastRow);
        if (indexOf >= 18) {
            for (int i = 1; i <= count; i++) {
                totalRow--;
                Component component = rows.getChildren().get(indexOf - i);
                if (i % 2 == 1) {
                    lstInputCbbBankCtrls.remove(component.getChildren().get(1));
                } else {
                    lstInputTextBankCtrls.remove(component.getChildren().get(1));
                }
                rows.removeChild(component);
            }
        }
    }

    public void onClick$btnSavePopup() throws Exception {
        try {
            if (validate()) {
                UserEntity newUser = getSavedUser();
                PartnerDTO newPartnerDTO = getDataFromClient();
              partnerService.insert(newPartnerDTO);
              List<PartnerDTO> listPartner = partnerService.findAll();
              newUser.setPersonInfoId(listPartner.get(listPartner.size()-1).getId());
                userService.insert(newUser);
               // Clients.showNotification(LanguageBundleUtils.getMessage("global.message.update.successful", LanguageBundleUtils.getString("global.edit")), "info", null, "middle_center", 3000);
                userService.sendEmail(newUser);
                Sessions.getCurrent().setAttribute("user", newUser);
                Executions.sendRedirect("/loginlogout/confirmuser.zul");
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    public PartnerDTO getDataFromClient() throws Exception {
        PartnerDTO object = new PartnerDTO();

        object.setPartnerCode(txtParnerCode.getValue().trim());
        object.setCompanyName(txtParnerName.getValue().trim());
        object.setCompanyNameShort(txtParnerShortName.getValue().trim());
        object.setAddressOffice(txtAddressOffice.getValue().trim());
        object.setAddressTradingOffice(txtAddressTransaction.getValue().trim());
        object.setPhone(txtphone.getValue().trim());
        object.setRepName(txtRepresentLaw.getValue().trim());
        object.setRepPosition(txtPositionLaw.getValue().trim());
        object.setEmail(txtEmailTransaction.getValue().trim());
        object.setTaxCode(txtTaxCode.getValue().trim());
        object.setBusinessRegisNumber(txtBusinessLicenceNo.getValue().trim());
        object.setBusinessRegisDate(dbBusinessLicenceDate.getValue());
        object.setBusinessRegisCount(txtBusinessLicenceCount.getValue().trim());
        if (cboGroupPartner.getSelectedItem() != null) {
            String groupPartner = cboGroupPartner.getSelectedItem().getValue().toString();
            object.setPartnerGroup(groupPartner);
            if (groupPartner.equals(ParamUtils.PARTNER_GROUP.INTERNAL)) {
                object.setPartnerGroupProvince(cboProvince.getSelectedItem().getValue().toString());
            } else {
                object.setPartnerGroupProvince("");
            }
        }
        if (cboTypePartner.getSelectedItem() != null) {
            object.setPartnerTypeCode(cboTypePartner.getSelectedItem().getValue().toString());
        }
        List<PartnerBankDTO> lstPartnerBankDTO = new ArrayList<>();
        List<String> listInputBank = new ArrayList(genComponent.getDataFromControls(lstInputTextBankCtrls).values());
        List<String> listCbbBank = new ArrayList(genComponent.getDataFromControls(lstInputCbbBankCtrls).values());

        for (int i = 0, total = listInputBank.size(); i < total; i++) {
            PartnerBankDTO partnerBankDTO = new PartnerBankDTO();
            partnerBankDTO.setAccountNumber(listInputBank.get(i));
            partnerBankDTO.setBankCode(listCbbBank.get(i));
            lstPartnerBankDTO.add(partnerBankDTO);
        }
        object.setLstPartnerBank(lstPartnerBankDTO);
        return object;
    }

    private boolean validate() {
        if (!StringUtils.isValidString(txtParnerCode.getValue())) {
            Clients.wrongValue(txtParnerCode, LanguageBundleUtils.getMessage("global.validate.notnull", LanguageBundleUtils.getString("partner.code")));
            return false;
        }

        if (!StringUtils.isValidString(txtParnerShortName.getValue())) {
            Clients.wrongValue(txtParnerShortName, LanguageBundleUtils.getMessage("global.validate.notnull", LanguageBundleUtils.getString("partner.shortName")));
            return false;
        }

        if (!StringUtils.isValidString(txtParnerName.getValue())) {
            Clients.wrongValue(txtParnerName, LanguageBundleUtils.getMessage("global.validate.notnull", LanguageBundleUtils.getString("partner.name")));
            return false;
        }

        if (!StringUtils.isValidString(txtAddressOffice.getValue())) {
            Clients.wrongValue(txtAddressOffice, LanguageBundleUtils.getMessage("global.validate.notnull", LanguageBundleUtils.getString("partner.address")));
            return false;
        }

        if (!StringUtils.isValidString(txtAddressTransaction.getValue())) {
            Clients.wrongValue(txtAddressTransaction, LanguageBundleUtils.getMessage("global.validate.notnull", LanguageBundleUtils.getString("partner.addressTransaction")));
            return false;
        }

        if (!StringUtils.isValidString(txtphone.getValue())) {
            Clients.wrongValue(txtphone, LanguageBundleUtils.getMessage("global.validate.notnull", LanguageBundleUtils.getString("partner.phone")));
            return false;
        }

        if (!StringUtils.isValidPhone(txtphone.getValue())) {
            Clients.wrongValue(txtphone, LanguageBundleUtils.getMessage("global.validate.notformat.phone", LanguageBundleUtils.getString("partner.phone")));
            return false;
        }

        if (!StringUtils.isValidString(txtRepresentLaw.getValue())) {
            Clients.wrongValue(txtRepresentLaw, LanguageBundleUtils.getMessage("global.validate.notnull", LanguageBundleUtils.getString("partner.representLaw")));
            return false;
        }

        if (!StringUtils.isValidString(txtPositionLaw.getValue())) {
            Clients.wrongValue(txtPositionLaw, LanguageBundleUtils.getMessage("global.validate.notnull", LanguageBundleUtils.getString("partner.positionLaw")));
            return false;
        }

        if (!StringUtils.isValidString(txtEmailTransaction.getValue())) {
            Clients.wrongValue(txtEmailTransaction, LanguageBundleUtils.getMessage("global.validate.notnull", LanguageBundleUtils.getString("partner.emailTransaction")));
            return false;
        }

        if (!StringUtils.isValidEmail(txtEmailTransaction.getValue())) {
            Clients.wrongValue(txtEmailTransaction, LanguageBundleUtils.getMessage("global.validate.notformat.email", LanguageBundleUtils.getString("partner.emailTransaction")));
            return false;
        }

        if (!StringUtils.isValidString(txtTaxCode.getValue())) {
            Clients.wrongValue(txtTaxCode, LanguageBundleUtils.getMessage("global.validate.notnull", LanguageBundleUtils.getString("partner.taxCode")));
            return false;
        }

        if (!StringUtils.isValidString(txtBusinessLicenceNo.getValue())) {
            Clients.wrongValue(txtBusinessLicenceNo, LanguageBundleUtils.getMessage("global.validate.notnull", LanguageBundleUtils.getString("partner.businessLicencenNo")));
            return false;
        }

        if (!StringUtils.isValidString(dbBusinessLicenceDate.getValue())) {
            Clients.wrongValue(dbBusinessLicenceDate, LanguageBundleUtils.getMessage("global.validate.notnull", LanguageBundleUtils.getString("partner.dbBusinessLicenceDate")));
            return false;
        }

        if (!StringUtils.isValidDate(dbBusinessLicenceDate.getValue())) {
            Clients.wrongValue(dbBusinessLicenceDate, LanguageBundleUtils.getMessage("global.validate.notformat.date.ddMMyyyy", LanguageBundleUtils.getString("partner.dbBusinessLicenceDate")));
            return false;
        }

        if (!StringUtils.isValidString(txtBusinessLicenceCount.getValue())) {
            Clients.wrongValue(txtBusinessLicenceCount, LanguageBundleUtils.getMessage("global.validate.notnull", LanguageBundleUtils.getString("partner.businessLicenceCount")));
            return false;
        }

        if (cboGroupPartner.getSelectedItem() != null && cboGroupPartner.getSelectedItem().getValue() != null && ParamUtils.SELECT_NOTHING_VALUE_STR.equals(cboGroupPartner.getSelectedItem().getValue())) {
            Clients.wrongValue(cboGroupPartner, LanguageBundleUtils.getMessage("global.validate.notchoose", LanguageBundleUtils.getString("partner.groupPartner")));
            return false;
        }

        if (cboProvince.getSelectedItem() != null && cboProvince.getSelectedItem().getValue() != null) {
            String groupPartner = cboGroupPartner.getSelectedItem().getValue().toString();
            if (groupPartner.equals(ParamUtils.PARTNER_GROUP.INTERNAL)) {
                if (cboProvince.getSelectedItem() != null && cboProvince.getSelectedItem().getValue() != null && ParamUtils.SELECT_NOTHING_VALUE_STR.equals(cboProvince.getSelectedItem().getValue())) {
                    Clients.wrongValue(cboProvince, LanguageBundleUtils.getMessage("global.validate.notchoose", LanguageBundleUtils.getString("partner.province")));
                    return false;
                }
            }
        }

        if (cboTypePartner.getSelectedItem().getValue() != null && ParamUtils.SELECT_NOTHING_VALUE_STR.equals(cboTypePartner.getSelectedItem().getValue())) {
            Clients.wrongValue(cboTypePartner, LanguageBundleUtils.getMessage("global.validate.notchoose", LanguageBundleUtils.getString("partner.type")));
            return false;
        }

        Map<Object, Object> mapInputBank = genComponent.getDataFromControls(lstInputTextBankCtrls);
        for (Map.Entry<Object, Object> entry : mapInputBank.entrySet()) {
            Object value = entry.getValue();
            Component component = (Component) entry.getKey();
            if (!StringUtils.isValidString(value)) {
                Clients.wrongValue(component, LanguageBundleUtils.getMessage("global.validate.notnull", LanguageBundleUtils.getString("partner.accountNumber")));
                return false;
            }
        }
        return true;
    }

    private void buildComponentToData() {
        Row tmpRow;
        Component tmpComp;
        Component firstComp = null;
        try {
            //Bank
            List<PartnerBankDTO> lstPartnerBankDTO = partnerBankService.findAll();
            List<BankDTO> lstBankDTO = bankService.findAll();
            for (PartnerBankDTO itemPartnerBankDTO : lstPartnerBankDTO) {
                totalRow++;
                tmpRow = new Row();
                ComponentDTO componentTextDTO = new ComponentDTO();
                componentTextDTO.setAttributeType(ParamUtils.ReportDataType.STRING);
                componentTextDTO.setAttributeLength("50");
                componentTextDTO.setIsRequired(false);
                componentTextDTO.setStandardAttributeId(String.valueOf("txt" + totalRow));
                componentTextDTO.setDefaultValue(itemPartnerBankDTO.getAccountNumber());
                componentTextDTO.setAttributeName(LanguageBundleUtils.getString("partner.accountNumber"));
                tmpComp = genComponent.createComponent(componentTextDTO);
                genComponent.createCellLabel(componentTextDTO.getAttributeName(), componentTextDTO.getIsRequired(), tmpRow);
                if (tmpComp != null) {
                    tmpRow.appendChild(tmpComp);
                    lstInputTextBankCtrls.add(tmpComp);
                    if (firstComp == null) {
                        firstComp = tmpComp;
                    }
                }
                rows.insertBefore(tmpRow, lastRow);

                tmpRow = new Row();
                ComponentDTO componentDTO = new ComponentDTO();
                componentDTO.setAttributeType(ParamUtils.ReportDataType.COMBOBOX);
                componentDTO.setAttributeType(ParamUtils.ReportDataType.COMBOVALUE);
                componentDTO.setIsRequired(false);
                componentDTO.setStandardAttributeId(String.valueOf("cbb" + totalRow));
                componentDTO.setDefaultValue(itemPartnerBankDTO.getBankCode());
                componentDTO.setAttributeName(LanguageBundleUtils.getString("partner.bank"));
                StringBuilder parValue = new StringBuilder();
                for (BankDTO itemBankDTO : lstBankDTO) {
                    parValue.append(itemBankDTO.getCode());
                    parValue.append(";");
                    parValue.append(itemBankDTO.getName());
                    parValue.append(";");
                }
                componentDTO.setInitValue(parValue.toString());
                tmpComp = genComponent.createComponent(componentDTO);
                genComponent.createCellLabel(componentDTO.getAttributeName(), componentDTO.getIsRequired(), tmpRow);
                if (tmpComp != null) {
                    tmpRow.appendChild(tmpComp);
                    lstInputCbbBankCtrls.add(tmpComp);
                    if (firstComp == null) {
                        firstComp = tmpComp;
                    }
                }
                rows.insertBefore(tmpRow, lastRow);
            }
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
    }

    private UserEntity getSavedUser() {
        UserEntity user = new UserEntity();
       String status = "WAITING_AUTHEN";
       String personType = "PARTNER";
        user = (UserEntity) Sessions.getCurrent().getAttribute("user");
         user.setVerifyCodeDate(new Date());
       int randomNum = ThreadLocalRandom.current().nextInt(10000, 999999 + 1);
       
       user.setVerifyCode(Integer.toString(randomNum));
       user.setFullName(txtParnerName.getValue().trim());
       user.setTelephoneNumber(txtphone.getValue().trim());
       user.setStatus(status);
       user.setCurrentAddress(txtAddressOffice.getValue());
       user.setPersonType(personType);
       return user;

    }
}
