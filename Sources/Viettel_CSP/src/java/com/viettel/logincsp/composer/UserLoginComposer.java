/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.logincsp.composer;

import com.viettel.csp.entity.UserEntity;
import com.viettel.csp.util.EncryptionUtils;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.eafs.util.SpringUtil;
import com.viettel.logincsp.AuthenticationService;
import java.security.MessageDigest;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Textbox;

/**
 *
 * @author admin
 */
public class UserLoginComposer extends GenericForwardComposer<Component> {

    private Textbox tvTenTaiKhoan, tvMatKhau;
    private AuthenticationService authenticationService = SpringUtil.getBean("authenticationService", AuthenticationService.class);

    final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger("ContactComposer");

    public void onClick$bntReset() {
        tvTenTaiKhoan.setText("");
        tvMatKhau.setText("");
    }

    public void onClick$bntSubmit() {
        if (null != tvTenTaiKhoan.getValue() || "".equals(tvTenTaiKhoan.getValue()) && null != tvMatKhau.getValue() || "".equals(tvMatKhau.getValue())) {
            MessageDigest md;
            UserEntity user = authenticationService.checkAuthen(tvTenTaiKhoan.getValue(), tvMatKhau.getValue());
            if(user != null){
                Sessions.getCurrent().setAttribute("user", user);
                Clients.showNotification(LanguageBundleUtils.getString("Chưa xác nhận tài khoản !"), "warning", null, "middle_center", 3000);
                Executions.sendRedirect("/loginlogout/confirmuser.zul");
            }else {
                if (authenticationService.login(tvTenTaiKhoan.getValue(), tvMatKhau.getValue())) {
                    Executions.sendRedirect("/index.zul");
                } else {
                    Clients.showNotification(LanguageBundleUtils.getString("Tài khoản không tồn tại hoặc không chính xác!"), "warning", null, "middle_center", 3000);
                }
            }
        }
    }

}
