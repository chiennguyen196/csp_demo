package com.viettel.logincsp.composer;

import com.viettel.csp.entity.UserEntity;
import com.viettel.csp.service.UserService;
import com.viettel.csp.util.EncryptionUtils;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.eafs.util.SpringUtil;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Textbox;
import org.zkoss.zk.ui.util.Clients;
import java.text.ParseException;

/**
 * Created by admin on 6/15/2017.
 */
public class ConfirmUserComposer extends GenericForwardComposer<Component> {
    private Textbox txtcomfirmCode;
    private final UserService userService = SpringUtil.getBean("userService", UserService.class);

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
    }

    public void onClick$btnSend () {
        String code = txtcomfirmCode.getValue();
        UserEntity user = new UserEntity();
        user = (UserEntity) Sessions.getCurrent().getAttribute("user");

        if(user.getVerifyCode().equals(code)){
            user.setStatus("INACTIVE");
            userService.updateUser(user);
            Clients.showNotification(LanguageBundleUtils.getMessage("global.message.update.successful", LanguageBundleUtils.getString("global.edit")), "info", null, "middle_center", 6000);
            Executions.sendRedirect("/login.zul");
        }else{
            Clients.showNotification(LanguageBundleUtils.getString("Mã xác nhận không trùng nhau"), "warning", null, "middle_center", 3000);
        }
    }

}
