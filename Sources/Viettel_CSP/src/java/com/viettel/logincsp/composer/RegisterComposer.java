/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.logincsp.composer;

import com.viettel.csp.entity.UserEntity;
import com.viettel.csp.service.UserService;
import com.viettel.csp.util.CtrlValidator;
import com.viettel.csp.util.EncryptionUtils;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.RandomGenerater;
import com.viettel.eafs.util.SpringUtil;
import java.util.ArrayList;
import java.util.List;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Textbox;

/**
 *
 * @author admin
 */
public class RegisterComposer extends GenericForwardComposer<Component> {

    private final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger("ContactComposer");
    private Textbox txtEmail, txtphone, txtUsername, txtPass, txtcomfirmPass;
    private UserService userService = SpringUtil.getBean("userService", UserService.class);

    public void onClick$btnRegister() {
        UserEntity userEntity = new UserEntity();

        if (validate()) {
            userEntity.setEmail(txtEmail.getValue());
            userEntity.setMobileNumber(txtphone.getValue());
            userEntity.setUserName(txtUsername.getValue());
            String salt = String.valueOf(RandomGenerater.generateChar(20));
            userEntity.setRandomKey(salt);
            userEntity.setPassword(EncryptionUtils.sha256WithSalt(txtPass.getValue(), salt));

            Sessions.getCurrent().setAttribute("user", userEntity);
            Executions.sendRedirect("/loginlogout/updateRegisterUser.zul");
        }
    }

    public boolean validate() {
        List<String> errors = new ArrayList<>();
        boolean anyError = false;

        anyError = anyError || validateEmail(errors);
        anyError = anyError || validatePhone(errors);
        anyError = anyError || validatUsername(errors);
        anyError = anyError || validatePassword(errors);

        if (errors.size() > 0) {
            Clients.showNotification(errors.get(0), "info", null, "middle_center", 3000);
        }
        return !anyError;
    }

    public boolean validateEmail(List<String> errors) {
        boolean anyError = false;
        //check format
        anyError = anyError || CtrlValidator.checkValidEmail(txtEmail, true, txtEmail.getName());
        //check existed
        if (userService.existedEmail(txtEmail.getValue())) {
            errors.add(LanguageBundleUtils.getMessage("global.message.error.emailExisted"));
            anyError = true;
        }

        return !anyError;
    }

    public boolean validatePhone(List<String> errors) {
        boolean anyError = false;
        //check format
        anyError = anyError || CtrlValidator.checkValidPhone(txtphone, true, txtphone.getName());
        //check existed
        if (userService.existedMobileNumber(txtphone.getValue())) {
            errors.add(LanguageBundleUtils.getMessage("global.message.error.phoneExisted"));
            anyError = true;
        }

        return !anyError;
    }

    public boolean validatePassword(List<String> errors) {
        return !CtrlValidator.checkConfirmPassword(txtPass, txtcomfirmPass, true, "");
    }

    private boolean validatUsername(List<String> errors) {
        boolean anyError = false;
        //check format
        anyError = anyError || CtrlValidator.checkValidUsername(txtUsername, true, txtUsername.getName());
        //check existed
        if (userService.existedUsername(txtUsername.getValue())) {
            errors.add(LanguageBundleUtils.getMessage("global.message.error.userExisted"));
            anyError = true;
        }

        return !anyError;
    }

}
