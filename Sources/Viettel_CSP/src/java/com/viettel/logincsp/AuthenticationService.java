/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.logincsp;

import com.viettel.csp.entity.UserEntity;

/**
 *
 * @author admin
 */
public interface AuthenticationService {

    public UserCredential getUserCredential();

    public boolean login(String userName, String password);

    public void logout();

    public UserEntity checkAuthen(String userName, String password);
}
