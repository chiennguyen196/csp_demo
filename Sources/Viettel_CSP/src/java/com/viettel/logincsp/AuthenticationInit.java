/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.logincsp;

import com.viettel.eafs.util.SpringUtil;

import java.util.Map;

import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.util.Initiator;

/**
 * @author admin
 */
public class AuthenticationInit implements Initiator {

    AuthenticationService authService = SpringUtil.getBean("authenticationService", AuthenticationService.class);

    @Override
    public void doInit(Page page, Map<String, Object> map) throws Exception {
        UserCredential cre = authService.getUserCredential();
        String url = Executions.getCurrent().getDesktop().getRequestPath();
        if ((cre == null || cre.isAnonymous()) && !url.equals("/login.zul")) {
            Executions.sendRedirect("/login.zul");
            return;
        } else {
            if (url.equals("/login.zul") && !cre.isAnonymous()) {
                Executions.sendRedirect("/index.zul");
            }
        }
    }

}
