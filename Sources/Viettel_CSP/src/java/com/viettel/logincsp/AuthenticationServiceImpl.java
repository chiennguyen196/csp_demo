/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.logincsp;

import com.viettel.csp.DTO.ContactDTO;
import com.viettel.csp.DTO.PartnerDTO;
import com.viettel.csp.DTO.UserDTO;
import com.viettel.csp.entity.UserEntity;
import com.viettel.csp.service.ContactService;
import com.viettel.csp.service.PartnerService;
import com.viettel.csp.service.UserService;
import com.viettel.csp.util.EncryptionUtils;
import com.viettel.csp.util.ParamUtils;
import java.io.Serializable;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;

/**
 *
 * @author admin
 */
public class AuthenticationServiceImpl implements AuthenticationService, Serializable {

    org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AuthenticationServiceImpl.class);

    @Autowired
    private UserService userService;

    @Autowired
    private ContactService contactService;

    @Autowired
    private PartnerService partnerService;

    public ContactService getContactService() {
        return contactService;
    }

    public void setContactService(ContactService contactService) {
        this.contactService = contactService;
    }

    public UserService getUserService() {
        return userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public PartnerService getPartnerService() {
        return partnerService;
    }

    public void setPartnerService(PartnerService partnerService) {
        this.partnerService = partnerService;
    }

    @Override
    public UserCredential getUserCredential() {
        Session sess = Sessions.getCurrent();
        UserCredential cre = (UserCredential) sess.getAttribute(ParamUtils.USER_TOKEN);
        if (cre == null) {
            cre = new UserCredential();
            sess.setAttribute(ParamUtils.USER_TOKEN, cre);
        }
        return cre;
    }

    @Override
    public boolean login(String userName, String password) {
        try {
            UserDTO udto = new UserDTO();
            udto.setUserName(userName);
            udto.setPassword(password);
            List<UserDTO> listUser = userService.checkLogin(udto);

            if (listUser == null || listUser.size() != 1) {
                return false;
            } else {
                UserDTO loginUser = listUser.get(0);
                if (loginUser.getPassword().equals(EncryptionUtils.sha256WithSalt(password, loginUser.getRandomKey()))) {

                    UserDTO login = listUser.get(0);
                    Long partnerId = null;
                    Session sess = Sessions.getCurrent();
                    if (ParamUtils.USER_TYPE.CONTACT.equalsIgnoreCase(login.getPersonType())) {
                        ContactDTO contactDTO = contactService.getObjectDTOById(login.getPersonInfoId());
                        partnerId = contactDTO.getPartnerId();
                    } else if (ParamUtils.USER_TYPE.PARTNER.equalsIgnoreCase(login.getPersonType())) {
                        PartnerDTO partnerDTO = partnerService.getObjectDTOById(login.getPersonInfoId());
                        partnerId = partnerDTO.getId();
                    }

                    UserCredential credential = new UserCredential(login.getUserName(), login.getId(), login.getFullName(), login.getEmail(), login.getTelephoneNumber(), login.getPersonInfoId(), login.getPersonType(), login.getStatus(), partnerId);
                    sess.setAttribute(ParamUtils.USER_TOKEN, credential);
                    return true;
                } else {
                    return false;
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return false;
    }

    @Override
    public void logout() {
        Session session = Sessions.getCurrent();
        session.removeAttribute(ParamUtils.USER_TOKEN);
    }

    @Override
    public UserEntity checkAuthen(String userName, String password) {
        UserDTO udto = new UserDTO();
        udto.setUserName(userName);
        udto.setPassword(password);
        List<UserDTO> listUser = userService.checkLogin(udto);

        if (listUser == null || listUser.size() != 1) {
            return null;
        } else {
            UserDTO loginUser = listUser.get(0);
            if (loginUser.getPassword().equals(EncryptionUtils.sha256WithSalt(password, loginUser.getRandomKey()))) {

                UserDTO login = listUser.get(0);
                if(login.getStatus().equals("WAITING_AUTHEN")) {
                    UserEntity user = getUserService().getUserByID(login.getId());
                    return user;
                }
            }
        }
        return null;
    }
}
