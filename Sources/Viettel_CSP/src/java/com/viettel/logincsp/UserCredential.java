/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.logincsp;

/**
 *
 * @author admin
 */
public class UserCredential {

    private String userName;
    private Long userId;
    private String fullName;
    private String email;
    private String telephoneNumber;
    private Long personInfoId;
    private String personType;
    private String status;
    private String ip;
    private Long partnerId;

    public UserCredential(String userName, Long userId, String fullName, String email, String telephoneNumber, Long personInfoId, String personType, String status, Long partnerId) {
        this.userName = userName;
        this.userId = userId;
        this.fullName = fullName;
        this.email = email;
        this.telephoneNumber = telephoneNumber;
        this.personInfoId = personInfoId;
        this.personType = personType;
        this.status = status;
        this.partnerId = partnerId;
    }

    public UserCredential() {

    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    boolean isAnonymous() {
        return userName == null;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public Long getPersonInfoId() {
        return personInfoId;
    }

    public void setPersonInfoId(Long personInfoId) {
        this.personInfoId = personInfoId;
    }

    public String getPersonType() {
        return personType;
    }

    public void setPersonType(String personType) {
        this.personType = personType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Long getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(Long partnerId) {
        this.partnerId = partnerId;
    }

}
