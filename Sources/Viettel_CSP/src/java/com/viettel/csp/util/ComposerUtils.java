/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.util;

import java.util.ArrayList;
import java.util.List;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.*;
import org.zkoss.zul.impl.InputElement;

/**
 *
 * @author tuanna67
 */
public class ComposerUtils {
    public static void clearAllCtrls(Component root){
        List<Component> lstComp = root.getChildren();
        for (Component comp : lstComp) {
            if (comp instanceof Combobox && ((Combobox) comp).getModel() != null) {
                ListModelList modelList = (ListModelList) ((Combobox) comp).getModel();
                if (modelList.size() > 0) {
                    modelList.addToSelection(modelList.getElementAt(0));
                }
            } else if (comp instanceof Textbox || comp instanceof Longbox) {
                ((InputElement) comp).setRawValue(null);
            } else if (comp instanceof Checkbox) {
                ((Checkbox) comp).setChecked(false);
            } else {
                clearAllCtrls(comp);
            }
        }
    }
    
    public static void setDisabled(Component root, boolean isDisable){
        List<Component> lstComp = root.getChildren();
        for (Component comp : lstComp) {
            if (comp instanceof InputElement) {
                ((InputElement) comp).setDisabled(isDisable);
            } else if (comp instanceof Checkbox) {
                ((Checkbox) comp).setDisabled(isDisable);
            } else {
                setDisabled(comp, isDisable);
            }
        }
    }
    
    public static void setReadonly(Component root, boolean isReadonly){
        List<Component> lstComp = root.getChildren();
        for (Component comp : lstComp) {
            if (comp instanceof Combobox) {
                ((Combobox) comp).setDisabled(isReadonly);
            } else if (comp instanceof Button) {
                ((Button) comp).setDisabled(isReadonly);
            } else if (comp instanceof InputElement) {
                ((InputElement) comp).setReadonly(isReadonly);
            } else if (comp instanceof Checkbox) {
                ((Checkbox) comp).setDisabled(isReadonly);
            } else {
                setReadonly(comp, isReadonly);
            }
        }
    }
    
    public static String getValue(Component comp) {
        String result = null;
        if (comp instanceof Doublebox) {
            result = ((Doublebox) comp).getText();
        } else if (comp instanceof Longbox) {
            result = ((Longbox) comp).getText();
        } else if (comp instanceof Textbox) {
            result = ((Textbox) comp).getText();
        }
        return result;
    }
    
    public static void resetAllCtrl(Component inputCtrl) {
        List<Component> lstComp = inputCtrl.getChildren();
        for (Component comp : lstComp) {
            if (comp instanceof Combobox && ((Combobox) comp).getModel() != null) {
                ListModelList modelList = (ListModelList) ((Combobox) comp).getModel();
                if (modelList.size() > 0) {
                    modelList.addToSelection(modelList.getElementAt(0));
                }
            }else if(comp instanceof Combobox){
                ((Combobox)comp).setSelectedIndex(0);
            } else if (comp instanceof Textbox || comp instanceof Longbox || comp instanceof Doublebox) {
                ((InputElement) comp).setRawValue(null);
            } else if (comp instanceof Checkbox) {
                ((Checkbox) comp).setChecked(false);
            } else {
                resetAllCtrl(comp);
            }
        }
    }
    
    public static void setFixComboValue(Combobox combo, String value){
        for(Comboitem item : combo.getItems()){
            if(value.equals(item.getValue().toString())){
                combo.setSelectedItem(item);
                break;
            }
        }
    }
}