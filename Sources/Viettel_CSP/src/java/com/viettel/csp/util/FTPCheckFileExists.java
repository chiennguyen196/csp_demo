/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.impl.InputElement;

/**
 *
 * @author tuannm33
 */
public class FTPCheckFileExists {

    private FTPClient ftpClient;
    private int returnCode;

    public boolean checkDirectoryExists(String dirPath) throws IOException {
        ftpClient.changeWorkingDirectory(dirPath);
        returnCode = ftpClient.getReplyCode();
        if (returnCode == 550) {
            return false;
        }
        return true;
    }

    public boolean checkFileExists(String filePath) throws IOException {
        InputStream inputStream = ftpClient.retrieveFileStream(filePath);
        returnCode = ftpClient.getReplyCode();
        if (inputStream == null || returnCode == 550) {
            return false;
        }
        return true;
    }

    public boolean connect(String hostname, int port, String username, String password) throws SocketException, IOException {
        ftpClient = new FTPClient();
        ftpClient.connect(hostname, port);
        returnCode = ftpClient.getReplyCode();
        if (!FTPReply.isPositiveCompletion(returnCode)) {
            throw new IOException("Could not connect");
        }
        boolean loggedIn = ftpClient.login(username, password);
        if (!loggedIn) {
            throw new IOException("Could not login");
        }
        return loggedIn;
    }

    public void logout() throws IOException {
        if (ftpClient != null && ftpClient.isConnected()) {
            ftpClient.logout();
            ftpClient.disconnect();
        }
    }

    //TuanNM33: check exists ftp path
    public boolean checkConnectFTPHost(
            InputElement elementHost,
            int port,
            String username,
            String pass,
            boolean isFocused,
            String elementNameHost,
            String elementNameUser) throws SocketException, IOException {
        boolean loggedIn;
        ftpClient = new FTPClient();
        ftpClient.setConnectTimeout(7000);
        ftpClient.connect(elementHost.getText().trim(), port);
        returnCode = ftpClient.getReplyCode();
        if (!FTPReply.isPositiveCompletion(returnCode)) {
            //showNotification(elementHost, isFocused, "ERR.NOT_CONNECT", elementNameHost);
            loggedIn = false;
            Clients.showNotification(LanguageBundleUtils.getMessage("ERR.NOT_CONNECT", LanguageBundleUtils.getString(elementNameHost)), "warning", null, "middle_center", 3000);
        } else {
            loggedIn = ftpClient.login(username, pass);
//            if (!loggedIn) {
//                Clients.showNotification(LanguageBundleUtils.getMessage("ERR.INVALID", LanguageBundleUtils.getString(elementNameUser)), "warning", null, "middle_center", 3000);
//            }
        }
        return loggedIn;
    }

    public boolean checkExistsFtpPath(
            InputElement elementUrl,
            boolean isFocused,
            String elementNamePath) throws SocketException, IOException {
        boolean b;
        ftpClient.changeWorkingDirectory(elementUrl.getText().trim());
        returnCode = ftpClient.getReplyCode();
        if (returnCode == 550) {
            b = false;
            showNotification(elementUrl, isFocused, "ERR.NOT_EXISTS", elementNamePath);
        } else {
            b = true;
        }
        return b;
    }

    public static void showNotification(InputElement element, boolean isFocused, String errCode, String... msgParams) {
        if (element != null) {
            Clients.wrongValue(element, LanguageBundleUtils.getString(errCode, msgParams));
            if (!isFocused) {
                element.setFocus(true);
            }
        } else {
            Clients.showNotification(LanguageBundleUtils.getString(errCode, msgParams));
        }
    }
    
}
