//package com.viettel.csp.util;
//
//import com.viettel.eafs.util.SpringUtil;
//import com.viettel.plc.DTO.catalogue.AppParamsDTO;
//import com.viettel.plc.DTO.catalogue.StandardAttributeDTO;
//import com.viettel.plc.service.catalogue.AppParamsService;
//import org.zkoss.zk.ui.Component;
//import org.zkoss.zk.ui.Executions;
//import org.zkoss.zk.ui.Sessions;
//import org.zkoss.zk.ui.event.Event;
//import org.zkoss.zul.*;
//
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * Created by tuannm33 on 10/17/2016.
// */
//
//public class PolicyRenderParams implements ListitemRenderer<Object> {
//
//    private GenCodeComponent genCodeComponent = new GenCodeComponent();
//    private GenComponent genComponent = new GenComponent();
//    private List<Component> lstInputCtrls = new ArrayList<>();
//    private String action = "";
//
//    private AppParamsService appParamsService = SpringUtil.getBean("AppParamsService", AppParamsService.class);
//
//    @Override
//    public void render(Listitem lstItem, Object t, int i) throws Exception {
//        Object[] item = (Object[]) t;
//        Object[] mapItem = (Object[]) item[0];
//        List<StandardAttributeDTO> lstVisibleAttr = (List<StandardAttributeDTO>) item[1];
//        lstInputCtrls = (List<Component>) item[2];
//        action = item[3].toString();
//        //STT
//        lstItem.setValue(mapItem);
//        lstItem.appendChild(new Listcell((i + 1) + ""));
//        //Gen cac nut Sua, Xoa, Xem
//        Listcell lcActions = new Listcell();
//        Div div = new Div();
//        div.setAlign("center");
//        Hlayout hlayout = new Hlayout();
//        hlayout.setSpacing("1px");
//        Button btnEdit = new Button();
//        btnEdit.setImage("/images/pencil-small.png");
//        btnEdit.setMold("trendy");
//        btnEdit.setAutodisable("self");
//        btnEdit.setSclass("btn-nopadding");
//        btnEdit.setTooltiptext(LanguageBundleUtils.getString("global.edit"));
//        btnEdit.addForward("onClick", Executions.getCurrent().getDesktop().getFirstPage().getFellow("bodyLayout").getFellow("wdPolicy"), "onEditPolicy", lstItem);
//        hlayout.appendChild(btnEdit);
//        //
//        if (!Constants.POLICY.APPROVAL_WAIT.equals(action)) {
//            Button btnDel = new Button();
//            btnDel.setImage("/images/icon_delete.png");
//            btnDel.setMold("trendy");
//            btnDel.setAutodisable("self");
//            btnDel.setSclass("btn-nopadding");
//            btnDel.setTooltiptext(LanguageBundleUtils.getString("global.delete"));
//            btnDel.addForward("onClick", Executions.getCurrent().getDesktop().getFirstPage().getFellow("bodyLayout").getFellow("wdPolicy"), "onDelPolicy", lstItem);
//            hlayout.appendChild(btnDel);
//        }
//        //
//        Button btnView = new Button();
//        btnView.setImage("/images/view.png");
//        btnView.setMold("trendy");
//        btnView.setAutodisable("self");
//        btnView.setSclass("btn-nopadding");
//        btnView.setTooltiptext(LanguageBundleUtils.getString("global.view"));
//        btnView.addForward("onClick", Executions.getCurrent().getDesktop().getFirstPage().getFellow("bodyLayout").getFellow("wdPolicy"), "onViewPolicy", lstItem);
//        hlayout.appendChild(btnView);
//        //
//        div.appendChild(hlayout);
//        lcActions.appendChild(div);
//        lstItem.appendChild(lcActions);
//        //Gen column fix
//        Listcell lcPolicyId = new Listcell();
//        lcPolicyId.setVisible(false);
//        Label lblPolicyId = new Label();
//        lblPolicyId.setValue(mapItem[0] != null ? mapItem[0].toString() : "");
//        lblPolicyId.setVisible(false);
//        lcPolicyId.appendChild(lblPolicyId);
//        lstItem.appendChild(lcPolicyId);
//
//        Listcell lcPolicyCode = new Listcell();
//        lcPolicyCode.setStyle("text-align:left");
//        Label lblPolicyCode = new Label();
//        lblPolicyCode.setValue(mapItem[1] != null ? mapItem[1].toString() : "");
//        lcPolicyCode.appendChild(lblPolicyCode);
//        lstItem.appendChild(lcPolicyCode);
//
//        Listcell lcPolicyName = new Listcell();
//        lcPolicyName.setStyle("text-align:left");
//        Label lblPolicyName = new Label();
//        lblPolicyName.setValue(mapItem[2] != null ? mapItem[2].toString() : "");
//        lcPolicyName.appendChild(lblPolicyName);
//        lstItem.appendChild(lcPolicyName);
//
//        Listcell lcDocType = new Listcell();
//        lcDocType.setStyle("text-align:left");
//        Label lblDocType = new Label();
//        lblDocType.setValue(getDocTypeName(mapItem[8] != null ? mapItem[8].toString() : ""));   //Loai van ban
//        lcDocType.appendChild(lblDocType);
//        lstItem.appendChild(lcDocType);
//
//        //Gen column dynamic
//        if (lstVisibleAttr != null && !lstVisibleAttr.isEmpty()) {
//            int start = 9;
//            for (int j = 0; j < lstVisibleAttr.size(); j++) {
//                StandardAttributeDTO sa = lstVisibleAttr.get(j);
//                Listcell lcParams = new Listcell();
//                String align;
//                if (ParamUtils.ReportDataType.LONG.equalsIgnoreCase(sa.getAttributeType()) ||
//                        ParamUtils.ReportDataType.DOUBLE.equalsIgnoreCase(sa.getAttributeType())) {
//                    align = "right";
//                } else if (ParamUtils.ReportDataType.DATE.equalsIgnoreCase(sa.getAttributeType())) {
//                    align = "center";
//                } else {
//                    align = "left";
//                }
//                lcParams.setStyle("text-align:" + align);
//                if (ParamUtils.ReportDataType.DOUBLE.equalsIgnoreCase(sa.getAttributeType())) {
//                    Label lblDouble = new Label();
//                    lblDouble.setValue(mapItem[start] != null ? mapItem[start].toString().replace(".", ",") : "");
//                    lcParams.appendChild(lblDouble);
//                } else if (ParamUtils.ReportDataType.CHECKBOX.equalsIgnoreCase(sa.getAttributeType())) {
//                    Checkbox ckb = new Checkbox();
//                    if (mapItem[start] != null) {
//                        ckb.setChecked("1".equals(mapItem[start].toString()) ? true : false);
//                    }
//                    ckb.setDisabled(true);
//                    lcParams.appendChild(ckb);
//                } else if (ParamUtils.ReportDataType.COMBOBOX.equals(sa.getAttributeType()) || ParamUtils.ReportDataType.COMBOVALUE.equals(sa.getAttributeType())) {
//                    if (mapItem[start] != null) {
//                        List<KeyValueBean> lstKeyValue = genComponent.getLstListitem(sa.getAttributeType(), sa.getInitValue());
//                        for (KeyValueBean keyValue : lstKeyValue) {
//                            if (keyValue.getKey() != null && keyValue.getKey().toString().equals(mapItem[start].toString())) {
//                                Label lblCombo = new Label();
//                                lblCombo.setValue(keyValue.getValue());
//                                lcParams.appendChild(lblCombo);
//                            }
//                        }
//                    }
//                } else if (ParamUtils.ReportDataType.ATTACHFILE.equalsIgnoreCase(sa.getAttributeType())) {
//                    if (mapItem[start] != null) {
//                        String[] strFiles = mapItem[start].toString().split("\r\n");
//                        for (int k = 0; k < strFiles.length; k++) {
//                            Label lblFile = new Label();
//                            lblFile.setStyle("color:blue; cursor:pointer;");
//                            lblFile.setValue(strFiles[k]);
//                            lblFile.setTooltiptext(strFiles[k]);
//                            lblFile.setSclass("myLabelOnMouseOver");
//                            lblFile.addEventListener("onClick", new org.zkoss.zk.ui.event.EventListener() {
//                                public void onEvent(Event evt) {
//                                    genCodeComponent.onViewFile(evt);
//                                }
//                            });
//                            if (k < strFiles.length - 1) {
//                                lcParams.appendChild(lblFile);
//                                Separator line = new Separator();
//                                line.setBar(true);
//                                lcParams.appendChild(line);
//                            } else {
//                                lcParams.appendChild(lblFile);
//                            }
//                        }
//                    } else {
//                        Label lblFile = new Label();
//                        lblFile.setStyle("color:blue; cursor:pointer;");
//                        lblFile.setValue("");
//                        lblFile.setSclass("myLabelOnMouseOver");
//                        lcParams.appendChild(lblFile);
//                    }
//                } else if (ParamUtils.ReportDataType.TREEBOX.equalsIgnoreCase(sa.getAttributeType())
//                        || ParamUtils.ReportDataType.TREEVALUE.equalsIgnoreCase(sa.getAttributeType())) {
//                    Label lblTree = new Label();
//                    lblTree.setValue(getNameTree(mapItem[start] != null ? mapItem[start].toString() : "", sa.getAttributeCode()));
//                    lcParams.appendChild(lblTree);
//                } else if (ParamUtils.ReportDataType.LISTBOX.equalsIgnoreCase(sa.getAttributeType())
//                        || ParamUtils.ReportDataType.LISTVALUE.equalsIgnoreCase(sa.getAttributeType())) {
//                    Label lblListbox = new Label();
//                    lblListbox.setValue(getNameListbox(mapItem[start] != null ? mapItem[start].toString() : "", sa.getAttributeCode()));
//                    lcParams.appendChild(lblListbox);
//                } else {
//                    Label lblPar = new Label();
//                    lblPar.setValue(mapItem[start] != null ? mapItem[start].toString() : "");
//                    lcParams.appendChild(lblPar);
//                }
//                lstItem.appendChild(lcParams);
//                start++;
//            }
//        }
//    }
//
//    //get name for tree
//    public String getNameTree(String value, String idComp) {
//        String result = "";
//        for (Component component : lstInputCtrls) {
//            if (component.getId().equals(idComp) && component.getChildren().get(0).getChildren().get(0).getNextSibling() instanceof Tree) {
//                List<Object[]> lstObjectTree = (List<Object[]>) Sessions.getCurrent().getAttribute(idComp);
//                int p = Integer.parseInt(component.getAttribute("POSITION_VIEW").toString());
//                String[] arrVal = value.trim().split("\\|");
//                if (lstObjectTree != null && !lstObjectTree.isEmpty()) {
//                    int i = 0;
//                    for (String val : arrVal) {
//                        for (Object[] itemObj : lstObjectTree) {
//                            if (val.trim().equals(itemObj[0].toString().trim())) {
//                                if (i < arrVal.length - 1) {
//                                    result += itemObj[5 + p] + "|";
//                                } else {
//                                    result += itemObj[5 + p];
//                                }
//                                break;
//                            }
//                        }
//                        i++;
//                    }
//                }
//                break;
//            }
//        }
//        return result;
//    }
//
//    //get name for list
//    public String getNameListbox(String value, String idComp) {
//        String result = "";
//        for (Component component : lstInputCtrls) {
//            if (component.getId().equals(idComp) && component.getChildren().get(0).getChildren().get(0).getNextSibling() instanceof Listbox) {
//                List<Object[]> lstObjectListbox = (List<Object[]>) Sessions.getCurrent().getAttribute(idComp);
//                int p = Integer.parseInt(component.getAttribute("POSITION_VIEW").toString());
//                String[] arrVal = value.trim().split("\\|");
//                if (lstObjectListbox != null && !lstObjectListbox.isEmpty()) {
//                    int i = 0;
//                    for (String val : arrVal) {
//                        for (Object[] itemObj : lstObjectListbox) {
//                            if (val.trim().equals(itemObj[0].toString().trim())) {
//                                if (i < arrVal.length - 1) {
//                                    result += itemObj[p - 1] + "|";
//                                } else {
//                                    result += itemObj[p - 1];
//                                }
//                                break;
//                            }
//                        }
//                        i++;
//                    }
//                }
//                break;
//            }
//        }
//        return result;
//    }
//
//    //get name for doc type
//    public String getDocTypeName(String value) {
//        String docTypeName = "";
//        List<KeyValueBean> lstKeyValueDocType = (List<KeyValueBean>) Sessions.getCurrent().getAttribute("SESSION_DOC_TYPE");
//        for (KeyValueBean kv : lstKeyValueDocType) {
//            if (StringUtils.validString(kv.getValue()) && value.equals(kv.getKey())) {  //value trong app_params
//                docTypeName = kv.getValue();  //name trong app_params
//                break;
//            }
//        }
//        return docTypeName;
//    }
//
//}
