/*
 * Copyright (C) 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.viettel.csp.util;

//import com.viettel.plc.DTO.ActionDetailDTO;
import java.lang.annotation.Annotation;
import java.lang.reflect.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.persistence.Column;

/**
 *
 * @author kdvt_binhnt22@viettel.com.vn
 * @version 1.0
 * @since May 2012
 */
public class ReflectUtils {

    public static final String ANNO_COLUMN_NAME = "name=";
    public static final String ANNO_TEMPORAL_TYPE = "value=";

    public static String getAnnotationProperty(Method method, String property) {
        String result = "";
        Annotation annos = method.getAnnotation(Column.class);
        if (annos != null) {
            String annoColumn = annos.toString();
            result = annoColumn.substring(
                    annoColumn.indexOf(property) + property.length(),
                    annoColumn.indexOf(",", annoColumn.indexOf(property)));
        }
        return result;
    }

    public static String getColumnBeanName(Method method) {
        String methodName = method.getName();
        return methodName.substring(3, 4).toLowerCase() + methodName.substring(4);
    }

    public static String getColumnName(Method method) {
        return getAnnotationProperty(method, ANNO_COLUMN_NAME);
    }

    public static String getTemporalType(Method method) {
        return getAnnotationProperty(method, ANNO_TEMPORAL_TYPE);
    }

    public static boolean isGetter(Method method) {
        if (!method.getName().startsWith("get")) {
            return false;
        }
        if (method.getParameterTypes().length != 0) {
            return false;
        }
        if (method.getReturnType().equals(Void.TYPE)) {
            return false;
        }
        return true;
    }

    public static boolean isSetter(Method method) {
        if (!method.getName().startsWith("set")) {
            return false;
        }
        if (method.getParameterTypes().length != 1) {
            return false;
        }
        if (!method.getReturnType().equals(Void.TYPE)) {
            return false;
        }
        return true;
    }
    
    public static void buildObject(Map<String,String> mapValues, Object obj) throws Exception{
        if(mapValues == null || mapValues.isEmpty() || obj == null){
            return;
        }
        Method methods[] = obj.getClass().getDeclaredMethods();
        Column column;
        for(Method m : methods){
            if (m.getName().startsWith("set")){
                column = m.getAnnotation(Column.class);
                if(column != null && mapValues.containsKey(column.name())){
                    m.invoke(obj, mapValues.get(column.name()));
                }
            }
        }
    }
    
//    public static List<ActionDetailDTO> getChangedInfo(String tbl, Object oldObj, Object newObj) throws Exception{
//        List<ActionDetailDTO> result = new ArrayList<>();
//        ActionDetailDTO temp;
//        Method methods[] = oldObj.getClass().getDeclaredMethods();
//        Column column;
//        Object oldObjVal, newObjVal;
//        String oldVal, newVal;
//        for(Method m : methods){
//            if (m.getName().startsWith("get")){
//                column = m.getAnnotation(Column.class);
//                if(column != null){
//                    oldObjVal = m.invoke(oldObj);
//                    newObjVal = m.invoke(newObj);
//                    oldVal = oldObjVal == null ? null : oldObjVal.toString();
//                    newVal = newObjVal == null ? null : newObjVal.toString();
//                    if(!StringUtils.isEqual(oldVal, newVal)){
//                        temp = new ActionDetailDTO();
//                        temp.setTableName(tbl);
//                        temp.setColumnName(column.name());
//                        temp.setOldValue(oldVal);
//                        temp.setNewValue(newVal);
//                        result.add(temp);
//                    }
//                }
//            }
//        }
//        return result;
//    }
}