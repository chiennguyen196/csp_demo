/*
 * Copyright (C) 2011 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.viettel.csp.util;

/**
 * @author kdvt_binhnt22@viettel.com.vn
 * @version 1.0
 * @since since_text
 */
public class ParamUtils {

    public static final String STRING_EMPTY = "";
    public static final String TYPE_NUMBER = "LONG,INTEGER,SHORT,BYTE,INT,DOUBLE,FLOAT";
    public static final String TYPE_STRING = "STRING";
    public static final String TYPE_DATE = "DATE";
    public static final String SUCCESS = "SUCCESS";
    public static final String FAIL = "FAIL";
    public static final String NOT_EXIST = "NOT_EXIST";
    public static final String GENDER_MALE_STR = "MALE";
    public static final String GENDER_FEMALE_STR = "FEMALE";
    public static final String ACTIVE_STR = "ACTIVE";
    public static final String NOT_ACTIVE_STR = "NOT ACTIVE";
    public static final String INACTIVE_STR = "INACTIVE";
    public static final String LOCK_STR = "LOCK";
    public static final Long DELETE = 1L;
    public static final Long NOT_DELETE = 0L;
    public static final Long ACTIVE = 1L;
    public static final Long INACTIVE = 0L;
    public static final Long LNG_ACTIVE = 1l;
    public static final Long LNG_INACTIVE = 0l;
    public static final Long DEFAULT_VALUE = -99L;
    public static final Long DEFAULT_VALUE_LONG = -99L;
    public static final int SELECT_NOTHING_INDEX = 0;
    public static final Long SELECT_NOTHING_VALUE = -99L;
    public static final String SELECT_MORE_OTHER_VALUE_STR = "-9999";
    public static final String SELECT_NOTHING_VALUE_STR = "-99";
    public static final String DEFAULT_VALUE_STR = "-99";
    public static final String USER_TOKEN = "userToken";
    public static final String VSA_USER_TOKEN = "vsaUserToken";
    public static final String SESSION_TIMEOUT = "sessionTimeout";
    public static final String ERROR_PAGE = "error";
    public static final String BASE_ID = "baseId";
    public static final String PARAMETER_LOCALE = "request_locale=";
    public static final String REQUEST_HEADER_REFERER = "referer";
    public static final String LOCALE_SEPARATOR = "_";
    public static final String ORDER_ASC = "ASC";
    public static final String ORDER_DESC = "DESC";
    public static final Long INTERVAL_CACHE_REFESH = 15L * 60 * 1000;
    public static final int DEFAULT_PAGE_SIZE = 10;
    public static final String ACTION_COMMAND = "COMMAND";
    public static final String ZUL_PATH = "/controls/";
    public static final String COMMON_AVAILABLE_STATUS_VALUE = "1";
    //------------------OPERATOR FOR GENERATE SQL
    public static final String OP_EQUAL = " = ";
    public static final String OP_NOT_EQUAL = " != ";
    public static final String OP_GREATER = " > ";
    public static final String OP_GREATER_EQUAL = " >= ";
    public static final String OP_LESS = " < ";
    public static final String OP_LESS_EQUAL = " <= ";
    public static final String OP_LIKE = " like ";
    public static final String OP_IN = " in ";
    public static final String OP_BETWEEN = " between ";
    public static final String OP_IS_NULL = " is null ";
    public static final String OP_IS_NOT_NULL = " is not null ";
    public static final String LOGIC_OR = " or ";
    public static final String LOGIC_AND = " and ";
    public static final String ddMMyyyy = "dd/MM/yyyy";
    public static final String MMyyyy = "MM/yyyy";
    public static final String ddMMMyy = "dd-MMM-yy";
    public static final String dateTimeFormat = "dd/MM/yyyy HH:mm:ss";
    public static final int START_END_DATE_CONFIG = 15;
    public static final int DEFAULT_DAY_VALUE = 15;
    //    //------------------DIV MESSAGE
    public static final String SEPARATOR = "|";
    public static final String LOG_DETAIL_SEPERATOR = "|";
    public static final String LOG_SEPERATOR = "||";
    public static final String SPLITTER = ",";
    public static final int MAX_PIC_SIZE = 50 * 1024;
    //    // -----------------SEARCH FORM ---------------------
    public static final String RECORD_ID = "id";
    public static final String SEARCH_STARTVAL = "startval";
    public static final String SEARCH_SORT = "sort";
    public static final String SORT_ASC = "asc";
    public static final String SORT_DESC = "desc";
    public static final String SEARCH_COUNT = "count";
    public static final String SEARCH_PARAM_DELIM = "-";
    public static final String SEARCH_NUMROW = "10";
    public static final String PARAM_GROUP = "paramGroup";
    public static final String PARAM_NAME = "paramName";
    public static final String PARAM_BUSINESS_VAL = "businessValue";
    public static final String COMPOSER = "COMPOSER";
    public static final String ACTION = "ACTION";
    public static final String ACTION_CREATE = "CREATE";
    public static final String ACTION_VIEW = "VIEW";
    public static final String ACTION_UPDATE = "UPDATE";
    public static final String ACTION_PROFILE = "PROFILE";
    public static final String ACTION_UPDATE_FILE = "UPDATE_FILE";
    public static final String ACTION_UPDATE_CONTRACT = "UPDATE_CONTRACT";
    public static final String ACTION_SELECT_ASSSIGN = "ACTION_SELECT_ASSSIGN";
    public static final String ACTION_DELETE = "DELETE";
    public static final String ACTION_SEARCH = "SEARCH";
    public static final String ACTION_PAGING = "PAGING";
    public static final String ACTION_PARTNER_LOCK = "ACTION_PARTNER_LOCK";
    public static final String ACTION_PARTNER_OPEN = "ACTION_PARTNER_OPEN";
    public static final String ACTION_PARTNER_HANDLE_VIOLATION = "ACTION_PARTNER_HANDLE_VIOLATION";
    public static final String ACTION_ACTIVATE = "ACTIVATE";
    public static final String ACTION_VIEW_REPORT = "VIEW_REPORT";
    public static final String ACTION_EXPORT_REPORT = "EXPORT_REPORT";
    public static final String ACTION_REFRESH = "REFRESH GRID";
    public static final String ACTION_UPLOAD_FILE = "ACTION_UPLOAD_FILE";
    public static final String ACTION_CHANGE_STATUS = "CHANGE STATUS";
    //    //-----------------------COMMON--------------------------
    public static final String AP_PARAM_CATALOGUE_STATUS = "STATUS";
    public static final String CURRENT_OBJECT = "CURRENT_OBJECT";
    public static final String REPORT_TEMPLATE_PATH = "/WEB-INF/report_template/";
    public static final String TEMP_FOLDER = "tempFolder";
    public static final String COMMAND_ACTIVE_CARD = "active";
    public static final String SQL_PARAM_SEPARATOR = "#";
    public static final String MAIL_SERVER_HOST = "smtp.viettel.com.vn";
    public static final int MAIL_SERVER_PORT = 465;
    public static final String CONFIG_RESROUCE_FILE = "config";
    public static final String WRONG_FILE_CONTENT_FORMAT = "WRONG_FILE_CONTENT_FORMAT";
    public static final String EXCEPTION_LOG = "exceptionLog";
    public static final String ACTION_LOG = "actionLog";
    public static final String DEFAULT_EXCEPTION_LEVEL = "ERROR";
    public static final String DEFAULT_ERR_CODE = "FMS_01";
    public static final String APP_CODE = "CSP";
    public static final String REQUEST_TIME = "REQUEST_TIME";
    public static final String REQUEST_PARAMS = "REQUEST_PARAMS";
    public static final String REPORT_CODE = "REPORT_CODE";
    public static final int NOTIFY_STATUS = 1;
    public static final String DATA = "DATA";
    public static final String CONTRACTSENDPARTNER = "12";
    public static final String DEPARTMENT_VIETTEL_CSP = "005";;
    public static final int SELECT_MAX_ROW = 100;


    public static class DataType {

        public static final String LONG = "LONG";
        public static final String DOUBLE = "DOUBLE";
        public static final String FLOAT = "FLOAT";
        public static final String STRING = "STRING";
        public static final String DATE = "DATE";
        public static final String COMBO = "COMBO";
        public static final String COMBOVALUE = "COMBOVALUE";
        public static final String LISTBOX = "LISTBOX";
    }

    public static class ReportDataType {

        public static final String STRING = "STRING";
        public static final String LONG = "LONG";
        public static final String DOUBLE = "DOUBLE";
        public static final String DATE = "DATE";
        public static final String COMBOBOX = "COMBOBOX";
        public static final String COMBOVALUE = "COMBOVALUE";
        public static final String CHECKBOX = "CHECKBOX";
        public static final String LISTBOX = "LISTBOX";
        public static final String LISTVALUE = "LISTVALUE";
        public static final String TREEBOX = "TREEBOX";
        public static final String TREEVALUE = "TREEVALUE";
        public static final String ATTACHFILE = "ATTACHFILE";
        public static final String IDVOFFICE = "IDVOFFICE";
    }

    public class USER_TYPE {

        public static final String CONTACT = "CONTACT";
        public static final String PARTNER = "PARTNER";
        public static final String VIETTEL = "VIETTEL";
    }

    public class USER_STATUS {

        public static final String LOCK = "LOCK";
        public static final String ACTIVE = "ACTIVE";

        public static final String INACTIVE = "INACTIVE";
        public static final String WAITING_AUTHEN = "WAITING_AUTHEN";
    }

    public class PARTNER_GROUP {

        public static final String INTERNAL = "INTERNAL";
        public static final String FORENAL = "FORENAL";
    }

    public class REASON_TYPE {

        public static final String LOCK_PARTNER = "LOCK_PARTNER";
        public static final String CONTRACT_PROFILE = "CONTRACT_PROFILE";
        public static final String BREACH = "BREACH";
        public static final String REFLECT = "REFLECT";

    }

    public class CONTRACT_TYPE {

//        public static final String CONTRACT_TYPE_OK_CONTRACT = "9";
//        public static final String CONTRACT_TYPE_CANCLE_CONTRACT = "8";
//        public static final String CONTRACT_TYPE_CREATE_CONTRACT = "1";
        public static final String CONTRACT_TYPE_WAITING_OK_CONTRACT = "2";
        public static final String CONTRACT_TYPE_NOT_OK_CONTRACT = "3";
        public static final String CONTRACT_TYPE_WAITING_OK_CONTRACT_FILES = "4";
        public static final String CONTRACT_TYPE_CANCLE_REGIS_OK_CONTRACT_FILES = "5";
        public static final String CONTRACT_TYPE_REGIS_OK_CONTRACT_FILES = "6";
        public static final String CONTRACT_TYPE_CREATE_REGIS_OK_CONTRACT_FILES = "7";

        public static final String CONTRACT_TYPE_STATUS_ACTIVE = "A002";
        public static final String CONTRACT_TYPE_STATUS_AWAIT_ACTIVE = "A001";
        public static final String CONTRACT_TYPE_STATUS_COLSE = "A003";
        public static final String CONTRACT_TYPE_STATUS_FINISH = "A004";

        public static final String CONTRACT_TYPE_TASK_WAIT_ACTIVE = "01";
        public static final String CONTRACT_TYPE_TASK_ACTIVE = "02";
        public static final String CONTRACT_TYPE_TASK_COLSE = "03";
        public static final String CONTRACT_TYPE_TASK_FINISH = "04";

        public static final String CONTRACT_TYPE_OK_ACCEPT = "25";
        public static final String CONTRACT_TYPE_NO_ACCEPT = "24";
        public static final String CONTRACT_TYPE_OK_CONTRACT_FILES = "10";
        public static final String CONTRACT_TYPE_OK_CONTRACT_FILES_FOR_PARTNER = "11";
        public static final String CONTRACT_TYPE_OK_CONTRACT_FILES_FOR_PARTNER_1 = "12";
        public static final String CONTRACT_TYPE_OK_CONTRACT_FILES_WAITING_FOR_PARTNER = "13";
        public static final String CONTRACT_TYPE_REGISTER = "15";
        public static final String CONTRACT_TYPE_OK_CONTRACT_FILES_REGISTED = "18";
        public static final String CONTRACT_TYPE_OK_CONTRACT_FILES_REGISTED_PARTNER = "17";
        public static final String CONTRACT_TYPE_OK_CONTRACT_FILES_REGISTED_TO_PARTNER = "19";
        public static final String CONTRACT_TYPE_OK_CONTRACT_EFFECTING_CONTRACT = "26";
        public static final String CONTRACT_TYPE_OK_CONTRACT_TERMINATED_CONTRACT = "28";
        public static final String CONTRACT_TYPE_TGD_CANCLE = "14";
        public static final String CONTRACT_TYPE_TTHĐ = "TL02";
        public static final String CONTRACT_TYPE_REJECT_SIGN = "20";
        public static final String CONTRACT_TYPE_SAVE_AND_DEPLOYMENT = "21";
    }

    public class FILE_TYPE {

        public static final String FILE_TYPE_HS = "HOSO";
        public static final String FILE_TYPE_HD = "HOPDONG";
        public static final String FILE_TYPE_PHULUC = "PHULUC";
        public static final String FILE_TYPE_TOTRINH_HOPDONG = "TOTRINH_HOPDONG";
        public static final String FILE_TYPE_TOTRINH_HOSO = "TOTRINH_HOSO";
        public static final String FILE_TYPE_TLHĐ = "THANHLI_HĐ";
        public static final String FILE_TYPE_PHULUC_TLHĐ = "PHULUC_TLHĐ";
    }

    public static class TYPE_ACTION {

        public static final String[] LIST_ACTION_PROFILE = {"1", "2", "3", "4", "5", "6", "8", "9", "10", "-99"};
        public static final String[] LIST_ACTION_CONTRACT = {"7", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "-99"};
        public static final String[] LIST_ACTION_LIQUIDATION = {"TL01", "TL02", "TL03", "TL04", "TL05", "TL06", "TL07", "TL08", "TL09", "TL10", "TL11", "TL12","-99"};
        //1	Đang tạo hồ sơ
        //2	Đang chờ duyệt hồ sơ
        //3	Hồ sơ không hợp lệ
        //4	Đang chờ duyệt tờ trình
        //5	Từ chối ký tờ trình
        //6	Ký tờ trình
        //7	Tạo hợp đồng dự thảo
        //8	Từ chối hồ sơ
        //9	Duyệt hồ sơ
        //10	Duyệt tờ trình
        //11	Đã gửi HĐ cho đối tác
        //12	Gửi HĐ cho Viettel
        //13	Trình phê duyệt HĐ
        //14	Từ chối phê duyệt HĐ
        //15	Trình ký HĐ
        //16	Từ chối ký HĐ
        //17	Đã ký HĐ
        //18	Gửi HĐ đã ký cho đối tác
        //19	Đối tác đã ký HĐ
        //20	Đối tác từ chối Ký HĐ
        //21	Lưu và triển khai HĐ
        //22	Hủy bỏ Hồ sơ dự thảo
        //23	Hủy bỏ HĐ dự thảo
        //24	Từ chối tiếp nhận công việc
        //26    Hợp đồng có hiệu lực
        //28    Hợp đồng đang tạm dừng
        public static final int CANCLE_CONTRACT = 0;
        public static final int NOK_APPROVE_PROFILE= 1;
        public static final int WAITING_OK_CONTRACT_FILES = 2;
        public static final int WAITING_REFUSE_CONTRACT_FILES = 3;
        public static final int WAITING_SEND_CONTRACT_FILES = 5;
        public static final int OK_SEND_CONTRACT_FILES = 6;
        public static final int OK_ACCEPT = 10;
        public static final int NO_ACCEPT = 11;
        public static final int OK_SEND_CONTRACT_TO_CSP_REGISTER = 7;
        public static final int OK_SEND_CONTRACT_WAITING_TO_CSP_REGISTER = 8;
        public static final int CREAT_CONTRACT = 9;
        public static final int CREAT_CONTRACT_REGISTER_PARTNER = 12;
        public static final int CONTRACTSENDPARTNER = 12;
        public static final int CANCLE_CONTRACT_APPROVAL = 14;
        public static final int REGISTER_CONTRACT_APPROVAL = 15;
        public static final int SEND_CONTRACT_TO_VIETTEL = 16;
        public static final int TGD_CANCLE_VIETTEL = 17;
        public static final int TGD_CANCLE_VIETTEL_REGISTER = 18;
        public static final int EFFECTING_CONTRACT = 26;
        public static final int TERMINATED_CONTRACT = 28;
        public static final int SUBMMITED_TO_APPRAISER = 13;
        public static final int NOT_ACTIVE_TASK_CONTRACT = 24;
        public static final int SAVE_AND_DEPLOYMENT_CONTRACT = 21;
    }

    public class CONTRACT_PARTNER {

        public static final String CONTRACTFILESNAME = "HOPDONG";
        public static final String CONTRACTFILESNAMEMENU = "PHULUCHOPDONG";
        public static final int CONTRACTSENDPARTNER = 12;
        public static final String CONTRACTSENDPARTNERSTATUS = "12";
        public static final int CONTRACT_STARUS_REGISTER = 19;
        public static final String CONTRACT_REGISTER = "19";
        public static final String CONTRACTHIS_STATUS_BEFORE = "11";
        public static final String CONTRACTHIS_STATUS_AFTER = "12";
        public static final String CONTRACT_STATUS_CANCEL_REGISTER = "20";
        public static final int CONTRACT_CANCEL_REGISTER = 20;
        public static final int CONTRACT_SEND_PARNER = 17;
        public static final String CONTRACT_STATUS_SEND_PARNER = "18";
    }


    public class REFLECT_STATUS {

        public static final String NEW = "001";
        public static final String SEND = "002";
        public static final String REPONSE_CUSTOMER = "003";
        public static final String CLOSE = "004";
    }

    public class PARTNER_STATUS {

        public static final String INACTIVE = "001";
        public static final String REQUEST_VIETTEL_APPROVE = "002";
        public static final String ACTIVE = "003";
        public static final String LOCK = "004";
    }

    public class SOLUTION_STATUS {

        public static final String REPONSE = "REPONSE";
        public static final String OK = "OK";
        public static final String NOK = "NOK";
        public static final String MORE_INFO = "MORE_INFO";
    }

    public class TASKS_STATUS {

        public static final String OK_ACTIVE = "1";
        public static final String NOT_ACTIVE = "2";
        public static final String WAIT_ACTIVE = "0";
        public static final String FAIL_TASK = "3";
        public static final String DONE_TASK = "4";
    }

    public class LI_STATUS {
        public static final String LIQUIDATION_CREAT = "TL02";
        public static final String LIQUIDATION_SENDTOVT = "TL03";
        public static final String LIQUIDATION_ASSIGN_HD = "TL04";
        public static final String LIQUIDATION_ASSIGN_REGISTER = "TL05";
        public static final String LIQUIDATION_CANCLE_REGISTER = "TL06";
        public static final String LIQUIDATION_REGISTERED = "TL07";
        public static final String LIQUIDATION_REGISTERED_CACLE = "TL08";
        public static final String LIQUIDATION_SEND_TO_PARTNER = "TL09";
        public static final String LIQUIDATION_PARTNER_REGISTER = "TL10";
        public static final String LIQUIDATION_PARTNER_CANCLE_REGISTER = "TL11";
        public static final String LIQUIDATION_END_CONTRACT = "TL12";
    }

    public class FILE_STATUS {
        public static final String DELETE = "DELETE";
    }

    public class COMPOSER_ADD {
        public static final String COMPOSER_ADDFILE = "COMPOSER";
    }
    public class SERVICE_STATUS {
        public static final String NEW = "1";
        public static final String NOT_ACTIVE = "2";
        public static final String WAIT_ACTIVE = "0";
        public static final String FAIL_TASK = "3";
        public static final String DONE_TASK = "4";
    }

}
