//package com.viettel.csp.util;
//
//import com.lowagie.text.html.HtmlEncoder;
//import com.viettel.eafs.util.SpringUtil;
////import com.viettel.plc.DTO.catalogue.ItemDTO;
////import com.viettel.plc.DTO.catalogue.StandardAttributeDTO;
////import com.viettel.plc.DTO.utilities.report.MediaDTO;
////import com.viettel.plc.entity.catalogue.StandardAttributeEntity;
////import com.viettel.plc.service.catalogue.AppParamsService;
////import com.viettel.plc.service.catalogue.StandardAttributeService;
//import com.viettel.plc.widget.FileDisplay;
//import com.viettel.plc.widget.MultiFileSelector;
//import com.viettel.plc.widget.ViewVOfficeFile;
//import org.zkoss.util.media.AMedia;
//import org.zkoss.util.media.Media;
//import org.zkoss.zk.ui.Component;
//import org.zkoss.zk.ui.Desktop;
//import org.zkoss.zk.ui.Executions;
//import org.zkoss.zk.ui.Sessions;
//import org.zkoss.zk.ui.event.Event;
//import org.zkoss.zk.ui.event.UploadEvent;
//import org.zkoss.zk.ui.util.Clients;
//import org.zkoss.zk.ui.util.GenericForwardComposer;
//import org.zkoss.zul.*;
//
//import java.io.File;
//import java.math.BigDecimal;
//import java.util.*;
//import java.util.List;
//
///**
// * Created by tuydv1 on 6/9/2016.
// */
//public class GenCodeComponent extends GenericForwardComposer {
//
//    private static final String ATTR_VALUE_PATTERN = "VALUE_PATTERN";
//    private static final String ATTR_DISPLAY_NAME = "DISPLAY_NAME";
//    private static final String ATTR_REQUIRED = "REQUIRED";
//    private static StandardAttributeEntity standardAttributeEntity;
//    //gen code tree
//    private Bandbox bandboxDynamicGen;
//    private Listbox listboxDynamicGen;
//    private String idBbOld = ""; //dung de luu id cua Bandbox da chon
//    private Tree treeDynamicGen;
//    private Checkbox checkboxDynamicGen;
//    private Textbox txtSearchTreeDynamicGen;
//    private Textbox txtSearchListDynamicGen;
//    private Button btnSearchTreeDynamicGen;
//    private Button btnSearchListDynamicGen;
//    private StandardAttributeDTO bbGenSelectedDTO;
//    private Map<String, Map<String, ItemDTO>> mapItemSearch;
//    private List<Object[]> lstObjectForTree;
//    private List<Object[]> lstObjectForListbox;
//    private final AppParamsService appParamsService = SpringUtil.getBean("AppParamsService", AppParamsService.class);
//    public StandardAttributeService standardAttributeService = SpringUtil.getBean("StandardAttributeService", StandardAttributeService.class);
//    final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger("exceptionLogger");
//    //gen upload file
//    private MultiFileSelector multiFileAttach;
//    private Vbox pnlMultiFile;
//    private Button btnAddFile;
//    private FileDisplay fileDisplay;
//    private Label btnDown;
//    private Image btnDel;
//    private Media media = null;
//    private Media[] medias;
//    private String[] FILE_EXTEND;
//    private String value = "";
//    private int positionView;
//    //xem file VOffice
//    private ViewVOfficeFile viewVOfficeFile;
//
//    public void createCellLabel(String label, boolean isRequired, Component parent) throws Exception {
//        StringBuilder strCell = new StringBuilder("")
//                .append("<cell colspan=\"1\" width=\"150px\">")
//                .append("    <label value=\"").append(HtmlEncoder.encode(label)).append("\" style=\"word-wrap: break-word;\"/>");
//        if (isRequired) {
//            strCell.append(" <label value=\" (*)\" style=\"color:red\"></label>");
//        }
//        strCell.append(" </cell>");
//        Executions.createComponentsDirectly(strCell.toString(), "zul", parent, null);
//    }
//
//    public void createCellLabelForSearch(String label, boolean isRequired, Component parent) throws Exception {
//        StringBuilder strCell = new StringBuilder("")
//                .append("<cell colspan=\"1\" width=\"150px\">")
//                .append("    <label value=\"").append(HtmlEncoder.encode(label)).append("\" style=\"word-wrap: break-word;\"/>")
//                .append("</cell>");
//        Executions.createComponentsDirectly(strCell.toString(), "zul", parent, null);
//    }
//
//    public synchronized Component createComponent(StandardAttributeDTO standardAttributeDTO) throws Exception {
//        standardAttributeEntity = standardAttributeDTO.toBO();
//        Component tmpComp = null;
//        if (ParamUtils.ReportDataType.STRING.equalsIgnoreCase(standardAttributeEntity.getAttributeType())) {
//            tmpComp = createTextbox(standardAttributeEntity);
//        } else if (ParamUtils.ReportDataType.LONG.equalsIgnoreCase(standardAttributeEntity.getAttributeType())) {
//            tmpComp = createLongbox(standardAttributeEntity);
//        } else if (ParamUtils.ReportDataType.DOUBLE.equalsIgnoreCase(standardAttributeEntity.getAttributeType())) {
//            tmpComp = createDoublebox(standardAttributeEntity);
//        } else if (ParamUtils.ReportDataType.DATE.equalsIgnoreCase(standardAttributeEntity.getAttributeType())) {
//            tmpComp = createDatebox(standardAttributeEntity);
//            //Date defaultDate = getDefaultDate(standardAttributeEntity.getDefaultValue(), standardAttributeEntity.getValPattern());
//            //((Datebox) tmpComp).setValue(defaultDate == null ? new Date() : defaultDate);
//        } else if (ParamUtils.ReportDataType.COMBOVALUE.equalsIgnoreCase(standardAttributeEntity.getAttributeType())
//                || ParamUtils.ReportDataType.COMBOBOX.equalsIgnoreCase(standardAttributeEntity.getAttributeType())) {
//            tmpComp = createCombobox(standardAttributeEntity);
//        } else if (ParamUtils.ReportDataType.CHECKBOX.equalsIgnoreCase(standardAttributeEntity.getAttributeType())) {
//            tmpComp = createCheckbox(standardAttributeEntity);
//        } else if (ParamUtils.ReportDataType.TREEBOX.equalsIgnoreCase(standardAttributeDTO.getAttributeType())
//                || ParamUtils.ReportDataType.TREEVALUE.equalsIgnoreCase(standardAttributeDTO.getAttributeType())) {
//            tmpComp = createBandbox(standardAttributeDTO);
//        } else if (ParamUtils.ReportDataType.LISTBOX.equalsIgnoreCase(standardAttributeDTO.getAttributeType())
//                || ParamUtils.ReportDataType.LISTVALUE.equalsIgnoreCase(standardAttributeDTO.getAttributeType())) {
//            tmpComp = createListbox(standardAttributeDTO);
//        } else if (ParamUtils.ReportDataType.ATTACHFILE.equalsIgnoreCase(standardAttributeEntity.getAttributeType())) {
//            tmpComp = createAttachFile(standardAttributeDTO);
//        } else if (ParamUtils.ReportDataType.IDVOFFICE.equalsIgnoreCase(standardAttributeEntity.getAttributeType())) {
//            tmpComp = createVOfficeView(standardAttributeDTO);
//        }
//        return tmpComp;
//    }
//
//    /////////////Begin create controls
//    private Textbox createTextbox(StandardAttributeEntity standardAttributeEntity) {
//        Textbox textBox = new Textbox();
//        textBox.setId(standardAttributeEntity.getAttributeCode());
//        textBox.setHflex("3");
//        textBox.setMaxlength(standardAttributeEntity.getAttributeLength().intValue());
//        textBox.setAttribute(ATTR_DISPLAY_NAME, standardAttributeEntity.getAttributeName());
//        textBox.setAttribute(ATTR_REQUIRED, standardAttributeEntity.getIsRequired());
//        if (StringUtils.validString(standardAttributeEntity.getValPattern())) {
//            textBox.setAttribute(ATTR_VALUE_PATTERN, standardAttributeEntity.getValPattern());
//        }
//        if (standardAttributeEntity.getDefaultValue() != null) {
//            textBox.setValue(standardAttributeEntity.getDefaultValue());
//        }
//        return textBox;
//    }
//
//    private Longbox createLongbox(StandardAttributeEntity standardAttributeEntity) {
//        Longbox longBox = new Longbox();
//        longBox.setId(standardAttributeEntity.getAttributeCode());
//        longBox.setHflex("3");
//        longBox.setMaxlength(standardAttributeEntity.getAttributeLength().intValue());
//        longBox.setAttribute(ATTR_DISPLAY_NAME, standardAttributeEntity.getAttributeName());
//        longBox.setAttribute(ATTR_REQUIRED, standardAttributeEntity.getIsRequired());
//        if (standardAttributeEntity.getDefaultValue() != null) {
//            longBox.setValue(StringUtils.getLong(standardAttributeEntity.getDefaultValue()));
//        }
//        return longBox;
//    }
//
//    private Checkbox createCheckbox(StandardAttributeEntity standardAttributeEntity) {
//        Checkbox chkBox = new Checkbox();
//        chkBox.setId(standardAttributeEntity.getAttributeCode());
//        chkBox.setHflex("3");
//        //chkBox.setLabel(standardAttributeEntity.getAttributeName());
//        chkBox.setAttribute(ATTR_DISPLAY_NAME, standardAttributeEntity.getAttributeName());
//        chkBox.setAttribute(ATTR_REQUIRED, standardAttributeEntity.getIsRequired());
//        return chkBox;
//    }
//
//    private Doublebox createDoublebox(StandardAttributeEntity standardAttributeEntity) {
//        Doublebox doubleBox = new Doublebox();
//        doubleBox.setId(standardAttributeEntity.getAttributeCode());
//        doubleBox.setHflex("3");
//        doubleBox.setMaxlength(standardAttributeEntity.getAttributeLength().intValue());
//        //doubleBox.setFormat(",###.###");
//        doubleBox.setPlaceholder("######,##");
//        doubleBox.setConstraint("no negative");
//        doubleBox.setAttribute(ATTR_DISPLAY_NAME, standardAttributeEntity.getAttributeName());
//        doubleBox.setAttribute(ATTR_REQUIRED, standardAttributeEntity.getIsRequired());
//        if (standardAttributeEntity.getDefaultValue() != null) {
//            doubleBox.setValue(StringUtils.getDouble(standardAttributeEntity.getDefaultValue()));
//        }
//        return doubleBox;
//    }
//
//    private Datebox createDatebox(StandardAttributeEntity standardAttributeEntity) {
//        Datebox dateBox = new Datebox();
//        dateBox.setId(standardAttributeEntity.getAttributeCode());
//        dateBox.setHflex("3");
//        dateBox.setAttribute(ATTR_DISPLAY_NAME, standardAttributeEntity.getAttributeName());
//        dateBox.setAttribute(ATTR_REQUIRED, standardAttributeEntity.getIsRequired());
//        if (standardAttributeEntity.getValPattern() != null) {
//            dateBox.setFormat(standardAttributeEntity.getValPattern());
//        } else {
//            dateBox.setFormat(ParamUtils.ddMMyyyy);
//        }
//        return dateBox;
//    }
//
//    private Combobox createCombobox(StandardAttributeEntity standardAttributeEntity) {
//        Combobox comboBox = new Combobox();
//        List<KeyValueBean> lstItems = getLstListitem(standardAttributeEntity.getAttributeType(),
//                standardAttributeEntity.getInitValue(),
//                standardAttributeEntity.getDefaultValue());
//        String defaultValue = standardAttributeEntity.getDefaultValue();
//        comboBox.setId(standardAttributeEntity.getAttributeCode());
//        comboBox.setMold("rounded");
//        comboBox.setHflex("3");
//        comboBox.setReadonly(true);
//        comboBox.setAttribute(ATTR_DISPLAY_NAME, standardAttributeEntity.getAttributeName());
//        comboBox.setAttribute(ATTR_REQUIRED, standardAttributeEntity.getIsRequired());
//        Comboitem comboItem;
//        Comboitem selectedItem = null;
//        for (KeyValueBean bean : lstItems) {
//            comboItem = new Comboitem();
//            comboItem.setValue(bean.getKey());
//            comboItem.setLabel(bean.getValue());
//            comboItem.setParent(comboBox);
//            if (defaultValue != null && defaultValue.equalsIgnoreCase(bean.getValue().toString())) {
//                selectedItem = comboItem;
//            }
//        }
//        if (selectedItem != null) {
//            comboBox.setSelectedItem(selectedItem);
//        } else {
//            comboBox.setSelectedIndex(0);
//        }
//        return comboBox;
//    }
//
//    private List<KeyValueBean> getLstListitem(String type, String parValue, String defaultValue) {
//        List<KeyValueBean> result = new ArrayList<>();
//        if (defaultValue != null && !"".equals(defaultValue)) {
//            result.add(new KeyValueBean("", LanguageBundleUtils.getString(defaultValue)));
//        }
//        if (ParamUtils.ReportDataType.COMBOVALUE.equalsIgnoreCase(type)) {
//            String[] splitted = parValue.split(";");
//            for (int i = 0; i < splitted.length - 1; i = i + 2) {
//                result.add(new KeyValueBean(splitted[i].trim(), LanguageBundleUtils.getString(splitted[i + 1].trim())));
//            }
//        } else {
//            result.addAll(standardAttributeService.getListCboItem(parValue));
//        }
//        return result;
//    }
//
//    public Object getDefaultValue(StandardAttributeEntity standardAttributeEntity) throws Exception {
//        Object result;
//        if (ParamUtils.ReportDataType.LONG.equalsIgnoreCase(standardAttributeEntity.getAttributeType())) {
//            result = StringUtils.getLong(standardAttributeEntity.getDefaultValue());
//        } else if (ParamUtils.ReportDataType.DOUBLE.equalsIgnoreCase(standardAttributeEntity.getAttributeType())) {
//            result = StringUtils.getDouble(standardAttributeEntity.getDefaultValue());
//        } else if (ParamUtils.ReportDataType.DATE.equalsIgnoreCase(standardAttributeEntity.getAttributeType())) {
//            String pattern = standardAttributeEntity.getValPattern() == null ? ParamUtils.ddMMyyyy : standardAttributeEntity.getValPattern();
//            result = getDefaultDate(standardAttributeEntity.getDefaultValue(), pattern);
//        } else {
//            result = standardAttributeEntity.getDefaultValue();
//        }
//        return result;
//    }
//
//    private Date getDefaultDate(String defaultValue, String pattern) throws Exception {
//        Date result = null;
//        Date curDate = new Date();
//        try {
//            defaultValue = defaultValue.toLowerCase().trim();
//            //Neu ngay thang la ngay cu the : 05/08/2014...
//            try {
//                result = DateTimeUtils.convertStringToTime(defaultValue, pattern);
//            } catch (Exception ex) {
//                logger.info(ex.getMessage(), ex);
//            }
//            //Neu ngay thang ko phai la ngay cu the: sysdate + 1, sysdate, sysdate - 1...
//            if (result == null && defaultValue.contains("sysdate")) {
//                boolean isAdd = true;
//                int operatorIdx;
//                int addValue;
//                //Check co +/- ngay thang ko
//                if (defaultValue.contains("+M") || defaultValue.contains("-M")) {
//                    if (defaultValue.contains("+")) {
//                        operatorIdx = defaultValue.indexOf("+");
//                    } else { //Neu tru ngay (sysdate - 1, sydate - 100 ...
//                        operatorIdx = defaultValue.indexOf("-");
//                        isAdd = false;
//                    }
//                    //Lay so ngay tru/cong
//                    addValue = Integer.valueOf(defaultValue.substring(operatorIdx + 2).trim());
//                    //addValue = getValidAddDate(curDate, isAdd, addValue);
//                    addValue = isAdd ? addValue : (0 - addValue);
//
//                    //Lay gia tri mac dinh
//                    result = DateTimeUtils.addMonth(curDate, addValue);
//                } else {
//                    if (defaultValue.contains("+") || defaultValue.contains("-")) {
//                        //Neu cong them ngay (sysdate + 1, sydate + 100 ...
//                        if (defaultValue.contains("+")) {
//                            operatorIdx = defaultValue.indexOf("+");
//                        } else { //Neu tru ngay (sysdate - 1, sydate - 100 ...
//                            operatorIdx = defaultValue.indexOf("-");
//                            isAdd = false;
//                        }
//                        //Lay so ngay tru/cong
//                        addValue = Integer.valueOf(defaultValue.substring(operatorIdx + 1).trim());
//                        addValue = DateTimeUtils.getValidAddDate(curDate, isAdd, addValue);
//                        addValue = isAdd ? addValue : (0 - addValue);
//
//                        //Lay gia tri mac dinh
//                        result = DateTimeUtils.addDate(curDate, addValue);
//                    } else {
//                        //Neu khong cong/tru ngay thang -> tra ve sysdate
//                        result = curDate;
//                    }
//                }
//            } else {
//                result = result == null ? curDate : result;
//            }
//        } catch (Exception ex) {
//            logger.error(ex.getMessage(), ex);
//            result = curDate;
//        }
//        return result;
//    }
//
//    public void setFocus(Component comp) {
//        if (comp instanceof Datebox) {
//            ((Datebox) comp).setFocus(true);
//        } //Xy ly Long controls
//        else if (comp instanceof Longbox) {
//            ((Longbox) comp).setFocus(true);
//        } //Xy ly Float controls
//        else if (comp instanceof Doublebox) {
//            ((Doublebox) comp).setFocus(true);
//        } //Xy ly Textbox controls
//        else if (comp instanceof Textbox && !(comp instanceof Combobox)) {
//            ((Textbox) comp).setFocus(true);
//        } //Xu ly Combo controls
//        else if (comp instanceof Listbox) {
//            ((Listbox) comp).setFocus(true);
//        } else if (comp instanceof Combobox) {
//            ((Combobox) comp).setFocus(true);
//        }
//    }
//
//    public String getValue(Component comp) throws Exception {
//        String result = null;
//        if (comp instanceof Doublebox) {
//            if (((Doublebox) comp).getValue() != null) {
//                result = ((Doublebox) comp).getValue().toString().replace(",", ".");
//            }
//        } else if (comp instanceof Longbox) {
//            result = ((Longbox) comp).getText().trim();
//        } else if (comp instanceof Datebox) {
//            result = DateTimeUtils.convertDateToString(((Datebox) comp).getValue());
//        } else if (comp instanceof Combobox) {
//            result = ((Combobox) comp).getSelectedItem().getValue().toString();
//        } else if (comp instanceof Checkbox) {
//            Checkbox checkbox = (Checkbox) comp;
//            result = checkbox.isChecked() == true ? "1" : "0";
//        } else if (comp instanceof Bandbox) {
//            result = "";
//            //Lay gia tri truong dau tien trong cau lenh truoc khi save DB
//            if (comp.getChildren().get(0).getChildren().get(0).getNextSibling() instanceof Tree) {
//                List<Object[]> lstObjectTree = (List<Object[]>) Sessions.getCurrent().getAttribute(comp.getId());
//                int p = Integer.parseInt(comp.getAttribute("POSITION_VIEW").toString());
//                if (StringUtils.validString(((Bandbox) comp).getText().trim())) {
//                    Object[] compValues = ((Bandbox) comp).getText().trim().split("\\|");
//                    int i = 0;
//                    for (Object val : compValues) {
//                        for (Object[] itemObj : lstObjectTree) {
//                            if (val.toString().equals(itemObj[5 + p].toString())) {
//                                if (i < compValues.length - 1) {
//                                    result += itemObj[0] + "|";
//                                } else {
//                                    result += itemObj[0];
//                                }
//                                break;
//                            }
//                        }
//                        i++;
//                    }
//                }
//            } else if (comp.getChildren().get(0).getChildren().get(0).getNextSibling() instanceof Listbox) {
//                List<Object[]> lstObjectListbox = (List<Object[]>) Sessions.getCurrent().getAttribute(comp.getId());
//                int p = Integer.parseInt(comp.getAttribute("POSITION_VIEW").toString());
//                if (StringUtils.validString(((Bandbox) comp).getText().trim())) {
//                    Object[] compValues = ((Bandbox) comp).getText().trim().split("\\|");
//                    int i = 0;
//                    for (Object val : compValues) {
//                        for (Object[] itemObj : lstObjectListbox) {
//                            if (val.toString().equals(itemObj[p - 1].toString())) {
//                                if (i < compValues.length - 1) {
//                                    result += itemObj[0] + "|";
//                                } else {
//                                    result += itemObj[0];
//                                }
//                                break;
//                            }
//                        }
//                        i++;
//                    }
//                }
//            }
//        } else if (comp instanceof MultiFileSelector) {
//            result = "";
//            List<Component> lstComp = comp.getChildren().get(0).getLastChild().getChildren();
//            if (lstComp != null && !lstComp.isEmpty()) {
//                int i = 0;
//                for (Component c : lstComp) {
//                    if (i < lstComp.size() - 1) {
//                        result += ((FileDisplay) c).getValue().trim() + "|";
//                        i++;
//                    } else {
//                        result += ((FileDisplay) c).getValue().trim();
//                    }
//                }
//            }
//        } else if (comp instanceof Textbox) {
//            result = ((Textbox) comp).getText().trim();
//        }
//        return result;
//    }
//
//    public void setValue(Component comp, String value, String pattern) throws Exception {
//        if (comp instanceof Doublebox) {
//            if (StringUtils.validString(value)) {
//                //((Doublebox) comp).setValue(Double.valueOf(value.replaceAll(",", ".")));
//                ((Doublebox) comp).setValue(Double.parseDouble(value.replace(",", ".")));
//            } else {
//                ((Doublebox) comp).setValue(null);
//            }
//        } else if (comp instanceof Longbox) {
//            if (StringUtils.validString(value)) {
//                ((Longbox) comp).setValue(Long.parseLong(value));
//            } else {
//                ((Longbox) comp).setValue(null);
//            }
//        } else if (comp instanceof Datebox) {
//            if (pattern == null || pattern == "") {
//                pattern = ParamUtils.ddMMyyyy;
//            }
//            if (value == null || "".equals(value)) {
//                ((Datebox) comp).setValue(null);
//            } else {
//                ((Datebox) comp).setValue(DateTimeUtils.convertStringToTime(value, pattern));
//            }
//        } else if (comp instanceof Combobox) {
//            setSelectedCbo((Combobox) comp, value);
//        } else if (comp instanceof Checkbox) {
//            Checkbox checkbox = (Checkbox) comp;
//            if (StringUtils.validString(value) && "1".equals(value)) {
//                checkbox.setChecked(true);
//            } else {
//                checkbox.setChecked(false);
//            }
//        } else if (comp instanceof Bandbox) {
//            if (value == null || "".equals(value)) {
//                ((Bandbox) comp).setValue(null);
//            }
//            if (value != null && value.trim().length() > 0) {
//                String valMapped = "";
//                if (comp.getChildren().get(0).getChildren().get(0).getNextSibling() instanceof Tree) {
//                    List<Object[]> lstObjectTree = (List<Object[]>) Sessions.getCurrent().getAttribute(comp.getId());
//                    int p = Integer.parseInt(comp.getAttribute("POSITION_VIEW").toString());
//                    String[] arrVal = value.trim().split("\\|");
//                    if (lstObjectTree != null && !lstObjectTree.isEmpty()) {
//                        int i = 0;
//                        for (String val : arrVal) {
//                            for (Object[] itemObj : lstObjectTree) {
//                                if (val.trim().equals(itemObj[0].toString().trim())) {
//                                    if (i < arrVal.length - 1) {
//                                        valMapped += itemObj[5 + p].toString() + "|";
//                                    } else {
//                                        valMapped += itemObj[5 + p].toString();
//                                    }
//                                    break;
//                                }
//                            }
//                            i++;
//                        }
//                    }
//                }
//                if (comp.getChildren().get(0).getChildren().get(0).getNextSibling() instanceof Listbox) {
//                    List<Object[]> lstObjectListbox = (List<Object[]>) Sessions.getCurrent().getAttribute(comp.getId());
//                    int p = Integer.parseInt(comp.getAttribute("POSITION_VIEW").toString());
//                    String[] arrVal = value.trim().split("\\|");
//                    if (lstObjectListbox != null && !lstObjectListbox.isEmpty()) {
//                        int i = 0;
//                        for (String val : arrVal) {
//                            for (Object[] itemObj : lstObjectListbox) {
//                                if (val.trim().equals(itemObj[0].toString().trim())) {
//                                    if (i < arrVal.length - 1) {
//                                        valMapped += itemObj[p - 1].toString() + "|";
//                                    } else {
//                                        valMapped += itemObj[p - 1].toString();
//                                    }
//                                    break;
//                                }
//                            }
//                            i++;
//                        }
//                    }
//                }
//                ((Bandbox) comp).setValue(valMapped);
//            }
////            ((Bandbox) comp).setValue(value);
////            if (value != null && value.trim().length() > 0 && comp.getChildren().get(0).getChildren().get(0).getNextSibling() instanceof Listbox) {
////                String[] arrVal = value.split("\\|");
////                Listbox listBox = (Listbox) comp.getChildren().get(0).getChildren().get(0).getNextSibling();
////                int position = getPosition(listBox);
////                ListModel listModel = listBox.getModel();
////                int size = ((ListModelList) listModel).size();
////                for (int i = 0; i < size; i++) {
////                    Object[] item = (Object[]) ((ListModelList) listModel).get(i);
////                    for (String val : arrVal) {
////                        if (item != null && item[position - 1].equals(val)) {
////                            ((ListModelList) listModel).addToSelection(item);
////                            break;
////                        }
////                    }
////                }
////                listBox.setModel(listModel);
////            }
//        } else if (comp instanceof Textbox) {
//            ((Textbox) comp).setValue(value);
//        } else if (comp instanceof MultiFileSelector) {
//            if (StringUtils.validString(value)) {
//                String[] fileNames = value.split("\r\n");
//                MultiFileSelector multiFileSelector = (MultiFileSelector) comp;
//                Vbox vbxDisplay = (Vbox) comp.getChildren().get(0).getLastChild();
//                if (fileNames.length > 0) {
//                    for (String name : fileNames) {
//                        FileDisplay display = createFileDisplay(vbxDisplay.getId().substring(12, vbxDisplay.getId().length()));
//                        init(name, null);
//                        display.setValue(name);
//                        display.setiFileDisplay(multiFileSelector);
//                        vbxDisplay.appendChild(display);
//                    }
//                    multiFileSelector.invalidate();
//                    vbxDisplay.setVisible(true);
//                }
//            }
//        }
//    }
//
//    public void setSelectedCbo(Combobox cbo, String value) {
//        if (value == null || "".equals(value)) {
//            cbo.setSelectedItem(cbo.getItems().get(0));
//            return;
//        } else {
//            for (Comboitem item : cbo.getItems()) {
//                if (item.getValue().toString().equals(value)) {
//                    cbo.setSelectedItem(item);
//                    return;
//                }
//            }
//        }
//    }
//
//    //BUILD TREE
//    public Bandbox createBandbox(StandardAttributeDTO standardAttributeDTO) throws Exception {
//        Bandbox bandbox;
//        String strId = standardAttributeDTO.getAttributeCode();
//        int p = Integer.parseInt(standardAttributeDTO.getDefaultValue()); //truong hien thi tren giao dien
//        String[] treeHeader = standardAttributeDTO.getAttributeFormat().split(";");
//        StringBuilder strBandbox = new StringBuilder();
//        if (standardAttributeDTO.getAttributeLength() == null) {
//            strBandbox.append("<bandbox id=\"").append(strId).append("\" onClick=\"$composer.onSearchBbTreeGen(event)\" hflex=\"1\" mold=\"rounded\" readonly=\"true\">");
//        } else {
//            strBandbox.append("<bandbox id=\"").append(strId).append("\" onClick=\"$composer.onSearchBbTreeGen(event)\" hflex=\"1\" mold=\"rounded\" readonly=\"true\" maxlength=\"").append(standardAttributeDTO.getAttributeLength()).append("\">");
//        }
//        strBandbox.append("        <custom-attributes BANDBOX_ID=\"").append(strId).append("\"/>")
//                .append("          <custom-attributes POSITION_VIEW=\"").append(p).append("\"/>")
//                .append("          <bandpopup height=\"300px\">")
//                .append("              <hbox spacing=\"0\" align=\"center\">")
//                .append("                  <textbox id=\"txtTreeSearch").append(HtmlEncoder.encode(strId)).append("\" maxlength=\"50\" width=\"200px\" forward=\"onOK=$composer.onCustomTreeSearch()\"/>")
//                .append("                  <button id=\"btnTreeSearch").append(HtmlEncoder.encode(strId)).append("\" image=\"/images/search.png\" mold=\"trendy\" autodisable=\"self\" sclass=\"btn-nopadding\" onClick=\"$composer.onCustomTreeSearch()\"/>")
//                .append("              </hbox>")
//                //.append("        <tree id=\"treeBbGen").append(HtmlEncoder.encode(strId)).append("\" vflex=\"1\" width=\"800px\" zclass=\"z-dottree\" onSelect=\"$composer.onSelectBbTreeitemGen(event)\">")
//                .append("              <tree id=\"treeBbGen").append(HtmlEncoder.encode(strId)).append("\" vflex=\"1\" width=\"800px\" zclass=\"z-dottree\" checkmark=\"true\" multiple=\"true\">")
//                .append("                  <treecols sizable=\"true\">");
//        for (int k = 0; k < treeHeader.length; k++) {
//            if (k == 0) {
//                strBandbox.append("            <treecol hflex=\"1\">")
//                        .append("                  <checkbox id=\"checkboxAll").append(HtmlEncoder.encode(strId)).append("\" label=\"").append(HtmlEncoder.encode(treeHeader[k])).append("\" onCheck=\"$composer.onCheckAllTreeitem()\"/>")
//                        .append("              </treecol>");
//            } else {
//                strBandbox.append("            <treecol hflex=\"1\">")
//                        .append("                  <label value=\"").append(HtmlEncoder.encode(treeHeader[k])).append("\"/>")
//                        .append("              </treecol>");
//            }
//        }
//        strBandbox.append("                </treecols>")
//                .append("                  <template name=\"model\">")
//                .append("                      <treeitem open=\"false\" onOpen=\"$composer.onOpenTreeItemGen(event)\" checkable=\"${$composer.onCheckableTreeitem(each.data[5])}\">")
//                .append("                          <treerow>");
//        for (int k = 0; k < treeHeader.length; k++) {
//            strBandbox.append("                        <treecell>")
//                    .append("                              <label value=\"${each.data[").append(5 + k + 1).append("]}\" multiline=\"true\"/>")
//                    .append("                          </treecell>");
//        }
//        strBandbox.append("                        </treerow>")
//                .append("                      </treeitem>")
//                .append("                  </template>")
//                .append("              </tree>")
//                .append("              <div align=\"center\"> ")
//                .append("                  <button id=\"btnChoice").append(HtmlEncoder.encode(strId)).append("\" label=\"${c:l('global.choice')}\" onClick=\"$composer.onMultiSelectBbTreeGen(event)\" mold=\"trendy\" image=\"/images/accept-icon.png\"/>")
//                .append("              </div> ")
//                .append("      </bandpopup>")
//                .append("  </bandbox>");
//        bandbox = (Bandbox) Executions.createComponentsDirectly(strBandbox.toString(), "zul", null, null);
//        if (ParamUtils.ReportDataType.TREEBOX.equalsIgnoreCase(standardAttributeDTO.getAttributeType().toUpperCase())) {
//            lstObjectForTree = appParamsService.getLstObjectBySQL(standardAttributeDTO.getInitValue(), null);
//            Sessions.getCurrent().setAttribute(bandbox.getAttribute("BANDBOX_ID").toString(), lstObjectForTree);
//        }
//        if (ParamUtils.ReportDataType.TREEVALUE.equalsIgnoreCase(standardAttributeDTO.getAttributeType().toUpperCase())) {
//            String[] arrayInit = standardAttributeDTO.getInitValue().trim().split("\\|");
//            lstObjectForTree = new ArrayList<>();
//            if (arrayInit != null && arrayInit.length > 0) {
//                for (String obj : arrayInit) {
//                    Object[] arrObj = obj.split(";");
//                    lstObjectForTree.add(arrObj);
//                }
//            }
//            Sessions.getCurrent().setAttribute(bandbox.getAttribute("BANDBOX_ID").toString(), lstObjectForTree);
//        }
//        idBbOld = "";
//        return bandbox;
//    }
//
//    //Tao checkbox
//    public boolean onCheckableTreeitem(Object val) {
//        boolean isSub = false;
//        if (val != null) {
//            if ("1".equals(val.toString())) {
//                isSub = true;
//            } else {
//                isSub = false;
//            }
//        }
//        return isSub;
//    }
//
//    //gen code tree
//    public void onSearchBbTreeGen(Event event) throws Exception {
//        bandboxDynamicGen = (Bandbox) event.getTarget();
//        treeDynamicGen = (Tree) event.getTarget().getChildren().get(0).getChildren().get(0).getNextSibling();
//        checkboxDynamicGen = (Checkbox) event.getTarget().getChildren().get(0).getChildren().get(0).getNextSibling().getChildren().get(0).getChildren().get(0).getChildren().get(0);
//        txtSearchTreeDynamicGen = (Textbox) event.getTarget().getChildren().get(0).getChildren().get(0).getChildren().get(0);
//        btnSearchTreeDynamicGen = (Button) event.getTarget().getChildren().get(0).getChildren().get(0).getChildren().get(1);
//        positionView = Integer.parseInt(bandboxDynamicGen.getAttribute("POSITION_VIEW").toString());
//        String idSelected = bandboxDynamicGen.getAttribute("BANDBOX_ID").toString();
//        //Neu chon component khac moi gen lai tree
//        if (!idBbOld.equals(idSelected)) {
//            StandardAttributeDTO stdaDTOSearch = new StandardAttributeDTO();
//            stdaDTOSearch.setAttributeCode(idSelected);
//            List<StandardAttributeDTO> lstStdAttr = standardAttributeService.search(stdaDTOSearch);
//            if (lstStdAttr == null || lstStdAttr.isEmpty()) {
//                bbGenSelectedDTO = new StandardAttributeDTO();
//            } else {
//                bbGenSelectedDTO = lstStdAttr.get(0);
//            }
//            DefaultTreeModel treeGenModel = buildBandboxTreeGen(bbGenSelectedDTO, null);
//            treeDynamicGen.setModel(treeGenModel);
//            //Open first node
//            ((AbstractTreeModel) treeDynamicGen.getModel()).addOpenPath(new int[]{0});
//            idBbOld = bandboxDynamicGen.getAttribute("BANDBOX_ID").toString();
//        }
//    }
//
//    /**
//     * obj[0] = saveDB obj[1] = id obj[2] = type obj[3] = parent_id obj[4] =
//     * child_count obj[5] = sub_catalogue obj[5 + v] = view_1 obj[5 + 1 + v] =
//     * view_2 obj[5 + 2 + v] = view_3
//     */
//    private DefaultTreeModel buildBandboxTreeGen(StandardAttributeDTO bbGenSelectedDTO, String loginUser) {
//        DefaultTreeNode[] result = null;
//        //TREEBOX
//        if (ParamUtils.ReportDataType.TREEBOX.equalsIgnoreCase(bbGenSelectedDTO.getAttributeType().toUpperCase())) {
//            try {
//                lstObjectForTree = appParamsService.getLstObjectBySQL(bbGenSelectedDTO.getInitValue(), null);
//                Sessions.getCurrent().setAttribute(bbGenSelectedDTO.getAttributeCode(), lstObjectForTree);
//                //
//                Map<String, List<DefaultTreeNode>> mapItemType = new HashMap<>();
//                DefaultTreeNode temp;
//                for (Object[] item : lstObjectForTree) {
//                    if (!mapItemType.containsKey(item[2].toString())) {
//                        mapItemType.put(item[2].toString(), new ArrayList());
//                    }
//                    if (item[4] != null && Integer.parseInt(item[4].toString()) > 0) {
//                        temp = new DefaultTreeNode(item, new ArrayList());
//                    } else {
//                        temp = new DefaultTreeNode(item);
//                    }
//                    if (item[3] == null || "".equals(item[3].toString())) {
//                        mapItemType.get(item[2].toString()).add(temp);
//                    }
//                }
//                //gen cay
//                if (mapItemType.isEmpty()) {
//                    result = new DefaultTreeNode[0];
//                } else {
//                    result = new DefaultTreeNode[mapItemType.size()];
//                    int idx = 0;
//                    for (String type : mapItemType.keySet()) {
//                        Object[] newObj = new Object[100];
//                        newObj[0] = "";
//                        newObj[1] = "";
//                        newObj[2] = type;
//                        newObj[3] = "";
//                        newObj[4] = "";
//                        newObj[5] = "0";
//                        for (int p = 6; p <= 6 + positionView; p++) {
//                            if (p == 6) {
//                                newObj[p] = type;
//                            } else {
//                                newObj[p] = type;
//                            }
//                        }
//                        result[idx++] = new DefaultTreeNode(newObj, mapItemType.get(type));
//                    }
//                }
//            } catch (Exception ex) {
//                logger.error(ex.getMessage(), ex);
//            }
//        }
//
//        //TREEVALUE
//        if (ParamUtils.ReportDataType.TREEVALUE.equalsIgnoreCase(bbGenSelectedDTO.getAttributeType().toUpperCase())) {
//            String[] arrayInit = bbGenSelectedDTO.getInitValue().trim().split("\\|");
//            lstObjectForTree = new ArrayList<>();
//            if (arrayInit != null && arrayInit.length > 0) {
//                for (String obj : arrayInit) {
//                    Object[] arrObj = obj.split(";");
//                    lstObjectForTree.add(arrObj);
//                }
//            }
//            if (lstObjectForTree != null && !lstObjectForTree.isEmpty()) {
//                Sessions.getCurrent().setAttribute(bbGenSelectedDTO.getAttributeCode(), lstObjectForTree);
//                Map<String, List<DefaultTreeNode>> mapItemType = new HashMap<>();
//                DefaultTreeNode temp;
//                for (Object[] item : lstObjectForTree) {
//                    if (!mapItemType.containsKey(item[2].toString())) {
//                        mapItemType.put(item[2].toString(), new ArrayList());
//                    }
//                    if (item[4] != null && Integer.parseInt(item[4].toString()) > 0) {
//                        temp = new DefaultTreeNode(item, new ArrayList());
//                    } else {
//                        temp = new DefaultTreeNode(item);
//                    }
//                    if (item[3] == null || "".equals(item[3].toString())) {
//                        mapItemType.get(item[2].toString()).add(temp);
//                    }
//                }
//                if (mapItemType.isEmpty()) {
//                    result = new DefaultTreeNode[0];
//                } else {
//                    result = new DefaultTreeNode[mapItemType.size()];
//                    int idx = 0;
//                    for (String type : mapItemType.keySet()) {
//                        Object[] newObj = new Object[100];
//                        newObj[0] = "";
//                        newObj[1] = "";
//                        newObj[2] = type;
//                        newObj[3] = "";
//                        newObj[4] = "";
//                        newObj[5] = "0";
//                        for (int p = 6; p <= 6 + positionView; p++) {
//                            if (p == 6) {
//                                newObj[p] = type;
//                            } else {
//                                newObj[p] = type;
//                            }
//                        }
//                        result[idx++] = new DefaultTreeNode(newObj, mapItemType.get(type));
//                    }
//                }
//            }
//        }
////        DefaultTreeModel treeGenModel = new DefaultTreeModel(new DefaultTreeNode(null, result));
////        treeGenModel.setMultiple(true);
////        //Check mac dinh cac ban ghi da duoc chon
////        if (StringUtils.validString(bandboxDynamicGen.getValue())) {
////            String[] strValues = bandboxDynamicGen.getValue().trim().split("\\|");
////            for (DefaultTreeNode defaultNode : result) {
////                List<TreeNode> lstNode = defaultNode.getChildren();
////                if (lstNode != null && !lstNode.isEmpty()) {
////                    for (TreeNode treeNode : lstNode) {
////                        if (Arrays.asList(strValues).contains(((Object[]) treeNode.getData())[5 + positionView].toString())) {
////                            treeGenModel.addToSelection(treeNode);
////                        }
////                    }
////                }
////            }
////        }
//
//        final DefaultTreeModel treeGenModel = new DefaultTreeModel(new DefaultTreeNode(null, result));
//        treeGenModel.setMultiple(true);
//        final Desktop desk = Executions.getCurrent().getDesktop();
//        desk.enableServerPush(true);
//        new Thread() {
//            @Override
//            public void run() {
//                try {
//                    Executions.activate(desk);
//                    if (StringUtils.validString(bandboxDynamicGen.getValue())) {
//                        String[] strValues = bandboxDynamicGen.getValue().trim().split("\\|");
//                        List<TreeNode> lstNode = ((DefaultTreeNode) ((DefaultTreeNode) treeGenModel.getRoot()).getChildren().get(0)).getChildren();
//                        if (lstNode != null && !lstNode.isEmpty()) {
//                            for (TreeNode treeNode : lstNode) {
//                                if (Arrays.asList(strValues).contains(((Object[]) treeNode.getData())[5 + positionView].toString())) {
//                                    treeGenModel.addToSelection(treeNode);
//                                }
//                            }
//                        }
//                    }
//                    Executions.deactivate(desk);
//                } catch (Exception ex) {
//                    logger.error(ex.getMessage(), ex);
//                }
//            }
//        }.start();
//        return treeGenModel;
//    }
//
//    //Click vao nut tren tree gen
//    public void onOpenTreeItemGen(Event event) throws Exception {
//        Treeitem selectedItem = (Treeitem) event.getTarget();
//        DefaultTreeNode currTreeNode = selectedItem.getValue();
//        if (currTreeNode.getChildCount() == 0) {
//            Object[] selectedDTO = (Object[]) currTreeNode.getData();
//            List<Object[]> lstSubItems = new ArrayList<>();
//            if (ParamUtils.ReportDataType.TREEBOX.equalsIgnoreCase(bbGenSelectedDTO.getAttributeType().toUpperCase())) {
//                lstSubItems = appParamsService.getLstObjectBySQL(bbGenSelectedDTO.getInitValue(), null);
//            }
//            if (ParamUtils.ReportDataType.TREEVALUE.equalsIgnoreCase(bbGenSelectedDTO.getAttributeType().toUpperCase())) {
//                lstSubItems = lstObjectForTree;
//            }
//            if (lstSubItems != null && !lstSubItems.isEmpty()) {
//                for (Object[] item : lstSubItems) {
//                    if (item[3] != null && item[3].toString().equals(selectedDTO[1].toString())) {
//                        if (item[4] != null && Integer.parseInt(item[4].toString()) > 0) {
//                            currTreeNode.add(new DefaultTreeNode(item, new ArrayList()));
//                        } else {
//                            currTreeNode.add(new DefaultTreeNode(item));
//                        }
//                    }
//                }
//            }
//        }
//    }
//
//    public void onCheckAllTreeitem() throws Exception {
//        Collection<Treeitem> colItems = treeDynamicGen.getItems();
//        for (Treeitem item : colItems) {
//            if (checkboxDynamicGen.isChecked()) {
//                item.setSelected(true);
//            } else {
//                item.setSelected(false);
//            }
//        }
//    }
//
//    //Truong hop chon nhieu item cho TREE
//    public void onMultiSelectBbTreeGen(Event event) throws Exception {
//        String txtView = "";
//        if (treeDynamicGen != null && treeDynamicGen.getSelectedCount() > 0) {
//            List<Treeitem> lstItems = new ArrayList<>();
//            for (Treeitem item : treeDynamicGen.getSelectedItems()) {
//                if (((Object[]) ((DefaultTreeNode) item.getValue()).getData())[1] != null
//                        && !"".equals(((Object[]) ((DefaultTreeNode) item.getValue()).getData())[1].toString())) {
//                    lstItems.add(item);
//                }
//            }
//            if (!lstItems.isEmpty()) {
//                for (int i = 0; i < lstItems.size(); i++) {
//                    if (i < lstItems.size() - 1) {
//                        txtView += (((Object[]) ((DefaultTreeNode) lstItems.get(i).getValue()).getData()))[5 + positionView].toString() + "|";
//                    } else {
//                        txtView += (((Object[]) ((DefaultTreeNode) lstItems.get(i).getValue()).getData()))[5 + positionView].toString();
//                    }
//                }
//            }
//        }
//        bandboxDynamicGen.setValue(txtView);
//        bandboxDynamicGen.close();
//        checkboxDynamicGen.setChecked(false);
//    }
//
//    //Tim kiem cac tree item khi nhap text
//    public void onCustomTreeSearch() {
//        if (txtSearchTreeDynamicGen == null) {
//            txtSearchTreeDynamicGen = new Textbox();
//        }
//        if (bandboxDynamicGen == null) {
//            bandboxDynamicGen = new Bandbox();
//        }
//        if (treeDynamicGen == null) {
//            treeDynamicGen = new Tree();
//        }
//        String strSearch = txtSearchTreeDynamicGen.getText().trim();
//        List<Object[]> lstResult = new ArrayList<>();
//        if (StringUtils.validString(strSearch)) {
//            for (Object[] dto : lstObjectForTree) {
//                int l = dto.length;  //so columns trong 1 doi tuong
//                if (dto[1] != null && !"".equals(dto[1].toString())) {
//                    for (int i = 6; i < l; i++) {  //bo qua 6 phan tu dau tien
//                        if (dto[i].toString().toLowerCase().contains(strSearch.toLowerCase())) {
//                            if (!lstResult.contains(dto)) {
//                                lstResult.add(dto);
//                                break;
//                            }
//                        }
//                    }
//                }
//            }
//        } else {
//            lstResult = lstObjectForTree;
//        }
//        //Build lai TREE sau khi tim kiem
//        if (lstResult != null && !lstResult.isEmpty()) {
//            DefaultTreeNode[] result;
//            Map<String, List<DefaultTreeNode>> mapItemType = new HashMap<>();
//            DefaultTreeNode temp;
//            for (Object[] item : lstResult) {
//                if (!mapItemType.containsKey(item[2].toString())) {
//                    mapItemType.put(item[2].toString(), new ArrayList());
//                }
//                if (item[4] != null && Integer.parseInt(item[4].toString()) > 0) {
//                    temp = new DefaultTreeNode(item, new ArrayList());
//                } else {
//                    temp = new DefaultTreeNode(item);
//                }
//                //Fix de co the tim kiem dc item con
//                if (StringUtils.validString(strSearch)) {
//                    if (!mapItemType.get(item[2]).contains(temp)) {
//                        mapItemType.get(item[2]).add(temp);
//                    }
//                } else {
//                    if (item[3] == null || "".equals(item[3].toString())) {
//                        mapItemType.get(item[2]).add(temp);
//                    }
//                }
//            }
//            if (mapItemType.isEmpty()) {
//                result = new DefaultTreeNode[0];
//            } else {
//                result = new DefaultTreeNode[mapItemType.size()];
//                int idx = 0;
//                for (String type : mapItemType.keySet()) {
//                    Object[] newObj = new Object[100];
//                    newObj[0] = "";
//                    newObj[1] = "";
//                    newObj[2] = type;
//                    newObj[3] = "";
//                    newObj[4] = "";
//                    newObj[5] = "0";
//                    for (int p = 6; p <= 6 + positionView; p++) {
//                        if (p == 6) {
//                            newObj[p] = type;
//                        } else {
//                            newObj[p] = type;
//                        }
//                    }
//                    result[idx++] = new DefaultTreeNode(newObj, mapItemType.get(type));
//                }
//            }
//            if (result != null) {
//                DefaultTreeModel treeGenSearch = new DefaultTreeModel(new DefaultTreeNode(null, result));
//                treeGenSearch.setMultiple(true);
//                //Check mac dinh cac ban ghi da duoc chon
//                if (StringUtils.validString(bandboxDynamicGen.getValue())) {
//                    String[] strValues = bandboxDynamicGen.getValue().trim().split("\\|");
//                    for (DefaultTreeNode defaultNode : result) {
//                        List<TreeNode> lstNode = defaultNode.getChildren();
//                        if (lstNode != null && !lstNode.isEmpty()) {
//                            for (TreeNode treeNode : lstNode) {
//                                if (Arrays.asList(strValues).contains(((Object[]) treeNode.getData())[5 + positionView].toString())) {
//                                    treeGenSearch.addToSelection(treeNode);
//                                }
//                            }
//                        }
//                    }
//                }
//                treeDynamicGen.setModel(treeGenSearch);
//                ((AbstractTreeModel) treeDynamicGen.getModel()).addOpenPath(new int[]{0});
//            }
//        }
//    }
//
//    //----------------------------------------LISTBOX GEN
//    public Bandbox createListbox(StandardAttributeDTO standardAttributeDTO) throws Exception {
//        Bandbox bandbox;
//        String strId = standardAttributeDTO.getAttributeCode();
//        positionView = Integer.parseInt(standardAttributeDTO.getDefaultValue()); //truong hien thi tren giao dien
//        String[] listHeader = standardAttributeDTO.getAttributeFormat().split(";");
//        StringBuilder strBandbox = new StringBuilder()
//                .append("<bandbox id=\"").append(strId).append("\" onClick=\"$composer.onSearchBbListGen(event)\" width=\"100%\" mold=\"rounded\" readonly=\"true\"> ")
//                .append("    <custom-attributes BANDBOX_ID=\"").append(strId).append("\"/> ")
//                .append("    <custom-attributes POSITION_VIEW=\"").append(positionView).append("\"/>")
//                .append("    <bandpopup width=\"800px\" style=\"text-align:center;\"> ")
//                .append("        <hbox spacing=\"0\" align=\"center\"> ")
//                .append("            <textbox id=\"txtListSearch").append(HtmlEncoder.encode(strId)).append("\" maxlength=\"50\" width=\"200px\" forward=\"onOK=$composer.onCustomListSearch()\"/>")
//                .append("            <button id=\"btnListSearch").append(HtmlEncoder.encode(strId)).append("\" image=\"/images/search.png\" mold=\"trendy\" autodisable=\"self\" sclass=\"btn-nopadding\" onClick=\"$composer.onCustomListSearch()\"/>")
//                .append("        </hbox>")
//                //.append("        <listbox id=\"lbx").append(HtmlEncoder.encode(strId)).append("\" checkmark=\"true\" multiple=\"true\" onSelect=\"$composer.onSelectBbListItemGen(event)\"> ")
//                .append("        <listbox id=\"lbx").append(HtmlEncoder.encode(strId)).append("\" checkmark=\"true\" multiple=\"true\" mold=\"paging\" pageSize=\"10\">")
//                .append("            <listhead> ");
//        strBandbox.append("              <listheader width=\"50px\" label=\"STT\"/> ");
//        for (int k = 1; k < listHeader.length; k++) {
//            strBandbox.append("          <listheader label=\"").append(HtmlEncoder.encode(listHeader[k])).append("\"/> ");
//        }
//        strBandbox.append("          </listhead> ")
//                .append("            <template name=\"model\"> ")
//                .append("                <listitem> ")
//                .append("                    <listcell> ")
//                .append("                        <label value=\"${forEachStatus.index+1}\"/> ")
//                .append("                    </listcell> ");
//        for (int k = 1; k < listHeader.length; k++) {
//            strBandbox.append("              <listcell label=\"${each[").append(k).append("]}\" style=\"text-align:left;\"/> ");
//        }
//        strBandbox.append("              </listitem> ")
//                .append("            </template> ")
//                .append("        </listbox> ")
//                .append("        <div align=\"center\"> ")
//                .append("            <button id=\"btnChoice").append(HtmlEncoder.encode(strId)).append("\" label=\"${c:l('global.choice')}\" onClick=\"$composer.onMultiSelectBbListItemGen(event)\" mold=\"trendy\" image=\"/images/accept-icon.png\"/>")
//                .append("        </div> ")
//                .append("    </bandpopup> ")
//                .append("</bandbox> ");
//        bandbox = (Bandbox) Executions.createComponentsDirectly(strBandbox.toString(), "zul", null, null);
//        lstObjectForListbox = getLstItemListBox(standardAttributeDTO);
//        Sessions.getCurrent().setAttribute(standardAttributeDTO.getAttributeCode(), lstObjectForListbox);
//        return bandbox;
//    }
//
//    //gen code list
//    public void onSearchBbListGen(Event event) throws Exception {
//        bandboxDynamicGen = (Bandbox) event.getTarget();
//        listboxDynamicGen = (Listbox) event.getTarget().getChildren().get(0).getChildren().get(0).getNextSibling();
//        txtSearchListDynamicGen = (Textbox) event.getTarget().getChildren().get(0).getChildren().get(0).getChildren().get(0);
//        btnSearchListDynamicGen = (Button) event.getTarget().getChildren().get(0).getChildren().get(0).getChildren().get(1);
//        String idSelected = bandboxDynamicGen.getAttribute("BANDBOX_ID").toString();
//        //Neu chon component khac moi goi ham search
//        if (!idBbOld.equals(idSelected)) {
//            StandardAttributeDTO stdAttrSearch = new StandardAttributeDTO();
//            stdAttrSearch.setAttributeCode(idSelected);
//            List<StandardAttributeDTO> lstStdAttr = standardAttributeService.search(stdAttrSearch);
//            if (lstStdAttr == null || lstStdAttr.isEmpty()) {
//                bbGenSelectedDTO = new StandardAttributeDTO();
//            } else {
//                bbGenSelectedDTO = lstStdAttr.get(0);
//            }
//            lstObjectForListbox = getLstItemListBox(bbGenSelectedDTO);
//            Sessions.getCurrent().setAttribute(bandboxDynamicGen.getId(), lstObjectForListbox);
//            //
//            ListModelList lstModel = new ListModelList(lstObjectForListbox);
//            lstModel.setMultiple(true);
//            //Check mac dinh ban ghi da duoc chon
//            if (StringUtils.validString(bandboxDynamicGen.getValue())) {
//                if (lstObjectForListbox != null && !lstObjectForListbox.isEmpty()) {
//                    int position = getPosition(listboxDynamicGen);
//                    String[] strValues = bandboxDynamicGen.getValue().trim().split("\\|");
//                    for (Object[] obj : lstObjectForListbox) {
//                        String col = convertObjectToString(obj[position - 1]);
//                        if (Arrays.asList(strValues).contains(col)) {
//                            lstModel.addToSelection(obj);
//                        }
//                    }
//                }
//            }
//            listboxDynamicGen.setModel(lstModel);
//            listboxDynamicGen.setPageSize(10);
//            idBbOld = bandboxDynamicGen.getAttribute("BANDBOX_ID").toString();
//        }
//    }
//
//    private List<Object[]> getLstItemListBox(StandardAttributeDTO standardAttributeDTO) {
//        List<Object[]> result = new ArrayList<>();
//        if (ParamUtils.ReportDataType.LISTVALUE.equalsIgnoreCase(standardAttributeDTO.getAttributeType())) {
//            String[] values = standardAttributeDTO.getInitValue().split("[|]");
//            for (String value : values) {
//                Object[] detail = value.split("[;]");
//                if (detail != null) {
//                    result.add(detail);
//                }
//            }
//        } else {
//            try {
//                String sql = standardAttributeDTO.getInitValue();
//                result = appParamsService.getDataBySQLForList(sql);
//            } catch (Exception ex) {
//                logger.error(ex.getMessage(), ex);
//            }
//        }
//        return result;
//    }
//
//    //Truong hop chon mot ban ghi
//    public void onSelectBbListItemGen(Event event) throws Exception {
//        Listbox listbox = (Listbox) event.getTarget();
//        int position = getPosition(listbox);
//        String label = "";
//        for (Listitem item : listbox.getSelectedItems()) {
//            label = convertObjectToString(((Object[]) (item.getValue()))[position - 1]);
//        }
//        Bandbox bdx = (Bandbox) event.getTarget().getParent().getParent();
//        bdx.setValue(label);
//        bdx.close();
//    }
//
//    //Ham lay thu tu truong hien thi
//    public int getPosition(Listbox listbox) {
//        int position = -1;
//        String id = listbox.getId().substring(3);
//        StandardAttributeDTO standardAttributeDTO = new StandardAttributeDTO();
//        standardAttributeDTO.setAttributeCode(id);
//        standardAttributeDTO.setStatus(Constants.POLICY.ACTIVE);
//        String positionStr = standardAttributeService.search(standardAttributeDTO).get(0).getDefaultValue();
//        if (positionStr != null && positionStr.length() > 0) {
//            position = Integer.parseInt(positionStr.trim());
//        }
//        return position;
//    }
//
//    public String convertObjectToString(Object object) {
//        if (object == null) {
//            return null;
//        } else {
//            return object.toString().trim();
//        }
//    }
//
//    //TuanNM33: bo sung Truong hop chon nhieu ban ghi
//    public void onMultiSelectBbListItemGen(Event event) throws Exception {
//        Listbox listBox = (Listbox) event.getTarget().getParent().getPreviousSibling();
//        int position = getPosition(listBox);
//        String txtView = "";
//        if (listBox != null && listBox.getSelectedCount() > 0) {
//            int i = 0;
//            for (Listitem item : listBox.getSelectedItems()) {
//                if (i < listBox.getSelectedCount() - 1) {
//                    txtView += convertObjectToString(((Object[]) item.getValue())[position - 1]) + "|";
//                    i++;
//                } else {
//                    txtView += convertObjectToString(((Object[]) item.getValue())[position - 1]);
//                }
//            }
//        }
//        Bandbox bdx = (Bandbox) event.getTarget().getParent().getParent().getParent();
//        bdx.close();
//        bdx.setValue(txtView);
//    }
//
//    //Tim kiem cac list item khi nhap text
//    public void onCustomListSearch() {
//        String strSearch = txtSearchListDynamicGen.getText().trim();
//        List<Object[]> lstResult = new ArrayList<>();
//        if (StringUtils.validString(strSearch)) {
//            for (Object[] obj : lstObjectForListbox) {
//                if (obj != null) {
//                    for (Object tmp : obj) {
//                        if (tmp.toString().toLowerCase().contains(strSearch.toLowerCase())) {
//                            lstResult.add(obj);
//                            break;
//                        }
//                    }
//                }
//            }
//        } else {
//            lstResult = lstObjectForListbox;
//        }
//        if (lstResult != null && !lstResult.isEmpty()) {
//            ListModelList lstModel = new ListModelList(lstResult);
//            lstModel.setMultiple(true);
//            //Check mac dinh ban ghi dc chon
//            int position = getPosition(listboxDynamicGen);
//            String[] strValues = bandboxDynamicGen.getValue().trim().split("\\|");
//            for (Object[] obj : lstResult) {
//                String col = convertObjectToString(obj[position - 1]);
//                if (Arrays.asList(strValues).contains(col)) {
//                    lstModel.addToSelection(obj);
//                }
//            }
//            listboxDynamicGen.setModel(lstModel);
//            listboxDynamicGen.setPageSize(10);
//        }
//    }
//
//    //---------------------------------------- VIEW VOFFICE FILE
//    public Component createVOfficeView(StandardAttributeDTO standardAttributeDTO) throws Exception {
//        String strId = standardAttributeDTO.getAttributeCode();
//        StringBuilder strVOfficeView = new StringBuilder();
//        strVOfficeView.append("")
//                .append("<?component name=\"viewVOfficeFile\" class=\"com.viettel.plc.widget.ViewVOfficeFile\"?>")
//                .append("<?taglib uri=\"http://www.zkoss.org/dsp/web/core\" prefix=\"c\"?>")
//                .append("<cell id=\"").append(HtmlEncoder.encode(strId)).append("\">")
//                .append("    <viewVOfficeFile id=\"viewVOfficeFile").append(HtmlEncoder.encode(strId)).append("\">")
//                .append("        <hlayout id=\"hlVOffice").append(HtmlEncoder.encode(strId)).append("\" hflex=\"1\" width=\"100%\" class=\"z-label\">")
//                .append("            <textbox id=\"txtVOffice").append(HtmlEncoder.encode(strId)).append("\" hflex=\"1\" width=\"100%\"/>")
//                .append("            <button id=\"btnVOffice").append(HtmlEncoder.encode(strId)).append("\" class=\"buttonLink\" label=\"${c:l('global.view')}\" onClick=\"$composer.onVerifyAccountSSO(event)\"/>")
//                .append("        </hlayout>")
//                .append("    </viewVOfficeFile>")
//                .append("</cell>");
//        return Executions.createComponentsDirectly(strVOfficeView.toString(), "zul", null, null);
//    }
//
//    public void onVerifyAccountSSO(Event event) {
//        String id = ((Textbox) event.getTarget().getPreviousSibling()).getText();
//        if (!StringUtils.validString(id)) {
//            Clients.wrongValue((Textbox) event.getTarget().getPreviousSibling(), LanguageBundleUtils.getMessage("ERR.REQUIRED", LanguageBundleUtils.getString("attribute.IDVOFFICE")));
//            return;
//        }
//        Map<String, Object> argu = new HashMap();
//        argu.put("ID", id);
//        Window userSSO = (Window) Executions.createComponents("/controls/widget/verifyAccountSSO.zul", null, argu);
//        userSSO.doModal();
//    }
//
//    //---------------------------------------- UPLOAD FILE GEN
//    public Component createAttachFile(StandardAttributeDTO standardAttributeDTO) throws Exception {
//        String strId = standardAttributeDTO.getAttributeCode();
//        StringBuilder strAttachFile = new StringBuilder();
//        strAttachFile.append("")
//                .append("<?component name=\"multiFileSelector\" class=\"com.viettel.plc.widget.MultiFileSelector\"?>")
//                .append("<?taglib uri=\"http://www.zkoss.org/dsp/web/core\" prefix=\"c\"?>")
//                .append("<cell id=\"").append(HtmlEncoder.encode(strId)).append("\">")
//                .append("    <custom-attributes FILE_EXTEND_STR=\"").append(standardAttributeDTO.getAttributeFormat()).append("\"/>")
//                .append("    <multiFileSelector id=\"multiFileAttach").append(HtmlEncoder.encode(strId)).append("\" hflex=\"1\">")
//                .append("        <vlayout width=\"100%\" class=\"z-label\">")
//                .append("            <button id=\"btnAddFile").append(HtmlEncoder.encode(strId)).append("\" image=\"/images/arrow.png\" upload=\"true, multiple=true\" label=\"${c:l('global.choice')}\" onUpload=\"$composer.onUploadFile(event)\"/>")
//                .append("            <vbox id=\"pnlMultiFile").append(HtmlEncoder.encode(strId)).append("\" visible=\"false\" hflex=\"1\" width=\"100%\" style=\"padding: 5px 5px\">")
//                .append("            </vbox>")
//                .append("        </vlayout>")
//                .append("    </multiFileSelector>")
//                .append("</cell>");
//        return Executions.createComponentsDirectly(strAttachFile.toString(), "zul", null, null);
//    }
//
//    public FileDisplay createFileDisplay(String strId) {
//        StringBuilder strFileDisplay = new StringBuilder();
//        strFileDisplay.append("")
//                .append("<?component name=\"fileDisplay\" class=\"com.viettel.plc.widget.FileDisplay\"?>")
//                .append("<fileDisplay>")
//                .append("    <hbox align=\"left\" width=\"100%\" vflex=\"1\">")
//                .append("        <image id=\"btnDel").append(HtmlEncoder.encode(strId)).append("\" src=\"/images/icon_delete.png\" onClick=\"$composer.onDeleteFile(event)\" ")
//                .append("               style=\"padding: 1px 1px; font-size:9pt; cursor: pointer\" width=\"9pt\" height=\"9pt\"/>")
//                .append("        <label id=\"btnDown").append(HtmlEncoder.encode(strId)).append("\" hflex=\"1\" style=\"padding: 0px 2px; font-size: 9pt; color:blue; cursor: pointer; white-space: nowrap;\" ")
//                .append("               onMouseOver='self.setStyle(\"color:lime; padding: 0px 2px; font-size: 9pt; cursor: pointer; white-space: nowrap;\"); self.setTooltiptext(self.value)' ")
//                .append("               onMouseOut='self.setStyle(\"color:blue; padding: 0px 2px; font-size: 9pt; cursor: pointer; white-space: nowrap;\")' ")
//                .append("               width=\"100%\" onClick=\"$composer.onViewFile(event)\"/>")
//                .append("    </hbox>")
//                .append("</fileDisplay>");
//        fileDisplay = (FileDisplay) Executions.createComponentsDirectly(strFileDisplay.toString(), "zul", null, null);
//        btnDown = (Label) fileDisplay.getChildren().get(0).getChildren().get(0).getNextSibling();
//        return fileDisplay;
//    }
//
//    //
//    public void onUploadFile(UploadEvent evt) {
//        try {
//            multiFileAttach = (MultiFileSelector) evt.getTarget().getParent().getParent();
//            FILE_EXTEND = multiFileAttach.getParent().getAttribute("FILE_EXTEND_STR").toString().split(";");
//            btnAddFile = (Button) evt.getTarget();
//            String strId = btnAddFile.getId().substring(10, btnAddFile.getId().length());
//            pnlMultiFile = (Vbox) btnAddFile.getNextSibling();
//            medias = evt.getMedias();
//            if (!validateFile(medias)) {
//                return;
//            }
//            if ((pnlMultiFile.getChildren().size() + medias.length) > 5) {
//                Clients.showNotification(LanguageBundleUtils.getMessage("policy.upload.max", "5"));
//                return;
//            }
//            if (medias.length > 0) {
//                for (Media media : medias) {
//                    String name = getCorrectFileName(media);
//                    FileDisplay display = createFileDisplay(strId);
//                    init(name, media);
//                    display.setData(media);
//                    display.setValue(name);
//                    display.setiFileDisplay(multiFileAttach);
//                    pnlMultiFile.appendChild(display);
//                }
//                multiFileAttach.invalidate();
//                pnlMultiFile.setVisible(true);
//            }
//        } catch (Exception ex) {
//            logger.error(ex.getMessage(), ex);
//        }
//    }
//
//    private boolean isCorrectExtension(String extension) {
//        boolean test = false;
//        for (int i = 0; i < FILE_EXTEND.length; i++) {
//            if (FILE_EXTEND[i].trim().equalsIgnoreCase(extension)) {
//                test = true;
//                break;
//            }
//        }
//        return test;
//    }
//
//    private boolean validateFile(Media[] medias) {
//        if (medias == null) {
//            return false;
//        }
//        for (Media media : medias) {
//            String fileName = media.getName();
//            if (duplicate(fileName)) {
//                return false;
//            }
//            if (fileName.lastIndexOf(".") == -1) {
//                Clients.showNotification(LanguageBundleUtils.getString("policy.incorrect_extension3"), btnAddFile);
//                return false;
//            } else {
//                if (!isCorrectExtension(fileName.substring(fileName.lastIndexOf(".")))) {
//                    Clients.showNotification(LanguageBundleUtils.getString("policy.incorrect_extension3"), btnAddFile);
//                    return false;
//                }
//            }
//            if (media.getByteData().length > 15420 * 1024) {
//                Clients.showNotification(LanguageBundleUtils.getString("policy.file.outOfSize"), btnAddFile);
//                return false;
//            }
//        }
//        return true;
//    }
//
//    public String getCorrectFileName(Media data) {
//        return FileUtils.getSafeFileName(data.getName()).replaceAll(",", "-");
//    }
//
//    public boolean init(String fileName, Media data) {
//        btnDown.setValue(fileName);
//        media = data;
//        value = fileName;
//        return true;
//    }
//
//    //Check trung file
//    public boolean duplicate(String name) {
//        List<Component> coms = pnlMultiFile.getChildren();
//        String fileNew = name.replaceAll(",", "-");
//        for (Component com : coms) {
//            if (com instanceof FileDisplay) {
//                FileDisplay file = (FileDisplay) com;
//                if (file.getValue().contains(fileNew)) {
//                    Clients.showNotification(LanguageBundleUtils.getString("policy.upload.duplicate"), com);
//                    return true;
//                }
//            }
//        }
//        return false;
//    }
//
//    public void onDeleteFile(Event evt) {
//        List<Component> lstComponent = evt.getTarget().getParent().getParent().getParent().getChildren();
//        if (lstComponent != null && !lstComponent.isEmpty()) {
//            //Neu con 1 file thi an luon danh sach file
//            if (lstComponent.size() == 1) {
//                pnlMultiFile = (Vbox) evt.getTarget().getParent().getParent().getParent();
//                clear();
//            } else {
//                FileDisplay fileSelected = (FileDisplay) evt.getTarget().getParent().getParent();
//                if (!lstComponent.isEmpty()) {
//                    //Start fix sonar
////                    int size = lstComponent.size();
////                    for (int i = 0; i < size; ++i) {
////                        FileDisplay f = (FileDisplay) lstComponent.get(i);
////                        if (fileSelected.getValue().trim().equals(f.getValue().trim())) {
////                            f.detach();
////                            --i;
////                            --size;
////                        }
////                    }
//                    for (Component fdp : lstComponent) {
//                        FileDisplay f = (FileDisplay) fdp;
//                        if (fileSelected.getValue().trim().equals(f.getValue().trim())) {
//                            f.detach();
//                            break;
//                        }
//                    }
//                    //End fix
//                }
//            }
//        }
//    }
//
//    //Xoa toan bo cac file
//    private void clear() {
//        List<Component> coms = pnlMultiFile.getChildren();
//        int n = coms.size();
//        for (int i = 0; i < n; i++) {
//            pnlMultiFile.removeChild(coms.get(0));
//        }
//        pnlMultiFile.setVisible(false);
//    }
//
//    //Xem file
//    public void onViewFile(Event event) {
//        String fileName = ((Label) event.getTarget()).getValue().trim();
//        String directory = LanguageBundleUtils.getString(ParamUtils.CONFIG_RESROUCE_FILE, "PATH_FILE_POLICY");
//        try {
//            List<MediaDTO> lstMedia = new ArrayList();
//            MediaDTO mediaDTO = new MediaDTO();
//            if (fileName != null && fileName.length() > 0) {
//                mediaDTO.setAbsoluteFilePath(directory + fileName);
//                mediaDTO.setFileName(fileName);
//                File file = new File(mediaDTO.getAbsoluteFilePath());
//                if (file.exists()) {
//                    AMedia amedia = new AMedia(file, "application/pdf", null);
//                    mediaDTO.setMedia(amedia);
//                    lstMedia.add(mediaDTO);
//                } else {
//                    mediaDTO = new MediaDTO();
//                    List<Component> coms = pnlMultiFile.getChildren();
//                    for (Component com : coms) {
//                        if (com instanceof FileDisplay) {
//                            FileDisplay fileView = (FileDisplay) com;
//                            if (fileView.getValue().equals(fileName)) {
//                                mediaDTO.setMedia(fileView.getData());
//                                mediaDTO.setFileName(fileView.getValue());
//                                lstMedia.add(mediaDTO);
//                            }
//                        }
//                    }
//                }
//            }
//            if (lstMedia.isEmpty()) {
//                Clients.showNotification(LanguageBundleUtils.getString("policy.detail.notFileExist"), "warning", null, "middle_center", 3000);
//                return;
//            }
//            Map arg2 = new HashMap();
//            arg2.put("AMEDIA_LIST", lstMedia);
//            final Window win = (Window) Executions.createComponents("/controls/catalogue/policyDetail/pdfViewerPopup.zul", null, arg2);
//            win.doModal();
//        } catch (Exception ex) {
//            logger.error(ex.getMessage(), ex);
//        }
//    }
//}
