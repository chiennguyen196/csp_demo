/*
 * Copyright (C) 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.viettel.csp.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.impl.InputElement;

/**
 * @author binhnt22@viettel.com.vn
 * @since May 2012
 * @version 1.1
 */
public final class StringUtils {

    private static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(StringUtils.class);

    /**
     * alphabeUpCaseNumber.
     */
    private static String alphabeUpCaseNumber = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    private static String mask = "0123456789_aAáÁàÀảẢạẠãÃâÂấẤầẦẩẨậẬẫẪăĂắẮằẰẳẲặẶẵẴbBcCdDđĐeEéÉèÈẻẺẹẸẽẼêÊếẾềỀểỂệỆễỄfFgGhHiIíÍìÌỉỈịIHĩĨjJkKlLmMnNoOóÓòÒỏỎọỌõÕôÔốỐồỒổỔộỘỗỖơƠớỚờỜởỞợỢỡỠpPqQrRsStTuUúÚùÙủỦụỤũŨưƯứỨừỪửỬựỰữỮvVwWxXyYýÝỳỲỷỶỵỴỹỸzZ";
    private static String maskVN = "áÁàÀảẢạẠãÃâÂấẤầẦẩẨậẬẫẪăĂắẮằẰẳẲặẶẵẴđĐéÉèÈẻẺẹẸẽẼêÊếẾềỀểỂệỆễỄíÍìÌỉỈịĩĨóÓòÒỏỎọỌõÕôÔốỐồỒổỔộỘỗỖơƠớỚờỜởỞợỢỡỠúÚùÙủỦụỤũŨưƯứỨừỪửỬựỰữỮýÝỳỲỷỶỵỴỹỸ";
    private static String maskEN = "0123456789_aAbBcCdDeEfFgGhHiIjJkKlLmMnNoOpPqQrRsStTuUvVwWxXyYzZ";
    /**
     * ZERO.
     */
    private static final String ZERO = "0";
    private static final String c[] = {"<", ">"};
    private static final String expansion[] = {"&lt;", "&gt;"};

    public static String HTMLEncode(String s) {
        for (int j = 0; j < c.length; j++) {
            s = s.replace(c[j], expansion[j]);
        }
        return s;
    }

    public static String HTMLDecode(String s) {
        String mine = s;
        for (int i = 0; i < c.length; i++) {
            mine = mine.replaceAll(expansion[i], (c[i] + ""));  //Fix sonar
        }
        return mine;
    }

    /**
     * method compare two string
     *
     * @param str1 String
     * @param str2 String
     * @return boolean
     */
    public static boolean compareString(String str1, String str2) {
        if (str1 == null) {
            str1 = "";
        }
        if (str2 == null) {
            str2 = "";
        }

        if (str1.equals(str2)) {
            return true;
        }
        return false;
    }

    /**
     * method convert long to string
     *
     * @param lng Long
     * @return String
     * @throws abc Exception
     */
    public static String convertFromLongToString(Long lng) throws Exception {
        return Long.toString(lng);
    }

    /*
     * @todo: convert from Long array to String array
     */
    public static String[] convertFromLongToString(Long[] arrLong) throws Exception {
        String[] arrResult = new String[arrLong.length];
        for (int i = 0; i < arrLong.length; i++) {
            arrResult[i] = convertFromLongToString(arrLong[i]);
        }
        return arrResult;
    }

    /*
     * @todo: convert from String array to Long array
     */
    public static long[] convertFromStringToLong(String[] arrStr) throws Exception {
        long[] arrResult = new long[arrStr.length];
        for (int i = 0; i < arrStr.length; i++) {
            arrResult[i] = Long.parseLong(arrStr[i]);
        }
        return arrResult;
    }

    /*
     * @todo: convert from String value to Long value
     */
    public static long convertFromStringToLong(String value) throws Exception {
        return Long.parseLong(value);
    }


    /*
     * Check String that containt only AlphabeUpCase and Number Return True if
     * String was valid, false if String was not valid
     */
    public static boolean checkAlphabeUpCaseNumber(String value) {
        boolean result = true;
        for (int i = 0; i < value.length(); i++) {
            String temp = value.substring(i, i + 1);
            if (alphabeUpCaseNumber.indexOf(temp) == -1) {
                result = false;
                return result;
            }
        }
        return result;
    }

    public static boolean isValidString(Object temp) {
        if (temp == null || "".equals(temp.toString().trim())) {
            return false;
        }
        return true;
    }

    public static boolean maskVN(String str) {
        if (!isValidString(str)) {
            return false;
        }
        str = str.trim();
        for (int i = 0; i < str.length(); i++) {
            if (mask.indexOf(str.charAt(i)) < 0) {
                return false;
            }
        }
        return true;
    }

    public static boolean isValidVN(String str) {
        if (!isValidString(str)) {
            return false;
        }
        str = str.trim();
        for (int i = 0; i < str.length(); i++) {
            if (maskVN.indexOf(str.charAt(i)) >= 0) {
                return true;
            }
        }
        return false;
    }

    public static boolean maskEN(String str) {
        if (!isValidString(str)) {
            return false;
        }
        str = str.trim();
        for (int i = 0; i < str.length(); i++) {
            if (maskEN.indexOf(str.charAt(i)) < 0) {
                return false;
            }
        }
        if (str.toLowerCase().charAt(0) < 'a' || str.toLowerCase().charAt(0) > 'z') {
            return false;
        }
        return true;
    }

    public static boolean isValidLong(String str) {
        if (str == null || !str.matches("[0-9]+$")) {
            return false;
        }
        return true;
    }

    public static boolean isValidInteger(String str) {
        if (str == null || !str.matches("[0-9]+$")) {
            return false;
        }
        return true;
    }

    public static String formatString(String str) {
        return " '" + str.trim().toLowerCase() + "'";
    }

    public static String formatLike(String str) {
        return "%" + str.trim().toLowerCase().replaceAll("_", "\\\\_") + "%";
    }

    public static String formatOrder(String str, String direction) {
        if (str == null || "".equals(str)) {
            return null;
        }
        return " NLSSORT(" + str + ",'NLS_SORT=vietnamese') " + direction;
    }

    public static String formatDate(Date date) {
        return DateTimeUtils.convertDateToString(date, ParamUtils.ddMMyyyy);
    }

    public static String formatDateTime(Date date) {
        return DateTimeUtils.convertDateToString(date, ParamUtils.dateTimeFormat);
    }

    public static String formatFunction(String function, String str) {
        return " " + function + "(" + str + ") ";
    }

    public static String formatConstant(String str) {
        String str1 = "";
        int index = 0;
        String alphabe = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        for (int i = 1; i < str.length(); i++) {
            if (alphabe.indexOf(str.charAt(i)) > 0) {
                str1 = str1 + str.substring(index, i).toUpperCase() + "_";
                index = i;
            }
        }
        str1 = str1 + str.substring(index, str.length()).toUpperCase() + "_";
        return str1;
    }

    public static boolean isValidDouble(String str) {
        try {
            Double tmp = Double.valueOf(str);
            if (tmp != null) {
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            return false;
        }
    }

    public static Long getLong(Object obj) {
        try {
            return Long.valueOf(obj.toString());
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            return null;
        }
    }

    public static Integer getInt(Object obj) {
        try {
            return Integer.valueOf(obj.toString());
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            return null;
        }
    }

    public static Double getDouble(Object obj) {
        try {
            return Double.valueOf(obj.toString());
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            return null;
        }
    }

    public static boolean containSpecialCharacteristic(String str) {
        if (str == null) {
            return false;
        }
        List lstSpecialCharacteristic = new ArrayList<String>();
        lstSpecialCharacteristic.add("!");
        lstSpecialCharacteristic.add("@");
        lstSpecialCharacteristic.add("#");
        lstSpecialCharacteristic.add("%");
        lstSpecialCharacteristic.add("^");
        lstSpecialCharacteristic.add("&");
        lstSpecialCharacteristic.add("*");
        lstSpecialCharacteristic.add("(");
        lstSpecialCharacteristic.add(")");
        lstSpecialCharacteristic.add(" ");
        for (int i = 0; i < lstSpecialCharacteristic.size(); i++) {
            if (str.contains(lstSpecialCharacteristic.get(i).toString())) {
                return true;
            }
        }
        return false;
    }

    public static boolean isValidPhone(String str) {
        if (!isValidString(str)) {
            return false;
        }
        str = str.trim();
        if (str == null || !str.matches("[0-9]{9,15}$")) {
            return false;
        }
        return true;
    }

    public static boolean isValidEmail(String strEmail) {
        if (!isValidString(strEmail)) {
            return false;
        }
        strEmail = strEmail.trim();
        String emailPattern = "^[_a-z0-9-]+(\\.[_a-z0-9-]+)*@[a-z0-9-]+(\\.[a-z0-9-]+)*(\\.[a-z]{2,4})$";
        return strEmail.toLowerCase().matches(emailPattern);
    }

    public static boolean isValidUsername(String strUserName) {
        if (!isValidString(strUserName)) {
            return false;
        }
        strUserName = strUserName.trim();
        String usernamePattern = "^[a-z]+[a-z0-9_.]*$";
        return strUserName.toLowerCase().matches(usernamePattern);
    }

    public static boolean isValidExcellCellAddr(String strCellAddr) {
        if (!isValidString(strCellAddr)) {
            return false;
        }
        strCellAddr = strCellAddr.trim();
        String addrPattern = "[a-z]{1,3}[1-9]{1}[0-9]{0,5}";
        return strCellAddr.toLowerCase().matches(addrPattern);
    }

    public static boolean isValidDate(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        try {
            dateFormat.format(date);
            return true;
        } catch (Exception e) {
        }
        return false;
    }

    public static boolean isValidDateTime(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        try {
            dateFormat.format(date);
            return true;
        } catch (Exception e) {
        }
        return false;
    }

    public static boolean isValidUrl(String strUrl) {
        if (!isValidString(strUrl)) {
            return false;
        }
        strUrl = strUrl.trim();
        String urlPattern = "^(http|https|ftp)\\://([a-zA-Z0-9\\.\\-]+(\\:[a-zA-Z0-9\\.&amp;%\\$\\-]+)*@)*((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])|localhost|([a-zA-Z0-9\\-]+\\.)*[a-zA-Z0-9\\-]+\\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(\\:[0-9]+)*(/($|[a-zA-Z0-9\\.\\,\\?\\'\\\\\\+&amp;%\\$#\\=~_\\-]+))*$";
        return strUrl.matches(urlPattern);
    }

    public static boolean isValidEmailHost(String strUrl) {
        if (!isValidString(strUrl)) {
            return false;
        }
        strUrl = strUrl.trim();
        String urlPattern = "^([a-zA-Z0-9\\.\\-]+(\\:[a-zA-Z0-9\\.&amp;%\\$\\-]+)*@)*((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])|localhost|([a-zA-Z0-9\\-]+\\.)*[a-zA-Z0-9\\-]+\\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(\\:[0-9]+)*(/($|[a-zA-Z0-9\\.\\,\\?\\'\\\\\\+&amp;%\\$#\\=~_\\-]+))*$";
        return strUrl.matches(urlPattern);
    }

    public static boolean isValidIp(String strIp) {
        if (!isValidString(strIp)) {
            return false;
        }
        strIp = strIp.trim();
        String ipPattern = "^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$";
        return strIp.matches(ipPattern);
    }

    public static boolean isMatchPattern(String strInput, String pattern) {
        return strInput.matches(pattern);
    }

    public static String toString(Object obj) {
        return obj == null ? "" : obj.toString();
    }

    public static boolean isEqual(String left, String right) {
        left = left == null ? "" : left;
        right = right == null ? "" : right;
        return left.equals(right);
    }

    public static boolean isEqual(Long left, Long right) {
        boolean result = true;
        if (left == null) {
            if (right != null) {
                result = false;
            }
        } else {
            result = left.equals(right);
        }
        return result;
    }

    public static String getString(Object obj) {
        try {
            return obj != null ? String.valueOf(obj) : "";
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static Boolean isNullOrEmptyOrSpace(Object obj) {
        try {
            return (obj == null || obj.toString().equals("") || obj.toString().trim().equals("")) ? true : false;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static Object getParamLike(String name) {
        return isNullOrEmptyOrSpace(name) ? "%%" : "%" + name.trim().toLowerCase() + "%";
    }

    /**
     * <P>
     * Check is Integer or not</P>
     *
     * @param str String to check
     * @param str
     * @return
     * @boolean true if valid, false if not valid
     */
    public static boolean isInteger(String str) {
        if (str == null || !str.matches("[0-9]+$")) {
            return false;
        }
        return true;
    }

    /*
     * @author : bss_tuanna67
     * @desc :  validate value of combobox
     */
    public static boolean validComboValue(String val) {
        return !(val == null || String.valueOf(ParamUtils.DEFAULT_VALUE).equals(val));
    }

    /*
     * @author : bss_tuanna67
     * @desc :  validate value of combobox
     */
    public static boolean validComboValue(Long val) {
        return !(val == null || ParamUtils.DEFAULT_VALUE == val.intValue());
    }

    /*
     * @author : bss_tuanna67
     * @desc :  validate max length
     */
    public static boolean validMaxLength(String str, int maxLength) {
        return !(str != null && str.length() > maxLength);
    }

    /////////////////Validate zl controls//////////////////////////
    public static boolean checkMaxLength(InputElement element, int maxLength, Boolean isFocused, String... msgParams) {
        boolean isValid = true;
        if (element.getText().trim().length() > maxLength) {
            isValid = false;
            showNotification(element, isFocused, "ERR.MAX_LENGTH", msgParams);
        }
        return isValid;
    }

    public static boolean checkMinLength(InputElement element, int minLength, Boolean isFocused, String... msgParams) {
        boolean isValid = true;
        if (element.getText().trim().length() < minLength) {
            isValid = false;
            showNotification(element, isFocused, "ERR.MIN_LENGTH", msgParams);
        }
        return isValid;
    }

    public static boolean checkValidElement(InputElement element, Boolean isFocused, String... msgParams) {
        boolean isValid = true;
        if (!element.isValid()) {
            isValid = false;
            if (!(element instanceof Datebox)) {
                showNotification(element, isFocused, "ERR.NOT_VALID_DATA", msgParams);
            }
        }
        return isValid;
    }

    public static boolean checkRequired(InputElement element, Boolean isFocused, String... msgParams) {
        boolean isValid = true;
        if (!StringUtils.isValidString(element.getText().trim())) {
            isValid = false;
            showNotification(element, isFocused, "ERR.REQUIRED", msgParams);
        }
        return isValid;
    }

    public static boolean checkStartEndDate(Datebox startDate, Datebox endDate, Boolean isFocused, String... msgParams) {
        boolean isValid = true;
        if (startDate.getValue() != null && endDate.getValue() != null
                && startDate.getValue().after(endDate.getValue())) {
            isValid = false;
            showNotification(endDate, isFocused, "ERR.NOT_LESS_THAN", msgParams);
        }
        return isValid;
    }

    public static void showNotification(InputElement element, boolean isFocused, String errCode, String... msgParams) {
        if (element != null) {
            //Clients.showNotification(createMessage(errCode, msgParams), element);
            Clients.wrongValue(element, createMessage(errCode, msgParams));
            if (!isFocused) {
                element.setFocus(true);
            }
        } else {
            Clients.showNotification(createMessage(errCode, msgParams));
        }
    }

    //Create message from key in .properties file
    protected static String createMessage(String msg, String... args) {
        return LanguageBundleUtils.getString(msg, args);
    }

    public static Object getParamEquals(String name) {
        return isNullOrEmptyOrSpace(name) ? "" : name.toLowerCase();
    }
}
