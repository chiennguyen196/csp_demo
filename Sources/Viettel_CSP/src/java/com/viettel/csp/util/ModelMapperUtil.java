/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.util;

import com.viettel.csp.DTO.BaseCustomDTO;
import com.viettel.csp.DTO.DTOInterface;
import java.util.ArrayList;
import java.util.List;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;

/**
 *
 * @author GEM
 */
public class ModelMapperUtil {

    ModelMapper modelMapper = null;

    public ModelMapperUtil() {
        modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
    }

    public <objectMap extends Object> objectMap mapper(Object objectData, Class<objectMap> mapClass) {
        return (objectMap) modelMapper.map(objectData, mapClass);
    }
    
    public List<KeyValueBean> convertListFormToBean(List<? extends BaseCustomDTO> listForm) {
        List<KeyValueBean> lstBean = new ArrayList<KeyValueBean>();
        if (listForm != null) {
            for (DTOInterface form : listForm) {
                lstBean.add(form.toBean());
            }
        }
        return lstBean;
    }
}
