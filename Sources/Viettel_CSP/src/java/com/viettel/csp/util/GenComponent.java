package com.viettel.csp.util;

import com.lowagie.text.html.HtmlEncoder;
import com.viettel.csp.DTO.ComponentDTO;
import com.viettel.csp.DTO.ContactTypeDTO;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.*;

import java.util.*;

/**
 * Created by tiennv@gemvietnam.com
 */
public class GenComponent extends GenericForwardComposer<Component> {

    private static final String ATTR_VALUE_PATTERN = "VALUE_PATTERN";
    private static final String ATTR_DISPLAY_NAME = "DISPLAY_NAME";
    private static final String ATTR_REQUIRED = "REQUIRED";
    private List<Object[]> lstObjectForTree;
    private List<Object[]> lstObjectForListbox;
//    private StandardAttributeService standardAttributeService = SpringUtil.getBean("StandardAttributeService", StandardAttributeService.class);
//    private AppParamsService appParamsService = SpringUtil.getBean("AppParamsService", AppParamsService.class);
    private final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger("exceptionLogger");

    public void createCellLabel(String label, boolean isRequired, Component parent) throws Exception {
        StringBuilder strCell = new StringBuilder("<cell colspan=\"1\" width=\"150px\" align=\"right\">")
                .append("<label value=\"").append(HtmlEncoder.encode(label)).append("\" style=\"word-wrap: break-word;\"/>");
        if (isRequired) {
            strCell.append("<label value=\" (*)\" style=\"color:red\"></label>");
        }
        strCell.append("</cell>");
        Executions.createComponentsDirectly(strCell.toString(), "zul", parent, null);
    }

    public Bandbox createListbox(ComponentDTO standardAttributeDTO) throws Exception {
        Bandbox bandbox;
        String strId = standardAttributeDTO.getStandardAttributeId();
        int p = Integer.parseInt(standardAttributeDTO.getDefaultValue()); //truong hien thi tren giao dien
        String[] listHeader = standardAttributeDTO.getAttributeFormat().split(";");
        StringBuilder strBandbox = new StringBuilder()
                .append("<bandbox id =\"").append(strId).append("\" onClick=\"$composer.onSearchBbListGen(event)\" width=\"100%\" mold=\"rounded\" readonly=\"true\">")
                .append("    <custom-attributes BANDBOX_ID=\"").append(strId).append("\"/>")
                .append("    <custom-attributes POSITION_VIEW=\"").append(p).append("\"/>")
                .append("    <bandpopup width=\"800px\">")
                .append("        <hbox spacing=\"0\" align=\"center\">")
                .append("            <textbox id=\"txtListSearch").append(HtmlEncoder.encode(strId)).append("\" maxlength=\"50\" width=\"200px\" forward=\"onOK=$composer.onCustomListSearch()\"/>")
                .append("            <button id=\"btnListSearch").append(HtmlEncoder.encode(strId)).append("\" image=\"/images/search.png\" mold=\"trendy\" autodisable=\"self\" sclass=\"btn-nopadding\" onClick=\"$composer.onCustomListSearch()\"/>")
                .append("        </hbox>")
                .append("        <listbox id=\"lbxBbGen").append(HtmlEncoder.encode(strId)).append("\" vflex=\"1\" checkmark=\"true\" multiple=\"true\" mold=\"paging\" pageSize=\"10\">")
                .append("             <listhead>")
                .append("                 <listheader label=\"STT\" width=\"50px\"/>");
        for (int i = 1; i < listHeader.length; i++) {
            strBandbox.append("           <listheader label=\"").append(HtmlEncoder.encode(listHeader[i])).append("\"/>");
        }
        strBandbox.append("           </listhead>")
                .append("             <template name=\"model\">")
                .append("                 <listitem>")
                .append("                     <listcell>")
                .append("                         <label value=\"${forEachStatus.index+1}\"/>")
                .append("                     </listcell>");
        for (int k = 1; k < listHeader.length; k++) {
            strBandbox.append("               <listcell label =\"${each[").append(k).append("]}\" style=\"text-align:left;\"/>");
        }
        strBandbox.append("               </listitem>")
                .append("             </template>")
                .append("        </listbox>")
                .append("        <div align=\"center\">")
                .append("            <button id=\"btnChoice").append(HtmlEncoder.encode(strId)).append("\" label=\"${c:l('global.choice')}\" onClick=\"$composer.onMultiSelectBbListItemGen(event)\" mold=\"trendy\" image=\"/images/accept-icon.png\"/>")
                .append("        </div>")
                .append("    </bandpopup>")
                .append("</bandbox>");
        bandbox = (Bandbox) Executions.createComponentsDirectly(strBandbox.toString(), "zul", null, null);
        lstObjectForListbox = getLstItemListBox(standardAttributeDTO);
        Sessions.getCurrent().setAttribute(bandbox.getAttribute("BANDBOX_ID").toString(), lstObjectForListbox);
        return bandbox;
    }

    private List<Object[]> getLstItemListBox(ComponentDTO standardAttributeDTO) {
        List<Object[]> result = new ArrayList<>();
        if (ParamUtils.ReportDataType.LISTVALUE.equalsIgnoreCase(standardAttributeDTO.getAttributeType())) {
            String[] values = standardAttributeDTO.getInitValue().split("[|]");
            for (String value : values) {
                Object[] detail = value.split("[;]");
                if (detail != null) {
                    result.add(detail);
                }
            }
        } else {
            try {
//                String sql = standardAttributeDTO.getInitValue();
//                result = appParamsService.getDataBySQLForList(sql);
            } catch (Exception ex) {
                log.error(ex.getMessage(), ex);
            }
        }
        return result;
    }

    public Bandbox createBandbox(ComponentDTO standardAttributeDTO) throws Exception {
        Bandbox bandbox;
        String strId = standardAttributeDTO.getStandardAttributeId();
        int p = Integer.parseInt(standardAttributeDTO.getDefaultValue());
        String[] treeHeader = standardAttributeDTO.getAttributeFormat().split(";");
        StringBuilder strBandbox = new StringBuilder()
                .append("<bandbox id=\"").append(strId).append("\" onClick=\"$composer.onSearchBbTreeGen(event)").append("\" hflex=\"1\" mold=\"rounded\" readonly=\"true\">")
                .append("    <custom-attributes BANDBOX_ID=\"").append(strId).append("\"/>")
                .append("    <custom-attributes POSITION_VIEW=\"").append(p).append("\"/>")
                .append("    <bandpopup height=\"300px\">")
                .append("        <hbox spacing=\"0\" align=\"center\">")
                .append("            <textbox id=\"txtTreeSearch").append(HtmlEncoder.encode(strId)).append("\" maxlength=\"50\" width=\"200px\" forward=\"onOK=$composer.onCustomTreeSearch()\"/>")
                .append("            <button id=\"btnTreeSearch").append(HtmlEncoder.encode(strId)).append("\" image=\"/images/search.png\" mold=\"trendy\" autodisable=\"self\" sclass=\"btn-nopadding\" onClick=\"$composer.onCustomTreeSearch()\"/>")
                .append("        </hbox>")
                .append("        <tree id=\"treeBbGen").append(strId).append("\" vflex=\"1\" width=\"800px\" zclass=\"z-dottree\" checkmark=\"true\" multiple=\"true\">")
                .append("            <treecols sizable=\"true\">");
        for (int k = 0; k < treeHeader.length; k++) {
            if (k == 0) {
                strBandbox.append("      <treecol hflex=\"1\">")
                        .append("            <checkbox id=\"checkboxAll").append(HtmlEncoder.encode(strId)).append("\" label=\"").append(HtmlEncoder.encode(treeHeader[k])).append("\" onCheck=\"$composer.onCheckAllTreeitem()\"/>")
                        .append("        </treecol>");
            } else {
                strBandbox.append("      <treecol hflex=\"1\">")
                        .append("            <label value=\"").append(HtmlEncoder.encode(treeHeader[k])).append("\"/>")
                        .append("        </treecol>");
            }
        }
        strBandbox.append("          </treecols>")
                .append("            <template name=\"model\">")
                .append("                <treeitem open=\"false\" onOpen=\"$composer.onOpenTreeItemGen(event)\" checkable=\"${$composer.onCheckableTreeitem(each.data[5])}\">")
                .append("                    <treerow>");
        for (int k = 0; k < treeHeader.length; k++) {
            strBandbox.append("                        <treecell>")
                    .append("                              <label value=\"${each.data[").append(5 + k + 1).append("]}\" multiline=\"true\"/>")
                    .append("                          </treecell>");
        }
        strBandbox.append("                  </treerow>")
                .append("                </treeitem>")
                .append("            </template>")
                .append("        </tree>")
                .append("        <div align=\"center\"> ")
                .append("            <button id=\"btnChoice").append(HtmlEncoder.encode(strId)).append("\" label=\"${c:l('global.choice')}\" onClick=\"$composer.onMultiSelectBbTreeGen(event)\" mold=\"trendy\" image=\"/images/accept-icon.png\"/>")
                .append("        </div> ")
                .append("    </bandpopup>")
                .append("</bandbox>");
        bandbox = (Bandbox) Executions.createComponentsDirectly(strBandbox.toString(), "zul", null, null);
        if (ParamUtils.ReportDataType.TREEBOX.equalsIgnoreCase(standardAttributeDTO.getAttributeType().toUpperCase())) {
//            lstObjectForTree = appParamsService.getLstObjectBySQL(standardAttributeDTO.getInitValue(), null);
//            Sessions.getCurrent().setAttribute(bandbox.getAttribute("BANDBOX_ID").toString(), lstObjectForTree);
        }
        return bandbox;
    }

    //---------------------------------------- VIEW VOFFICE FILE
    public Component createVOfficeView(ComponentDTO standardAttributeDTO) throws Exception {
        String strId = standardAttributeDTO.getAttributeCode();
        StringBuilder strVOfficeView = new StringBuilder();
        strVOfficeView.append("")
                .append("<?component name=\"viewVOfficeFile\" class=\"com.viettel.plc.widget.ViewVOfficeFile\"?>")
                .append("<?taglib uri=\"http://www.zkoss.org/dsp/web/core\" prefix=\"c\"?>")
                .append("<cell id=\"").append(HtmlEncoder.encode(strId)).append("\">")
                .append("    <viewVOfficeFile id=\"viewVOfficeFile").append(HtmlEncoder.encode(strId)).append("\">")
                .append("        <hlayout id=\"hlVOffice").append(HtmlEncoder.encode(strId)).append("\" hflex=\"1\" width=\"100%\" class=\"z-label\">")
                .append("            <textbox id=\"txtVOffice").append(HtmlEncoder.encode(strId)).append("\" hflex=\"1\" width=\"100%\"/>")
                .append("            <button id=\"btnVOffice").append(HtmlEncoder.encode(strId)).append("\" class=\"buttonLink\" label=\"${c:l('global.view')}\" onClick=\"$composer.onVerifyAccountSSO(event)\"/>")
                .append("        </hlayout>")
                .append("    </viewVOfficeFile>")
                .append("</cell>");
        return Executions.createComponentsDirectly(strVOfficeView.toString(), "zul", null, null);
    }

    public void onVerifyAccountSSO(Event event) {
        String id = ((Textbox) event.getTarget().getPreviousSibling()).getText();
        if (!StringUtils.isValidString(id)) {
            Clients.wrongValue((Textbox) event.getTarget().getPreviousSibling(), LanguageBundleUtils.getMessage("ERR.REQUIRED", LanguageBundleUtils.getString("attribute.IDVOFFICE")));
            return;
        }
        Map<String, Object> argu = new HashMap();
        argu.put("ID", id);
        Window userSSO = (Window) Executions.createComponents("/controls/widget/verifyAccountSSO.zul", null, argu);
        userSSO.doModal();

    }

    public Component createComponent(ComponentDTO componentDTO) throws Exception {
        Component tmpComp = null;
        if (ParamUtils.ReportDataType.STRING.equalsIgnoreCase(componentDTO.getAttributeType())) {
            tmpComp = createTextbox(componentDTO);
        } else if (ParamUtils.ReportDataType.LONG.equalsIgnoreCase(componentDTO.getAttributeType())) {
            tmpComp = createLongbox(componentDTO);
        } else if (ParamUtils.ReportDataType.DOUBLE.equalsIgnoreCase(componentDTO.getAttributeType())) {
            tmpComp = createDoublebox(componentDTO);
        } else if (ParamUtils.ReportDataType.DATE.equalsIgnoreCase(componentDTO.getAttributeType())) {
            tmpComp = createDatebox(componentDTO);
        } else if (ParamUtils.ReportDataType.COMBOVALUE.equalsIgnoreCase(componentDTO.getAttributeType())) {
            tmpComp = createCombobox(componentDTO);
        } else if (ParamUtils.ReportDataType.COMBOBOX.equalsIgnoreCase(componentDTO.getAttributeType())) {
            tmpComp = createCombobox(componentDTO);
        } else if (ParamUtils.ReportDataType.CHECKBOX.equalsIgnoreCase(componentDTO.getAttributeType())) {
            tmpComp = createCheckbox(componentDTO);
        } else if (ParamUtils.ReportDataType.LISTBOX.equalsIgnoreCase(componentDTO.getAttributeType())
                || ParamUtils.ReportDataType.LISTVALUE.equalsIgnoreCase(componentDTO.getAttributeType())) {
            tmpComp = createListbox(componentDTO);
        } else if (ParamUtils.ReportDataType.TREEBOX.equalsIgnoreCase(componentDTO.getAttributeType())
                || ParamUtils.ReportDataType.TREEVALUE.equalsIgnoreCase(componentDTO.getAttributeType())) {
            tmpComp = createBandbox(componentDTO);
        } else if (ParamUtils.ReportDataType.IDVOFFICE.equalsIgnoreCase(componentDTO.getAttributeType())) {
            tmpComp = createVOfficeView(componentDTO);
        }
        return tmpComp;
    }

    /////////////Begin create controls
    private Textbox createTextbox(ComponentDTO standardAttributeDTO) {
        Textbox textBox = new Textbox();
        //textBox.setId(standardAttributeDTO.getAttributeCode());
        textBox.setId(standardAttributeDTO.getStandardAttributeId());
        textBox.setHflex("1");
        textBox.setMaxlength(Integer.parseInt(standardAttributeDTO.getAttributeLength()));
        textBox.setAttribute(ATTR_DISPLAY_NAME, standardAttributeDTO.getAttributeName());
        textBox.setAttribute(ATTR_REQUIRED, standardAttributeDTO.getIsRequired());
        if (StringUtils.isValidString(standardAttributeDTO.getValPattern())) {
            textBox.setAttribute(ATTR_VALUE_PATTERN, standardAttributeDTO.getValPattern());
        }
        if (standardAttributeDTO.getDefaultValue() != null) {
            textBox.setValue(standardAttributeDTO.getDefaultValue());
        }
        return textBox;
    }

    private Longbox createLongbox(ComponentDTO standardAttributeDTO) {
        Longbox longBox = new Longbox();
        //longBox.setId(standardAttributeDTO.getAttributeCode());
        longBox.setId(standardAttributeDTO.getStandardAttributeId());
        longBox.setHflex("1");
        longBox.setMaxlength(Integer.parseInt(standardAttributeDTO.getAttributeLength()));
        longBox.setAttribute(ATTR_DISPLAY_NAME, standardAttributeDTO.getAttributeName());
        longBox.setAttribute(ATTR_REQUIRED, standardAttributeDTO.getIsRequired());
        if (standardAttributeDTO.getDefaultValue() != null) {
            longBox.setValue(StringUtils.getLong(standardAttributeDTO.getDefaultValue()));
        }
        return longBox;
    }

    private Checkbox createCheckbox(ComponentDTO standardAttributeDTO) {
        Checkbox chkBox = new Checkbox();
        //chkBox.setId(standardAttributeDTO.getAttributeCode());
        chkBox.setId(standardAttributeDTO.getStandardAttributeId());
        chkBox.setHflex("1");
        //chkBox.setLabel(standardAttributeDTO.getAttributeName());
        chkBox.setAttribute(ATTR_DISPLAY_NAME, standardAttributeDTO.getAttributeName());
        chkBox.setAttribute(ATTR_REQUIRED, standardAttributeDTO.getIsRequired());
        return chkBox;
    }

    private Doublebox createDoublebox(ComponentDTO standardAttributeDTO) {
        Doublebox doubleBox = new Doublebox();
        //doubleBox.setId(standardAttributeDTO.getAttributeCode());
        doubleBox.setId(standardAttributeDTO.getStandardAttributeId());
        doubleBox.setHflex("1");
        doubleBox.setMaxlength(Integer.parseInt(standardAttributeDTO.getAttributeLength()));
        //doubleBox.setFormat(",###.###");
        doubleBox.setPlaceholder("######,##");
        doubleBox.setConstraint("no negative");
        doubleBox.setAttribute(ATTR_DISPLAY_NAME, standardAttributeDTO.getAttributeName());
        doubleBox.setAttribute(ATTR_REQUIRED, standardAttributeDTO.getIsRequired());
        if (standardAttributeDTO.getDefaultValue() != null) {
            doubleBox.setValue(StringUtils.getDouble(standardAttributeDTO.getDefaultValue()));
        }
        return doubleBox;
    }

    private Datebox createDatebox(ComponentDTO standardAttributeDTO) {
        Datebox dateBox = new Datebox();
        //dateBox.setId(standardAttributeDTO.getAttributeCode());
        dateBox.setId(standardAttributeDTO.getStandardAttributeId());
        dateBox.setHflex("1");
        dateBox.setAttribute(ATTR_DISPLAY_NAME, standardAttributeDTO.getAttributeName());
        dateBox.setAttribute(ATTR_REQUIRED, standardAttributeDTO.getIsRequired());
        if (standardAttributeDTO.getValPattern() != null) {
            dateBox.setFormat(standardAttributeDTO.getValPattern());
        } else {
            dateBox.setFormat(ParamUtils.ddMMyyyy);
        }
        return dateBox;
    }

    private Combobox createCombobox(ComponentDTO componentDTO) {
        List<KeyValueBean> lstAllItems = new ArrayList<KeyValueBean>();
        String defaultValue = componentDTO.getDefaultValue();
        String selectItemValue = componentDTO.getSelectItemValue();
        if (defaultValue != null) {
            lstAllItems.add(new KeyValueBean(ParamUtils.SELECT_NOTHING_VALUE_STR, defaultValue));
        }
        List<KeyValueBean> lstItems = getLstListitem(componentDTO.getAttributeType(), componentDTO.getInitValue());
        if (lstItems != null && !lstItems.isEmpty()) {
            lstAllItems.addAll(lstItems);
        }
        Combobox comboBox = new Combobox();
        comboBox.setId(componentDTO.getStandardAttributeId());
        comboBox.setMold("rounded");
        comboBox.setHflex("1");
        comboBox.setReadonly(true);
        comboBox.setAttribute(ATTR_DISPLAY_NAME, componentDTO.getAttributeName());
        comboBox.setAttribute(ATTR_REQUIRED, componentDTO.getIsRequired());
        Comboitem comboItem;
        Comboitem selectedItem = null;
        for (KeyValueBean bean : lstAllItems) {
            comboItem = new Comboitem();
            comboItem.setValue(bean.getKey());
            comboItem.setLabel(bean.getValue());
            comboItem.setParent(comboBox);
            if (selectItemValue != null && selectItemValue.equalsIgnoreCase(bean.getKey().toString())) {
                selectedItem = comboItem;
            }
        }
        if (selectedItem != null) {
            comboBox.setSelectedItem(selectedItem);
        } else {
            comboBox.setSelectedIndex(0);
        }
        return comboBox;
    }

    public List<KeyValueBean> getLstListitem(String type, String parValue) {
        List<KeyValueBean> result = null;
        if (ParamUtils.ReportDataType.COMBOVALUE.equalsIgnoreCase(type)) {
            String[] splitted = parValue.split(";");
            result = new ArrayList<KeyValueBean>();
            for (int i = 0; i < splitted.length - 1; i = i + 2) {
                result.add(new KeyValueBean(splitted[i].trim(), LanguageBundleUtils.getString(splitted[i + 1].trim())));
            }
        }
        return result;
    }

    public Object getDefaultValue(ComponentDTO standardAttributeDTO) throws Exception {
        Object result;
        if (ParamUtils.ReportDataType.LONG.equalsIgnoreCase(standardAttributeDTO.getAttributeType())) {
            result = StringUtils.getLong(standardAttributeDTO.getDefaultValue());
        } else if (ParamUtils.ReportDataType.DOUBLE.equalsIgnoreCase(standardAttributeDTO.getAttributeType())) {
            result = StringUtils.getDouble(standardAttributeDTO.getDefaultValue());
        } else if (ParamUtils.ReportDataType.DATE.equalsIgnoreCase(standardAttributeDTO.getAttributeType())) {
            String pattern = standardAttributeDTO.getValPattern() == null ? ParamUtils.ddMMyyyy : standardAttributeDTO.getValPattern();
            result = getDefaultDate(standardAttributeDTO.getDefaultValue(), pattern);
        } else {
            result = standardAttributeDTO.getDefaultValue();
        }
        return result;
    }

    private Date getDefaultDate(String defaultValue, String pattern) throws Exception {
        Date result = null;
        Date curDate = new Date();
        try {
            defaultValue = defaultValue.toLowerCase().trim();
            //Neu ngay thang la ngay cu the : 05/08/2014...
            try {
                result = DateTimeUtils.convertStringToTime(defaultValue, pattern);
            } catch (Exception ex) {
                log.info(ex.getMessage(), ex);
            }
            //Neu ngay thang ko phai la ngay cu the: sysdate + 1, sysdate, sysdate - 1...
            if (result == null && defaultValue.contains("sysdate")) {
                boolean isAdd = true;
                int operatorIdx;
                int addValue;
                //Check co +/- ngay thang ko
                if (defaultValue.contains("+M") || defaultValue.contains("-M")) {
                    if (defaultValue.contains("+")) {
                        operatorIdx = defaultValue.indexOf("+");
                    } else { //Neu tru ngay (sysdate - 1, sydate - 100 ...
                        operatorIdx = defaultValue.indexOf("-");
                        isAdd = false;
                    }
                    //Lay so ngay tru/cong
                    addValue = Integer.valueOf(defaultValue.substring(operatorIdx + 2).trim());
                    //addValue = getValidAddDate(curDate, isAdd, addValue);
                    addValue = isAdd ? addValue : (0 - addValue);

                    //Lay gia tri mac dinh
                    result = DateTimeUtils.addMonth(curDate, addValue);
                } else {
                    if (defaultValue.contains("+") || defaultValue.contains("-")) {
                        //Neu cong them ngay (sysdate + 1, sydate + 100 ...
                        if (defaultValue.contains("+")) {
                            operatorIdx = defaultValue.indexOf("+");
                        } else { //Neu tru ngay (sysdate - 1, sydate - 100 ...
                            operatorIdx = defaultValue.indexOf("-");
                            isAdd = false;
                        }
                        //Lay so ngay tru/cong
                        addValue = Integer.valueOf(defaultValue.substring(operatorIdx + 1).trim());
                        addValue = DateTimeUtils.getValidAddDate(curDate, isAdd, addValue);
                        addValue = isAdd ? addValue : (0 - addValue);

                        //Lay gia tri mac dinh
                        result = DateTimeUtils.addDate(curDate, addValue);
                    } else {
                        //Neu khong cong/tru ngay thang -> tra ve sysdate
                        result = curDate;
                    }
                }
            } else {
                result = result == null ? curDate : result;
            }
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            result = curDate;
        }
        return result;
    }

    public void setFocus(Component comp) {
        if (comp instanceof Datebox) {
            ((Datebox) comp).setFocus(true);
        } //Xy ly Long controls
        else if (comp instanceof Longbox) {
            ((Longbox) comp).setFocus(true);
        } //Xy ly Float controls
        else if (comp instanceof Doublebox) {
            ((Doublebox) comp).setFocus(true);
        } //Xy ly Textbox controls
        else if (comp instanceof Textbox && !(comp instanceof Combobox)) {
            ((Textbox) comp).setFocus(true);
        } //Xu ly Combo controls
        else if (comp instanceof Listbox) {
            ((Listbox) comp).setFocus(true);
        } else if (comp instanceof Combobox) {
            ((Combobox) comp).setFocus(true);
        }
    }

    public String getValue(Component comp) throws Exception {
        String result = null;
        if (comp instanceof Doublebox) {
            //result = ((Doublebox) comp).getText();
            if (((Doublebox) comp).getValue() != null) {
                result = ((Doublebox) comp).getValue().toString().replace(",", ".");
            }
        } else if (comp instanceof Longbox) {
            result = ((Longbox) comp).getText();
        } else if (comp instanceof Datebox) {
            result = DateTimeUtils.convertDateToString(((Datebox) comp).getValue());
        } else if (comp instanceof Combobox) {
//            result = ((Combobox) comp).getSelectedItem().getValue().toString();//comment tam lay id
            result = ((Combobox) comp).getValue().toString();
        } else if (comp instanceof Checkbox) {
            result = ((Checkbox) comp).getValue();
        } else if (comp instanceof Textbox) {
            result = ((Textbox) comp).getText().trim();
        } else if (comp instanceof Bandbox) {
            if (comp.getChildren().get(0).getChildren().get(0) instanceof Tree) {
                result = ((Bandbox) comp).getText().trim();
            } else if (comp.getChildren().get(0).getChildren().get(0) instanceof Listbox) {
                result = ((Bandbox) comp).getText().trim();
            }
        }
        return result;
    }

    public void setValue(Component comp, String value, String patterm) throws Exception {
        if (comp instanceof Doublebox) {
            if (StringUtils.isValidString(value)) {
                //((Doublebox) comp).setValue(Double.valueOf(value));
                ((Doublebox) comp).setValue(Double.parseDouble(value.replace(",", ".")));
            } else {
                ((Doublebox) comp).setValue(null);
            }
        } else if (comp instanceof Longbox) {
            if (StringUtils.isValidString(value)) {
                ((Longbox) comp).setValue(Long.parseLong(value));
            } else {
                ((Longbox) comp).setValue(null);
            }
        } else if (comp instanceof Datebox) {
            if (!StringUtils.isValidString(value)) {
                patterm = ParamUtils.ddMMyyyy;
            }
            if (StringUtils.isValidString(value)) {
                ((Datebox) comp).setValue(DateTimeUtils.convertStringToTime(value, patterm));
            } else {
                ((Datebox) comp).setValue(null);
            }
        } else if (comp instanceof Combobox) {
            setSelectedCbo((Combobox) comp, value);
        } else if (comp instanceof Checkbox) {
            //((Checkbox) comp).setValue(value);
            if (StringUtils.isValidString(value)) {
                ((Checkbox) comp).setChecked(true);
            } else {
                ((Checkbox) comp).setChecked(false);
            }
        } else if (comp instanceof Bandbox) {
            ((Bandbox) comp).setValue(value);
            if (value != null && value.trim().length() > 0 && comp.getChildren().get(0).getChildren().get(0) instanceof Listbox) {
                Listbox listBox = (Listbox) comp.getChildren().get(0).getChildren().get(0);
                int position = getPosition(listBox);
                ListModel listModel = listBox.getModel();
                int size = ((ListModelList) listModel).size();
                for (int i = 0; i < size; i++) {
                    Object[] item = (Object[]) ((ListModelList) listModel).get(i);
                    if (item != null && item[position - 1].equals(value)) {
                        ((ListModelList) listModel).addToSelection(item);
                        break;
                    }
                }
                listBox.setModel(listModel);
            }
        } else if (comp instanceof Textbox) {
            ((Textbox) comp).setValue(value);
        }
    }

    public void setSelectedCbo(Combobox cbo, String value) {
        if (value == null || "".equals(value)) {
            cbo.setSelectedItem(cbo.getItems().get(0));
            return;
        } else {
            for (Comboitem item : cbo.getItems()) {
                if (item.getValue().toString().equals(value)) {
                    cbo.setSelectedItem(item);
                    return;
                }
            }
        }
    }

    //ham lay thu tu truong hien thi
    public int getPosition(Listbox listbox) {
        int position = -1;
//        String id = listbox.getId().substring(3);
//        ComponentDTO standardAttributeDTO = new ComponentDTO();
//        standardAttributeDTO.setAttributeCode(id);
//        standardAttributeDTO.setStatus(Constants.POLICY.ACTIVE);
//        String positionStr = standardAttributeService.search(standardAttributeDTO).get(0).getDefaultValue();
//        if (positionStr != null && positionStr.length() > 0) {
//            position = Integer.parseInt(positionStr.trim());
//        }
        return position;
    }

    //Lay du lieu tu cac controls dua vao doi tuong
    public Map<Object, Object> getDataFromControls(List<Component> lstInput) {
        Map<Object, Object> mapDataGetter = new HashMap<>();
        try {
            String value;
            for (Component comp : lstInput) {
                value = getValue(comp);
                if (value != null) {
                    if (comp instanceof Combobox) {
//                        Long val = -5L;
//                        try {
//                            val = Long.valueOf(value.toString());
//                        } catch (Exception e) {
//                            log.error(e.getMessage(), e);
//                        }
//                        if (val > -4 && val < 0) {
//                            continue;
//                        }
                        Combobox combobox = (Combobox) comp;
                        if (combobox.getSelectedItem() != null && combobox.getSelectedItem().getValue() != null) {
                            value = StringUtils.getString(combobox.getSelectedItem().getValue());
                        }
                    }
                    if (((Object) value) instanceof Date) {
                        mapDataGetter.put(comp, new java.sql.Date(((Date) ((Object) value)).getTime()));
                    } else {
                        mapDataGetter.put(comp, value);
                    }
                }
            }
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
        return mapDataGetter;
    }

    public void bindDataCbbContactType(Combobox cbb, List<KeyValueBean> listKeyValueBean, String valueSelect) throws Exception {
        try {
            ListModelList<KeyValueBean> lmlKeyValueBean = new ListModelList<>();
            listKeyValueBean.add(0, new KeyValueBean(ParamUtils.DEFAULT_VALUE_STR, LanguageBundleUtils.getString("global.combobox.choose")));
            lmlKeyValueBean = new ListModelList(listKeyValueBean);
            int i = 0;
            if (StringUtils.isValidString(valueSelect)) {
                for (KeyValueBean itemParam : lmlKeyValueBean.getInnerList()) {
                    if (itemParam.getKey() != null && String.valueOf(itemParam.getKey()).equals(valueSelect)) {
                        lmlKeyValueBean.addToSelection(lmlKeyValueBean.getElementAt(i));
                        i++;
                        break;
                    }
                }
            } else {
                lmlKeyValueBean.addToSelection(lmlKeyValueBean.getElementAt(0));
            }
            cbb.setModel(lmlKeyValueBean);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public void bindDataCbbContactType(Combobox cbb, List<KeyValueBean> listKeyValueBean, String valueSelect, boolean flag) throws Exception {
        try {
            ListModelList<KeyValueBean> lmlKeyValueBean = new ListModelList<>();
            if (flag == false){
                listKeyValueBean.add(0, new KeyValueBean(ParamUtils.DEFAULT_VALUE_STR, LanguageBundleUtils.getString("global.combobox.choose")));
            }
            lmlKeyValueBean = new ListModelList(listKeyValueBean);
            int i = 0;
            if (StringUtils.isValidString(valueSelect)) {
                for (KeyValueBean itemParam : lmlKeyValueBean.getInnerList()) {
                    if (itemParam.getKey() != null) {
                        lmlKeyValueBean.addToSelection(lmlKeyValueBean.getElementAt(i));
                        i++;
                        break;
                    }
                }
            } else {
                lmlKeyValueBean.addToSelection(lmlKeyValueBean.getElementAt(0));
            }
            cbb.setModel(lmlKeyValueBean);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

}
