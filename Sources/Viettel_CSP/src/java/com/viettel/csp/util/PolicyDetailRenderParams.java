//package com.viettel.csp.util;
//
//import com.viettel.eafs.util.SpringUtil;
//import com.viettel.plc.DTO.catalogue.StandardAttributeDTO;
//import com.viettel.plc.entity.CustomUserToken;
//import com.viettel.plc.service.catalogue.PolicyDetailService;
//import org.zkoss.zk.ui.Component;
//import org.zkoss.zk.ui.Executions;
//import org.zkoss.zk.ui.Sessions;
//import org.zkoss.zk.ui.event.Event;
//import org.zkoss.zul.*;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
///**
// * Created by tuannm33 on 10/13/2016.
// */
//public class PolicyDetailRenderParams implements ListitemRenderer<Object> {
//
//    private GenCodeComponent genCodeComponent = new GenCodeComponent();
//    private GenComponent genComponent = new GenComponent();
//    private List<Component> lstInputCtrls = new ArrayList<>();
//    private PolicyDetailService policyDetailService = SpringUtil.getBean("PolicyDetailService", PolicyDetailService.class);
//
//    @Override
//    public void render(Listitem lstItem, Object t, int i) throws Exception {
//        Object[] item = (Object[]) t;
//        Map<String, Object> mapItem = (Map<String, Object>) item[0];
//        List<StandardAttributeDTO> lstVisibleAttr = (List<StandardAttributeDTO>) item[1];
//        lstInputCtrls = (List<Component>) item[2];
//        //STT
//        lstItem.appendChild(new Listcell((i + 1) + ""));
//        //Gen column fix
//        for (String keyFix : mapItem.keySet()) {
//            if ("POLICY_ID".equals(keyFix)) {
//                Listcell lcPolicyId = new Listcell();
//                lcPolicyId.setVisible(false);
//                Label lblPolicyId = new Label();
//                lblPolicyId.setValue(mapItem.get(keyFix) != null ? mapItem.get(keyFix).toString() : "");
//                lblPolicyId.setVisible(false);
//                lcPolicyId.appendChild(lblPolicyId);
//                lstItem.appendChild(lcPolicyId);
//            } else if ("POLICY_CODE".equals(keyFix)) {
//                Listcell lcPolicyCode = new Listcell();
//                lcPolicyCode.setStyle("text-align:left");
//                Label lblPolicyCode = new Label();
//                lblPolicyCode.setValue(mapItem.get(keyFix) != null ? mapItem.get(keyFix).toString() : "");
//                lcPolicyCode.appendChild(lblPolicyCode);
//                lstItem.appendChild(lcPolicyCode);
//            } else if ("POLICY_NAME".equals(keyFix)) {
//                Listcell lcPolicyName = new Listcell();
//                lcPolicyName.setStyle("text-align:left");
//                Label lblPolicyName = new Label();
//                lblPolicyName.setValue(mapItem.get(keyFix) != null ? mapItem.get(keyFix).toString() : "");
//                lcPolicyName.appendChild(lblPolicyName);
//                lstItem.appendChild(lcPolicyName);
//            } else if ("DOC_TYPE".equals(keyFix)) {
//                Listcell lcDtlDocType = new Listcell();
//                lcDtlDocType.setStyle("text-align:left");
//                Label lblDtlDocType = new Label();
//                lblDtlDocType.setValue(getDocTypeName(mapItem.get(keyFix) != null ? mapItem.get(keyFix).toString() : ""));
//                Label lblDtlDocTypeId = new Label();  //lay ra id Loai van ban
//                lblDtlDocTypeId.setValue(mapItem.get(keyFix) != null ? mapItem.get(keyFix).toString() : "");
//                lblDtlDocTypeId.setVisible(false);
//                lcDtlDocType.appendChild(lblDtlDocType);
//                lcDtlDocType.appendChild(lblDtlDocTypeId);
//                lstItem.appendChild(lcDtlDocType);
//            }
//
//        }
//        //Gen column dynamic
//        if (lstVisibleAttr != null && !lstVisibleAttr.isEmpty()) {
//            for (int j = 0; j < lstVisibleAttr.size(); j++) {
//                StandardAttributeDTO sa = lstVisibleAttr.get(j);
//                Listcell lcParams = new Listcell();
//                if (mapItem.keySet().contains(sa.getAttributeCode())) {
//                    String align;
//                    if (ParamUtils.ReportDataType.LONG.equalsIgnoreCase(sa.getAttributeType()) ||
//                            ParamUtils.ReportDataType.DOUBLE.equalsIgnoreCase(sa.getAttributeType())) {
//                        align = "right";
//                    } else if (ParamUtils.ReportDataType.DATE.equalsIgnoreCase(sa.getAttributeType())) {
//                        align = "center";
//                    } else {
//                        align = "left";
//                    }
//                    lcParams.setStyle("text-align:" + align);
//                    if (ParamUtils.ReportDataType.DOUBLE.equalsIgnoreCase(sa.getAttributeType())) {
//                        Label lblDouble = new Label();
//                        lblDouble.setValue(mapItem.get(sa.getAttributeCode()) != null ? mapItem.get(sa.getAttributeCode()).toString().replace(".", ",") : "");
//                        lcParams.appendChild(lblDouble);
//                    } else if (ParamUtils.ReportDataType.CHECKBOX.equalsIgnoreCase(sa.getAttributeType())) {
//                        Checkbox ckb = new Checkbox();
//                        if (mapItem.get(sa.getAttributeCode()) != null) {
//                            ckb.setChecked("1".equals(mapItem.get(sa.getAttributeCode()).toString()) ? true : false);
//                        }
//                        ckb.setDisabled(true);
//                        lcParams.appendChild(ckb);
//                    } else if (ParamUtils.ReportDataType.COMBOBOX.equals(sa.getAttributeType()) || ParamUtils.ReportDataType.COMBOVALUE.equals(sa.getAttributeType())) {
//                        if (mapItem.get(sa.getAttributeCode()) != null) {
//                            List<KeyValueBean> lstKeyValue = genComponent.getLstListitem(sa.getAttributeType(), sa.getInitValue());
//                            for (KeyValueBean keyValue : lstKeyValue) {
//                                if (keyValue.getKey() != null && keyValue.getKey().toString().equals(mapItem.get(sa.getAttributeCode()).toString())) {
//                                    Label lblCombo = new Label();
//                                    lblCombo.setValue(keyValue.getValue());
//                                    lcParams.appendChild(lblCombo);
//                                }
//                            }
//                        }
//                    } else if (ParamUtils.ReportDataType.ATTACHFILE.equalsIgnoreCase(sa.getAttributeType())) {
//                        if (mapItem.get(sa.getAttributeCode()) != null) {
//                            String[] strFiles = mapItem.get(sa.getAttributeCode()).toString().split("\r\n");
//                            for (int k = 0; k < strFiles.length; k++) {
//                                Label lblFile = new Label();
//                                lblFile.setStyle("color:blue; cursor:pointer;");
//                                lblFile.setValue(strFiles[k]);
//                                lblFile.setTooltiptext(strFiles[k]);
//                                lblFile.setSclass("myLabelOnMouseOver");
//                                lblFile.addEventListener("onClick", new org.zkoss.zk.ui.event.EventListener() {
//                                    public void onEvent(Event evt) {
//                                        genCodeComponent.onViewFile(evt);
//                                    }
//                                });
//                                if (k < strFiles.length - 1) {
//                                    lcParams.appendChild(lblFile);
//                                    Separator line = new Separator();
//                                    line.setBar(true);
//                                    lcParams.appendChild(line);
//                                } else {
//                                    lcParams.appendChild(lblFile);
//                                }
//                            }
//                        } else {
//                            Label lblFile = new Label();
//                            lblFile.setStyle("color:blue; cursor:pointer;");
//                            lblFile.setValue("");
//                            lblFile.setSclass("myLabelOnMouseOver");
//                            lcParams.appendChild(lblFile);
//                        }
//                    } else if (ParamUtils.ReportDataType.TREEBOX.equalsIgnoreCase(sa.getAttributeType())
//                            || ParamUtils.ReportDataType.TREEVALUE.equalsIgnoreCase(sa.getAttributeType())) {
//                        Label lblTree = new Label();
//                        lblTree.setValue(getNameTree(mapItem.get(sa.getAttributeCode()) != null ? mapItem.get(sa.getAttributeCode()).toString() : "", sa.getStandardAttributeId()));
//                        lcParams.appendChild(lblTree);
//                    } else if (ParamUtils.ReportDataType.LISTBOX.equalsIgnoreCase(sa.getAttributeType())
//                            || ParamUtils.ReportDataType.LISTVALUE.equalsIgnoreCase(sa.getAttributeType())) {
//                        Label lblListbox = new Label();
//                        lblListbox.setValue(getNameListbox(mapItem.get(sa.getAttributeCode()) != null ? mapItem.get(sa.getAttributeCode()).toString() : "", sa.getStandardAttributeId()));
//                        lcParams.appendChild(lblListbox);
//                    } else {
//                        Label lblPar = new Label();
//                        lblPar.setValue(mapItem.get(sa.getAttributeCode()) != null ? mapItem.get(sa.getAttributeCode()).toString() : "");
//                        lcParams.appendChild(lblPar);
//                    }
//                } else {
//                    Label lblPar = new Label();
//                    lblPar.setValue("");
//                    lcParams.appendChild(lblPar);
//                }
//                lstItem.appendChild(lcParams);
//            }
//            //Gen nut Nhap chi tiet
//            Listcell lcBtnDetail = new Listcell();
//            lcBtnDetail.setStyle("text-align: center;");
//            Button btnPolicyDetail = new Button();
//            btnPolicyDetail.setImage("/images/edit-icon.png");
//            if (mapItem.get("APP_POLICY_CODE") == null || !StringUtils.validString(mapItem.get("APP_POLICY_CODE").toString())) {
//                btnPolicyDetail.setVisible(false);
//            } else {
//                btnPolicyDetail.setVisible(btnDetailCondition(mapItem.get("POLICY_ID").toString(), mapItem.get("DOC_TYPE").toString()));
//                btnPolicyDetail.addEventListener("onClick", new org.zkoss.zk.ui.event.EventListener() {
//                    public void onEvent(Event event) {
//                        String policyId = ((Label) event.getTarget().getParent().getParent().getFirstChild().getNextSibling().getFirstChild()).getValue();
//                        String policyCode = ((Label) event.getTarget().getParent().getParent().getFirstChild().getNextSibling().getNextSibling().getFirstChild()).getValue();
//                        String policyName = ((Label) event.getTarget().getParent().getParent().getFirstChild().getNextSibling().getNextSibling().getNextSibling().getFirstChild()).getValue();
//                        String docTypeId = ((Label) event.getTarget().getParent().getParent().getFirstChild().getNextSibling().getNextSibling().getNextSibling().getNextSibling().getLastChild()).getValue();
//                        showPolicyDetailPopup(policyId, policyCode, policyName, docTypeId);
//                    }
//                });
//            }
//            lcBtnDetail.appendChild(btnPolicyDetail);
//            lstItem.appendChild(lcBtnDetail);
//        }
//    }
//
//    //get name for tree
//    public String getNameTree(String value, String idComp) {
//        String result = "";
//        for (Component component : lstInputCtrls) {
//            if (component.getId().equals(idComp) && component.getChildren().get(0).getChildren().get(0).getNextSibling() instanceof Tree) {
//                List<Object[]> lstObjectTree = (List<Object[]>) Sessions.getCurrent().getAttribute(idComp);
//                int p = Integer.parseInt(component.getAttribute("POSITION_VIEW").toString());
//                String[] arrVal = value.trim().split("\\|");
//                if (lstObjectTree != null && !lstObjectTree.isEmpty()) {
//                    int i = 0;
//                    for (String val : arrVal) {
//                        for (Object[] itemObj : lstObjectTree) {
//                            if (val.trim().equals(itemObj[0].toString().trim())) {
//                                if (i < arrVal.length - 1) {
//                                    result += itemObj[5 + p] + "|";
//                                } else {
//                                    result += itemObj[5 + p];
//                                }
//                                break;
//                            }
//                        }
//                        i++;
//                    }
//                }
//                break;
//            }
//        }
//        return result;
//    }
//
//    //get name for list
//    public String getNameListbox(String value, String idComp) {
//        String result = "";
//        for (Component component : lstInputCtrls) {
//            if (component.getId().equals(idComp) && component.getChildren().get(0).getChildren().get(0).getNextSibling() instanceof Listbox) {
//                List<Object[]> lstObjectListbox = (List<Object[]>) Sessions.getCurrent().getAttribute(idComp);
//                int p = Integer.parseInt(component.getAttribute("POSITION_VIEW").toString());
//                String[] arrVal = value.trim().split("\\|");
//                if (lstObjectListbox != null && !lstObjectListbox.isEmpty()) {
//                    int i = 0;
//                    for (String val : arrVal) {
//                        for (Object[] itemObj : lstObjectListbox) {
//                            if (val.trim().equals(itemObj[0].toString().trim())) {
//                                if (i < arrVal.length - 1) {
//                                    result += itemObj[p - 1] + "|";
//                                } else {
//                                    result += itemObj[p - 1];
//                                }
//                                break;
//
//                            }
//                        }
//                        i++;
//                    }
//                }
//                break;
//            }
//        }
//        return result;
//    }
//
//    //get name for doc type
//    public String getDocTypeName(String value) {
//        String docTypeName = "";
//        List<KeyValueBean> lstKeyValueDocType = (List<KeyValueBean>) Sessions.getCurrent().getAttribute("SESSION_DTL_DOC_TYPE");
//        for (KeyValueBean kv : lstKeyValueDocType) {
//            if (StringUtils.validString(kv.getValue()) && value.equals(kv.getKey())) {  //value trong app_params
//                docTypeName = kv.getValue();  //name trong app_params
//                break;
//            }
//        }
//        return docTypeName;
//    }
//
//    //An, hien nut Nhap chi tiet theo quyen User
//    public boolean btnDetailCondition(String policyId, String docTypeId) {
//        boolean isPrivilege = false;
//        CustomUserToken userToken = (CustomUserToken) Sessions.getCurrent().getAttribute(ParamUtils.USER_TOKEN);
//        List<Object[]> lstObjPrivilege = policyDetailService.checkPolicyDetailCondition(policyId, docTypeId);
//        if (lstObjPrivilege == null || lstObjPrivilege.isEmpty()) {
//            isPrivilege = false;
//        } else {
//            for (Object[] obj : lstObjPrivilege) {
//                if (obj == null || obj.length == 0) {
//                    isPrivilege = false;
//                } else {
//                    if (obj[5] != null) {
//                        String[] arrUsers = obj[5].toString().split(";");
//                        for (String user : arrUsers) {
//                            if (userToken.getLoginName().toLowerCase().equals(user.toLowerCase().trim())) {
//                                isPrivilege = true;
//                                break;
//                            }
//                        }
//                    }
//                }
//                if (isPrivilege) {
//                    break;
//                }
//            }
//        }
//        return isPrivilege;
//    }
//
//    //Show popup them, sua, xoa chi tiet
//    public void showPolicyDetailPopup(String policyId, String policyCode, String policyName, String docTypeId) {
//        Map<String, Object> argu = new HashMap();
//        argu.put("SELECTED_POLICY_ID", policyId);
//        argu.put("SELECTED_POLICY_CODE", policyCode.toUpperCase());
//        argu.put("SELECTED_POLICY_NAME", policyName);
//        argu.put("SELECTED_DOC_TYPE", docTypeId);
//        Window wd = (Window) Executions.createComponents("/controls/catalogue/policyDetail/policyDetailPopup.zul", null, argu);
//        wd.doModal();
//    }
//
//}
