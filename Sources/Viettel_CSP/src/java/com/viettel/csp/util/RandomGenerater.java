/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.util;

import java.util.Random;

/**
 *
 * @author TienNV@gemvietnam.com
 */
public class RandomGenerater {

    public static String generatePW() {
        return String.valueOf(generateChar(8));
    }

    public static char[] generateChar(int len) {
        String charsCaps = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String Chars = "abcdefghijklmnopqrstuvwxyz";
        String nums = "0123456789";
        String symbols = "!@#$%^&*()_+-=.,/';:?><~*/-+";
        String passSymbols = charsCaps + Chars + nums + symbols;
        Random rnd = new Random();
        char[] password = new char[len];
        for (int i = 0; i < len; i++) {
            password[i] = passSymbols.charAt(rnd.nextInt(passSymbols.length()));
        }
        System.out.println("random:" + password);
        return password;

    }
}
