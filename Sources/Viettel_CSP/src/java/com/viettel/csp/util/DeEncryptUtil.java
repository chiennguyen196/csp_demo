/*
 * Copyright 2011 Viettel Telecom. All rights reserved
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.viettel.csp.util;

import com.viettel.security.AppSecurity;
import com.viettel.security.ByteUtils;

/**
 *
 * @author sonnh27
 */
public class DeEncryptUtil {

    private static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(DeEncryptUtil.class);

    public static String encrypt(String input) {
        String result = null;
        try {
            if (input != null) {
                byte[] b = AppSecurity.encrypt0(input);
                result = ByteUtils.bytesToString(b);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            result = "";
        }
        return result;
    }

    public static String decrypt(String input) {
        String result = null;
        try {
            result = AppSecurity.decrypt(ByteUtils.stringToBytes(input));
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            result = "";
        }
        return result;
    }
}
