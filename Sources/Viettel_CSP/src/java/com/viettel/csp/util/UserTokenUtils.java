/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.util;

import com.viettel.logincsp.UserCredential;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;

/**
 *
 * @author GEM
 */
public class UserTokenUtils {

    public static Long getUserId() {
        return getCustomUserToken().getUserId();
    }

    public static String getFullName() {
        return getCustomUserToken().getFullName();
    }

    public static String getUserName() {
        return getCustomUserToken().getUserName();
    }

    public static String getEmail() {
        return getCustomUserToken().getEmail();
    }

    public static String getPersonType() {
        return getCustomUserToken().getPersonType();
    }

    public static Long getPartnerId() {
        return getCustomUserToken().getPartnerId();
    }

    public static UserCredential getCustomUserToken() {
        Session session = Sessions.getCurrent();
        return (UserCredential) session.getAttribute(ParamUtils.USER_TOKEN);
    }
}
