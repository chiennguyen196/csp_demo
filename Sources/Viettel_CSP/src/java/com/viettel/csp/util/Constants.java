/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.util;

/**
 *
 * @author BSS_KIENTT
 */
public class Constants {
    //public static final String BCCS_RATING ="BCCS_RATING";

    public static final String PROJECT_NAME = "BCCS_RATING";
    public static final String DEFAULT = "DEFAULT";
    public static final String CURRENT_PAGE = "currentPage";
    public static final String REPORT_OBJECT = "objReportObject";
    public static final String USER_TOKEN = "userToken";
    public static final String IS_EXPORT_EXCEL = "isExportExcel";
    public static final String IS_EXPORT_HTML = "isExportHTML";
    public static final String EXCEL_STREAM = "excelStream";
    public static final String SESSION_TIMEOUT = "sessionTimeout";
    public static final String RULE_OBJECT = "objRule";
    public static final String COPY_RULE_OBJECT = "objCopyRule";
    public static final String COPY_RULES_OF_EVENT_OBJECT = "objCopyRulesOfEvent";
    public static final String FIST_VALUE = "-1";
    public static final String UNKNOWN = "UNKNOWN";
    public static final String PLEASE_SELECT = "--Please select--";

    public final class SEQ_CONSTANTS {

        public final static String ruleMapSeq = "RT_RULE_MAP_SEQ";
        public final static String ruleMapDetailSeq = "RT_RULE_MAP_DETAIL_SEQ";
        public final static String ruleSeq = "RT_RULE_SEQ";
        public final static String actionSeq = "RT_ACTION_SEQ";
        public final static String expressionSeq = "RT_EXPRESSION_SEQ";
        public final static String subExpressionSeq = "RT_SUB_EXPRESSION_SEQ";
    }

    public final class CDRTYPE_CONSTANTS {

        public final static String SYSTEM = "SYSTEM";
    }

    public final class DATA_TYPE {

        public final static String INTEGER = "Integer";
        public final static String LONG = "Long";
        public final static String DOUBLE = "Double";
        public final static String DATE = "Date";
        public final static String STRING = "String";
        public final static String BOOLEAN = "Boolean";
    }

    public final class LOG_ACTION_TYPE {

        public final static String SELECT = "SELECT";
        public final static String INSERT = "INSERT";
        public final static String UPDATE = "UPDATE";
        public final static String DELETE = "DELETE";
        public final static String COPY = "COPY";
    }

    /**
     * constants group item for combobox on form
     */
    public static class GROUP_PARAM_CONSTANTS {

        public final static String DATA_TYPE = "DATA_TYPE";
        public final static String ACCOUNT_GROUP_TYPE = "ACCOUNT_GROUP_TYPE";
        public final static String IS_PARENT_ACCOUNT_TYPE = "IS_PARENT_ACCOUNT_TYPE";
        public final static String TYPE_PRICE_PLAN = "TYPE_PRICE_PLAN";
        public final static String CYCLE_UNIT = "CYCLE_UNIT";
        public final static String CALCULATION_PRICE = "CALCULATION_PRICE";
        public final static String CDR_PROP_TYPE = "CDR_PROP_TYPE";
        public final static String CDR_PROP_UNIT_TYPE = "CDR_PROP_UNIT_TYPE";
        public final static String SWITCHBOARD = "SWITCHBOARD";
        public final static String CDR_GROUP = "CDR_GROUP";
        public final static String BACKUP_STYLE = "BACKUP_STYLE";
        public final static String DOWNLOAD_TYPE = "DOWNLOAD_TYPE";
        public final static String ZONE_RATING_TYPE = "ZONE_RATING_TYPE";
        public final static String ACCOUNT_TYPE_TYPE = "ACCOUNT_TYPE_TYPE";
        public final static String NEW_BASIC_PRICE = "NEW_BASIC_PRICE";
        public final static String TER_BASIC_PRICE = "TER_BASIC_PRICE";
        public final static String NORMAL_BASIC_PRICE = "NORMAL_BASIC_PRICE";
        public final static String PROCESS_GROUP_NAME = "PROCESS_GROUP_NAME";
        public final static String CACHE_GROUP_NAME = "CACHE_GROUP_NAME";
        public final static String RECURRING_TYPE = "RECURRING_TYPE";
        public final static String RECURRING = "recurring";

    }
    public final static String EVENT_GROUP = "ONEOFF";

    public static class ConditionType {

        public static final String PROCESS_SAME_MONTH = "PROCESS_SAME_MONTH";
        public static final String PROCESS_SAME_DAY = "PROCESS_SAME_DAY";
        public static final String PROCESS_SAME_HOUR = "PROCESS_SAME_HOUR";
        public static final String DAY_HOUR = "DAY_HOUR";
        public static final String MON_DAY = "MON_DAY";
        public static final String PROCESS_IN_MONTH = "PROCESS_IN_MONTH";
        public static final String PROCESS_IN_DAY = "PROCESS_IN_DAY";
        public static final String LOAD_FILE = "LOAD_FILE";
        public static final String CUSTOM_SQL = "CUSTOM_SQL";
        public static final String START_SAME_HOUR = "SAME_HOUR";
        public static final String START_SAME_DAY = "SAME_DAY";
        public static final String START_SAME_MONTH = "SAME_MONTH";
    }

    public static class OrderConstants {
        public static final String ZK_SORT_NATURAL = "natural";
        public static final String ZK_SORT_ASC = "ascending";
        public static final String ZK_SORT_DESC = "descending";
        public static final String SORT_ASC = "asc";
        public static final String SORT_DESC = "desc";
    }
    
    public static class JobState {
        public static final String NEW = "0";
        public static final String WAITING = "1";
        public static final String RUNNING = "2";
        public static final String RETRYING = "3";
        public static final String ERROR = "4";
        public static final String COMPLETED = "5";
        public static final String UNKNOWN = "-99";
        public static final String NOLOGGING = "-100";
    }
    
    public static class AuditParamType {
        public static final Long PROCESS_CODE = 1l;
    }
    public static class POLICY {
        //tuydv1
        public static final String ACTIVE = "1";//dang hoat dong
        public static final String CHANGE = "2"; //tien trinh thay doi
        public static final String APPROVAL_WAIT = "3"; //doi duyet
        public static final String NO_ACTIVE = "0";
        public static final int SIZE_MAX =1000;

    }



}