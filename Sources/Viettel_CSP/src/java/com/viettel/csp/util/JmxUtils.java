///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.viettel.csp.util;
//
//import java.text.MessageFormat;
//import java.util.HashMap;
//import java.util.Map;
//import java.util.Set;
//import javax.management.InstanceNotFoundException;
//import javax.management.MBeanServerConnection;
//import javax.management.ObjectName;
//import javax.management.remote.JMXConnector;
//import javax.management.remote.JMXConnectorFactory;
//import javax.management.remote.JMXServiceURL;
//import jxl.common.Logger;
//
///**
// *
// * @author tuanna67
// */
//public class JmxUtils {
//
//    private static String jmxUrl = LanguageBundleUtils.getString(ParamUtils.CONFIG_RESROUCE_FILE, "jmxUrl");
//    private static String jmxUser = LanguageBundleUtils.getString(ParamUtils.CONFIG_RESROUCE_FILE, "jmxUser");
//    private static String jmxPass = LanguageBundleUtils.getString(ParamUtils.CONFIG_RESROUCE_FILE, "jmxPass");
//    private static String jmxNamePattern = LanguageBundleUtils.getString(ParamUtils.CONFIG_RESROUCE_FILE, "jmxNamePattern");
//    private static String jmxProcessPattern = LanguageBundleUtils.getString(ParamUtils.CONFIG_RESROUCE_FILE, "jmxProcessPattern");
//    private static String jmxProcessMaster = LanguageBundleUtils.getString(ParamUtils.CONFIG_RESROUCE_FILE, "jmxProcessMaster");
//    private static Logger logger = Logger.getLogger(JmxUtils.class);
//
//    static {
//        if (StringUtils.validString(jmxPass)) {
//            jmxPass = DeEncryptUtil.decrypt(jmxPass);
//        }
//    }
//
//    public static String sendJMXCommand(String action, String objName, String[] params) throws Exception {
//        JMXConnector jmxConnector = null;
//        String result = ParamUtils.SUCCESS;
//        try {
//            JMXServiceURL url = new JMXServiceURL(jmxUrl);
//            Map<String, String[]> env = new HashMap<String, String[]>();
//            String[] credentials = {jmxUser, jmxPass};
//            env.put(JMXConnector.CREDENTIALS, credentials);
//            try {
//                jmxConnector = JMXConnectorFactory.connect(url, env);
//            } catch (Exception e) {
//                logger.error(e.getMessage(), e);
//            }
//            if (jmxConnector != null) {
//                MBeanServerConnection conn = jmxConnector.getMBeanServerConnection();
//                ObjectName name = new ObjectName(objName);
//                result = (String) conn.invoke(name, action, params, null);
//            } else {
//                result = "CONNECT_FAIL";
//            }
//        } catch (Exception ex) {
//            result = "INVOKE_ERROR";
//            logger.error(ex.getMessage(), ex);
//        } finally {
//            if (jmxConnector != null) {
//                jmxConnector.close();
//            }
//        }
//        return result;
//    }
//
//    public static Object getAttribute(String jmxProcessName, String attrName) throws Exception {
//        JMXConnector jmxConnector = null;
//        Object result = null;
//        try {
//            JMXServiceURL url = new JMXServiceURL(jmxUrl);
//            Map<String, String[]> env = new HashMap<String, String[]>();
//            String[] credentials = {jmxUser, jmxPass};
//            env.put(JMXConnector.CREDENTIALS, credentials);
//            try {
//                jmxConnector = JMXConnectorFactory.connect(url, env);
//            } catch (Exception e) {
//                logger.error(e.getMessage(), e);
//            }
//            if (jmxConnector != null) {
//                MBeanServerConnection conn = jmxConnector.getMBeanServerConnection();
//                result = conn.getAttribute(new ObjectName(jmxProcessName), attrName);
//            }
//        } catch(InstanceNotFoundException inf){
//            result = null;
//            logger.error(inf.getMessage(), inf);
//        } catch (Exception ex) {
//            result = null;
//            logger.error(ex.getMessage(), ex);
//        } finally {
//            if (jmxConnector != null) {
//                jmxConnector.close();
//            }
//        }
//        return result;
//    }
//
//    public static boolean checkExist(String jmxProcessName) throws Exception {
//        JMXConnector jmxConnector = null;
//        boolean result = false;
//        try {
//            JMXServiceURL url = new JMXServiceURL(jmxUrl);
//            Map<String, String[]> env = new HashMap<String, String[]>();
//            String[] credentials = {jmxUser, jmxPass};
//            env.put(JMXConnector.CREDENTIALS, credentials);
//            try {
//                jmxConnector = JMXConnectorFactory.connect(url, env);
//            } catch (Exception e) {
//                logger.error(e.getMessage(), e);
//            }
//            if (jmxConnector != null) {
//                MBeanServerConnection conn = jmxConnector.getMBeanServerConnection();
//                ObjectName name = new ObjectName(jmxProcessName);
//                Set<ObjectName> lstMbean = conn.queryNames(name, null);
//                result = !lstMbean.isEmpty();
//            } else {
//                result = false;
//            }
//        } catch (Exception ex) {
//            result = false;
//            logger.error(ex.getMessage(), ex);
//        } finally {
//            if (jmxConnector != null) {
//                jmxConnector.close();
//            }
//        }
//        return result;
//    }
//
//    public static String getProcessUrl(String processType, String processCode) {
//        MessageFormat msgFormat = new MessageFormat(jmxProcessPattern);
//        Object[] arguments = new Object[2];
//        arguments[0] = processCode;
//        arguments[1] = processType;
//        return msgFormat.format(arguments);
//    }
//}
