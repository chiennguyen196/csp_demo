/*
 * Copyright (C) 2010 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.viettel.csp.util;

import java.text.MessageFormat;
import java.util.ResourceBundle;
import java.util.Locale;
import net.redhogs.cronparser.CronExpressionDescriptor;
import org.zkoss.util.Locales;
import org.zkoss.util.resource.Labels;

/**
 *
 * @author binhnt22@viettel.com.vn
 * @since Apr 12, 2010
 * @version 1.0
 */
public class LanguageBundleUtils {

    private static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(LanguageBundleUtils.class);

    private LanguageBundleUtils() {
    }
    /**
     * RESOURCE.
     */
    //private static final String RESOURCE = "com/viettel/config/ ";
    private static final String RESOURCE = "language";
    private static final String RESOURCEMESSAGE = "com/viettel/config/message/Message";
    /**
     * local.
     */
    private static Locale local = null;
    /**
     * languageRb.
     */
    private static ResourceBundle languageRb = null;
    private static ResourceBundle messageRb = null;

    /**
     * .
     */
    /**
     * getString
     *
     * @param key String
     * @return value
     */
    public static String getString(String key) {
        return Labels.getLabel(key) == null ? key : Labels.getLabel(key);
    }

    public static String getString(Locale locale, String key) {
        return getString(key);
    }

    public static String getStringMessage(Locale locale, String key) {
        return getString(key);
    }

    public static String getString(String key, String... args) {
        MessageFormat msgFormat = new MessageFormat(Labels.getLabel(key));
        Object[] arguments = new Object[args.length];
        int i = 0;
        for (Object obj : args) {
            String arg = (String) obj;
            try {
                arg = getString((String) obj);
            } catch (Exception e) {
                System.out.println(e.getMessage());
                logger.info(e.getMessage(), e);
            } finally {
                arguments[i++] = arg;
            }

        }
        String output = msgFormat.format(arguments);
        return output;
    }

    public static String getString(Locale locale, String key, String... args) {

        MessageFormat msgFormat = new MessageFormat(Labels.getLabel(key));
        Object[] arguments = new Object[args.length];
        int i = 0;
        for (Object obj : args) {
            String arg = (String) obj;
            try {
                arg = getString((String) obj);
            } catch (Exception e) {
                System.out.println(e.getMessage());
                logger.info(e.getMessage(), e);
            } finally {
                arguments[i++] = arg;
            }
        }
        String output = msgFormat.format(arguments);
        return output;
    }

    public static String createMessage(Locale locale, String divName, String key, String... args) {
        String output = getString(locale, key, args);
        output = divName + ParamUtils.SEPARATOR + output;
        return output;
    }

    public static String createMessage(String divName, String content) {
        return divName + ParamUtils.SEPARATOR + content;
    }

    public static String getMessage(Locale locale, String key, String... args) {
        MessageFormat msgFormat = new MessageFormat(Labels.getLabel(key));
        Object[] arguments = new Object[args.length];
        int i = 0;
        for (Object obj : args) {
            String arg = (String) obj;
            try {
                arg = getString((String) obj);
            } catch (Exception e) {
                System.out.println(e.getMessage());
                logger.info(e.getMessage(), e);
            } finally {
                arguments[i++] = arg;
            }

        }
        String output = msgFormat.format(arguments);
        return output;
    }

    public static String getMessage(String key, String... args) {
        MessageFormat msgFormat = new MessageFormat(getString(key));
        Object[] arguments = new Object[args.length];
        int i = 0;
        for (Object obj : args) {
            String arg = (String) obj;
            try {
                arg = getString((String) obj);
            } catch (Exception e) {
                System.out.println(e.getMessage());
                logger.info(e.getMessage(), e);
            } finally {
                arguments[i++] = arg;
            }
        }
        String output = msgFormat.format(arguments);
        return output;
    }

    public static String getString(String resourceName, String key) {
        try {
            ResourceBundle bundle = ResourceBundle.getBundle(resourceName);
            return bundle.getString(key);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            return null;
        }
    }

    public static String getConfigParam(String key) {
        return LanguageBundleUtils.getString(ParamUtils.CONFIG_RESROUCE_FILE, key);
    }

    public static String getCronDescription(String cronExpression) {
        String result = null;
        try {
            if(StringUtils.isValidString(cronExpression)){
                result = CronExpressionDescriptor.getDescription(cronExpression, Locales.getCurrent());
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return result;
    }
}
