/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.util;

import java.util.Date;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Doublebox;
import org.zkoss.zul.Longbox;
import org.zkoss.zul.impl.InputElement;

/**
 *
 * @author tuanna67
 */
public class CtrlValidator {

    //Validate textbox
    public static boolean checkValidBandbox(InputElement element, boolean isRequired, boolean isFocused, String elementName) {
        boolean isValid;
        isValid = checkValidElement(element, isFocused, elementName);
        if (isValid && isRequired) {
            isValid = checkRequired(element, isFocused, elementName);
        }
        if (isValid) {
            isValid = checkMaxLength(element, isFocused, elementName);
        }
        return isValid;
    }

    //Validate textbox
    public static boolean checkValidTextbox(InputElement element, boolean isRequired, boolean isFocused, String elementName) {
        boolean isValid;
        isValid = checkValidElement(element, isFocused, elementName);
        if (isValid && isRequired) {
            isValid = checkRequired(element, isFocused, elementName);
        }
        if (isValid) {
            isValid = checkMaxLength(element, isFocused, elementName);
        }
        return isValid;
    }

    //Validate combobox (valid element, required)
    public static boolean checkValidComboValue(Combobox element, boolean isRequired, boolean isFocused, String elementName) {
        boolean isValid;
        isValid = checkValidElement(element, isFocused, elementName);
        if (isValid && isRequired) {
            if (element.getSelectedItem() == null
                    || ParamUtils.DEFAULT_VALUE_STR.equals(element.getSelectedItem().getValue())) {
                isValid = false;
                showNotification(element, isFocused, "global.validate.notnull", elementName);
            }
        }
        return isValid;
    }

    //Validate longbox (required, maxLength, isValidLong, isNaviativeNumber)
    public static boolean checkValidLngBox(Longbox lngBox, boolean isRequired, boolean isFocused, String elementName) {
        boolean isValid;
        isValid = checkValidElement(lngBox, isFocused, elementName);
        if (isValid && isRequired) {
            isValid = checkRequired(lngBox, isFocused, elementName);
        }
        if (isValid) {
            isValid = checkMaxLength((InputElement) lngBox, isFocused, elementName);
        }
        if (isValid) {
            isValid = checkLong(lngBox, isFocused, elementName);
        }
        if (isValid) {
            isValid = checkNegativeNumber(lngBox, isFocused, elementName);
        }
        return isValid;
    }

    //Validate longbox (required, maxLength, isValidLong, isNaviativeNumber)
    public static boolean checkValidDblBox(Doublebox dblBox, boolean isRequired, boolean isFocused, String elementName) {
        boolean isValid;
        isValid = checkValidElement(dblBox, isFocused, elementName);
        if (isValid && isRequired) {
            isValid = checkRequired(dblBox, isFocused, elementName);
        }
        if (isValid) {
            isValid = checkMaxLength((InputElement) dblBox, isFocused, elementName);
        }
        if (isValid) {
            isValid = checkLong(dblBox, isFocused, elementName);
        }
        if (isValid) {
            isValid = checkNegativeNumber(dblBox, isFocused, elementName);
        }
        return isValid;
    }

    //Validate datebox (valid element, less than sysdate)
    public static boolean checkValidDateBox(Datebox dateBox, boolean isFocused, String elementName) {
        boolean isValid = true;
        if (!dateBox.isValid()) {
            isValid = false;
            showNotification(dateBox, isFocused, "global.validate.notformat.date.ddMMyyyy", elementName);
        } else if (dateBox.getValue() != null
                && DateTimeUtils.trimDate(dateBox.getValue()).after(DateTimeUtils.trimDate(new Date()))) {
            isValid = false;
            showNotification(dateBox, isFocused, "global.validate.notformat.date.less.sysdate", elementName);
        }
        return isValid;
    }
    //Validate double box

    public static boolean checkMaxLength(InputElement element, boolean isFocused, String elementName) {
        boolean isValid = true;
        if (element.getText().trim().length() > element.getMaxlength()) {
            isValid = false;
            showNotification(element, isFocused, "global.validate.error.max_leght", elementName, String.valueOf(element.getMaxlength()));
        }
        return isValid;
    }

    public static boolean checkLength(InputElement element, boolean isFocused, Long min, String elementName) {
        boolean isValid = true;
        if (element.getText().trim().length() > element.getMaxlength()
                && element.getText().trim().length() < min) {
            isValid = false;
            showNotification(element, isFocused, "global.validate.error.minmax_leght", elementName, String.valueOf(min), String.valueOf(element.getMaxlength()));
        }
        return isValid;
    }

    public static boolean checkLong(InputElement element, boolean isFocused, String elementName) {
        boolean isValid = true;
        if (StringUtils.isValidString(element.getText()) && !StringUtils.isValidLong(element.getText())) {
            isValid = false;
            showNotification(element, isFocused, "global.validate.notformat.number.integer", elementName);
        }
        return isValid;
    }

    public static boolean checkDouble(InputElement element, boolean isFocused, String elementName) {
        boolean isValid = true;
        if (StringUtils.isValidString(element.getText()) && !StringUtils.isValidDouble(element.getText())) {
            isValid = false;
            showNotification(element, isFocused, "global.validate.notformat.number", elementName);
        }
        return isValid;
    }

    public static boolean checkNegativeNumber(InputElement element, boolean isFocused, String elementName) {
        boolean isValid = true;
        if (StringUtils.isValidString(element.getText())
                && ((element instanceof Longbox && StringUtils.getLong(element.getText()) < 0)
                || (element instanceof Doublebox && ((Doublebox) element).getValue() < 0))) {
            isValid = false;
            showNotification(element, isFocused, "ERR.NEGATIVE_NUMBER", elementName);
        }
        return isValid;
    }

    public static boolean checkRequired(InputElement element, boolean isFocused, String elementName) {
        boolean isValid = true;
        if (!StringUtils.isValidString(element.getText().trim())) {
            isValid = false;
            showNotification(element, isFocused, "global.validate.notnull", elementName);
        }
        return isValid;
    }

    public static boolean checkValidElement(InputElement element, boolean isFocused, String elementName) {
        boolean isValid = true;
        if (!element.isValid()) {
            isValid = false;
            if (!(element instanceof Datebox)) {
                showNotification(element, isFocused, "global.validate.notformat.date.ddMMyyyy", elementName);
            }
        }
        return isValid;
    }

    public static boolean checkStartEndDate(Datebox startDate, Datebox endDate, boolean isFocused, String startName, String endName) {
        boolean isValid = true;
        Clients.clearWrongValue(startDate);
        Clients.clearWrongValue(endDate);
        if (startDate.getValue() != null && endDate.getValue() != null
                && startDate.getValue().after(endDate.getValue())) {
            isValid = false;
            showNotification(endDate, isFocused, "global.validate.notformat.date.compare", startName, endName);
        }
        return isValid;
    }

    public static boolean checkDateRange(Datebox startDate, Datebox endDate, long maxDiff, boolean isFocused, String startName, String endName) {
        boolean isValid = true;
        Clients.clearWrongValue(startDate);
        Clients.clearWrongValue(endDate);
        if (DateTimeUtils.dateDiff(startDate.getValue(), endDate.getValue()) > maxDiff) {
            isValid = false;
            showNotification(endDate, isFocused, "global.validate.notformat.date.compare.range", endName, startName, String.valueOf(maxDiff));
        }
        return isValid;
    }

    public static boolean checkValidUrl(InputElement element, boolean isFocused, String elementName) {
        boolean isValid = true;
        if (!StringUtils.isValidUrl(element.getText().trim())) {
            isValid = false;
            showNotification(element, isFocused, "global.validate.error.notformat.url", elementName);
        }
        return isValid;
    }

    public static boolean checkValidEmail(InputElement element, boolean isFocused, String elementName) {
        boolean isValid = true;
        if (!StringUtils.isValidEmail(element.getText().trim())) {
            isValid = false;
            showNotification(element, isFocused, "global.validate.notformat.email", elementName);
        }
        return isValid;
    }

    public static boolean checkValidUsername(InputElement element, boolean isFocused, String elementName) {
        boolean isValid = true;
        if (!StringUtils.isValidUsername(element.getText().trim())) {
            isValid = false;
            showNotification(element, isFocused, "global.validate.notformat.username", elementName);
        }
        return isValid;
    }

    public static boolean checkValidHost(InputElement element, boolean isFocused, String elementName) {
        boolean isValid = true;
        if (!StringUtils.isValidIp(element.getText().trim()) && !StringUtils.isValidEmailHost(element.getText().trim())) {
            isValid = false;
            showNotification(element, isFocused, "global.validate.error.notformat.ip", elementName);
        }
        return isValid;
    }

    public static boolean checkValidIP(InputElement element, boolean isFocused, String elementName) {
        boolean isValid = true;
        if (!StringUtils.isValidIp(element.getText().trim())) {
            isValid = false;
            showNotification(element, isFocused, "global.validate.error.notformat.ip", elementName);
        }
        return isValid;
    }

    public static boolean checkValidPhone(InputElement element, boolean isFocused, String elementName) {
        boolean isValid = true;
        if (!StringUtils.isValidPhone(element.getText().trim())) {
            isValid = false;
            showNotification(element, isFocused, "global.validate.error.notformat.phone", elementName);
        }
        return isValid;
    }

    public static boolean checkConfirmPassword(InputElement password, InputElement confirmPassword, boolean isFocused, String elementName) {
        boolean isValid = true;
        if (!password.getText().trim().equals(confirmPassword.getText().trim())) {
            isValid = false;
            showNotification(confirmPassword, isFocused, "global.validate.error.confirmPassNotTheSame");
        }
        return isValid;
    }

    public static void showNotification(InputElement element, boolean isFocused, String errCode, String... msgParams) {
        if (element != null) {
            Clients.wrongValue(element, LanguageBundleUtils.getString(errCode, msgParams));
            if (!isFocused) {
                element.setFocus(true);
            }
        } else {
            Clients.showNotification(LanguageBundleUtils.getString(errCode, msgParams));
        }
    }

    public static String getSelectedValue(Combobox cbo) {
        String result = null;
        if (cbo.getSelectedItem() != null && !ParamUtils.DEFAULT_VALUE_STR.equals(cbo.getSelectedItem().getValue())) {
            result = cbo.getSelectedItem().getValue().toString();
        }
        return result;
    }
}
