/*
 * Copyright (C) 2010 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.viettel.csp.composer.examples.signPDF.certificate;

import org.bouncycastle.ocsp.CertificateStatus;

/**
 * Interface for the OCSP Client.
 *
 * @author sonnt38@viettel.com.vn
 * @since 1.1
 * @version 1.1
 */
public interface OcspClient {

    /**
     * get an encoded byte array
     *
     * @return byte array
     */
    byte[] getEncoded();

    /**
     * check the current status of a certificate
     *
     * @return <CODE>CertificateStatus</CODE>
     */
    CertificateStatus checkOCSP();
}
