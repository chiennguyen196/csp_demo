/*
 * Copyright (C) 2010 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.viettel.csp.composer.examples.signPDF.certificate;

import java.security.KeyStore;
import java.security.cert.CRL;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Calendar;
import java.util.Collection;
import java.util.Enumeration;
import java.util.GregorianCalendar;

/**
 * this class provide utilities to verify certificate
 *
 * @author $sonnt38@viettel.com.vn
 * @since since_text
 * @version 1.0
 */
public class X509VerificationUtil {

    private X509VerificationUtil() {
    }

    /**
     * Verifies a single certificate against current time
     *
     * @param cert X509 certificate
     * @return <CODE>String</CODE> if invalid or <CODE>null</CODE> if successful
     */
    public static String verifyCertificate(X509Certificate cert) {
        return verifyCertificate(cert, null, null);
    }

    /**
     * Verifies a single certificate against particular time
     *
     * @param cert : X509 certificate
     * @param calendar : time to verify
     * @return <CODE>String</CODE> if invalid or <CODE>null</CODE> if successful
     */
    public static String verifyCertificate(X509Certificate cert, Calendar calendar) {
        return verifyCertificate(cert, null, calendar);
    }

    /**
     * Verifies a single certificate against particular CRLs at current time
     *
     * @param cert X509 certificate
     * @param crls the certificate revocation list
     * @return <CODE>String</CODE> if invalid or <CODE>null</CODE> if successful
     */
    public static String verifyCertificate(X509Certificate cert, Collection<CRL> crls) {
        return verifyCertificate(cert, crls, null);
    }

    /**
     * Verifies a single certificate.
     *
     * @param cert the certificate to verify
     * @param crls the certificate revocation list or <CODE>null</CODE> if no
     * CRL
     * @param calendar the date or <CODE>null</CODE> for the current date
     * @return a <CODE>String</CODE> with the error description or
     * <CODE>null</CODE> if no error
     */
    public static String verifyCertificate(X509Certificate cert, Collection<CRL> crls, Calendar calendar) {
        if (calendar == null) {
            calendar = new GregorianCalendar();
        }
        if (cert.hasUnsupportedCriticalExtension()) {
            return "Has unsupported critical extension";
        }
        try {
            cert.checkValidity(calendar.getTime());
        } catch (Exception e) {
            return e.getMessage();
        }
        if (crls != null) {
            for (CRL crl : crls) {
                if (crl.isRevoked(cert)) {
                    return "Certificate revoked";
                }
            }
        }
        return null;
    }

    /**
     * verify certificate against key store
     *
     * @param certificate : X509 certificate
     * @param keyStore : key store contains one or more certificate
     * @return <CODE>String</CODE> if fail or <CODE>null</CODE> if successful
     */
    public static String verifyCertificate(X509Certificate certificate, KeyStore keyStore) {
        Certificate[] chain = new Certificate[1];
        chain[0] = certificate;
        return verifyCertificates(chain, keyStore, null, null);
    }

    /**
     * verify the X509 certificate against the key store
     *
     * @param certificate X509 certificate
     * @param keyStore key store
     * @param crls CRLs
     * @param calendar particular time
     * @return <CODE>String</CODE> if invalid or <CODE>null</CODE> if successful
     */
    public static String verifyCertificate(X509Certificate certificate, KeyStore keyStore, Collection<CRL> crls, Calendar calendar) {
        Certificate[] chain = new Certificate[1];
        chain[0] = certificate;
        return verifyCertificates(chain, keyStore, crls, calendar);
    }

    /**
     * verify the certificate chain against a certificate at current time
     *
     * @param chain the certificate chain
     * @param externalCert the certificate to verify
     * @return <CODE>Object</CODE> if invalid or <CODE>null</CODE> if successful
     */
    public static Object[] verifyCertificates(Certificate[] chain, Certificate externalCert) {
        return verifyCertificates(chain, externalCert, null, null);
    }

    /**
     * verify the certificate chain against a certificate and CRLs
     *
     * @param chain the certificate chain
     * @param externalCert the external certificate
     * @param crls CRLs to check revocation
     * @return <CODE>Object</CODE> if invalid or <CODE>null</CODE> if successful
     */
    public static Object[] verifyCertificates(Certificate[] chain, Certificate externalCert, Collection<CRL> crls) {
        return verifyCertificates(chain, externalCert, crls, null);
    }

    /**
     * verify the certificate chain against a certificate at a particular time
     *
     * @param chain the certificate chain
     * @param externalCert the external certificate
     * @param calendar the time to check
     * @return <CODE>Object</CODE> if invalid or <CODE>null</CODE> if successful
     */
    public static Object[] verifyCertificates(Certificate[] chain, Certificate externalCert, Calendar calendar) {
        return verifyCertificates(chain, externalCert, null, calendar);
    }

    /**
     * verify the certificate chain with a external certificate and CRL at a
     * particular time
     *
     * @param chain the certificate chain
     * @param externalCert the external certificate
     * @param crls CRLs to check revocation
     * @param calendar time to check
     * @return <CODE>Object</CODE> if invalid or <CODE>null</CODE> if successful
     */
    public static Object[] verifyCertificates(Certificate[] chain, Certificate externalCert, Collection<CRL> crls, Calendar calendar) {
        for (int k = 0; k < chain.length; ++k) {
            X509Certificate cert = (X509Certificate) chain[k];
            /*
             * check the validity of certificate
             */
            String err = verifyCertificate(cert, crls, calendar);
            if (err != null) {
                return new Object[]{cert, err};
            }

            try {
                cert.verify(externalCert.getPublicKey());
                return null;
            } catch (Exception e) {
            }
            /*
             * check the chain if it is real chain
             */
            int j = k + 1;
            if (j < chain.length) {
                X509Certificate nextCert = (X509Certificate) chain[j];
                try {
                    cert.verify(nextCert.getPublicKey());
                } catch (Exception e) {
                    return new Object[]{null, "Invalid certificate chain"};
                }
            }
        }
        return new Object[]{null, "CA certificate can not verify user"};
    }

    /**
     * verify the certificate chain against the key store at current time
     *
     * @param chain certificate chain
     * @param keyStore key store
     * @return <CODE>String</CODE> if invalid or <CODE>null</CODE> if successful
     */
    public static String verifyCertificates(Certificate[] chain, KeyStore keyStore) {
        return verifyCertificates(chain, keyStore, null, null);
    }

    /**
     * verify the certificate chain against the key store
     *
     * @param chain certificate chain
     * @param keyStore key store
     * @param crls CRLs
     * @param calendar particular time
     * @return <CODE>String</CODE> if invalid or <CODE>null</CODE> if successful
     */
    public static String verifyCertificates(Certificate[] chain, KeyStore keyStore, Collection<CRL> crls, Calendar calendar) {
        try {
            for (Enumeration<String> aliases = keyStore.aliases(); aliases.hasMoreElements();) {
                String alias = aliases.nextElement();
                /*
                 * find the certificate entries in key store
                 */
                if (!keyStore.isCertificateEntry(alias)) {
                    continue;
                }
                X509Certificate caCertificate = (X509Certificate) keyStore.getCertificate(alias);
                Object[] results = verifyCertificates(chain, caCertificate, crls, calendar);
                /*if validate successfully, it will get here */
                if (results == null) {
                    return null;
                }

            }
            return "Chain verification fails";
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    /**
     * check a certificate chain if it is a real chain, at current time
     *
     * @param chain X509 certificate
     * @return <CODE>String</CODE> if fail or <CODE>null</CODE> if successful
     */
    public static String verifyCertificate(Certificate[] chain) {
        for (int i = 0; i < chain.length; ++i) {
            X509Certificate innercert = (X509Certificate) chain[i];
            String res = verifyCertificate(innercert);
            if (res != null) {
                return res;
            }

            int j = i + 1;
            if (j < chain.length) {
                X509Certificate cacert = (X509Certificate) chain[j];
                try {
                    innercert.verify(cacert.getPublicKey());
                } catch (Exception e) {
                    return e.getMessage();
                }
            }
        }
        return null;
    }
}
