/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.composer.contractManagement;

import com.viettel.csp.DTO.ComponentDTO;
import com.viettel.csp.DTO.ContactDTO;
import com.viettel.csp.DTO.ContactPointDTO;
import com.viettel.csp.DTO.ContactTypeDTO;
import com.viettel.csp.DTO.ContractDTO;
import com.viettel.csp.DTO.ContractFilesDTO;
import com.viettel.csp.DTO.ContractHisDTO;
import com.viettel.csp.DTO.PartnerDTO;
import com.viettel.csp.DTO.ServiceDTO;
import com.viettel.csp.composer.contract.*;
import com.viettel.csp.service.ContactPointService;
import com.viettel.csp.service.ContactService;
import com.viettel.csp.service.ContactTypeService;
import com.viettel.csp.service.ContractFilesService;
import com.viettel.csp.service.ContractHisService;
import com.viettel.csp.service.ContractService;
import com.viettel.csp.service.ContractStatusService;
import com.viettel.csp.service.PartnerService;
import com.viettel.csp.service.ServiceService;
import com.viettel.csp.util.CtrlValidator;
import com.viettel.csp.util.GenComponent;
import com.viettel.csp.util.KeyValueBean;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.util.StringUtils;
import com.viettel.csp.util.UserTokenUtils;
import com.viettel.eafs.util.SpringUtil;
import com.viettel.logincsp.AuthenticationService;
import com.viettel.plc.widget.FileInfo;
import com.viettel.plc.widget.IUploaderCallback;
import com.viettel.plc.widget.UploadComponent;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.ForwardEvent;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 * @author chitv@gemvietnam.com
 */
public class ContractManagerCreatPopupContractComposer extends
    GenericForwardComposer<Component> implements IUploaderCallback {

    final static org.apache.log4j.Logger logger = org.apache.log4j.Logger
        .getLogger(ContractManagerCreatPopupContractComposer.class);
    private final ContactTypeService contactTypeService = SpringUtil
        .getBean("contactTypeService", ContactTypeService.class);
    private final ContactService contactService = SpringUtil
        .getBean("contactService", ContactService.class);
    private final ContactPointService contactPointService = SpringUtil
        .getBean("contactPointService", ContactPointService.class);
    private AuthenticationService authenticationService = SpringUtil
        .getBean("authenticationService", AuthenticationService.class);
    private Label txtAddressHeadOffice, txtPartnerCode, txtAddressTradingOffice, txtCompanyNameShort, txtPhone, txtRepName, dbFilingDate, txtContractStatus;
    private Textbox txtContractNumber, txtCompanyName, txtHeadOfService;
    private Combobox cbbContractStatus, cbServiceCode, cboCtSignForm, cboContractType;
    private Textbox txtContenReason;
    private List<KeyValueBean> lstReason;
    private ListModelList<KeyValueBean> kvbGroupServiceTree;
    private ListModelList<KeyValueBean> kvbContractType;
    private List<KeyValueBean> lstServiceName;
    private ListModelList<KeyValueBean> kvbServiceName;
    private Combobox cbbReason;
    private Datebox dbStartDate, dbEndDate;
    private String action;
    private Long contractId;
    private ArrayList indexLst;
    private ContractDTO oldDTO;
    private ContactPointDTO contactPointDTO;
    private PartnerDTO partNerDTO;
    private ContractHisDTO contractHisDTO;
    private ContractService contractService = SpringUtil
        .getBean("contractService", ContractService.class);
    private PartnerService partnerService = SpringUtil
        .getBean("partnerService", PartnerService.class);
    private ContractStatusService contractStatusService = SpringUtil
        .getBean("contractStatusService", ContractStatusService.class);
    private ContractHisService contractHisService = SpringUtil
        .getBean("contractHisService", ContractHisService.class);
    private ContractFilesService contractFilesService = SpringUtil
        .getBean("contractFilesService", ContractFilesService.class);
    private ServiceService service = SpringUtil.getBean("serviceService", ServiceService.class);
    private ContractComposer contractComposer;
    private ContractPopupEditComposer contractPopupEditComposer;
    private Listbox lbxViewAddContract;
    private Grid gridSearch;
    private List<ContractFilesDTO> lstContractFiles = new ArrayList<>();
    private UploadComponent uploadComponent;
    private List<String> filesLocation;
    private List<FileInfo> filesInfo;
    private Rows rows;
    private Row lastRow;
    private Long totalRow = 0L;
    private GenComponent genComponent = new GenComponent();
    private List<Component> lstInputCbbContact = new ArrayList<>();
    private List<ContactTypeDTO> lstInputContactType = new ArrayList<>();
    private ContactPointDTO pointDTO;
    private ContractManagerPopupEditToCSPComposer contractManagementComposer;

    @Override
    public void uploadedFilesInfoCallback(List<FileInfo> filesInfo) {
        this.filesInfo = filesInfo;
        System.out.println(filesInfo);
    }

    public Listbox getLbxViewAddContract() {
        return this.lbxViewAddContract;
    }

    public void setLbxViewAddContract(Listbox lbxViewAddContract) {
        this.lbxViewAddContract = lbxViewAddContract;
    }

    @Override
    public void uploadedFilesLocationCallback(List<String> filesLocation) {
        this.filesLocation = filesLocation;
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        if (arg.containsKey(ParamUtils.COMPOSER)) {
            contractManagementComposer = (ContractManagerPopupEditToCSPComposer) arg.get(ParamUtils.COMPOSER);
        }
        if (this.arg.containsKey(ParamUtils.ACTION)) {
            this.action = this.arg.get(ParamUtils.ACTION).toString();
        }
        oldDTO = (ContractDTO) this.arg.get("LIST_CONTRACT");

        loadComboboxServiceCode(ParamUtils.DEFAULT_VALUE_STR);
        bindData(oldDTO);
        this.uploadComponent.setUploaderCallback(this);
    }

    private void bindData(ContractDTO contractDTO){
        this.txtContractNumber.setValue(contractDTO.getContractNumber());
        this.cbServiceCode.setValue(this.oldDTO.getServiceCode());
        this.cboContractType.setValue(this.oldDTO.getContractType());
        this.cboCtSignForm.setValue(this.oldDTO.getCtSignForm());
        this.dbStartDate.setValue(this.oldDTO.getStartDate());
        this.dbEndDate.setValue(this.oldDTO.getEndDate());
        this.txtHeadOfService.setValue(this.oldDTO.getHeadOfService());
    }

    public ContractDTO getDataFromClient() {
        this.oldDTO.setContractNumber(this.txtContractNumber.getValue().trim());
        if (this.cbServiceCode.getSelectedIndex() > 0) {
            this.oldDTO.setServiceCode(this.cbServiceCode.getSelectedItem().getValue());
        }
        if (this.cboContractType.getSelectedIndex() > 0) {
            this.oldDTO.setContractType(this.cboContractType.getSelectedItem().getValue());
        }
        if (this.cboCtSignForm.getSelectedIndex() > 0) {
            this.oldDTO.setCtSignForm(this.cboCtSignForm.getSelectedItem().getValue());
        }
        this.oldDTO.setStartDate(this.dbStartDate.getValue());
        this.oldDTO.setEndDate(this.dbEndDate.getValue());
        this.oldDTO.setHeadOfService(this.txtHeadOfService.getValue().trim());

        return this.oldDTO;
    }

    public void loadComboboxServiceCode(String valueSelect) throws Exception {
        try {
            List<ServiceDTO> listServiceDTO = this.service.findAll();
            this.lstServiceName = new ArrayList<>();
            this.lstServiceName.add(new KeyValueBean(ParamUtils.DEFAULT_VALUE_STR,
                LanguageBundleUtils.getString("global.combobox.choose")));
            if (listServiceDTO != null && listServiceDTO.size() > 0) {
                for (ServiceDTO serviceDTO : listServiceDTO) {
                    KeyValueBean key = new KeyValueBean();
                    key.setKey(serviceDTO.getCode());
                    key.setValue(serviceDTO.getName());
                    this.lstServiceName.add(key);
                }
            }
            this.kvbServiceName = new ListModelList(this.lstServiceName);
            int i = 0;
            for (KeyValueBean itemParam : this.kvbServiceName.getInnerList()) {
                if (itemParam.getKey() != null && String.valueOf(itemParam.getKey())
                    .equals(valueSelect)) {
                    this.kvbServiceName.addToSelection(this.kvbServiceName.getElementAt(i));
                    break;
                }
                i++;
            }
            this.cbServiceCode.setModel(this.kvbServiceName);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            Clients
                .showNotification(LanguageBundleUtils.getString("global.message.error"), "warning",
                    null, "middle_center", 3000);
        }
    }

    public void onClick$btnAddFilePopup() {
        try {
            Map<String, Object> argu = new HashMap();
            argu.put(ParamUtils.ACTION, ParamUtils.ACTION_CREATE);
            argu.put(ParamUtils.COMPOSER, this);
            Window wd;
            wd = (Window) Executions.createComponents(
                "controls/contractManagement/contractManagementAddPopupViettelB.zul", null, argu);
            wd.doModal();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    public void setDataGrid(ContractFilesDTO contractFilesDTO) {

        this.lstContractFiles.add(contractFilesDTO);
        ListModelList model = new ListModelList(this.lstContractFiles);

        model.setMultiple(true);
        this.lbxViewAddContract.setModel(model);
    }

    public String formatDateTime(Date date) throws Exception {
        return StringUtils.formatDateTime(date);
    }


    public void onClick$bntSendContractFile() {
        try {
            this.uploadComponent.onClick$doSave();
            String messageContract = "";
            String messageHis = "";
            int flat = 5;
            if (validate()) {
                ContractDTO contractDTO = getDataFromClient();
                ContractHisDTO contractHisDTO = getDataFromClientContractHisDTO();
                List<ContractFilesDTO> lstContractFilesDTO = new ArrayList<>();
                if (null == filesLocation || null == filesInfo) {
                    Clients.showNotification(
                        LanguageBundleUtils.getString("contractFiles.messageFiles.error"),
                        "warning", null, "middle_center", 3000);
                    return;
                }
                if (this.filesInfo != null) {
                    for (int i = 0; i < this.filesInfo.size(); i++) {
                        ContractFilesDTO filesDTO = getDataFromClientContractFilesDTO(
                            this.filesInfo.get(i));
                        lstContractFilesDTO.add(filesDTO);
                    }
                }

                if (lstContractFiles != null) {
                    contractDTO.setLstContractFilesDTOMenu(lstContractFiles);
                }
                contractDTO.setLstContractFilesDTO(lstContractFilesDTO);
                boolean result = this.contractService.rejectContract(contractDTO, contractHisDTO);
                if (result) {
                    Clients.showNotification(
                        LanguageBundleUtils.getString("global.message.create.successful"),
                        "suscess", null, "middle_center", 1000);
                    this.self.detach();
                    contractManagementComposer.setData();
                } else {
                    Clients
                        .showNotification(messageContract, "warning", null, "middle_center", 3000);
                }
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients
                .showNotification(LanguageBundleUtils.getString("global.message.error"), "warning",
                    null, "middle_center", 3000);
        }
    }

    public ContractHisDTO getDataFromClientContractHisDTO() {
        ContractHisDTO contractHisDTO = new ContractHisDTO();
        contractHisDTO.setCreateBy(UserTokenUtils.getUserId());
        contractHisDTO.setCreateAt(new Date());
        contractHisDTO.setStatusAfter(ParamUtils.CONTRACT_PARTNER.CONTRACTHIS_STATUS_BEFORE);
        contractHisDTO.setStatusBefore(ParamUtils.CONTRACT_PARTNER.CONTRACTHIS_STATUS_AFTER);
        return contractHisDTO;
    }


    private boolean validate() {
        if (!CtrlValidator.checkValidTextbox(this.txtContractNumber, true, true, this.action)) {
            return false;
        }
        if (!StringUtils.isValidDate(this.dbStartDate.getValue())) {
            Clients.wrongValue(this.dbStartDate, LanguageBundleUtils
                .getMessage("global.validate.notformat.date.ddMMyyyy",
                    LanguageBundleUtils.getString("contract.startDate")));
            return false;
        }
        if (!StringUtils.isValidDate(this.dbEndDate.getValue())) {
            Clients.wrongValue(this.dbEndDate, LanguageBundleUtils
                .getMessage("global.validate.notformat.date.ddMMyyyy",
                    LanguageBundleUtils.getString("contract.endDate")));
            return false;
        }
        return true;
    }


    public void onDownloadFileContract(ForwardEvent evt) {
        evt.stopPropagation();
        try {
            Image btn = (Image) evt.getOrigin().getTarget();
            Listitem litem = (Listitem) btn.getParent().getParent();
            onClick$btnDownloadFile(litem);
        } catch (Exception ex) {
//            java.util.logging.Logger.getLogger(ContractEditManagementType1.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onClick$btnDownloadFile(Listitem litem) {
        try {
            List<ContractFilesDTO> lstContractFilesResult = new ArrayList<>();
            lstContractFilesResult.add(litem.getValue());
            File file = new File(lstContractFilesResult.get(0).getPathFile());
            Filedownload.save(file, null);
        } catch (Exception ex) {
            Messagebox.show(LanguageBundleUtils.getMessage("messageUploadFileError"),
                LanguageBundleUtils.getMessage("Messagebox.warning"), Messagebox.OK,
                Messagebox.ERROR, null);
        }
    }

    public ContractFilesDTO getDataFromClientContractFilesDTO(FileInfo fileInfo) {
        ContractFilesDTO contractFilesDTO = new ContractFilesDTO();
        contractFilesDTO.setFileName(new File(fileInfo.getLocation()).getName());
        contractFilesDTO.setPathFile(new File(fileInfo.getLocation()).getPath());
        contractFilesDTO.setDescription(fileInfo.getDescription());
        contractFilesDTO.setFileType(ParamUtils.CONTRACT_PARTNER.CONTRACTFILESNAME);
        return contractFilesDTO;
    }

    public void onClick$btnViewFilePopup() {
        try {
            Map<String, Object> argu = new HashMap();
            argu.put(ParamUtils.ACTION, ParamUtils.ACTION_UPDATE_FILE);
            List<Integer> indexs = new ArrayList();
            indexs.add(lbxViewAddContract.getSelectedItem().getIndex());
            argu.put("LIST_ACTION_INDEX", indexs);
            argu.put("LISTBOX", lbxViewAddContract.getSelectedItem().getValue());
            argu.put(ParamUtils.COMPOSER, this);
            lstContractFiles.remove(lstContractFiles.get(lbxViewAddContract.getSelectedIndex()));
            lbxViewAddContract.removeItemAt(lbxViewAddContract.getSelectedIndex());
            Window wd;
            wd = (Window) Executions.createComponents(
                "controls/contractManagement/contractManagementAddPopupViettelB.zul", null, argu);
            wd.doModal();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    public void onClick$btnRemoveFilePopup() {
        try {
            if (lbxViewAddContract.getSelectedCount() > 0) {
                lstContractFiles
                    .remove(lstContractFiles.get(lbxViewAddContract.getSelectedIndex()));
                lbxViewAddContract.removeItemAt(lbxViewAddContract.getSelectedIndex());
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients
                .showNotification(LanguageBundleUtils.getString("global.message.error"), "warning",
                    null, "middle_center", 3000);
        }
    }
}

