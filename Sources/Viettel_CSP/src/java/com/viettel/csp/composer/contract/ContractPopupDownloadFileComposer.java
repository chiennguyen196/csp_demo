/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.composer.contract;

import com.viettel.csp.DTO.ContractFilesDTO;
import com.viettel.csp.DTO.ServiceDTO;
import com.viettel.csp.service.ServiceService;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.ParamUtils;
import com.viettel.eafs.util.SpringUtil;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.ForwardEvent;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Image;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

/**
 *
 * @author chitv@gemvietnam.com
 */
public class ContractPopupDownloadFileComposer extends GenericForwardComposer<Component> {

    private String action;
    private Listbox lbxContractFiles;
    private ServiceService serviceService = SpringUtil.getBean("serviceService", ServiceService.class);
    final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ContractPopupDownloadFileComposer.class);
    private ContractComposer contractComposer;

    public Listbox getLbxContractFiles() {
        return lbxContractFiles;
    }

    public void setLbxContractFiles(Listbox lbxContractFiles) {
        this.lbxContractFiles = lbxContractFiles;
    }

    @Override
    public void doAfterCompose(Component comp) {
        try {
            super.doAfterCompose(comp);
            if (arg.containsKey(ParamUtils.COMPOSER)) {
                contractComposer = (ContractComposer) arg.get(ParamUtils.COMPOSER);
            }
            if (arg.containsKey(ParamUtils.ACTION)) {
                action = arg.get(ParamUtils.ACTION).toString();
            }
            lbxContractFiles.setMultiple(true);
            setData();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    public void setData() throws Exception {
        if (ParamUtils.ACTION_CREATE.equals(action)) {
            ((Window) self).setTitle(LanguageBundleUtils.getString("global.edit"));
            bindDataToTabContractFiles();
        }
    }

    public void onDownloadFileContract(ForwardEvent evt) {
        evt.stopPropagation();
        try {
            Image btn = (Image) evt.getOrigin().getTarget();
            Listitem litem = (Listitem) btn.getParent().getParent();
            List<ServiceDTO> lstContractFilesResult = new ArrayList<>();
            lstContractFilesResult.add(litem.getValue());
            File file = new File(lstContractFilesResult.get(0).getPathFile());
            Filedownload.save(file, null);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(ContractComposer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onClick$btnDownloadFile(Listitem litem) {
        try {
            List<ContractFilesDTO> lstContractFilesResult = new ArrayList<>();
            lstContractFilesResult.add(litem.getValue());
            File file = new File(lstContractFilesResult.get(0).getPathFile());
            Filedownload.save(file, null);
        } catch (FileNotFoundException ex) {
            Messagebox.show(LanguageBundleUtils.getMessage("messageUploadFileError"), LanguageBundleUtils.getMessage("Messagebox.warning"), Messagebox.OK, Messagebox.ERROR, null);
        }
    }

    private void bindDataToTabContractFiles() {
        try {
            List<ServiceDTO> lstServiceResult = serviceService.getListServiceChild();
            if (lstServiceResult != null && !lstServiceResult.isEmpty()) {
                ListModelList model = new ListModelList(lstServiceResult);
                lbxContractFiles.setModel(model);
            } else {
                lbxContractFiles.setModel(new ListModelList());
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    public void onClick$btnClose() {
        try {
            self.detach();
            contractComposer.bindDataToGrid();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }
}
