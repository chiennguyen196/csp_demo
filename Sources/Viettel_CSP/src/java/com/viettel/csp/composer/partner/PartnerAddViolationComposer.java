package com.viettel.csp.composer.partner;

import com.viettel.csp.DTO.ContractBreachDisDTO;
import com.viettel.csp.service.ContractBreachDisService;
import com.viettel.csp.service.ContractViolationService;
import com.viettel.csp.util.GenComponent;
import com.viettel.eafs.util.SpringUtil;
import org.zkoss.idom.Text;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by GEM on 6/17/2017.
 */
public class PartnerAddViolationComposer extends GenericForwardComposer<Component> {

    private Combobox cbViolate;
    private Textbox txtHeadOfService;
    private Datebox startDateViolation;
    private Datebox endDateViolation;
    private Textbox txtContent;
    private Textbox txtContentValue;
    private Textbox txtNumberViolation;
    private Button addViolation;
    private Row rStartDate;
    private Row rEndDate;
    private Row rContent;
    private Row rContentValue;
    private Row rNumberViolation;
    private GenComponent component = new GenComponent();

    private ContractViolationService contractViolationService = SpringUtil.getBean("contractViolationService", ContractViolationService.class);
    private ContractBreachDisService contractBreachDisService = SpringUtil.getBean("contractBreachDisService", ContractBreachDisService.class);

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        binData();
    }

    private void binData() throws Exception {
        rContent.setVisible(false);
        rContentValue.setVisible(false);
        rNumberViolation.setVisible(false);
        //Dk
        startDateViolation.setValue(new Date());
        endDateViolation.setValue(new Date());

        setComboboxViolation(); // bang discipline
    }

    public void setComboboxViolation() throws Exception {
        List<String> stringList = new ArrayList<>();
        String value = null;
        stringList.add("Khóa đầu số/Dải số"); // 1
        stringList.add("Tạm dừng dịch vụ"); // 2
        stringList.add("Tạm dừng hợp đồng"); // 3
        stringList.add("Thanh lí hợp đồng"); // 4
        stringList.add("Phạt tiền"); // 5
        stringList.add("Gửi công văn nhắc nhở"); //6

        component.bindDataCbbContactType(cbViolate, contractViolationService.bindDataToCombobox(stringList), value);
    }

//    public void onClick$addViolation(){
//        if(validate() == false){
//            Clients.showNotification("Nhập đủ thông tin", "warn", null, "middle_center", 3000);
//            return;
//        }
//
//        long contractBreachId = (long)Sessions.getCurrent().getAttribute("idForAddViolation");
//        ContractBreachDisDTO contractBreachDisDTO = contractBreachDisService.createBreachDisDTO(cbViolate.getSelectedItem().getValue(), startDateViolation.getValue(),
//                endDateViolation.getValue(), txtContent.getValue(), txtContentValue.getValue(), contractBreachId);
//
//        contractBreachDisService.insertData(contractBreachDisDTO);
//    }

    private boolean validate(){
        if (cbViolate.getSelectedItem().getValue().equals("-99"))
            return false;

        int key = (int)cbViolate.getSelectedItem().getValue();
        if (txtHeadOfService.getValue().equals(""))
            return false;

        switch (key){
            case 5:
                if (txtContent.getValue().equals("") || txtContentValue.getValue().equals(""))
                    return false;
                break;
            case 6:
                if (txtContent.getValue().equals(""))
                    return false;
                break;
        }

        return true;
    }

    public void onSelect$cbViolate(){
        if (cbViolate.getSelectedItem().getValue().equals("-99"))
            return;

        setVisiableForRow();
    }

    private void setVisiableForRow(){
        if (cbViolate.getSelectedItem().getValue().equals("-99"))
            return;

        int key = (int)cbViolate.getSelectedItem().getValue();
        switch (key){
            case 6:
                rStartDate.setVisible(false);
                rEndDate.setVisible(false);
                rContent.setVisible(true);
                rContentValue.setVisible(false);
                rNumberViolation.setVisible(true);
                return;
            case 1:
            case 2:
            case 3:
                rStartDate.setVisible(true);
                rEndDate.setVisible(true);
                rContent.setVisible(false);
                rContentValue.setVisible(false);
                break;
            case 4:
                rStartDate.setVisible(true);
                rEndDate.setVisible(false);
                rContent.setVisible(false);
                rContentValue.setVisible(false);
                break;
            case 5:
                rStartDate.setVisible(false);
                rEndDate.setVisible(false);
                rContent.setVisible(true);
                rContentValue.setVisible(true);
                break;
            case -99:
                return;
        }
        rNumberViolation.setVisible(false);
    }
}
