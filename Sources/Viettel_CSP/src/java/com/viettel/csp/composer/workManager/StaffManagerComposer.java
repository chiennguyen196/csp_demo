/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.composer.workManager;

import com.viettel.csp.DTO.DepartmentDTO;
import com.viettel.csp.entity.DepartmentEntity;
import com.viettel.csp.entity.UserEntity;
import com.viettel.csp.service.DepartmentService;
import com.viettel.csp.service.UserService;
import com.viettel.csp.util.UserTokenUtils;
import com.viettel.eafs.util.SpringUtil;
import java.util.List;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;

public class StaffManagerComposer extends GenericForwardComposer<Component> {

    final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(StaffManagerComposer.class);
    DepartmentService departmentService = SpringUtil.getBean("departmentService", DepartmentService.class);
    UserService userService = SpringUtil.getBean("userService", UserService.class);

    Listbox lbxDepart;
    Listbox lbxStreff;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        UserEntity userEntity = this.userService.getUserByID(UserTokenUtils.getUserId());
        Long organizationId = userEntity.getOrganizationId();
        List<DepartmentDTO> departmentDTOS = this.departmentService.getAllsWithOrgnization(organizationId);
        if (departmentDTOS != null && !departmentDTOS.isEmpty()) {
            ListModelList model = new ListModelList(departmentDTOS);
            model.setMultiple(false);
            lbxDepart.setModel(model);
        } else {
            lbxDepart.setModel(new ListModelList());
        }
    }

}
