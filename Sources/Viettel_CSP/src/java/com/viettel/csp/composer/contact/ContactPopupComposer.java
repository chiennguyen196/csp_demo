package com.viettel.csp.composer.contact;

import com.viettel.csp.DTO.ContactDTO;
import com.viettel.csp.DTO.ContactTypeDTO;
import com.viettel.csp.DTO.ResultInsertContactDTO;
import com.viettel.csp.entity.MessageEntity;
import com.viettel.csp.service.ContactService;
import com.viettel.csp.service.ContactTypeService;
import com.viettel.csp.service.MessageService;
import com.viettel.csp.service.UserService;
import com.viettel.csp.util.*;
import com.viettel.eafs.util.SpringUtil;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.util.Clients;

/**
 * Created by tiennv@gemvietnam.com
 */
public class ContactPopupComposer extends GenericForwardComposer<Component> {

    private Textbox txtUserName;
    private Textbox txtContactName;
    private Textbox txtEmail;
    private Textbox txtFixedPhoneNumber;
    private Textbox txtMobilePhoneNumber;
    private Textbox txtDepartment;
    private Textbox txtPosition;
    private Textbox txtOtherInfo;
    private Combobox cbbSex;
    private Combobox cbbContactType;
    private Combobox cbbContactStatus;
    private Row rContactStatus;
    private Row rContactType;
    private Button btnSavePopup;
    private Button btnFixProfile;
    private ListModelList<KeyValueBean> kvbContactType;
    private List<KeyValueBean> lstContactType;
    private String action;
    private Long contactId;
    private ContactDTO oldDTO;
    private ContactService contactService = SpringUtil.getBean("contactService", ContactService.class);
    private UserService userService = SpringUtil.getBean("userService", UserService.class);
    private ContactTypeService contactTypeService = SpringUtil.getBean("contactTypeService", ContactTypeService.class);
    final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ContactPopupComposer.class);
    private ContactComposer contactComposer = new ContactComposer();
    private MessageService messageService=SpringUtil.getBean("messageService", MessageService.class);
    public ListModelList<KeyValueBean> getKvbContactType() {
        return kvbContactType;
    }

    public void setKvbContactType(ListModelList<KeyValueBean> kvbContactType) {
        this.kvbContactType = kvbContactType;
    }

    @Override
    public void doAfterCompose(Component comp) {
        try {
            super.doAfterCompose(comp);
            if (arg.containsKey(ParamUtils.COMPOSER)) {
                contactComposer = (ContactComposer) arg.get(ParamUtils.COMPOSER);
            }
            if (arg.containsKey(ParamUtils.ACTION)) {
                action = arg.get(ParamUtils.ACTION).toString();
            }
            if (arg.containsKey("CONTACT_ID") && StringUtils.isValidString(String.valueOf(arg.get("CONTACT_ID")))) {
                contactId = (Long) arg.get("CONTACT_ID");
            }
            setData();

            //Hidden combobox in ContactPopup for fix user information 
            String status = (String) Sessions.getCurrent().getAttribute("contactPopup");
            if (status != null && status.equals("ok")) {
                rContactStatus.setVisible(false);
                rContactType.setVisible(false);
                Sessions.getCurrent().removeAttribute("contactPopup");
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    public void bindDataCbbContactType(String valueSelect) throws Exception {
        try {
            List<ContactTypeDTO> lstContactTypeDTO = contactTypeService.findAll();
            lstContactType = new ArrayList<>();
            lstContactType.add(new KeyValueBean(ParamUtils.DEFAULT_VALUE_STR, LanguageBundleUtils.getString("global.combobox.choose")));
            if (lstContactTypeDTO != null && lstContactTypeDTO.size() > 0) {
                for (ContactTypeDTO contactTypeDTO : lstContactTypeDTO) {
                    KeyValueBean key = new KeyValueBean();
                    key.setKey(contactTypeDTO.getCode());
                    key.setValue(contactTypeDTO.getName());
                    lstContactType.add(key);
                }
            }
            kvbContactType = new ListModelList(lstContactType);
            int i = 0;
            for (KeyValueBean itemParam : kvbContactType.getInnerList()) {
                if (itemParam.getKey() != null && String.valueOf(itemParam.getKey()).equals(valueSelect)) {
                    kvbContactType.addToSelection(kvbContactType.getElementAt(i));
                    break;
                }
                i++;
            }
            cbbContactType.setModel(kvbContactType);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    public void setData() throws Exception {
        if (ParamUtils.ACTION_CREATE.equals(action)) {
            ((Window) self).setTitle(LanguageBundleUtils.getString("global.add"));
            txtUserName.setValue("");
            txtContactName.setValue("");
            txtEmail.setValue("");
            txtFixedPhoneNumber.setValue("");
            txtMobilePhoneNumber.setValue("");
            txtDepartment.setValue("");
            txtPosition.setValue("");
            txtOtherInfo.setValue("");
            cbbSex.setSelectedIndex(ParamUtils.SELECT_NOTHING_INDEX);
            //fix by luong
            cbbContactStatus.setValue(LanguageBundleUtils.getString("contact.status.inactive"));
            cbbContactStatus.setDisabled(true);
            bindDataCbbContactType(ParamUtils.DEFAULT_VALUE_STR);
        } else if (ParamUtils.ACTION_UPDATE.equals(action)) {
            ContactDTO objectSearch = new ContactDTO();
            objectSearch.setId(contactId);
            List<ContactDTO> list = contactService.getList(objectSearch);
            oldDTO = list.get(0);
            ((Window) self).setTitle(LanguageBundleUtils.getString("global.edit"));
            if (oldDTO != null) {
                txtUserName.setValue(oldDTO.getUserName());
                txtUserName.setDisabled(true);
                txtContactName.setValue(oldDTO.getName());
                txtEmail.setValue(oldDTO.getEmail());
                txtFixedPhoneNumber.setValue(oldDTO.getFixedPhoneNumber());
                txtMobilePhoneNumber.setValue(oldDTO.getMobilePhoneNumber());
                txtDepartment.setValue(oldDTO.getDepartment());
                txtPosition.setValue(oldDTO.getPosition());
                txtOtherInfo.setValue(oldDTO.getOtherInfo());
                if (StringUtils.isValidString(oldDTO.getSex()) && oldDTO.getSex() != null) {
                    cbbSex.setText(contactComposer.getContactSex(oldDTO.getSex()));
                }

                if (StringUtils.isValidString(oldDTO.getContactStatus())) {
                    //fix by luong
                    if (oldDTO.getContactStatus().equals(ParamUtils.NOT_ACTIVE_STR)){
                        cbbContactStatus.setValue(LanguageBundleUtils.getString("contact.status.inactive"));
                        cbbContactStatus.setDisabled(true);
                    }else {
                        cbbContactStatus.setText(contactComposer.getContactStatusName(oldDTO.getContactStatus()));
                    }

                }
                bindDataCbbContactType(oldDTO.getContactType());
            }
        }
    }

    public ContactDTO getDataFromClient() {
        if (ParamUtils.ACTION_CREATE.equals(action)) {
            oldDTO = new ContactDTO();
        }
        oldDTO.setUserName(txtUserName.getValue().trim().toLowerCase());

        oldDTO.setName(txtContactName.getValue().trim());

        oldDTO.setEmail(txtEmail.getValue().trim());

        oldDTO.setFixedPhoneNumber(txtFixedPhoneNumber.getValue().trim());

        oldDTO.setMobilePhoneNumber(txtMobilePhoneNumber.getValue().trim());

        oldDTO.setDepartment(txtDepartment.getValue().trim());

        oldDTO.setPosition(txtPosition.getValue().trim());

        oldDTO.setOtherInfo(txtOtherInfo.getValue().trim());

        if (cbbSex.getSelectedIndex() >= 0) {
            oldDTO.setSex(cbbSex.getSelectedItem().getValue().toString());
        }

        if (cbbContactType.getSelectedIndex() >= 0) {
            oldDTO.setContactType(cbbContactType.getSelectedItem().getValue().toString());
        }


        if (cbbContactStatus.getValue().toString().equals(LanguageBundleUtils.getString("contact.status.inactive"))){
            oldDTO.setContactStatus(ParamUtils.NOT_ACTIVE_STR);
        }else {
            oldDTO.setContactStatus(cbbContactStatus.getSelectedItem().getValue().toString());
        }


        return oldDTO;
    }

    public void onClick$btnSavePopup() {
        try {
            String message = "";
            if (validate()) {
                oldDTO = getDataFromClient();

                ResultInsertContactDTO resultInsertContactDTO=contactService.saveContact(oldDTO, action);
                message = resultInsertContactDTO.getMessage();

                if (ParamUtils.SUCCESS.equals(message)) {

                    if (ParamUtils.ACTION_CREATE.equals(action)) {

                        messageService.insertMessage(contactComposer.getMessageEntity(resultInsertContactDTO.getIdContact(),LanguageBundleUtils.getString("contact.message.add"), LanguageBundleUtils.getString("contact.message.add")));


                    }else if (ParamUtils.ACTION_UPDATE.equals(action)){
                        long idd=oldDTO.getId();
                        messageService.insertMessage(contactComposer.getMessageEntity(oldDTO.getId(),LanguageBundleUtils.getString("contact.message.update"), LanguageBundleUtils.getString("contact.message.update")));

                    }

                    contactComposer.bindDataToGrid();
                    Clients.showNotification(LanguageBundleUtils.getString("global.message.save.successful"), "info", null, "middle_center", 1000);
                    self.detach();
                    return;
                } else {
                    Clients.showNotification(message, "warning", null, "middle_center", 3000);
                    return;
                }
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    public void onClick$btnClosePopup() {
        self.detach();
    }

    private boolean validate() {
        if (ParamUtils.ACTION_CREATE.equals(action)) {
            if (!StringUtils.isValidString(txtUserName.getValue())) {
                Clients.wrongValue(txtUserName, LanguageBundleUtils.getMessage("global.validate.notnull", LanguageBundleUtils.getString("contact.userName")));
                return false;
            }

            if (!StringUtils.maskEN(txtUserName.getValue())) {
                Clients.wrongValue(txtUserName, LanguageBundleUtils.getMessage("global.validate.error.isEN", LanguageBundleUtils.getString("contact.userName")));
                return false;
            }
        }

        if (!StringUtils.isValidString(txtContactName.getValue())) {
            Clients.wrongValue(txtContactName, LanguageBundleUtils.getMessage("global.validate.notnull", LanguageBundleUtils.getString("contact.name")));
            return false;
        }

        if (!StringUtils.isValidString(txtEmail.getValue())) {
            Clients.wrongValue(txtEmail, LanguageBundleUtils.getMessage("global.validate.notnull", LanguageBundleUtils.getString("contact.email")));
            return false;
        }

        if (!StringUtils.isValidEmail(txtEmail.getValue())) {
            Clients.wrongValue(txtEmail, LanguageBundleUtils.getMessage("global.validate.notformat.email", LanguageBundleUtils.getString("contact.email")));
            return false;
        }

        if (!StringUtils.isValidString(txtFixedPhoneNumber.getValue())) {
            Clients.wrongValue(txtFixedPhoneNumber, LanguageBundleUtils.getMessage("global.validate.notnull", LanguageBundleUtils.getString("contact.fixedPhoneNumber")));
            return false;
        }

        if (!StringUtils.isValidPhone(txtFixedPhoneNumber.getValue())) {
            Clients.wrongValue(txtFixedPhoneNumber, LanguageBundleUtils.getMessage("global.validate.notformat.phone", LanguageBundleUtils.getString("contact.fixedPhoneNumber")));
            return false;
        }

        if (!StringUtils.isValidString(txtMobilePhoneNumber.getValue())) {
            Clients.wrongValue(txtMobilePhoneNumber, LanguageBundleUtils.getMessage("global.validate.notnull", LanguageBundleUtils.getString("contact.mobilePhoneNumber")));
            return false;
        }

        if (!StringUtils.isValidPhone(txtMobilePhoneNumber.getValue())) {
            Clients.wrongValue(txtMobilePhoneNumber, LanguageBundleUtils.getMessage("global.validate.notformat.phone", LanguageBundleUtils.getString("contact.mobilePhoneNumber")));
            return false;
        }

        if (cbbSex.getSelectedItem().getValue() != null && ParamUtils.SELECT_NOTHING_VALUE_STR.equals(cbbSex.getSelectedItem().getValue())) {
            Clients.wrongValue(cbbSex, LanguageBundleUtils.getMessage("global.validate.notchoose", LanguageBundleUtils.getString("contact.sex")));
            return false;
        }

//        if (cbbContactStatus.getSelectedItem().getValue() != null && ParamUtils.SELECT_NOTHING_VALUE_STR.equals(cbbContactStatus.getSelectedItem().getValue())) {
//            Clients.wrongValue(cbbContactStatus, LanguageBundleUtils.getMessage("global.validate.notchoose", LanguageBundleUtils.getString("contact.contactStatusName")));
//            return false;
//        }

        if (cbbContactType.getSelectedItem().getValue() != null && ParamUtils.SELECT_NOTHING_VALUE_STR.equals(cbbContactType.getSelectedItem().getValue())) {
            Clients.wrongValue(cbbContactType, LanguageBundleUtils.getMessage("global.validate.notchoose", LanguageBundleUtils.getString("contact.contactTypeName")));
            return false;
        }
        return true;
    }
}
