/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.composer;

import com.viettel.csp.DTO.BaseCustomDTO;
import com.viettel.csp.entity.EditedDataInListArg;
import com.viettel.csp.entity.FmApParamEntity;
import com.viettel.csp.service.BaseCustomService;
import com.viettel.csp.util.KeyValueBean;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.ParamUtils;
import com.viettel.logincsp.UserCredential;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.ForwardEvent;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Window;
import org.zkoss.zul.event.PagingEvent;
import org.zkoss.zul.impl.InputElement;

/**
 * @author binhnt22
 */
public abstract class BaseCustomComposer<TService extends BaseCustomService, TForm extends BaseCustomDTO> extends
    GenericForwardComposer {

    protected static Logger actionLog = Logger.getLogger(ParamUtils.ACTION_LOG);
    protected static Logger exceptionLog = Logger.getLogger(ParamUtils.EXCEPTION_LOG);
    private static Logger log = Logger.getLogger(BaseCustomComposer.class);
    protected KeyValueBean dummy;
    protected TService service;
    protected TForm dto;
    protected TForm searchDTO;
    protected ListModelList<TForm> listData = new ListModelList();
    protected String dialogFilePath;
    protected int totalRecord;
    Locale locale;
    private Paging dataPaging;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        if (this.dataPaging != null) {
            this.dataPaging.setPageSize(ParamUtils.DEFAULT_PAGE_SIZE);
        }
    }

    //Get dummy kevValueBean (- Please select -)
    private KeyValueBean getDummy() {
        this.dummy = new KeyValueBean();
        this.dummy.setValue(LanguageBundleUtils.getString(getLocale(), "MSG.PLEASE_SELECT"));
        this.dummy.setKey(ParamUtils.DEFAULT_VALUE.toString());
        return this.dummy;
    }

    protected KeyValueBean getActiveStatus(ListModelList<KeyValueBean> lstStatus) {
        for (KeyValueBean bean : lstStatus) {
            if (bean.getKey().toString().equals(String.valueOf(ParamUtils.ACTIVE))) {
                return bean;
            }
        }
        return lstStatus.getElementAt(0);
    }

    //Get current locale
    protected Locale getLocale() {
        try {
            String langLocal = Sessions.getCurrent().getAttribute("Demo_Locale").toString();
            this.locale = langLocal.length() > 2
                ? new Locale(langLocal.substring(0, 2), langLocal.substring(3))
                : new Locale(langLocal);
        } catch (Exception e) {
            this.locale = Locale.ENGLISH;
            log.error(e.getMessage(), e);
        }
        return this.locale;
    }

    //Create message from key in .properties file
    protected String createMessage(String msg, String... args) {
        return LanguageBundleUtils.getString(getLocale(), msg, args);
    }

    //Get keyValueBean from list of BaseCustomDTO
    protected List<KeyValueBean> genCombo(List<? extends BaseCustomDTO> data) {
        List<KeyValueBean> newList = new ArrayList<>();
        for (BaseCustomDTO dto : data) {
            newList.add(dto.toBean());
        }
        return newList;
    }

    //Get keyValueBean from list of BaseCustomDTO with default value
    protected List<KeyValueBean> genComboWithDefault(List<? extends BaseCustomDTO> data) {
        List<KeyValueBean> newList = new ArrayList<>();
        newList.add(getDummy());
        for (BaseCustomDTO dto : data) {
            newList.add(dto.toBean());
        }
        return newList;
    }

    //Get keyValueBean from list of FmApParamEntity with default value
//    protected List<KeyValueBean> genApComboWithDefault(List<? extends BaseCustomEntity> data) {
//        List<KeyValueBean> newList = new ArrayList<KeyValueBean>();
//        newList.add(getDummy());
//        for (BaseCustomEntity dto : data) {
//            newList.add(dto.toBean());
//        }
//        return newList;
//    }

    protected List<KeyValueBean> genApComboCodeNameWithDefault(
        List<? extends FmApParamEntity> data) {
        List<KeyValueBean> newList = new ArrayList<>();
        newList.add(getDummy());
        for (FmApParamEntity dto : data) {
            newList.add(new KeyValueBean(dto.getParamCode(),
                LanguageBundleUtils.getString(dto.getParamName())));
        }
        return newList;
    }

    protected KeyValueBean getSelectedItem(ListModelList<KeyValueBean> lst, String key) {
        KeyValueBean result = null;
        if (lst.size() > 0) {
            for (int i = 0; i < lst.size(); ++i) {
                if (lst.getElementAt(i).getKey().toString().equals(key)) {
                    result = lst.getElementAt(i);
                    break;
                }
            }
        }
        return result;
    }

    protected void showErrorMsg(Logger log, Exception ex) {
        logError(log, ex);
        Window window = (Window) Executions.createComponents("/error.zul", this.self, null);
        window.doModal();
    }

    protected void logError(Logger log, Exception ex) {
        StringBuilder strError = new StringBuilder(new Date().toString())
            .append(ParamUtils.LOG_SEPERATOR).append(this.getClass().getName())
            .append(ParamUtils.LOG_SEPERATOR).append(ParamUtils.DEFAULT_EXCEPTION_LEVEL)
            .append(ParamUtils.LOG_SEPERATOR).append(ex.getMessage())
            .append(ParamUtils.LOG_SEPERATOR).append(ParamUtils.DEFAULT_ERR_CODE);
        exceptionLog.error(strError.toString());
        log.error(ex.getMessage(), ex);
    }

    protected void logAction(String action, List<TForm> lstDto) {
        String remoteAddr = Executions.getCurrent().getRemoteAddr();
        Object obj = Sessions.getCurrent().getAttribute(ParamUtils.REQUEST_TIME);
        Date requestTime = obj == null ? null : (Date) obj;
        UserCredential userToken = (UserCredential) Sessions.getCurrent()
            .getAttribute(ParamUtils.USER_TOKEN);
        String userName = userToken.getUserName();
        String strLog = new StringBuilder(ParamUtils.APP_CODE)
            .append(ParamUtils.LOG_SEPERATOR).append("N/A")
            .append(ParamUtils.LOG_SEPERATOR).append(requestTime)
            .append(ParamUtils.LOG_SEPERATOR).append(new Date())
            .append(ParamUtils.LOG_SEPERATOR).append(userName)
            .append(ParamUtils.LOG_SEPERATOR).append(remoteAddr)
            .append(ParamUtils.LOG_SEPERATOR).append(this.getClass().getName())
            .append(ParamUtils.LOG_SEPERATOR).append(action).toString();
        StringBuilder logDetail;
        if (lstDto != null && !lstDto.isEmpty()) {
            for (BaseCustomDTO form : lstDto) {
                logDetail = new StringBuilder(strLog);
                logDetail.append(ParamUtils.LOG_SEPERATOR).append(form.getObjectId())
                    .append(ParamUtils.LOG_SEPERATOR).append(form.getDetailString());
                actionLog.info(logDetail);
            }
        } else {
            logDetail = new StringBuilder(strLog)
                .append(ParamUtils.LOG_SEPERATOR).append("N/A")
                .append(ParamUtils.LOG_SEPERATOR).append("N/A");
            actionLog.info(logDetail);
        }

    }

    protected void showWarningMsg(String message, Component validateComponent) {
        Clients.showNotification(message, "warning", validateComponent, "middle_right", 1000);
    }

    protected void showBusy() {
        Clients.showBusy(createMessage("MSG.LOADING"));
    }

    protected void clearBusy() {
        Clients.clearBusy();
    }

    protected String getExportFolder() {
        String relativeFolder = LanguageBundleUtils
            .getString(ParamUtils.CONFIG_RESROUCE_FILE, "exportFolder");
        return relativeFolder;//Sessions.getCurrent().getWebApp().getRealPath(relativeFolder);
    }

    //    public void onClick$btnSyncHadoop() {
//        try {
//            service.exportData(getExportFolder());
//            Clients.showNotification(LanguageBundleUtils.getString("MSG.SYNC_HADOOP_SUCCESS"));
//        } catch (Exception e) {
//            showErrorMsg(getLogger(), e);
//        }
//    }
    protected abstract Logger getLogger();

    protected abstract Window getWindow();

    protected abstract Listbox getGridData();

    protected abstract String getArgDTOKey();

    protected abstract void buildSearchObject();

    public ListModel<TForm> getListData() {
        return this.listData;
    }

    public void setListData(ListModelList<TForm> lstData) {
        this.listData = lstData;
    }

    public void setSearchDTO(TForm searchDTO) {
        this.searchDTO = searchDTO;
    }

    protected void doSearch(TForm searchForm) throws Exception {
        //Dem so ban ghi
        if (this.dataPaging != null) {
            this.totalRecord = new Integer(String.valueOf(this.service.count(searchForm)));
            this.dataPaging.setTotalSize(this.totalRecord);
            if (this.dataPaging.getTotalSize() <= this.dataPaging.getPageSize()) {
                this.dataPaging.setVisible(false);
            } else {
                this.dataPaging.setVisible(true);
                this.dataPaging.setActivePage(0);
            }
            //Lay danh sach ban ghi trong page dau tien
            this.listData = new ListModelList<TForm>(
                this.service.search(searchForm, 0, ParamUtils.DEFAULT_PAGE_SIZE,
                    ParamUtils.ORDER_ASC, searchForm.getDefaultSortField()));
        } else {
            this.listData = new ListModelList<TForm>(this.service.search(searchForm));
        }
        getGridData().setModel(this.listData);
        this.listData.setMultiple(true);
    }

    //Xu ly su kien search
    public void onClick$btnSearch() {
        try {
            if (validateSearch()) {
                buildSearchObject();
                doSearch(this.searchDTO);
            }
            logAction(ParamUtils.ACTION_SEARCH, null);
        } catch (Exception e) {
            showErrorMsg(getLogger(), e);
        }
    }

    public void onPaging$dataPaging(Event event) {
        try {
            if (this.totalRecord == 0) {
                return;
            }
            ForwardEvent fe = (ForwardEvent) event;
            final PagingEvent pe = (PagingEvent) fe.getOrigin();
            int startPage = pe.getActivePage();
            int start = startPage * ParamUtils.DEFAULT_PAGE_SIZE;
            this.listData = new ListModelList<TForm>(
                this.service.search(this.searchDTO, start, ParamUtils.DEFAULT_PAGE_SIZE,
                    ParamUtils.ORDER_ASC, this.searchDTO.getDefaultSortField()));

            getGridData().setModel(this.listData);
            this.listData.setMultiple(true);
            logAction(ParamUtils.ACTION_PAGING, null);
        } catch (Exception ex) {
            showErrorMsg(getLogger(), ex);
        }
    }

    protected boolean validateSearch() {
        return true;
    }

    //Xy ly su kien bam nut Add
    public void onClick$btnAdd() {
        try {
            openEditorPopup(ParamUtils.ACTION_CREATE);
        } catch (Exception e) {
            showErrorMsg(getLogger(), e);
        }
    }
    //Xu ly su kien bam nut Edit

    public void onClick$btnEdit() {

        try {
            if (checkBeforeEdit()) {
                openEditorPopup(ParamUtils.ACTION_UPDATE);
            }
        } catch (Exception e) {
            showErrorMsg(getLogger(), e);
        }
    }

    protected boolean checkBeforeEdit() {
        boolean result = true;
        TForm selectedItem = (getGridData().getSelectedItem() == null) ? null
            : (TForm) getGridData().getSelectedItem().getValue();
        if (selectedItem != null) {
            if (ParamUtils.INACTIVE_STR.equalsIgnoreCase(selectedItem.getStatus())) {
                result = false;
                Clients.showNotification(createMessage("MSG.EDIT_INACTIVE"));
            }
        } else {
            result = false;
            Clients.showNotification(createMessage("MSG.SELECT_TO_UPDATE"), getGridData());
        }
        return result;
    }

    protected void openEditorPopup(String command) {
        ListModelList<TForm> listModel = (ListModelList) getGridData().getModel();
        TForm currentDTO = null;
        if (command.equalsIgnoreCase(ParamUtils.ACTION_UPDATE)) {
            currentDTO = getGridData().getSelectedItem().getValue();
        }
        if (currentDTO != null || command.equalsIgnoreCase(ParamUtils.ACTION_CREATE)) {
            Map<String, Object> arguments = new HashMap<>();
            EditedDataInListArg arg = new EditedDataInListArg();
            arg.setData(currentDTO);
            arg.setIndex(listModel.indexOf(currentDTO));
            arguments.put(getArgDTOKey(), arg);
            arguments.put(ParamUtils.ACTION_COMMAND, command);
            Window window = (Window) Executions.createComponents(
                ParamUtils.ZUL_PATH + this.dialogFilePath, getWindow(), arguments);
            window.doModal();
        }
    }

    public void refreshGridData(EditedDataInListArg arg) {
        if (this.dataPaging != null) {
            int startPage = this.dataPaging.getActivePage();
            int start = startPage * ParamUtils.DEFAULT_PAGE_SIZE;
            this.listData = new ListModelList<TForm>(
                this.service.search(this.searchDTO, start, ParamUtils.DEFAULT_PAGE_SIZE,
                    ParamUtils.ORDER_ASC, this.searchDTO.getDefaultSortField()));

        } else {
            this.listData = new ListModelList(this.service.search(this.searchDTO));
        }
        getGridData().setModel(this.listData);
        this.listData.setMultiple(true);
        logAction(ParamUtils.ACTION_REFRESH, null);
    }

    //Xu ly su kien delete
    public void onClick$btnDelete() {
        try {
            if (getGridData().getSelectedCount() > 0) {
                Messagebox
                    .show(createMessage("MSG.CONFIRM_DELETE"), createMessage("global.confirm"),
                        Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
                        new org.zkoss.zk.ui.event.EventListener() {
                            @Override
                            public void onEvent(Event evt) throws Exception {
                                if ("onOK".equals(evt.getName())) {
                                    List<TForm> lstSelected = new ArrayList<>();
                                    Set<Listitem> lstSelectedItem = getGridData()
                                        .getSelectedItems();
                                    for (Listitem item : lstSelectedItem) {
                                        lstSelected.add((TForm) item.getValue());
                                    }
                                    //Delete items
                                    performDelete();
                                    //Log action
                                    logAction(ParamUtils.ACTION_DELETE, lstSelected);
                                } else {
                                }
                            }
                        });
            } else {
                Clients.showNotification(createMessage("MSG.SELECT_TO_DELETE"), getGridData());
            }
        } catch (Exception e) {
            showErrorMsg(getLogger(), e);
        }
    }

    protected void performDelete() {
        UserCredential userToken = (UserCredential) Sessions.getCurrent()
            .getAttribute(ParamUtils.USER_TOKEN);
        TForm tmp;
        List<Object> deleteList = new ArrayList<>();
        Set<Listitem> lstSelectedItem = getGridData().getSelectedItems();
        for (Listitem item : lstSelectedItem) {
            tmp = (TForm) item.getValue();
            if (tmp.getStatus() == null || tmp.getStatus().isEmpty() || tmp.getStatus()
                .equals(ParamUtils.ACTIVE_STR)) {
                deleteList.add(String.valueOf(tmp.catchId()));
            }
        }
        if (deleteList.size() > 0) {
            int count = deleteItems(deleteList, userToken);
            Clients.showNotification(
                createMessage("MSG.SUCCESS_WITH_RECORD", "MSG.DELETE", String.valueOf(count)));
        } else {
            Clients.showNotification(createMessage("MSG.SUCCESS_WITH_RECORD", "MSG.DELETE", "0"));
        }
        refreshGridData(null);
    }

    protected int deleteItems(List<Object> deleteList, UserCredential userToken) {
        return 0;
    }

    //Xu ly activate ban ghi
    public void onClick$btnActivate() {
        try {
            if (getGridData().getSelectedCount() > 0) {
                Messagebox
                    .show(createMessage("MSG.CONFIRM_ACTIVATE"), createMessage("global.confirm"),
                        Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
                        new org.zkoss.zk.ui.event.EventListener() {
                            @Override
                            public void onEvent(Event evt) throws Exception {
                                if ("onOK".equals(evt.getName())) {
                                    List<TForm> lstSelected = new ArrayList<>();
                                    Set<Listitem> lstSelectedItem = getGridData()
                                        .getSelectedItems();
                                    for (Listitem item : lstSelectedItem) {
                                        lstSelected.add((TForm) item.getValue());
                                    }
                                    //Activate
                                    performActivate();
                                    //Log action
                                    logAction(ParamUtils.ACTION_ACTIVATE, lstSelected);
                                } else {
                                }
                            }
                        });
            } else {
                Clients.showNotification(createMessage("MSG.SELECT_TO_ACTIVATE"), getGridData());
            }
        } catch (Exception e) {
            showErrorMsg(getLogger(), e);
        }
    }

    public void performActivate() {
        UserCredential userToken = (UserCredential) Sessions.getCurrent()
            .getAttribute(ParamUtils.USER_TOKEN);
        TForm tmp;
        List<Object> inactiveList = new ArrayList<>();
        Set<Listitem> lstSelectedItem = getGridData().getSelectedItems();
        for (Listitem item : lstSelectedItem) {
            tmp = item.getValue();
            if (tmp.getStatus().equals(ParamUtils.INACTIVE_STR)) {
                inactiveList.add(tmp);
            }
        }
        if (inactiveList.size() > 0) {
//            int count = service.activate(inactiveList, userToken);
//            Clients.showNotification(createMessage("MSG.SUCCESS_WITH_RECORD", "MSG.ACTIVATE", String.valueOf(count)));
        } else {
            Clients.showNotification(createMessage("MSG.SUCCESS_WITH_RECORD", "MSG.ACTIVATE", "0"));
        }
        refreshGridData(null);
    }

    protected String getConfigParam(String key) {
        return LanguageBundleUtils.getString(ParamUtils.CONFIG_RESROUCE_FILE, key);
    }

    protected String getRealFilePath(String relativePath) {
        return Sessions.getCurrent().getWebApp().getRealPath(relativePath);
    }

    protected boolean checkValidElement(InputElement element, Boolean isFocused,
        String... msgParams) {
        boolean isValid = true;
        if (!element.isValid()) {
            isValid = false;
            showNotification(element, isFocused, "ERR.NOT_VALID_DATA", msgParams);
        }
        return isValid;
    }

    protected void showNotification(InputElement element, boolean isFocused, String errCode,
        String... msgParams) {
        if (element != null) {
            Clients.showNotification(createMessage(errCode, msgParams), element);
            if (!isFocused) {
                element.setFocus(true);
            }
        } else {
            Clients.showNotification(createMessage(errCode, msgParams));
        }
    }
}
