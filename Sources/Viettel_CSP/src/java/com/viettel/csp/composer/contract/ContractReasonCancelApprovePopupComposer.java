/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.composer.contract;

import com.viettel.csp.DTO.ContractDTO;
import com.viettel.csp.DTO.ContractHisDTO;
import com.viettel.csp.DTO.PartnerDTO;
import com.viettel.csp.DTO.ReasonDTO;
import com.viettel.csp.service.*;
import com.viettel.csp.util.*;
import com.viettel.eafs.util.SpringUtil;
import com.viettel.logincsp.AuthenticationService;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Textbox;

import java.util.ArrayList;
import java.util.List;

/**
 * @author chitv@gemvietnam.com
 */
public class ContractReasonCancelApprovePopupComposer extends GenericForwardComposer<Component> {

    private Label txtCompanyName, txtAddressHeadOffice, txtPartnerCode, txtAddressTradingOffice, txtCompanyNameShort, txtPhone, txtRepName;
    private Textbox txtContenReason;
    private Combobox cbbReason;
    private ListModelList<KeyValueBean> kvbContractStatus;
    private ListModelList<KeyValueBean> kvbReason;
    private List<KeyValueBean> lstContractStatus;
    private List<KeyValueBean> lstReason;
    private String action;
    private Long contractId;
    private ArrayList indexLst;
    private ContractDTO oldDTO;
    private PartnerDTO PartDTO;
    private ContractHisDTO contractHisDTO;
    private AuthenticationService authenticationService = SpringUtil.getBean("authenticationService", AuthenticationService.class);
    private ContractService contractService = SpringUtil.getBean("contractService", ContractService.class);
    private PartnerService partnerService = SpringUtil.getBean("partnerService", PartnerService.class);
    private ContractStatusService contractStatusService = SpringUtil.getBean("contractStatusService", ContractStatusService.class);
    private ContractHisService contractHisService = SpringUtil.getBean("contractHisService", ContractHisService.class);
    final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ContractReasonCancelApprovePopupComposer.class);
    private ReasonService reasonService = SpringUtil.getBean("reasonService", ReasonService.class);
    private ContractFilesService contractFilesService = SpringUtil.getBean("contractFilesService", ContractFilesService.class);
    private ContractPopupEditComposer contractPopupEditComposer;

    public ListModelList<KeyValueBean> getKvbContractStatus() {
        return kvbContractStatus;
    }

    public void setKvbContractStatus(ListModelList<KeyValueBean> kvbContractStatus) {
        this.kvbContractStatus = kvbContractStatus;
    }

    public List<KeyValueBean> getLstContractStatus() {
        return lstContractStatus;
    }

    public void setLstContractStatus(List<KeyValueBean> lstContractStatus) {
        this.lstContractStatus = lstContractStatus;
    }

    public Combobox getCbbReason() {
        return cbbReason;
    }

    public void setCbbReason(Combobox cbbReason) {
        this.cbbReason = cbbReason;
    }

    public ListModelList<KeyValueBean> getKvbReason() {
        return kvbReason;
    }

    public void setKvbReason(ListModelList<KeyValueBean> kvbReason) {
        this.kvbReason = kvbReason;
    }

    public List<KeyValueBean> getLstReason() {
        return lstReason;
    }

    public void setLstReason(List<KeyValueBean> lstReason) {
        this.lstReason = lstReason;
    }

    @Override
    public void doAfterCompose(Component comp) {
        try {
            super.doAfterCompose(comp);
            if (arg.containsKey(ParamUtils.COMPOSER)) {
                contractPopupEditComposer = (ContractPopupEditComposer) arg.get(ParamUtils.COMPOSER);
            }
            if (arg.containsKey(ParamUtils.ACTION)) {
                action = arg.get(ParamUtils.ACTION).toString();
            }
            if (arg.containsKey("CONTRACT_ID") && StringUtils.isValidString(String.valueOf(arg.get("CONTRACT_ID")))) {
                contractId = (Long) arg.get("CONTRACT_ID");
                ContractDTO objectSearch = new ContractDTO();
                objectSearch.setContractId(this.contractId);
                List<ContractDTO> list = this.contractService.getListContractProfile(objectSearch, this.authenticationService.getUserCredential());
                this.oldDTO = list.get(0);
            }
            bindDataCbbContractStatus(ParamUtils.DEFAULT_VALUE_STR);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    public void bindDataCbbContractStatus(String valueSelect) throws Exception {
        try {
            List<ReasonDTO> lstReasonDTO = reasonService.findListByType(ParamUtils.REASON_TYPE.CONTRACT_PROFILE);
            lstReason = new ArrayList<>();
            lstReason.add(new KeyValueBean(ParamUtils.DEFAULT_VALUE_STR, LanguageBundleUtils.getString("global.combobox.choose")));
            if (lstReasonDTO != null && lstReasonDTO.size() > 0) {
                for (ReasonDTO reasonDTO : lstReasonDTO) {
                    KeyValueBean key = new KeyValueBean();
                    key.setKey(reasonDTO.getCode());
                    key.setValue(reasonDTO.getName());
                    lstReason.add(key);
                }
            }
            kvbReason = new ListModelList(lstReason);
            int i = 0;
            for (KeyValueBean itemParam : kvbReason.getInnerList()) {
                if (itemParam.getKey() != null && String.valueOf(itemParam.getKey()).equals(valueSelect)) {
                    kvbReason.addToSelection(kvbReason.getElementAt(i));
                    break;
                }
                i++;
            }
            cbbReason.setModel(kvbReason);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    public void onClick$btnSendReason() {
        try {
            if (!CtrlValidator.checkValidComboValue(cbbReason, true, true, LanguageBundleUtils.getString("reason.content"))) {
                return;
            }
            if (!CtrlValidator.checkValidTextbox(txtContenReason, true, true, LanguageBundleUtils.getString("reason.description"))) {
                return;
            }
            int flat = ParamUtils.TYPE_ACTION.NOK_APPROVE_PROFILE;
            String message = "";
            contractService.updateContract(this.oldDTO, this.action, flat);
            contractFilesService.updateFilesNotUsing(this.oldDTO, ParamUtils.FILE_TYPE.FILE_TYPE_TOTRINH_HOSO);
            contractHisDTO = getDataFromClientContractHis(oldDTO);
            message = contractHisService.insert(contractId, contractHisDTO);
            if (ParamUtils.SUCCESS.equals(message)) {
                Clients.showNotification(LanguageBundleUtils.getString("global.message.update.successful"), "info", null, "middle_center", 1000);
                self.detach();
                contractPopupEditComposer.onClick$btnClose();
            } else {
                Clients.showNotification(message, "warning", null, "middle_center", 3000);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    public ContractHisDTO getDataFromClientContractHis(ContractDTO contractDTO) {
        contractHisDTO = new ContractHisDTO();
        contractHisDTO.setContractId(contractId);
        if (cbbReason.getSelectedIndex() > 0) {
            contractHisDTO.setContent(LanguageBundleUtils.getString("message.partner.no.approve.of.profile") + cbbReason.getValue() + ": " + txtContenReason.getValue());
        }
        return contractHisDTO;
    }

    public void onClick$btnClose() {
        try {
            self.detach();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

}
