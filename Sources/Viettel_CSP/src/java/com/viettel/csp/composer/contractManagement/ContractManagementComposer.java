package com.viettel.csp.composer.contractManagement;

import com.viettel.csp.DTO.ContractDTO;
import com.viettel.csp.DTO.ContractStatusDTO;
import com.viettel.csp.constant.ContractManagerURL;
import com.viettel.csp.constant.ContractStatus;
import com.viettel.csp.service.ContractService;
import com.viettel.csp.service.ContractStatusService;
import com.viettel.csp.service.PartnerService;
import com.viettel.csp.service.UserService;
import com.viettel.csp.util.*;
import com.viettel.eafs.util.SpringUtil;
import com.viettel.logincsp.AuthenticationService;
import org.apache.log4j.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.*;

import java.util.*;

/**
 * @author Chitv@gemvietnam.com
 */
public class ContractManagementComposer extends GenericForwardComposer<Component> {

    private static final Logger logger = Logger.getLogger(ContractManagementComposer.class);
    private ListModelList<ContractDTO> listContract = new ListModelList<>();
    private ListModelList<KeyValueBean> kvbContractStatus;
    private List<ContractDTO> lstResult;
    private Listbox lbxContractManagement;
    private Textbox txtCompanyName, txtCompanyNameShort, txtContractNumber;
    private Grid gridSearch;
    private Combobox cbbContractStatus;
    private Map<String, String> mapParent = new HashMap<>();
    private AuthenticationService authenticationService = SpringUtil.getBean("authenticationService", AuthenticationService.class);
    private ContractService contractService = SpringUtil.getBean("contractService", ContractService.class);
    private PartnerService partnerService = SpringUtil.getBean("partnerService", PartnerService.class);
    private UserService userService = SpringUtil.getBean("userService", UserService.class);
    private ContractStatusService contractStatusService = SpringUtil.getBean("contractStatusService", ContractStatusService.class);
    private Button btnAdd;

    public ListModelList<ContractDTO> getListContract() {
        return this.listContract;
    }

    public void setListContract(ListModelList<ContractDTO> listContract) {
        this.listContract = listContract;
    }

    public ListModelList<KeyValueBean> getKvbContractStatus() {
        return this.kvbContractStatus;
    }

    public void setKvbContractStatus(ListModelList<KeyValueBean> kvbContractStatus) {
        this.kvbContractStatus = kvbContractStatus;
    }

    public List<ContractDTO> getLstResult() {
        return this.lstResult;
    }

    public void setLstResult(List<ContractDTO> lstResult) {
        this.lstResult = lstResult;
    }

    public Listbox getLbxContractManagement() {
        return this.lbxContractManagement;
    }

    public void setLbxContractManagement(Listbox lbxContractManagement) {
        this.lbxContractManagement = lbxContractManagement;
    }

    public Textbox getTxtCompanyNameShort() {
        return this.txtCompanyNameShort;
    }

    public void setTxtCompanyNameShort(Textbox txtCompanyNameShort) {
        this.txtCompanyNameShort = txtCompanyNameShort;
    }

    public Textbox getTxtContractNumber() {
        return this.txtContractNumber;
    }

    public void setTxtContractNumber(Textbox txtContractNumber) {
        this.txtContractNumber = txtContractNumber;
    }

    public PartnerService getPartnerService() {
        return this.partnerService;
    }

    public void setPartnerService(PartnerService partnerService) {
        this.partnerService = partnerService;
    }

    public Textbox getTxtCompanyName() {
        return this.txtCompanyName;
    }

    public void setTxtCompanyName(Textbox txtCompanyName) {
        this.txtCompanyName = txtCompanyName;
    }

    public Grid getGridSearch() {
        return this.gridSearch;
    }

    public void setGridSearch(Grid gridSearch) {
        this.gridSearch = gridSearch;
    }

    public Combobox getCbbContractStatus() {
        return this.cbbContractStatus;
    }

    public void setCbbContractStatus(Combobox cbbContractStatus) {
        this.cbbContractStatus = cbbContractStatus;
    }

    public ContractService getContractService() {
        return this.contractService;
    }

    public void setContractService(ContractService contractService) {
        this.contractService = contractService;
    }

    public UserService getUserService() {
        return this.userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public ContractStatusService getContractStatusService() {
        return this.contractStatusService;
    }

    public void setContractStatusService(ContractStatusService contractStatusService) {
        this.contractStatusService = contractStatusService;
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        this.lbxContractManagement.setMultiple(true);
        bindDataToCombo();
        bindData();
        setVisible(UserTokenUtils.getPersonType());
    }

    public void bindDataToCombo() throws Exception {
        List<KeyValueBean> lstKeyValueBean = this.contractStatusService
                .getListToKeyValueBean(true);
        if (lstKeyValueBean != null && lstKeyValueBean.size() > 0) {
            this.kvbContractStatus = new ListModelList(lstKeyValueBean);
            this.kvbContractStatus.addToSelection(this.kvbContractStatus.getElementAt(0));
            this.cbbContractStatus.setModel(this.kvbContractStatus);
        } else {
            this.cbbContractStatus.setModel(new ListModelList());
        }
        this.cbbContractStatus
                .setValue(LanguageBundleUtils.getString("global.combobox.choose"));
    }

    public ContractDTO getDataFromClient() {
        ContractDTO object = new ContractDTO();

        if (StringUtils.isValidString(this.txtCompanyName.getValue())) {
            object.setCompanyName(this.txtCompanyName.getValue().trim());
        }
        if (StringUtils.isValidString(this.txtCompanyNameShort.getValue())) {
            object.setCompanyNameShort(this.txtCompanyNameShort.getValue().trim());
        }
        if (StringUtils.isValidString(this.txtContractNumber.getValue())) {
            object.setContractNumber(this.txtContractNumber.getValue().trim());
        }
        if (this.cbbContractStatus.getSelectedIndex() > 0) {
            object.setContractStatusCode(this.cbbContractStatus.getSelectedItem().getValue());
        }
        return object;
    }

    public void onClick$btnSearch() {
        bindData();
    }

    public void bindData() {
        ContractDTO contractDTO = getDataFromClient();
        this.lstResult = this.contractService.getListContract(contractDTO, authenticationService.getUserCredential());
        if (this.lstResult != null && !this.lstResult.isEmpty()) {
            ListModelList model = new ListModelList(this.lstResult);
            model.setMultiple(true);
            this.lbxContractManagement.setModel(model);
            if (this.gridSearch != null) {
                this.gridSearch.invalidate();
            }
        } else {
            this.lbxContractManagement.setModel(new ListModelList());
        }
    }

    public void onClick$btnEdit() {
        if (this.lbxContractManagement.getSelectedCount() == 1) {
            Map<String, Object> argu = new HashMap();
            argu.put(ParamUtils.ACTION, ParamUtils.ACTION_UPDATE);
            List<Integer> indexs = new ArrayList();
            indexs.add(this.lbxContractManagement.getSelectedItem().getIndex());
            ContractDTO contactDTO = (ContractDTO) this.lbxContractManagement.getSelectedItem().getValue();
            ContractStatus contractStatus = ContractStatus.getContractStatus(this.lstResult.get(this.lbxContractManagement.getSelectedIndex()).getContractStatus());
            argu.put("LIST_ACTION_INDEX", indexs);
            argu.put("LISTBOX", this.lbxContractManagement);
            argu.put("MAP_SERVER_GROUP", this.mapParent);
            argu.put("ID",
                    this.lstResult.get(this.lbxContractManagement.getSelectedIndex())
                            .getContractId());
            argu.put("CONTRACT_ID", contactDTO.getContractId());
            argu.put("CONTRACT", contactDTO);
            argu.put("PARTNER_ID", contactDTO.getPartnerId());
            argu.put("contractType", contactDTO.getContractType());
            argu.put(ParamUtils.COMPOSER, this);
            Window wd;


            String popupURL = ContractManagerURL.getByContractStatus(contractStatus);
            wd = (Window) Executions
                    .createComponents(popupURL, null, argu);
            wd.doModal();
        } else {
            Clients
                    .showNotification(
                            LanguageBundleUtils.getString("global.action.confirm.is_select.edit"),
                            "warning", null, "middle_center", 3000);
        }
    }

    public void onClick$btnDelete() {
        if (this.lbxContractManagement.getSelectedCount() > 0) {
            Messagebox.show(LanguageBundleUtils.getString("global.action.confirm.multi.delete"),
                    "Confirm Dialog", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
                    new org.zkoss.zk.ui.event.EventListener() {
                        @Override
                        public void onEvent(Event evt) throws InterruptedException, Exception {
                            if ("onOK".equals(evt.getName())) {
                                List<ContractDTO> lstDeleteDTO = new ArrayList<>();
                                for (Listitem item : ContractManagementComposer.this.lbxContractManagement
                                        .getSelectedItems()) {
                                    ContractDTO deleteDTO = ((ContractDTO) item.getValue());
                                    lstDeleteDTO.add(deleteDTO);
                                }
                                String message = ContractManagementComposer.this.contractService
                                        .deleteContract(lstDeleteDTO);
                                bindDataToGrid();
                                Clients
                                        .showNotification(message, "info", null, "middle_center", 3000);
                                return;
                            }
                        }
                    });
        } else {
            Clients.showNotification(
                    LanguageBundleUtils.getString("global.action.confirm.is_select"),
                    "warning", null, "middle_center", 3000);
        }
    }

    public void bindDataToGrid() {
        onClick$btnSearch();
    }

    public String formatDateTime(Date date) throws Exception {
        return StringUtils.formatDateTime(date);
    }

    public void onClick$btnAdd() {
        try {
            Map<String, Object> argu = new HashMap();
            argu.put(ParamUtils.ACTION, ParamUtils.ACTION_CREATE);
            argu.put("CONTACT_ID", "");
            argu.put(ParamUtils.COMPOSER, this);
            Window wd = (Window) Executions.createComponents("/controls/contractManagement/contractManagementAddViettelB.zul", null, argu);
//                wd = (Window) Executions
//                    .createComponents("/controls/contract/contractAddPopup.zul", null, argu);
            wd.doModal();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    public String getContractStatusHisName(String paramType) throws Exception {
        ContractStatusDTO contractStatusDTO = contractStatusService.getObjectDTOByCode(paramType);
        return contractStatusDTO.getName();
    }

    public void setVisible(String contractStatus) {
        if (contractStatus.equals(ParamUtils.USER_TYPE.VIETTEL)) {
            btnAdd.setVisible(false);
        } else if (contractStatus.equals(ParamUtils.USER_TYPE.CONTACT)) {
            btnAdd.setVisible(false);
        }
    }

}
