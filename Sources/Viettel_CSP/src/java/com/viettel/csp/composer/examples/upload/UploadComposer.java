/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.composer.examples.upload;

import com.viettel.csp.composer.partner.PartnerComposer;
import com.viettel.csp.util.ParamUtils;
import com.viettel.plc.widget.FileViews;
import com.viettel.plc.widget.IUploaderCallback;
import com.viettel.plc.widget.UploadComponent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Window;

/**
 *
 * @author GEM
 */
public class UploadComposer extends GenericForwardComposer<Component> implements IUploaderCallback {

    private static final Logger logger = Logger.getLogger(PartnerComposer.class);
    private UploadComponent uploadComponent;
    private List<String> filesLocation;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        //Xoa session key trong phan partner lock
        Sessions.getCurrent().removeAttribute("upload");
        uploadComponent.setUploaderCallback(this);
    }

    public void onClick$btnUpload() {
        try {
            Map<String, Object> argu = new HashMap();
            argu.put(ParamUtils.ACTION, ParamUtils.ACTION_CREATE);
            argu.put(ParamUtils.COMPOSER, this);
//            List<String> filesPath = new ArrayList();
//            filesPath.add("C:\\Users\\admin\\Desktop\\links.pdf");
//            filesPath.add("C:\\Users\\admin\\Desktop\\The.pdf");
//            filesPath.add("C:\\Users\\admin\\Desktop\\ebook-how-to-build-and-scale-with-microservices - Copy.pdf");
            List<FileViews> filesPath = new ArrayList<>();
            filesPath.add(new FileViews("links.pdf", "C:\\Users\\admin\\Desktop"));
            filesPath.add(new FileViews("The.pdf", "C:\\Users\\admin\\Desktop"));
            argu.put("DATA", filesPath);
//            argu.put("LIST_FILE", filesPath);
//            argu.put("SIGN_TYPE", 1);
//            argu.put("SIGN_TIME", 1);
//            argu.put("CREATE_USER", "GEM");
//            argu.put("OBJECT_ID", "123456");
//            argu.put("CONTROLLER", this);
//            argu.put("METHOD_CALLBACK", "hohoho");

            Window wd = (Window) Executions.createComponents("/controls/widget/pdfFilesViewerPopup.zul", null, argu);
//            Window wd = (Window) Executions.createComponents("/SignCaPopup.zul", null, argu);
            wd.doModal();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

//    public void onClick$btnUpload() {
//        try {
//            Map<String, Object> argu = new HashMap();
//            argu.put(ParamUtils.ACTION, ParamUtils.ACTION_CREATE);
//            argu.put(ParamUtils.COMPOSER, this);
//            Window wd = (Window) Executions.createComponents("/controls/widget/uploadPopup.zul", null, argu);
//            wd.doModal();
//        } catch (Exception ex) {
//            logger.error(ex.getMessage(), ex);
//        }
//    }
    @Override
    public void uploadedFilesLocationCallback(List<String> filesLocation) {
        this.filesLocation = filesLocation;
        System.out.println(filesLocation);
    }

    public void hohoho() {

    }

}
