/*
 * Copyright (C) 2010 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.viettel.csp.composer.examples.signPDF;

/**
 * this class contains parameters to sign
 * @author sonnt38@viettel.com.vn
 * @since 25/11/2010
 * @version 1.0
 */
public class SignatureParameter {
    /**
     * Comment of signer
     */
    private String comment;
    
    /**
     * main constructor
     * @param comment comment of signer
     */
    public SignatureParameter(String comment) {
        this.comment = comment;
    }

    public String getComment() {
        return comment;
    }

    
}
