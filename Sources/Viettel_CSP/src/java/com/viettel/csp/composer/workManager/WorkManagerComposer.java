/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.composer.workManager;

import com.viettel.csp.DTO.ContractDTO;
import com.viettel.csp.DTO.ServiceDTO;
import com.viettel.csp.service.ContractService;
import com.viettel.csp.service.ServiceService;
import com.viettel.csp.util.*;
import com.viettel.eafs.util.SpringUtil;
import org.apache.log4j.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.*;

import java.util.*;

/**
 * @author admin
 */
public class WorkManagerComposer extends GenericForwardComposer<Component> {

    private static final Logger logger = Logger.getLogger(WorkManagerComposer.class);
    private ListModelList<ContractDTO> listContract = new ListModelList<ContractDTO>();
    private List<ContractDTO> lstResult;
    private Listbox lbxContract;
    private Grid gridSearch;

    private Textbox txtNumberContract, txtNameContract, txtHeadOfService, txtCodeContract;
    private Combobox ccbService, ccbServiceGroup, ccbStatus;
    private ListModelList<KeyValueBean> kvbService, kvbServiceGroup, kvbStatus;

    private ContractService contractService = SpringUtil.getBean("contractService", ContractService.class);
    private ServiceService serviceService = SpringUtil.getBean("serviceService", ServiceService.class);

    private List<ServiceDTO> listParent;
    private Map<String, String> mapParent = new HashMap<>();

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        try {
            super.doAfterCompose(comp);
            lbxContract.setMultiple(true);
            this.bindData();
            this.onClick$btnSearch();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    public void bindData() throws Exception {
        try {
            List<KeyValueBean> lstKeyValueBean;
            this.listParent = serviceService.getListWithNullParent();
            for (ServiceDTO ins : this.listParent) {
                mapParent.put(ins.getCode(), ins.getName());
            }
            lstKeyValueBean = this.createSelected(this.listParent);
            if (lstKeyValueBean != null && lstKeyValueBean.size() > 0) {
                kvbServiceGroup = new ListModelList(lstKeyValueBean);
                kvbServiceGroup.addToSelection(kvbServiceGroup.getElementAt(0));
                ccbServiceGroup.setModel(kvbServiceGroup);
            } else {
                ccbServiceGroup.setModel(new ListModelList());
            }
            ccbService.setDisabled(true);
            ccbService.setValue(LanguageBundleUtils.getString("global.combobox.choose"));

            lstKeyValueBean = new ArrayList<>();
            lstKeyValueBean.add(new KeyValueBean(ParamUtils.SELECT_NOTHING_VALUE_STR, LanguageBundleUtils.getString("global.combobox.choose")));
            lstKeyValueBean.add(new KeyValueBean(ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_STATUS_ACTIVE, LanguageBundleUtils.getString("contract.status.active")));
            lstKeyValueBean.add(new KeyValueBean(ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_STATUS_AWAIT_ACTIVE, LanguageBundleUtils.getString("contract.status.await.active")));
            lstKeyValueBean.add(new KeyValueBean(ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_STATUS_COLSE, LanguageBundleUtils.getString("contract.status.close")));
            lstKeyValueBean.add(new KeyValueBean(ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_STATUS_FINISH, LanguageBundleUtils.getString("contract.status.finish")));
            kvbStatus = new ListModelList(lstKeyValueBean);
            kvbStatus.addToSelection(kvbStatus.getElementAt(0));
            ccbStatus.setModel(kvbStatus);
            ccbStatus.setValue(LanguageBundleUtils.getString("global.combobox.choose"));
        } catch (Exception e) {
            logger.error("bindData() : WorkManagerComposer", e);
        }
    }

    public void onChangeServiceGroup(Event event) throws Exception {
        String key = ccbServiceGroup.getSelectedItem().getValue();
        if (StringUtils.isValidString(key) && !"-99".equals(key)) {
            ccbService.setDisabled(false);
            List<KeyValueBean> lstKeyValueBean;
            lstKeyValueBean = this.createSelected(serviceService.getServiceByCode(key));
            if (lstKeyValueBean != null && lstKeyValueBean.size() > 0) {
                kvbService = new ListModelList(lstKeyValueBean);
                kvbService.addToSelection(kvbService.getElementAt(0));
                ccbService.setModel(kvbService);
            } else {
                ccbService.setModel(new ListModelList());
            }
        } else {
            ccbService.setDisabled(true);
            ccbService.setValue(LanguageBundleUtils.getString("global.combobox.choose"));
        }
    }

    private List<KeyValueBean> createSelected(List<ServiceDTO> list) {
        List<KeyValueBean> lstKeyValueBean = new ArrayList<>();
        lstKeyValueBean.add(new KeyValueBean(ParamUtils.SELECT_NOTHING_VALUE_STR, LanguageBundleUtils.getString("global.combobox.choose")));
        for (ServiceDTO ins : list) {
            lstKeyValueBean.add(new KeyValueBean(ins.getCode(), ins.getName()));
        }
        return lstKeyValueBean;
    }

    public ContractDTO getDataFromClient() {
        ContractDTO object = new ContractDTO();
        object.setDeployUserId(UserTokenUtils.getUserId());
        if (StringUtils.isValidString(txtCodeContract.getValue())) {
            object.setPartnerCode(txtCodeContract.getValue().trim());
        }
        if (StringUtils.isValidString(txtHeadOfService.getValue())) {
            object.setHeadOfService(txtHeadOfService.getValue().trim());
        }
        if (StringUtils.isValidString(txtNameContract.getValue())) {
            object.setCompanyName(txtNameContract.getValue().trim());
        }
        if (StringUtils.isValidString(txtNumberContract.getValue())) {
            object.setContractNumber(txtNumberContract.getValue().trim());
        }
        if (ccbService.getSelectedIndex() > 0) {
            object.setServiceCode(ccbService.getSelectedItem().getValue());
        }
        if (ccbServiceGroup.getSelectedIndex() > 0) {
            object.setServiceGroup(ccbServiceGroup.getSelectedItem().getValue());
        }
        if (ccbStatus.getSelectedIndex() > 0) {
            object.setServiceCode(ccbStatus.getSelectedItem().getValue());
        }
        return object;
    }

    public void onClick$btnSearch() throws Exception {
        try {
            ContractDTO contractDTO = this.getDataFromClient();
            lstResult = contractService.getListContractForWorkManager(contractDTO);
            if (lstResult != null && !lstResult.isEmpty()) {
                ListModelList model = new ListModelList(lstResult);
                model.setMultiple(false);
                lbxContract.setModel(model);
                if (gridSearch != null) {
                    gridSearch.invalidate();
                }
            } else {
                lbxContract.setModel(new ListModelList());
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    public void onClick$btnEdit() {
        try {
            if (lbxContract.getSelectedCount() == 1) {
                Map<String, Object> argu = new HashMap();
                argu.put(ParamUtils.ACTION, ParamUtils.ACTION_UPDATE);
                List<Integer> indexs = new ArrayList();
                indexs.add(lbxContract.getSelectedItem().getIndex());
                argu.put("LIST_ACTION_INDEX", indexs);
                argu.put("CONTRACT_DETAIL", lbxContract);
                argu.put("MAP_SERVER_GROUP", this.mapParent);
                argu.put(ParamUtils.COMPOSER, this);
                Window wd = (Window) Executions.createComponents("/controls/workManager/detailWork.zul", null, argu);
                wd.doModal();
            } else {
                Clients.showNotification(LanguageBundleUtils.getString("global.action.confirm.is_select.edit"), "warning", null, "middle_center", 3000);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    public void onClick$btnListTask() {
        try {
            Map<String, Object> argu = new HashMap();
            argu.put(ParamUtils.COMPOSER, this);
            Window wd = (Window) Executions.createComponents("/controls/workManager/taskManager.zul", null, argu);
            wd.doModal();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }


    public void onClick$btnDelete() {
        try {
            if (lbxContract.getSelectedCount() > 0) {
                Messagebox.show(LanguageBundleUtils.getString("global.action.confirm.multi.delete"), "Confirm Dialog", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION, new org.zkoss.zk.ui.event.EventListener() {
                    public void onEvent(Event evt) throws InterruptedException, Exception {
                        if ("onOK".equals(evt.getName())) {
                            List<ContractDTO> lstDeleteDTO = new ArrayList<>();
                            for (Listitem item : lbxContract.getSelectedItems()) {
                                ContractDTO deleteDTO = ((ContractDTO) item.getValue());
                                lstDeleteDTO.add(deleteDTO);
                            }
                            String message = contractService.deleteContract(lstDeleteDTO);
                            onClick$btnSearch();
                            Clients.showNotification(message, "info", null, "middle_center", 3000);
                            return;
                        }
                    }
                });
            } else {
                Clients.showNotification(LanguageBundleUtils.getString("global.action.confirm.is_select"), "warning", null, "middle_center", 3000);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    public String formatDateTime(Date date) throws Exception {
        return StringUtils.formatDateTime(date);
    }

    public String getServiceGroup(String parentCode) {
        return this.mapParent.get(parentCode) != null ? this.mapParent.get(parentCode) : "";
    }

    public List<ServiceDTO> getListParent() {
        return listParent;
    }

    public void setListParent(List<ServiceDTO> listParent) {
        this.listParent = listParent;
    }

    public ServiceService getServiceService() {
        return serviceService;
    }

    public void setServiceService(ServiceService serviceService) {
        this.serviceService = serviceService;
    }

    public ListModelList<ContractDTO> getListContract() {
        return listContract;
    }

    public void setListContract(ListModelList<ContractDTO> listContract) {
        this.listContract = listContract;
    }

    public Listbox getLbxContract() {
        return lbxContract;
    }

    public void setLbxContract(Listbox lbxContract) {
        this.lbxContract = lbxContract;
    }

    public Grid getGridSearch() {
        return gridSearch;
    }

    public void setGridSearch(Grid gridSearch) {
        this.gridSearch = gridSearch;
    }

    public List<ContractDTO> getLstResult() {
        return lstResult;
    }

    public void setLstResult(List<ContractDTO> lstResult) {
        this.lstResult = lstResult;
    }

    public Textbox getTxtCodeContract() {
        return txtCodeContract;
    }

    public void setTxtCodeContract(Textbox txtCodeContract) {
        this.txtCodeContract = txtCodeContract;
    }

    public ContractService getContractService() {
        return contractService;
    }

    public void setContractService(ContractService contractService) {
        this.contractService = contractService;
    }

    public Textbox getTxtNumberContract() {
        return txtNumberContract;
    }

    public void setTxtNumberContract(Textbox txtNumberContract) {
        this.txtNumberContract = txtNumberContract;
    }

    public Textbox getTxtNameContract() {
        return txtNameContract;
    }

    public void setTxtNameContract(Textbox txtNameContract) {
        this.txtNameContract = txtNameContract;
    }

    public Textbox getTxtHeadOfService() {
        return txtHeadOfService;
    }

    public void setTxtHeadOfService(Textbox txtHeadOfService) {
        this.txtHeadOfService = txtHeadOfService;
    }

    public Combobox getCcbService() {
        return ccbService;
    }

    public void setCCbService(Combobox cbbService) {
        this.ccbService = cbbService;
    }

    public Combobox getCcbServiceGroup() {
        return ccbServiceGroup;
    }

    public void setCcbServiceGroup(Combobox ccbServiceGroup) {
        this.ccbServiceGroup = ccbServiceGroup;
    }

    public Combobox getCcbStatus() {
        return ccbStatus;
    }

    public void setCcbStatus(Combobox ccbStatus) {
        this.ccbStatus = ccbStatus;
    }

    public ListModelList<KeyValueBean> getKvbService() {
        return kvbService;
    }

    public void setKvbService(ListModelList<KeyValueBean> kvbService) {
        this.kvbService = kvbService;
    }

    public ListModelList<KeyValueBean> getKvbServiceGroup() {
        return kvbServiceGroup;
    }

    public void setKvbServiceGroup(ListModelList<KeyValueBean> kvbServiceGroup) {
        this.kvbServiceGroup = kvbServiceGroup;
    }

    public ListModelList<KeyValueBean> getKvbStatus() {
        return kvbStatus;
    }

    public void setKvbStatus(ListModelList<KeyValueBean> kvbStatus) {
        this.kvbStatus = kvbStatus;
    }

}
