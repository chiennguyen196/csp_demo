/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.composer.contract;

import com.viettel.csp.DTO.*;
import com.viettel.csp.entity.ContractHisEntity;
import com.viettel.csp.entity.UserEntity;
import com.viettel.csp.service.*;
import com.viettel.csp.util.*;
import com.viettel.eafs.util.SpringUtil;
import com.viettel.logincsp.AuthenticationService;
import com.viettel.plc.widget.FileInfo;
import com.viettel.plc.widget.IUploaderCallback;
import com.viettel.plc.widget.UploadComponent;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.ForwardEvent;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.*;

import java.io.File;
import java.util.*;
import java.util.logging.Level;

/**
 * @author chitv@gemvietnam.com
 */
public class ContractPopupEditComposer extends GenericForwardComposer<Component> implements
        IUploaderCallback {

    final static org.apache.log4j.Logger logger = org.apache.log4j.Logger
            .getLogger(ContractPopupEditComposer.class);
    private AuthenticationService authenticationService = SpringUtil.getBean("authenticationService", AuthenticationService.class);
    private Label txtCompanyName, txtAddressHeadOffice, txtPartnerCode, txtAddressTradingOffice, txtCompanyNameShort, txtPhone, txtRepName, dbFilingDate, txtContractStatus;
    private Combobox cbbContractStatus;
    private Textbox txtContenReason;
    private Textbox txtSearchBandBox;
    private Bandbox bdUser;
    private ListModelList<KeyValueBean> kvbContractStatus;
    private List<KeyValueBean> lstContractStatus;
    private String action;
    private Long contractId;
    private ContractDTO oldDTO;
    private PartnerDTO partNerDTO;
    private ContractFilesDTO contractFilesDTO;
    private ContractHisDTO contractHisDTO;
    private Button btnCreateCovenant, btnSendContractFiles, btnNoOKContractFiles, btnSendReplayContractFiles, btnOKContractFiles, btnUpdateContract, btnSendContractFilesToViettel;
    private Button btnCancleContract, btnDownloadFile;
    private Listbox lbxContractFiles, lbxContractFilesPartner, lbxContractFilesHistory, lbxContractUser;
    private ContractService contractService = SpringUtil.getBean("contractService", ContractService.class);
    private UserService userService = SpringUtil.getBean("userService", UserService.class);
    private PartnerService partnerService = SpringUtil.getBean("partnerService", PartnerService.class);
    private ContractStatusService contractStatusService = SpringUtil.getBean("contractStatusService", ContractStatusService.class);
    private ContractHisService contractHisService = SpringUtil.getBean("contractHisService", ContractHisService.class);
    private ContractFilesService contractFilesService = SpringUtil.getBean("contractFilesService", ContractFilesService.class);
    private ContractComposer contractComposer;
    private UploadComponent uploadComponent;
    private List<String> filesLocation;
    private List<FileInfo> filesInfo;
    private Row rowContractUser;

    public Listbox getLbxContractFiles() {
        return lbxContractFiles;
    }

    public void setLbxContractFiles(Listbox lbxContractFiles) {
        lbxContractFiles = lbxContractFiles;
    }

    public Listbox getLbxContractFilesPartner() {
        return lbxContractFilesPartner;
    }

    public void setLbxContractFilesPartner(Listbox lbxContractFilesPartner) {
        lbxContractFilesPartner = lbxContractFilesPartner;
    }

    public Listbox getLbxContractFilesHistory() {
        return lbxContractFilesHistory;
    }

    public void setLbxContractFilesHistory(Listbox lbxContractFilesHistory) {
        lbxContractFilesHistory = lbxContractFilesHistory;
    }

    public ListModelList<KeyValueBean> getKvbContractStatus() {
        return kvbContractStatus;
    }

    public void setKvbContractStatus(ListModelList<KeyValueBean> kvbContractStatus) {
        kvbContractStatus = kvbContractStatus;
    }

    public List<KeyValueBean> getLstContractStatus() {
        return lstContractStatus;
    }

    public void setLstContractStatus(List<KeyValueBean> lstContractStatus) {
        lstContractStatus = lstContractStatus;
    }

    @Override
    public void doAfterCompose(Component comp) {
        try {
            super.doAfterCompose(comp);
            if (arg.containsKey(ParamUtils.COMPOSER)) {
                contractComposer = (ContractComposer) arg.get(ParamUtils.COMPOSER);
            }
            if (arg.containsKey(ParamUtils.ACTION)) {
                action = arg.get(ParamUtils.ACTION).toString();
            }
            if (arg.containsKey("CONTRACT_ID")) {
                contractId = (Long) arg.get("CONTRACT_ID");
            }
            lbxContractFiles.setMultiple(true);
            uploadComponent.setUploaderCallback(this);
            setData();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            Clients
                    .showNotification(LanguageBundleUtils.getString("global.message.error"), "warning",
                            null,
                            "middle_center", 3000);
        }
    }

    public void bindDataCbbContractStatus(String valueSelect) throws Exception {
        try {
            List<ContractStatusDTO> lstContractStatusDTO = contractStatusService.findAll();
            lstContractStatus = new ArrayList<>();
            lstContractStatus.add(new KeyValueBean(ParamUtils.DEFAULT_VALUE_STR,
                    LanguageBundleUtils.getString("global.combobox.choose")));
            if (lstContractStatusDTO != null && lstContractStatusDTO.size() > 0) {
                for (ContractStatusDTO contractStatusDTO : lstContractStatusDTO) {
                    KeyValueBean key = new KeyValueBean();
                    key.setKey(contractStatusDTO.getCode());
                    key.setValue(contractStatusDTO.getName());
                    lstContractStatus.add(key);
                }
            }
            kvbContractStatus = new ListModelList(lstContractStatus);
            int i = 0;
            for (KeyValueBean itemParam : kvbContractStatus.getInnerList()) {
                if (itemParam.getKey() != null && String.valueOf(itemParam.getKey())
                        .equals(valueSelect)) {
                    kvbContractStatus.addToSelection(kvbContractStatus.getElementAt(i));
                    break;
                }
                i++;
            }
            cbbContractStatus.setModel(kvbContractStatus);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    public void setData() throws Exception {
        if (ParamUtils.ACTION_CREATE.equals(action)) {
            ((Window) self).setTitle(LanguageBundleUtils.getString("global.add"));
            txtCompanyName.setValue("");
            txtAddressHeadOffice.setValue("");
            txtPartnerCode.setValue("");
            txtAddressTradingOffice.setValue("");
            txtCompanyNameShort.setValue("");
            txtPhone.setValue("");
            dbFilingDate.setValue(null);
            txtRepName.setValue("");
        } else if (ParamUtils.ACTION_UPDATE.equals(action)) {
            ContractDTO objectSearch = new ContractDTO();
            objectSearch.setContractId(contractId);
            List<ContractDTO> list = contractService.getListContractProfile(objectSearch, authenticationService.getUserCredential());
            oldDTO = list.get(0);

            ((Window) self).setTitle(LanguageBundleUtils.getString("global.info.contract.profile"));
            if (oldDTO != null) {
                bindDataToTabContractFiles(oldDTO.getContractId());
                bindDataToTabContractHis(oldDTO.getContractId());
                bindDataToTabContractFilesPartner(oldDTO.getContractId());
                bindDataToBandboxUser();
                txtCompanyName.setValue(oldDTO.getCompanyName());
                txtAddressHeadOffice.setValue(oldDTO.getAddressHeadOffice());
                txtPartnerCode.setValue(oldDTO.getPartnerCode());
                txtAddressTradingOffice.setValue(oldDTO.getAddressTradingOffice());
                txtCompanyNameShort.setValue(oldDTO.getCompanyNameShort());
                txtPhone.setValue(oldDTO.getPhone());
                dbFilingDate.setValue(StringUtils.formatDate(oldDTO.getFilingDate()));
                txtRepName.setValue(oldDTO.getRepName());
                txtContractStatus.setValue(oldDTO.getContractStatusName());

                setVisibiale(oldDTO.getContractStatusCode());
            }
        }
    }

    private void setVisibiale(String contractStatus) {
        DepartmentDTO departmentDTO = userService.getDepartmentByOrganizationId();
        btnCreateCovenant.setVisible(false);
        btnSendContractFiles.setVisible(false);
        btnSendReplayContractFiles.setVisible(false);
        btnOKContractFiles.setVisible(false);
        btnNoOKContractFiles.setVisible(false);
        btnUpdateContract.setVisible(false);
        btnCancleContract.setVisible(false);
        btnDownloadFile.setVisible(false);
        uploadComponent.setVisible(false);
        rowContractUser.setVisible(false);
        uploadComponent.setUploadIcon(false);
        //btnSendContractFilesToViettel.setVisible(false);
        if (UserTokenUtils.getPersonType().equals(ParamUtils.USER_TYPE.CONTACT)
                || UserTokenUtils.getPersonType().equals(ParamUtils.USER_TYPE.PARTNER)) {
            switch (contractStatus) {
                case ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_NOT_OK_CONTRACT:
                    btnUpdateContract.setVisible(true);
                    uploadComponent.setVisible(true);
                    break;
            }
        } else if (ParamUtils.DEPARTMENT_VIETTEL_CSP.equalsIgnoreCase(departmentDTO.getCode())) {
            switch (contractStatus) {
                case ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_WAITING_OK_CONTRACT:
                    btnCancleContract.setVisible(true);
                    btnSendContractFiles.setVisible(true);
                    uploadComponent.setVisible(true);
                    rowContractUser.setVisible(true);
                    break;
                case ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_CANCLE_REGIS_OK_CONTRACT_FILES:
                    btnCancleContract.setVisible(true);
                    btnSendReplayContractFiles.setVisible(true);
                    uploadComponent.setVisible(true);
                    rowContractUser.setVisible(true);
                    break;
                case ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_REGIS_OK_CONTRACT_FILES:
                    btnCreateCovenant.setVisible(true);
                    break;
            }
        } else {
            switch (contractStatus) {
                case ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_WAITING_OK_CONTRACT_FILES:
                    btnOKContractFiles.setVisible(true);
                    btnNoOKContractFiles.setVisible(true);
                    break;
            }
        }
    }

    private void bindDataToBandboxUser() {
        try {
            UserEntity objectUserEntity = new UserEntity();
            objectUserEntity.setPersonType(ParamUtils.USER_TYPE.VIETTEL);
            List<UserDTO> userDTOs = userService.fillByPersonType(ParamUtils.USER_TYPE.VIETTEL, txtSearchBandBox.getValue());
            if (userDTOs != null) {
                ListModelList model = new ListModelList(userDTOs);
                lbxContractUser.setModel(model);
            } else {
                lbxContractUser.setModel(new ListModelList());
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    public void onDownloadFileContract(ForwardEvent evt) {
        evt.stopPropagation();
        try {
            Image btn = (Image) evt.getOrigin().getTarget();
            Listitem litem = (Listitem) btn.getParent().getParent();
            List<ContractFilesDTO> lstContractFilesResult = new ArrayList<>();
            lstContractFilesResult.add(litem.getValue());
            File file = new File(lstContractFilesResult.get(0).getPathFile());
            Filedownload.save(file, null);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(ContractComposer.class.getName())
                    .log(Level.SEVERE, null, ex);
        }
    }

    public void onViewDownloadFileContract(ForwardEvent evt) {
        evt.stopPropagation();
        try {
            Image btn = (Image) evt.getOrigin().getTarget();
            Listitem litem = (Listitem) btn.getParent().getParent();
            List<File> lstFile = new ArrayList();
            List<ContractFilesDTO> lstContractFilesResult = new ArrayList<>();
            lstContractFilesResult.add(litem.getValue());
            File file = new File("ttt", lstContractFilesResult.get(0).getFileName());
            File f = new File("demo", "demo.pdf");
            lstFile.add(file);
            Map<String, Object> argu = new HashMap();
            argu.put(ParamUtils.ACTION, ParamUtils.ACTION_CREATE);
            argu.put(ParamUtils.COMPOSER, this);
            argu.put(ParamUtils.DATA, lstFile);
            Window wd = (Window) Executions
                    .createComponents("/controls/widget/pdfFilesViewerPopup.zul", null, argu);
            wd.doModal();
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(ContractComposer.class.getName())
                    .log(Level.SEVERE, null, ex);
        }
    }

    private void bindDataToTabContractFiles(Long contractId) {
        try {
            ContractFilesDTO objectContractFiles = new ContractFilesDTO();
            objectContractFiles.setContractId(contractId);
            objectContractFiles.setFileType(ParamUtils.FILE_TYPE.FILE_TYPE_HS);
            List<ContractFilesDTO> lstContractFilesResult = contractFilesService.getListContractFiles(objectContractFiles);
            if (lstContractFilesResult != null && !lstContractFilesResult.isEmpty()) {
                ListModelList model = new ListModelList(lstContractFilesResult);
                lbxContractFiles.setModel(model);
            } else {
                lbxContractFiles.setModel(new ListModelList());
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    private void bindDataToTabContractFilesPartner(Long contractId) {
        try {
            ContractFilesDTO objectContractFiles = new ContractFilesDTO();
            objectContractFiles.setContractId(contractId);
            objectContractFiles.setFileType(ParamUtils.FILE_TYPE.FILE_TYPE_TOTRINH_HOSO);
            List<ContractFilesDTO> lstContractFilesResult = contractFilesService.getListContractFiles(objectContractFiles);
            if (lstContractFilesResult != null && !lstContractFilesResult.isEmpty()) {
                ListModelList model = new ListModelList(lstContractFilesResult);
                lbxContractFilesPartner.setModel(model);
            } else {
                lbxContractFilesPartner.setModel(new ListModelList());
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    private void bindDataToTabContractHis(Long contractId) {
        try {
            ContractHisDTO objectContractHis = new ContractHisDTO();
            objectContractHis.setContractId(contractId);
            List<ContractHisDTO> lstContractHisResult = contractHisService.getListContractHis(objectContractHis);
            if (lstContractHisResult != null && !lstContractHisResult.isEmpty()) {
                ListModelList model = new ListModelList(lstContractHisResult);
                lbxContractFilesHistory.setModel(model);
            } else {
                lbxContractFilesHistory.setModel(new ListModelList());
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    public ContractDTO getDataFromClient() {
        oldDTO.setCompanyName(txtCompanyName.getValue().trim().toLowerCase());

        oldDTO.setAddressHeadOffice(txtAddressHeadOffice.getValue().trim());

        oldDTO.setPartnerCode(txtPartnerCode.getValue().trim());

        oldDTO.setAddressTradingOffice(txtAddressTradingOffice.getValue().trim());

        oldDTO.setCompanyNameShort(txtCompanyNameShort.getValue().trim());

        oldDTO.setPhone(txtPhone.getValue().trim());

        oldDTO.setRepName(txtRepName.getValue().trim());
        if (StringUtils.isValidString(bdUser.getValue())) {
            UserEntity userEntity = new UserEntity();
            userEntity.setUserName(bdUser.getValue());
            List<UserEntity> lstUserName = userService.fillByUsername(userEntity.getUserName());
            oldDTO.setSignCtApprove(lstUserName.get(0).getId());
        }
        return oldDTO;
    }

    public PartnerDTO getDataFromClientPartner(ContractDTO contractDTO) {
        if (ParamUtils.ACTION_CREATE.equals(action)) {
            partNerDTO = new PartnerDTO();
            partNerDTO.setCompanyName(txtCompanyName.getValue().trim().toLowerCase());

            partNerDTO.setAddressHeadOffice(txtAddressHeadOffice.getValue().trim());

            partNerDTO.setPartnerCode(txtPartnerCode.getValue().trim());

            partNerDTO.setAddressTradingOffice(txtAddressTradingOffice.getValue().trim());

            partNerDTO.setCompanyNameShort(txtCompanyNameShort.getValue().trim());

            partNerDTO.setPhone(txtPhone.getValue().trim());

            partNerDTO.setRepName(txtRepName.getValue().trim());
        } else {
            if (ParamUtils.ACTION_UPDATE.equals(action)) {
                partNerDTO = new PartnerDTO();
                partNerDTO.setId(contractDTO.getPartnerId());
                partNerDTO.setCompanyName(contractDTO.getCompanyName());

                partNerDTO.setAddressHeadOffice(contractDTO.getAddressHeadOffice());

                partNerDTO.setPartnerCode(contractDTO.getPartnerCode());

                partNerDTO.setAddressTradingOffice(contractDTO.getAddressTradingOffice());

                partNerDTO.setCompanyNameShort(contractDTO.getCompanyNameShort());

                partNerDTO.setPhone(contractDTO.getPhone());

                partNerDTO.setRepName(contractDTO.getRepName());
            }
        }

        return partNerDTO;
    }

    public void onClick$btnDownloadFile(Listitem litem) {
        try {
            List<ContractFilesDTO> lstContractFilesResult = new ArrayList<>();
            lstContractFilesResult.add(litem.getValue());
            File file = new File(lstContractFilesResult.get(0).getPathFile());
            Filedownload.save(file, null);
        } catch (Exception ex) {
            Messagebox.show(LanguageBundleUtils.getMessage("messageUploadFileError"),
                    LanguageBundleUtils.getMessage("Messagebox.warning"), Messagebox.OK,
                    Messagebox.ERROR,
                    null);
        }
    }

//    public void onClick$btnOkContract() {
//        try {
//            String messageContract = "";
//            int flat = ParamUtils.TYPE_ACTION.WAITING_OK_CONTRACT_FILES;
//            if (validate()) {
//                oldDTO = getDataFromClient();
//
//                messageContract = contractService.update(oldDTO, action, flat);
//                ContractHisEntity hisEntity = new ContractHisEntity(contractId, LanguageBundleUtils.getString("message.profile.create.contract.profile.approve"));
//                contractHisService.insertEntity(hisEntity);
//                if (ParamUtils.SUCCESS.equals(messageContract)) {
//                    contractComposer.bindDataToGrid();
//                    Clients.showNotification(LanguageBundleUtils.getString("global.message.save.successful"), "info", null, "middle_center", 1000);
//                    self.detach();
//                } else {
//                    Clients.showNotification(messageContract, "warning", null, "middle_center", 3000);
//                }
//            }
//        } catch (Exception ex) {
//            logger.error(ex.getMessage(), ex);
//            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
//        }
//    }

    // Ho so khong hop le
    public void onClick$btnCancleContract() {
        try {
            if (validate()) {
                Map<String, Object> argu = new HashMap();
                argu.put(ParamUtils.ACTION, ParamUtils.ACTION_UPDATE);
                argu.put("CONTRACT_ID", oldDTO.getContractId());
                argu.put(ParamUtils.COMPOSER, this);
                Window wd = (Window) Executions.createComponents("/controls/contract/contractReasonPopup.zul", null, argu);
                wd.doModal();
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    //Tu choi phe duyet ho so hop tac
    public void onClick$btnNoOKContractFiles() {
        try {
            if (validate()) {
                Map<String, Object> argu = new HashMap();
                argu.put(ParamUtils.ACTION, ParamUtils.ACTION_UPDATE);
                argu.put("CONTRACT_ID", oldDTO.getContractId());
                argu.put(ParamUtils.COMPOSER, this);
                Window wd = (Window) Executions.createComponents("/controls/contract/contractReasonCancelApprovePopup.zul", null, argu);
                wd.doModal();
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients
                    .showNotification(LanguageBundleUtils.getString("global.message.error"), "warning",
                            null,
                            "middle_center", 3000);
        }
    }

    //Phe duyet ho so hop tac
    public void onClick$btnOKContractFiles() {
        try {
            String message = "";
            int flat = ParamUtils.TYPE_ACTION.OK_SEND_CONTRACT_FILES;
            ContractHisEntity hisEntity = new ContractHisEntity(contractId, LanguageBundleUtils.getString("message.profile.ok.contract.profile.approve"));
            contractHisService.insertEntity(hisEntity);
            message = contractService.updateContract(oldDTO, action, flat);
            if (ParamUtils.SUCCESS.equals(message)) {
                contractComposer.bindDataToGrid();
                Clients.showNotification(LanguageBundleUtils.getString("global.message.update.successful"), "info", null, "middle_center", 1000);
                self.detach();
            } else {
                Clients.showNotification(message, "warning", null, "middle_center", 3000);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients
                    .showNotification(LanguageBundleUtils.getString("global.message.error"), "warning",
                            null,
                            "middle_center", 3000);
        }
    }

    //Gui to trinh ho so
    public void onClick$btnSendContractFiles() {
        try {
            String message = "";
            int flat = ParamUtils.TYPE_ACTION.WAITING_OK_CONTRACT_FILES;
            uploadComponent.onClick$doSave();
            if (validateFile()) {
                if (!StringUtils.isValidString(bdUser.getValue())) {
                    Clients.showNotification(LanguageBundleUtils.getMessage("global.validate.notnull", LanguageBundleUtils.getString("contract.select.account.approve.profile")), "warning", null, "middle_center", 3000);
                    return;
                }
                oldDTO = getDataFromClient();
                if (filesInfo.size() > 0) {
                    for (int i = 0; i < filesInfo.size(); i++) {
                        contractFilesDTO = getDataFromClientContractFiles(oldDTO, filesInfo.get(i));
                        oldDTO.setContractStatus(ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_WAITING_OK_CONTRACT_FILES);
                        contractFilesService.insert(oldDTO, contractFilesDTO);
                    }
                }
                ContractHisEntity hisEntity = new ContractHisEntity(contractId, LanguageBundleUtils.getString("message.profile.send.contract.profile.approve"));
                contractHisService.insertEntity(hisEntity);
                message = contractService.updateContract(oldDTO, action, flat);
                if (ParamUtils.SUCCESS.equals(message)) {
                    contractComposer.bindDataToGrid();
                    Clients.showNotification(LanguageBundleUtils.getString("global.message.update.successful"), "info", null, "middle_center", 1000);
                    self.detach();
                } else {
                    Clients.showNotification(message, "warning", null, "middle_center", 3000);
                }
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    //Gui lai to trinh ho so hop tac
    public void onClick$btnSendReplayContractFiles() {
        try {
            String message = "";
            int flat = ParamUtils.TYPE_ACTION.WAITING_OK_CONTRACT_FILES;
            uploadComponent.onClick$doSave();
            if (validateFile()) {
                if (!StringUtils.isValidString(bdUser.getValue())) {
                    Clients.showNotification(LanguageBundleUtils.getMessage("global.validate.notnull", LanguageBundleUtils.getString("contract.select.account.approve.profile")), "warning", null, "middle_center", 3000);
                    return;
                }
                oldDTO = getDataFromClient();
                contractFilesService.updateFilesNotUsing(oldDTO, ParamUtils.FILE_TYPE.FILE_TYPE_TOTRINH_HOSO);
                if (filesInfo.size() > 0) {
                    for (int i = 0; i < filesInfo.size(); i++) {
                        contractFilesDTO = getDataFromClientContractFiles(oldDTO, filesInfo.get(i));
                        oldDTO.setContractStatus(ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_CANCLE_REGIS_OK_CONTRACT_FILES);
                        contractFilesService.insert(oldDTO, contractFilesDTO);
                    }
                }
                message = contractService.updateContract(oldDTO, action, flat);
                ContractHisEntity hisEntity = new ContractHisEntity(contractId, LanguageBundleUtils.getString("message.profile.resend.contract.profile.approve"));
                contractHisService.insertEntity(hisEntity);
                if (ParamUtils.SUCCESS.equals(message)) {
                    contractComposer.bindDataToGrid();
                    Clients
                            .showNotification(
                                    LanguageBundleUtils.getString("global.message.update.successful"),
                                    "info", null, "middle_center", 1000);
                    self.detach();
                } else {
                    Clients.showNotification(message, "warning", null, "middle_center", 3000);
                }
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients
                    .showNotification(LanguageBundleUtils.getString("global.message.error"), "warning",
                            null,
                            "middle_center", 3000);
        }
    }

//    public void onClick$btnSendContractFilesToViettel() {
//        try {
//            String messageContract = "";
//            String messagePartner = "";
//            String messageHis = "";
//            int flat = 16;
//            uploadComponent.onClick$doSave();
//            if (validateFile()) {
//                for (int i = 0, total = filesInfo.size(); i < total; i++) {
//                    oldDTO = getDataFromClient();
//                    contractFilesDTO = getDataFromClientContractFiles(oldDTO,
//                            filesInfo.get(i));
//                    messageHis = contractFilesService
//                            .insert(oldDTO, contractFilesDTO);
//                    messageContract = contractService.update(oldDTO, action, flat);
//                }
//                if (ParamUtils.SUCCESS.equals(messageContract)) {
//                    contractComposer.bindDataToGrid();
//                    Clients
//                            .showNotification(
//                                    LanguageBundleUtils.getString("global.message.create.successful"),
//                                    "info", null, "middle_center", 1000);
//                    self.detach();
//                } else {
//                    Clients
//                            .showNotification(messageContract, "warning", null, "middle_center", 3000);
//                }
//            }
//        } catch (Exception ex) {
//            logger.error(ex.getMessage(), ex);
//            Clients
//                    .showNotification(LanguageBundleUtils.getString("global.message.error"), "warning",
//                            null,
//                            "middle_center", 3000);
//        }
//    }

    //Gui lai ho so hop tac
    public void onClick$btnUpdateContract() {
        try {
            String messageContract = "";
            int flat = 9;
            uploadComponent.onClick$doSave();
            if (validateFile()) {
                oldDTO = getDataFromClient();
                contractFilesService.updateFilesNotUsing(oldDTO, ParamUtils.FILE_TYPE.FILE_TYPE_HS);
                if (filesInfo.size() > 0) {
                    for (int i = 0; i < filesInfo.size(); i++) {
                        contractFilesDTO = getDataFromClientContractFiles(oldDTO, filesInfo.get(i));
                        contractFilesService.insert(oldDTO, contractFilesDTO);
                    }
                }
                ContractHisEntity hisEntity = new ContractHisEntity(contractId, LanguageBundleUtils.getString("message.profile.resend.contract.profile"));
                contractHisService.insertEntity(hisEntity);
                messageContract = contractService.updateContract(oldDTO, action, flat);
                if (ParamUtils.SUCCESS.equals(messageContract)) {
                    contractComposer.bindDataToGrid();
                    Clients.showNotification(LanguageBundleUtils.getString("global.message.update.successful"), "info", null, "middle_center", 1000);
                    self.detach();
                } else {
                    Clients.showNotification(messageContract, "warning", null, "middle_center", 3000);
                }
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

//    public void onClick$btnAdd() {
//        try {
//            String messageContract = "";
//            String messagePartner = "";
//            int flat = 0;
//            if (validate()) {
//                oldDTO = getDataFromClient();
//                partNerDTO = getDataFromClientPartner(oldDTO);
//                messageContract = contractService.insert(oldDTO, flat);
//                messagePartner = partnerService.insert(partNerDTO);
//                if (ParamUtils.SUCCESS.equals(messagePartner)) {
//                    contractComposer.bindDataToGrid();
//                    Clients.showNotification(LanguageBundleUtils.getString("global.message.create.successful"), "info", null, "middle_center", 1000);
//                    self.detach();
//                } else {
//                    Clients.showNotification(messageContract, "warning", null, "middle_center", 3000);
//                }
//            }
//        } catch (Exception ex) {
//            logger.error(ex.getMessage(), ex);
//            Clients
//                    .showNotification(LanguageBundleUtils.getString("global.message.error"), "warning",
//                            null,
//                            "middle_center", 3000);
//        }
//    }

    public void onClick$btnChooseBandBox() {
        try {
            if (lbxContractUser.getSelectedItem() == null && lbxContractUser.getSelectedItem().getValue() == null) {
                Clients.showNotification(LanguageBundleUtils.getString("global.message.choose.row"), "warning", null, "middle_center", 3000);
                return;
            }
            UserDTO userDTO = lbxContractUser.getSelectedItem().getValue();
            bdUser.setValue(userDTO.getUserName());
            bdUser.close();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients
                    .showNotification(LanguageBundleUtils.getString("global.message.error"), "warning",
                            null,
                            "middle_center", 3000);
        }
    }

    public void onClick$btnSearchBandBox() {
        try {
            UserEntity objectUserEntity = new UserEntity();
            objectUserEntity.setPersonType(ParamUtils.USER_TYPE.VIETTEL);
            List<UserDTO> userDTOs = userService.fillByPersonType(ParamUtils.USER_TYPE.VIETTEL, txtSearchBandBox.getValue());
            if (userDTOs != null) {
                ListModelList model = new ListModelList(userDTOs);
                lbxContractUser.setModel(model);
            } else {
                lbxContractUser.setModel(new ListModelList());
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients
                    .showNotification(LanguageBundleUtils.getString("global.message.error"), "warning",
                            null,
                            "middle_center", 3000);
        }
    }

    public void onClick$btnCreateCovenant() {
        try {
            Map<String, Object> argu = new HashMap();
            argu.put(ParamUtils.ACTION, ParamUtils.ACTION_CREATE);
            argu.put("ID", oldDTO.getContractId());
            argu.put(ParamUtils.COMPOSER, this);
            Window wd;
            wd = (Window) Executions.createComponents("/controls/contract/contractCovenantFile.zul", null, argu);
            wd.doModal();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    public void onClick$btnClose() {
        try {
            self.detach();
            contractComposer.bindDataToGrid();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning",  null,  "middle_center", 3000);
        }
    }

    @Override
    public void uploadedFilesLocationCallback(List<String> filesLocation) {
        this.filesLocation = filesLocation;
        System.out.println(filesLocation);
    }

    @Override
    public void uploadedFilesInfoCallback(List<FileInfo> filesInfo) {
        this.filesInfo = filesInfo;
        System.out.println(filesInfo);
    }

    public ContractHisDTO getDataFromClientContractHis(ContractDTO contractDTO) {

        contractHisDTO = new ContractHisDTO();
        contractHisDTO.setContractId(oldDTO.getContractId());
        contractHisDTO.setContent(txtContenReason.getValue());
        return contractHisDTO;
    }

    public ContractFilesDTO getDataFromClientContractFiles(ContractDTO contractDTO,
                                                           FileInfo fileInfo) {

        contractFilesDTO = new ContractFilesDTO();
        contractFilesDTO.setContractId(oldDTO.getContractId());
        contractFilesDTO.setFileName(new File(fileInfo.getLocation()).getName());
        contractFilesDTO.setPathFile(new File(fileInfo.getLocation()).getPath());
        contractFilesDTO.setDescription(fileInfo.getDescription());
        return contractFilesDTO;
    }

    private boolean validateFile() {
        if (this.filesInfo == null) {
            Clients.showNotification(LanguageBundleUtils.getString("global.message.errorFile"), "warning", null, "middle_center", 3000);
            return false;
        }
        return true;
    }

    private boolean validate() {

        return true;
    }

    public String formatDateTime(Date date) throws Exception {
        return StringUtils.formatDateTime(date);
    }

    public String getContractStatusHisName(String paramType) throws Exception {
        ContractStatusDTO contractStatusDTO = contractStatusService.getObjectDTOByCode(paramType);
        return contractStatusDTO.getName();
    }

    public void onClick$btnUpload() {
        try {
            Map<String, Object> argu = new HashMap();
            argu.put(ParamUtils.ACTION, ParamUtils.ACTION_CREATE);
            argu.put(ParamUtils.COMPOSER, this);
            Window wd = (Window) Executions
                    .createComponents("/controls/widget/uploadPopup.zul", null, argu);
            wd.doModal();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    public String getUserById(Long userId) {
        try {
            UserEntity usrUser = userService.getUserByID(userId);
            return usrUser.getFullName();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return "";
    }

}
