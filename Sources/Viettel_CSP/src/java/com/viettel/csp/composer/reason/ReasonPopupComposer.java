package com.viettel.csp.composer.reason;

import com.viettel.csp.DTO.ReasonDTO;
import com.viettel.csp.service.ReasonService;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.util.StringUtils;
import com.viettel.eafs.util.SpringUtil;
import org.hibernate.HibernateException;
import org.hibernate.exception.ConstraintViolationException;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Textbox;

/**
 * Created by admin on 7/10/2017.
 */
public class ReasonPopupComposer extends GenericForwardComposer<Component> {
    @Wire
    private Textbox txtReasonCode, txtReasonName, txtReasonDescription;
    @Wire
    private Combobox cbbReasonType;

    private ReasonDTO reasonDTO;

    private ReasonService reasonService = SpringUtil.getBean("reasonService", ReasonService.class);

    private ReasonComposer reasonComposer;

    private String action;

    public void doAfterCompose(Component component) throws Exception {
        super.doAfterCompose(component);
        this.reasonDTO = (ReasonDTO) arg.get("OBJECT");
        this.reasonComposer = (ReasonComposer) arg.get(ParamUtils.COMPOSER);
        if(arg.containsKey(ParamUtils.ACTION)) {
            this.action = arg.get(ParamUtils.ACTION).toString();
        }
        if(ParamUtils.ACTION_UPDATE.equals(action)){
            this.txtReasonCode.setDisabled(true);
        }

        showData();
    }

    protected void showData(){
        this.txtReasonCode.setValue(reasonDTO.getCode());
        this.txtReasonName.setValue(reasonDTO.getName());
        this.txtReasonDescription.setValue(reasonDTO.getDescription());
        this.cbbReasonType.setValue(reasonDTO.getType());
    }

    public void onClick$btnSavePopup(){

        try {
            if (validate()) {
                ReasonDTO reasonDTO = getDataFromClient();

                if (ParamUtils.ACTION_CREATE.equals(action)) {
                    reasonService.save(reasonDTO);
                } else if (ParamUtils.ACTION_UPDATE.equals(action)) {
                    reasonService.update(reasonDTO);
                }
                reasonComposer.showDataToGrip();

                self.detach();
                Clients.showNotification("Save thành công!",
                        "info", null, "middle_center", 1000);
            }
        } catch (Exception e){
            String msg = "";
            System.out.println(e.toString());
            if(e instanceof ConstraintViolationException){
                msg = "Trùng khóa chính";
            }
            Clients.showNotification("Có lỗi xảy ra!\n\r" + msg,
                    "error", null, "middle_center", 1000);
        }
    }

    protected Boolean validate(){
        if(!StringUtils.isValidString(txtReasonCode.getValue())){
            Clients.wrongValue(txtReasonCode, LanguageBundleUtils.getMessage("global.validate.notnull", LanguageBundleUtils.getString("reason.code")));
            return false;
        }
        if(!StringUtils.isValidString(txtReasonName.getValue())){
            Clients.wrongValue(txtReasonCode, LanguageBundleUtils.getMessage("global.validate.notnull", LanguageBundleUtils.getString("reason.name")));
            return false;
        }
        if(cbbReasonType.getSelectedIndex() <= 0){
            Clients.wrongValue(cbbReasonType, LanguageBundleUtils.getMessage("global.validate.notchoose", LanguageBundleUtils.getString("reason.type")));
            return false;
        }
        return true;
    }

    protected ReasonDTO getDataFromClient(){
        ReasonDTO reasonDTO = new ReasonDTO();
        reasonDTO.setCode(txtReasonCode.getValue().trim().toUpperCase());
        reasonDTO.setName(txtReasonName.getValue().trim());
        reasonDTO.setDescription(txtReasonDescription.getValue().trim());
        reasonDTO.setType(cbbReasonType.getSelectedItem().getValue());
        return reasonDTO;
    }


    public void onClick$btnClosePopup(){
        self.detach();
    }

}
