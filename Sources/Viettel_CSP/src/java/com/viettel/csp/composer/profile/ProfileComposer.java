/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.viettel.csp.composer.profile;

import com.viettel.csp.composer.contact.ContactPopupComposer;
import com.viettel.csp.service.ContactService;
import com.viettel.csp.service.UserService;
import com.viettel.csp.util.CtrlValidator;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.UserTokenUtils;
import com.viettel.eafs.util.SpringUtil;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 * @author StormSpirit
 */
public class ProfileComposer extends GenericForwardComposer<Component> {
    private Textbox newPassword;
    private Textbox reNewPassword;
    private Textbox oldPassword;
    private Window passWindow;
    private UserService userService = SpringUtil.getBean("userService", UserService.class);
    private ContactService contactService = SpringUtil.getBean("contactService", ContactService.class);
    final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ProfileComposer.class);

    public void onClick$btnSavePassword() {
        try {
            if (!validate()) {
                return;
            }

            String username = UserTokenUtils.getUserName();
            String status = contactService.updatePassword(username, newPassword.getValue(), oldPassword.getValue());

            if (status.equals("success")) {
                Messagebox.show(LanguageBundleUtils.getString("global.message.password.successful"), "info", Messagebox.OK, Messagebox.INFORMATION);
            } else {
                Messagebox.show(LanguageBundleUtils.getString("global.message.password.wrong"), "warning", Messagebox.OK, Messagebox.EXCLAMATION);
            }

            passWindow.detach();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    private boolean validate() {
        if (!CtrlValidator.checkValidTextbox(newPassword, true, true, LanguageBundleUtils.getString("contact.dialog.password.new"))) {
            return false;
        }

        if (!CtrlValidator.checkLength(newPassword, true, 6L, LanguageBundleUtils.getString("contact.dialog.password.new"))) {
            return false;
        }

        if (!CtrlValidator.checkValidTextbox(reNewPassword, true, true, LanguageBundleUtils.getString("contact.dialog.password.reInput"))) {
            return false;
        }
        if (!CtrlValidator.checkLength(newPassword, true, 6L, LanguageBundleUtils.getString("contact.dialog.password.new"))) {
            return false;
        }

        if (!newPassword.getValue().equals(reNewPassword.getValue())) {
            Messagebox.show(LanguageBundleUtils.getString("global.message.password.error"), "warning", Messagebox.OK, Messagebox.EXCLAMATION);
            return false;
        }
        return true;
    }

    public void onClick$btnClosePopup() {
        self.detach();
    }

}
