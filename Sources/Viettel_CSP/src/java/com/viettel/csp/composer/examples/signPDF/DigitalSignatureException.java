package com.viettel.csp.composer.examples.signPDF;

/*
 * Copyright (C) 2010 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */


/**
 *
 * @author sonnt38@viettel.com.vn
 * @since since_text
 * @version 1.0
 */
public class DigitalSignatureException extends Exception{

    public DigitalSignatureException() {
        super();
    }

    public DigitalSignatureException(String message) {
        super(message);
    }

    public DigitalSignatureException(Throwable cause) {
        super(cause);
    }

    public DigitalSignatureException(String message, Throwable cause) {
        super(message, cause);
    }


}

