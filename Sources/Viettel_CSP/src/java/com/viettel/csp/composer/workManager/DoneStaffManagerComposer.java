package com.viettel.csp.composer.workManager;

import com.viettel.csp.DTO.*;
import com.viettel.csp.entity.MessageEntity;
import com.viettel.csp.entity.TaskFilesEntity;
import com.viettel.csp.service.*;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.util.StringUtils;
import com.viettel.csp.util.UserTokenUtils;
import com.viettel.eafs.util.SpringUtil;
import com.viettel.plc.widget.FileInfo;
import com.viettel.plc.widget.IUploaderCallback;
import com.viettel.plc.widget.UploadComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.ForwardEvent;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.*;

import java.io.File;
import java.util.*;

public class DoneStaffManagerComposer extends GenericForwardComposer<Component> implements IUploaderCallback {

    final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(DoneStaffManagerComposer.class);
    private Grid grdContactPoint;
    private Listbox lbxContractFiles;
    private Listbox lbxTask;
    private Listbox lbxTaskFiles;
    private List<Integer> indexes = new ArrayList();
    private Map<String, String> mapParent = new HashMap<>();
    private List<ServiceDTO> listParent;
    private Label lbContracNumber, lbNamePartner, lbServiceGroup, lbServiceGroupName, lbService, lbServiceName, lbHeadOfService, lbDateBegin, lbDateEnd, lbContracType, lbContracTypeName, lbContractStatusName, lbContractStatus;

    private TasksDTO tasksDTO;
    private List<ContactPointDTO> contractPointDTOs;
    private List<ContractFilesDTO> contractFilesDTOs;
    private ContractFilesService contractFilesService = SpringUtil.getBean("contractFileService", ContractFilesService.class);
    private ContactPointService contactPointService = SpringUtil.getBean("contactPointService", ContactPointService.class);
    private ContractService contractService = SpringUtil.getBean("contractService", ContractService.class);
    private ServiceService serviceService = SpringUtil.getBean("serviceService", ServiceService.class);
    private TaskFilesSevice taskFilesSevice = SpringUtil.getBean("taskFilesService", TaskFilesSevice.class);
    private TasksService tasksService = SpringUtil.getBean("tasksService", TasksService.class);
    private MessageService messageService=SpringUtil.getBean("messageService", MessageService.class);

    @Autowired
    private TaskManagerComposer taskManagerComposer;
    private UploadComponent uploadComponent;
    private List<FileInfo> filesInfos;
    private List<String> filesLocation;
    private List<TaskFilesEntity> taskFilesEntities;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        if (arg.containsKey("TASK_DETAIL")) {
            this.lbxTask = (Listbox) arg.get("TASK_DETAIL");
        }
        if (arg.containsKey("COMPOSER")) {
            this.taskManagerComposer = (TaskManagerComposer) arg.get("COMPOSER");
        }
        if (arg.containsKey("LIST_ACTION_INDEX")) {
            this.indexes = (List<Integer>) arg.get("LIST_ACTION_INDEX");
        }
        this.listParent = serviceService.getListWithNullParent();
        for (ServiceDTO ins : this.listParent) {
            mapParent.put(ins.getCode(), ins.getName());
        }

        ((Window) self).setTitle(LanguageBundleUtils.getString("global.edit"));
        tasksDTO = lbxTask.getItemAtIndex(indexes.get(0)).getValue();
        this.bindData(tasksDTO);
        uploadComponent.setUploaderCallback(this);
    }

    public void onClick$btnOKTask() throws Exception {
        try {
            tasksDTO.setStatus(ParamUtils.TASKS_STATUS.DONE_TASK);
            String message = this.tasksService.updateDoneTask(tasksDTO);

            if (ParamUtils.SUCCESS.equals(message)) {
                Clients.showNotification(LanguageBundleUtils.getString("global.message.create.successful"), "info", null, "middle_center", 1000);
                messageService.insertMessage(getMessageEntity(contractService.getContractEntity(tasksDTO.getContactId()).getDeployUserId(),tasksDTO.getTaskUserId(),LanguageBundleUtils.getString("task.message.Active"), LanguageBundleUtils.getString("task.message.Active")));
                this.taskManagerComposer.bindData();
                self.detach();
            } else {
                Clients.showNotification("Thất bại cập nhật", "warning", null, "middle_center", 3000);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    public void onClick$btnSaveFiles() {
        try {
            String message = "";
            this.uploadComponent.onClick$doSave();
            if (this.validateFile()) {
                if (filesInfos.size() > 0) {
                    TaskFilesDTO taskFilesDTO;
                    for (FileInfo fileInfo : filesInfos) {
                        taskFilesDTO = new TaskFilesDTO();
                        taskFilesDTO.setDescription(fileInfo.getDescription());
                        taskFilesDTO.setFileName(new File(fileInfo.getLocation()).getName());
                        taskFilesDTO.setPathFile(new File(fileInfo.getLocation()).getPath());
                        taskFilesDTO.setTaskId(this.tasksDTO.getId());
                        message = taskFilesSevice.insert(taskFilesDTO);
                    }
                }
                if (ParamUtils.SUCCESS.equals(message)) {
                    Clients.showNotification(LanguageBundleUtils.getString("global.message.create.successful"), "info", null, "middle_center", 1000);
                    //self.detach();
                    this.bindDataTaskFiles();
                } else {
                    Clients.showNotification(message, "warning", null, "middle_center", 3000);
                }
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    private boolean validateFile() {
        if (this.filesInfos == null || this.filesInfos.isEmpty()) {
            Clients.showNotification(LanguageBundleUtils.getString("global.message.errorFile"), "warning", null, "middle_center", 3000);
            return false;
        }
        return true;
    }

    public void bindDataTaskFiles() throws Exception {
        taskFilesEntities = this.taskFilesSevice.findAlles(this.tasksDTO.getId());
        if (this.taskFilesEntities != null && !this.taskFilesEntities.isEmpty()) {
            ListModelList model = new ListModelList(this.taskFilesEntities);
            model.setMultiple(false);
            lbxTaskFiles.setModel(model);
        } else {
            lbxTaskFiles.setModel(new ListModelList());
        }
    }

    private void bindData(TasksDTO tasksDTO) throws Exception {
        Long contactId = tasksDTO.getContactId();
        ContractDTO contDTO = new ContractDTO();
        contDTO.setContractId(contactId);
        try {
            if (arg.containsKey("TASK_DETAIL")) {
                this.lbxTask = (Listbox) arg.get("TASK_DETAIL");
            }
            if (arg.containsKey("COMPOSER")) {
                this.taskManagerComposer = (TaskManagerComposer) arg.get("COMPOSER");
            }
            if (arg.containsKey("LIST_ACTION_INDEX")) {
                this.indexes = (List<Integer>) arg.get("LIST_ACTION_INDEX");
            }
            this.tasksDTO = lbxTask.getItemAtIndex(indexes.get(0)).getValue();
            List<ContractDTO> listContractForWorkManager = this.contractService.getListContractForWorkManager(contDTO);
            if (listContractForWorkManager == null || listContractForWorkManager.isEmpty()) {
                logger.info("listContractForWorkManager is null");
                return;
            }
            ContractDTO contractDTO = listContractForWorkManager.get(0);
            this.lbContracNumber.setValue(contractDTO.getContractNumber());
            this.lbNamePartner.setValue(contractDTO.getCompanyName());
            this.lbServiceGroupName.setValue(mapParent.get(contractDTO.getServiceGroup()) != null ? mapParent.get(contractDTO.getServiceGroup()) : "");
            this.lbService.setValue(contractDTO.getServiceCode());
            this.lbServiceName.setValue(contractDTO.getServiceName());
            this.lbDateBegin.setValue(StringUtils.formatDateTime(contractDTO.getStartDate()));
            this.lbDateEnd.setValue(StringUtils.formatDateTime(contractDTO.getEndDate()));
            this.lbContracTypeName.setValue(contractDTO.getContractTypeName());
            this.lbContractStatusName.setValue(contractDTO.getContractStatusName());

            this.contractPointDTOs = this.contactPointService.getList(contractDTO.getContractId());
            if (this.contractPointDTOs != null && !this.contractPointDTOs.isEmpty()) {
                ListModelList model = new ListModelList(this.contractPointDTOs);
                model.setMultiple(false);
                grdContactPoint.setModel(model);
            } else {
                grdContactPoint.setModel(new ListModelList());
            }
            ContractFilesDTO contractFilesDTO = new ContractFilesDTO();
            contractFilesDTO.setContractId(contactId);
            this.contractFilesDTOs = this.contractFilesService.getListContractFiles(contractFilesDTO);
            if (this.contractFilesDTOs != null && !this.contractFilesDTOs.isEmpty()) {
                ListModelList model = new ListModelList(this.contractFilesDTOs);
                model.setMultiple(false);
                lbxContractFiles.setModel(model);
            } else {
                lbxContractFiles.setModel(new ListModelList());
            }
            this.bindDataTaskFiles();
        } catch (Exception e) {
            logger.error("", e);
            throw e;
        }
    }

    public void onDownloadFileTask(ForwardEvent evt) {
        evt.stopPropagation();
        try {
            Image btn = (Image) evt.getOrigin().getTarget();
            Listitem listitem = (Listitem) btn.getParent().getParent();
            File file = new File(((TaskFilesEntity) listitem.getValue()).getPathFile());
            Filedownload.save(file, null);
        } catch (Exception ex) {
            logger.error(ex.getMessage(),ex);
            Clients.showNotification("Lỗi tải tệp tin", "warning", null, "middle_center", 3000);
        }
    }

    public void onDownloadFileContract(ForwardEvent evt) {
        evt.stopPropagation();
        try {
            Image btn = (Image) evt.getOrigin().getTarget();
            Listitem listitem = (Listitem) btn.getParent().getParent();
            File file = new File(((ContractFilesDTO) listitem.getValue()).getPathFile());
            Filedownload.save(file, null);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification("Lỗi tải tệp tin", "warning", null, "middle_center", 3000);
        }
    }

    @Override
    public void uploadedFilesLocationCallback(List<String> filesLocation) {
        this.filesLocation = filesLocation;
        logger.info(filesLocation);
    }

    @Override
    public void uploadedFilesInfoCallback(List<FileInfo> filesInfos) {
        this.filesInfos = filesInfos;
        logger.info(filesInfos);
    }

    public String formatDateTime(Date date) throws Exception {
        return StringUtils.formatDateTime(date);
    }
    public MessageEntity getMessageEntity(Long sentUserId,Long receiveUserId, String content, String description){
        MessageEntity messageEntity=new MessageEntity();
        messageEntity.setSendUserId(sentUserId);
        messageEntity.setReceiveUserId(receiveUserId);
        messageEntity.setStatus(0);
        messageEntity.setContent(content);
        messageEntity.setDescription(description);
        messageEntity.setCreateBy(sentUserId);
        messageEntity.setCreatAt(new Date());
        return messageEntity;
    }
}
