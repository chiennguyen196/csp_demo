package com.viettel.csp.composer.contractManagement;

import com.viettel.csp.DTO.ContactPointDTO;
import com.viettel.csp.DTO.ContractDTO;
import com.viettel.csp.DTO.ContractFilesDTO;
import com.viettel.csp.DTO.ContractHisDTO;
import com.viettel.csp.service.*;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.util.StringUtils;
import com.viettel.eafs.util.SpringUtil;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.ForwardEvent;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.*;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by admin on 6/15/2017.
 */
public class ContracManagerRegisterViettelBComposer extends GenericForwardComposer<Component> {

    private Label txtContractNumber, txtCompanyName, txtHeadOfService, txtServiceCode, dbStartDate, dbEndDate, txtContractStatus, txtContractType, txtCtSignForm;
    private ContractDTO oldDTO;
    private String action;
    Long contract_id;
    List<ContactPointDTO> contractPointDTOs;
    private Grid grdContactPoint;
    private List<ContractFilesDTO> lstContractFilesDTO = new ArrayList<>();
    private List<ContractHisDTO> lstContractHisDTO = new ArrayList<>();
    private Listbox lbxViewAddContractManagement, lbxContractHis;
    final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ContractManagementAddFileComposer.class);
    private final ContactTypeService contactTypeService = SpringUtil.getBean("contactTypeService", ContactTypeService.class);
    private final ContactService contactService = SpringUtil.getBean("contactService", ContactService.class);
    private final ContractFilesService contractFilesService = SpringUtil.getBean("contractFilesService", ContractFilesService.class);
    private final ContractHisService contractHisService = SpringUtil.getBean("contractHisService", ContractHisService.class);
    private final ContactPointService contactPointService = SpringUtil.getBean("contactPointService", ContactPointService.class);
    private final ContractService contractService = SpringUtil.getBean("contractService", ContractService.class);
    private ContractManagementComposer contractManagementComposer;

    public ContractManagementComposer getContractManagementComposer() {
        return contractManagementComposer;
    }

    public void setContractManagementComposer(ContractManagementComposer contractManagementComposer) {
        this.contractManagementComposer = contractManagementComposer;
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        if (this.arg.containsKey("CONTRACT_ID")) {
            this.contract_id = (Long) this.arg.get("CONTRACT_ID");
        }
        if (this.arg.containsKey(ParamUtils.ACTION)) {
            this.action = this.arg.get(ParamUtils.ACTION).toString();
        }
        if (this.arg.containsKey(ParamUtils.COMPOSER)) {
            this.contractManagementComposer = (ContractManagementComposer) this.arg.get(ParamUtils.COMPOSER);
        }
        setData();
        loadLBXContractFiles(contract_id);
        loadLBXContractHis(contract_id);
    }

    public void setData() throws Exception {
        ContractDTO objectSearch = new ContractDTO();
        objectSearch.setContractId(this.contract_id);
        List<ContractDTO> list = this.contractService.getListContract(objectSearch);
        this.oldDTO = list.get(0);
        ((Window) this.self).setTitle(LanguageBundleUtils.getString("contractManagent.register"));
        if (this.oldDTO != null) {
            this.txtCompanyName.setValue(this.oldDTO.getCompanyName());
            this.txtContractNumber.setValue(this.oldDTO.getAddressHeadOffice());
            this.txtHeadOfService.setValue(this.oldDTO.getPartnerCode());
            this.txtServiceCode.setValue(this.oldDTO.getAddressTradingOffice());
            String date = "dd/MM/yyyy";
            String dateStr = null;
            String dateStrEnd = null;
            DateFormat df = new SimpleDateFormat(date);
            dateStr = df.format(this.oldDTO.getStartDate());
            dateStrEnd = df.format(this.oldDTO.getEndDate());
            this.dbStartDate.setValue(dateStr);
            this.dbEndDate.setValue(dateStr);
            this.txtCtSignForm.setValue(this.oldDTO.getCtSignForm());
            this.txtContractStatus.setValue(this.oldDTO.getContractStatusName());
            this.txtContractType.setValue(this.oldDTO.getContractType());
            this.contractPointDTOs = this.contactPointService.getList(this.contract_id);
            if (this.contractPointDTOs != null && !this.contractPointDTOs.isEmpty()) {
                ListModelList model = new ListModelList(this.contractPointDTOs);
                model.setMultiple(false);
                this.grdContactPoint.setModel(model);
            }
        }
    }

    public void loadLBXContractFiles(Long contract_id) {
        ContractFilesDTO contractFilesDTO = new ContractFilesDTO();
        contractFilesDTO.setContractId(contract_id);
        this.lstContractFilesDTO = this.contractFilesService.getListMenuContractFiles(contractFilesDTO);
        if (this.lstContractFilesDTO != null) {
            ListModelList model = new ListModelList(this.lstContractFilesDTO);
            model.setMultiple(true);
            this.lbxViewAddContractManagement.setModel(model);
        }
    }

    public void loadLBXContractHis(Long contract_id) {
        ContractHisDTO contractHisDTO = new ContractHisDTO();
        contractHisDTO.setContractId(contract_id);
        this.lstContractHisDTO = this.contractHisService.getListContractHisAddUser(contractHisDTO);
        if (this.lstContractHisDTO != null) {
            ListModelList model = new ListModelList(this.lstContractHisDTO);
            model.setMultiple(true);
            this.lbxContractHis.setModel(model);
        }
    }

    public String formatDateTime(Date date) throws Exception {
        return StringUtils.formatDateTime(date);
    }

    public void onDownloadFileContract(ForwardEvent evt) {
        evt.stopPropagation();
        try {
            Image btn = (Image) evt.getOrigin().getTarget();
            Listitem litem = (Listitem) btn.getParent().getParent();
            onClick$btnDownloadFile(litem);
        } catch (Exception ex) {
//            java.util.logging.Logger.getLogger(ContractEditManagementType1.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onClick$btnDownloadFile(Listitem litem) {
        try {
            List<ContractFilesDTO> lstContractFilesResult = new ArrayList<>();
            lstContractFilesResult.add(litem.getValue());
            File file = new File(lstContractFilesResult.get(0).getPathFile());
            Filedownload.save(file, null);
        } catch (Exception ex) {
            Messagebox.show(LanguageBundleUtils.getMessage("messageUploadFileError"),
                    LanguageBundleUtils.getMessage("Messagebox.warning"), Messagebox.OK,
                    Messagebox.ERROR,
                    null);
        }
    }

    public void onClick$btnClose() {
        try {
            self.detach();
        } catch (Exception ex) {
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
            logger.error(ex.getMessage(), ex);
        }
    }

    public void onClick$btncancelRegister() throws Exception {
        String messageContract = "";
        String messageHis = "";
        ContractDTO contractDTO = new ContractDTO();
        contractDTO.setContractId(contract_id);
        messageHis = contractService.updateContractSendPartner(contractDTO, action, ParamUtils.CONTRACT_PARTNER.CONTRACT_CANCEL_REGISTER);
        contractManagementComposer.bindData();
        if (ParamUtils.SUCCESS.equals(messageHis)) {
            Clients.showNotification(LanguageBundleUtils.getString("contractManagent.register.cancel"), "suscess", null, "middle_center", 1000);
            contractManagementComposer.bindData();
            this.self.detach();
        } else {
            Clients.showNotification(messageContract, "warning", null, "middle_center", 3000);
        }
        self.detach();
    }

    public void onClick$btnregister() throws Exception {
        String messageContract = "";
        String messageHis = "";
        ContractDTO contractDTO = new ContractDTO();
        contractDTO.setContractId(contract_id);
        messageHis = contractService.updateContractSendPartner(contractDTO, action, ParamUtils.CONTRACT_PARTNER.CONTRACT_STARUS_REGISTER);
        contractManagementComposer.bindData();
        if (ParamUtils.SUCCESS.equals(messageHis)) {
            Clients.showNotification(LanguageBundleUtils.getString("contractManagent.register.suscess"), "suscess", null, "middle_center", 1000);
            contractManagementComposer.bindData();
            this.self.detach();
        } else {
            Clients.showNotification(messageContract, "warning", null, "middle_center", 3000);
        }
        self.detach();
    }

}
