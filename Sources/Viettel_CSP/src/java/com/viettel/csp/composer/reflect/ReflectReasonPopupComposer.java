/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.composer.reflect;

import com.viettel.csp.DTO.ContractDTO;
import com.viettel.csp.DTO.ContractHisDTO;
import com.viettel.csp.DTO.PartnerDTO;
import com.viettel.csp.DTO.ReasonDTO;
import com.viettel.csp.DTO.ReflectDTO;
import com.viettel.csp.service.ContractService;
import com.viettel.csp.service.PartnerService;
import com.viettel.csp.service.ReasonService;
import com.viettel.csp.service.ReflectService;
import com.viettel.csp.util.CtrlValidator;
import com.viettel.csp.util.KeyValueBean;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.util.StringUtils;
import com.viettel.eafs.util.SpringUtil;
import java.util.ArrayList;
import java.util.List;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Textbox;

/**
 *
 * @author chitv@gemvietnam.com
 */
public class ReflectReasonPopupComposer extends GenericForwardComposer<Component> {

    private Combobox cbbContractStatus;
    private Textbox txtContenReason;
    private Combobox cbbReason;
    private ListModelList<KeyValueBean> kvbReason;
    private List<KeyValueBean> lstReason;
    private String action;
    private Long reflectId;
    private ContractDTO oldDTO;
    private PartnerDTO PartDTO;
    private ContractHisDTO contractHisDTO;
    private ContractService contractService = SpringUtil.getBean("contractService", ContractService.class);
    private PartnerService partnerService = SpringUtil.getBean("partnerService", PartnerService.class);
    final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ReflectReasonPopupComposer.class);
    private ReasonService reasonService = SpringUtil.getBean("reasonService", ReasonService.class);
    private ReflectService reflectService = SpringUtil.getBean("reflectService", ReflectService.class);
    private ReflectPopupComposer reflectPopupComposer;

    public Combobox getCbbReason() {
        return cbbReason;
    }

    public void setCbbReason(Combobox cbbReason) {
        this.cbbReason = cbbReason;
    }

    public ListModelList<KeyValueBean> getKvbReason() {
        return kvbReason;
    }

    public void setKvbReason(ListModelList<KeyValueBean> kvbReason) {
        this.kvbReason = kvbReason;
    }

    public List<KeyValueBean> getLstReason() {
        return lstReason;
    }

    public void setLstReason(List<KeyValueBean> lstReason) {
        this.lstReason = lstReason;
    }

    @Override
    public void doAfterCompose(Component comp) {
        try {
            super.doAfterCompose(comp);
            if (arg.containsKey(ParamUtils.COMPOSER)) {
                reflectPopupComposer = (ReflectPopupComposer) arg.get(ParamUtils.COMPOSER);
            }
            if (arg.containsKey("REFLECT_ID") && StringUtils.isValidString(String.valueOf(arg.get("REFLECT_ID")))) {
                reflectId = (Long) arg.get("REFLECT_ID");
            }
            bindDataCbbReflectStatus(ParamUtils.DEFAULT_VALUE_STR);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    public void bindDataCbbReflectStatus(String valueSelect) throws Exception {
        try {
            List<ReasonDTO> lstReasonDTO = reasonService.findListByType(ParamUtils.REASON_TYPE.REFLECT);
            lstReason = new ArrayList<>();
            lstReason.add(new KeyValueBean(ParamUtils.DEFAULT_VALUE_STR, LanguageBundleUtils.getString("global.combobox.choose")));
            if (lstReasonDTO != null && lstReasonDTO.size() > 0) {
                for (ReasonDTO reasonDTO : lstReasonDTO) {
                    KeyValueBean key = new KeyValueBean();
                    key.setKey(reasonDTO.getCode());
                    key.setValue(reasonDTO.getName());
                    lstReason.add(key);
                }
            }
            kvbReason = new ListModelList(lstReason);
            int i = 0;
            for (KeyValueBean itemParam : kvbReason.getInnerList()) {
                if (itemParam.getKey() != null && String.valueOf(itemParam.getKey()).equals(valueSelect)) {
                    kvbReason.addToSelection(kvbReason.getElementAt(i));
                    break;
                }
                i++;
            }
            cbbReason.setModel(kvbReason);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    public ReflectDTO getDataFromClient() {
        ReflectDTO reflectDTO = new ReflectDTO();
        if (cbbReason.getSelectedItem() != null && StringUtils.isValidString(cbbReason.getSelectedItem().getValue())) {
            reflectDTO.setReasonCode(cbbReason.getSelectedItem().getValue());
        }
        if (StringUtils.isValidString(txtContenReason.getValue())) {
            reflectDTO.setReasonDescription(txtContenReason.getValue());
        }
        return reflectDTO;
    }

    public void onClick$btnSendReason() {
        try {
            if (validate()) {
                String message = "";
                ReflectDTO reflectDTO = getDataFromClient();
                reflectDTO.setId(reflectId);
                reflectDTO.setReflectStatusCode(ParamUtils.REFLECT_STATUS.CLOSE);
                message = reflectService.saveChangeStatus(reflectDTO);
                if (ParamUtils.SUCCESS.equals(message)) {
                    Clients.showNotification(LanguageBundleUtils.getString("global.message.create.successful"), "info", null, "middle_center", 1000);
                    self.detach();
                    reflectPopupComposer.setData();
                } else {
                    Clients.showNotification(message, "warning", null, "middle_center", 3000);
                }
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    public void onClick$btnClose() {
        try {
            self.detach();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    private boolean validate() {
        if (!CtrlValidator.checkValidComboValue(cbbReason, true, true, LanguageBundleUtils.getString("reflect.reason"))) {
            return false;
        }
        if (!CtrlValidator.checkValidTextbox(txtContenReason, true, true, LanguageBundleUtils.getString("reflect.reasonDescription"))) {
            return false;
        }
        return true;
    }

}
