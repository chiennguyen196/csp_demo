package com.viettel.csp.composer.contact;

import com.viettel.csp.DTO.ContactDTO;
import com.viettel.csp.entity.MessageEntity;
import com.viettel.csp.service.ContactService;
import com.viettel.csp.service.ContactTypeService;
import com.viettel.csp.service.MessageService;
import com.viettel.csp.util.*;
import com.viettel.eafs.util.SpringUtil;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.*;

/**
 * Created by tiennv
 */
public class ContactComposer extends GenericForwardComposer<Component> {

    private Window wdContact;
    private Grid gridSearch;
    private Listbox lbxContactSearch;
    private Button btnAdd;
    private Button btnEdit;
    private Button btnDelete;
    private Button btnSave;
    private Button btnReset;
    private Textbox txtUserName;
    private Textbox txtName;
    private Combobox cbbContactType;
    private Combobox cbbContactStatus;
    private ListModelList<KeyValueBean> kvbContactType;
    private List<ContactDTO> lstResult;
    private ContactService contactService = SpringUtil.getBean("contactService", ContactService.class);
    private ContactTypeService contactTypeService = SpringUtil.getBean("contactTypeService", ContactTypeService.class);
    private MessageService messageService=SpringUtil.getBean("messageService", MessageService.class);


    final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger("ContactComposer");

    public ListModelList<KeyValueBean> getKvbContactType() {
        return kvbContactType;
    }

    public void setKvbContactType(ListModelList<KeyValueBean> kvbContactType) {
        this.kvbContactType = kvbContactType;
    }

    public Listbox getLbxContactSearch() {
        return lbxContactSearch;
    }

    public void setLbxContactSearch(Listbox lbxContactSearch) {
        this.lbxContactSearch = lbxContactSearch;
    }

    public List<ContactDTO> getLstResult() {
        return lstResult;
    }

    public void setLstResult(List<ContactDTO> lstResult) {
        this.lstResult = lstResult;
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        lbxContactSearch.setMultiple(true);
        wdContact.setContentStyle("overflow:auto;");
        bindData();
        bindDataToGrid();
    }

    public void bindData() {
        try {
            List<KeyValueBean> lstKeyValueBean = contactTypeService.getListToKeyValueBean(true);
            if (lstKeyValueBean != null && lstKeyValueBean.size() > 0) {
                kvbContactType = new ListModelList(lstKeyValueBean);
                kvbContactType.addToSelection(kvbContactType.getElementAt(0));
                cbbContactType.setModel(kvbContactType);
            } else {
                cbbContactType.setModel(new ListModelList());
            }
            cbbContactType.setValue(LanguageBundleUtils.getString("global.combobox.choose"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ContactDTO getDataSearchFromClient() {
        ContactDTO object = new ContactDTO();
        if (txtUserName == null) {
            return null;
        }

        if (StringUtils.isValidString(txtUserName.getValue())) {
            object.setUserName(txtUserName.getValue().trim());
        }

        if (StringUtils.isValidString(txtName.getValue())) {
            object.setName(txtName.getValue().trim());
        }


        if (cbbContactStatus.getSelectedIndex() > 0) {
            object.setContactStatus(cbbContactStatus.getSelectedItem().getValue().toString());
        }

        if (cbbContactType.getSelectedIndex() > 0) {
            object.setContactType(cbbContactType.getSelectedItem().getValue().toString());
        }
        return object;
    }

    public void bindDataToGrid() throws Exception {
        ContactDTO object = getDataSearchFromClient();
        if (object == null) {
            return;
        }

        lstResult = contactService.getList(object);
        if (lstResult != null && !lstResult.isEmpty()) {
            ListModelList model = new ListModelList(lstResult);
            model.setMultiple(true);
            lbxContactSearch.setModel(model);
            if (gridSearch != null) {
                gridSearch.invalidate();
            }
        } else {
            lbxContactSearch.setModel(new ListModelList());
        }
    }

    public void onClick$btnSearch() {
        try {
            bindDataToGrid();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    public void onOK() {
        onClick$btnSearch();
    }

    public void onClick$btnAdd() {
        try {
            Map<String, Object> argu = new HashMap();
            argu.put(ParamUtils.ACTION, ParamUtils.ACTION_CREATE);
            argu.put("CONTACT_ID", "");
            argu.put(ParamUtils.COMPOSER, this);
            Window wd = (Window) Executions.createComponents("/controls/contact/contactPopup.zul", null, argu);
            wd.doModal();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    public void onClick$btnEdit() {
        try {
            if (lbxContactSearch.getSelectedCount() == 1) {
                Map<String, Object> argu = new HashMap();
                argu.put(ParamUtils.ACTION, ParamUtils.ACTION_UPDATE);
                ContactDTO oldDTO = (ContactDTO) lbxContactSearch.getItemAtIndex(lbxContactSearch.getSelectedItem().getIndex()).getValue();
                if (oldDTO.getContactStatus().equalsIgnoreCase(ParamUtils.USER_STATUS.INACTIVE)) {
                    Clients.showNotification(LanguageBundleUtils.getString("contact.message.notupdate.user.inactive"), "warning", null, "middle_center", 3000);
                } else {
                    argu.put("CONTACT_ID", oldDTO.getId());
                    argu.put(ParamUtils.COMPOSER, this);
                    Window wd;
                    wd = (Window) Executions.createComponents("/controls/contact/contactPopup.zul", null, argu);
                    wd.doModal();
                }
            } else {
                Clients.showNotification(LanguageBundleUtils.getString("global.action.confirm.is_select.edit"), "warning", null, "middle_center", 3000);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    public void onClick$btnDelete() {
        try {
            if (lbxContactSearch.getSelectedCount() > 0) {
                Messagebox.show(LanguageBundleUtils.getString("global.action.confirm.multi.delete"), "Confirm Dialog", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION, new org.zkoss.zk.ui.event.EventListener() {
                    public void onEvent(Event evt) throws InterruptedException, Exception {
                        if ("onOK".equals(evt.getName())) {
                            List<ContactDTO> lstDeleteDTO = new ArrayList<>();
                            for (Listitem item : lbxContactSearch.getSelectedItems()) {
                                ContactDTO deleteDTO = ((ContactDTO) item.getValue());
                                lstDeleteDTO.add(deleteDTO);

                                //code by phamvanluong
                                messageService.insertMessage(getMessageEntity(deleteDTO.getId(),LanguageBundleUtils.getString("contact.message.delete"), LanguageBundleUtils.getString("contact.message.delete")));
                            }
                            String message = contactService.deleteContact(lstDeleteDTO);
                            bindDataToGrid();
                            Clients.showNotification(message, "info", null, "middle_center", 3000);
                            return;
                        }
                    }
                });
            } else {
                Clients.showNotification(LanguageBundleUtils.getString("global.action.confirm.is_select"), "warning", null, "middle_center", 3000);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    //code by phamvanluong
    public  MessageEntity getMessageEntity(long receiveUserId, String content, String description){
        MessageEntity messageEntity=new MessageEntity();
        messageEntity.setSendUserId(UserTokenUtils.getUserId());
        messageEntity.setReceiveUserId(receiveUserId);
        messageEntity.setStatus(0);
        messageEntity.setContent(content);
        messageEntity.setDescription(description);
        messageEntity.setCreateBy(UserTokenUtils.getUserId());
        messageEntity.setCreatAt(new Date());

        return messageEntity;
    }

    public void onClick$btnReset() {
        bindData();
    }

    public String getContactSex(String paramType) {
        try {
            switch (paramType) {
                case ParamUtils.GENDER_MALE_STR:
                    return LanguageBundleUtils.getString("global.sex.male");
                case ParamUtils.GENDER_FEMALE_STR:
                    return LanguageBundleUtils.getString("global.sex.female");
                default:
                    return "";
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return "";
    }

    public String getContactStatusName(String paramType) {
        try {
            switch (paramType) {
                case ParamUtils.ACTIVE_STR:
                    return LanguageBundleUtils.getString("contact.status.active");
                case ParamUtils.LOCK_STR:
                    return LanguageBundleUtils.getString("contact.status.lock");
                case ParamUtils.NOT_ACTIVE_STR:
                    return LanguageBundleUtils.getString("contact.status.inactive");
                default:
                    return "";
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return "";
    }

    public String formatDateTime(Date date) throws Exception {
        return StringUtils.formatDateTime(date);
    }
}
