package com.viettel.csp.composer.contractManagement;

import com.viettel.csp.DTO.*;
import com.viettel.csp.composer.contract.ContractComposer;
import com.viettel.csp.composer.contract.ContractPopupEditComposer;
import com.viettel.csp.service.*;
import com.viettel.csp.util.*;
import com.viettel.eafs.util.SpringUtil;
import com.viettel.logincsp.AuthenticationService;
import com.viettel.plc.widget.FileInfo;
import com.viettel.plc.widget.IUploaderCallback;
import com.viettel.plc.widget.UploadComponent;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.ForwardEvent;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.*;

import java.io.File;
import java.util.*;

/**
 * Created by admin on 6/12/2017.
 */
public class ContractManagementAddViettelBComposer extends
        GenericForwardComposer<Component> implements IUploaderCallback {

    final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ContractManagementAddViettelBComposer.class);
    private final ContactTypeService contactTypeService = SpringUtil.getBean("contactTypeService", ContactTypeService.class);
    private final ContactService contactService = SpringUtil.getBean("contactService", ContactService.class);
    private final ContactPointService contactPointService = SpringUtil.getBean("contactPointService", ContactPointService.class);
    private AuthenticationService authenticationService = SpringUtil.getBean("authenticationService", AuthenticationService.class);
    private Label txtAddressHeadOffice, txtPartnerCode, txtAddressTradingOffice, txtCompanyNameShort, txtPhone, txtRepName, dbFilingDate, txtContractStatus;
    private Textbox txtContractNumber, txtCompanyName, txtHeadOfService;
    private Combobox cbbContractStatus, cbServiceCode, cboCtSignForm, cboContractType;
    private Textbox txtContenReason;
    private List<KeyValueBean> lstReason;
    private ListModelList<KeyValueBean> kvbGroupServiceTree;
    private ListModelList<KeyValueBean> kvbContractType;
    private List<KeyValueBean> lstServiceName;
    private ListModelList<KeyValueBean> kvbServiceName;
    private Combobox cbbReason;
    private Datebox dbStartDate, dbEndDate;
    private String action;
    private Long contractId;
    private ArrayList indexLst;
    private ContractDTO oldDTO;
    private ContactPointDTO contactPointDTO;
    private PartnerDTO partNerDTO;
    private ContractHisDTO contractHisDTO;
    private ContractService contractService = SpringUtil.getBean("contractService", ContractService.class);
    private PartnerService partnerService = SpringUtil.getBean("partnerService", PartnerService.class);
    private ContractStatusService contractStatusService = SpringUtil.getBean("contractStatusService", ContractStatusService.class);
    private ContractHisService contractHisService = SpringUtil.getBean("contractHisService", ContractHisService.class);
    private ContractFilesService contractFilesService = SpringUtil.getBean("contractFilesService", ContractFilesService.class);
    private ServiceService service = SpringUtil.getBean("serviceService", ServiceService.class);
    private ContractComposer contractComposer;
    private ContractPopupEditComposer contractPopupEditComposer;
    private Listbox lbxViewAddContract;
    private Grid gridSearch;
    private List<ContractFilesDTO> lstContractFiles = new ArrayList<>();
    private UploadComponent uploadComponent;
    private List<String> filesLocation;
    private List<FileInfo> filesInfo;
    private Rows rows;
    private Row lastRow;
    private Long totalRow = 0L;
    private GenComponent genComponent = new GenComponent();
    private List<Component> lstInputCbbContact = new ArrayList<>();
    private List<ContactTypeDTO> lstInputContactType = new ArrayList<>();
    private ContactPointDTO pointDTO;
    private ContractManagementComposer contractManagementComposer;

    public ContractManagementComposer getContractManagementComposer() {
        return contractManagementComposer;
    }

    public void setContractManagementComposer(ContractManagementComposer contractManagementComposer) {
        this.contractManagementComposer = contractManagementComposer;
    }

    @Override
    public void uploadedFilesInfoCallback(List<FileInfo> filesInfo) {
        this.filesInfo = filesInfo;
        System.out.println(filesInfo);
    }

    public Listbox getLbxViewAddContract() {
        return this.lbxViewAddContract;
    }

    public void setLbxViewAddContract(Listbox lbxViewAddContract) {
        this.lbxViewAddContract = lbxViewAddContract;
    }

    @Override
    public void uploadedFilesLocationCallback(List<String> filesLocation) {
        this.filesLocation = filesLocation;
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        if (arg.containsKey(ParamUtils.COMPOSER)) {
            contractManagementComposer = (ContractManagementComposer) arg.get(ParamUtils.COMPOSER);
        }
        if (this.arg.containsKey(ParamUtils.ACTION)) {
            this.action = this.arg.get(ParamUtils.ACTION).toString();
        }

        loadDataContactType();
        loadComboboxServiceCode(ParamUtils.DEFAULT_VALUE_STR);
        this.uploadComponent.setUploaderCallback(this);
    }

    public ContractDTO getDataFromClient() {
        this.oldDTO = new ContractDTO();
//        oldDTO.setCompanyName(txtCompanyName.getValue().trim().toLowerCase());
        this.oldDTO.setContractNumber(this.txtContractNumber.getValue().trim());
        if (this.cbServiceCode.getSelectedIndex() > 0) {
            this.oldDTO.setServiceCode(this.cbServiceCode.getSelectedItem().getValue());
        }
        if (this.cboContractType.getSelectedIndex() > 0) {
            this.oldDTO.setContractType(this.cboContractType.getSelectedItem().getValue());
        }
        if (this.cboCtSignForm.getSelectedIndex() > 0) {
            this.oldDTO.setCtSignForm(this.cboCtSignForm.getSelectedItem().getValue());
        }
        this.oldDTO.setStartDate(this.dbStartDate.getValue());
        this.oldDTO.setEndDate(this.dbEndDate.getValue());
        this.oldDTO.setHeadOfService(this.txtHeadOfService.getValue().trim());

        return this.oldDTO;
    }

    public ContactPointDTO getDataFromClientContactPointDTO() {
        Long contactID = 0L;
        pointDTO = new ContactPointDTO();
        List<ContactPointDTO> lstContactPointDTO = new ArrayList<>();
        Map<Object, Object> t = this.genComponent.getDataFromControls(this.lstInputCbbContact);
        List<Combobox> lstCbbContactDTO = new ArrayList(t.keySet());
        Collections.sort(lstCbbContactDTO, new Comparator<Combobox>() {
            @Override
            public int compare(Combobox o1, Combobox o2) {
                return o1.getId().compareTo(o2.getId());
            }
        });
        for (int i = 0; i < lstCbbContactDTO.size(); i++) {
            ContactPointDTO contactPointDTO = new ContactPointDTO();
            String v = (String) t.get(lstCbbContactDTO.get(i));
            contactID = Long.parseLong(v);
            if (contactID != -99) {
                contactPointDTO.setContactId(contactID);
                contactPointDTO.setContactType(lstInputContactType.get(i).getCode());
                contactPointDTO.setId(lstInputContactType.get(i).getContactPointID());
                lstContactPointDTO.add(contactPointDTO);
            }
        }
        pointDTO.setListContactPointDTO(lstContactPointDTO);
        return pointDTO;
    }

    public void loadDataContactType() {
        ContactDTO contactDTO = new ContactDTO();
//        contactDTO.setPartnerId(partner_ID);
        Row tmpRow;
        Component tmpComp;
        Component firstComp = null;
        try {
            //Bank
            List<ContactTypeDTO> lstContactTypeDTO = this.contactTypeService.findAll();
            List<ContactDTO> lstContactDTO = this.contactService.getList(contactDTO);
            StringBuilder parValue = new StringBuilder();
            for (ContactDTO cdto : lstContactDTO) {
                parValue.append(cdto.getId());
                parValue.append(";");
                parValue.append(cdto.getName());
                parValue.append(";");
            }
            for (ContactTypeDTO contactTypeDTO : lstContactTypeDTO) {
                ContactPointDTO cpdto = new ContactPointDTO();
                cpdto.setContactType(contactTypeDTO.getCode());
                List<ContactPointDTO> list = new ArrayList<>();
                String s = null;
                this.totalRow++;
                tmpRow = new Row();
                ComponentDTO componentDTO = new ComponentDTO();
                componentDTO.setAttributeType(ParamUtils.ReportDataType.COMBOBOX);
                componentDTO.setAttributeType(ParamUtils.ReportDataType.COMBOVALUE);
                componentDTO.setIsRequired(false);
                componentDTO.setStandardAttributeId(String.valueOf("cbb" + this.totalRow));
                componentDTO.setDefaultValue(LanguageBundleUtils.getString("global.combobox.choose"));
                for (ContactPointDTO dTO : list) {
                    componentDTO.setSelectItemValue(String.valueOf(list != null && list.size() > 0 ? dTO.getContactId() : ParamUtils.SELECT_NOTHING_VALUE_STR));
                    contactTypeDTO.setContactPointID(dTO.getId());
                }
                componentDTO.setAttributeName(contactTypeDTO.getName());

                componentDTO.setInitValue(parValue.toString());
                tmpComp = this.genComponent.createComponent(componentDTO);
                this.genComponent.createCellLabel(componentDTO.getAttributeName(), componentDTO.getIsRequired(), tmpRow);
                if (tmpComp != null) {
                    tmpRow.appendChild(tmpComp);
                    this.lstInputCbbContact.add(tmpComp);
                    this.lstInputContactType.add(contactTypeDTO);
                    if (firstComp == null) {
                        firstComp = tmpComp;
                    }
                }
                this.rows.insertBefore(tmpRow, this.lastRow);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    public void loadComboboxServiceCode(String valueSelect) throws Exception {
        try {
            List<ServiceDTO> listServiceDTO = this.service.findAll();
            this.lstServiceName = new ArrayList<>();
            this.lstServiceName.add(new KeyValueBean(ParamUtils.DEFAULT_VALUE_STR,
                    LanguageBundleUtils.getString("global.combobox.choose")));
            if (listServiceDTO != null && listServiceDTO.size() > 0) {
                for (ServiceDTO serviceDTO : listServiceDTO) {
                    KeyValueBean key = new KeyValueBean();
                    key.setKey(serviceDTO.getCode());
                    key.setValue(serviceDTO.getName());
                    this.lstServiceName.add(key);
                }
            }
            this.kvbServiceName = new ListModelList(this.lstServiceName);
            int i = 0;
            for (KeyValueBean itemParam : this.kvbServiceName.getInnerList()) {
                if (itemParam.getKey() != null && String.valueOf(itemParam.getKey())
                        .equals(valueSelect)) {
                    this.kvbServiceName.addToSelection(this.kvbServiceName.getElementAt(i));
                    break;
                }
                i++;
            }
            this.cbServiceCode.setModel(this.kvbServiceName);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            Clients
                    .showNotification(LanguageBundleUtils.getString("global.message.error"), "warning",
                            null, "middle_center", 3000);
        }
    }

    public void onClick$btnAddFilePopup() {
        try {
            Map<String, Object> argu = new HashMap();
            argu.put(ParamUtils.ACTION, ParamUtils.ACTION_CREATE);
            argu.put(ParamUtils.COMPOSER, this);
            Window wd;
            wd = (Window) Executions.createComponents("controls/contractManagement/contractManagementAddPopupViettelB.zul", null, argu);
            wd.doModal();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    public void setDataGrid(ContractFilesDTO contractFilesDTO) {

        this.lstContractFiles.add(contractFilesDTO);
        ListModelList model = new ListModelList(this.lstContractFiles);

        model.setMultiple(true);
        this.lbxViewAddContract.setModel(model);
    }

    public String formatDateTime(Date date) throws Exception {
        return StringUtils.formatDateTime(date);
    }


    public void onClick$bntSendContractFile() {
        try {
            String messageContract = "";
            String messageHis = "";
            if (validate()) {
                this.uploadComponent.onClick$doSave();
                ContractDTO contractDTO = getDataFromClient();
                ContactPointDTO pointDTO = getDataFromClientContactPointDTO();
                ContractHisDTO contractHisDTO = getDataFromClientContractHisDTO();
                List<ContractFilesDTO> lstContractFilesDTO = new ArrayList<>();
                if (null == filesLocation || null == filesInfo) {
                    Clients.showNotification(LanguageBundleUtils.getString("contractFiles.messageFiles.error"), "warning", null, "middle_center", 3000);
                    return;
                }
                if (this.filesInfo != null) {
                    for (int i = 0; i < this.filesInfo.size(); i++) {
                        ContractFilesDTO filesDTO = getDataFromClientContractFilesDTO(this.filesInfo.get(i));
                        lstContractFilesDTO.add(filesDTO);
                    }
                }

                if (lstContractFiles != null) {
                    contractDTO.setLstContractFilesDTOMenu(lstContractFiles);
                }
                contractDTO.setLstContractFilesDTO(lstContractFilesDTO);
                messageHis = this.contractService.insertContractAndContactPoint(contractDTO, pointDTO, contractHisDTO);
                if (ParamUtils.SUCCESS.equals(messageHis)) {
                    Clients.showNotification(LanguageBundleUtils.getString("global.message.create.successful"), "suscess", null, "middle_center", 1000);
                    contractManagementComposer.bindData();
                    this.self.detach();
                } else {
                    Clients.showNotification(messageContract, "warning", null, "middle_center", 3000);
                }
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients
                    .showNotification(LanguageBundleUtils.getString("global.message.error"), "warning",
                            null, "middle_center", 3000);
        }
    }

    public ContractHisDTO getDataFromClientContractHisDTO() {
        ContractHisDTO contractHisDTO = new ContractHisDTO();
        contractHisDTO.setCreateBy(UserTokenUtils.getUserId());
        contractHisDTO.setCreateAt(new Date());
        contractHisDTO.setStatusAfter(ParamUtils.CONTRACT_PARTNER.CONTRACTHIS_STATUS_AFTER);
        contractHisDTO.setStatusBefore(ParamUtils.CONTRACT_PARTNER.CONTRACTHIS_STATUS_BEFORE);
        return contractHisDTO;
    }


    private boolean validate() {
        if (!CtrlValidator.checkValidTextbox(txtContractNumber, true, true, LanguageBundleUtils.getString("contract.contractNumber"))) {
            return false;
        }
        if (!StringUtils.isValidDate(this.dbStartDate.getValue())) {
            Clients.wrongValue(this.dbStartDate, LanguageBundleUtils.getMessage("global.validate.notformat.date.ddMMyyyy", LanguageBundleUtils.getString("contract.startDate")));
            return false;
        }
        if (!StringUtils.isValidDate(this.dbEndDate.getValue())) {
            Clients.wrongValue(this.dbEndDate, LanguageBundleUtils.getMessage("global.validate.notformat.date.ddMMyyyy", LanguageBundleUtils.getString("contract.endDate")));
            return false;
        }
        if (!CtrlValidator.checkValidComboValue(cbServiceCode, true, true, LanguageBundleUtils.getString("reflect.cbServiceName"))) {
            return false;
        }
        if (!CtrlValidator.checkValidComboValue(cboContractType, true, true, LanguageBundleUtils.getString("contract.contractType"))) {
            return false;
        }
        if (!CtrlValidator.checkValidTextbox(txtHeadOfService, true, true, LanguageBundleUtils.getString("contract.headOfService"))) {
            return false;
        }
        if (!CtrlValidator.checkValidComboValue(cboCtSignForm, true, true, LanguageBundleUtils.getString("contract.ctSignForm"))) {
            return false;
        }
        return true;
    }


    public void onDownloadFileContract(ForwardEvent evt) {
        evt.stopPropagation();
        try {
            Image btn = (Image) evt.getOrigin().getTarget();
            Listitem litem = (Listitem) btn.getParent().getParent();
            onClick$btnDownloadFile(litem);
        } catch (Exception ex) {
//            java.util.logging.Logger.getLogger(ContractEditManagementType1.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onClick$btnDownloadFile(Listitem litem) {
        try {
            List<ContractFilesDTO> lstContractFilesResult = new ArrayList<>();
            lstContractFilesResult.add(litem.getValue());
            File file = new File(lstContractFilesResult.get(0).getPathFile());
            Filedownload.save(file, null);
        } catch (Exception ex) {
            Messagebox.show(LanguageBundleUtils.getMessage("messageUploadFileError"),
                    LanguageBundleUtils.getMessage("Messagebox.warning"), Messagebox.OK,
                    Messagebox.ERROR, null);
        }
    }

    public ContractFilesDTO getDataFromClientContractFilesDTO(FileInfo fileInfo) {
        ContractFilesDTO contractFilesDTO = new ContractFilesDTO();
        contractFilesDTO.setFileName(new File(fileInfo.getLocation()).getName());
        contractFilesDTO.setPathFile(new File(fileInfo.getLocation()).getPath());
        contractFilesDTO.setDescription(fileInfo.getDescription());
        contractFilesDTO.setFileType(ParamUtils.CONTRACT_PARTNER.CONTRACTFILESNAME);
        return contractFilesDTO;
    }

    public void onClick$btnViewFilePopup() {
        try {
            Map<String, Object> argu = new HashMap();
            argu.put(ParamUtils.ACTION, ParamUtils.ACTION_UPDATE_FILE);
            List<Integer> indexs = new ArrayList();
            indexs.add(lbxViewAddContract.getSelectedItem().getIndex());
            argu.put("LIST_ACTION_INDEX", indexs);
            argu.put("LISTBOX", lbxViewAddContract.getSelectedItem().getValue());
            argu.put(ParamUtils.COMPOSER, this);
            lstContractFiles.remove(lstContractFiles.get(lbxViewAddContract.getSelectedIndex()));
            lbxViewAddContract.removeItemAt(lbxViewAddContract.getSelectedIndex());
            Window wd;
            wd = (Window) Executions.createComponents("controls/contractManagement/contractManagementAddPopupViettelB.zul", null, argu);
            wd.doModal();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    public void onClick$btnRemoveFilePopup() {
        try {
            if (lbxViewAddContract.getSelectedCount() > 0) {
                lstContractFiles.remove(lstContractFiles.get(lbxViewAddContract.getSelectedIndex()));
                lbxViewAddContract.removeItemAt(lbxViewAddContract.getSelectedIndex());
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }


}
