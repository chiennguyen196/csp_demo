/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.composer.partner;

import com.viettel.csp.DTO.BankDTO;
import com.viettel.csp.DTO.ComponentDTO;
import com.viettel.csp.DTO.PartnerBankDTO;
import com.viettel.csp.DTO.PartnerDTO;
import com.viettel.csp.entity.PartnerBankEntity;
import com.viettel.csp.service.BankService;
import com.viettel.csp.service.LocationService;
import com.viettel.csp.service.MessageService;
import com.viettel.csp.service.PartnerBankService;
import com.viettel.csp.service.PartnerService;
import com.viettel.csp.service.PartnerTypeService;
import com.viettel.csp.util.ComposerUtils;
import com.viettel.csp.util.GenComponent;
import com.viettel.csp.util.KeyValueBean;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.util.StringUtils;
import com.viettel.eafs.util.SpringUtil;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import static jxl.biff.BaseCellFeatures.logger;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author GEM
 */
public class PartnerRegisterPopupComposer extends GenericForwardComposer<Component> {

    private Long totalRow = 0L;
    private int FIX_ROW = 16;
    private String action = "";
    private Listbox lbxAtribute;
    private String provinceCodeOld = "";
    private List<Integer> indexs = new ArrayList();
    private PartnerDTO oldDTO = null;
    private List<Component> lstInputCbbBankCtrls = new ArrayList<>();
    private List<Component> lstInputTextBankCtrls = new ArrayList<>();
    private Rows rows;
    private Row lastRow;
    private Datebox dbBusinessLicenceDate;
    private Textbox txtParnerCode, txtParnerName, txtParnerShortName, txtAddressOffice, txtAddressTransaction, txtphone, txtRepresentLaw, txtPositionLaw, txtEmailTransaction, txtTaxCode, txtBusinessLicenceNo, txtBusinessLicenceCount;
    private Combobox cboGroupPartner, cboProvince, cboTypePartner;
    private ListModelList<KeyValueBean> listBankModel = new ListModelList<KeyValueBean>();
    private ListModelList<KeyValueBean> listLocationModel = new ListModelList<KeyValueBean>();
    private ListModelList<KeyValueBean> listPartnerTypeModel = new ListModelList<KeyValueBean>();
    private DateFormat df = new SimpleDateFormat("dd/MM/yyy");
    private GenComponent genComponent = new GenComponent();
    private static final Logger log = Logger.getLogger(PartnerRegisterPopupComposer.class);
    private static final String ATTR_VALUE_PATTERN = "VALUE_PATTERN";
    private static final String ATTR_DISPLAY_NAME = "DISPLAY_NAME";
    private static final String ATTR_REQUIRED = "REQUIRED";

    @Autowired
    private PartnerComposer partnerComposer;
    private BankService bankService = SpringUtil.getBean("bankService", BankService.class);
    private LocationService locationService = SpringUtil.getBean("locationService", LocationService.class);
    private PartnerService partnerService = SpringUtil.getBean("partnerService", PartnerService.class);
    private PartnerTypeService partnerTypeService = SpringUtil.getBean("partnerTypeService", PartnerTypeService.class);
    private MessageService messageService = SpringUtil.getBean("messageService", MessageService.class);
    private PartnerBankService partnerBankService = SpringUtil.getBean("partnerBankService", PartnerBankService.class);

    public PartnerComposer getPartnerComposer() {
        if (partnerComposer == null) {
            partnerComposer = new PartnerComposer();
        }
        return partnerComposer;
    }

    public void setPartnerComposer(PartnerComposer partnerComposer) {
        this.partnerComposer = partnerComposer;
    }

    public ListModelList<KeyValueBean> getListBankModel() {
        return listBankModel;
    }

    public void setListBankModel(ListModelList<KeyValueBean> listBankModel) {
        this.listBankModel = listBankModel;
    }

    public ListModelList<KeyValueBean> getListLocationModel() {
        return listLocationModel;
    }

    public void setListLocationModel(ListModelList<KeyValueBean> listLocationModel) {
        this.listLocationModel = listLocationModel;
    }

    public ListModelList<KeyValueBean> getListPartnerTypeModel() {
        return listPartnerTypeModel;
    }

    public void setListPartnerTypeModel(ListModelList<KeyValueBean> listPartnerTypeModel) {
        this.listPartnerTypeModel = listPartnerTypeModel;
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp); //To change body of generated methods, choose Tools | Templates.
        if (arg.containsKey(ParamUtils.ACTION)) {
            action = arg.get(ParamUtils.ACTION).toString();
        }
        if (arg.containsKey("LISTBOX")) {
            lbxAtribute = (Listbox) arg.get("LISTBOX");
        }
        if (arg.containsKey("COMPOSER")) {
            partnerComposer = (PartnerComposer) arg.get("COMPOSER");
        }
        if (arg.containsKey("LIST_ACTION_INDEX")) {
            indexs = (List<Integer>) arg.get("LIST_ACTION_INDEX");
        }

        if (ParamUtils.ACTION_UPDATE.equals(action)) {
            ((Window) self).setTitle(LanguageBundleUtils.getString("global.edit"));
            oldDTO = (PartnerDTO) lbxAtribute.getItemAtIndex(indexs.get(0)).getValue();
            bindData(oldDTO);
        } else {
            ((Window) self).setTitle(LanguageBundleUtils.getString("global.add"));
        }
    }

    public void loadComboboxProvince() throws Exception {
        List<KeyValueBean> lstProvince = new ArrayList<KeyValueBean>();
        lstProvince = locationService.getListDataToCombobox("PROVINCE", null);
        listLocationModel = new ListModelList<KeyValueBean>();
        listLocationModel.addAll(lstProvince);
        cboProvince.setModel(listLocationModel);
    }

    public void loadComboboxTypePartner() throws Exception {
        List<KeyValueBean> lstPartnerType = new ArrayList<KeyValueBean>();
        lstPartnerType = partnerTypeService.getListToKeyValueBean(true);
        listPartnerTypeModel = new ListModelList<KeyValueBean>();
        listPartnerTypeModel.addAll(lstPartnerType);
        cboTypePartner.setModel(listPartnerTypeModel);
    }

    public List<PartnerBankDTO> getInfoBankByPartnerId(Long partnerId) throws Exception {
        List<PartnerBankDTO> lstPartnerBankDTO = new ArrayList<PartnerBankDTO>();
        lstPartnerBankDTO = partnerBankService.getInfoBankByPartnerId(partnerId);
        return lstPartnerBankDTO;
    }

    public void bindDataToCombo() throws Exception {
        loadComboboxTypePartner();
        loadComboboxProvince();

    }

    public void bindData(PartnerDTO object) throws Exception {
        bindDataToCombo();
        txtParnerCode.setValue(object.getPartnerCode());
        txtParnerName.setValue(object.getCompanyName());
        txtParnerShortName.setValue(object.getCompanyNameShort());
        txtAddressOffice.setValue(object.getAddressOffice());
        txtAddressTransaction.setValue(object.getAddressTradingOffice());
        txtphone.setValue(object.getPhone());
        txtRepresentLaw.setValue(object.getRepName());
        txtPositionLaw.setValue(object.getRepPosition());
        txtEmailTransaction.setValue(object.getEmail());
        txtTaxCode.setValue(object.getTaxCode());
        txtBusinessLicenceNo.setValue(object.getBusinessRegisNumber());
        dbBusinessLicenceDate.setValue(object.getBusinessRegisDate());
        txtBusinessLicenceCount.setValue(object.getBusinessRegisCount());
        if (object.getPartnerGroup() != null && object.getPartnerGroup().equalsIgnoreCase(ParamUtils.PARTNER_GROUP.INTERNAL)) {
            cboGroupPartner.setValue(LanguageBundleUtils.getString("partner.domestic"));
            List<KeyValueBean> lstProvince = new ArrayList<KeyValueBean>();
            lstProvince = locationService.getListDataToCombobox("PROVINCE", null);
            int i = 0;
            for (KeyValueBean keyValueBean : lstProvince) {
                if (keyValueBean.getKey().equals(object.getPartnerGroupProvince())) {
                    provinceCodeOld = keyValueBean.getKey().toString();
                    cboProvince.setValue(keyValueBean.getValue());
                    break;
                }
                i++;
            }
            cboProvince.setDisabled(false);
        } else {
            cboGroupPartner.setValue(LanguageBundleUtils.getString("partner.foreign"));
            cboProvince.setValue(LanguageBundleUtils.getString("global.combobox.choose"));
            cboProvince.setDisabled(true);
        }

        if (StringUtils.isValidString(object.getPartnerTypeCode())) {
            List<KeyValueBean> lstPartnerType = new ArrayList<KeyValueBean>();
            lstPartnerType = partnerService.getListPartnerTypeByCode(object.getPartnerTypeCode());
            String partnerTypeName = lstPartnerType.get(0).getValue();
            cboTypePartner.setValue(partnerTypeName);
        }

        txtParnerCode.setDisabled(true);
        removeComponentBank(rows.getChildren().indexOf(lastRow) - FIX_ROW);
        buildComponentToData(object.getId());
    }

    public List<PartnerBankEntity> convertToListBankEntity(List<PartnerBankDTO> lstPartnerBankDTO) {
        PartnerBankEntity partnerBankEntity = null;
        List<PartnerBankEntity> lstPartnerBankEntity = new ArrayList<PartnerBankEntity>();

        for (int i = 0; i < lstPartnerBankDTO.size(); i++) {
            partnerBankEntity = PartnerBankEntity.setEntity(lstPartnerBankDTO.get(i), partnerBankEntity);
            lstPartnerBankEntity.add(partnerBankEntity);
        }

        return lstPartnerBankEntity;
    }

    public void onClick$btnAddBankPopup() {
        Row tmpRow;
        Component tmpComp;
        Component firstComp = null;
        try {
            totalRow++;
            tmpRow = new Row();
            List<BankDTO> lstBankDTO = bankService.findAll();
            ComponentDTO componentTextDTO = new ComponentDTO();
            componentTextDTO.setAttributeType(ParamUtils.ReportDataType.STRING);
            componentTextDTO.setAttributeLength("50");
            componentTextDTO.setIsRequired(false);
            componentTextDTO.setStandardAttributeId(String.valueOf("txt" + totalRow));
            componentTextDTO.setDefaultValue("");
            componentTextDTO.setAttributeName(LanguageBundleUtils.getString("partner.accountNumber"));
            tmpComp = genComponent.createComponent(componentTextDTO);
            genComponent.createCellLabel(componentTextDTO.getAttributeName(), componentTextDTO.getIsRequired(), tmpRow);
            if (tmpComp != null) {
                tmpRow.appendChild(tmpComp);
                lstInputTextBankCtrls.add(tmpComp);
                if (firstComp == null) {
                    firstComp = tmpComp;
                }
            }
            rows.insertBefore(tmpRow, lastRow);

            tmpRow = new Row();
            ComponentDTO componentDTO = new ComponentDTO();
            componentDTO.setAttributeType(ParamUtils.ReportDataType.COMBOBOX);
            componentDTO.setAttributeType(ParamUtils.ReportDataType.COMBOVALUE);
            componentDTO.setIsRequired(false);
            componentDTO.setStandardAttributeId(String.valueOf("cbb" + totalRow));
            componentDTO.setDefaultValue(null);
            componentDTO.setAttributeName(LanguageBundleUtils.getString("partner.bank"));
            StringBuilder parValue = new StringBuilder();
            for (BankDTO itemBankDTO : lstBankDTO) {
                parValue.append(itemBankDTO.getCode());
                parValue.append(";");
                parValue.append(itemBankDTO.getName());
                parValue.append(";");
            }
            componentDTO.setInitValue(parValue.toString());
            tmpComp = genComponent.createComponent(componentDTO);
            genComponent.createCellLabel(componentDTO.getAttributeName(), componentDTO.getIsRequired(), tmpRow);
            if (tmpComp != null) {
                tmpRow.appendChild(tmpComp);
                lstInputCbbBankCtrls.add(tmpComp);
                if (firstComp == null) {
                    firstComp = tmpComp;
                }
            }
            rows.insertBefore(tmpRow, lastRow);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    public void onClick$btnSavePopup() throws Exception {
        try {
            if (indexs.size() > 0) {
                if (validate()) {
                    PartnerDTO newPartnerDTO = getDataFromClient();
                    List<PartnerDTO> lst = (List<PartnerDTO>) ((ListModelList) lbxAtribute.getModel()).getInnerList();
                    PartnerDTO oldPartnerDTO = (PartnerDTO) lbxAtribute.getItemAtIndex(indexs.get(0)).getValue();
                    if (newPartnerDTO.getPartnerCode().equals(oldPartnerDTO.getPartnerCode())) {
                        partnerService.update(newPartnerDTO);
                        partnerBankService.deletePartnerBank(newPartnerDTO.getId());
                        partnerBankService.insert(newPartnerDTO.getLstPartnerBank());
                        messageService.insert();
                        partnerComposer.onClick$btnSearch();
                        Clients.showNotification(LanguageBundleUtils.getMessage("global.message.update.successful", LanguageBundleUtils.getString("global.edit")), "info", null, "middle_center", 3000);
                        self.detach();
                    }
                }
            } else {
                Clients.showNotification(LanguageBundleUtils.getString("global.choose.action"), "warning", null, "middle_center", 3000);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    public PartnerDTO getDataFromClient() throws Exception {
        PartnerDTO object = new PartnerDTO();
        Long partnerId = ((PartnerDTO) lbxAtribute.getItemAtIndex(indexs.get(0)).getValue()).getId();
        object.setId(partnerId);

        object.setPartnerCode(txtParnerCode.getValue().trim());
        object.setCompanyName(txtParnerName.getValue().trim());
        object.setCompanyNameShort(txtParnerShortName.getValue().trim());
        object.setAddressOffice(txtAddressOffice.getValue().trim());
        object.setAddressTradingOffice(txtAddressTransaction.getValue().trim());
        object.setPhone(txtphone.getValue().trim());
        object.setRepName(txtRepresentLaw.getValue().trim());
        object.setRepPosition(txtPositionLaw.getValue().trim());
        object.setEmail(txtEmailTransaction.getValue().trim());
        object.setTaxCode(txtTaxCode.getValue().trim());
        object.setBusinessRegisNumber(txtBusinessLicenceNo.getValue().trim());
        object.setBusinessRegisDate(dbBusinessLicenceDate.getValue());
        object.setBusinessRegisCount(txtBusinessLicenceCount.getValue().trim());
        if (cboGroupPartner.getSelectedItem() != null) {
            String groupPartner = cboGroupPartner.getSelectedItem().getValue().toString();
            object.setPartnerGroup(groupPartner);
            if (groupPartner.equals(ParamUtils.PARTNER_GROUP.INTERNAL)) {
                object.setPartnerGroupProvince(cboProvince.getSelectedItem().getValue().toString());
            } else {
                object.setPartnerGroupProvince("");
            }
        }
        if (cboTypePartner.getSelectedItem() != null) {
            object.setPartnerTypeCode(cboTypePartner.getSelectedItem().getValue().toString());
        }
        List<PartnerBankDTO> lstPartnerBankDTO = new ArrayList<>();
        List<String> listInputBank = new ArrayList(genComponent.getDataFromControls(lstInputTextBankCtrls).values());
        List<String> listCbbBank = new ArrayList(genComponent.getDataFromControls(lstInputCbbBankCtrls).values());

        for (int i = 0, total = listInputBank.size(); i < total; i++) {
            PartnerBankDTO partnerBankDTO = new PartnerBankDTO();
            partnerBankDTO.setPartnerId(partnerId);
            partnerBankDTO.setAccountNumber(listInputBank.get(i));
            partnerBankDTO.setBankCode(listCbbBank.get(i));
            lstPartnerBankDTO.add(partnerBankDTO);
        }
        object.setLstPartnerBank(lstPartnerBankDTO);
        return object;
    }

    public void onClick$btnResetPopup() throws Exception {
        if (ParamUtils.ACTION_UPDATE.equals(action)) {
            oldDTO = (PartnerDTO) lbxAtribute.getItemAtIndex(indexs.get(0)).getValue();
            bindData(oldDTO);
        }
    }

    public void onClick$btnRemoveBankPopup() throws Exception {
        try {
            removeComponentBank(2);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    public void onChangeGroupPartner(Event event) throws Exception {
        String value = cboGroupPartner.getSelectedItem().getValue();
        if (value.equals(ParamUtils.PARTNER_GROUP.INTERNAL)) {
            if (StringUtils.isValidString(provinceCodeOld)) {
                ComposerUtils.setFixComboValue(cboProvince, provinceCodeOld);
            } else {
                cboProvince.setValue(LanguageBundleUtils.getString("global.combobox.choose"));
            }
            cboProvince.setDisabled(false);
        } else {
            cboProvince.setValue(LanguageBundleUtils.getString("global.combobox.choose"));
            cboProvince.setDisabled(true);
        }
    }

    private boolean validate() {
        if (ParamUtils.ACTION_UPDATE.equals(action)) {
            if (!StringUtils.isValidString(txtParnerCode.getValue())) {
                Clients.wrongValue(txtParnerCode, LanguageBundleUtils.getMessage("global.validate.notnull", LanguageBundleUtils.getString("partner.code")));
                return false;
            }
        }

        if (!StringUtils.isValidString(txtParnerShortName.getValue())) {
            Clients.wrongValue(txtParnerShortName, LanguageBundleUtils.getMessage("global.validate.notnull", LanguageBundleUtils.getString("partner.shortName")));
            return false;
        }

        if (!StringUtils.isValidString(txtParnerName.getValue())) {
            Clients.wrongValue(txtParnerName, LanguageBundleUtils.getMessage("global.validate.notnull", LanguageBundleUtils.getString("partner.name")));
            return false;
        }

        if (!StringUtils.isValidString(txtAddressOffice.getValue())) {
            Clients.wrongValue(txtAddressOffice, LanguageBundleUtils.getMessage("global.validate.notnull", LanguageBundleUtils.getString("partner.address")));
            return false;
        }

        if (!StringUtils.isValidString(txtAddressTransaction.getValue())) {
            Clients.wrongValue(txtAddressTransaction, LanguageBundleUtils.getMessage("global.validate.notnull", LanguageBundleUtils.getString("partner.addressTransaction")));
            return false;
        }

        if (!StringUtils.isValidString(txtphone.getValue())) {
            Clients.wrongValue(txtphone, LanguageBundleUtils.getMessage("global.validate.notnull", LanguageBundleUtils.getString("partner.phone")));
            return false;
        }

        if (!StringUtils.isValidPhone(txtphone.getValue())) {
            Clients.wrongValue(txtphone, LanguageBundleUtils.getMessage("global.validate.notformat.phone", LanguageBundleUtils.getString("partner.phone")));
            return false;
        }

        if (!StringUtils.isValidString(txtRepresentLaw.getValue())) {
            Clients.wrongValue(txtRepresentLaw, LanguageBundleUtils.getMessage("global.validate.notnull", LanguageBundleUtils.getString("partner.representLaw")));
            return false;
        }

        if (!StringUtils.isValidString(txtPositionLaw.getValue())) {
            Clients.wrongValue(txtPositionLaw, LanguageBundleUtils.getMessage("global.validate.notnull", LanguageBundleUtils.getString("partner.positionLaw")));
            return false;
        }

        if (!StringUtils.isValidString(txtEmailTransaction.getValue())) {
            Clients.wrongValue(txtEmailTransaction, LanguageBundleUtils.getMessage("global.validate.notnull", LanguageBundleUtils.getString("partner.emailTransaction")));
            return false;
        }

        if (!StringUtils.isValidEmail(txtEmailTransaction.getValue())) {
            Clients.wrongValue(txtEmailTransaction, LanguageBundleUtils.getMessage("global.validate.notformat.email", LanguageBundleUtils.getString("partner.emailTransaction")));
            return false;
        }

        if (!StringUtils.isValidString(txtTaxCode.getValue())) {
            Clients.wrongValue(txtTaxCode, LanguageBundleUtils.getMessage("global.validate.notnull", LanguageBundleUtils.getString("partner.taxCode")));
            return false;
        }

        if (!StringUtils.isValidString(txtBusinessLicenceNo.getValue())) {
            Clients.wrongValue(txtBusinessLicenceNo, LanguageBundleUtils.getMessage("global.validate.notnull", LanguageBundleUtils.getString("partner.businessLicencenNo")));
            return false;
        }

        if (!StringUtils.isValidString(dbBusinessLicenceDate.getValue())) {
            Clients.wrongValue(dbBusinessLicenceDate, LanguageBundleUtils.getMessage("global.validate.notnull", LanguageBundleUtils.getString("partner.dbBusinessLicenceDate")));
            return false;
        }

        if (!StringUtils.isValidDate(dbBusinessLicenceDate.getValue())) {
            Clients.wrongValue(dbBusinessLicenceDate, LanguageBundleUtils.getMessage("global.validate.notformat.date.ddMMyyyy", LanguageBundleUtils.getString("partner.dbBusinessLicenceDate")));
            return false;
        }

        if (!StringUtils.isValidString(txtBusinessLicenceCount.getValue())) {
            Clients.wrongValue(txtBusinessLicenceCount, LanguageBundleUtils.getMessage("global.validate.notnull", LanguageBundleUtils.getString("partner.businessLicenceCount")));
            return false;
        }
        if (cboGroupPartner.getSelectedItem() != null && cboGroupPartner.getSelectedItem().getValue() != null && ParamUtils.SELECT_NOTHING_VALUE_STR.equals(cboGroupPartner.getSelectedItem().getValue())) {
            Clients.wrongValue(cboGroupPartner, LanguageBundleUtils.getMessage("global.validate.notchoose", LanguageBundleUtils.getString("partner.groupPartner")));
            return false;
        }

        if (cboProvince.getSelectedItem() != null && cboProvince.getSelectedItem().getValue() != null) {
            String groupPartner = cboGroupPartner.getSelectedItem().getValue().toString();
            if (groupPartner.equals(ParamUtils.PARTNER_GROUP.INTERNAL)) {
                if (cboProvince.getSelectedItem() != null && cboProvince.getSelectedItem().getValue() != null && ParamUtils.SELECT_NOTHING_VALUE_STR.equals(cboProvince.getSelectedItem().getValue())) {
                    Clients.wrongValue(cboProvince, LanguageBundleUtils.getMessage("global.validate.notchoose", LanguageBundleUtils.getString("partner.province")));
                    return false;
                }
            }
        }

        if (cboTypePartner.getSelectedItem().getValue() != null && ParamUtils.SELECT_NOTHING_VALUE_STR.equals(cboTypePartner.getSelectedItem().getValue())) {
            Clients.wrongValue(cboTypePartner, LanguageBundleUtils.getMessage("global.validate.notchoose", LanguageBundleUtils.getString("partner.type")));
            return false;
        }

        Map<Object, Object> mapInputBank = genComponent.getDataFromControls(lstInputTextBankCtrls);
        for (Map.Entry<Object, Object> entry : mapInputBank.entrySet()) {
            Object value = entry.getValue();
            Component component = (Component) entry.getKey();
            if (!StringUtils.isValidString(value)) {
                Clients.wrongValue(component, LanguageBundleUtils.getMessage("global.validate.notnull", LanguageBundleUtils.getString("partner.accountNumber")));
                return false;
            }
        }
        return true;
    }

    private void buildComponentToData(Long partnerId) {
        Row tmpRow;
        Component tmpComp;
        Component firstComp = null;
        try {
            //Bank
            List<PartnerBankDTO> lstPartnerBankDTO = partnerBankService.getInfoBankByPartnerId(partnerId);
            List<BankDTO> lstBankDTO = bankService.findAll();
            for (PartnerBankDTO itemPartnerBankDTO : lstPartnerBankDTO) {
                totalRow++;
                tmpRow = new Row();
                ComponentDTO componentTextDTO = new ComponentDTO();
                componentTextDTO.setAttributeType(ParamUtils.ReportDataType.STRING);
                componentTextDTO.setAttributeLength("50");
                componentTextDTO.setIsRequired(false);
                componentTextDTO.setStandardAttributeId(String.valueOf("txt" + totalRow));
                componentTextDTO.setDefaultValue(itemPartnerBankDTO.getAccountNumber());
                componentTextDTO.setAttributeName(LanguageBundleUtils.getString("partner.accountNumber"));
                tmpComp = genComponent.createComponent(componentTextDTO);
                genComponent.createCellLabel(componentTextDTO.getAttributeName(), componentTextDTO.getIsRequired(), tmpRow);
                if (tmpComp != null) {
                    tmpRow.appendChild(tmpComp);
                    lstInputTextBankCtrls.add(tmpComp);
                    if (firstComp == null) {
                        firstComp = tmpComp;
                    }
                }
                rows.insertBefore(tmpRow, lastRow);

                tmpRow = new Row();
                ComponentDTO componentDTO = new ComponentDTO();
                componentDTO.setAttributeType(ParamUtils.ReportDataType.COMBOBOX);
                componentDTO.setAttributeType(ParamUtils.ReportDataType.COMBOVALUE);
                componentDTO.setIsRequired(false);
                componentDTO.setStandardAttributeId(String.valueOf("cbb" + totalRow));
                componentDTO.setSelectItemValue(itemPartnerBankDTO.getBankCode());
                componentDTO.setAttributeName(LanguageBundleUtils.getString("partner.bank"));
                StringBuilder parValue = new StringBuilder();
                for (BankDTO itemBankDTO : lstBankDTO) {
                    parValue.append(itemBankDTO.getCode());
                    parValue.append(";");
                    parValue.append(itemBankDTO.getName());
                    parValue.append(";");
                }
                componentDTO.setInitValue(parValue.toString());
                tmpComp = genComponent.createComponent(componentDTO);
                genComponent.createCellLabel(componentDTO.getAttributeName(), componentDTO.getIsRequired(), tmpRow);
                if (tmpComp != null) {
                    tmpRow.appendChild(tmpComp);
                    lstInputCbbBankCtrls.add(tmpComp);
                    if (firstComp == null) {
                        firstComp = tmpComp;
                    }
                }
                rows.insertBefore(tmpRow, lastRow);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    private void removeComponentBank(int count) {
        int indexOf = rows.getChildren().indexOf(lastRow);
        if (indexOf >= 18) {
            for (int i = 1; i <= count; i++) {
                totalRow--;
                Component component = rows.getChildren().get(indexOf - i);
                if (i % 2 == 1) {
                    lstInputCbbBankCtrls.remove(component.getChildren().get(1));
                } else {
                    lstInputTextBankCtrls.remove(component.getChildren().get(1));
                }
                rows.removeChild(component);
            }
        }
    }
}
