/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.composer.contractManagement;

import com.viettel.csp.DTO.ContractDTO;
import com.viettel.csp.DTO.ContractHisDTO;
import com.viettel.csp.DTO.PartnerDTO;
import com.viettel.csp.DTO.ReasonDTO;
import com.viettel.csp.service.ContractHisService;
import com.viettel.csp.service.ContractService;
import com.viettel.csp.service.ContractStatusService;
import com.viettel.csp.service.PartnerService;
import com.viettel.csp.service.ReasonService;
import com.viettel.csp.util.KeyValueBean;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.util.StringUtils;
import com.viettel.eafs.util.SpringUtil;
import java.util.ArrayList;
import java.util.List;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Textbox;

/**
 * @author chitv@gemvietnam.com
 */
public class ContractManagementPopupReasonComposer extends GenericForwardComposer<Component> {

    final static org.apache.log4j.Logger logger = org.apache.log4j.Logger
        .getLogger(ContractManagementPopupReasonComposer.class);
    //    private Textbox txtCompanyName,txtAddressHeadOffice, txtPartnerCode, txtAddressTradingOffice, txtCompanyNameShort, txtPhone, txtRepName;
    private Label txtCompanyName, txtAddressHeadOffice, txtPartnerCode, txtAddressTradingOffice, txtCompanyNameShort, txtPhone, txtRepName, dbFilingDate, txtContractStatus;
    private Combobox cbbContractStatus;
    private Textbox txtContenReason;
    private Combobox cbbReason;
    private ListModelList<KeyValueBean> kvbContractStatus;
    private ListModelList<KeyValueBean> kvbReason;
    private List<KeyValueBean> lstContractStatus;
    private List<KeyValueBean> lstReason;
    private String action;
    private Long contractId;
    private ArrayList indexLst;
    private ContractDTO oldDTO;
    private PartnerDTO PartDTO;
    private ContractHisDTO contractHisDTO;
    private ContractService contractService = SpringUtil
        .getBean("contractService", ContractService.class);
    private PartnerService partnerService = SpringUtil
        .getBean("partnerService", PartnerService.class);
    private ContractStatusService contractStatusService = SpringUtil
        .getBean("contractStatusService", ContractStatusService.class);
    private ContractHisService contractHisService = SpringUtil
        .getBean("contractHisService", ContractHisService.class);
    private ReasonService reasonService = SpringUtil.getBean("reasonService", ReasonService.class);
    private ContractManagerPopupEditToCSPComposer contractManagerPopupEditToCSPComposer;

    //    private Datebox dbFilingDate;
    public ListModelList<KeyValueBean> getKvbContractStatus() {
        return this.kvbContractStatus;
    }

    public void setKvbContractStatus(ListModelList<KeyValueBean> kvbContractStatus) {
        this.kvbContractStatus = kvbContractStatus;
    }

    public List<KeyValueBean> getLstContractStatus() {
        return this.lstContractStatus;
    }

    public void setLstContractStatus(List<KeyValueBean> lstContractStatus) {
        this.lstContractStatus = lstContractStatus;
    }

    public Combobox getCbbReason() {
        return this.cbbReason;
    }

    public void setCbbReason(Combobox cbbReason) {
        this.cbbReason = cbbReason;
    }

    public ListModelList<KeyValueBean> getKvbReason() {
        return this.kvbReason;
    }

    public void setKvbReason(ListModelList<KeyValueBean> kvbReason) {
        this.kvbReason = kvbReason;
    }

    public List<KeyValueBean> getLstReason() {
        return this.lstReason;
    }

    public void setLstReason(List<KeyValueBean> lstReason) {
        this.lstReason = lstReason;
    }

    @Override
    public void doAfterCompose(Component comp) {
        try {
            super.doAfterCompose(comp);
            if (this.arg.containsKey(ParamUtils.COMPOSER)) {
                this.contractManagerPopupEditToCSPComposer = (ContractManagerPopupEditToCSPComposer) this.arg
                    .get(ParamUtils.COMPOSER);
            }
            if (this.arg.containsKey(ParamUtils.ACTION)) {
                this.action = this.arg.get(ParamUtils.ACTION).toString();
            }
            if (this.arg.containsKey("ID") && StringUtils
                .isValidString(String.valueOf(this.arg.get("ID")))) {
                this.contractId = (Long) this.arg.get("ID");
            }
            bindDataCbbContractStatus(ParamUtils.DEFAULT_VALUE_STR);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            Clients
                .showNotification(LanguageBundleUtils.getString("global.message.error"), "warning",
                    null,
                    "middle_center", 3000);
        }
    }

    public void bindDataCbbContractStatus(String valueSelect) throws Exception {
        try {
            this.reasonService.getListToKeyValueBeanByType(ParamUtils.REASON_TYPE.CONTRACT_PROFILE);
            List<ReasonDTO> lstReasonDTO = this.reasonService
                .findListByType(ParamUtils.REASON_TYPE.CONTRACT_PROFILE);
            this.lstReason = new ArrayList<>();
            this.lstReason.add(new KeyValueBean(ParamUtils.DEFAULT_VALUE_STR,
                LanguageBundleUtils.getString("global.combobox.choose")));
            if (lstReasonDTO != null && lstReasonDTO.size() > 0) {
                for (ReasonDTO reasonDTO : lstReasonDTO) {
                    KeyValueBean key = new KeyValueBean();
                    key.setKey(reasonDTO.getCode());
                    key.setValue(reasonDTO.getName());
                    this.lstReason.add(key);
                }
            }
            this.kvbReason = new ListModelList(this.lstReason);
            int i = 0;
            for (KeyValueBean itemParam : this.kvbReason.getInnerList()) {
                if (itemParam.getKey() != null && String.valueOf(itemParam.getKey())
                    .equals(valueSelect)) {
                    this.kvbReason.addToSelection(this.kvbReason.getElementAt(i));
                    break;
                }
                i++;
            }
            this.cbbReason.setModel(this.kvbReason);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            Clients
                .showNotification(LanguageBundleUtils.getString("global.message.error"), "warning",
                    null,
                    "middle_center", 3000);
        }
    }

    public ContractDTO getDataFromClient() {
        this.oldDTO.setCompanyName(this.txtCompanyName.getValue().trim().toLowerCase());

        this.oldDTO.setAddressHeadOffice(this.txtAddressHeadOffice.getValue().trim());

        this.oldDTO.setPartnerCode(this.txtPartnerCode.getValue().trim());

        this.oldDTO.setAddressTradingOffice(this.txtAddressTradingOffice.getValue().trim());

        this.oldDTO.setCompanyNameShort(this.txtCompanyNameShort.getValue().trim());

        this.oldDTO.setPhone(this.txtPhone.getValue().trim());

        this.oldDTO.setRepName(this.txtRepName.getValue().trim());

        return this.oldDTO;
    }

    public void onClick$btnSendReason() {
        try {
            String messageContract = "";
            String messageHis = "";
            this.contractHisDTO = getDataFromClientContractHis(this.oldDTO);
            messageHis = this.contractHisService.insert(this.contractId, this.contractHisDTO);
            if (ParamUtils.SUCCESS.equals(messageHis)) {
                Clients.showNotification(
                    LanguageBundleUtils.getString("global.message.create.successful"),
                    "info", null, "middle_center", 1000);
                this.self.detach();
            } else {
                Clients.showNotification(messageContract, "warning", null, "middle_center", 3000);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients
                .showNotification(LanguageBundleUtils.getString("global.message.error"), "warning",
                    null,
                    "middle_center", 3000);
        }
    }

    public ContractHisDTO getDataFromClientContractHis(ContractDTO contractDTO) {

        this.contractHisDTO = new ContractHisDTO();
        this.contractHisDTO.setContractId(this.contractId);
        if (this.cbbReason.getSelectedIndex() > 0) {
            this.contractHisDTO
                .setContent(
                    this.txtContenReason.getValue() + "-" + this.cbbReason.getSelectedItem()
                        .getLabel());
        }
        return this.contractHisDTO;
    }

    public void onClick$btnClose() {
        try {
            this.self.detach();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients
                .showNotification(LanguageBundleUtils.getString("global.message.error"), "warning",
                    null,
                    "middle_center", 3000);
        }
    }

}
