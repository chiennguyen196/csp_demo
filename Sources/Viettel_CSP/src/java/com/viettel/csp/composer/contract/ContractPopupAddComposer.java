/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.composer.contract;

import com.viettel.csp.DTO.ContractDTO;
import com.viettel.csp.DTO.ContractFilesDTO;
import com.viettel.csp.DTO.PartnerDTO;
import com.viettel.csp.entity.PartnerEntity;
import com.viettel.csp.service.ContractFilesService;
import com.viettel.csp.service.ContractHisService;
import com.viettel.csp.service.ContractService;
import com.viettel.csp.service.ContractStatusService;
import com.viettel.csp.service.PartnerService;
import com.viettel.csp.util.KeyValueBean;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.util.StringUtils;
import com.viettel.eafs.util.SpringUtil;
import com.viettel.logincsp.AuthenticationService;
import com.viettel.plc.widget.FileInfo;
import com.viettel.plc.widget.IUploaderCallback;
import com.viettel.plc.widget.UploadComponent;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.ForwardEvent;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

/**
 *
 * @author chitv@gemvietnam.com
 */
public class ContractPopupAddComposer extends GenericForwardComposer<Component> implements IUploaderCallback {

    private AuthenticationService authenticationService = SpringUtil.getBean("authenticationService", AuthenticationService.class);
    private Label txtCompanyName, txtAddressHeadOffice, txtPartnerCode, txtAddressTradingOffice, txtCompanyNameShort, txtPhone, txtRepName;
    private ListModelList<KeyValueBean> kvbContractStatus;
    private List<KeyValueBean> lstContractStatus;
    private String action;
    private Long contractId;
    private Long partnerId;
    private ArrayList indexLst;
    private ContractDTO contractDTO;
    private PartnerEntity partnerEntity;
    private PartnerDTO partNerDTO;
    private ContractFilesDTO contractFilesDTO;
    private Listbox lbxContractFiles, lbxContractFilesPartner, lbxContractFilesHistory;
    private ContractService contractService = SpringUtil.getBean("contractService", ContractService.class);
    private PartnerService partnerService = SpringUtil.getBean("partnerService", PartnerService.class);
    private ContractStatusService contractStatusService = SpringUtil.getBean("contractStatusService", ContractStatusService.class);
    private ContractHisService contractHisService = SpringUtil.getBean("contractHisService", ContractHisService.class);
    private ContractFilesService contractFilesService = SpringUtil.getBean("contractFilesService", ContractFilesService.class);
    final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ContractPopupAddComposer.class);
    private ContractComposer contractComposer;
    private UploadComponent uploadComponent;
    private List<String> filesLocation;
    private List<FileInfo> filesInfo;

    public Listbox getLbxContractFiles() {
        return lbxContractFiles;
    }

    public void setLbxContractFiles(Listbox lbxContractFiles) {
        this.lbxContractFiles = lbxContractFiles;
    }

    public Listbox getLbxContractFilesPartner() {
        return lbxContractFilesPartner;
    }

    public void setLbxContractFilesPartner(Listbox lbxContractFilesPartner) {
        this.lbxContractFilesPartner = lbxContractFilesPartner;
    }

    public Listbox getLbxContractFilesHistory() {
        return lbxContractFilesHistory;
    }

    public void setLbxContractFilesHistory(Listbox lbxContractFilesHistory) {
        this.lbxContractFilesHistory = lbxContractFilesHistory;
    }

    public ListModelList<KeyValueBean> getKvbContractStatus() {
        return kvbContractStatus;
    }

    public void setKvbContractStatus(ListModelList<KeyValueBean> kvbContractStatus) {
        this.kvbContractStatus = kvbContractStatus;
    }

    public List<KeyValueBean> getLstContractStatus() {
        return lstContractStatus;
    }

    public void setLstContractStatus(List<KeyValueBean> lstContractStatus) {
        this.lstContractStatus = lstContractStatus;
    }

    @Override
    public void doAfterCompose(Component comp) {
        try {
            super.doAfterCompose(comp);
            if (arg.containsKey(ParamUtils.COMPOSER)) {
                contractComposer = (ContractComposer) arg.get(ParamUtils.COMPOSER);
            }
            if (arg.containsKey(ParamUtils.ACTION)) {
                action = arg.get(ParamUtils.ACTION).toString();
            }

            if (arg.containsKey("PARTNER_ID")) {
                partnerId = (Long) arg.get("PARTNER_ID");
            }
            if (arg.containsKey("LIST_ACTION_INDEX")) {
                indexLst = (ArrayList) arg.get("LIST_ACTION_INDEX");
            }
            uploadComponent.setUploaderCallback(this);
            setData();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    public void setData() throws Exception {
        if (ParamUtils.ACTION_CREATE.equals(action)) {
            List<PartnerEntity> list = partnerService.getListPartnerContract(partnerId);
            partnerEntity = list.get(0);
            ((Window) self).setTitle(LanguageBundleUtils.getString("global.addNewContractFile"));
            if (partnerEntity != null) {
                txtCompanyName.setValue(partnerEntity.getCompanyName());
                txtAddressHeadOffice.setValue(partnerEntity.getAddressHeadOffice());
                txtPartnerCode.setValue(partnerEntity.getPartnerCode());
                txtAddressTradingOffice.setValue(partnerEntity.getAddressTradingOffice());
                txtCompanyNameShort.setValue(partnerEntity.getCompanyNameShort());
                txtPhone.setValue(partnerEntity.getPhone());
                txtRepName.setValue(partnerEntity.getRepName());
            }
        }
    }

    public void onDownloadFileContract(ForwardEvent evt) {
        evt.stopPropagation();
        try {
            Image btn = (Image) evt.getOrigin().getTarget();
            Listitem litem = (Listitem) btn.getParent().getParent();
            List<ContractFilesDTO> lstContractFilesResult = new ArrayList<>();
            lstContractFilesResult.add(litem.getValue());
            File file = new File(lstContractFilesResult.get(0).getPathFile());
            Filedownload.save(file, null);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(ContractComposer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onClick$btnDownloadFile(Listitem litem) {
        try {
            List<ContractFilesDTO> lstContractFilesResult = new ArrayList<>();
            lstContractFilesResult.add(litem.getValue());
            File file = new File(lstContractFilesResult.get(0).getPathFile());
            Filedownload.save(file, null);
        } catch (Exception ex) {
            Messagebox.show(LanguageBundleUtils.getMessage("messageUploadFileError"), LanguageBundleUtils.getMessage("Messagebox.warning"), Messagebox.OK, Messagebox.ERROR, null);
        }
    }

    public ContractDTO getDataFromClient(ContractDTO contractDTO) {
        contractDTO.setCompanyName(txtCompanyName.getValue().trim().toLowerCase());
        contractDTO.setAddressHeadOffice(txtAddressHeadOffice.getValue().trim());
        contractDTO.setPartnerCode(txtPartnerCode.getValue().trim());
        contractDTO.setAddressTradingOffice(txtAddressTradingOffice.getValue().trim());
        contractDTO.setCompanyNameShort(txtCompanyNameShort.getValue().trim());
        contractDTO.setPhone(txtPhone.getValue().trim());
        contractDTO.setRepName(txtRepName.getValue().trim());
        return contractDTO;
    }

    public PartnerDTO getDataFromClientPartner(ContractDTO contractDTO) {
        if (ParamUtils.ACTION_CREATE.equals(action)) {
            partNerDTO = new PartnerDTO();
            partNerDTO.setId(contractDTO.getPartnerId());
            partNerDTO.setCompanyName(contractDTO.getCompanyName());
            partNerDTO.setAddressHeadOffice(contractDTO.getAddressHeadOffice());
            partNerDTO.setPartnerCode(contractDTO.getPartnerCode());
            partNerDTO.setAddressTradingOffice(contractDTO.getAddressTradingOffice());
            partNerDTO.setCompanyNameShort(contractDTO.getCompanyNameShort());
            partNerDTO.setPhone(contractDTO.getPhone());
            partNerDTO.setRepName(contractDTO.getRepName());
        }
        return partNerDTO;
    }

    @Override
    public void uploadedFilesLocationCallback(List<String> filesLocation) {
        this.filesLocation = filesLocation;
        System.out.println(filesLocation);
    }

    @Override
    public void uploadedFilesInfoCallback(List<FileInfo> filesInfo) {
        this.filesInfo = filesInfo;
        System.out.println(filesInfo);
    }

    public void onClick$btnAddContractFiless() {
        try {
            int flat = 2;
            uploadComponent.onClick$doSave();
            ContractDTO contractDto = new ContractDTO();
            contractDto = getDataFromClient(contractDto);
            long contractId = contractService.insertContract(contractDto, flat);
            contractDto.setContractId(contractId);
            contractDto.setContractStatus(ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_WAITING_OK_CONTRACT);
            if (filesInfo!=null && filesInfo.size() > 0) {
                for (int i = 0; i < filesInfo.size(); i++) {
                    contractFilesDTO = getDataFromClientContractFiles(contractDto, filesInfo.get(i));
                    contractFilesService.insert(contractDto, contractFilesDTO);
                }
            }
            self.detach();
            contractComposer.bindDataToGrid();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    public ContractFilesDTO getDataFromClientContractFiles(ContractDTO contractDTO, FileInfo fileInfo) {

        contractFilesDTO = new ContractFilesDTO();
        contractFilesDTO.setContractId(contractDTO.getContractId());
        contractFilesDTO.setFileName(new File(fileInfo.getLocation()).getName());
        contractFilesDTO.setPathFile(new File(fileInfo.getLocation()).getPath());
        contractFilesDTO.setDescription(fileInfo.getDescription());
        return contractFilesDTO;
    }

    public void onClick$btnClose() {
        try {
            self.detach();
            contractComposer.bindDataToGrid();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    public String formatDateTime(Date date) throws Exception {
        return StringUtils.formatDateTime(date);
    }

}
