package com.viettel.csp.composer.partner;

import com.viettel.csp.DTO.*;
import com.viettel.csp.service.*;
import com.viettel.csp.service.impl.ContractViolationServiceImpl;
import com.viettel.csp.util.*;
import com.viettel.eafs.util.SpringUtil;
import com.viettel.plc.widget.FileInfo;
import com.viettel.plc.widget.IUploaderCallback;
import com.viettel.plc.widget.UploadComponent;
import org.apache.log4j.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.*;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by GEM on 6/14/2017.
 */
public class PartnerViolationPopupComposer extends GenericForwardComposer<Component> implements IUploaderCallback {
    private Textbox txtPartner;
    private Combobox cbNumber;
    private Combobox cbContractNumber;
    private Datebox startDate;
    private Textbox txtService;
    private Datebox endDate;
    private PartnerDTO objectPartner;
    private String action;
    private PartnerPopupComposer partnerPopupComposer;
    private long partnerID;
    private List<ContractDTO> contractDTOs;
    private Textbox txtNumber;
    private GenComponent component = new GenComponent();
    private boolean isSelect;
    private Textbox txtArea;
    private Listbox lbViolation;
    private Button btnUpdateViolate;
    private Listitem other;
    private boolean isChecked;
    private ContractDTO contractDTO;
    private Datebox endDateViolation;
    private Datebox startDateViolation;
    private UploadComponent uploadComponent;
    private List<FileInfo> filesInfo;
    private List<String> filesLocation;
    private Listitem publicViolater;
    private Listitem hiddenViolater;
    private Combobox cbViolate;
    private Textbox txtHeadOfService;
    private Combobox reasonVioltion;
    private Long contractBreachId;
    private ContractBreachDTO breachDTO;
    private Rows rowButton;
    private boolean isPopup;
    private Textbox txtContent;
    private Textbox txtContentValue;
    private Row rStartDate;
    private Row rEndDate;
    private Row rContent;
    private Row rContentValue;
    private Row rNumberViolation;
    private Textbox txtNumberViolation;
    private Button addViolation;
    private List<ContractBreachDisDTO> contractBreachDisDTOs = new ArrayList<>();
    private boolean isSaveData = true;
    private Listbox showViolation;
    private static final Logger logger = Logger.getLogger(PartnerViolationPopupComposer.class);

    private ContractService contractService = SpringUtil.getBean("contractService", ContractService.class);
    private ContractViolationService contractViolationService = SpringUtil.getBean("contractViolationService", ContractViolationService.class);
    private ContractBreachReasonService contractBreachReasonService = SpringUtil.getBean("contractBreachReasonService", ContractBreachReasonService.class);
    private ContractBreachFilesService contractBreachFilesService = SpringUtil.getBean("contractBreachFilesService", ContractBreachFilesService.class);
    private ContractBreachDisService contractBreachDisService = SpringUtil.getBean("contractBreachDisService", ContractBreachDisService.class);

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        isPopup = false;

        if (arg.containsKey(ParamUtils.ACTION)) {
            action = arg.get(ParamUtils.ACTION).toString();
        }
        if (arg.containsKey(ParamUtils.COMPOSER)) {
            partnerPopupComposer = (PartnerPopupComposer) arg.get(ParamUtils.COMPOSER);
        }
        if (arg.containsKey("PARNER_OBJECT")) {
            objectPartner = (PartnerDTO) arg.get("PARNER_OBJECT");
        }
        if (arg.containsKey("CONTRACT_BREACH")) {
            breachDTO = (ContractBreachDTO) arg.get("CONTRACT_BREACH");
            isPopup = true;
        }

        partnerID = (long) Sessions.getCurrent().getAttribute("partnerID");
        uploadComponent.setUploaderCallback(this);

        binData(isPopup);
    }


    private void binData(boolean isPopup) throws Exception {
        isSaveData = true;
        txtPartner.setReadonly(true);
        txtService.setReadonly(true);
        txtNumber.setReadonly(true);
        startDate.setReadonly(true);
        endDate.setReadonly(true);
        txtArea.setDisabled(true);
        uploadComponent.setVisible(true);
        rowButton.setVisible(true);
        txtPartner.setValue(objectPartner.getCompanyName());
        rContent.setVisible(false);
        rContentValue.setVisible(false);
        rNumberViolation.setVisible(false);
        isChecked = false;
        showViolation.setVisible(false);
        //Dk
        txtContentValue.setValue("");
        txtContent.setValue("");
        startDateViolation.setValue(new Date());
        endDateViolation.setValue(new Date());

        contractDTOs = contractService.getListContractForPartnerLock(new ContractDTO());
       // contractDTOs = contractViolationService.fillContractListByStatus(contractDTOs, 26);
        String value = null;
        if (isPopup) {
            contractDTOs = contractService.getContractByContractId(contractDTOs, breachDTO.getContractId());
            onSelect$cbContractNumber();
            value = contractDTOs.get(0).getContractNumber();

            if (breachDTO.getIsPublic() == 1) {
                publicViolater.setSelected(true);
            } else {
                hiddenViolater.setSelected(true);
            }

            List<ContractBreachReasonDTO> breachReasonDTOs = contractBreachReasonService.getByContractBreachId(breachDTO.getId());
            setViolations(breachReasonDTOs);

            endDateViolation.setValue(breachDTO.getEndDate());
            startDateViolation.setValue(breachDTO.getStartDate());
            txtHeadOfService.setValue(breachDTO.getHeadOfService());
            uploadComponent.setVisible(false);
            rowButton.setVisible(false);


        }

        component.bindDataCbbContactType(cbContractNumber, contractViolationService.getListToKeyValueBeanByType(contractDTOs), value, isPopup);
        setComboboxViolation(isPopup); // bang discipline
        setComboboxReason(isPopup);
    }

    private void setCbViolate(ContractBreachDisDTO contractBreachDisDTO) {
        if (contractBreachDisDTO == null) {
            return;
        }
        String key = contractBreachDisDTO.getDisCode();
        String value = getValueForCbViolate(key);
        cbViolate.setValue(value);
        setVisiableForRow(Integer.parseInt(key));
    }

    private String getValueForCbViolate(String key) {
        switch (key) {
            case "1":
                return LanguageBundleUtils.getString("partner.violation.blockNumber");
            case "2":
                return LanguageBundleUtils.getString("partner.violation.stopService");
            case "3":
                return LanguageBundleUtils.getString("partner.violation.stopContract");
            case "4":
                return LanguageBundleUtils.getString("partner.violation.dropContract");
            case "5":
                return LanguageBundleUtils.getString("partner.violation.money");
            case "6":
                return LanguageBundleUtils.getString("partner.violation.notify");
        }
        return "";
    }


    public void onSelect$cbViolate() {
        if (cbViolate.getSelectedItem().getValue().equals("-99"))
            return;

        int key = (int) cbViolate.getSelectedItem().getValue();
        for (ContractBreachDisDTO disDTO : contractBreachDisDTOs) {
            if (disDTO.getDisCode().equals(String.valueOf(key))) {
                disBreachDTO = disDTO;
            }
        }

        setVisiableForRow(key);
        if (isPopup)
            setDateWithKey(disBreachDTO);
    }

    ContractBreachDisDTO disBreachDTO;

    private void setVisiableForRow(int key) {
        switch (key) {
            case 6:
                rStartDate.setVisible(false);
                rEndDate.setVisible(false);
                rContent.setVisible(true);
                rContentValue.setVisible(false);
                rNumberViolation.setVisible(false);
                return;
            case 1:
            case 2:
            case 3:
                rStartDate.setVisible(true);
                rEndDate.setVisible(true);
                rContent.setVisible(false);
                rContentValue.setVisible(false);
                break;
            case 4:
                rStartDate.setVisible(true);
                rEndDate.setVisible(false);
                rContent.setVisible(false);
                rContentValue.setVisible(false);
                break;
            case 5:
                rStartDate.setVisible(false);
                rEndDate.setVisible(false);
                rContent.setVisible(true);
                rContentValue.setVisible(true);
                break;
            case -99:
                return;
        }
        rNumberViolation.setVisible(false);
    }

    private void setViolations(List<ContractBreachReasonDTO> breachReasonDTOs) {
        List<Listitem> items = lbViolation.getItems();

        for (ContractBreachReasonDTO dto : breachReasonDTOs) {
            for (Listitem item : items) {
                String value = item.getValue();
                if (value.equals(dto.getReasonCode())) {
                    item.setSelected(true);
                    if (value.equals("6")) {
                        txtArea.setDisabled(false);
                        txtArea.setValue(dto.getOther());
                    }
                }
            }
        }

    }



    public void setComboboxViolation(boolean isPopup) throws Exception {
        List<String> stringList = new ArrayList<>();
        String value = null;
        if (isPopup == false) {
            stringList.add(LanguageBundleUtils.getString("partner.violation.blockNumber")); // 1
            stringList.add(LanguageBundleUtils.getString("partner.violation.stopService")); // 2
            stringList.add(LanguageBundleUtils.getString("partner.violation.stopContract")); // 3
            stringList.add(LanguageBundleUtils.getString("partner.violation.dropContract")); // 4
            stringList.add(LanguageBundleUtils.getString("partner.violation.money")); // 5
            stringList.add(LanguageBundleUtils.getString("partner.violation.notify")); //6
        } else {
            //get tu db
            contractBreachDisDTOs = contractBreachDisService.getBreachContractId(breachDTO.getId());
            for (ContractBreachDisDTO disDTO : contractBreachDisDTOs) {
                String strCode = disDTO.getDisCode();
                stringList.add(getValueForCbViolate(strCode));
            }

            setVisiableForRow(Integer.parseInt(contractBreachDisDTOs.get(0).getDisCode()));
            setDateWithKey(contractBreachDisDTOs.get(0));
            processView();
            ListModelList model = new ListModelList(contractBreachDisDTOs);
            model.setMultiple(true);
            showViolation.setModel(model);
        }

        component.bindDataCbbContactType(cbViolate, contractViolationService.bindDataToCombobox(stringList), value, isPopup);
    }

    private void processView(){
        String headOfService = breachDTO.getHeadOfService();

        for (ContractBreachDisDTO dto:contractBreachDisDTOs){
            dto = contractBreachDisService.setValue(Integer.parseInt(dto.getDisCode()), dto, dto.getPauseStartDate(), dto.getPauseEndDate(),
                    dto.getContent(), dto.getContentValue(), headOfService, 0);
        }
    }

    private void setDateWithKey(ContractBreachDisDTO contractBreachDisDTO) {
        int key = Integer.parseInt(contractBreachDisDTO.getDisCode());
        switch (key) {
            case 1:
            case 2:
            case 3:
                startDateViolation.setValue(contractBreachDisDTO.getPauseStartDate());
                endDateViolation.setValue(contractBreachDisDTO.getPauseEndDate());
                break;
            case 4:
                startDateViolation.setValue(contractBreachDisDTO.getDateOff());
                break;
            case 5:
                txtContent.setValue(contractBreachDisDTO.getContent());
                txtContentValue.setValue(contractBreachDisDTO.getContentValue());
                break;
            case 6:
                txtContent.setValue(contractBreachDisDTO.getContent());
                txtNumberViolation.setValue(contractBreachDisDTO.getContentValue());
                break;
        }

    }

    private void setComboboxReason(boolean isPopup) throws Exception {
        List<String> stringList = new ArrayList<>();
        String value = null;
        if (isPopup == false) {
            stringList.add(LanguageBundleUtils.getString("partner.violation.violateContract"));
            stringList.add(LanguageBundleUtils.getString("partner.violation.required"));
        } else {
            value = breachDTO.getReasonDisciplineCode();

            stringList.add(value);
        }

        component.bindDataCbbContactType(reasonVioltion, contractViolationService.bindDataToCombobox(stringList), value, isPopup);
    }

    public void onSelect$cbContractNumber() {
        String selectCb = cbContractNumber.getValue();
        isSelect = selectCb.equals(LanguageBundleUtils.getString("global.combobox.choose"));
        if (isSelect == true) {
            return;
        }

        contractDTO = new ContractDTO();
        for (ContractDTO dto : contractDTOs) {
            if (isPopup == false && dto.getContractNumber().equals(selectCb)) {
                contractDTO = dto;
            }
            if (isPopup == true) {
                contractDTO = dto;
            }
        }
        txtNumber.setValue(contractDTO.getHeadOfService());
        startDate.setValue(contractDTO.getStartDate());
        endDate.setValue(contractDTO.getEndDate());
        txtService.setValue(contractDTO.getServiceCode());
    }

    private void isSave(boolean flag){
        isSaveData = flag;
    }


    public void onClick$btnUpdateViolate() {

        List<String> violationList = getListCheckbox(lbViolation);
        if (validate(violationList) == false) {
            Clients.showNotification(LanguageBundleUtils.getString("partner.violation.message.info"), "warn", null, "middle_center", 3000);
            return;
        }

        //Phần popup xác nhận
        Messagebox.show("Bạn có muốn lưu lại?", "Question", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION, new EventListener()
        {
            public void onEvent(Event e)
            {
                if (Messagebox.ON_NO.equals(e.getName()))
                {
                    isSave(false);
                    return;
                }
            }
        });

        if (isSaveData == false)
            return;

        boolean check = contractViolationService.checkExistByContractId(contractDTO.getContractId());
        ContractBreachDTO contractBreachDTO = createContractBreachDTO(check);
        //File
        Sessions.getCurrent().setAttribute("violation", "ok");
        uploadComponent.onClick$doSave();
        Sessions.getCurrent().removeAttribute("violation");

        if (check) {
            contractViolationService.update(contractBreachDTO);
        } else {
            contractViolationService.insert(contractBreachDTO);
        }
        contractBreachDTO = contractViolationService.getContractBreachByContractId(contractBreachDTO.getContractId());

        //set data mac dinh trong truong hop null
//        ContractBreachDisDTO contractBreachDisDTO = contractBreachDisService.createBreachDisDTO(cbViolate.getSelectedItem().getValue(), startDateViolation.getValue(),
//                                                        endDateViolation.getValue(), txtContent.getValue(), txtContentValue.getValue(), contractBreachDTO.getId());


//        ContractBreachDisDTO contractBreachDisDTO = createBreachDisList(contractBreachDTO.getId(), false);
//        if (contractBreachDisDTO == null) {
//            return;
//        }
        //insertList
        if (contractBreachDisDTOs.size() != 0) {
            setBreachIdForDisList(contractBreachDTO.getId());
            contractBreachDisService.insertDataList(contractBreachDisDTOs);
        }

        List<ContractBreachReasonDTO> listReason = createContractReasonDTO(violationList);
        contractBreachReasonService.insertList(listReason);
        saveFileInfo();
        Clients.showNotification(LanguageBundleUtils.getString("partner.violation.message.success"), "ok", null, "middle_center", 3000);
    }

    private void setBreachIdForDisList(long id) {
        for (ContractBreachDisDTO dto : contractBreachDisDTOs) {
            dto.setContractBreachId(id);
        }
    }

    private ContractBreachDisDTO createBreachDisList(long contractBreachId, boolean flag) {
        if (cbViolate.getSelectedItem().getValue().equals("-99")){
            Clients.showNotification(LanguageBundleUtils.getString("partner.violation.message.info"), "warn", null, "middle_center", 2000);
            return null;
        }

        ContractBreachDisDTO contractBreachDisDTO = contractBreachDisService.createBreachDisDTO(cbViolate.getSelectedItem().getValue(), startDateViolation.getValue(),
                endDateViolation.getValue(), txtContent.getValue(), txtContentValue.getValue(), txtHeadOfService.getValue(), contractBreachId);
        for (ContractBreachDisDTO dto : contractBreachDisDTOs) {
            if (dto.getDisCode().equals(contractBreachDisDTO.getDisCode())) {
                Clients.showNotification(LanguageBundleUtils.getString("partner.violation.message.dupViolation"), "warn", null, "middle_center", 2000);
                return null;
            }
        }

        contractBreachDisDTOs.add(contractBreachDisDTO);

        return contractBreachDisDTO;
    }

    private boolean validate(List<String> violationList) {
        if (txtHeadOfService.getValue().equals("") || violationList == null || violationList.size() == 0)
            return false;
        if (reasonVioltion.getValue().equals(LanguageBundleUtils.getString("global.combobox.choose")))
            return false;

        if (publicViolater.isSelected() == false && hiddenViolater.isSelected() == false)
            return false;

//        if (cbViolate.getValue().equals(LanguageBundleUtils.getString("global.combobox.choose")))
//            return false;

        if (cbContractNumber.getValue().equals(LanguageBundleUtils.getString("global.combobox.choose")))
            return false;
        txtContentValue.setValue("");
        txtContent.setValue("");

        return true;
    }

    public void saveFileInfo() {
        if (filesInfo == null)
            return;

        List<ContractBreachFileDTO> fileList = new ArrayList<>();
        for (FileInfo file : filesInfo) {
            ContractBreachFileDTO filesDTO = new ContractBreachFileDTO();
            filesDTO.setDescription(file.getDescription());
            filesDTO.setFileName(getFileName(file.getLocation()));
            filesDTO.setPartnerBreachId(contractBreachId);
            filesDTO.setUploadDate(new Date());
            filesDTO.setPathFile(file.getLocation());
            fileList.add(filesDTO);
        }

        try {
            contractBreachFilesService.insertList(fileList);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    private String getFileName(String location) {
        File file = new File(location);
        return file.getName();
    }

    private ContractBreachDTO createContractBreachDTO(boolean isUpdate) {
        ContractBreachDTO contractBreachDTO = new ContractBreachDTO();
        contractBreachDTO.setContractId(contractDTO.getContractId());
        contractBreachDTO.setStartDate(startDateViolation.getValue());
        contractBreachDTO.setEndDate(endDateViolation.getValue());
        contractBreachDTO.setUpdateBY(UserTokenUtils.getUserId());
        contractBreachDTO.setPartnerId(partnerID);
        contractBreachDTO.setUpdateAt(new Date());
        contractBreachDTO.setHeadOfService(txtHeadOfService.getValue());
        contractBreachDTO.setReasonDisciplineCode(reasonVioltion.getValue());
        if (publicViolater.isSelected()) {
            contractBreachDTO.setIsPublic(1L); // 1 la public | 0 : khong public
        } else {
            contractBreachDTO.setIsPublic(0L); // 1 la public | 0 : khong public
        }

        if (isUpdate == false) {
            contractBreachDTO.setCreateAt(new Date());
            contractBreachDTO.setCreateBy(UserTokenUtils.getUserId());
        }

        return contractBreachDTO;
    }

    private List<ContractBreachReasonDTO> createContractReasonDTO(List<String> violationList) {
        List<ContractBreachReasonDTO> dtos = new ArrayList<>();
        contractBreachId = contractViolationService.getContractBreachByContractId(contractDTO.getContractId()).getId();// return entity

        for (int i = 0; i < violationList.size(); i++) {
            ContractBreachReasonDTO dto = new ContractBreachReasonDTO();
            dto.setContractBreachId(contractBreachId);
            String code = violationList.get(i);
            dto.setReasonCode(code);
            if (code.equals("6")) {
                dto.setOther(violationList.get(i + 1));
                dtos.add(dto);
                break;
            }
            dto.setOther("");
            dtos.add(dto);
        }

        return dtos;
    }

    public void onClick$other() {
        isChecked = true;
        txtArea.setDisabled(false);
        if (!other.isSelected()){
            txtArea.setDisabled(true);
        }else {
            txtArea.setDisabled(false);
        }
    }

    public void onClick$addViolation() {
        if (createBreachDisList(0L, true) == null) {
            return;
        }
        Clients.showNotification(LanguageBundleUtils.getString("partner.violation.message.addViolationSucess"), "ok", null, "middle_center", 2000);
        Clients.showNotification(LanguageBundleUtils.getString("partner.violation.message"), "ok", null, "middle_center", 2000);
        ListModelList model = new ListModelList(contractBreachDisDTOs);
        model.setMultiple(true);
        showViolation.setModel(model);
        showViolation.setVisible(true);
    }

    private List<String> getListCheckbox(Listbox listbox) {
        List<Listitem> items = listbox.getItems();

        List<String> values = new ArrayList<>();
        for (Listitem item : items) {
            if (item.isSelected()) {
                String value = item.getValue();
                values.add(value);
                if (isChecked == true && value.equals("6")) {
                    values.add(txtArea.getValue());
                }
            }
        }

        return values;
    }


    @Override
    public void uploadedFilesInfoCallback(List<FileInfo> filesInfo) {
        this.filesInfo = filesInfo;
        System.out.println(filesInfo);
    }

    @Override
    public void uploadedFilesLocationCallback(List<String> filesLocation) {
        this.filesLocation = filesLocation;
        System.out.println(filesLocation);
    }

    public String formatDateTime(Date date) throws Exception {
        return StringUtils.formatDateTime(date);
    }

    public void delete(){
        if(isPopup){
            return;
        }

        ContractBreachDisDTO dto = showViolation.getSelectedItem().getValue();
        for (ContractBreachDisDTO disDTO:contractBreachDisDTOs){
            if(disDTO.getViolationName().equals(dto.getViolationName())){
                contractBreachDisDTOs.remove(disDTO);
                break;
            }
        }

        ListModelList model = new ListModelList(contractBreachDisDTOs);
        model.setMultiple(true);
        showViolation.setModel(model);
    }

    public Listbox getShowViolation() {
        return showViolation;
    }

    public void setShowViolation(Listbox showViolation) {
        this.showViolation = showViolation;
    }
}
