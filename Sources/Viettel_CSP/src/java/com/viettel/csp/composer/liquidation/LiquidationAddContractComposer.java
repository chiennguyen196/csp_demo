/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.composer.liquidation;

import com.viettel.csp.DTO.*;
import com.viettel.csp.composer.contract.ContractComposer;
import com.viettel.csp.service.*;
import com.viettel.csp.util.GenComponent;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.util.StringUtils;
import com.viettel.eafs.util.SpringUtil;
import com.viettel.plc.widget.FileInfo;
import com.viettel.plc.widget.IUploaderCallback;
import com.viettel.plc.widget.UploadComponent;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.ForwardEvent;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.*;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;

import static jxl.biff.BaseCellFeatures.logger;

/**
 * @author admin
 */
public class LiquidationAddContractComposer extends GenericForwardComposer<Component> implements
        IUploaderCallback {

    private final ContactTypeService contactTypeService = SpringUtil
            .getBean("contactTypeService", ContactTypeService.class);
    private final ContactService contactService = SpringUtil
            .getBean("contactService", ContactService.class);
    private final ContractFilesService contractFilesService = SpringUtil
            .getBean("contractFilesService", ContractFilesService.class);
    private final ContractHisService contractHisService = SpringUtil
            .getBean("contractHisService", ContractHisService.class);
    private final ContactPointService contactPointService = SpringUtil
            .getBean("contactPointService", ContactPointService.class);
    private final ContractService contractService = SpringUtil
            .getBean("contractService", ContractService.class);
    Long partner_ID, contractId;
    String contractType;
    private Label txtContractNumber, txtCompanyName, txtHeadOfService, txtServiceCode, dbStartDate, dbEndDate, txtContractStatus, txtContractType, txtCtSignForm;
    private Long totalRow = 0L;
    private GenComponent genComponent = new GenComponent();
    private List<Component> lstInputCbbContact = new ArrayList<>();
    private List<ContactTypeDTO> lstInputContactType = new ArrayList<>();
    private List<Component> lstInputTextContactType = new ArrayList<>();
    private String action;
    private Rows rows;
    private Row lastRow;
    private ContractDTO cdto;
    private ContactPointDTO pointDTO;
    private Textbox txtFileContractNumber, txtContent, txtDescription, txtFileName;
    //    private Datebox dbStartDate, dbEndDate;
    private List<ContractFilesDTO> lstContractFiles = new ArrayList<>();
    private Listbox lbxViewAddContractManagement, lbxViewAddContract, lbxContractHis;
    private List<ContractFilesDTO> lstContractFilesDTO = new ArrayList<>();
    private List<ContractHisDTO> lstContractHisDTO = new ArrayList<>();
    private ContractDTO oldDTO;
    private List<String> filesLocation;
    private List<FileInfo> filesInfo;
    private UploadComponent uploadComponent;
    private LiquidationComposer liquidationComposer;
    private Grid grdContactPoint;
    List<ContactPointDTO> contractPointDTOs;

    public LiquidationComposer getLiquidationComposer() {
        return liquidationComposer;
    }

    public void setLiquidationComposer(LiquidationComposer liquidationComposer) {
        this.liquidationComposer = liquidationComposer;
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);

        if (this.arg.containsKey("PARTNER_ID")) {
            this.partner_ID = (Long) this.arg.get("PARTNER_ID");
        }
        if (this.arg.containsKey("CONTRACT_ID")) {
            this.contractId = (Long) this.arg.get("CONTRACT_ID");
        }
        if (this.arg.containsKey("contractType")) {
            this.contractType = (String) this.arg.get("contractType");
        }
        if (this.arg.containsKey(ParamUtils.ACTION)) {
            this.action = this.arg.get(ParamUtils.ACTION).toString();
        }
        if (this.arg.containsKey(ParamUtils.COMPOSER)) {
            this.liquidationComposer = (LiquidationComposer) this.arg
                    .get(ParamUtils.COMPOSER);
        }

        if ("002".equals(this.contractType)) {
            this.uploadComponent.setVisible(false);
        } else {
            this.uploadComponent.setUploaderCallback(this);
        }

//        loadLBXContractFiles(this.contractId);
//        loadLBXContract(contractId);
        setData();
    }


    public void loadLBXContractFiles(Long contractId) {
        ContractFilesDTO contractFilesDTO = new ContractFilesDTO();
        contractFilesDTO.setContractId(contractId);
        this.lstContractFilesDTO = this.contractFilesService
                .getListMenuContractFiles(contractFilesDTO);
        if (this.lstContractFilesDTO != null) {
            ListModelList model = new ListModelList(this.lstContractFilesDTO);
            model.setMultiple(true);
            this.lbxViewAddContractManagement.setModel(model);
        }
    }

    public void onDeleteContract(ForwardEvent evt) {
        evt.stopPropagation();

        try {
            List<ContractFilesDTO> lstDeleteDTO = new ArrayList<>();
            Image btn = (Image) evt.getOrigin().getTarget();
            Listitem litem = (Listitem) btn.getParent().getParent();
            ContractFilesDTO deleteDTO = ((ContractFilesDTO) litem.getValue());
            lstDeleteDTO.add(deleteDTO);
            String message = this.contractFilesService.deleteContractFiles(lstDeleteDTO);
            Clients.showNotification(message, "suscess", null, "middle_center", 3000);
            loadLBXContractFiles(this.contractId);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(ContractComposer.class.getName())
                    .log(Level.SEVERE, null, ex);
        }
    }

    public void onDownloadFileContract(ForwardEvent evt) {
        evt.stopPropagation();
        Image btn = (Image) evt.getOrigin().getTarget();
        Listitem litem = (Listitem) btn.getParent().getParent();
        onClick$btnDownloadFile(litem);
    }

    public void onClick$btnDownloadFile(Listitem litem) {
        try {
            List<ContractFilesDTO> lstContractFilesResult = new ArrayList<>();
            lstContractFilesResult.add(litem.getValue());
            File file = new File(lstContractFilesResult.get(0).getPathFile());
            Filedownload.save(file, null);
        } catch (Exception ex) {
            Messagebox.show(LanguageBundleUtils.getMessage("messageUploadFileError"),
                    LanguageBundleUtils.getMessage("Messagebox.warning"), Messagebox.OK,
                    Messagebox.ERROR,
                    null);
        }
    }


    public void loadLBXContract(Long contractId) {
        ContractFilesDTO contractFilesDTO = new ContractFilesDTO();
        contractFilesDTO.setContractId(contractId);
        this.lstContractFilesDTO = this.contractFilesService.getMenuContractFile(contractFilesDTO);
        if (this.lstContractFilesDTO != null) {
            ListModelList model = new ListModelList(this.lstContractFilesDTO);
            model.setMultiple(true);
            this.lbxViewAddContract.setModel(model);
        }
    }


    public ContactPointDTO getDataFromClient() {
        this.pointDTO = new ContactPointDTO();
        List<ContactPointDTO> lstContactPointDTO = new ArrayList<>();
        List<PartnerBankDTO> lstPartnerBankDTO = new ArrayList<>();
        List<ContactDTO> lstContactDTO = new ArrayList<>();
        List<String> lstCbbContactDTO = new ArrayList(
                this.genComponent.getDataFromControls(this.lstInputCbbContact).values());
        for (int i = 0; i < this.lstInputContactType.size(); i++) {
            ContactPointDTO contactPointDTO = new ContactPointDTO();
            contactPointDTO.setContractId(this.contractId);
            contactPointDTO.setContactId(Long.parseLong(lstCbbContactDTO.get(i)));
            contactPointDTO.setContactType(this.lstInputContactType.get(i).getCode());
            contactPointDTO.setId(this.lstInputContactType.get(i).getContactPointID());
            lstContactPointDTO.add(contactPointDTO);
        }
        this.pointDTO.setListContactPointDTO(lstContactPointDTO);
        return this.pointDTO;
    }

    public void onClick$btnclosePopup() {
        self.detach();
    }

    public void onClick$btnAddFilePopup() {
        try {
            Map<String, Object> argu = new HashMap();
            argu.put(ParamUtils.ACTION, ParamUtils.ACTION_CREATE);
            argu.put(ParamUtils.COMPOSER, this);
            Window wd;
            wd = (Window) Executions
                    .createComponents("controls/liquidation/liquidationAddFile.zul",
                            null,
                            argu);
            wd.doModal();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    public void onClick$btnViewFilePopup() {
        try {
            if (lbxViewAddContract.getSelectedItem().getIndex() > 0) {
                Map<String, Object> argu = new HashMap();
                argu.put(ParamUtils.ACTION, ParamUtils.ACTION_UPDATE_FILE);
                List<Integer> indexs = new ArrayList();
                indexs.add(this.lbxViewAddContract.getSelectedItem().getIndex());
                argu.put("LIST_ACTION_INDEX", indexs);
                argu.put("LISTBOX", this.lbxViewAddContract.getSelectedItem().getValue());
                argu.put(ParamUtils.COMPOSER, this);
                this.lstContractFiles
                        .remove(this.lstContractFiles.get(this.lbxViewAddContract.getSelectedIndex()));
                this.lbxViewAddContract.removeItemAt(this.lbxViewAddContract.getSelectedIndex());
                Window wd;
                wd = (Window) Executions
                        .createComponents("controls/liquidation/liquidationAddFile.zul",
                                null,
                                argu);
                wd.doModal();
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    public void setDataGrid(ContractFilesDTO contractFilesDTO) {
        this.lstContractFiles.add(contractFilesDTO);
        ListModelList model = new ListModelList(this.lstContractFiles);
        model.setMultiple(true);
        this.lbxViewAddContract.setModel(model);
    }

    public String formatDateTime(Date date) throws Exception {
        return StringUtils.formatDateTime(date);
    }

    public void setData() throws Exception {
        if (ParamUtils.ACTION_CREATE.equals(this.action)) {
            ((Window) this.self).setTitle(LanguageBundleUtils.getString("global.add"));

        } else if (ParamUtils.ACTION_UPDATE.equals(this.action)) {
            ContractDTO objectSearch = new ContractDTO();
            objectSearch.setContractId(this.contractId);
            List<ContractDTO> list = this.contractService.getListContract(objectSearch);
            this.oldDTO = list.get(0);
            ((Window) this.self).setTitle(LanguageBundleUtils.getString("global.edit"));
            if (this.oldDTO != null) {
                this.txtCompanyName.setValue(this.oldDTO.getCompanyName());
                this.txtContractNumber.setValue(this.oldDTO.getAddressHeadOffice());
                this.txtHeadOfService.setValue(this.oldDTO.getPartnerCode());
                this.txtServiceCode.setValue(this.oldDTO.getAddressTradingOffice());
                String date = "dd/MM/yyyy";
                String dateStr = null;
                String dateStrEnd = null;
                DateFormat df = new SimpleDateFormat(date);
                dateStr = df.format(this.oldDTO.getStartDate());
                dateStrEnd = df.format(this.oldDTO.getEndDate());
                this.dbStartDate.setValue(dateStr);
                this.dbEndDate.setValue(dateStr);
                this.txtCtSignForm.setValue(this.oldDTO.getCtSignForm());
                this.txtContractStatus.setValue(this.oldDTO.getContractStatusName());
                this.txtContractType.setValue(this.oldDTO.getContractType());
                this.contractPointDTOs = this.contactPointService.getList(contractId);
                if (this.contractPointDTOs != null && !this.contractPointDTOs.isEmpty()) {
                    ListModelList model = new ListModelList(this.contractPointDTOs);
                    model.setMultiple(false);
                    grdContactPoint.setModel(model);
                }
            }
        }
    }

    @Override
    public void uploadedFilesInfoCallback(List<FileInfo> filesInfo) {
        this.filesInfo = filesInfo;
        System.out.println(filesInfo);
    }

    @Override
    public void uploadedFilesLocationCallback(List<String> filesLocation) {
        this.filesLocation = filesLocation;
    }

    public void onClick$btnLiquidationContract() {
        try {
            long id;
            id = this.contractService.updateCreatLiquidation(this.oldDTO, this.action, ParamUtils.LI_STATUS.LIQUIDATION_CREAT);
            if (id > 0) {
                this.liquidationComposer.bindDataToGrid();
                Clients
                        .showNotification(
                                LanguageBundleUtils.getString("global.message.create.successful"),
                                "info", null, "middle_center", 1000);
                this.self.detach();
            } else {
                Clients
                        .showNotification(ParamUtils.SUCCESS, "warning", null, "middle_center", 3000);
            }

        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients
                    .showNotification(LanguageBundleUtils.getString("global.message.error"), "warning",
                            null,
                            "middle_center", 3000);
        }
    }
//
//    public void onClick$btnRemoveFilePopup() {
//        try {
//            if (this.lbxViewAddContract.getSelectedCount() > 0) {
//                this.lstContractFiles
//                        .remove(this.lstContractFiles.get(this.lbxViewAddContract.getSelectedIndex()));
//                this.lbxViewAddContract.removeItemAt(this.lbxViewAddContract.getSelectedIndex());
//            }
//        } catch (Exception ex) {
//            logger.error(ex.getMessage(), ex);
//            Clients
//                    .showNotification(LanguageBundleUtils.getString("global.message.error"), "warning",
//                            null,
//                            "middle_center", 3000);
//        }
//    }

}
