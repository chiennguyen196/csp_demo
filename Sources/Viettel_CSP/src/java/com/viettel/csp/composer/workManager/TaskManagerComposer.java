/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.composer.workManager;

import com.viettel.csp.DTO.TasksDTO;
import com.viettel.csp.entity.MessageEntity;
import com.viettel.csp.service.ContractService;
import com.viettel.csp.service.MessageService;
import com.viettel.csp.service.TasksService;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.util.StringUtils;
import com.viettel.csp.util.UserTokenUtils;
import com.viettel.eafs.util.SpringUtil;

import java.util.*;

import org.apache.log4j.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.*;

/**
 * @author admin
 */
public class TaskManagerComposer extends GenericForwardComposer<Component> {

    private static final Logger logger = Logger.getLogger(TaskManagerComposer.class);

    private TasksService tasksService = SpringUtil.getBean("tasksService", TasksService.class);
    private MessageService messageService=SpringUtil.getBean("messageService", MessageService.class);
    private ContractService contractService = SpringUtil.getBean("contractService", ContractService.class);

    private List<TasksDTO> tasksDTOS;
    private TasksDTO tasksDTO;
    private Listbox lbxTaskWait;
    private Listbox lbxTaskActive;
    private Listbox lbxTaskDone;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        tasksDTO = new TasksDTO();
        tasksDTO.setAssignBy(UserTokenUtils.getUserId());
        this.bindData();
    }

    public void bindData() throws Exception {
        tasksDTO.setStatus(ParamUtils.TASKS_STATUS.WAIT_ACTIVE);
        this.tasksDTOS = this.tasksService.findAllToDTO(tasksDTO);
        if (this.tasksDTOS != null && this.tasksDTOS.size() > 0) {
            ListModelList model = new ListModelList(this.tasksDTOS);
            model.setMultiple(false);
            lbxTaskWait.setModel(model);
        } else {
            lbxTaskWait.setModel(new ListModelList());
        }

        tasksDTO.setStatus(ParamUtils.TASKS_STATUS.OK_ACTIVE);
        this.tasksDTOS = this.tasksService.findAllToDTO(tasksDTO);
        if (this.tasksDTOS != null && this.tasksDTOS.size() > 0) {
            ListModelList model = new ListModelList(this.tasksDTOS);
            model.setMultiple(false);
            lbxTaskActive.setModel(model);
        } else {
            lbxTaskActive.setModel(new ListModelList());
        }

        tasksDTO.setStatus(ParamUtils.TASKS_STATUS.DONE_TASK);
        this.tasksDTOS = this.tasksService.findAllToDTO(tasksDTO);
        if (this.tasksDTOS != null && this.tasksDTOS.size() > 0) {
            ListModelList model = new ListModelList(this.tasksDTOS);
            model.setMultiple(false);
            lbxTaskDone.setModel(model);
        } else {
            lbxTaskDone.setModel(new ListModelList());
        }
    }

    public void onClick$btnOKActive() {
        try {
            if (lbxTaskWait.getSelectedCount() == 1) {
                Map<String, Object> argu = new HashMap();
                argu.put(ParamUtils.ACTION, ParamUtils.ACTION_UPDATE);
                tasksDTO = lbxTaskWait.getItemAtIndex(lbxTaskWait.getSelectedItem().getIndex()).getValue();
                tasksDTO.setStatus(ParamUtils.TASKS_STATUS.OK_ACTIVE);
                String message = this.tasksService.updateStatus(tasksDTO);
                if (ParamUtils.SUCCESS.equals(message)) {
                    Clients.showNotification(LanguageBundleUtils.getString("global.message.create.successful"), "info", null, "middle_center", 1000);
                    this.bindData();
                    //self.detach();
                    messageService.insertMessage(getMessageEntity(contractService.getContractEntity(tasksDTO.getContactId()).getDeployUserId(),tasksDTO.getTaskUserId(),LanguageBundleUtils.getString("task.message.Active"), LanguageBundleUtils.getString("task.message.noActive")));

                } else {
                    Clients.showNotification("Thất bại cập nhật", "warning", null, "middle_center", 3000);
                }
            } else {
                Clients.showNotification(LanguageBundleUtils.getString("global.action.confirm.is_select.edit"), "warning", null, "middle_center", 3000);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    public void onClick$btnNOActive() {
        try {
            if (lbxTaskActive.getSelectedCount() == 1 || lbxTaskWait.getSelectedCount() == 1) {
                Map<String, Object> argu = new HashMap();
                argu.put(ParamUtils.ACTION, ParamUtils.ACTION_UPDATE);
                if (lbxTaskActive.getSelectedCount() == 1)
                    tasksDTO = lbxTaskActive.getItemAtIndex(lbxTaskActive.getSelectedItem().getIndex()).getValue();
                else
                    tasksDTO = lbxTaskWait.getItemAtIndex(lbxTaskWait.getSelectedItem().getIndex()).getValue();
                tasksDTO.setStatus(ParamUtils.TASKS_STATUS.NOT_ACTIVE);
                String message = this.tasksService.updateStatus(tasksDTO);
                if (ParamUtils.SUCCESS.equals(message)) {
                    Clients.showNotification(LanguageBundleUtils.getString("global.message.create.successful"), "info", null, "middle_center", 1000);
                    self.detach();
                } else {
                    Clients.showNotification("Thất bại cập nhật", "warning", null, "middle_center", 3000);
                }
                messageService.insertMessage(getMessageEntity(contractService.getContractEntity(tasksDTO.getContactId()).getDeployUserId(),tasksDTO.getTaskUserId(),LanguageBundleUtils.getString("task.message.noActive"), LanguageBundleUtils.getString("task.message.noActive")));

            } else {
                Clients.showNotification(LanguageBundleUtils.getString("global.action.confirm.is_select.edit"), "warning", null, "middle_center", 3000);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    public void onClick$btnAcceptDoneTask() {
        try {
            if (lbxTaskActive.getSelectedCount() == 1) {
                Map<String, Object> argu = new HashMap();
                argu.put(ParamUtils.ACTION, ParamUtils.ACTION_UPDATE);
                List<Integer> indexs = new ArrayList();
                indexs.add(lbxTaskActive.getSelectedItem().getIndex());
                argu.put("LIST_ACTION_INDEX", indexs);
                argu.put("TASK_DETAIL", lbxTaskActive);
                argu.put(ParamUtils.COMPOSER, this);
                Window wd = (Window) Executions.createComponents("/controls/workManager/doneStaffManager.zul", null, argu);
                wd.doModal();
            } else {
                Clients.showNotification(LanguageBundleUtils.getString("global.action.confirm.is_select.edit"), "warning", null, "middle_center", 3000);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    public String formatDateTime(Date date) throws Exception {
        return StringUtils.formatDateTime(date);
    }

    public MessageEntity getMessageEntity(Long sentUserId ,long receiveUserId, String content, String description){
        MessageEntity messageEntity=new MessageEntity();
        messageEntity.setSendUserId(sentUserId);
        messageEntity.setReceiveUserId(receiveUserId);
        messageEntity.setStatus(0);
        messageEntity.setContent(content);
        messageEntity.setDescription(description);
        messageEntity.setCreateBy(sentUserId);
        messageEntity.setCreatAt(new Date());
        return messageEntity;
    }
}
