package com.viettel.csp.composer.workManager;

import com.viettel.csp.DTO.DepartmentDTO;
import com.viettel.csp.DTO.UserDTO;
import com.viettel.csp.entity.TaskTypeEntity;
import com.viettel.csp.entity.TasksEntity;
import com.viettel.csp.service.DepartmentService;
import com.viettel.csp.service.TaskTypeService;
import com.viettel.csp.service.TasksService;
import com.viettel.csp.service.UserService;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.util.UserTokenUtils;
import com.viettel.eafs.util.SpringUtil;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.*;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class HandoverTaskComposer extends GenericForwardComposer<Component> {

    final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(HandoverTaskComposer.class);
    private TaskTypeService taskTypeService = SpringUtil.getBean("taskTypeService", TaskTypeService.class);
    private TasksService tasksService = SpringUtil.getBean("tasksService", TasksService.class);
    DepartmentService departmentService = SpringUtil.getBean("departmentService", DepartmentService.class);
    UserService userService = SpringUtil.getBean("userService", UserService.class);
    private List<TaskTypeEntity> taskTypeEntities;
    private List<TaskTypeEntity> taskTypeEntitiesFist;
    @Wire
    private Listbox lbxDepartment;
    @Wire
    private Listbox lbxStaff;
    private Long contractId;
    private DetailWorkComposer detailWorkComposer;

    private Combobox cbbTaskTypes;
    private Combobox cbbTaskTypesFist;
    private Datebox dbCompleteDate;
    private Textbox txtContent;
    private UserDTO userDTO;
    private Textbox txtUserName;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        List<DepartmentDTO> departmentDTOS = this.departmentService.getAllsWithOrgnization(this.userService.getUserByID(UserTokenUtils.getUserId()).getOrganizationId());
        if (departmentDTOS != null && !departmentDTOS.isEmpty()) {
            ListModelList model = new ListModelList(departmentDTOS);
            model.setMultiple(false);
            lbxDepartment.setModel(model);
        } else
            lbxDepartment.setModel(new ListModelList());
        if (arg.containsKey("CONTRACT_ID")) {
            this.contractId = (Long) arg.get("CONTRACT_ID");
        }
        if (arg.containsKey("COMPOSER")) {
            this.detailWorkComposer = (DetailWorkComposer) arg.get("COMPOSER");
        }
        txtUserName.setReadonly(true);
        this.taskTypeEntities = this.taskTypeService.findAllWithoutInTask(this.contractId);
        if (this.taskTypeEntities != null && !this.taskTypeEntities.isEmpty()) {
            ListModelList model = new ListModelList(this.taskTypeEntities);
            model.setMultiple(false);
            cbbTaskTypes.setModel(model);
        } else
            cbbTaskTypes.setModel(new ListModelList());
        this.taskTypeEntitiesFist = this.taskTypeService.findAlles();
        if (this.taskTypeEntitiesFist != null && !this.taskTypeEntitiesFist.isEmpty()) {
            ListModelList model = new ListModelList(this.taskTypeEntitiesFist);
            model.setMultiple(false);
            cbbTaskTypesFist.setModel(model);
        } else
            cbbTaskTypesFist.setModel(new ListModelList());
    }

    public void setListStaff() {
        if (lbxDepartment.getSelectedCount() == 1) {
            DepartmentDTO departmentDTO = lbxDepartment.getItemAtIndex(lbxDepartment.getSelectedItem().getIndex()).getValue();
            List<UserDTO> userDTOS = userService.findUserByOrgzanition(departmentDTO.getOrganizationId());
            if (userDTOS != null && !userDTOS.isEmpty()) {
                ListModelList model = new ListModelList(userDTOS);
                model.setMultiple(false);
                lbxStaff.setModel(model);
            } else
                lbxStaff.setModel(new ListModelList());
        }
    }

    public void setUserNameStaff() {
        if (lbxStaff.getSelectedCount() == 1) {
            userDTO = lbxStaff.getItemAtIndex(lbxStaff.getSelectedItem().getIndex()).getValue();
            txtUserName.setValue(userDTO.getUserName());
        } else {
            userDTO = new UserDTO();
            txtUserName.setValue("");
        }
    }

    public void onClick$btnSaveTask() throws Exception {
        try {
            Date time = Calendar.getInstance().getTime();
            TasksEntity tasksEntity = new TasksEntity();
            tasksEntity.setContactId(contractId);
            tasksEntity.setCompleteDate(null);
            tasksEntity.setStatus(ParamUtils.TASKS_STATUS.WAIT_ACTIVE);
            tasksEntity.setAssignAt(time);
            tasksEntity.setAssignBy(userDTO.getId());
            tasksEntity.setCreateAt(time);
            tasksEntity.setCreateBy(UserTokenUtils.getUserId());
            tasksEntity.setContent(txtContent.getValue().trim());
            tasksEntity.setTaskType(cbbTaskTypes.getSelectedItem() == null ? null : cbbTaskTypes.getSelectedItem().getValue());
            tasksEntity.setRequiredCompleteDate(dbCompleteDate.getValue());
            tasksEntity.setBeforeTask(cbbTaskTypesFist.getSelectedItem() == null ? null : cbbTaskTypesFist.getSelectedItem().getValue());
            try {
                tasksService.insert(tasksEntity);
                Clients.showNotification(LanguageBundleUtils.getString("global.message.create.successful"), "info", null, "middle_center", 1000);
                this.detailWorkComposer.binDataTasks();
                self.detach();
            } catch (Exception e) {
                Clients.showNotification("Lưu dữ liệu lỗi", "warning", null, "middle_center", 3000);
                throw e;
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }

    }
}
