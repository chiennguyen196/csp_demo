/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.composer.reflect;

import com.viettel.csp.DTO.ReflectDTO;
import com.viettel.csp.service.ReflectService;
import com.viettel.csp.util.KeyValueBean;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.util.StringUtils;
import com.viettel.csp.util.UserTokenUtils;
import com.viettel.eafs.util.SpringUtil;
import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static jxl.biff.BaseCellFeatures.logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Button;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Grid;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author GEM
 */
public class ReflectComposer extends GenericForwardComposer<Component> {

    private Combobox cbbReflectType;
    private ReflectService reflectService = SpringUtil.getBean("reflectService", ReflectService.class);
    private ListModelList<KeyValueBean> listReflectType = new ListModelList<KeyValueBean>();
    private List<ReflectDTO> lstResult;
    private Grid gridSearch;
    private Listbox lbxReflectSearch;
    private Textbox txtCode, txtCustomer, txtNameService, txtNumberhead;
    private Datebox dateReply;
    private Button btnAdd;
    private Button btnDelete;
    private Button btnSendReflect;
    private Button btnImport;
    private Button btnReset;

//    private Listbox lbxReflect;

    public Combobox getCbbReflectType() {
        return cbbReflectType;
    }

    public void setCbbReflectType(Combobox cbbReflectType) {
        this.cbbReflectType = cbbReflectType;
    }

    public ReflectService getReflectService() {
        return reflectService;
    }

    public void setReflectService(ReflectService reflectService) {
        this.reflectService = reflectService;
    }

    public List<ReflectDTO> getLstResult() {
        return lstResult;
    }

    public void setLstResult(List<ReflectDTO> lstResult) {
        this.lstResult = lstResult;
    }

    public Listbox getLbxReflectSearch() {
        return lbxReflectSearch;
    }

    public void setLbxReflectSearch(Listbox lbxReflectSearch) {
        this.lbxReflectSearch = lbxReflectSearch;
    }

    public ListModelList<KeyValueBean> getListReflectType() {
        return listReflectType;
    }

    public void setListReflectType(ListModelList<KeyValueBean> listReflectType) {
        this.listReflectType = listReflectType;
    }

    @Override
    public void doAfterCompose(Component comp) {
        try {
            super.doAfterCompose(comp);
            loadComboboxTypeReflect();
            bindDataToGrid();
            if (UserTokenUtils.getPersonType().equalsIgnoreCase(ParamUtils.USER_TYPE.CONTACT)
                    || UserTokenUtils.getPersonType().equalsIgnoreCase(ParamUtils.USER_TYPE.PARTNER)) {
                btnAdd.setVisible(false);
                btnDelete.setVisible(false);
                btnSendReflect.setVisible(false);
                btnImport.setVisible(false);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    public ReflectDTO getDataFromClient() throws Exception {
        ReflectDTO object = new ReflectDTO();

        if (StringUtils.isValidString(txtCode.getValue())) {
            object.setCode(txtCode.getValue().trim());
        }

        if (StringUtils.isValidString(txtCustomer.getValue())) {
            object.setCustomerName(txtCustomer.getValue().trim());
        }

        if (cbbReflectType.getSelectedIndex() > 0) {
            String reflectType = cbbReflectType.getSelectedItem().getValue().toString();
            object.setReflectType(reflectType);
        }

        if (StringUtils.isValidString(txtNameService.getValue())) {
            object.setServiceName(txtNameService.getValue().trim());
        }

        if (StringUtils.isValidString(txtNumberhead.getValue())) {
            object.setHeadOfService(txtNumberhead.getValue().trim());
        }

        if (StringUtils.isValidString(dateReply.getValue())) {

            object.setRequiredReponseDate(dateReply.getValue());
        }
        return object;
    }

    public void onClick$btnSearch() {
        bindDataToGrid();
    }

    public void onClick$btnReset(){
        bindDataToGrid();
    }

    public void onClick$btnSendReflect(){
        try {
            if (lbxReflectSearch.getSelectedCount() > 0) {
                Messagebox.show(LanguageBundleUtils.getString("global.action.confirm.multi.sendReflect"), "Confirm Dialog", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION, new org.zkoss.zk.ui.event.EventListener() {
                    public void onEvent(Event evt) throws InterruptedException, Exception {
                        if ("onOK".equals(evt.getName())) {

                            int max=lbxReflectSearch.getSelectedItems().size();
                            int cout=0;

                            for (Listitem item : lbxReflectSearch.getSelectedItems()) {
                                ReflectDTO reflectDTO = ((ReflectDTO) item.getValue());

                                if (reflectDTO.getReflectStatusCode().equals(ParamUtils.REFLECT_STATUS.NEW)) {

                                    reflectDTO.setReflectStatusCode(ParamUtils.REFLECT_STATUS.SEND);
                                    reflectService.sendPartner(reflectDTO);

                                    cout++;

                                }

                            }

                            bindDataToGrid();

                            Clients.showNotification("Đã gửi thành công " + cout+ "trên"+ max+ "bản ghi", "info", null, "middle_center", 1000);
                            return;
                        }
                    }
                });
            } else {
                Clients.showNotification(LanguageBundleUtils.getString("global.action.confirm.is_select"), "warning", null, "middle_center", 3000);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    public void onClick$btnAdd() {
        Map<String, Object> argu = new HashMap();
        argu.put(ParamUtils.ACTION, ParamUtils.ACTION_CREATE);
        argu.put("REFLECT_ID", "");
        argu.put(ParamUtils.COMPOSER, this);
        Window wd;//reflectPopup
        wd = (Window) Executions.createComponents("/controls/reflect/reflectPopup.zul", null, argu);
        wd.doModal();
    }

    public void onClick$btnImport(){

        Map<String, Object> argu = new HashMap();
        argu.put(ParamUtils.ACTION, ParamUtils.ACTION_CREATE);
        argu.put(ParamUtils.COMPOSER, this);

        Window wd;
        wd= (Window) Executions.createComponents("/controls/reflect/reflectImportPAKN.zul", null, argu);

        wd.doModal();

    }

    public void loadComboboxTypeReflect() throws Exception {
        List<KeyValueBean> lstDataType = new ArrayList<KeyValueBean>();
        lstDataType.addAll(reflectService.getListReflectType());
        listReflectType = new ListModelList<KeyValueBean>();
        listReflectType.addAll(lstDataType);
        if (listReflectType != null && listReflectType.size() > 0) {
            listReflectType.addToSelection(listReflectType.getElementAt(0));
        }
        cbbReflectType.setModel(listReflectType);
        cbbReflectType.setValue(LanguageBundleUtils.getString("global.combobox.choose"));
    }

    public void onClick$btnEdit() {
        try {
            if (lbxReflectSearch.getSelectedCount() == 1) {
                Map<String, Object> argu = new HashMap();
                argu.put(ParamUtils.ACTION, ParamUtils.ACTION_UPDATE);
                List<Integer> indexs = new ArrayList();
                indexs.add(lbxReflectSearch.getSelectedItem().getIndex());
                ReflectDTO reflectDTO = (ReflectDTO) lbxReflectSearch.getItemAtIndex(lbxReflectSearch.getSelectedItem().getIndex()).getValue();
                argu.put("REFLECT_ID", reflectDTO.getId());
                argu.put("LIST_ACTION_INDEX", indexs);
                argu.put("LISTBOX", lbxReflectSearch);
                argu.put(ParamUtils.COMPOSER, this);
                Window wd;
                wd = (Window) Executions.createComponents("/controls/reflect/reflectPopup.zul", null, argu);
                wd.doModal();
            } else {
                Clients.showNotification(LanguageBundleUtils.getString("global.action.confirm.is_select.edit"), "warning", null, "middle_center", 3000);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    public void onClick$btnDelete() {
        try {
            if (lbxReflectSearch.getSelectedCount() > 0) {
                Messagebox.show(LanguageBundleUtils.getString("global.action.confirm.multi.delete"), "Confirm Dialog", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION, new org.zkoss.zk.ui.event.EventListener() {
                    public void onEvent(Event evt) throws InterruptedException, Exception {
                        if ("onOK".equals(evt.getName())) {
                            List<ReflectDTO> lstDeleteDTO = new ArrayList<>();
                            for (Listitem item : lbxReflectSearch.getSelectedItems()) {
                                ReflectDTO reflectDTO = ((ReflectDTO) item.getValue());
                                lstDeleteDTO.add(reflectDTO);
                            }
                            String message = reflectService.deleteReflect(lstDeleteDTO);
                            bindDataToGrid();
                            Clients.showNotification(message, "info", null, "middle_center", 3000);
                            return;
                        }
                    }
                });
            } else {
                Clients.showNotification(LanguageBundleUtils.getString("global.action.confirm.is_select"), "warning", null, "middle_center", 3000);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }



    public void bindDataToGrid( ) {
        try {
            ReflectDTO reflectDTO = getDataFromClient();
            lstResult = reflectService.getListReflect(reflectDTO);

            if (lstResult != null && !lstResult.isEmpty()) {
                for (int i = 0; i < lstResult.size(); i++) {
                    if ("VERY_IMPORTANT".equals(lstResult.get(i).getPriorityLevel())) {
                        lstResult.get(i).setPriorityLevel(LanguageBundleUtils.getString("reflect.VERY_IMPORTANT"));
                    } else if ("IMPORTANT".equals(lstResult.get(i).getPriorityLevel())) {
                        lstResult.get(i).setPriorityLevel(LanguageBundleUtils.getString("reflect.IMPORTANT"));
                    } else if ("NORMAL".equals(lstResult.get(i).getPriorityLevel())) {
                        lstResult.get(i).setPriorityLevel(LanguageBundleUtils.getString("reflect.NORMAL"));
                    }
                }
//                if (reflectDTO.getRequiredReponseDate() != null) {
//                    for (int i = 0; i < lstResult.size(); i++) {
//                        ReflectDTO reflect = lstResult.get(i);
//                        if (reflect.getRequiredReponseDate() == null || reflectDTO.getRequiredReponseDate().compareTo(reflect.getRequiredReponseDate()) != 0) {
//                            lstResult.remove(reflect);
//                        }
//                    }
//                }

                ListModelList model = new ListModelList(lstResult);
                model.setMultiple(true);
                lbxReflectSearch.setModel(model);
                if (gridSearch != null) {
                    gridSearch.invalidate();
                }
            } else {
                lbxReflectSearch.setModel(new ListModelList());
            }
        } catch (Throwable ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }
}
