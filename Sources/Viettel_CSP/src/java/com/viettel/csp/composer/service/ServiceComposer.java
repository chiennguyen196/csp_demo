package com.viettel.csp.composer.service;

import com.viettel.csp.DTO.ContractFilesDTO;
import com.viettel.csp.DTO.ReflectDTO;
import com.viettel.csp.DTO.ServiceDTO;
import com.viettel.csp.service.ServiceService;
import com.viettel.csp.util.KeyValueBean;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.util.StringUtils;
import com.viettel.eafs.util.SpringUtil;
import com.viettel.plc.widget.IFileDisplay;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.*;

import java.io.File;
import java.util.*;

import static jxl.biff.BaseCellFeatures.logger;

/**
 * Created by LINHDT on 06/12/2017.
 */

public class ServiceComposer extends GenericForwardComposer<Component> {
    private ServiceService serviceService = SpringUtil.getBean("serviceService", ServiceService.class);
    final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ServiceComposer.class);

    private Textbox txtName, txtCode, txtParentCode;
    //    private Intbox txtCode;
    private Combobox cbbIdParentCode;
    private Button btnNew, btnSearch, btnEdit, btnDelete, btnReset,btnDownloadFile ;
    private List<ServiceDTO> lstResult;
    private Listbox lbxAttribute;

    private ListModelList<ServiceDTO> listService;

    private ListModelList<KeyValueBean> listParentCode = new ListModelList<KeyValueBean>();


    private Grid gridSearch;

    public ListModelList<KeyValueBean> getListParentCode() {
        return listParentCode;
    }

    public void setListParentCode(ListModelList<KeyValueBean> listParentCode) {
        this.listParentCode = listParentCode;
    }

    public Listbox getLbxAttribute() {
        return lbxAttribute;
    }

    public List<ServiceDTO> getLstResult() {
        return lstResult;
    }

    public void setLstResult(List<ServiceDTO> lstResult) {
        this.lstResult = lstResult;
    }

    public void setLbxAttribute(Listbox lbxAttribute) {
        this.lbxAttribute = lbxAttribute;
    }

    public Grid getGridSearch() {
        return gridSearch;
    }

    public void setGridSearch(Grid gridSearch) {
        this.gridSearch = gridSearch;
    }

    public ListModelList<ServiceDTO> getListService() {
        return (ListModelList<ServiceDTO>) serviceService.getList();
    }

    public void setListService(ListModelList<ServiceDTO> listService) {
        this.listService = listService;
    }

    public ServiceService getServiceService() {
        return serviceService;
    }

    public void setServiceService(ServiceService serviceService) {
        this.serviceService = serviceService;
    }


    @Override
    public void doAfterCompose(Component comp) throws Exception {
        try {
            super.doAfterCompose(comp);
            bindDataToGrid();
            loadComboboxParentCode(cbbIdParentCode);
        } catch (Throwable ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    public Textbox getTxtName() {
        return txtName;
    }

    public void setTxtName(Textbox txtName) {
        this.txtName = txtName;
    }

    public Textbox getTxtCode() {
        return txtCode;
    }

    public void setTxtCode(Textbox txtCode) {
        this.txtCode = txtCode;
    }

    public Textbox getTxtParentCode() {
        return txtParentCode;
    }

    public void setTxtParentCode(Textbox txtParentCode) {
        this.txtParentCode = txtParentCode;
    }

    public Button getBtnNew() {
        return btnNew;
    }

    public void setBtnNew(Button btnNew) {
        this.btnNew = btnNew;
    }

    public Button getBtnSearch() {
        return btnSearch;
    }

    public void setBtnSearch(Button btnSearch) {
        this.btnSearch = btnSearch;
    }

    public Button getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(Button btnEdit) {
        this.btnEdit = btnEdit;
    }

    public Button getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(Button btnDelete) {
        this.btnDelete = btnDelete;
    }

    public Button getBtnReset() {
        return btnReset;
    }

    public void setBtnReset(Button btnReset) {
        this.btnReset = btnReset;
    }

    public void loadComboboxParentCode(Combobox id) throws Exception {
        List<KeyValueBean> lstDataParentCode = new ArrayList<KeyValueBean>();
        lstDataParentCode.addAll(serviceService.listParrentCode());
        listParentCode = new ListModelList<KeyValueBean>();
        listParentCode.addAll(lstDataParentCode);
        if (listParentCode != null && listParentCode.size() > 0) {
            listParentCode.addToSelection(listParentCode.getElementAt(0));
        }
        id.setModel(listParentCode);
        id.setValue(LanguageBundleUtils.getString("global.combobox.choose"));
    }

    public String formatDateTime(Date date) throws Exception {
        return StringUtils.formatDateTime(date);
    }

    public ServiceDTO getDataFromClient() {
        ServiceDTO dto = new ServiceDTO();

        if (StringUtils.isValidString(txtCode.getValue().toString())) {
            dto.setCode(txtCode.getValue().toString());
        }

        if (StringUtils.isValidString(txtName.getValue())) {
            dto.setName(txtName.getValue().trim());
        }
        if (cbbIdParentCode.getSelectedIndex() > 0) {
            dto.setParentCode(cbbIdParentCode.getSelectedItem().getValue());
        } else {
            dto.setParentCode(null);
        }
        return dto;
    }

    public void bindDataToGrid() {
        try {
            ServiceDTO serviceDTO = getDataFromClient();
            lstResult = serviceService.getListServiceByDTO(serviceDTO);
            if (lstResult != null && !lstResult.isEmpty()) {
                ListModelList model = new ListModelList(lstResult);
                model.setMultiple(true);
                lbxAttribute.setModel(model);
                if (gridSearch != null) {
                    gridSearch.invalidate();
                }
            } else {
                lbxAttribute.setModel(new ListModelList());
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    public void onClick$btnNew() {
        Map<String, Object> argu = new HashMap();
        argu.put(ParamUtils.ACTION, ParamUtils.ACTION_CREATE);
        argu.put("SERVICE_CODE", "");
        argu.put(ParamUtils.COMPOSER, this);
        Window wd;
        wd = (Window) Executions.createComponents("/controls/service/servicePopup.zul", null, argu);
        wd.doModal();
    }

    public void onClick$btnSearch() {
        bindDataToGrid();
    }

    public void onClick$btnDelete() {
        try {
            if (lbxAttribute.getSelectedCount() > 0) {
                Messagebox.show(LanguageBundleUtils.getString("global.action.confirm.multi.delete"), "Confirm Dialog", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION, new org.zkoss.zk.ui.event.EventListener() {
                    public void onEvent(Event evt) throws InterruptedException, Exception {
                        if ("onOK".equals(evt.getName())) {
                            List<ServiceDTO> serviceDTOS = new ArrayList<>();
                            for (Listitem item : lbxAttribute.getSelectedItems()) {
                                ServiceDTO serviceDTO = ((ServiceDTO) item.getValue());
                                serviceDTOS.add(serviceDTO);
                                if (serviceDTO.getParentCode() == null) {
                                    List<ServiceDTO> listChilService = serviceService.getListChilService(serviceDTO.getCode());
                                    String deleteChilService = serviceService.deleteService(listChilService);
                                }
                            }
                            String message = serviceService.deleteService(serviceDTOS);

                            bindDataToGrid();
                            Clients.showNotification(message, "info", null, "middle_center", 3000);
                            return;
                        }
                    }
                });
            } else {
                Clients.showNotification(LanguageBundleUtils.getString("global.action.confirm.is_select"), "warning", null, "middle_center", 3000);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    public void onClick$btnReset() {

    }

    public void onClick$btnDownloadFile() {
        try {
            if (lbxAttribute.getSelectedCount() == 1) {
                ServiceDTO serviceDTO = (ServiceDTO) lbxAttribute.getItemAtIndex(lbxAttribute.getSelectedItem().getIndex()).getValue();
                System.out.println(serviceDTO.getPathFile());
                btnDownloadFile(serviceDTO);
            } else {
                Clients.showNotification(LanguageBundleUtils.getString("global.action.confirm.is_select.edit"), "warning", null, "middle_center", 3000);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    public void onClick$btnEdit() {
        try {
            if (lbxAttribute.getSelectedCount() == 1) {
                Map<String, Object> argu = new HashMap();
                argu.put(ParamUtils.ACTION, ParamUtils.ACTION_UPDATE);
                List<Integer> indexs = new ArrayList();
                indexs.add(lbxAttribute.getSelectedItem().getIndex());
                ServiceDTO serviceDTO = (ServiceDTO) lbxAttribute.getItemAtIndex(lbxAttribute.getSelectedItem().getIndex()).getValue();
                argu.put("SERVICE_CODE", serviceDTO.getCode());
                argu.put("LIST_ACTION_INDEX", indexs);
                argu.put("LISTBOX", lbxAttribute);
                argu.put(ParamUtils.COMPOSER, this);

                Window wd;//servicePopup
                wd = (Window) Executions.createComponents("/controls/service/servicePopup.zul", null, argu);
                wd.doModal();
            } else {
                Clients.showNotification(LanguageBundleUtils.getString("global.action.confirm.is_select.edit"), "warning", null, "middle_center", 3000);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    public void btnDownloadFile(ServiceDTO serviceDTO) {
        try {
            File file = new File(serviceDTO.getPathFile());
            Filedownload.save(file, null);
        } catch (Exception ex) {
            Messagebox.show(LanguageBundleUtils.getMessage("messageUploadFileError"),
                    LanguageBundleUtils.getMessage("Messagebox.warning"), Messagebox.OK,
                    Messagebox.ERROR,
                    null);
        }
    }

}