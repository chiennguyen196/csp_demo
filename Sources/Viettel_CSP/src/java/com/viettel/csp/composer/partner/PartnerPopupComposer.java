/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.composer.partner;

import com.viettel.csp.DTO.*;
import com.viettel.csp.entity.PartnerBankEntity;
import com.viettel.csp.entity.PartnerEntity;
import com.viettel.csp.entity.UserEntity;
import com.viettel.csp.service.*;
import com.viettel.csp.util.ComposerUtils;
import com.viettel.csp.util.GenComponent;
import com.viettel.csp.util.KeyValueBean;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.util.StringUtils;
import com.viettel.eafs.util.SpringUtil;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static jxl.biff.BaseCellFeatures.logger;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.*;

/**
 * @author TienNV@gemvietnam.com
 */
public class PartnerPopupComposer extends GenericForwardComposer<Component> {

    private Long totalRow = 0L;
    private int FIX_ROW = 1;
    private String action = "";
    private Listbox lbxPartnerSearch;
    private Listbox lbxContact;
    private Listbox lbxContract;
    private Listbox lbxReflect;
    private Grid gridContact;
    private String provinceCodeOld = "";
    private List<Integer> indexs = new ArrayList();
    private PartnerDTO oldDTO = null;
    private List<Component> lstInputCbbBankCtrls = new ArrayList<>();
    private List<Component> lstInputTextBankCtrls = new ArrayList<>();
    private Rows rows;
    private Row lastRow;
    private Datebox dbBusinessLicenceDate;
    private Textbox txtUserName;
    private Textbox txtParnerStatus;
    private Textbox txtParnerCode;
    private Textbox txtParnerName;
    private Textbox txtParnerShortName;
    private Textbox txtAddressHeadOffice;
    private Textbox txtAddressTransaction;
    private Textbox txtphone;
    private Textbox txtRepresentLaw;
    private Textbox txtPositionLaw;
    private Textbox txtEmailTransaction;
    private Textbox txtTaxCode;
    private Textbox txtBusinessLicenceNo;
    private Textbox txtBusinessLicenceCount;
    private Combobox cboGroupPartner;
    private Combobox cboProvince;
    private Combobox cboTypePartner;
    private Button btnSavePopup;
    private ListModelList<KeyValueBean> listBankModel = new ListModelList<KeyValueBean>();
    private ListModelList<KeyValueBean> listLocationModel = new ListModelList<KeyValueBean>();
    private ListModelList<KeyValueBean> listPartnerTypeModel = new ListModelList<KeyValueBean>();
    private GenComponent genComponent = new GenComponent();
    private static final Logger log = Logger.getLogger(PartnerPopupComposer.class);
    private static final String ATTR_VALUE_PATTERN = "VALUE_PATTERN";
    private static final String ATTR_DISPLAY_NAME = "DISPLAY_NAME";
    private static final String ATTR_REQUIRED = "REQUIRED";
    private static String partnerStatus;

    @Autowired
    private PartnerComposer partnerComposer;
    private BankService bankService = SpringUtil.getBean("bankService", BankService.class);
    private LocationService locationService = SpringUtil.getBean("locationService", LocationService.class);
    private PartnerService partnerService = SpringUtil.getBean("partnerService", PartnerService.class);
    private PartnerTypeService partnerTypeService = SpringUtil.getBean("partnerTypeService", PartnerTypeService.class);
    private MessageService messageService = SpringUtil.getBean("messageService", MessageService.class);
    private PartnerBankService partnerBankService = SpringUtil.getBean("partnerBankService", PartnerBankService.class);
    private ContactService contactService = SpringUtil.getBean("contactService", ContactService.class);
    private ContractService contractService = SpringUtil.getBean("contractService", ContractService.class);
    private UserService userService = SpringUtil.getBean("userService", UserService.class);
    private ReflectService reflectService = SpringUtil.getBean("reflectService", ReflectService.class);

    public PartnerComposer getPartnerComposer() {
        if (partnerComposer == null) {
            partnerComposer = new PartnerComposer();
        }
        return partnerComposer;
    }

    public void setPartnerComposer(PartnerComposer partnerComposer) {
        this.partnerComposer = partnerComposer;
    }

    public ListModelList<KeyValueBean> getListBankModel() {
        return listBankModel;
    }

    public void setListBankModel(ListModelList<KeyValueBean> listBankModel) {
        this.listBankModel = listBankModel;
    }

    public ListModelList<KeyValueBean> getListLocationModel() {
        return listLocationModel;
    }

    public void setListLocationModel(ListModelList<KeyValueBean> listLocationModel) {
        this.listLocationModel = listLocationModel;
    }

    public ListModelList<KeyValueBean> getListPartnerTypeModel() {
        return listPartnerTypeModel;
    }

    public void setListPartnerTypeModel(ListModelList<KeyValueBean> listPartnerTypeModel) {
        this.listPartnerTypeModel = listPartnerTypeModel;
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp); //To change body of generated methods, choose Tools | Templates.
        if (arg.containsKey(ParamUtils.ACTION)) {
            action = arg.get(ParamUtils.ACTION).toString();
        }
        if (arg.containsKey("LISTBOX")) {
            lbxPartnerSearch = (Listbox) arg.get("LISTBOX");
        }
        if (arg.containsKey("COMPOSER")) {
            partnerComposer = (PartnerComposer) arg.get("COMPOSER");
        }
        if (arg.containsKey("LIST_ACTION_INDEX")) {
            indexs = (List<Integer>) arg.get("LIST_ACTION_INDEX");
        }

        if (ParamUtils.ACTION_UPDATE.equals(action)) {
            ((Window) self).setTitle(LanguageBundleUtils.getString("global.edit"));
            oldDTO = (PartnerDTO) lbxPartnerSearch.getItemAtIndex(indexs.get(0)).getValue();
            bindData(oldDTO);
            Sessions.getCurrent().setAttribute("partnerID", oldDTO.getId());
        } else {
            ((Window) self).setTitle(LanguageBundleUtils.getString("global.add"));
        }
        Sessions.getCurrent().setAttribute("upload","ok");
    }

    public void loadComboboxProvince() throws Exception {
        List<KeyValueBean> lstProvince = new ArrayList<KeyValueBean>();
        lstProvince = locationService.getListDataToCombobox("PROVINCE", null);
        listLocationModel = new ListModelList<KeyValueBean>();
        listLocationModel.addAll(lstProvince);
        cboProvince.setModel(listLocationModel);
    }

    public void loadComboboxTypePartner() throws Exception {
        List<KeyValueBean> lstPartnerType = new ArrayList<KeyValueBean>();
        lstPartnerType = partnerTypeService.getListToKeyValueBean(true);
        listPartnerTypeModel = new ListModelList<KeyValueBean>();
        listPartnerTypeModel.addAll(lstPartnerType);
        cboTypePartner.setModel(listPartnerTypeModel);
    }

    public List<PartnerBankDTO> getInfoBankByPartnerId(Long partnerId) throws Exception {
        List<PartnerBankDTO> lstPartnerBankDTO = new ArrayList<PartnerBankDTO>();
        lstPartnerBankDTO = partnerBankService.getInfoBankByPartnerId(partnerId);
        return lstPartnerBankDTO;
    }

    public void bindDataToCombo() throws Exception {
        loadComboboxTypePartner();
        loadComboboxProvince();
    }

    public void bindData(PartnerDTO dto) throws Exception {
        PartnerDTO searchObject = new PartnerDTO();
        searchObject.setId(dto.getId());
        PartnerDTO object = partnerService.findInfoById(dto);

        bindDataToCombo();
        bindDataToTab(object.getId());
        txtUserName.setValue(object.getUserName());
        txtParnerStatus.setValue(object.getPartnerStatusName());
        txtParnerCode.setValue(object.getPartnerCode());
        txtParnerName.setValue(object.getCompanyName());
        txtParnerShortName.setValue(object.getCompanyNameShort());
        txtAddressHeadOffice.setValue(object.getAddressHeadOffice());
        txtAddressTransaction.setValue(object.getAddressTradingOffice());
        txtphone.setValue(object.getPhone());
        txtRepresentLaw.setValue(object.getRepName());
        txtPositionLaw.setValue(object.getRepPosition());
        txtEmailTransaction.setValue(object.getEmail());
        txtTaxCode.setValue(object.getTaxCode());
        txtBusinessLicenceNo.setValue(object.getBusinessRegisNumber());
        dbBusinessLicenceDate.setValue(object.getBusinessRegisDate());
        txtBusinessLicenceCount.setValue(object.getBusinessRegisCount());
        if (object.getPartnerGroup() != null && object.getPartnerGroup().equalsIgnoreCase(ParamUtils.PARTNER_GROUP.INTERNAL)) {
            //cboGroupPartner.setValue(LanguageBundleUtils.getString("partner.domestic"));
            cboGroupPartner.setSelectedIndex(0);
            List<KeyValueBean> lstProvince = new ArrayList<KeyValueBean>();
            lstProvince = locationService.getListDataToCombobox("PROVINCE", null);
            int i = 0;
            for (KeyValueBean keyValueBean : lstProvince) {
                if (keyValueBean.getKey().equals(object.getPartnerGroupProvince())) {
                    //provinceCodeOld = keyValueBean.getKey().toString();
                    //cboProvince.setValue(keyValueBean.getValue());
                    listLocationModel.addToSelection(listLocationModel.get(i));
                    break;
                }
                i++;
            }
            cboProvince.setDisabled(false);
        } else {
            cboGroupPartner.setValue(LanguageBundleUtils.getString("partner.foreign"));
            cboProvince.setValue(LanguageBundleUtils.getString("global.combobox.choose"));
//            cboGroupPartner.setSelectedIndex(1);
//            cboProvince.setSelectedIndex(0);
            cboProvince.setDisabled(true);
        }

        if (StringUtils.isValidString(object.getPartnerTypeCode())) {
            List<KeyValueBean> lstPartnerType = new ArrayList<KeyValueBean>();
            lstPartnerType = partnerService.getListPartnerTypeByCode(object.getPartnerTypeCode());
            String partnerTypeName = lstPartnerType.get(0).getValue();
            cboTypePartner.setValue(partnerTypeName);
        }

        txtParnerCode.setDisabled(true);
        txtParnerStatus.setDisabled(true);
        txtUserName.setDisabled(true);
        removeComponentBank(rows.getChildren().indexOf(lastRow) - FIX_ROW);
        buildComponentToData(object.getId());
        if (ParamUtils.PARTNER_STATUS.REQUEST_VIETTEL_APPROVE.equalsIgnoreCase(dto.getPartnerStatusCode())) {
            partnerStatus = ParamUtils.PARTNER_STATUS.REQUEST_VIETTEL_APPROVE;
            Clients.showNotification(LanguageBundleUtils.getString("message.partner.save.of.profile"), "warning", null, "middle_center", 3000);
            btnSavePopup.setLabel(LanguageBundleUtils.getString("btn.partner.save.of.profile"));
        }
    }

    public List<PartnerBankEntity> convertToListBankEntity(List<PartnerBankDTO> lstPartnerBankDTO) {
        PartnerBankEntity partnerBankEntity = null;
        List<PartnerBankEntity> lstPartnerBankEntity = new ArrayList<PartnerBankEntity>();

        for (int i = 0; i < lstPartnerBankDTO.size(); i++) {
            partnerBankEntity = PartnerBankEntity.setEntity(lstPartnerBankDTO.get(i), partnerBankEntity);
            lstPartnerBankEntity.add(partnerBankEntity);
        }

        return lstPartnerBankEntity;
    }

    public void onClick$bntLockOpenPartner() {
        try {
            Map<String, Object> argu = new HashMap();
            argu.put(ParamUtils.ACTION, ParamUtils.ACTION_PARTNER_LOCK);
            argu.put(ParamUtils.COMPOSER, this);
            argu.put("PARNER_OBJECT", oldDTO);

            Window wd;//partnerPopup
            wd = (Window) Executions.createComponents("/controls/partner/partnerLockPopup.zul", null, argu);
            wd.doModal();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    public void onClick$btnAddBankPopup() {
        try {
            List<BankDTO> lstBankDTO = bankService.findAll();
            buildComponentBank(null, lstBankDTO);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    public void onClick$btnSavePopup() throws Exception {
        try {
            if (indexs.size() > 0) {
                if (validate()) {
                    PartnerDTO newPartnerDTO = getDataFromClient();
                    List<PartnerDTO> lst = (List<PartnerDTO>) ((ListModelList) lbxPartnerSearch.getModel()).getInnerList();
                    PartnerDTO oldPartnerDTO = (PartnerDTO) lbxPartnerSearch.getItemAtIndex(indexs.get(0)).getValue();
                    if (newPartnerDTO.getPartnerCode().equals(oldPartnerDTO.getPartnerCode())) {
                        partnerService.update(newPartnerDTO);
                        partnerBankService.deletePartnerBank(newPartnerDTO.getId());
                        partnerBankService.insert(newPartnerDTO.getLstPartnerBank());
                        messageService.insert();
                        partnerComposer.bindDataToGrid();
                        Clients.showNotification(LanguageBundleUtils.getMessage("global.message.update.successful", LanguageBundleUtils.getString("global.edit")), "info", null, "middle_center", 3000);
                        self.detach();
                    }
                }
            } else {
                Clients.showNotification(LanguageBundleUtils.getString("global.choose.action"), "warning", null, "middle_center", 3000);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }


    public void onClick$bntBreachPartner() throws Exception {
        try {

            Map<String, Object> argu = new HashMap();
            argu.put(ParamUtils.ACTION, ParamUtils.ACTION_PARTNER_HANDLE_VIOLATION);
            argu.put(ParamUtils.COMPOSER, this);
            argu.put("PARNER_OBJECT", oldDTO);
            Window wd;
            wd = (Window) Executions.createComponents("/controls/partner/partnerViolationPopup.zul", null, argu);
            wd.doModal();

        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }


    public PartnerDTO getDataFromClient() throws Exception {
        PartnerDTO object = new PartnerDTO();
        Long partnerId = ((PartnerDTO) lbxPartnerSearch.getItemAtIndex(indexs.get(0)).getValue()).getId();
        object.setId(partnerId);

        object.setPartnerCode(txtParnerCode.getValue().trim());
        object.setCompanyName(txtParnerName.getValue().trim());
        object.setCompanyNameShort(txtParnerShortName.getValue().trim());
        object.setAddressOffice(txtAddressHeadOffice.getValue().trim());
        object.setAddressTradingOffice(txtAddressTransaction.getValue().trim());
        object.setPhone(txtphone.getValue().trim());
        object.setRepName(txtRepresentLaw.getValue().trim());
        object.setRepPosition(txtPositionLaw.getValue().trim());
        object.setEmail(txtEmailTransaction.getValue().trim());
        object.setTaxCode(txtTaxCode.getValue().trim());
        object.setBusinessRegisNumber(txtBusinessLicenceNo.getValue().trim());
        object.setBusinessRegisDate(dbBusinessLicenceDate.getValue());
        object.setBusinessRegisCount(txtBusinessLicenceCount.getValue().trim());
        if (ParamUtils.PARTNER_STATUS.REQUEST_VIETTEL_APPROVE.equalsIgnoreCase(partnerStatus)
                || ParamUtils.PARTNER_STATUS.INACTIVE.equalsIgnoreCase(partnerStatus)) {
            object.setPartnerStatusCode(ParamUtils.PARTNER_STATUS.ACTIVE);
        }
        if (cboGroupPartner.getSelectedItem() != null) {
            String groupPartner = cboGroupPartner.getSelectedItem().getValue().toString();
            object.setPartnerGroup(groupPartner);
            if (groupPartner.equals(ParamUtils.PARTNER_GROUP.INTERNAL)) {
                object.setPartnerGroupProvince(cboProvince.getSelectedItem().getValue().toString());
            } else {
                object.setPartnerGroupProvince("");
            }
        }
        if (cboTypePartner.getSelectedItem() != null) {
            object.setPartnerTypeCode(cboTypePartner.getSelectedItem().getValue().toString());
        }
        List<PartnerBankDTO> lstPartnerBankDTO = new ArrayList<>();
        List<String> listInputBank = new ArrayList(genComponent.getDataFromControls(lstInputTextBankCtrls).values());
        List<String> listCbbBank = new ArrayList(genComponent.getDataFromControls(lstInputCbbBankCtrls).values());

        for (int i = 0, total = listInputBank.size(); i < total; i++) {
            PartnerBankDTO partnerBankDTO = new PartnerBankDTO();
            partnerBankDTO.setPartnerId(partnerId);
            partnerBankDTO.setAccountNumber(listInputBank.get(i));
            partnerBankDTO.setBankCode(listCbbBank.get(i));
            lstPartnerBankDTO.add(partnerBankDTO);
        }
        object.setLstPartnerBank(lstPartnerBankDTO);
        return object;
    }

    public void onClick$btnResetPopup() throws Exception {
        if (ParamUtils.ACTION_UPDATE.equals(action)) {
            oldDTO = (PartnerDTO) lbxPartnerSearch.getItemAtIndex(indexs.get(0)).getValue();
            bindData(oldDTO);
        }
    }

    public void onClick$btnRemoveBankPopup() throws Exception {
        try {
            removeComponentBank(1);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    public void onChangeGroupPartner(Event event) throws Exception {
        String value = cboGroupPartner.getSelectedItem().getValue();
        if (value.equals(ParamUtils.PARTNER_GROUP.INTERNAL)) {
            if (StringUtils.isValidString(provinceCodeOld)) {
                ComposerUtils.setFixComboValue(cboProvince, provinceCodeOld);
            } else {
                cboProvince.setValue(LanguageBundleUtils.getString("global.combobox.choose"));
            }
            cboProvince.setDisabled(false);
        } else {
            cboProvince.setValue(LanguageBundleUtils.getString("global.combobox.choose"));
            cboProvince.setDisabled(true);
        }
    }

    private boolean validate() {
        if (ParamUtils.ACTION_UPDATE.equals(action)) {
            if (!StringUtils.isValidString(txtParnerCode.getValue())) {
                Clients.wrongValue(txtParnerCode, LanguageBundleUtils.getMessage("global.validate.notnull", LanguageBundleUtils.getString("partner.code")));
                return false;
            }
        }

        if (!StringUtils.isValidString(txtParnerShortName.getValue())) {
            Clients.wrongValue(txtParnerShortName, LanguageBundleUtils.getMessage("global.validate.notnull", LanguageBundleUtils.getString("partner.shortName")));
            return false;
        }

        if (!StringUtils.isValidString(txtParnerName.getValue())) {
            Clients.wrongValue(txtParnerName, LanguageBundleUtils.getMessage("global.validate.notnull", LanguageBundleUtils.getString("partner.name")));
            return false;
        }

        if (!StringUtils.isValidString(txtAddressHeadOffice.getValue())) {
            Clients.wrongValue(txtAddressHeadOffice, LanguageBundleUtils.getMessage("global.validate.notnull", LanguageBundleUtils.getString("partner.address")));
            return false;
        }

        if (!StringUtils.isValidString(txtAddressTransaction.getValue())) {
            Clients.wrongValue(txtAddressTransaction, LanguageBundleUtils.getMessage("global.validate.notnull", LanguageBundleUtils.getString("partner.addressTransaction")));
            return false;
        }

        if (!StringUtils.isValidString(txtphone.getValue())) {
            Clients.wrongValue(txtphone, LanguageBundleUtils.getMessage("global.validate.notnull", LanguageBundleUtils.getString("partner.phone")));
            return false;
        }

        if (!StringUtils.isValidPhone(txtphone.getValue())) {
            Clients.wrongValue(txtphone, LanguageBundleUtils.getMessage("global.validate.notformat.phone", LanguageBundleUtils.getString("partner.phone")));
            return false;
        }

        if (!StringUtils.isValidString(txtRepresentLaw.getValue())) {
            Clients.wrongValue(txtRepresentLaw, LanguageBundleUtils.getMessage("global.validate.notnull", LanguageBundleUtils.getString("partner.representLaw")));
            return false;
        }

        if (!StringUtils.isValidString(txtPositionLaw.getValue())) {
            Clients.wrongValue(txtPositionLaw, LanguageBundleUtils.getMessage("global.validate.notnull", LanguageBundleUtils.getString("partner.positionLaw")));
            return false;
        }

        if (!StringUtils.isValidString(txtEmailTransaction.getValue())) {
            Clients.wrongValue(txtEmailTransaction, LanguageBundleUtils.getMessage("global.validate.notnull", LanguageBundleUtils.getString("partner.emailTransaction")));
            return false;
        }

        if (!StringUtils.isValidEmail(txtEmailTransaction.getValue())) {
            Clients.wrongValue(txtEmailTransaction, LanguageBundleUtils.getMessage("global.validate.notformat.email", LanguageBundleUtils.getString("partner.emailTransaction")));
            return false;
        }

        if (!StringUtils.isValidString(txtTaxCode.getValue())) {
            Clients.wrongValue(txtTaxCode, LanguageBundleUtils.getMessage("global.validate.notnull", LanguageBundleUtils.getString("partner.taxCode")));
            return false;
        }

        if (!StringUtils.isValidString(txtBusinessLicenceNo.getValue())) {
            Clients.wrongValue(txtBusinessLicenceNo, LanguageBundleUtils.getMessage("global.validate.notnull", LanguageBundleUtils.getString("partner.businessLicencenNo")));
            return false;
        }

        if (!StringUtils.isValidString(dbBusinessLicenceDate.getValue())) {
            Clients.wrongValue(dbBusinessLicenceDate, LanguageBundleUtils.getMessage("global.validate.notnull", LanguageBundleUtils.getString("partner.dbBusinessLicenceDate")));
            return false;
        }

        if (!StringUtils.isValidDate(dbBusinessLicenceDate.getValue())) {
            Clients.wrongValue(dbBusinessLicenceDate, LanguageBundleUtils.getMessage("global.validate.notformat.date.ddMMyyyy", LanguageBundleUtils.getString("partner.dbBusinessLicenceDate")));
            return false;
        }

        if (!StringUtils.isValidString(txtBusinessLicenceCount.getValue())) {
            Clients.wrongValue(txtBusinessLicenceCount, LanguageBundleUtils.getMessage("global.validate.notnull", LanguageBundleUtils.getString("partner.businessLicenceCount")));
            return false;
        }
        if (cboGroupPartner.getSelectedItem() != null && cboGroupPartner.getSelectedItem().getValue() != null && ParamUtils.SELECT_NOTHING_VALUE_STR.equals(cboGroupPartner.getSelectedItem().getValue())) {
            Clients.wrongValue(cboGroupPartner, LanguageBundleUtils.getMessage("global.validate.notchoose", LanguageBundleUtils.getString("partner.groupPartner")));
            return false;
        }

        if (cboProvince.getSelectedItem() != null && cboProvince.getSelectedItem().getValue() != null) {
            String groupPartner = cboGroupPartner.getSelectedItem().getValue().toString();
            if (groupPartner.equals(ParamUtils.PARTNER_GROUP.INTERNAL)) {
                if (cboProvince.getSelectedItem() != null && cboProvince.getSelectedItem().getValue() != null && ParamUtils.SELECT_NOTHING_VALUE_STR.equals(cboProvince.getSelectedItem().getValue())) {
                    Clients.wrongValue(cboProvince, LanguageBundleUtils.getMessage("global.validate.notchoose", LanguageBundleUtils.getString("partner.province")));
                    return false;
                }
            }
        }

        if (cboTypePartner.getSelectedItem().getValue() != null && ParamUtils.SELECT_NOTHING_VALUE_STR.equals(cboTypePartner.getSelectedItem().getValue())) {
            Clients.wrongValue(cboTypePartner, LanguageBundleUtils.getMessage("global.validate.notchoose", LanguageBundleUtils.getString("partner.type")));
            return false;
        }

        Map<Object, Object> mapInputBank = genComponent.getDataFromControls(lstInputTextBankCtrls);
        for (Map.Entry<Object, Object> entry : mapInputBank.entrySet()) {
            Object value = entry.getValue();
            Component component = (Component) entry.getKey();
            if (!StringUtils.isValidString(value)) {
                Clients.wrongValue(component, LanguageBundleUtils.getMessage("global.validate.notnull", LanguageBundleUtils.getString("partner.accountNumber")));
                return false;
            }
        }
        return true;
    }

    private void buildComponentToData(Long partnerId) {
        try {
            //Bank
            List<PartnerBankDTO> lstPartnerBankDTO = partnerBankService.getInfoBankByPartnerId(partnerId);
            List<BankDTO> lstBankDTO = bankService.findAll();
            for (PartnerBankDTO itemPartnerBankDTO : lstPartnerBankDTO) {
                buildComponentBank(itemPartnerBankDTO, lstBankDTO);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    private void removeComponentBank(int count) {
        int indexOf = rows.getChildren().indexOf(lastRow);
        if (indexOf > FIX_ROW) {
            for (int i = 1; i <= count; i++) {
                totalRow--;
                Component component = rows.getChildren().get(indexOf - i);
                lstInputTextBankCtrls.remove(component.getChildren().get(1));
                lstInputCbbBankCtrls.remove(component.getChildren().get(3));
                rows.removeChild(component);
            }
        }
    }

    private void bindDataToTab(Long partnerId) {
        try {
            ContactDTO object = new ContactDTO();
            object.setPartnerId(partnerId);
            List<ContactDTO> lstResult = contactService.getList(object);
            if (lstResult != null && !lstResult.isEmpty()) {
                ListModelList model = new ListModelList(lstResult);
                lbxContact.setModel(model);
            } else {
                lbxContact.setModel(new ListModelList());
            }

            ContractDTO contractDTO = new ContractDTO();
            contractDTO.setPartnerId(partnerId);
            List<ContractDTO> contractDTOs = contractService.getListContract(contractDTO);
            if (lstResult != null && !lstResult.isEmpty()) {
                ListModelList model = new ListModelList(contractDTOs);
                lbxContract.setModel(model);
            } else {
                lbxContract.setModel(new ListModelList());
            }

            ReflectDTO reflectDTO = new ReflectDTO();
            reflectDTO.setPartnerId(partnerId);
            List<ReflectDTO> reflectDTOs = reflectService.getListReflect(reflectDTO);
            if (lstResult != null && !lstResult.isEmpty()) {
                ListModelList model = new ListModelList(reflectDTOs);
                lbxReflect.setModel(model);
            } else {
                lbxReflect.setModel(new ListModelList());
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    public String getContactSex(String paramType) {
        try {
            switch (paramType) {
                case ParamUtils.GENDER_MALE_STR:
                    return LanguageBundleUtils.getString("global.sex.male");
                case ParamUtils.GENDER_FEMALE_STR:
                    return LanguageBundleUtils.getString("global.sex.female");
                default:
                    return "";
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return "";
    }

    public String getContactStatusName(String paramType) {
        try {
            switch (paramType) {
                case ParamUtils.ACTIVE_STR:
                    return LanguageBundleUtils.getString("contact.status.active");
                case ParamUtils.LOCK_STR:
                    return LanguageBundleUtils.getString("contact.status.lock");
                default:
                    return "";
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return "";
    }

    public String formatDateTime(Date date) throws Exception {
        return StringUtils.formatDateTime(date);
    }

    private void buildComponentBank(PartnerBankDTO itemPartnerBankDTO, List<BankDTO> lstBankDTO) throws Exception {
        Row tmpRow;
        Component txtBankNumber;
        Component cbbBank;
        Component txtBankBranch;
        Component firstComp = null;
        totalRow++;
        tmpRow = new Row();
        ComponentDTO cpBankNumberDTO = new ComponentDTO();
        cpBankNumberDTO.setAttributeType(ParamUtils.ReportDataType.STRING);
        cpBankNumberDTO.setAttributeLength("50");
        cpBankNumberDTO.setIsRequired(false);
        cpBankNumberDTO.setStandardAttributeId(String.valueOf("txtBankNumber" + totalRow));
        cpBankNumberDTO.setDefaultValue(itemPartnerBankDTO != null && itemPartnerBankDTO.getAccountNumber() != null ? itemPartnerBankDTO.getAccountNumber() : "");
        cpBankNumberDTO.setAttributeName(LanguageBundleUtils.getString("partner.accountNumber"));
        txtBankNumber = genComponent.createComponent(cpBankNumberDTO);
        genComponent.createCellLabel(cpBankNumberDTO.getAttributeName(), cpBankNumberDTO.getIsRequired(), tmpRow);
        if (txtBankNumber != null) {
            tmpRow.appendChild(txtBankNumber);
            lstInputTextBankCtrls.add(txtBankNumber);
        }

        ComponentDTO cpBanktDTO = new ComponentDTO();
        cpBanktDTO.setAttributeType(ParamUtils.ReportDataType.COMBOBOX);
        cpBanktDTO.setAttributeType(ParamUtils.ReportDataType.COMBOVALUE);
        cpBanktDTO.setIsRequired(false);
        cpBanktDTO.setStandardAttributeId(String.valueOf("cbbBank" + totalRow));
        cpBanktDTO.setSelectItemValue(itemPartnerBankDTO != null && itemPartnerBankDTO.getBankCode() != null ? itemPartnerBankDTO.getBankCode() : "");
        cpBanktDTO.setAttributeName(LanguageBundleUtils.getString("partner.bank"));
        StringBuilder parValue = new StringBuilder();
        for (BankDTO itemBankDTO : lstBankDTO) {
            parValue.append(itemBankDTO.getCode());
            parValue.append(";");
            parValue.append(itemBankDTO.getName());
            parValue.append(";");
        }
        cpBanktDTO.setInitValue(parValue.toString());
        cbbBank = genComponent.createComponent(cpBanktDTO);
        genComponent.createCellLabel(cpBanktDTO.getAttributeName(), cpBankNumberDTO.getIsRequired(), tmpRow);
        if (cbbBank != null) {
            tmpRow.appendChild(cbbBank);
            lstInputCbbBankCtrls.add(cbbBank);
        }

        rows.insertBefore(tmpRow, lastRow);
    }

    public void onClick$btnClose() {
        self.detach();
    }

    public String getUserById(Long userId) {
        try {
            UserEntity usrUser = userService.getUserByID(userId);
            return usrUser.getFullName();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return "";
    }

    private List<ContractDTO> contractDTOs;
    private ContractViolationService contractViolationService = SpringUtil.getBean("contractViolationService", ContractViolationService.class);
    private Listbox ViolationHistory;
    public void onSelect$tabViolationHistory(){
        contractDTOs = contractService.getListContractForPartnerLock(new ContractDTO());
        //contractDTOs = contractViolationService.fillContractListByStatus(contractDTOs, 26);

        List<ContractBreachDTO> contractBreachDTOs = new ArrayList<>();
        for (ContractDTO dto:contractDTOs){
            ContractBreachDTO contractBreachDTO = new ContractBreachDTO();
            contractBreachDTO = contractViolationService.getContractBreachByContractId(dto.getContractId());
            if (contractBreachDTO != null){
                contractBreachDTOs.add(contractBreachDTO);
            }
        }

        ListModelList model = new ListModelList(contractBreachDTOs);
        model.setMultiple(true);
        ViolationHistory.setModel(model);
    }

    public void onClick$showDetailViolation(){
        if(ViolationHistory.getSelectedItem() == null){
            Clients.showNotification(LanguageBundleUtils.getString("partner.violation.view"), "warn", null, "middle_center", 3000);
            return;
        }

        ContractBreachDTO dto = (ContractBreachDTO) ViolationHistory.getSelectedItem().getValue();
        Map<String, Object> argu = new HashMap();
        argu.put(ParamUtils.COMPOSER, this);
        argu.put("CONTRACT_BREACH", dto);
        argu.put("PARNER_OBJECT", oldDTO);
        Window wd;//partnerPopup
        wd = (Window) Executions.createComponents("/controls/partner/partnerViolationPopup.zul", null, argu);
        wd.doModal();
    }

    public Listbox getViolationHistory() {
        return ViolationHistory;
    }

    public void setViolationHistory(Listbox violationHistory) {
        ViolationHistory = violationHistory;
    }
}
