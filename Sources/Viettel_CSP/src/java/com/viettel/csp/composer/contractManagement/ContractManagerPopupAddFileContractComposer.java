/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.composer.contractManagement;

import com.viettel.csp.DTO.ContractDTO;
import com.viettel.csp.DTO.ContractFilesDTO;
import com.viettel.csp.util.CtrlValidator;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.util.StringUtils;
import com.viettel.plc.widget.FileInfo;
import com.viettel.plc.widget.IUploaderCallback;
import com.viettel.plc.widget.UploadComponent;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Button;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author chitv@gemvietnam.com
 */
public class ContractManagerPopupAddFileContractComposer extends GenericForwardComposer<Component> implements IUploaderCallback {

    private Textbox txtFileContractNumber, txtContent, txtDescription, txtFileName;
    private Datebox dbStartDate, dbEndDate;
    private String action;
    private ContractDTO oldDTO;
    final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ContractManagerPopupAddFileContractComposer.class);
    private ContractManagerCreatPopupContractComposer contractManagerCreatPopupContractComposer;
    private ContractFilesDTO contractFilesDTO;
    private UploadComponent uploadComponent;
    private List<String> filesLocation;
    private List<ContractFilesDTO> lstContractFiles = new ArrayList<>();
    private Listbox lbxViewAddContract;
    private ArrayList indexLst;
    private Button bntAddSendContractFile;
    private List<FileInfo> filesInfo;

    public ContractFilesDTO getContractFilesDTO() {
        return contractFilesDTO;
    }

    public void setContractFilesDTO(ContractFilesDTO contractFilesDTO) {
        this.contractFilesDTO = contractFilesDTO;
    }

    public List<String> getFilesLocation() {
        return filesLocation;
    }

    public void setFilesLocation(List<String> filesLocation) {
        this.filesLocation = filesLocation;
    }

    public List<ContractFilesDTO> getLstContractFiles() {
        return lstContractFiles;
    }

    public void setLstContractFiles(List<ContractFilesDTO> lstContractFiles) {
        this.lstContractFiles = lstContractFiles;
    }

    public Listbox getLbxViewAddContract() {
        return lbxViewAddContract;
    }

    public void setLbxViewAddContract(Listbox lbxViewAddContract) {
        this.lbxViewAddContract = lbxViewAddContract;
    }

    @Override
    public void doAfterCompose(Component comp) {
        try {
            super.doAfterCompose(comp);
            if (arg.containsKey(ParamUtils.COMPOSER)) {
                contractManagerCreatPopupContractComposer = (ContractManagerCreatPopupContractComposer) arg.get(ParamUtils.COMPOSER);
            }
            if (arg.containsKey(ParamUtils.ACTION)) {
                action = arg.get(ParamUtils.ACTION).toString();
            }
            if (arg.containsKey("LIST_ACTION_INDEX")) {
                indexLst = (ArrayList) arg.get("LIST_ACTION_INDEX");
            }
            if (arg.containsKey("LISTBOX")) {
                contractFilesDTO = (ContractFilesDTO) arg.get("LISTBOX");
            }
            uploadComponent.setUploaderCallback(this);
            if (indexLst != null) {
                setData(indexLst.get(0));
            } else {
                setData();
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    public void setData(Object indexLst) throws Exception {
        if (ParamUtils.ACTION_CREATE.equals(action)) {
            ((Window) self).setTitle(LanguageBundleUtils.getString("global.add"));
        } else if (ParamUtils.ACTION_UPDATE_FILE.equals(action)) {
            bntAddSendContractFile.setVisible(false);
            if (contractFilesDTO != null) {
                txtFileContractNumber.setValue(contractFilesDTO.getFileContractNumber());
                txtContent.setValue(contractFilesDTO.getContent());
                txtDescription.setValue(contractFilesDTO.getDescription());
                txtFileName.setValue(contractFilesDTO.getFileName());
                dbStartDate.setValue(contractFilesDTO.getStartDate());
                dbEndDate.setValue(contractFilesDTO.getEndDate());
            }
        }
    }

    public void setData() throws Exception {
        if (ParamUtils.ACTION_CREATE.equals(action)) {
            ((Window) self).setTitle(LanguageBundleUtils.getString("global.add"));
        }
    }

    public void onClick$bntAddSendContractFile() {
        try {
            uploadComponent.onClick$doSave();
            if (validate()) {
                contractFilesDTO = getDataFromClientContractFiles(oldDTO, filesInfo.get(0));
                lstContractFiles.add(contractFilesDTO);
//                contractManagerCreatPopupContractComposer.setDataGrid(contractFilesDTO);
                self.detach();
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    public void onClick$bntUpdateSendContractFile() {
        try {
            uploadComponent.onClick$doSave();
            if (validate()) {
                contractFilesDTO = getDataFromClientContractFiles(oldDTO, filesInfo.get(0));
                lstContractFiles.add(contractFilesDTO);
//                contractManagerCreatPopupContractComposer.setDataGrid(contractFilesDTO);
                self.detach();
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    public ContractFilesDTO getDataFromClientContractFiles(ContractDTO contractDTO, FileInfo filesInfo) {

        contractFilesDTO = new ContractFilesDTO();
        contractFilesDTO.setFileContractNumber(txtFileContractNumber.getValue());
        contractFilesDTO.setContent(txtContent.getValue());
        contractFilesDTO.setDescription(filesInfo.getDescription());
        contractFilesDTO.setStartDate(dbStartDate.getValue());
        contractFilesDTO.setEndDate(dbEndDate.getValue());
        contractFilesDTO.setFileName(new File(filesInfo.getLocation()).getName());
        contractFilesDTO.setPathFile(new File(filesInfo.getLocation()).getPath());
        return contractFilesDTO;
    }

    public void onClick$btnCloseAddContract() {
        try {
            self.detach();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    private boolean validate() {
        if (!CtrlValidator.checkValidTextbox(txtFileContractNumber, true, true, action)) {
            return false;
        }
        if (!CtrlValidator.checkValidTextbox(txtContent, true, true, action)) {
            return false;
        }
        if (!CtrlValidator.checkValidTextbox(txtDescription, true, true, action)) {
            return false;
        }

        if (!StringUtils.isValidDate(dbStartDate.getValue())) {
            Clients.wrongValue(dbStartDate, LanguageBundleUtils.getMessage("global.validate.notformat.date.ddMMyyyy", LanguageBundleUtils.getString("contract.startDate")));
            return false;
        }
        if (!StringUtils.isValidDate(dbEndDate.getValue())) {
            Clients.wrongValue(dbEndDate, LanguageBundleUtils.getMessage("global.validate.notformat.date.ddMMyyyy", LanguageBundleUtils.getString("contract.endDate")));
            return false;
        }

        return true;
    }

    @Override
    public void uploadedFilesLocationCallback(List<String> filesLocation) {
        this.filesLocation = filesLocation;
        System.out.println(filesLocation);
    }

    @Override
    public void uploadedFilesInfoCallback(List<FileInfo> filesInfo) {
        this.filesInfo = filesInfo;
        System.out.println(filesInfo);
    }
}
