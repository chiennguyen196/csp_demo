/*
 * Copyright (C) 2010 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.viettel.csp.composer.examples.signPDF;

import com.itextpdf.text.Rectangle;

/**
 * this class contains parameters to sign
 * @author sonnt38@viettel.com.vn
 * @since 25/11/2010
 * @version 1.0
 */
public class PdfSignatureParameter extends SignatureParameter {
    /**
     * name of singer field
     */
    private String name;

    /**
     * location of the signer
     */
    private String location;
    /**
     * position to put signature
     */
    private Rectangle rectangle;
    /**
     * page number to put signature
     */
    private int pageNumber;
    /** TuanNM33: bo sung 05/11/2013
     * page to sign
     */
    private String signerPage;
    /**
     * main constructor
     * @param comment of signer
     */
    public PdfSignatureParameter(String comment) {
        super(comment);
        pageNumber = 0;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getSignerName() {
        return this.name;
    }

    public String getSignerLocation() {
        return this.location;
    }
    
    public void setSignerLocation(String location) {
        this.location = location;
    }

    public Rectangle getPosition() {
        return this.rectangle;
    }
    
    public void setPosition(Rectangle rectangle) {
        this.rectangle = rectangle;
    }

    public void setPage(int i) {
        this.pageNumber = i;
    }

    public int getPage() {
        return this.pageNumber;
    }

    public String getSignerPage() {
        return signerPage;
    }

    public void setSignerPage(String signerPage) {
        this.signerPage = signerPage;
    }
    
    
}
