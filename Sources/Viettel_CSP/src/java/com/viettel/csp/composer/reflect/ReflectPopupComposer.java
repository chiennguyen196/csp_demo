/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.composer.reflect;

import com.viettel.csp.DTO.PartnerDTO;
import com.viettel.csp.DTO.ReflectDTO;
import com.viettel.csp.DTO.ReflectFilesDTO;
import com.viettel.csp.DTO.ReflectTypeDTO;
import com.viettel.csp.DTO.ServiceDTO;
import com.viettel.csp.DTO.SolutionDTO;
import com.viettel.csp.composer.contract.ContractComposer;
import com.viettel.csp.service.PartnerService;
import com.viettel.csp.service.ReflectFilesService;
import com.viettel.csp.service.ReflectService;
import com.viettel.csp.service.ReflectTypeService;
import com.viettel.csp.service.ServiceService;
import com.viettel.csp.service.SolutionService;
import com.viettel.csp.util.CtrlValidator;
import com.viettel.csp.util.GenComponent;
import com.viettel.csp.util.KeyValueBean;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.util.StringUtils;
import com.viettel.csp.util.UserTokenUtils;
import com.viettel.eafs.util.SpringUtil;
import com.viettel.plc.widget.FileInfo;
import com.viettel.plc.widget.FileViews;
import com.viettel.plc.widget.IUploaderCallback;
import com.viettel.plc.widget.UploadComponent;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import static jxl.biff.BaseCellFeatures.logger;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.ForwardEvent;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Button;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Image;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Row;
import org.zkoss.zul.Tabbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 * @author admin
 */
public class ReflectPopupComposer extends GenericForwardComposer<Component> implements IUploaderCallback {

    private Combobox cbPrioritylevel, cbServiceCode, cbCustomerName, cbReflectTypeName;
    private ReflectService reflectService = SpringUtil.getBean("reflectService", ReflectService.class);
    private ServiceService service = SpringUtil.getBean("serviceService", ServiceService.class);
    private PartnerService partnerService = SpringUtil.getBean("partnerService", PartnerService.class);
    private ReflectTypeService reflectTypeService = SpringUtil.getBean("reflectTypeService", ReflectTypeService.class);
    private ReflectFilesService reflectFilesService = SpringUtil.getBean("reflectFilesService", ReflectFilesService.class);
    private SolutionService solutionService = SpringUtil.getBean("solutionService", SolutionService.class);
    private List<ReflectDTO> lstResult;
    private ListModelList<KeyValueBean> kvbServiceName;
    private List<KeyValueBean> lstServiceName;
    private ListModelList<KeyValueBean> kvbPartner;
    private List<KeyValueBean> lstPartner;
    private ListModelList<KeyValueBean> kvbReflectType;
    private List<KeyValueBean> lstReflectType;
    private Button btnSendPartner;
    private Button btnSavePopup;
    private Button btnSolution;
    private Button btnReflectClose;
    private Button btnSolutionNOK;
    private Button btnSolutionMoreInfo;
    private String action;
    private ArrayList indexLst;
    private Textbox txtCode, txtCustomerPhone, txtContent, txtHeadOfService, txtNameCustomer, txtReflectStatus, txtReason, txtReasonDescription;
    private Textbox txtCustomerCareStaffName;
    private Textbox txtCustomerCareStaffPhone;
    private Datebox dbRequiredReponseDate;
    private ReflectDTO rdto;
    private PartnerDTO pdto;
    private Long reflectId;
    private Groupbox gbUploadComponent;
    private ReflectComposer reflectComposer;
    private Listbox lbxContactFiles, lbxSolution;
    private Tabbox tbReflect;
    private UploadComponent uploadComponent;
    private List<String> filesLocation;
    private CtrlValidator ctrlValidator;
    private Row rowReason;
    private List<FileInfo> filesInfo;
//    private Datebox dateReply;

    public Combobox getCbServiceCode() {
        return cbServiceCode;
    }

    public void setCbServiceCode(Combobox cbServiceCode) {
        this.cbServiceCode = cbServiceCode;
    }

    public Combobox getCbCustomerName() {
        return cbCustomerName;
    }

    public void setCbCustomerName(Combobox cbCustomerName) {
        this.cbCustomerName = cbCustomerName;
    }

    public Combobox getCbReflectTypeName() {
        return cbReflectTypeName;
    }

    public void setCbReflectTypeName(Combobox cbReflectTypeName) {
        this.cbReflectTypeName = cbReflectTypeName;
    }

    public ReflectService getReflectService() {
        return reflectService;
    }

    public void setReflectService(ReflectService reflectService) {
        this.reflectService = reflectService;
    }

    public ServiceService getService() {
        return service;
    }

    public void setService(ServiceService service) {
        this.service = service;
    }

    public List<ReflectDTO> getLstResult() {
        return lstResult;
    }

    public void setLstResult(List<ReflectDTO> lstResult) {
        this.lstResult = lstResult;
    }

    public List<KeyValueBean> getLstServiceName() {
        return lstServiceName;
    }

    public void setLstServiceName(List<KeyValueBean> lstServiceName) {
        this.lstServiceName = lstServiceName;
    }

    public Long getReflectID() {
        return reflectId;
    }

    public void setReflectID(Long reflectID) {
        this.reflectId = reflectID;
    }

    public ReflectComposer getReflectComposer() {
        return reflectComposer;
    }

    public void setReflectComposer(ReflectComposer reflectComposer) {
        this.reflectComposer = reflectComposer;
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp); //To change body of generated methods, choose Tools | Templates.
        if (arg.containsKey("REFLECT_ID") && StringUtils.isValidString(String.valueOf(arg.get("REFLECT_ID")))) {
            reflectId = (Long) arg.get("REFLECT_ID");
        }
        if (arg.containsKey(ParamUtils.ACTION)) {
            action = arg.get(ParamUtils.ACTION).toString();
        }
        if (arg.containsKey(ParamUtils.COMPOSER)) {
            reflectComposer = (ReflectComposer) arg.get(ParamUtils.COMPOSER);
        }
        uploadComponent.setUploaderCallback(this);
        setData();
        loadListBoxReflectFiles(reflectId);
    }

    @Override
    public void uploadedFilesLocationCallback(List<String> filesLocation) {
        this.filesLocation = filesLocation;
    }

    public void loadListboxSolution(Long reflectID) throws Exception {
        try {
            SolutionDTO solutionDTO = new SolutionDTO();
            solutionDTO.setReflectId(reflectID);
            List<SolutionDTO> listsolutionDTO = solutionService.getListSolution(solutionDTO);
            if (listsolutionDTO != null && !listsolutionDTO.isEmpty()) {
                ListModelList model = new ListModelList(listsolutionDTO);
                lbxSolution.setModel(model);
            } else {
                lbxSolution.setModel(new ListModelList());
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    public void loadComboboxServiceCode(String valueSelect) throws Exception {
        try {
            List<ServiceDTO> listServiceDTO = service.findAll();
            lstServiceName = new ArrayList<>();
            lstServiceName.add(new KeyValueBean(ParamUtils.DEFAULT_VALUE_STR, LanguageBundleUtils.getString("global.combobox.choose")));
            if (listServiceDTO != null && listServiceDTO.size() > 0) {
                for (ServiceDTO serviceDTO : listServiceDTO) {
                    KeyValueBean key = new KeyValueBean();
                    key.setKey(serviceDTO.getCode());
                    key.setValue(serviceDTO.getName());
                    lstServiceName.add(key);
                }
            }
            kvbServiceName = new ListModelList(lstServiceName);
            int i = 0;
            for (KeyValueBean itemParam : kvbServiceName.getInnerList()) {
                if (itemParam.getKey() != null && String.valueOf(itemParam.getKey()).equals(valueSelect)) {
                    kvbServiceName.addToSelection(kvbServiceName.getElementAt(i));
                    break;
                }
                i++;
            }
            cbServiceCode.setModel(kvbServiceName);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    public void loadComboboxPartner(String valueSelect) throws Exception {
        try {
            List<PartnerDTO> listPartner = partnerService.getListPartnerActive();
            lstPartner = new ArrayList<>();
            lstPartner.add(new KeyValueBean(ParamUtils.DEFAULT_VALUE_STR, LanguageBundleUtils.getString("global.combobox.choose")));
            if (lstPartner != null && lstPartner.size() > 0) {
                for (PartnerDTO partnerDTO : listPartner) {
                    KeyValueBean key = new KeyValueBean();
                    key.setKey(partnerDTO.getId());
                    key.setValue(partnerDTO.getCompanyName());
                    lstPartner.add(key);
                }
            }
            kvbPartner = new ListModelList(lstPartner);
            int i = 0;
            for (KeyValueBean itemParam : kvbPartner.getInnerList()) {
                if (itemParam.getKey() != null && String.valueOf(itemParam.getKey()).equals(valueSelect)) {
                    kvbPartner.addToSelection(kvbPartner.getElementAt(i));
                    break;
                }
                i++;
            }
            cbCustomerName.setModel(kvbPartner);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    public void loadListBoxReflectFiles(Long reflectID) throws Exception {
        try {
            ReflectFilesDTO filesDTO = new ReflectFilesDTO();
            filesDTO.setReflectId(reflectID);
            List<ReflectFilesDTO> listfilesDTO = reflectFilesService.getList(filesDTO);
            if (listfilesDTO != null && !listfilesDTO.isEmpty()) {
                ListModelList model = new ListModelList(listfilesDTO);
                lbxContactFiles.setModel(model);
            } else {
                lbxContactFiles.setModel(new ListModelList());
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }

    }

    public void loadComboboxReflectType(String valueSelect) throws Exception {
        try {
            List<ReflectTypeDTO> listReflectTypeDTO = reflectTypeService.findAll();
            lstReflectType = new ArrayList<>();
            lstReflectType.add(new KeyValueBean(ParamUtils.DEFAULT_VALUE_STR, LanguageBundleUtils.getString("global.combobox.choose")));
            if (listReflectTypeDTO != null && listReflectTypeDTO.size() > 0) {
                for (ReflectTypeDTO reflectTypeDTO : listReflectTypeDTO) {
                    KeyValueBean key = new KeyValueBean();
                    key.setKey(reflectTypeDTO.getCode());
                    key.setValue(reflectTypeDTO.getName());
                    lstReflectType.add(key);
                }
            }
            kvbReflectType = new ListModelList(lstReflectType);
            int i = 0;
            for (KeyValueBean itemParam : kvbReflectType.getInnerList()) {
                if (itemParam.getKey() != null && String.valueOf(itemParam.getKey()).equals(valueSelect)) {
                    kvbReflectType.addToSelection(kvbReflectType.getElementAt(i));
                    break;
                }
                i++;
            }
            cbReflectTypeName.setModel(kvbReflectType);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    public ReflectDTO getDataFromClient(String statusCode) {
        rdto = new ReflectDTO();
        rdto.setId(reflectId);
        rdto.setReflectStatusCode(statusCode);
        rdto.setCode(txtCode.getValue().trim());
        rdto.setHeadOfService(txtHeadOfService.getValue().trim());
        rdto.setCustomerPhone(txtCustomerPhone.getValue().trim());
        rdto.setContent(txtContent.getValue().trim());
        rdto.setCustomerName(txtNameCustomer.getValue().trim());
        rdto.setCustomerCareStaffName(txtCustomerCareStaffName.getValue().trim());
        rdto.setCustomerCareStaffPhone(txtCustomerCareStaffPhone.getValue().trim());
        if (cbPrioritylevel.getSelectedIndex() >= 0) {
            rdto.setPriorityLevel(cbPrioritylevel.getSelectedItem().getValue().toString());
        }
        if (cbServiceCode.getSelectedIndex() >= 0) {
            rdto.setServiceCode(cbServiceCode.getSelectedItem().getValue().toString());
        }
        if (cbReflectTypeName.getSelectedIndex() >= 0) {
            rdto.setReflectTypeCode(cbReflectTypeName.getSelectedItem().getValue().toString());
        }
        if (cbServiceCode.getSelectedIndex() >= 0) {
            rdto.setServiceCode(cbServiceCode.getSelectedItem().getValue().toString());
        }
        if (cbCustomerName.getSelectedIndex() >= 0) {
            rdto.setPartnerId(Long.parseLong(cbCustomerName.getSelectedItem().getValue().toString()));
        }
        rdto.setRequiredReponseDate(dbRequiredReponseDate.getValue());

        return rdto;
    }

    public void onClick$btnSavePopup() {
        try {
            if (validate()) {
                if (ParamUtils.ACTION_CREATE.equals(action)) {
                    ReflectDTO reflectDTO = getDataFromClient(ParamUtils.REFLECT_STATUS.NEW);

                    this.uploadComponent.onClick$doSave();
                    uploadedFilesInfoCallback(filesInfo);
                    List<ReflectFilesDTO> reflectFilesDTO = new ArrayList<>();
                    if(filesInfo!=null) {
                        for (int i = 0; i < filesInfo.size(); i++) {
                            ReflectFilesDTO filesDTO = getDataFromClientReflectFilesDTO(filesInfo.get(i));
                            reflectFilesDTO.add(filesDTO);
                        }
                    }

                    reflectDTO.setLstReflectFilesDTO(reflectFilesDTO);

                    reflectService.insert(reflectDTO);

                    Clients.showNotification(LanguageBundleUtils.getString("reflect.message.sucess"), "info", null, "middle_center", 1000);
                } else if (ParamUtils.ACTION_UPDATE.equals(action)) {
                    ReflectDTO reflectDTO = getDataFromClient(ParamUtils.REFLECT_STATUS.NEW);
                    reflectService.update(reflectDTO);
                    Clients.showNotification(LanguageBundleUtils.getString("reflect.messageupdate.sucess"), "info", null, "middle_center", 3000);
                }
                self.detach();
                reflectComposer.bindDataToGrid();
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    public void setData() throws Exception {
        if (ParamUtils.ACTION_CREATE.equals(action)) {
            ((Window) self).setTitle(LanguageBundleUtils.getString("global.add"));
            txtCode.setValue("");
            txtCustomerPhone.setValue("");
            txtContent.setValue("");
            txtHeadOfService.setValue("");
            txtReflectStatus.setDisabled(true);
            loadComboboxPartner(ParamUtils.DEFAULT_VALUE_STR);
            loadComboboxReflectType(ParamUtils.DEFAULT_VALUE_STR);
            loadComboboxServiceCode(ParamUtils.DEFAULT_VALUE_STR);
            cbPrioritylevel.setSelectedIndex(ParamUtils.SELECT_NOTHING_INDEX);
            lbxContactFiles.setVisible(false);
            lbxSolution.setVisible(false);
            tbReflect.setVisible(false);
            rowReason.setVisible(false);

            btnReflectClose.setVisible(false);
            btnSolutionNOK.setVisible(false);
            btnSolutionMoreInfo.setVisible(false);
            btnSolution.setVisible(false);
        } else if (ParamUtils.ACTION_UPDATE.equals(action)) {
            ReflectDTO reflectDTO = new ReflectDTO();
            reflectDTO.setId(reflectId);
            List<ReflectDTO> list = reflectService.getListReflect(reflectDTO);
            reflectDTO = list.get(0);
            ((Window) self).setTitle(LanguageBundleUtils.getString("global.edit"));
            if (reflectDTO != null) {
                txtCode.setValue(reflectDTO.getCode());
                txtCode.setDisabled(true);
                txtCustomerPhone.setValue(reflectDTO.getCustomerPhone());
                txtContent.setValue(reflectDTO.getContent());
                txtHeadOfService.setValue(reflectDTO.getHeadOfService());
                txtNameCustomer.setValue(reflectDTO.getCustomerName());
                dbRequiredReponseDate.setValue(reflectDTO.getRequiredReponseDate());
                txtReflectStatus.setValue(reflectDTO.getReflectStatusName());
                txtReason.setValue(reflectDTO.getReasonName());
                txtReasonDescription.setValue(reflectDTO.getReasonDescription());
                txtCustomerCareStaffName.setValue(reflectDTO.getCustomerCareStaffName());
                txtCustomerCareStaffPhone.setValue(reflectDTO.getCustomerCareStaffPhone());
                loadComboboxPartner(String.valueOf(reflectDTO.getPartnerId()));
                loadComboboxReflectType(reflectDTO.getReflectTypeCode());
                loadComboboxServiceCode(reflectDTO.getServiceCode());
                if (StringUtils.isValidString(reflectDTO.getPriorityLevel())) {
                    GenComponent component = new GenComponent();
                    component.setSelectedCbo(cbPrioritylevel, reflectDTO.getPriorityLevel());
                }
                setDisable(true);
                btnReflectClose.setVisible(false);
                btnSolutionNOK.setVisible(false);
                btnSolutionMoreInfo.setVisible(false);
                btnSavePopup.setVisible(false);
                btnSendPartner.setVisible(false);
                btnSolution.setVisible(false);
                gbUploadComponent.setVisible(false);
                rowReason.setVisible(false);
                if (reflectDTO.getReflectStatusCode().equalsIgnoreCase(ParamUtils.REFLECT_STATUS.SEND)) {
                    if (UserTokenUtils.getPersonType().equalsIgnoreCase(ParamUtils.USER_TYPE.CONTACT)
                            || UserTokenUtils.getPersonType().equalsIgnoreCase(ParamUtils.USER_TYPE.PARTNER)) {
                        btnSolution.setVisible(true);
                    } else {
                        btnReflectClose.setVisible(false);
                        btnSolutionNOK.setVisible(false);
                        btnSolutionMoreInfo.setVisible(false);
                    }
                } else if (reflectDTO.getReflectStatusCode().equalsIgnoreCase(ParamUtils.REFLECT_STATUS.NEW)
                        && UserTokenUtils.getPersonType().equalsIgnoreCase(ParamUtils.USER_TYPE.VIETTEL)) {
                    btnSavePopup.setVisible(true);
                    btnSendPartner.setVisible(true);
                    setDisable(false);
                } else if (reflectDTO.getReflectStatusCode().equalsIgnoreCase(ParamUtils.REFLECT_STATUS.REPONSE_CUSTOMER)
                        && UserTokenUtils.getPersonType().equalsIgnoreCase(ParamUtils.USER_TYPE.VIETTEL)) {
                    btnReflectClose.setVisible(true);
                    btnSolutionNOK.setVisible(true);
                    btnSolutionMoreInfo.setVisible(true);
                } else if (reflectDTO.getReflectStatusCode().equalsIgnoreCase(ParamUtils.REFLECT_STATUS.CLOSE)) {
                    rowReason.setVisible(true);
                }
                loadListBoxReflectFiles(reflectId);
                loadListboxSolution(reflectId);
            }
        }
    }

    public void onClick$btnReflectClose() {
        try {
            Map<String, Object> argu = new HashMap();
            argu.put("REFLECT_ID", reflectId);
            argu.put(ParamUtils.COMPOSER, this);
            Window wd;
            wd = (Window) Executions.createComponents("/controls/reflect/reflecClosetPopup.zul", null, argu);
            wd.doModal();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    public void onClick$btnSolution() {
        try {
            Map<String, Object> argu = new HashMap();
            argu.put(ParamUtils.ACTION, ParamUtils.ACTION_CREATE);
            argu.put("REFLECT_ID", reflectId);
            argu.put(ParamUtils.COMPOSER, this);
            Window wd;//reflectPopup
            wd = (Window) Executions.createComponents("/controls/reflect/solutionPopup.zul", null, argu);
            wd.doModal();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    public void onViewSolution(ForwardEvent evt) {
        try {
            evt.stopPropagation();
            Image btn = (Image) evt.getOrigin().getTarget();
            Listitem litem = (Listitem) btn.getParent().getParent();

            Map<String, Object> argu = new HashMap();
            SolutionDTO oldDTO = (SolutionDTO) lbxSolution.getItemAtIndex(litem.getIndex()).getValue();
            argu.put(ParamUtils.ACTION, ParamUtils.ACTION_VIEW);
            argu.put("SOLUTION_ID", oldDTO.getId());
            argu.put(ParamUtils.COMPOSER, this);
            Window wd;//reflectPopup
            wd = (Window) Executions.createComponents("/controls/reflect/solutionPopup.zul", null, argu);
            wd.doModal();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    public void onClick$btnSolutionNOK() {
        try {
            SolutionDTO solution = solutionService.getSolutionReponse(reflectId);
            Map<String, Object> argu = new HashMap();
            argu.put(ParamUtils.ACTION, ParamUtils.SOLUTION_STATUS.NOK);
            argu.put("SOLUTION_ID", solution.getId());
            argu.put(ParamUtils.COMPOSER, this);
            Window wd;//reflectPopup
            wd = (Window) Executions.createComponents("/controls/reflect/solutionEvalPopup.zul", null, argu);
            wd.doModal();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    public void onClick$btnSolutionMoreInfo() {
        try {
            SolutionDTO solution = solutionService.getSolutionReponse(reflectId);
            Map<String, Object> argu = new HashMap();
            argu.put(ParamUtils.ACTION, ParamUtils.SOLUTION_STATUS.MORE_INFO);
            argu.put("SOLUTION_ID", solution.getId());
            argu.put(ParamUtils.COMPOSER, this);
            Window wd;//reflectPopup
            wd = (Window) Executions.createComponents("/controls/reflect/solutionEvalPopup.zul", null, argu);
            wd.doModal();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    public void onClick$btnViewSolution() {
        try {
            if (lbxSolution.getSelectedCount() > 0) {
                Messagebox.show(LanguageBundleUtils.getString("global.action.confirm.multi.delete"), "Confirm Dialog", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION, new org.zkoss.zk.ui.event.EventListener() {
                    public void onEvent(Event evt) throws InterruptedException, Exception {
                        if ("onOK".equals(evt.getName())) {
                            SolutionDTO solution = (SolutionDTO) lbxSolution.getSelectedItem().getValue();
                            Map<String, Object> argu = new HashMap();
                            argu.put(ParamUtils.ACTION, ParamUtils.ACTION_VIEW);
                            argu.put("SOLUTION_ID", solution.getId());
                            argu.put(ParamUtils.COMPOSER, this);
                            Window wd;
                            wd = (Window) Executions.createComponents("/controls/reflect/solutionPopup.zul", null, argu);
                            wd.doModal();
                        }
                    }
                });
            } else {
                Clients.showNotification(LanguageBundleUtils.getString("global.action.confirm.is_select"), "warning", null, "middle_center", 3000);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    public void onClick$btnSendPartner() {
        try {
            String messageContract = "";
            ReflectDTO reflectDTO = getDataFromClient(ParamUtils.REFLECT_STATUS.SEND);
            reflectService.sendPartner(reflectDTO);
            Clients.showNotification(LanguageBundleUtils.getString("reflect.message.send.to.partner"), "info", null, "middle_center", 1000);
            self.detach();
            reflectComposer.bindDataToGrid();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    public ReflectFilesDTO getDataFromClientReflectFilesDTO(FileInfo fileInfo) {
        ReflectFilesDTO reflectFilesDTO = new ReflectFilesDTO();
        reflectFilesDTO.setFileName(new File(fileInfo.getLocation()).getName());
        reflectFilesDTO.setPathFile(new File(fileInfo.getLocation()).getPath());
        reflectFilesDTO.setDescription(fileInfo.getDescription());
        return reflectFilesDTO;
    }

    public boolean validate() {
        if (!ctrlValidator.checkValidTextbox(txtCode, true, true, LanguageBundleUtils.getString("reflect.code"))) {
            return false;
        }
        if (!ctrlValidator.checkValidDateBox(dbRequiredReponseDate, true, LanguageBundleUtils.getString("reflect.requiredreponsedate"))) {
            return false;
        }
        if (!ctrlValidator.checkValidComboValue(cbCustomerName, true, true, LanguageBundleUtils.getString("reflect.cbCustomerName"))) {
            return false;
        }
        if (!ctrlValidator.checkValidPhone(txtCustomerPhone, true, LanguageBundleUtils.getString("reflect.customerPhone"))) {
            return false;
        }
        if (!ctrlValidator.checkValidTextbox(txtCustomerCareStaffName, true, true, LanguageBundleUtils.getString("reflect.customerCareStaffName"))) {
            return false;
        }
        if (!ctrlValidator.checkValidPhone(txtCustomerCareStaffPhone, true, LanguageBundleUtils.getString("reflect.customerCareStaffPhone"))) {
            return false;
        }
        if (!ctrlValidator.checkValidComboValue(cbServiceCode, true, true, LanguageBundleUtils.getString("reflect.cbServiceName"))) {
            return false;
        }
        if (!ctrlValidator.checkValidTextbox(txtContent, true, true, LanguageBundleUtils.getString("reflect.Content"))) {
            return false;
        }
        if (!ctrlValidator.checkValidTextbox(txtHeadOfService, true, true, LanguageBundleUtils.getString("reflect.cbHeadOfService"))) {
            return false;
        }
        if (!ctrlValidator.checkValidComboValue(cbReflectTypeName, true, true, LanguageBundleUtils.getString("reflect.cbRefectTypeName"))) {
            return false;
        }
        if (!ctrlValidator.checkValidComboValue(cbPrioritylevel, true, true, LanguageBundleUtils.getString("reflect.prioritylevel"))) {
            return false;
        }
        return true;
    }

    public void onDownloadFileReflect(ForwardEvent evt) {
        evt.stopPropagation();
        try {
            Image btn = (Image) evt.getOrigin().getTarget();
            Listitem litem = (Listitem) btn.getParent().getParent();
            onClick$btnDownloadFile(litem);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(ContractComposer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onClick$btnDownloadFile(Listitem litem) {
        try {
            List<ReflectFilesDTO> lstContractFilesResult = new ArrayList<>();
            lstContractFilesResult.add(litem.getValue());
            File file = new File(lstContractFilesResult.get(0).getPathFile());
            Filedownload.save(file, null);
        } catch (Exception ex) {
            Messagebox.show(LanguageBundleUtils.getMessage("messageUploadFileError"), LanguageBundleUtils.getMessage("Messagebox.warning"), Messagebox.OK, Messagebox.ERROR, null);
        }
    }

    private void setDisable(boolean b) {
        txtNameCustomer.setDisabled(b);
        cbPrioritylevel.setDisabled(b);
        txtCustomerCareStaffName.setDisabled(b);
        txtCustomerCareStaffPhone.setDisabled(b);
        cbServiceCode.setDisabled(b);
        cbCustomerName.setDisabled(b);
        cbReflectTypeName.setDisabled(b);
        txtCustomerPhone.setDisabled(b);
        txtContent.setDisabled(b);
        txtHeadOfService.setDisabled(b);
        dbRequiredReponseDate.setDisabled(b);
        txtReflectStatus.setDisabled(true);
        txtReasonDescription.setDisabled(true);
        txtReason.setDisabled(true);
    }

    public void onDownloadFile(ForwardEvent evt) {
        evt.stopPropagation();
        try {
            Image btn = (Image) evt.getOrigin().getTarget();
            Listitem litem = (Listitem) btn.getParent().getParent();
            List<ReflectFilesDTO> lstDTO = new ArrayList<>();
            lstDTO.add(litem.getValue());
            java.io.File file = new java.io.File(lstDTO.get(0).getPathFile());
            Filedownload.save(file, null);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(ContractComposer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onViewFile(ForwardEvent evt) {
        evt.stopPropagation();
        try {
            List<FileViews> lstFile = new ArrayList();
            Image btn = (Image) evt.getOrigin().getTarget();
            Listitem litem = (Listitem) btn.getParent().getParent();
            List<ReflectFilesDTO> lstDTO = new ArrayList<>();
            lstDTO.add(litem.getValue());
            FileViews file = new FileViews(lstDTO.get(0).getFileName(), lstDTO.get(0).getPathFile());
            lstFile.add(file);
            Map<String, Object> argu = new HashMap();
            argu.put(ParamUtils.ACTION, ParamUtils.ACTION_CREATE);
            argu.put(ParamUtils.COMPOSER, this);
            argu.put(ParamUtils.DATA, lstFile);
            Window wd = (Window) Executions.createComponents("/controls/widget/pdfFilesViewerPopup.zul", null, argu);
            wd.doModal();
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(ContractComposer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void uploadedFilesInfoCallback(List<FileInfo> filesInfo) {
        this.filesInfo = filesInfo;
        System.out.println(filesInfo);
    }


    public void onClick$btnclosePopup() {
        reflectComposer.bindDataToGrid();
        self.detach();
    }
}
