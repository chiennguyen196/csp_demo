/*
 * Copyright (C) 2010 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.viettel.csp.composer.examples.signPDF.ooxml;

import com.viettel.csp.composer.examples.signPDF.ClientServerSignature;
import com.viettel.csp.composer.examples.signPDF.DigitalSignature;
import com.viettel.csp.composer.examples.signPDF.DigitalSignatureException;
import com.viettel.csp.composer.examples.signPDF.SignatureObject;
import com.viettel.csp.composer.examples.signPDF.SignatureParameter;
import com.viettel.csp.composer.examples.signPDF.Verifier;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.security.KeyException;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.List;

/**
 *
 * @author sonnt38@viettel.com.vn
 * @since since_text
 * @version 1.0
 */
public class OoxmlSignature extends ClientServerSignature implements DigitalSignature {

    /**
     * OOXML verifier object
     */
    private OoxmlSignatureVerifier osv;

    /**
     * Signer for client server
     */
    OoxmlDigitalSignature ods;

    /**
     * main constructor
     */
    public OoxmlSignature() {
        OoxmlSignatureVerifier.initial();
        osv = new OoxmlSignatureVerifier();
        ods = new OoxmlDigitalSignature();
    }

    @Override
    public void sign(URL fileURL, PrivateKey key, Certificate[] chain, SignatureParameter parameter, OutputStream os)
            throws Exception {
        checkInput(key, chain, os);
        OoxmlDigitalSignature ods = new OoxmlDigitalSignature(key, chain);
        ods.sign(fileURL, os, parameter);
    }

    @Override
    public void sign(String fileName, PrivateKey key, Certificate[] chain, SignatureParameter parameter, OutputStream os)
            throws Exception {
        checkInput(key, chain, os);
        File file = new File(fileName);
        OoxmlDigitalSignature ods = new OoxmlDigitalSignature(key, chain);
        ods.sign(file.toURI().toURL(), os, parameter);
    }

    @Override
    public List<Verifier> verify(URL fileURL, Certificate certificate) throws Exception {
        return osv.verify(fileURL, certificate, null);
    }

    @Override
    public List<Verifier> verify(String fileName, Certificate certificate) throws Exception {
        File file = new File(fileName);
        return osv.verify(file.toURI().toURL(), certificate, null);
    }

    @Override
    public List<Verifier> verify(URL fileURL, KeyStore keystore) throws Exception {
        return osv.verify(fileURL, keystore, null);
    }

    @Override
    public List<Verifier> verify(String fileName, KeyStore keystore) throws Exception {
        return osv.verify(new File(fileName).toURI().toURL(), keystore, null);
    }

    @Override
    public SignatureObject createDigest(String inFile, SignatureParameter signatureParameter, Certificate[] chain, boolean checkLogo) throws Exception {
        File file = new File(inFile);
        return createDigest(file.toURI().toURL(), signatureParameter, chain);
    }

    @Override
    public SignatureObject createDigest(URL fileURL, SignatureParameter signatureParameter, Certificate[] chain) throws Exception {

        if (fileURL == null) {
            throw new NullPointerException("OOXML file is not null");
        }
        return ods.createDigest(fileURL);
    }

    @Override
    public void insertSignature(SignatureObject object, String outFile) throws Exception {
        byte[] signed = ods.insertSignature(object);
        FileOutputStream fos = new FileOutputStream(outFile);
        fos.write(signed);
        if (fos != null) {
            fos.flush();
            fos.close();
        }
    }

    @Override
    public List<X509Certificate> getCertificate(String fileName) throws Exception {
        return osv.getCertificate(new File(fileName).toURI().toURL());
    }

    @Override
    public String getFirstIssuerCertificate(String fileName) throws Exception {
        return osv.getFirstIssuer(new File(fileName).toURI().toURL());
    }

    @Override
    public String getFirstIssuerCertificate(URL fileURL) throws Exception {
        return osv.getFirstIssuer(fileURL);
    }

    public int getSignatureNumbers(String fileName) throws Exception {
        return osv.getSignatureResourceNames(new File(fileName).toURI().toURL()).size();
    }

    public int getSignatureNumbers(URL fileURL) throws Exception {
        return osv.getSignatureResourceNames(fileURL).size();
    }

    private void checkInput(PrivateKey key, Certificate[] chain, OutputStream fos)
            throws DigitalSignatureException, KeyException {
        if (key == null) {
            throw new NullPointerException("Private key is not null");
        }

        if (fos == null) {
            throw new NullPointerException("Output stream is not null");
        }

        if (!key.getAlgorithm().equals("RSA")) {
            throw new KeyException("Just use RSA key for safety reason");
        }
        checkCertificateChain(chain);

    }

    private void checkCertificateChain(Certificate[] chain) throws DigitalSignatureException {
        if (chain == null
                || chain.length < 1) {
            throw new NullPointerException("Certificate chain is not null");
        }

        for (int i = 0; i < chain.length; ++i) {
            if (false == (chain[i] instanceof X509Certificate)) {
                throw new DigitalSignatureException("Certificate chain is not X.509 type");
            }
        }
    }

    @Override
    public byte[] createHash(String inFile, SignatureParameter parameter, Certificate[] chain) throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public byte[] createHash(InputStream is, SignatureParameter parameter, Certificate[] chain) throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void insertSignature(byte[] hash, String outFile) throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
