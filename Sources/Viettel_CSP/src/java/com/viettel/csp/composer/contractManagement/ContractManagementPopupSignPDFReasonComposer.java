/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.composer.contractManagement;

import com.viettel.csp.DTO.ContractDTO;
import com.viettel.csp.DTO.ContractHisDTO;
import com.viettel.csp.DTO.ReasonDTO;
import com.viettel.csp.service.ContractHisService;
import com.viettel.csp.service.ContractService;
import com.viettel.csp.service.ContractStatusService;
import com.viettel.csp.service.PartnerService;
import com.viettel.csp.service.ReasonService;
import com.viettel.csp.util.KeyValueBean;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.ParamUtils;
import com.viettel.eafs.util.SpringUtil;
import java.util.ArrayList;
import java.util.List;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Textbox;

/**
 * @author chitv@gemvietnam.com
 */
public class ContractManagementPopupSignPDFReasonComposer extends GenericForwardComposer<Component> {

    final static org.apache.log4j.Logger logger = org.apache.log4j.Logger
        .getLogger(ContractManagementPopupSignPDFReasonComposer.class);


    @Override
    public void doAfterCompose(Component comp) {
        try {
            super.doAfterCompose(comp);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            Clients
                .showNotification(LanguageBundleUtils.getString("global.message.error"), "warning",
                    null,
                    "middle_center", 3000);
        }
    }

    public void onClick$btnSign(){
        Events.postEvent(new Event("onSignPDFSuccess", this.self.getParent()));
        this.self.detach();
    }
}
