package com.viettel.csp.composer.reflect;


import com.viettel.csp.DTO.ReflectDTO;
import com.viettel.csp.service.ReflectService;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.ParamUtils;
import com.viettel.eafs.util.SpringUtil;
import org.activiti.engine.impl.bpmn.data.Data;
import org.apache.log4j.Logger;
import org.zkoss.util.media.AMedia;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.WebApps;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zss.api.*;
import org.zkoss.zss.api.model.Book;
import org.zkoss.zss.api.model.CellData;
import org.zkoss.zss.ui.Spreadsheet;
import org.zkoss.zul.Button;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Label;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created by phamvanluong on 2017-06-10.
 */
public class ReflectImportPAKNComposer extends GenericForwardComposer<Component> {

    Logger logger = Logger.getLogger(ReflectImportPAKNComposer.class);
    private ReflectService reflectService = SpringUtil.getBean("reflectService", ReflectService.class);

    private Button btnAddFiles;
    private Button btnclosePAKN;
    private Button btnSavePAKN;
    private Button btnDownFile;
    private ReflectComposer reflectComposer;
    private Book book = null;


    @Wire
    private Label lblFileName;

    @Wire("spreadsheet")
    Spreadsheet spreadsheet;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);

        if (arg.containsKey(ParamUtils.COMPOSER)) {
            reflectComposer = (ReflectComposer) arg.get(ParamUtils.COMPOSER);
        }

        btnDownFile.setVisible(false);
    }

    public void onUpload$btnAddFiles(UploadEvent evt) {


        Media medias = evt.getMedia();
        String name = getCorrectFileName(medias);
        int sizeNameFile = name.length();

        if (name.substring(sizeNameFile - 5).equals(".xlsx")) {

            if (medias.isBinary()) {

                lblFileName.setValue(name);

                Importer importer = Importers.getImporter();


                try {
                    String nameFile = name.substring(0, sizeNameFile - 5);
                    book = importer.imports(medias.getStreamData(), nameFile);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        } else {
            Clients.showNotification(LanguageBundleUtils.getString("global.message.upload.error.fileExcel"), "warning", null, "middle_center", 3000);

        }


    }

    private String getCorrectFileName(Media data) {
        return data.getName().replaceAll(",", "-");
    }

    public void onClick$btnclosePAKN() {
        self.detach();
    }

    public void onClick$btnSavePAKN() {
        try {

            spreadsheet.setBook(book);
            getData(0, 13).setEditText("Kết quả");

            int row = 1;
            while (!getData(row, 0).getEditText().equals("")) {

                ReflectDTO rdto = new ReflectDTO();
                rdto.setReflectStatusCode(ParamUtils.REFLECT_STATUS.NEW);
                rdto.setLstReflectFilesDTO(null);

                boolean checkInsert = true;

                for (int col = 1; col < 13; col++) {

                    if (getData(row, col).getEditText().equals("")) {

                        checkInsert = false;

                        checkColNull(row, col);

                        break;
                    } else {

                        switch (col) {
                            case 1:
                                rdto.setCode(getData(row, col).getEditText());
                                break;
                            case 2:
                                rdto.setCustomerName(getData(row, col).getEditText());
                                break;
                            case 3:
                                rdto.setServiceCode(getData(row, col).getEditText());
                                break;
                            case 4:
                                rdto.setHeadOfService(getData(row, col).getEditText());
                                break;
                            case 5:
                                rdto.setPartnerId(Long.parseLong(getData(row, col).getEditText()));
                                break;
                            case 6:
                                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                                try {
                                    Date date = formatter.parse(getData(row, col).getEditText());
                                    rdto.setRequiredReponseDate(date);

                                } catch (ParseException e) {
                                    e.printStackTrace();
                                    checkInsert = false;
                                    getData(row, 13).setEditText("NOT OK ( Định dạng ngày tháng sai)");
                                }
                                break;
                            case 7:
                                rdto.setCustomerPhone(getData(row, col).getEditText());
                                break;
                            case 8:
                                rdto.setPriorityLevel(getData(row, col).getEditText());
                                break;
                            case 9:
                                rdto.setReflectTypeCode(getData(row, col).getEditText());
                                break;
                            case 10:
                                rdto.setContent(getData(row, col).getEditText());
                                break;
                            case 11:
                                rdto.setCustomerCareStaffName(getData(row, col).getEditText());
                                break;
                            case 12:
                                rdto.setCustomerCareStaffPhone(getData(row, col).getEditText());
                                break;

                        }

                    }

                }

                if (checkInsert) {
                    try {
                        if (reflectService.checkCode(rdto.getCode())!=-1){

                            rdto.setId(reflectService.checkCode(rdto.getCode()));

                            reflectService.update(rdto);
                            getData(row, 13).setEditText("Update OK");

                        }else {

                            reflectService.insert(rdto);
                            getData(row, 13).setEditText("Insert OK");

                        }
                        Clients.showNotification(LanguageBundleUtils.getString("reflect.message.import"), "info", null, "middle_center", 1000);
                    } catch (Exception e) {
                        e.printStackTrace();
                        getData(row, 13).setEditText("NOT OK ( Không thể thêm bản ghi)");
                    }
                }

                row++;

            }

            if (row == 1) {
                Clients.showNotification(LanguageBundleUtils.getString("global.message.upload.error.noInsert"), "warning", null, "middle_center", 3000);

            } else {
                btnDownFile.setVisible(true);
            }

            reflectComposer.bindDataToGrid();




        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.upload.error.noSelectedFile"), "warning", null, "middle_center", 3000);
        }

    }

    private void checkColNull(int row, int col) {

        switch (col) {
            case 1:
                getData(row, 13).setEditText("NOT OK ( Mã khiếu nại null)");
                break;
            case 2:
                getData(row, 13).setEditText("NOT OK ( Tên khách hàng null)");
                break;
            case 3:
                getData(row, 13).setEditText("NOT OK ( Mã Dịch vụ null)");
                break;
            case 4:
                getData(row, 13).setEditText("NOT OK ( Đầu số null)");
                break;
            case 5:
                getData(row, 13).setEditText("NOT OK ( ID Đối tác bị phản ánh null)");
                break;
            case 6:
                getData(row, 13).setEditText("NOT OK ( Yêu cầu trả lời trong thời gian null)");
                break;
            case 7:
                getData(row, 13).setEditText("NOT OK ( SĐT chăm sóc khách hàng null)");
                break;
            case 8:
                getData(row, 13).setEditText("NOT OK ( Mức độ ưu tiên  null)");
                break;
            case 9:
                getData(row, 13).setEditText("NOT OK ( Mã Loại phản ánh null)");
                break;
            case 10:
                getData(row, 13).setEditText("NOT OK ( Nội dung null)");
                break;
            case 11:
                getData(row, 13).setEditText("NOT OK ( Nhân viên CSKH null)");
                break;
            case 12:
                getData(row, 13).setEditText("NOT OK ( SĐT nhân viên CSKH null)");
                break;

        }

    }

    public void onClick$btnDownFile() {

        Exporter exporter = Exporters.getExporter();
        Book book = spreadsheet.getBook();
        FileOutputStream fos = null;
        File file = null;
        try {
            file = File.createTempFile(Long.toString(System.currentTimeMillis()), "temp");
            fos = new FileOutputStream(file);
            exporter.export(book, fos);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        String dlname = "result.xlsx";
        try {
            Filedownload.save(new AMedia(dlname, null, null, file, true));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }


    private CellData getData(int row, int col) {
        Range range = Ranges.range(spreadsheet.getSelectedSheet(), row, col);
        CellData cellData = range.getCellData();

        return cellData;
    }
}
