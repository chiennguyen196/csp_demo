package com.viettel.csp.composer.service;

import com.viettel.csp.DTO.*;
import com.viettel.csp.composer.contract.ContractComposer;
import com.viettel.csp.entity.ServiceEntity;
import com.viettel.csp.service.ServiceService;
import com.viettel.csp.util.*;
import com.viettel.eafs.util.SpringUtil;
import com.viettel.plc.widget.FileInfo;
import com.viettel.plc.widget.IUploaderCallback;
import com.viettel.plc.widget.UploadComponent;
import com.viettel.plc.widget.UploadOneFileComponent;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.ForwardEvent;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.*;


import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import static jxl.biff.BaseCellFeatures.logger;

/**
 * Created by LINHDT on 06/13/2017.
 */
public class ServicePopupComposer extends GenericForwardComposer<Component> implements IUploaderCallback {

    final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ServicePopupComposer.class);
    private Textbox txtName, txtPathFile, txtCode, txtParentCode;
    private Button btnSavePopup, btnEdit, btnDelete, btnReset;
    private String serviceCode;
    private String action;
    private ServiceComposer serviceComposer;
    private CtrlValidator ctrlValidator;
    private Combobox cbbIdParentCode;
    private List<FileInfo> filesInfo;
    private UploadOneFileComponent uploadOneFileComponent;
    private List<String> filesLocation;
    private Groupbox gbUploadComponent;
    private List<KeyValueBean> lstServiceName;
    private List<KeyValueBean> lstDataParentCode;
    private List listParents;
    private Label labelPath, labelStart;
    private ListModelList<KeyValueBean> kvbServiceName;
    private ServiceService serviceService = SpringUtil.getBean("serviceService", ServiceService.class);

    private ListModelList<KeyValueBean> listParentCode = new ListModelList<KeyValueBean>();

    public ServiceComposer getServiceComposer() {
        return serviceComposer;
    }

    public void setServiceComposer(ServiceComposer serviceComposer) {
        this.serviceComposer = serviceComposer;
    }

    public Combobox getCbbIdParentCode() {
        return cbbIdParentCode;
    }

    public void setCbbIdParentCode(Combobox cbbIdParentCode) {
        this.cbbIdParentCode = cbbIdParentCode;
    }

    public ServiceService getServiceService() {
        return serviceService;
    }

    public void setServiceService(ServiceService serviceService) {
        this.serviceService = serviceService;
    }

    public ListModelList<KeyValueBean> getListParentCode() {
        return listParentCode;
    }

    public void setListParentCode(ListModelList<KeyValueBean> listParentCode) {
        this.listParentCode = listParentCode;
    }

    public Textbox getTxtName() {
        return txtName;
    }

    public void setTxtName(Textbox txtName) {
        this.txtName = txtName;
    }

    public Textbox getTxtPathFile() {
        return txtPathFile;
    }

    public void setTxtPathFile(Textbox txtPathFile) {
        this.txtPathFile = txtPathFile;
    }

    public Textbox getTxtCode() {
        return txtCode;
    }

    public void setTxtCode(Textbox txtCode) {
        this.txtCode = txtCode;
    }

    public Textbox getTxtParentCode() {
        return txtParentCode;
    }

    public void setTxtParentCode(Textbox txtParentCode) {
        this.txtParentCode = txtParentCode;
    }

    public Button getBtnSavePopup() {
        return btnSavePopup;
    }

    public void setBtnSavePopup(Button btnSavePopup) {
        this.btnSavePopup = btnSavePopup;
    }

    public Button getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(Button btnEdit) {
        this.btnEdit = btnEdit;
    }

    public Button getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(Button btnDelete) {
        this.btnDelete = btnDelete;
    }

    public Button getBtnReset() {
        return btnReset;
    }

    public void setBtnReset(Button btnReset) {
        this.btnReset = btnReset;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public Groupbox getGbUploadComponent() {
        return gbUploadComponent;
    }

    public void setGbUploadComponent(Groupbox gbUploadComponent) {
        this.gbUploadComponent = gbUploadComponent;
    }

    public List<KeyValueBean> getLstServiceName() {
        return lstServiceName;
    }

    public void setLstServiceName(List<KeyValueBean> lstServiceName) {
        this.lstServiceName = lstServiceName;
    }

    public ListModelList<KeyValueBean> getKvbServiceName() {
        return kvbServiceName;
    }

    public void setKvbServiceName(ListModelList<KeyValueBean> kvbServiceName) {
        this.kvbServiceName = kvbServiceName;
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp); //To change body of generated methods, choose Tools | Templates.
        if (arg.containsKey("SERVICE_CODE") && StringUtils.isValidString(String.valueOf(arg.get("SERVICE_CODE")))) {
            serviceCode = (String) arg.get("SERVICE_CODE");
        }
        if (arg.containsKey(ParamUtils.ACTION)) {
            action = arg.get(ParamUtils.ACTION).toString();
        }
        if (arg.containsKey(ParamUtils.COMPOSER)) {
            serviceComposer = (ServiceComposer) arg.get(ParamUtils.COMPOSER);
        }
        setData();
        uploadOneFileComponent.setUploaderCallback(this);

    }

    public void loadComboboxParentCode(Combobox id) throws Exception {
        lstDataParentCode = new ArrayList<KeyValueBean>();
        lstDataParentCode.addAll(serviceService.listParrentCode());
        listParentCode = new ListModelList<KeyValueBean>();
        listParentCode.addAll(lstDataParentCode);
        if (listParentCode != null && listParentCode.size() > 0) {
            listParentCode.addToSelection(listParentCode.getElementAt(0));
        }
        id.setModel(listParentCode);
        id.setValue(LanguageBundleUtils.getString("global.combobox.choose"));
    }

    public void setData() throws Exception {
        if (ParamUtils.ACTION_CREATE.equals(action)) {
            ((Window) self).setTitle(LanguageBundleUtils.getString("global.add"));
            txtCode.setValue(null);
            txtName.setValue("");
            loadComboboxServiceCode(ParamUtils.DEFAULT_VALUE_STR);
            labelPath.setVisible(false);
            labelStart.setVisible(false);
            cbbIdParentCode.setValue(null);
            txtPathFile.setVisible(false);
            btnEdit.setVisible(false);
        } else if (ParamUtils.ACTION_UPDATE.equals(action)) {
            ServiceDTO serviceDTO = new ServiceDTO();
            serviceDTO.setCode(serviceCode);
            ServiceEntity serviceEntity = serviceService.getServiceEntityByCode(serviceCode);
            serviceDTO.setName(serviceEntity.getName());
            serviceDTO.setParentCode(serviceEntity.getParentCode());
            serviceDTO.setPathFile(serviceEntity.getPathFile());
            ((Window) self).setTitle(LanguageBundleUtils.getString("global.edit"));
            if (serviceDTO != null) {
                txtCode.setValue(serviceDTO.getCode());
                txtName.setValue(serviceDTO.getName());

                if (serviceDTO.getParentCode() != null) {
                    loadComboboxServiceCode(serviceDTO.getParentCode().toString());
                } else {
                    cbbIdParentCode.setValue(null);
//                    loadComboboxServiceCode(ParamUtils.DEFAULT_VALUE_STR);
                }

                txtPathFile.setValue(serviceDTO.getPathFile());
                txtCode.setDisabled(true);
                txtPathFile.setDisabled(true);
                btnSavePopup.setVisible(false);
            }
        }
    }

    public boolean validate() {
        if (!ctrlValidator.checkValidTextbox(txtCode, true, true, LanguageBundleUtils.getString("service.code"))) {
            return false;
        }

        if (!ctrlValidator.checkValidTextbox(txtName, true, true, LanguageBundleUtils.getString("service.name"))) {
            return false;
        }
        if (!ctrlValidator.checkValidComboValue(cbbIdParentCode, false, true, LanguageBundleUtils.getString("service.parentCode"))) {
            return false;
        }
        return true;
    }

    public ServiceDTO getDataFromClient() {
        ServiceDTO dto = new ServiceDTO();

        if (StringUtils.isValidString(txtCode.getValue().toString())) {
            dto.setCode(txtCode.getValue().toString());
        }
        if (StringUtils.isValidString(txtName.getValue())) {
            dto.setName(txtName.getValue().trim());
        }
        if (StringUtils.isValidString(txtPathFile.getValue())) {
            dto.setPathFile(txtPathFile.getValue().trim());
        }
        if (cbbIdParentCode.getSelectedIndex() > 0) {
            dto.setParentCode(cbbIdParentCode.getSelectedItem().getValue());
        }
        return dto;
    }

    public void onClick$btnEdit() {
        try {
            if (validate()) {
                ServiceDTO serviceDTO = getDataFromClient();
                this.uploadOneFileComponent.onClick$doSave();
                uploadedFilesInfoCallback(filesInfo);
                List<ServiceFileDTO> fileDTOS = new ArrayList<>();
                if (filesInfo != null) {
                    for (int i = 0; i < filesInfo.size(); i++) {
                        ServiceFileDTO filesDTO = getDataFromClientServiceFileDTO(filesInfo.get(i));
                        fileDTOS.add(filesDTO);
                        serviceDTO.setPathFile(fileDTOS.get(i).getPathFile());
                    }
                }
                serviceService.updateService(serviceDTO);
                Clients.showNotification(LanguageBundleUtils.getMessage("global.message.update.successful", LanguageBundleUtils.getString("global.edit")), "info", null, "middle_center", 3000);
                self.detach();
                serviceComposer.bindDataToGrid();
            } else {
                Clients.showNotification(LanguageBundleUtils.getString("global.choose.action"), "warning", null, "middle_center", 3000);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    public void onClick$btnSavePopup() {
        try {
            if (validate()) {
                this.uploadOneFileComponent.onClick$doSave();
                if (ParamUtils.ACTION_CREATE.equals(action)) {
                    ServiceDTO serviceDTO = getDataFromClient();
                    uploadedFilesInfoCallback(filesInfo);
                    List<ServiceFileDTO> fileDTOS = new ArrayList<>();
                    if (filesInfo != null) {
                        for (int i = 0; i < filesInfo.size(); i++) {
                            ServiceFileDTO filesDTO = getDataFromClientServiceFileDTO(filesInfo.get(i));
                            fileDTOS.add(filesDTO);
                            serviceDTO.setPathFile(fileDTOS.get(i).getPathFile());
                        }
                    }

                    if (serviceService.insert(serviceDTO)) {
                        Clients.showNotification(LanguageBundleUtils.getString("reflect.message.sucess"), "info", null, "middle_center", 1000);
                    } else {
                        Clients.showNotification(LanguageBundleUtils.getString("service.code.error.duplicate"), "warning", null, "middle_center", 3000);
                    }
                }
                self.detach();
                serviceComposer.bindDataToGrid();
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    public void onClick$btnDelete() throws Exception {
        serviceComposer.bindDataToGrid();
        self.detach();
    }

    public void loadComboboxServiceCode(String valueSelect) throws Exception {
        try {
            List<ServiceDTO> listServiceDTO = serviceService.findAll();
            lstServiceName = new ArrayList<>();
            lstServiceName.add(new KeyValueBean(ParamUtils.DEFAULT_VALUE_STR, LanguageBundleUtils.getString("global.combobox.choose")));
            if (listServiceDTO != null && listServiceDTO.size() > 0) {
                for (ServiceDTO serviceDTO : listServiceDTO) {
                    if (serviceDTO.getParentCode() == null) {
                        KeyValueBean key = new KeyValueBean();
                        key.setKey(serviceDTO.getCode());
                        key.setValue(serviceDTO.getName());
                        lstServiceName.add(key);
                    }
                }
            }
            kvbServiceName = new ListModelList(lstServiceName);
            int i = 0;
            for (KeyValueBean itemParam : kvbServiceName.getInnerList()) {
                if (itemParam.getKey() != null && String.valueOf(itemParam.getKey()).equals(valueSelect)) {
                    kvbServiceName.addToSelection(kvbServiceName.getElementAt(i));
                    break;
                }
                i++;
            }
            cbbIdParentCode.setModel(kvbServiceName);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    @Override
    public void uploadedFilesInfoCallback(List<FileInfo> filesInfo) {
        this.filesInfo = filesInfo;
        System.out.println(filesInfo);
    }

    @Override
    public void uploadedFilesLocationCallback(List<String> filesLocation) {
        this.filesLocation = filesLocation;
    }

    public ServiceFileDTO getDataFromClientServiceFileDTO(FileInfo fileInfo) {
        ServiceFileDTO serviceFilesDTO = new ServiceFileDTO();
        serviceFilesDTO.setFileName(new File(fileInfo.getLocation()).getName());
        serviceFilesDTO.setPathFile(new File(fileInfo.getLocation()).getPath());
        serviceFilesDTO.setDescription(fileInfo.getDescription());
        return serviceFilesDTO;
    }

    public void loadComboboxService(String valueSelect) throws Exception {
        try {
            List<ServiceDTO> listParentDTO = serviceService.getListWithNullParent();
            listParents = new ArrayList<>();
            listParents.add(new KeyValueBean(ParamUtils.DEFAULT_VALUE_STR, LanguageBundleUtils.getString("global.combobox.choose")));
            if (listParents != null && listParents.size() > 0) {
                for (ServiceDTO serviceDTO : listParentDTO) {
                    KeyValueBean key = new KeyValueBean();
                    key.setKey(serviceDTO.getCode());
                    key.setValue(serviceDTO.getName());
                    listParents.add(key);
                }
            }
            kvbServiceName = new ListModelList(listParents);
            int i = 0;
            for (KeyValueBean itemParam : kvbServiceName.getInnerList()) {
                if (itemParam.getKey() != null && String.valueOf(itemParam.getKey()).equals(valueSelect)) {
                    kvbServiceName.addToSelection(kvbServiceName.getElementAt(i));
                    break;
                }
                i++;
            }
            cbbIdParentCode.setModel(kvbServiceName);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }
}
