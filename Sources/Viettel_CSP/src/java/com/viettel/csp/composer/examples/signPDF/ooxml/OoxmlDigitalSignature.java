/*
 * Copyright (C) 2010 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.viettel.csp.composer.examples.signPDF.ooxml;

import be.fedict.eid.applet.service.signer.TemporaryDataStorage;
import be.fedict.eid.applet.service.signer.ooxml.OOXMLProvider;
import be.fedict.eid.applet.service.spi.DigestInfo;
import com.viettel.csp.composer.examples.signPDF.DigitalSignatureException;
import com.viettel.csp.composer.examples.signPDF.SignatureObject;
import com.viettel.csp.composer.examples.signPDF.SignatureParameter;
import java.io.OutputStream;
import java.net.URL;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.Cipher;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.xml.security.exceptions.Base64DecodingException;

/**
 * Van digital signature for OOXML format
 *
 * @author quanghx2@viettel.com.vn
 * @since 21-10-2010
 * @version 1.0
 */
public class OoxmlDigitalSignature {

    Logger logger = Logger.getLogger(OoxmlDigitalSignature.class.getName());
    /**
     * OOXML specification on XML Digital Signature\
     */
    public static final byte[] SHA1_DIGEST_INFO_PREFIX = new byte[]{
        0x30, 0x1f, 0x30, 0x07, 0x06, 0x05, 0x2b, 0x0e, 0x03, 0x02, 0x1a, 0x04, 0x14};
    /**
     * Class initialed
     */
    private static boolean initialed = false;
    /**
     * PrivateKey for signing
     */
    private PrivateKey privateKey;
    /**
     * To get certificate signed file
     */
    private X509Certificate certificate;
    /**
     * chain
     */
    private List<X509Certificate> certificates;
    /*
     * service for client server sign
     */
    private OoxmlSignatureService service;

    public OoxmlDigitalSignature() {
    }

    /**
     * main constructor
     *
     * @param privateKey key to sign
     * @param certificates chain associates with key
     */
    public OoxmlDigitalSignature(PrivateKey privateKey, Certificate[] certificates) {
        this.privateKey = privateKey;
        this.certificates = new ArrayList<X509Certificate>();
        for (Certificate cert : certificates) {
            X509Certificate xc = (X509Certificate) cert;
            this.certificates.add(xc);
        }
    }

    /**
     * Must call initial method at least one time.
     */
    public static void initial() {
        if (!initialed) {
            OOXMLProvider.install();
            initialed = true;
        }
    }

    /**
     * signing method
     *
     * @param ooxmlUrl URL of file
     * @param os return OS
     * @param parameter sign parameter
     * @throws Exception if error
     */
    public void sign(URL ooxmlUrl, OutputStream os, SignatureParameter parameter) throws Exception {
        if (null == ooxmlUrl || ooxmlUrl.equals("")) {
            throw new NullPointerException("Input ooxmlUrl is not null");
        }

        if (null == os) {
            throw new NullPointerException("Output stream is not null");
        }

        if (!OoxmlSignatureVerifier.isOOXML(ooxmlUrl)) {
            throw new DigitalSignatureException("File type is not OOXML");
        }

        OoxmlSignatureService signatureService;
        if (parameter != null) {
            signatureService = new OoxmlSignatureService(ooxmlUrl, parameter.getComment());

        } else {
            signatureService = new OoxmlSignatureService(ooxmlUrl, "");
        }
        TemporaryDataStorage temporaryDataStorage = signatureService.getTemporaryDataStorage();

        try {
            /* Caculate document digest*/
            DigestInfo digestInfo = signatureService.preSign(null, this.certificates);

            /* Store preSing result*/
            String preSignResult = IOUtils.toString(temporaryDataStorage.getTempInputStream());


            /* Caculate document signature*/
            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(Cipher.ENCRYPT_MODE, privateKey);

            byte[] digestInfoValue = ArrayUtils.addAll(SHA1_DIGEST_INFO_PREFIX, digestInfo.digestValue);

            byte[] signatureValue = cipher.doFinal(digestInfoValue);
            //signatureService.postSign(signatureValue, Collections.singletonList(certificate));
            signatureService.postSign(signatureValue, this.certificates);

            /* Write signed OOXMLData to output stream*/
            byte[] signedOOXMLData = signatureService.getSignedOfficeOpenXMLDocumentData();
            os.write(signedOOXMLData);
        } catch (Exception ex) {
            os = null;
            logger.log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    protected SignatureObject createDigest(URL fileURL) throws Exception {
        service = new OoxmlSignatureService(fileURL, null);
        DigestInfo digestInfo = service.preSign(null, null);
        SignatureObject object = new SignatureObject();
        object.setOOXML(true);
        object.setDigest(digestInfo.digestValue);
        return object;
    }

    protected byte[] insertSignature(SignatureObject object) throws Base64DecodingException {
        List<X509Certificate> chain = new ArrayList<X509Certificate>();
//        Certificate[] chains = object.getCertificateChain();
//        for (Certificate cert : chains) {
//            X509Certificate xc = (X509Certificate) cert;
//            chain.add(xc);
//        }
        //------- TuanNM33 : moi bo sung 25/12/2013
        List<String> lst = object.getCertificateChain();
        for (String st : lst) {
            Certificate ce = null; //= CertificateUtils.GetX509Cert(st);
            X509Certificate xc = (X509Certificate) ce;
            chain.add(xc);
        }
        //-------
        byte[] signature = object.getSignature();
        if (signature == null) {
            throw new NullPointerException("signature is not null");
        }
        service.postSign(signature, chain);
        return service.getSignedOfficeOpenXMLDocumentData();
    }
}
