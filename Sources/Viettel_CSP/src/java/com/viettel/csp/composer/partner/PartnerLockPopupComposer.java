/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.composer.partner;

import com.viettel.csp.DTO.ContractDTO;
import com.viettel.csp.DTO.PartnerDTO;
import com.viettel.csp.DTO.PartnerLockDTO;
import com.viettel.csp.DTO.PartnerLockFilesDTO;
import com.viettel.csp.service.ContractService;
import com.viettel.csp.service.MessageService;
import com.viettel.csp.service.PartnerLockFilesService;
import com.viettel.csp.service.PartnerLockService;
import com.viettel.csp.service.ReasonService;
import com.viettel.csp.util.*;
import com.viettel.eafs.util.SpringUtil;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Calendar;
import java.util.logging.Level;

import com.viettel.plc.widget.FileInfo;
import com.viettel.plc.widget.FileViews;
import com.viettel.plc.widget.IUploaderCallback;
import com.viettel.plc.widget.UploadComponent;
import org.apache.log4j.Logger;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.*;

/**
 *
 * @author GEM
 */


public class PartnerLockPopupComposer extends GenericForwardComposer<Component> implements IUploaderCallback {

    private String action;
    private PartnerPopupComposer partnerPopupComposer;
    private PartnerDTO objectPartner;
    private Datebox dbStartDate;
    private Datebox dbEndDate;
    private Combobox cbbReason;
    private Textbox txtContent;
    private Button bntLockOpen;
    private Button bntBack;
    private PartnerLockDTO partnerLockDTO;
    private GenComponent component = new GenComponent();
    private ListModelList<KeyValueBean> listReason = new ListModelList<KeyValueBean>();
    private static final Logger logger = Logger.getLogger(PartnerComposer.class);
    private MessageService messageService = SpringUtil.getBean("messageService", MessageService.class);
    private PartnerLockService partnerLockService = SpringUtil.getBean("partnerLockService", PartnerLockService.class);
    private PartnerLockFilesService partnerLockFilesService = SpringUtil.getBean("partnerLockFilesService", PartnerLockFilesService.class);
    private ContractService contractService = SpringUtil.getBean("contractService", ContractService.class);
    private ReasonService reasonService = SpringUtil.getBean("reasonService", ReasonService.class);
    private Tab tabAttachment;
    private List<ContractDTO> lstResult;
    private long partnerID;
    private Button btnOpen;
    private UploadComponent uploadComponent;
    private List<String> filesLocation;
    private Div divUpload;
    private List<FileInfo> filesInfo;
    private boolean isLock;

    public ListModelList<KeyValueBean> getListReason() {
        return listReason;
    }

    public void setListReason(ListModelList<KeyValueBean> listReason) {
        this.listReason = listReason;
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp); //To change body of generated methods, choose Tools | Templates.
        if (arg.containsKey(ParamUtils.ACTION)) {
            action = arg.get(ParamUtils.ACTION).toString();
        }
        if (arg.containsKey(ParamUtils.COMPOSER)) {
            partnerPopupComposer = (PartnerPopupComposer) arg.get(ParamUtils.COMPOSER);
        }
        if (arg.containsKey("PARNER_OBJECT")) {
            objectPartner = (PartnerDTO) arg.get("PARNER_OBJECT");
        }
        partnerID = (long) Sessions.getCurrent().getAttribute("partnerID");
        uploadComponent.setUploaderCallback(this);
        bindData(objectPartner);
        getContractList();
        setComponentStatus();

    }

    private Listbox lbxContract;

    public Listbox getLbxContract() {
        return lbxContract;
    }

    public void setLbxContract(Listbox lbxContract) {
        this.lbxContract = lbxContract;
    }

    public void setComponentStatus(){
        if (!isLock){
            tabAttachment.setVisible(false);
            tabAttachment.setDisabled(true);
            tabAttachment.setClosable(true);

            bntLockOpen.setDisabled(false);
            bntLockOpen.setVisible(true);
            btnOpen.setVisible(false);
            btnOpen.setDisabled(true);

            dbEndDate.setDisabled(false);
            dbStartDate.setDisabled(false);
            txtContent.setDisabled(false);
            cbbReason.setDisabled(false);

            divUpload.setVisible(true);
        }else {
            tabAttachment.setVisible(true);
            tabAttachment.setDisabled(false);
            tabAttachment.setClosable(false);

            btnOpen.setVisible(true);
            btnOpen.setDisabled(false);
            bntLockOpen.setVisible(false);
            bntLockOpen.setDisabled(true);

            dbStartDate.setDisabled(true);
            dbEndDate.setDisabled(true);
            txtContent.setDisabled(true);
            cbbReason.setDisabled(true);

            divUpload.setVisible(false);
        }
    }

    private void getContractList(){
        ContractDTO contractDTO = new ContractDTO();
        partnerID = (long) Sessions.getCurrent().getAttribute("partnerID");
        try {
            lstResult = contractService.getListContractForPartnerLock(contractDTO);
            if (lstResult.get(0).getIsLock().equals("1")){
                isLock = true;
            }else {
                isLock = false;
            }
            ListModelList model = new ListModelList(lstResult);
            model.setMultiple(true);
            lbxContract.setModel(model);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(PartnerLockPopupComposer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String formatDateTime(Date date) throws Exception {
        return StringUtils.formatDateTime(date);
    }


    public void onClick$bntLockOpen() throws ParseException {
        if (!validate()){
            return;
        }
        //upload file
        uploadComponent.onClick$doSave();
        saveFileInfo();
        Sessions.getCurrent().removeAttribute("upload");
        //Lưu xuong DB
        setDataToDTO();
        //Lưu vào trong bảng contract vs trường is_lock : 1  | 0 : active
        contractService.updateContractLock(lstResult, 1L);
        isLock = true;
        setComponentStatus();
        Clients.showNotification(LanguageBundleUtils.getString("global.message.save.successful"), "OK", null, "middle_center", 3000);
    }



    private void setDataToDTO(){
        boolean flag = false;

        PartnerLockDTO partnerLockDTO = partnerLockService.findByPartnerId(partnerID);
        //check ton tai trong db
        if (partnerLockDTO == null) {
            partnerLockDTO = new PartnerLockDTO();
            partnerLockDTO.setCreateAt(new Date());
            partnerLockDTO.setCreateBy(UserTokenUtils.getUserId());
            partnerLockDTO.setPartnerId(partnerID);
            flag = true;
        }
        //Khong ton tai trong db
        partnerLockDTO.setContent(txtContent.getValue());
        partnerLockDTO.setEndDate(dbEndDate.getValue());
        partnerLockDTO.setReasonCode(cbbReason.getValue());
        partnerLockDTO.setUpdateBy(UserTokenUtils.getUserId());
        partnerLockDTO.setStartDate(dbStartDate.getValue());
        partnerLockDTO.setUpdateAt(new Date());

        if (flag){
            partnerLockService.savePartnerLock(partnerLockDTO, ParamUtils.ACTION_PARTNER_LOCK);
        }else {
            partnerLockService.update(partnerLockDTO);
        }
    }

    public void saveFileInfo(){
        if (filesInfo == null)
            return;

        List<PartnerLockFilesDTO> fileList = new ArrayList<>();
        for (FileInfo file:filesInfo){
            PartnerLockFilesDTO filesDTO = new PartnerLockFilesDTO();
            filesDTO.setFileName(getFileName(file.getLocation()));
            filesDTO.setDescription(file.getDescription());
            filesDTO.setPartnerLockId(partnerID);
            filesDTO.setPathFile(file.getLocation());
            filesDTO.setUploadDate(new Date());
            fileList.add(filesDTO);
        }

        try {
            partnerLockFilesService.insertList(fileList);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    private String getFileName(String location){
        File file = new File(location);
        return file.getName();
    }

    private Listbox lbxListFile;

    public Listbox getLbxListFile() {
        return lbxListFile;
    }

    public void setLbxListFile(Listbox lbxListFile) {
        this.lbxListFile = lbxListFile;
    }

    public void onClick$tabAttachment(){
        List<PartnerLockFilesDTO> filesDTOs = new ArrayList<>();
        try {
            filesDTOs = partnerLockFilesService.findByPartnerLockId(partnerID);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        ListModelList model = new ListModelList(filesDTOs);
        model.setMultiple(true);
        lbxListFile.setModel(model);
    }

    public void onClick$btnOpen(){
        contractService.updateContractLock(lstResult, 0L);
        isLock = false;
        setComponentStatus();
    }



    @Override
    public void uploadedFilesInfoCallback(List<FileInfo> filesInfo) {
        this.filesInfo = filesInfo;
        System.out.println(filesInfo);
    }
    private void bindData(PartnerDTO objectPartner) throws Exception {
        if (ParamUtils.ACTION_PARTNER_LOCK.equals(action)) {
            ((Window) self).setTitle(LanguageBundleUtils.getString("partner.lock.dialog.lock.tooltip.info") + ": " + objectPartner.getCompanyName());
            Calendar cal = Calendar.getInstance();
            dbStartDate.setValue(cal.getTime());
            cal.add(Calendar.DATE, ParamUtils.START_END_DATE_CONFIG);
            dbStartDate.setValue(cal.getTime());
            component.bindDataCbbContactType(cbbReason, reasonService.getListToKeyValueBeanByType(ParamUtils.REASON_TYPE.LOCK_PARTNER), null);
            txtContent.setValue(ParamUtils.STRING_EMPTY);

        } else if (ParamUtils.ACTION_PARTNER_OPEN.equals(action)) {
            ((Window) self).setTitle(LanguageBundleUtils.getString("partner.lock.dialog.open.tooltip.info") + ": " + objectPartner.getCompanyName());
            partnerLockDTO = partnerLockService.findByPartnerId(objectPartner.getId());
            if (partnerLockDTO != null) {
                dbStartDate.setValue(partnerLockDTO.getStartDate());
                dbStartDate.setValue(partnerLockDTO.getEndDate());
                component.bindDataCbbContactType(cbbReason, reasonService.getListToKeyValueBeanByType(ParamUtils.REASON_TYPE.LOCK_PARTNER), partnerLockDTO.getReasonCode());
                txtContent.setValue(partnerLockDTO.getContent());
            } else {
                Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
            }
        }
    }

    private PartnerLockDTO getDataFromClient() {
        partnerLockDTO.setStartDate(dbStartDate.getValue());
        partnerLockDTO.setEndDate(dbEndDate.getValue());
        if (cbbReason.getSelectedItem() != null && cbbReason.getSelectedItem().getValue() != null) {
            partnerLockDTO.setReasonCode(cbbReason.getSelectedItem().getValue());
        }
        partnerLockDTO.setContent(txtContent.getValue());

        return partnerLockDTO;
    }

    private boolean validate() {
        if(txtContent.getValue().equals("") || dbStartDate.getValue().equals("") || dbEndDate.getValue().equals("") || cbbReason.getSelectedItem().getValue().equals("-99")){
            Clients.showNotification(LanguageBundleUtils.getString("global.message.noData"), "warning", null, "middle_center", 3000);
            return false;
        }

        return true;
    }

    public void onClick$btnSavePopup() {
        try {
            String message = "";
            if (validate()) {
                partnerLockDTO = getDataFromClient();
                message = partnerLockService.savePartnerLock(partnerLockDTO, action);
                if (ParamUtils.SUCCESS.equals(message)) {
                    partnerPopupComposer.bindData(objectPartner);
                    Clients.showNotification(LanguageBundleUtils.getString("global.message.save.successful"), "info", null, "middle_center", 1000);
                    self.detach();
                    return;
                } else {
                    Clients.showNotification(message, "warning", null, "middle_center", 3000);
                    return;
                }
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    public Datebox getDbStartDate() {
        return dbStartDate;
    }

    public void setDbStartDate(Datebox dbStartDate) {
        this.dbStartDate = dbStartDate;
    }

    public Datebox getDbEndDate() {
        return dbEndDate;
    }

    public void setDbEndDate(Datebox dbEndDate) {
        this.dbEndDate = dbEndDate;
    }

    public Combobox getCbbReason() {
        return cbbReason;
    }

    public void setCbbReason(Combobox cbbReason) {
        this.cbbReason = cbbReason;
    }

    public Textbox getTxtContent() {
        return txtContent;
    }

    public void setTxtContent(Textbox txtContent) {
        this.txtContent = txtContent;
    }

    @Override
    public void uploadedFilesLocationCallback(List<String> filesLocation) {
        this.filesLocation = filesLocation;
        System.out.println(filesLocation);
    }
}
