/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.composer.contract;

import com.viettel.csp.DTO.ContractDTO;
import com.viettel.csp.DTO.ContractHisDTO;
import com.viettel.csp.DTO.PartnerDTO;
import com.viettel.csp.DTO.ReasonDTO;
import com.viettel.csp.composer.contractManagement.ContractManagerPopupEditEffectingContractComposer;
import com.viettel.csp.service.*;
import com.viettel.csp.service.ContractHisService;
import com.viettel.csp.service.ContractService;
import com.viettel.csp.service.ContractStatusService;
import com.viettel.csp.service.PartnerService;
import com.viettel.csp.service.ReasonService;
import com.viettel.csp.util.KeyValueBean;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.ParamUtils;
import com.viettel.eafs.util.SpringUtil;
import java.util.ArrayList;
import java.util.List;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Textbox;

/**
 * @author chitv@gemvietnam.com
 */
public class EffectingContractPopupReasonComposer extends GenericForwardComposer<Component> {

    final static org.apache.log4j.Logger logger = org.apache.log4j.Logger
        .getLogger(EffectingContractPopupReasonComposer.class);
    private Label txtCompanyName, txtAddressHeadOffice, txtPartnerCode, txtAddressTradingOffice, txtCompanyNameShort, txtPhone, txtRepName, dbFilingDate, txtContractStatus;
    private Combobox cbbContractStatus;
    private Textbox txtContenReason;
    private Combobox cbbReason;
    private ListModelList<KeyValueBean> kvbContractStatus;
    private ListModelList<KeyValueBean> kvbReason;
    private List<KeyValueBean> lstContractStatus;
    private List<KeyValueBean> lstReason;
    private String action;
    private Long contractId;
    private ArrayList indexLst;
    private ContractDTO oldDTO;
    private PartnerDTO PartDTO;
    private ContractHisDTO contractHisDTO;
    private ContractService contractService = SpringUtil
        .getBean("contractService", ContractService.class);
    private PartnerService partnerService = SpringUtil
        .getBean("partnerService", PartnerService.class);
    private ContractStatusService contractStatusService = SpringUtil
        .getBean("contractStatusService", ContractStatusService.class);
    private ContractHisService contractHisService = SpringUtil
        .getBean("contractHisService", ContractHisService.class);
    private ReasonService reasonService = SpringUtil.getBean("reasonService", ReasonService.class);
    private ContractManagerPopupEditEffectingContractComposer contractPopupEditComposer;
//    private Datebox dbFilingDate;

    public ListModelList<KeyValueBean> getKvbContractStatus() {
        return this.kvbContractStatus;
    }

    public void setKvbContractStatus(ListModelList<KeyValueBean> kvbContractStatus) {
        this.kvbContractStatus = kvbContractStatus;
    }

    public List<KeyValueBean> getLstContractStatus() {
        return this.lstContractStatus;
    }

    public void setLstContractStatus(List<KeyValueBean> lstContractStatus) {
        this.lstContractStatus = lstContractStatus;
    }

    public Combobox getCbbReason() {
        return this.cbbReason;
    }

    public void setCbbReason(Combobox cbbReason) {
        this.cbbReason = cbbReason;
    }

    public ListModelList<KeyValueBean> getKvbReason() {
        return this.kvbReason;
    }

    public void setKvbReason(ListModelList<KeyValueBean> kvbReason) {
        this.kvbReason = kvbReason;
    }

    public List<KeyValueBean> getLstReason() {
        return this.lstReason;
    }

    public void setLstReason(List<KeyValueBean> lstReason) {
        this.lstReason = lstReason;
    }

    @Override
    public void doAfterCompose(Component comp) {
        try {
            super.doAfterCompose(comp);
            if (this.arg.containsKey(ParamUtils.COMPOSER)) {
                this.contractPopupEditComposer = (ContractManagerPopupEditEffectingContractComposer) this.arg
                    .get(ParamUtils.COMPOSER);
            }
            
            bindDataCbbContractStatus(ParamUtils.DEFAULT_VALUE_STR);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            Clients
                .showNotification(LanguageBundleUtils.getString("global.message.error"), "warning",
                    null, "middle_center", 3000);
        }
    }

    public void bindDataCbbContractStatus(String valueSelect) throws Exception {
        try {
            List<ReasonDTO> lstReasonDTO = this.reasonService
                .findListByType(ParamUtils.REASON_TYPE.CONTRACT_PROFILE);
            this.lstReason = new ArrayList<>();
            this.lstReason.add(new KeyValueBean(ParamUtils.DEFAULT_VALUE_STR,
                LanguageBundleUtils.getString("global.combobox.choose")));
            if (lstReasonDTO != null && lstReasonDTO.size() > 0) {
                for (ReasonDTO reasonDTO : lstReasonDTO) {
                    KeyValueBean key = new KeyValueBean();
                    key.setKey(reasonDTO.getCode());
                    key.setValue(reasonDTO.getName());
                    this.lstReason.add(key);
                }
            }
            this.kvbReason = new ListModelList(this.lstReason);
            int i = 0;
            for (KeyValueBean itemParam : this.kvbReason.getInnerList()) {
                if (itemParam.getKey() != null && String.valueOf(itemParam.getKey())
                    .equals(valueSelect)) {
                    this.kvbReason.addToSelection(this.kvbReason.getElementAt(i));
                    break;
                }
                i++;
            }
            this.cbbReason.setModel(this.kvbReason);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            Clients
                .showNotification(LanguageBundleUtils.getString("global.message.error"), "warning",
                    null, "middle_center", 3000);
        }
    }

    public void onClick$btnSendReason() {
        Events.postEvent(new Event("onSelectedReason", this.self.getParent(),
            this.txtContenReason.getValue() + "-" + this.cbbReason.getSelectedItem().getLabel()));
        this.self.detach();
    }


    public void onClick$btnClose() {
        Events.postEvent(new Event("onClosePopup", this.self.getParent()));
        this.self.detach();
    }

}
