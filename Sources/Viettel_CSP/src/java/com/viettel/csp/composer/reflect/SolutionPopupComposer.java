/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.composer.reflect;

import com.viettel.csp.DTO.*;
import com.viettel.csp.composer.contract.ContractComposer;
import com.viettel.csp.service.*;
import com.viettel.csp.util.CtrlValidator;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.ParamUtils;
import com.viettel.eafs.util.SpringUtil;
import com.viettel.plc.widget.FileViews;
import com.viettel.plc.widget.FileInfo;
import com.viettel.plc.widget.IUploaderCallback;
import com.viettel.plc.widget.UploadComponent;

import java.util.*;
import java.util.logging.Level;

import static jxl.biff.BaseCellFeatures.logger;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.ForwardEvent;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.*;

/**
 * @author TienNV@gemvietnam.com
 */
public class SolutionPopupComposer extends GenericForwardComposer<Component> implements IUploaderCallback {

    private String action;
    private Long reflectId;
    private Long solutionId;
    private Datebox dbRequiredReponseDate;
    private Datebox dbReponseDate;
    private Datebox dbRequiredDate;
    private Combobox cbbSolutionStatus;
    private Row rowInfo;
    private Button btnSendSolution;
    private Button btnclosePopup;
    private Textbox txtContent;
    private SolutionDTO oldDto;
    private ReflectDTO reflectDTO;
    private UploadComponent uploadComponent;
    private Groupbox gbListFile;
    private Listbox lbxListFile;
    private List<String> filesLocation;
    private List<FileInfo> filesInfo;
    private ReflectPopupComposer reflectPopupComposer;
    private ReflectService reflectService = SpringUtil.getBean("reflectService", ReflectService.class);
    private SolutionService solutionService = SpringUtil.getBean("solutionService", SolutionService.class);
    private ReasonService reasonService = SpringUtil.getBean("reasonService", ReasonService.class);
    private ServiceService serviceService = SpringUtil.getBean("serviceService", ServiceService.class);
    private PartnerService partnerService = SpringUtil.getBean("partnerService", PartnerService.class);
    private SolutionFilesService solutionFilesService = SpringUtil.getBean("solutionFilesService", SolutionFilesService.class);

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        uploadComponent.setUploaderCallback(this);
        if (arg.containsKey(ParamUtils.ACTION)) {
            action = arg.get(ParamUtils.ACTION).toString();
        }
        if (arg.containsKey("REFLECT_ID")) {
            reflectId = (Long) arg.get("REFLECT_ID");
            reflectDTO = reflectService.getObjectDTOById(reflectId);
        }
        if (arg.containsKey("SOLUTION_ID")) {
            solutionId = (Long) arg.get("SOLUTION_ID");
            oldDto = solutionService.getById(solutionId);
        }
        if (arg.containsKey(ParamUtils.COMPOSER)) {
            reflectPopupComposer = (ReflectPopupComposer) arg.get(ParamUtils.COMPOSER);
        }
        setData();
    }

    private void setData() throws Exception {
        if (ParamUtils.ACTION_CREATE.equals(action)) {
            ((Window) self).setTitle(LanguageBundleUtils.getString("solution.dialog.tooltip.info"));
            dbReponseDate.setDisabled(true);
            dbRequiredDate.setDisabled(true);
            cbbSolutionStatus.setDisabled(true);
            rowInfo.setVisible(false);
            cbbSolutionStatus.setValue("");
            txtContent.setValue("");
            gbListFile.setVisible(false);
        } else if (ParamUtils.ACTION_VIEW.equals(action)) {
            ((Window) self).setTitle(LanguageBundleUtils.getString("solution.dialog.tooltip.info"));
            if (oldDto != null) {
                dbReponseDate.setValue(oldDto.getReponseDate());
                dbRequiredDate.setValue(oldDto.getRequestDate());
                String statusName=oldDto.getSolutionStatus();
                cbbSolutionStatus.setValue(statusName);
                switch (statusName){
                    case ParamUtils.SOLUTION_STATUS.OK:
                        cbbSolutionStatus.setValue(LanguageBundleUtils.getString("reflect.solution.ok"));
                        break;
                    case ParamUtils.SOLUTION_STATUS.NOK:
                        cbbSolutionStatus.setValue(LanguageBundleUtils.getString("reflect.solution.nok"));
                        break;
                    case ParamUtils.SOLUTION_STATUS.MORE_INFO:
                        cbbSolutionStatus.setValue(LanguageBundleUtils.getString("reflect.solution.moreinfo"));
                        break;
                }
                txtContent.setValue(oldDto.getContent());

                dbReponseDate.setDisabled(true);
                dbRequiredDate.setDisabled(true);
                cbbSolutionStatus.setDisabled(true);
                txtContent.setDisabled(true);
                btnSendSolution.setVisible(false);

                uploadComponent.setVisible(false);

                SolutionFilesDTO solutionFilesDTO = new SolutionFilesDTO();
                solutionFilesDTO.setSolutionId(solutionId);
                List<SolutionFilesDTO> solutionFilesDTOs = solutionFilesService.getListSolutionFiles(solutionFilesDTO);
                ListModelList model = null;
                if (solutionFilesDTOs != null && solutionFilesDTOs.size() > 0) {
                    model = new ListModelList(solutionFilesDTOs);
                } else {
                    model = new ListModelList<>();
                }
                lbxListFile.setModel(model);
            }
        }
    }

    public void onClick$btnclosePopup() {
        self.detach();
    }

    public void onClick$btnSendSolution() {
        try {
            String message = "";
            if (validate()) {
                SolutionDTO solutionDTO = getDataFromClient();
                if (ParamUtils.ACTION_CREATE.equals(action)) {
                    uploadedFilesInfoCallback(filesInfo);
                    List<SolutionFilesDTO> lstSolutionFilesDTO = new ArrayList<>();
                    if(filesInfo!=null) {
                        for (int i = 0, total = filesInfo.size(); i < total; i++) {
                            SolutionFilesDTO solutionFilesDTO = getDataFromClientContractFiles(solutionDTO, filesInfo.get(0));
                            lstSolutionFilesDTO.add(solutionFilesDTO);
                        }
                    }
                    solutionDTO.setLstSolutionFilesDTO(lstSolutionFilesDTO);
                    message = solutionService.sendSolution(solutionDTO);
                    if (ParamUtils.SUCCESS.equals(message)) {
                        Clients.showNotification(LanguageBundleUtils.getString("global.message.save.successful"), "info", null, "middle_center", 1000);
                        self.detach();
                        reflectPopupComposer.setData();
                        return;
                    } else {
                        Clients.showNotification(message, "warning", null, "middle_center", 3000);
                        return;
                    }
                }
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    public SolutionFilesDTO getDataFromClientContractFiles(SolutionDTO solutionDTO, FileInfo filesInfo) {

        SolutionFilesDTO solutionFilesDTO = new SolutionFilesDTO();
        solutionFilesDTO.setDescription(filesInfo.getDescription());
        solutionFilesDTO.setFileName(new java.io.File(filesInfo.getLocation()).getName());
        solutionFilesDTO.setPathFile(new java.io.File(filesInfo.getLocation()).getPath());
        solutionFilesDTO.setUploadDate(new Date());
        return solutionFilesDTO;
    }

    private SolutionDTO getDataFromClient() {
        SolutionDTO solutionDTO = new SolutionDTO();
        solutionDTO.setReflectId(reflectId);
        solutionDTO.setContent(txtContent.getValue());
        return solutionDTO;
    }

    private boolean validate() {
        Boolean kt = CtrlValidator.checkValidTextbox(txtContent, true, true, LanguageBundleUtils.getString("solution.content"));
        return kt;
    }

    @Override
    public void uploadedFilesInfoCallback(List<FileInfo> filesInfo) {
        this.filesLocation = filesLocation;
        System.out.println(filesLocation);
    }

    @Override
    public void uploadedFilesLocationCallback(List<String> filesLocation) {
        this.filesInfo = filesInfo;
        System.out.println(filesInfo);
    }

    public void onDownloadFile(ForwardEvent evt) {
        evt.stopPropagation();
        try {
            Image btn = (Image) evt.getOrigin().getTarget();
            Listitem litem = (Listitem) btn.getParent().getParent();
            List<SolutionFilesDTO> lstDTO = new ArrayList<>();
            lstDTO.add(litem.getValue());
            java.io.File file = new java.io.File(lstDTO.get(0).getPathFile());
            Filedownload.save(file, null);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(ContractComposer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onViewFile(ForwardEvent evt) {
        evt.stopPropagation();
        try {
            List<FileViews> lstFile = new ArrayList();
            Image btn = (Image) evt.getOrigin().getTarget();
            Listitem litem = (Listitem) btn.getParent().getParent();
            List<SolutionFilesDTO> lstDTO = new ArrayList<>();
            lstDTO.add(litem.getValue());
            FileViews file = new FileViews(lstDTO.get(0).getFileName(), lstDTO.get(0).getPathFile());
            lstFile.add(file);
            Map<String, Object> argu = new HashMap();
            argu.put(ParamUtils.ACTION, ParamUtils.ACTION_CREATE);
            argu.put(ParamUtils.COMPOSER, this);
            argu.put(ParamUtils.DATA, lstFile);
            Window wd = (Window) Executions.createComponents("/controls/widget/pdfFilesViewerPopup.zul", null, argu);
            wd.doModal();
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(ContractComposer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
