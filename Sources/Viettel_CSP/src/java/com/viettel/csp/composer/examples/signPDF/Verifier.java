/*
 * Copyright (C) 2010 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.viettel.csp.composer.examples.signPDF;

import java.util.Date;

/**
 * verifier result of each signature, if you want to alter the result, extents this class
 * @author $sonnt38@viettel.com.vn
 * @since since_text
 * @version 1.0
 */
public abstract class Verifier extends Object {
    /**
     * signer name
     */
    private String signerName;
    /**
     * signer organization
     */
    private String signerOrganization;
    /**
     * signer unit in the organization
     */
    private String signerOrganizationUnit;
    /**
     * signer certificate location
     */
    private String signerLocation;
    /**
     * the content of document is modified?
     */
    private boolean modified;
    /**
     * the certificate is valid?
     */
    private boolean valid;
    private String invalidReason;
    /**
     * comment of signer
     */
    private String comment;
    /**
     * signing date
     */
    private Date date;

    Verifier(String signerName, Date date, boolean modified, String location) {
        this.signerName = signerName;
        this.signerOrganization = "";
        this.signerOrganizationUnit = "";
        this.signerLocation = location;
        this.modified = modified;
        this.valid = false;
        this.invalidReason = "";
        this.comment = "";
        this.date = date;
    }

    public void setSignerOganization(String signerOganization) {
        this.signerOrganization = signerOganization;
    }

    public void setSignerOganizationUnit(String signerOganizationUnit) {
        this.signerOrganizationUnit = signerOganizationUnit;
    }

    public void setInvalidReason(String valid) {
        this.invalidReason = valid;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getComment() {
        return comment;
    }

    public Date getDate() {
        return date;
    }

    public String getSignerLocation() {
        return signerLocation;
    }

    public String getSignerName() {
        return signerName;
    }

    public String getSignerOrganization() {
        return signerOrganization;
    }

    public String getSignerOrganizationUnit() {
        return signerOrganizationUnit;
    }

    public boolean isModified() {
        return modified;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public boolean isValid() {
        return valid;
    }

    
    public String getInvalidReason() {
        return invalidReason;
    }

    @Override
    public abstract String toString();
}

