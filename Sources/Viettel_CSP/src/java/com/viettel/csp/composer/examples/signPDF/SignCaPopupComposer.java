package com.viettel.csp.composer.examples.signPDF;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.codec.Base64;
import com.viettel.csp.composer.BaseCustomComposer;
import com.viettel.csp.util.FileUtils;
import com.viettel.csp.util.LanguageBundleUtils;
import static com.viettel.csp.util.StringUtils.formatDate;
import java.io.*;
import java.lang.reflect.Method;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.*;
import org.apache.log4j.Logger;
import org.zkoss.json.JSONObject;
import org.zkoss.json.JSONValue;
import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.ForwardEvent;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.*;

/**
 * Created by sinhhv on 4/26/2016.
 */
public class SignCaPopupComposer extends BaseCustomComposer {

    final static org.apache.log4j.Logger signLog = org.apache.log4j.Logger.getLogger("signLogger");
    private String signType = "";
    private int signTime = 0;
    private java.util.List<String> listFileNeedSign;
    private Object controllerCallback;
    private String methodCallback;
    private java.util.List<SignPdfFile> lstPdfSig = new ArrayList();
    private Iframe report;
    private Combobox cboPdfFile;
    private ResourceBundle resbundConfigICTemplate;
    private String objectId;
    private String createUser;
    private Button btnFlashSign;
    private Vlayout appletContainer;
    private Listbox lbxIcLog;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
//        resbundConfigICTemplate = ResourceBundle.getBundle("configICTemplate");
        if (arg.containsKey("SIGN_TYPE")) {
            signType = arg.get("SIGN_TYPE").toString();
            if (signType.equals("1")) {
                btnFlashSign.setVisible(true);
                appletContainer.setVisible(false);
            } else if (signType.equals("2") || signType.equals("3")) {
                btnFlashSign.setVisible(false);
                appletContainer.setVisible(true);
            }
        }
        appletContainer.setVisible(true);
        if (arg.containsKey("SIGN_TIME")) {
            signTime = Integer.parseInt(arg.get("SIGN_TIME").toString());
        }
        if (arg.containsKey("LIST_FILE")) {
            listFileNeedSign = (java.util.List<String>) arg.get("LIST_FILE");
        }
        if (arg.containsKey("CONTROLLER")) {
            controllerCallback = arg.get("CONTROLLER");
        }
        if (arg.containsKey("METHOD_CALLBACK")) {
            methodCallback = arg.get("METHOD_CALLBACK").toString();
        }
        if (arg.containsKey("OBJECT_ID")) {
            objectId = arg.get("OBJECT_ID").toString();
        }
        if (arg.containsKey("CREATE_USER")) {
            createUser = arg.get("CREATE_USER").toString();
        }
        if (arg.containsKey("LOG_LIST")) {
            lbxIcLog.setModel((ListModelList) arg.get("LOG_LIST"));
        }
        if (checkValidParamsToSign()) {
            renderViewFile();
        }
    }

    private boolean checkValidParamsToSign() {
        String messError = "";
        boolean validate = true;
        if (signType == null) {
            messError += "ca.sign.type.not.null" + " - ";
            validate = false;
        }
        if (signTime <= 0) {
            messError += "ca.sign.time.not.null" + " - ";
            validate = false;
        }
        java.util.List<String> listFileChecked = new ArrayList();
        if (listFileNeedSign != null && !listFileNeedSign.isEmpty()) {
            for (String filePath : listFileNeedSign) {
                File file = new File(filePath);
                if (file.exists()) {
                    listFileChecked.add(filePath);
                }
            }
        }
        if (listFileChecked.isEmpty()) {
            messError += "ca.files.not.exist" + " - ";
            validate = false;
        } else {
            listFileNeedSign = listFileChecked;
        }
        if (controllerCallback == null) {
            messError += "ca.callback.controller.not.null" + " - ";
            validate = false;
        } else {
            if (methodCallback == null) {
                messError += "ca.callback.method.not.null" + " - ";
                validate = false;
            } else {
                Method method = null;
                try {
                    method = controllerCallback.getClass().getMethod(methodCallback);
                } catch (NoSuchMethodException ex) {
                }
                if (method == null) {
                    messError += "ca.callback.method.not.null" + " - ";
                    validate = false;
                }
            }
        }
        if (!validate) {
            Clients.showNotification(messError.endsWith("-") ? messError.substring(0, messError.length() - 1) : messError, "warning", lbxIcLog, "middle_center", 3000);
            self.detach();
        }
        return true;
    }

    private void renderViewFile() throws FileNotFoundException {
        boolean first = true;
        for (String filePath : listFileNeedSign) {
            File file = new File(filePath);
            AMedia amedia = new AMedia(file, "application/pdf", null);
            if (first) {
                report.setContent(amedia);
                first = false;
            }
            Comboitem item = new Comboitem();
            item.setLabel(file.getName());
            item.setValue(amedia);
            cboPdfFile.getItems().add(item);
        }
        cboPdfFile.setSelectedIndex(0);
    }

    public void onSelect$cboPdfFile() {
        AMedia media = (AMedia) cboPdfFile.getSelectedItem().getValue();
        report.setContent(media);
    }

    public void onClick$btnFlashSign() {
        PdfReader reader = null;
        PdfStamper stamper = null;
        try {
            //backup file trc khi ky
            backupFile();
            //bat dau ky nhay
            for (String fileSign : listFileNeedSign) {
                reader = new PdfReader(fileSign);
                Font font = new Font(BaseFont.createFont("/times.ttf", "Identity-H", BaseFont.EMBEDDED));
                font.setSize(10f);
                font.setStyle(Font.BOLD);
                Phrase phrase0 = new Phrase(":", font);
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                stamper = new PdfStamper(reader, out);
                PdfContentByte content = stamper.getOverContent(1);
//                String[] position = (getPosition(fileSign)[0]).toString().split(",");
//                float x = Float.parseFloat(position[0]);
//                float y = Float.parseFloat(position[1]);
                float x = Float.parseFloat("100");
                float y = Float.parseFloat("100");
                float space = 15f;
                if (signTime == 1) {
                    Phrase phrase1 = new Phrase("tradeNumber", font);
                    Phrase phrase2 = new Phrase(objectId + "", font);
                    ColumnText.showTextAligned(content, Element.ALIGN_LEFT, phrase1, x, y, 0);  // 0
                    ColumnText.showTextAligned(content, Element.ALIGN_LEFT, phrase0, x + 55, y, 0);
                    ColumnText.showTextAligned(content, Element.ALIGN_LEFT, phrase2, x + 60, y, 0);

                    Phrase phrase3 = new Phrase("createddate", font);
                    Phrase phrase4 = new Phrase(formatDate(new Date()), font);
                    ColumnText.showTextAligned(content, Element.ALIGN_LEFT, phrase3, x, y - space, 0);  // -15
                    ColumnText.showTextAligned(content, Element.ALIGN_LEFT, phrase0, x + 55, y - space, 0);
                    ColumnText.showTextAligned(content, Element.ALIGN_LEFT, phrase4, x + 60, y - space, 0);

                    Phrase phrase5 = new Phrase("create_person", font);
                    Phrase phrase6 = new Phrase(createUser, font);
                    ColumnText.showTextAligned(content, Element.ALIGN_LEFT, phrase5, x, y - (space * 2), 0);  // -30
                    ColumnText.showTextAligned(content, Element.ALIGN_LEFT, phrase0, x + 55, y - (space * 2), 0);
                    ColumnText.showTextAligned(content, Element.ALIGN_LEFT, phrase6, x + 60, y - (space * 2), 0);

                    Phrase phrase9 = new Phrase("GEM", font);
                    ColumnText.showTextAligned(content, Element.ALIGN_LEFT, phrase9, x + 60, y - (space * 3), 0);
                } else {
                    Phrase phrase9 = new Phrase("GEM", font);
                    ColumnText.showTextAligned(content, Element.ALIGN_LEFT, phrase9, x + 60, y, 0);
                }
                content.saveState();
                content.setColorStroke(BaseColor.BLACK);
                content.stroke();
                content.restoreState();
                stamper.close();
                reader.close();
                FileOutputStream fos = new FileOutputStream(fileSign);
                out.writeTo(fos);
                out.close();
                fos.close();
            }
            //ket thuc ky nhay

            //cap nhat DB sau khi ky
            updateAfterSign();

            //render lai file
            renderViewFile();
            btnFlashSign.setDisabled(true);
            Clients.showNotification(LanguageBundleUtils.getString("ca.flash.sign.success"), "info", lbxIcLog, "middle_center", 2000);
            self.detach();
        } catch (Exception ex) {
            Clients.showNotification(LanguageBundleUtils.getString("ca.sign.error"), "warning", lbxIcLog, "middle_center", 2000);
            signLog.error(ex.getMessage(), ex);
            rollback();
        } finally {
            if (stamper != null) {
                try {
                    stamper.close();
                } catch (Exception e) {
                    signLog.error(e.getMessage(), e);
                }
            }
            if (reader != null) {
                reader.close();
            }
        }

    }

    //cap nhat db hoac du lieu sau khi ky xong
    public void updateAfterSign() throws Exception {
        signLog.info("USER: " + "GEM" + " invoke ham callback");
        Method method = controllerCallback.getClass().getMethod(methodCallback);
        ListModelList list = (ListModelList) method.invoke(controllerCallback);
        lbxIcLog.setModel(list);
    }

    java.util.List<PDFDigitalSignature> listABC = new ArrayList();
    java.util.List<SignatureObject> storeHash = new ArrayList();

    public void onCreateHash$wdSignCAPopup(ForwardEvent event) {
        try {
            lstPdfSig.clear();
            listABC.clear();
            storeHash.clear();

            backupFile();

            Font font = new Font(BaseFont.createFont("times.ttf", "Identity-H", false));
            font.setSize(10f);
            font.setStyle(Font.BOLD);
            String base64Cert = (String) event.getOrigin().getData();
            X509Certificate x509Cert = (X509Certificate) Base64.decodeToObject(base64Cert);
            if (x509Cert != null) {
                if (!x509Cert.getSubjectDN().getName().contains("CMD")) {
                    String certSerial = x509Cert.getSerialNumber().toString(16);
                    signLog.info("Serial: " + certSerial);
                    String listHash = "";
                    String listPath = "";
                    for (int i = 0; i < listFileNeedSign.size(); i++) {
                        String filePath = listFileNeedSign.get(i);
                        filePath = filePath.replaceAll("\\\\", "/");
                        File file = new File(filePath);
                        if (file.exists()) {
                            SignPdfFile pdfSig = new SignPdfFile();
                            PdfSignatureParameter pdfSp = null;
                            pdfSp = new PdfSignatureParameter(resbundConfigICTemplate.getString("reason"));
                            pdfSp.setSignerLocation(resbundConfigICTemplate.getString("location"));
                            Object[] ob = getPosition(filePath);
                            pdfSp.setPosition((Rectangle) ob[0]);
                            pdfSp.setSignerPage("SIGNER_PAGE");
                            pdfSp.setSignerPage("top");
                            pdfSp.setPage((Integer) ob[1]);
                            PDFDigitalSignature pdfSign = new PDFDigitalSignature();
                            SignatureObject so = pdfSign.createDigest(filePath, pdfSp, new Certificate[]{x509Cert}, false);
                            listHash += Base64.encodeBytes(so.getDigest()) + ";";
                            listPath += filePath + ";";
                            listABC.add(pdfSign);
                            lstPdfSig.add(pdfSig);
                            storeHash.add(so);
                        }
                    }
                    if (listHash.length() > 0) {
                        Clients.evalJavaScript("document.appletSignCA.signCa('" + listHash.substring(0, listHash.length() - 1) + "','" + listPath.substring(0, listPath.length() - 1) + "')");
                    }
                }
            } else {
                Clients.showNotification(LanguageBundleUtils.getString("ca.not.use.usb.individual"), "warning", lbxIcLog, "middle_center", 2000);
            }
        } catch (Exception ex) {
            rollback();
            signLog.error(ex.getMessage(), ex);
            Clients.evalJavaScript("document.appletSignCA.enableAllControl(true)");
            Clients.showNotification(LanguageBundleUtils.getString("ca.sign.error"), "warning", lbxIcLog, "middle_center", 2000);
        }
    }

    public void onSignCA$wdSignCAPopup(ForwardEvent event) {
        try {
            Object object = JSONValue.parse(event.getOrigin().getData().toString());
            JSONObject json = (JSONObject) object;
            String[] signature = json.get("signature").toString().split(";");
            String[] files = json.get("file").toString().split(";");
            for (int i = 0; i < files.length; i++) {
                if (lstPdfSig.get(i) != null) {
                    File fileSign = new File(files[i]);
                    if (fileSign.exists()) {
                        storeHash.get(i).setSignature(Base64.decode(signature[i]));
                        listABC.get(i).insertSignature(storeHash.get(i), files[i]);
                    }
                } else {
                    Clients.showNotification(LanguageBundleUtils.getString("ca.sign.error"), "warning", lbxIcLog, "middle_center", 2000);
                    return;
                }
            }
            updateAfterSign();
            signLog.info("SIGN SUCCESS");
            renderViewFile();
            Clients.showNotification(LanguageBundleUtils.getString("ca.sign.ca.success"), "info", lbxIcLog, "middle_center", 2000);
            self.detach();
        } catch (Exception ex) {
            rollback();
            signLog.error(ex.getMessage(), ex);
            Clients.evalJavaScript("document.appletSignCA.enableAllControl(true)");
            Clients.showNotification(LanguageBundleUtils.getString("ca.sign.error"), "warning", lbxIcLog, "middle_center", 2000);
        }
    }

    public void backupFile() {
        signLog.info("BACKUP FILE");
        for (String filePath : listFileNeedSign) {
            try {
                File fileSource = new File(filePath);
                File fileDest = new File(fileSource.getAbsolutePath().replace(fileSource.getName(), "") + "/backup/" + fileSource.getName());
                if (fileSource.isFile()) {
                    FileUtils.copy(fileSource, fileDest);
                }
            } catch (Exception ex) {
                signLog.error("ERROR backup file", ex);
            }
        }
    }

    public String getSignType() {
        return signType;
    }

    public void setSignType(String signType) {
        this.signType = signType;
    }

    public void rollback() {
        signLog.info("ROLLBACK FILE");
        for (String filePath : listFileNeedSign) {
            try {
                File fileSource = new File(filePath);
                File fileDest = new File(fileSource.getAbsolutePath().replace(fileSource.getName(), "") + "/backup/" + fileSource.getName());
                if (fileDest.isFile()) {
                    FileUtils.copy(fileDest, fileSource);
                }
            } catch (Exception ex) {
                signLog.error("ERROR rollback file", ex);
            }
        }
    }

    private Object[] getPosition(String srcFile) {
        PdfReader reader = null;
        Object[] _return = new Object[2];
        try {
            reader = new PdfReader(srcFile);
            HashMap map = reader.getInfo();
            if (signType.equalsIgnoreCase("SIGN_TYPE_FLASH_SIGN")) {
                if (map.containsKey("POS_FLASH_SIGN_" + signTime)) {
                    _return[0] = map.get("POS_FLASH_SIGN_" + signTime);
                }
            } else {
                String[] pos = map.get("POS_SIGN_CA_" + signTime).toString().split(",");
                Rectangle rectangle = new Rectangle(new Float(pos[0]) - 80, new Float(pos[1]) + 5, new Float(pos[2]) + 80, new Float(pos[3]) - 95);
                _return[0] = rectangle;
                _return[1] = Integer.parseInt(pos[4]);
            }
        } catch (Exception ex) {
            System.out.print("Loai ky la:" + signType + "signType");
            System.out.print("thu tu ky la:" + signTime + "signTime");
            signLog.error(ex.getMessage(), ex);
        } finally {
            if (reader != null) {
                reader.close();
            }
        }
        return _return;
    }

    public String getOwnerTypeFiendlyName(String type) {
        return LanguageBundleUtils.getString("iclog.owner.type" + type);
    }

    public String getActionCodeFiendlyName(String actionCode) {
        return LanguageBundleUtils.getString(actionCode);
    }

    @Override
    protected Logger getLogger() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected Window getWindow() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected Listbox getGridData() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected String getArgDTOKey() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void buildSearchObject() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
