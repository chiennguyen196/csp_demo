/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.composer.liquidation;

import com.viettel.csp.DTO.ContractDTO;
import com.viettel.csp.DTO.ContractFilesDTO;
import com.viettel.csp.util.CtrlValidator;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.util.StringUtils;
import com.viettel.plc.widget.IUploaderCallback;
import com.viettel.plc.widget.UploadComponent;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @author chitv@gemvietnam.com
 */
public class LiquidationAddFile extends GenericForwardComposer<Component> implements
        IUploaderCallback {

    final static org.apache.log4j.Logger logger = org.apache.log4j.Logger
            .getLogger(LiquidationAddFile.class);
    private Textbox txtFileContractNumber, txtContent, txtDescription, txtFileName;
    private Datebox dbStartDate, dbEndDate;
    private String action;
    private ContractDTO oldDTO;
    private LiquidationUpdateComposer liquidationUpdateComposer;
    private ContractFilesDTO contractFilesDTO;
    private UploadComponent uploadComponent;
    private List<String> filesLocation;
    private List<ContractFilesDTO> lstContractFiles = new ArrayList<>();
    private Listbox lbxViewAddContract;
    private ArrayList indexLst;

    public ContractFilesDTO getContractFilesDTO() {
        return this.contractFilesDTO;
    }

    public void setContractFilesDTO(ContractFilesDTO contractFilesDTO) {
        this.contractFilesDTO = contractFilesDTO;
    }

    public List<String> getFilesLocation() {
        return this.filesLocation;
    }

    public void setFilesLocation(List<String> filesLocation) {
        this.filesLocation = filesLocation;
    }

    public List<ContractFilesDTO> getLstContractFiles() {
        return this.lstContractFiles;
    }

    public void setLstContractFiles(List<ContractFilesDTO> lstContractFiles) {
        this.lstContractFiles = lstContractFiles;
    }

    public Listbox getLbxViewAddContract() {
        return this.lbxViewAddContract;
    }

    public void setLbxViewAddContract(Listbox lbxViewAddContract) {
        this.lbxViewAddContract = lbxViewAddContract;
    }

    public LiquidationUpdateComposer getLiquidationUpdateComposer() {
        return liquidationUpdateComposer;
    }

    public void setLiquidationUpdateComposer(LiquidationUpdateComposer liquidationUpdateComposer) {
        this.liquidationUpdateComposer = liquidationUpdateComposer;
    }

    @Override
    public void doAfterCompose(Component comp) {
        try {
            super.doAfterCompose(comp);
            if (this.arg.containsKey(ParamUtils.COMPOSER)) {
                this.liquidationUpdateComposer = (LiquidationUpdateComposer) this.arg
                        .get(ParamUtils.COMPOSER);
            }
            if (this.arg.containsKey(ParamUtils.ACTION)) {
                this.action = this.arg.get(ParamUtils.ACTION).toString();
            }
            if (this.arg.containsKey("LIST_ACTION_INDEX")) {
                this.indexLst = (ArrayList) this.arg.get("LIST_ACTION_INDEX");
            }
            if (this.arg.containsKey("LISTBOX")) {
                this.contractFilesDTO = (ContractFilesDTO) this.arg.get("LISTBOX");
            }
            this.uploadComponent.setUploaderCallback(this);
            if (this.indexLst != null) {
                setData(this.indexLst.get(0));
            } else {
                setData();
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            Clients
                    .showNotification(LanguageBundleUtils.getString("global.message.error"), "warning",
                            null, "middle_center", 3000);
        }
    }

    public void setData(Object indexLst) throws Exception {
        if (ParamUtils.ACTION_CREATE.equals(this.action)) {
            ((Window) this.self).setTitle(LanguageBundleUtils.getString("global.add"));
        } else if (ParamUtils.ACTION_UPDATE_FILE.equals(this.action)) {

            if (this.contractFilesDTO != null) {
                this.txtFileContractNumber.setValue(this.contractFilesDTO.getFileContractNumber());
                this.txtContent.setValue(this.contractFilesDTO.getContent());
                this.txtDescription.setValue(this.contractFilesDTO.getDescription());
                this.txtFileName.setValue(this.contractFilesDTO.getFileName());
                this.dbStartDate.setValue(this.contractFilesDTO.getStartDate());
                this.dbEndDate.setValue(this.contractFilesDTO.getEndDate());
            }
        }
    }

    public void setData() throws Exception {
        if (ParamUtils.ACTION_CREATE.equals(this.action)) {
            ((Window) this.self).setTitle(LanguageBundleUtils.getString("global.add"));
        }
    }

    public void onClick$bntAddSendContractFile() {
        try {
            this.uploadComponent.onClick$doSave();
            String messageContract = "";
            String messagePartner = "";
            if (null == filesLocation) {
                Clients.showNotification(LanguageBundleUtils.getString("contractFiles.messageFiles.error"), "warning", null, "middle_center", 3000);
                return;
            }
            if (validate()) {
                contractFilesDTO = getDataFromClientContractFiles(oldDTO, filesLocation.get(0));
                lstContractFiles.add(contractFilesDTO);
                liquidationUpdateComposer.setDataGrid(contractFilesDTO);
                self.detach();
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients
                    .showNotification(LanguageBundleUtils.getString("global.message.error"), "warning",
                            null, "middle_center", 3000);
        }
    }

    public void onClick$bntUpdateSendContractFile() {
        try {
            this.uploadComponent.onClick$doSave();
            String messageContract = "";
            String messagePartner = "";
            if (null == this.filesLocation) {
                Clients.showNotification(
                        LanguageBundleUtils.getString("contractFiles.messageFiles.error"), "warning",
                        null, "middle_center", 3000);
                return;
            }
            if (validate()) {
                this.contractFilesDTO = getDataFromClientContractFiles(this.oldDTO,
                        this.filesLocation.get(0));
                this.lstContractFiles.add(this.contractFilesDTO);
                this.liquidationUpdateComposer.setDataGrid(this.contractFilesDTO);
                this.self.detach();
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients
                    .showNotification(LanguageBundleUtils.getString("global.message.error"), "warning",
                            null, "middle_center", 3000);
        }
    }

    public ContractFilesDTO getDataFromClientContractFiles(ContractDTO contractDTO,
                                                           String fileLocation) {
        this.contractFilesDTO = new ContractFilesDTO();
        this.contractFilesDTO.setFileContractNumber(this.txtFileContractNumber.getValue());
        this.contractFilesDTO.setContent(this.txtContent.getValue());
        this.contractFilesDTO.setDescription(this.txtDescription.getValue());
        this.contractFilesDTO.setStartDate(this.dbStartDate.getValue());
        this.contractFilesDTO.setEndDate(this.dbEndDate.getValue());
        this.contractFilesDTO.setFileName(new File(fileLocation).getName());
        this.contractFilesDTO.setPathFile(new File(fileLocation).getPath());
        return this.contractFilesDTO;
    }

    public void onClick$btnCloseAddContract() {
        try {
            this.self.detach();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients
                    .showNotification(LanguageBundleUtils.getString("global.message.error"), "warning",
                            null, "middle_center", 3000);
        }
    }

    private boolean validate() {
        if (!CtrlValidator.checkValidTextbox(this.txtFileContractNumber, true, true, this.action)) {
            return false;
        }
        if (!CtrlValidator.checkValidTextbox(this.txtContent, true, true, this.action)) {
            return false;
        }
        if (!CtrlValidator.checkValidTextbox(this.txtDescription, true, true, this.action)) {
            return false;
        }

        if (!StringUtils.isValidDate(this.dbStartDate.getValue())) {
            Clients.wrongValue(this.dbStartDate, LanguageBundleUtils
                    .getMessage("global.validate.notformat.date.ddMMyyyy",
                            LanguageBundleUtils.getString("contract.startDate")));
            return false;
        }
        if (!StringUtils.isValidDate(this.dbEndDate.getValue())) {
            Clients.wrongValue(this.dbEndDate, LanguageBundleUtils
                    .getMessage("global.validate.notformat.date.ddMMyyyy",
                            LanguageBundleUtils.getString("contract.endDate")));
            return false;
        }

        return true;
    }

    @Override
    public void uploadedFilesLocationCallback(List<String> filesLocation) {
        this.filesLocation = filesLocation;
        System.out.println(filesLocation);
    }

}
