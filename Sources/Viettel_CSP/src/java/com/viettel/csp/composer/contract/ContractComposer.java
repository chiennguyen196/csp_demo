/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.composer.contract;

import com.viettel.csp.DTO.ContractDTO;
import com.viettel.csp.DTO.ContractStatusDTO;
import com.viettel.csp.DTO.UserDTO;
import com.viettel.csp.composer.partner.PartnerComposer;
import com.viettel.csp.entity.UserEntity;
import com.viettel.csp.service.ContractService;
import com.viettel.csp.service.ContractStatusService;
import com.viettel.csp.service.PartnerService;
import com.viettel.csp.service.UserService;
import com.viettel.csp.util.KeyValueBean;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.util.StringUtils;
import com.viettel.csp.util.UserTokenUtils;
import com.viettel.eafs.util.SpringUtil;
import com.viettel.logincsp.AuthenticationService;

import java.util.*;
import java.util.logging.Level;

import org.apache.log4j.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.ForwardEvent;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.*;

/**
 * @author Chitv@gemvietnam.com
 */
public class ContractComposer extends GenericForwardComposer<Component> {

    private AuthenticationService authenticationService = SpringUtil.getBean("authenticationService", AuthenticationService.class);
    private ContractService contractService = SpringUtil.getBean("contractService", ContractService.class);
    private PartnerService partnerService = SpringUtil.getBean("partnerService", PartnerService.class);
    private UserService userService = SpringUtil.getBean("userService", UserService.class);
    private ContractStatusService contractStatusService = SpringUtil.getBean("contractStatusService", ContractStatusService.class);

    private static final Logger logger = Logger.getLogger(PartnerComposer.class);
    private ListModelList<ContractDTO> listContract = new ListModelList<>();
    private ListModelList<KeyValueBean> kvbContractStatus;
    private List<ContractDTO> lstResult;
    private Listbox lbxContract;
    private Textbox txtCompanyName;
    private Grid gridSearch;
    private Combobox cbbContractStatus;
    private Button btnAdd;

    public ListModelList<ContractDTO> getListContract() {
        return listContract;
    }

    public void setListContract(ListModelList<ContractDTO> listContract) {
        this.listContract = listContract;
    }

    public ListModelList<KeyValueBean> getKvbContractStatus() {
        return kvbContractStatus;
    }

    public void setKvbContractStatus(ListModelList<KeyValueBean> kvbContractStatus) {
        this.kvbContractStatus = kvbContractStatus;
    }

    public List<ContractDTO> getLstResult() {
        return lstResult;
    }

    public void setLstResult(List<ContractDTO> lstResult) {
        this.lstResult = lstResult;
    }

    public Listbox getLbxContract() {
        return lbxContract;
    }

    public void setLbxContract(Listbox lbxContract) {
        this.lbxContract = lbxContract;
    }

    public Textbox getTxtCompanyName() {
        return txtCompanyName;
    }

    public void setTxtCompanyName(Textbox txtCompanyName) {
        this.txtCompanyName = txtCompanyName;
    }

    public Grid getGridSearch() {
        return gridSearch;
    }

    public void setGridSearch(Grid gridSearch) {
        this.gridSearch = gridSearch;
    }

    public Combobox getCbbContractStatus() {
        return cbbContractStatus;
    }

    public void setCbbContractStatus(Combobox cbbContractStatus) {
        this.cbbContractStatus = cbbContractStatus;
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        try {
            super.doAfterCompose(comp);
            lbxContract.setMultiple(true);
            bindDataToCombo();
            onClick$btnSearch();
            setVisiable();
        } catch (Throwable ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    private void setVisiable() {
        if(UserTokenUtils.getPersonType().equalsIgnoreCase(ParamUtils.USER_TYPE.VIETTEL)){
            btnAdd.setVisible(false);
        }
    }

    public void bindDataToCombo() throws Exception {
        try {
            List<KeyValueBean> lstKey = new ArrayList<KeyValueBean>();
            List<KeyValueBean> lstKeyValueBean = contractStatusService.getListToKeyValueBean(true);
            if (lstKeyValueBean != null && lstKeyValueBean.size() > 0) {
                for (KeyValueBean bean : lstKeyValueBean) {
                    String key = String.valueOf(bean.getKey());
                    List<String> list = Arrays.asList(ParamUtils.TYPE_ACTION.LIST_ACTION_PROFILE);
                    if (list.contains(key)) {
                        lstKey.add(bean);
                    }
                }
                kvbContractStatus = new ListModelList(lstKey);
                kvbContractStatus.addToSelection(kvbContractStatus.getElementAt(0));
                cbbContractStatus.setModel(kvbContractStatus);
            } else {
                cbbContractStatus.setModel(new ListModelList());
            }
            cbbContractStatus.setValue(LanguageBundleUtils.getString("global.combobox.choose"));
        } catch (Exception e) {
            logger.error(e);
        }
    }

    private ContractDTO getDataFromClient() {
        ContractDTO object = new ContractDTO();

        if (StringUtils.isValidString(txtCompanyName.getValue())) {
            object.setCompanyName(txtCompanyName.getValue().trim());
        }
        if (cbbContractStatus.getSelectedIndex() > 0) {
            object.setContractStatusCode(cbbContractStatus.getSelectedItem().getValue());
        }
        return object;
    }

    public void onClick$btnSearch() {
        try {
            ContractDTO contractDTO = getDataFromClient();
            lstResult = contractService.getListContractProfile(contractDTO, authenticationService.getUserCredential());
            if (lstResult != null && !lstResult.isEmpty()) {
                ListModelList model = new ListModelList(lstResult);
                model.setMultiple(true);
                lbxContract.setModel(model);
                if (gridSearch != null) {
                    gridSearch.invalidate();
                }
            } else {
                lbxContract.setModel(new ListModelList());
            }
        } catch (Throwable ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    public void onDeleteContract(ForwardEvent evt) {
        evt.stopPropagation();
        try {
            List<ContractDTO> lstDeleteDTO = new ArrayList<>();
            Image btn = (Image) evt.getOrigin().getTarget();
            Listitem litem = (Listitem) btn.getParent().getParent();
            ContractDTO deleteDTO = ((ContractDTO) litem.getValue());
            lstDeleteDTO.add(deleteDTO);
            String message = contractService.deleteContract(lstDeleteDTO);
            bindDataToGrid();
            Clients.showNotification(message, "info", null, "middle_center", 3000);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(ContractComposer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onEditContract(ForwardEvent evt) {
        evt.stopPropagation();
        Image btn = (Image) evt.getOrigin().getTarget();
        Listitem litem = (Listitem) btn.getParent().getParent();
        int index = -1;
        for (int i = 0; i < lbxContract.getItemCount(); i++) {
            if (litem == lbxContract.getItemAtIndex(i)) {
                index = i;
            }
        }

        Map<String, Object> argu = new HashMap();
        argu.put(ParamUtils.ACTION, ParamUtils.ACTION_UPDATE);
        List<Integer> indexs = new ArrayList();
        indexs.add(index);
        argu.put("LIST_ACTION_INDEX", indexs);
        argu.put("LISTBOX", lbxContract);
        argu.put(ParamUtils.COMPOSER, this);
        Window wd;
        wd = (Window) Executions.createComponents("/controls/contract/contractEditPopup.zul", null, argu);
        wd.doModal();
    }

    public void onClick$btnEdit() {
        try {
            if (lbxContract.getSelectedCount() == 1) {
                Map<String, Object> argu = new HashMap();
                argu.put(ParamUtils.ACTION, ParamUtils.ACTION_UPDATE);
                ContractDTO contactDTO = (ContractDTO) lbxContract.getSelectedItem().getValue();
                argu.put("CONTRACT_ID", contactDTO.getContractId());
                argu.put(ParamUtils.COMPOSER, this);
                Window wd;
                wd = (Window) Executions.createComponents("/controls/contract/contractEditPopup.zul", null, argu);
                wd.doModal();
            } else {
                Clients.showNotification(LanguageBundleUtils.getString("global.action.confirm.is_select.edit"), "warning", null, "middle_center", 3000);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    public void onClick$btnDelete() {
        try {
            if (lbxContract.getSelectedCount() > 0) {
                Messagebox.show(LanguageBundleUtils.getString("global.action.confirm.multi.delete"), "Confirm Dialog", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION, new org.zkoss.zk.ui.event.EventListener() {
                    @Override
                    public void onEvent(Event evt) throws InterruptedException, Exception {
                        if ("onOK".equals(evt.getName())) {
                            List<ContractDTO> lstDeleteDTO = new ArrayList<>();
                            for (Listitem item : lbxContract.getSelectedItems()) {
                                ContractDTO deleteDTO = ((ContractDTO) item.getValue());
                                if (deleteDTO.getContractStatus().equalsIgnoreCase(ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_WAITING_OK_CONTRACT)) {
                                    lstDeleteDTO.add(deleteDTO);
                                }
                            }
                            String message = contractService.deleteContract(lstDeleteDTO);
                            bindDataToGrid();
                            Clients.showNotification(message, "info", null, "middle_center", 3000);
                        }
                    }
                });
            } else {
                Clients.showNotification(LanguageBundleUtils.getString("global.action.confirm.is_select"), "warning", null, "middle_center", 3000);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    public void bindDataToGrid() throws Exception {
        onClick$btnSearch();
    }

    public String formatDateTime(Date date) throws Exception {
        return StringUtils.formatDateTime(date);
    }

    public void onClick$btnAdd() {
        try {

            Map<String, Object> argu = new HashMap();
            argu.put(ParamUtils.ACTION, ParamUtils.ACTION_CREATE);
            argu.put("CONTACT_ID", "");
            argu.put("PARTNER_ID", UserTokenUtils.getPartnerId());
            argu.put(ParamUtils.COMPOSER, this);
            Window wd = (Window) Executions.createComponents("/controls/contract/contractAddPopup.zul", null, argu);
            wd.doModal();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    public void onClick$btnDownloadFile() {
        try {
            Map<String, Object> argu = new HashMap();
            argu.put(ParamUtils.ACTION, ParamUtils.ACTION_CREATE);
            argu.put(ParamUtils.COMPOSER, this);
            Window wd;
            wd = (Window) Executions.createComponents("/controls/contract/contractDownloadFilePopup.zul", null, argu);
            wd.doModal();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    public String getContractStatusHisName(String paramType) throws Exception{
        ContractStatusDTO contractStatusDTO = contractStatusService.getObjectDTOByCode(paramType);
        return contractStatusDTO.getName();
    }

    public String getUserById(Long userId) {
        try {
            UserEntity usrUser = userService.getUserByID(userId);
            return usrUser.getFullName();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return "";
    }
}
