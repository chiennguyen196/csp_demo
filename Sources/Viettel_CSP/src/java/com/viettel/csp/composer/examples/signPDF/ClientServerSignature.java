/*
 * Copyright (C) 2010 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.viettel.csp.composer.examples.signPDF;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfPKCS7;
import com.viettel.csp.composer.examples.signPDF.ooxml.OoxmlDigitalSignature;
import java.io.InputStream;
import java.io.Serializable;
import java.net.URL;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.crypto.Cipher;
import org.apache.commons.lang.ArrayUtils;

/**
 * client - server signing: server creates message digests and sends them to
 * client client signs the digests and sends the signatures to server server
 * inserts signature into file and creates signed file
 *
 * @author tuannm33@viettel.com.vn
 * @since since 17/09/2013
 * @version 1.0 * <pre>ClientServerSignature css = new OoxmlSignature();
 * URL url = null;
 * SignatureObject object = css.createDigest(docx, null, "output\\casess.docx");
 *
 * SignatureObject object1 = ClientServerSignature.signDigest(object, goodKey, goodChain3);
 *
 * css.insertSignature(object1);
 * </pre>
 */
public abstract class ClientServerSignature implements Serializable {

    /**
     * create digest from file
     *
     * @param fileURL file to create digest
     * @param signatureParameter parameter of signer
     * @param chain certificate chain of signer, this is temporary not use in
     * OOXML
     * @return <CODE>SignatureObject</CODE> to contain digest and type of digest
     * @throws Exception if error
     */
    public abstract SignatureObject createDigest(URL fileURL, SignatureParameter signatureParameter, Certificate[] chain) throws Exception;

    /**
     * create digest from file
     *
     * @param inFile file name
     * @param signatureParameter parameter of signer
     * @param chain certificate chain of signer, this is temporary not use in
     * OOXML
     * @return <CODE>SignatureObject</CODE> to contain digest and type of digest
     */
    public abstract SignatureObject createDigest(String inFile, SignatureParameter signatureParameter, Certificate[] chain, boolean checkLogo) throws Exception;

    /**
     *
     * @param reader
     * @param parameter
     * @param chain
     * @return
     * @throws Exception
     */
    public abstract byte[] createHash(String inFile, SignatureParameter parameter, Certificate[] chain) throws Exception;

    public abstract byte[] createHash(InputStream is, SignatureParameter parameter, Certificate[] chain) throws Exception;

    /**
     * sign the digest
     *
     * @param object : object contains digest and type of digest
     * @param privateKey : signing key of signer
     * @param certificates : certificate chain
     * @return signature in byte array
     */
    public static SignatureObject signDigest(SignatureObject object, PrivateKey privateKey, Certificate[] certificates) throws Exception {

        if (privateKey == null) {
            throw new NullPointerException("Private key is not null");
        }

        if (!privateKey.getAlgorithm().equals("RSA")) {
            throw new DigitalSignatureException("Just use RSA key for safety reason");
        }
        if (certificates == null || certificates.length < 1) {
            throw new NullPointerException("Certificate chain is not null");
        }

        for (int i = 0; i < certificates.length; ++i) {
            if (false == (certificates[i] instanceof X509Certificate)) {
                throw new DigitalSignatureException("Certificate chain is not X.509 type");
            }
        }
        if (object.isOOXML()) {
            return signOOXMLDigest(object, privateKey, certificates);
        } else {
            return signPDFDigest(object, privateKey, certificates);
        }

    }

    public static byte[] signPDFDigest(byte[] hash, PrivateKey privateKey, Certificate[] certificates) throws Exception {

        if (privateKey == null) {
            throw new NullPointerException("Private key is not null");
        }

        if (!privateKey.getAlgorithm().equals("RSA")) {
            throw new DigitalSignatureException("Just use RSA key for safety reason");
        }
        if (certificates == null || certificates.length < 1) {
            throw new NullPointerException("Certificate chain is not null");
        }

        for (int i = 0; i < certificates.length; ++i) {
            if (false == (certificates[i] instanceof X509Certificate)) {
                throw new DigitalSignatureException("Certificate chain is not X.509 type");
            }
        }
        PdfPKCS7 sgn = new PdfPKCS7(privateKey, certificates, null, "SHA1", null, false);
        if (hash == null) {
            throw new NullPointerException("hash digest is not null");
        }
        Calendar cal = Calendar.getInstance();
        byte[] sh = sgn.getAuthenticatedAttributeBytes(hash, cal, null);
        sgn.update(sh, 0, sh.length);

        byte[] encodedSig = sgn.getEncodedPKCS7(hash, cal, null, null);

        final int contentEstimated = 15000;
        if (contentEstimated + 2 < encodedSig.length) {
            throw new DocumentException("Not enough space");
        }

        byte[] paddedSig = new byte[contentEstimated];
        System.arraycopy(encodedSig, 0, paddedSig, 0, encodedSig.length);
        return paddedSig;

    }

    public static byte[] signXMLDigest(byte[] hash, PrivateKey privateKey, Certificate[] certificates) throws Exception {

        if (privateKey == null) {
            throw new NullPointerException("Private key is not null");
        }

        if (!privateKey.getAlgorithm().equals("RSA")) {
            throw new DigitalSignatureException("Just use RSA key for safety reason");
        }
        if (certificates == null || certificates.length < 1) {
            throw new NullPointerException("Certificate chain is not null");
        }

        for (int i = 0; i < certificates.length; ++i) {
            if (false == (certificates[i] instanceof X509Certificate)) {
                throw new DigitalSignatureException("Certificate chain is not X.509 type");
            }
        }
        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        cipher.init(Cipher.ENCRYPT_MODE, privateKey);

        if (hash == null) {
            throw new NullPointerException("hash digest is not null");
        }
        byte[] digestInfoValue = ArrayUtils.addAll(OoxmlDigitalSignature.SHA1_DIGEST_INFO_PREFIX, hash);

        byte[] signatureValue = cipher.doFinal(digestInfoValue);

        return signatureValue;

    }

    private static SignatureObject signPDFDigest(SignatureObject object, PrivateKey privateKey, Certificate[] certificates) throws Exception {
        PdfPKCS7 sgn = new PdfPKCS7(privateKey, certificates, null, "SHA1", null, false);
        byte[] hash = object.getDigest();
        if (hash == null) {
            throw new NullPointerException("hash digest is not null");
        }
        Calendar cal = Calendar.getInstance();
        byte[] sh = sgn.getAuthenticatedAttributeBytes(hash, cal, null);
        sgn.update(sh, 0, sh.length);

        byte[] encodedSig = sgn.getEncodedPKCS7(hash, cal, null, null);

        final int contentEstimated = 15000;
        if (contentEstimated + 2 < encodedSig.length) {
            throw new DocumentException("Not enough space");
        }

        byte[] paddedSig = new byte[contentEstimated];
        System.arraycopy(encodedSig, 0, paddedSig, 0, encodedSig.length);
        //object.setCertificateChain(certificates);
        //------- TuanNM33: moi bo sung 25/12/2013
        List<String> lst = new ArrayList();
        for (Certificate c : certificates) {
            lst.add(c.toString());
        }
        object.setCertificateChain(lst);
        //-------
        object.setSignature(paddedSig);
        return object;
    }

//    private static Holder<Certificate>[] fromCertificateArray(Certificate[] arr){
//        if(arr!=null && arr.length > 0){
//            Holder<Certificate>[] cArr = new Holder[arr.length];
//            for(int i = 0 ;i <arr.length;i++){
//                cArr[i] = new Holder(arr[i]);
//            }
//        }
//        return null;
//    }
    private static SignatureObject signOOXMLDigest(SignatureObject object, PrivateKey privateKey, Certificate[] certificates) throws Exception {
        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        cipher.init(Cipher.ENCRYPT_MODE, privateKey);
        byte[] hash = object.getDigest();
        if (hash == null) {
            throw new NullPointerException("hash digest is not null");
        }
        byte[] digestInfoValue = ArrayUtils.addAll(OoxmlDigitalSignature.SHA1_DIGEST_INFO_PREFIX, hash);

        byte[] signatureValue = cipher.doFinal(digestInfoValue);
        //object.setCertificateChain(certificates);
        //------- TuanNM33: moi bo sung 25/12/2013
        List<String> lst = new ArrayList();
        for (Certificate c : certificates) {
            lst.add(c.toString());
        }
        object.setCertificateChain(lst);
        //-------
        object.setSignature(signatureValue);
        return object;
    }

    /**
     * insert signature into origin file
     *
     * @param object : the signature of signer
     * @param outFile : path of the signed out file
     * @throws Exception: if error
     */
    public abstract void insertSignature(SignatureObject object, String outFile) throws Exception;

    //    public abstract  PdfSignatureAppearance getPdfSignatureAppearance();
    //Kien added to create signature with hash only
    public abstract void insertSignature(byte[] hash, String outFile) throws Exception;
}
