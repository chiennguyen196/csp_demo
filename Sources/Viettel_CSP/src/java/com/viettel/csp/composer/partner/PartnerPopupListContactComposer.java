/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.composer.partner;

import com.viettel.csp.DTO.ContactDTO;
import com.viettel.csp.service.ContactService;
import com.viettel.csp.service.ContactTypeService;
import com.viettel.csp.util.KeyValueBean;
import com.viettel.eafs.util.SpringUtil;
import java.util.List;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Grid;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author Dell
 */
public class PartnerPopupListContactComposer extends GenericForwardComposer<Component> {
        private Window wdContact;
    private Grid gridSearch;
    private Listbox lbxContactSearch;
    private Textbox txtUserName;
    private Textbox txtName;
    private Combobox cbbContactType;
    private Combobox cbbContactStatus;
    private ListModelList<KeyValueBean> kvbContactType;
    private List<ContactDTO> lstResult;
    private ContactService contactService = SpringUtil.getBean("contactService", ContactService.class);
    private ContactTypeService contactTypeService = SpringUtil.getBean("contactTypeService", ContactTypeService.class);

    public List<ContactDTO> getLstResult() {
        return lstResult;
    }

    public void setLstResult(List<ContactDTO> lstResult) {
        this.lstResult = lstResult;
    }
    
     @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
      //  lbxContactSearch.setMultiple(true);
        wdContact.setContentStyle("overflow:auto;");
        //bindData();
        bindDataToGrid();
    }
    
      public void bindDataToGrid() throws Exception {
        //ContactDTO object = getDataSearchFromClient();
       // lstResult = contactService.getList(object);
       
       
             List<ContactDTO> listContact = null;
             listContact =(List<ContactDTO>)  arg.get("LISTBOX");
            ListModelList model = new ListModelList(listContact);
            model.setMultiple(true);
            lbxContactSearch.setModel(model);
//            if (gridSearch != null) {
//                gridSearch.invalidate();
//            }
        
    }
}
