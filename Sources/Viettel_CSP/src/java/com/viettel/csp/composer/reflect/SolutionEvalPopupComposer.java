/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.composer.reflect;

import com.viettel.csp.DTO.ContactDTO;
import com.viettel.csp.DTO.ReasonDTO;
import com.viettel.csp.DTO.SolutionDTO;
import com.viettel.csp.service.ReasonService;
import com.viettel.csp.service.ReflectService;
import com.viettel.csp.service.SolutionService;
import com.viettel.csp.util.KeyValueBean;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.util.StringUtils;
import com.viettel.eafs.util.SpringUtil;
import java.util.ArrayList;
import java.util.List;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Button;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author TienNV@gemvietnam.com
 */
public class SolutionEvalPopupComposer extends GenericForwardComposer<Component> {

    private String action;
    private Long solutionId;
    private Combobox cbbEvalReasonCode;
    private Row rowSolutionEval;
    private Textbox txtEvalDescription;
    private Button btnSendEvalPartner;
    private Button btnclosePopup;
    private SolutionDTO oldDto;
    private ReflectPopupComposer reflectPopupComposer;
    private ReflectService reflectService = SpringUtil.getBean("reflectService", ReflectService.class);
    private SolutionService solutionService = SpringUtil.getBean("solutionService", SolutionService.class);
    private ReasonService reasonService = SpringUtil.getBean("reasonService", ReasonService.class);
    final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(SolutionEvalPopupComposer.class);

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        if (arg.containsKey(ParamUtils.ACTION)) {
            action = arg.get(ParamUtils.ACTION).toString();
        }
        if (arg.containsKey("SOLUTION_ID")) {
            solutionId = (Long) arg.get("SOLUTION_ID");
            oldDto = solutionService.getById(solutionId);
        }
        if (arg.containsKey(ParamUtils.COMPOSER)) {
            reflectPopupComposer = (ReflectPopupComposer) arg.get(ParamUtils.COMPOSER);
        }
        setData();
    }

    public void bindDataCbb() throws Exception {
        List<ReasonDTO> lstReasonDTO = reasonService.findListByType(ParamUtils.REASON_TYPE.REFLECT);
        List lstReason = new ArrayList<>();
        lstReason.add(new KeyValueBean(ParamUtils.DEFAULT_VALUE_STR, LanguageBundleUtils.getString("global.combobox.choose")));
        if (lstReasonDTO != null && lstReasonDTO.size() > 0) {
            for (ReasonDTO reasonDTO : lstReasonDTO) {
                KeyValueBean key = new KeyValueBean();
                key.setKey(reasonDTO.getCode());
                key.setValue(reasonDTO.getName());
                lstReason.add(key);
            }
        }
        ListModelList<KeyValueBean> kvbReason = new ListModelList(lstReason);
        kvbReason.addToSelection(kvbReason.getElementAt(0));
        cbbEvalReasonCode.setModel(kvbReason);
    }

    private void setData() {
        try {
            ((Window) self).setTitle(LanguageBundleUtils.getString("solution.eval.dialog.tooltip.info"));
            if (ParamUtils.SOLUTION_STATUS.NOK.equals(action)) {
                bindDataCbb();
                txtEvalDescription.setValue("");
            } else if (ParamUtils.SOLUTION_STATUS.MORE_INFO.equals(action)) {
                rowSolutionEval.setVisible(false);
                txtEvalDescription.setValue("");
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    public void onClick$btnClose() {
        try {
            self.detach();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    public SolutionDTO getDataFromClient() {
        if (ParamUtils.SOLUTION_STATUS.NOK.equals(action)) {
            if (cbbEvalReasonCode.getSelectedItem() != null && !ParamUtils.SELECT_NOTHING_VALUE_STR.equals(cbbEvalReasonCode.getSelectedItem().getValue())) {
                oldDto.setEvalReasonCode(cbbEvalReasonCode.getSelectedItem().getValue());
            }
        }
        if (StringUtils.isValidString(txtEvalDescription.getValue())) {
            oldDto.setEvalDescription(txtEvalDescription.getValue());
        }
        return oldDto;
    }

    public void onClick$btnSendEvalPartner() {
        try {
            String message = "";
            if (validate()) {
                oldDto = getDataFromClient();
                message = solutionService.sendEvaluatePartner(oldDto, action);
                if (ParamUtils.SUCCESS.equals(message)) {
                    Clients.showNotification(LanguageBundleUtils.getString("global.message.save.successful"), "info", null, "middle_center", 1000);
                    self.detach();
                    reflectPopupComposer.setData();
                    return;
                } else {
                    Clients.showNotification(message, "warning", null, "middle_center", 3000);
                    return;
                }
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    private boolean validate() {
        if (ParamUtils.SOLUTION_STATUS.NOK.equals(action)) {
            if (cbbEvalReasonCode.getSelectedItem().getValue() != null && ParamUtils.SELECT_NOTHING_VALUE_STR.equals(cbbEvalReasonCode.getSelectedItem().getValue())) {
                Clients.wrongValue(cbbEvalReasonCode, LanguageBundleUtils.getMessage("global.validate.notchoose", LanguageBundleUtils.getString("solution.evalReasonCode")));
                return false;
            }
        }

        if (!StringUtils.isValidString(txtEvalDescription.getValue())) {
            Clients.wrongValue(txtEvalDescription, LanguageBundleUtils.getMessage("global.validate.notnull", LanguageBundleUtils.getString("solution.evalDescription")));
            return false;
        }
        return true;
    }
}
