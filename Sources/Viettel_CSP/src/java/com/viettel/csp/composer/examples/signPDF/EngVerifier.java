/*
 * Copyright (C) 2010 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.viettel.csp.composer.examples.signPDF;

import java.util.Date;

/**
 *
 * @author $sonnt38@viettel.com.vn
 * @since since_text
 * @version 1.0
 */
public class EngVerifier extends Verifier {
    /**
     * constructor of English verifier
     * @param signerName signer name
     * @param date signing date
     * @param modified is document modified?
     * @param valid is signer valid?
     */
    public EngVerifier(String signerName, Date date, boolean modified, String valid) {
        super(signerName, date, modified, valid);
    }

    @Override
    public String toString() {
        String result = "";
        result += "Signer: " + super.getSignerName();
        if (super.getSignerOrganization() != null
                && !super.getSignerOrganization().equals("")) {
            result += "\nSigner Organization: " + super.getSignerOrganization();
        }

        if (super.getSignerOrganizationUnit() != null
                && !super.getSignerOrganizationUnit().equals("")) {
            result += "\nSigner Organization Unit: " + super.getSignerOrganizationUnit();
        }

        if (super.getSignerLocation() != null
                && !super.getSignerLocation().equals("")) {
            result += "\nSigner Location: " + super.getSignerLocation();
        }

        result += "\nSigning Date: " + super.getDate();
        result += "\nDocument modified: " + super.isModified();
        result += "\nSigner Identity validated: " + super.isValid();
        if(!super.isValid()) {
            result+= "\nInvalid Reason: " + super.getInvalidReason();
        }

        if (super.getComment() != null
                && !super.getComment().equals("")) {
            result += "\nComment: " + super.getComment();
        }
        return result;
    }

}

