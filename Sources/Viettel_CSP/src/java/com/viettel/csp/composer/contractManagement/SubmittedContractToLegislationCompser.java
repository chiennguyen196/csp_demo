/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.composer.contractManagement;

import com.viettel.csp.DTO.ContactPointDTO;
import com.viettel.csp.DTO.ContractDTO;
import com.viettel.csp.DTO.ContractFilesDTO;
import com.viettel.csp.DTO.ContractHisDTO;
import com.viettel.csp.DTO.ContractStatusDTO;
import com.viettel.csp.DTO.PartnerDTO;
import com.viettel.csp.composer.contract.ContractComposer;
import com.viettel.csp.constant.ContractStatus;
import com.viettel.csp.service.ContactPointService;
import com.viettel.csp.service.ContactService;
import com.viettel.csp.service.ContactTypeService;
import com.viettel.csp.service.ContractFilesService;
import com.viettel.csp.service.ContractHisService;
import com.viettel.csp.service.ContractService;
import com.viettel.csp.service.ContractStatusService;
import com.viettel.csp.service.PartnerService;
import com.viettel.csp.service.UserService;
import com.viettel.csp.util.GenComponent;
import com.viettel.csp.util.KeyValueBean;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.util.StringUtils;
import com.viettel.eafs.util.SpringUtil;
import com.viettel.logincsp.AuthenticationService;
import com.viettel.plc.widget.FileInfo;
import com.viettel.plc.widget.UploadComponent;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.ForwardEvent;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 * @author chitv@gemvietnam.com
 */
public class SubmittedContractToLegislationCompser extends GenericForwardComposer<Component> {

    final static org.apache.log4j.Logger logger = org.apache.log4j.Logger
        .getLogger(ContractManagerPopupEditToCSPComposer.class);
    ContactPointService contactPointService = SpringUtil
        .getBean("contactPointService", ContactPointService.class);
    List<ContactPointDTO> contractPointDTOs;
    private AuthenticationService authenticationService = SpringUtil
        .getBean("authenticationService", AuthenticationService.class);
    private ContractService contractService = SpringUtil
        .getBean("contractService", ContractService.class);
    private PartnerService partnerService = SpringUtil
        .getBean("partnerService", PartnerService.class);
    private ContractStatusService contractStatusService = SpringUtil
        .getBean("contractStatusService", ContractStatusService.class);
    private ContractHisService contractHisService = SpringUtil
        .getBean("contractHisService", ContractHisService.class);
    private ContractFilesService contractFilesService = SpringUtil
        .getBean("contractFilesService", ContractFilesService.class);
    private ContactTypeService contactTypeService = SpringUtil
        .getBean("contactTypeService", ContactTypeService.class);
    private ContactService contactService = SpringUtil
        .getBean("contactService", ContactService.class);
    private UserService userService = SpringUtil.getBean("userService", UserService.class);
    private Label txtContractNumber, txtCompanyName, txtHeadOfService, txtServiceCode, dbStartDate, dbEndDate, txtContractStatus, txtContractType, txtCtSignForm;
    private Combobox cbbContractStatus;
    private Textbox txtContenReason;
    private ListModelList<KeyValueBean> kvbContractStatus;
    private List<KeyValueBean> lstContractStatus;
    private String action;
    private Long contractId, partner_ID;
    private ArrayList indexLst;
    private ContractDTO oldDTO;
    private PartnerDTO partNerDTO;
    private ContractFilesDTO contractFilesDTO;
    private ContractHisDTO contractHisDTO;
    private Listbox lbxContractFiles, lbxContractFilesPartner, lbxContractFilesHistory;
    private ContractManagementComposer contractManagementComposer;
    private UploadComponent uploadComponent;
    private List<FileInfo> filesInfo;
    private Long totalRow = 0L;
    private GenComponent genComponent = new GenComponent();
    private Map<String, String> mapParent = new HashMap<>();
    private Grid grdContactPoint;

    @Override
    public void doAfterCompose(Component comp) {
        try {
            super.doAfterCompose(comp);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            Clients
                .showNotification(LanguageBundleUtils.getString("global.message.error"), "warning",
                    null, "middle_center", 3000);
            return;
        }
        if (arg.containsKey(ParamUtils.COMPOSER)) {
            contractManagementComposer = (ContractManagementComposer) arg.get(ParamUtils.COMPOSER);
        }
        if (arg.containsKey(ParamUtils.ACTION)) {
            action = arg.get(ParamUtils.ACTION).toString();
        }
        if (arg.containsKey("CONTRACT_ID") && StringUtils
            .isValidString(String.valueOf(arg.get("ID")))) {
            contractId = (Long) arg.get("CONTRACT_ID");
        }
        if (arg.containsKey("PARTNER_ID")) {
            partner_ID = (Long) arg.get("PARTNER_ID");
        }
        if (arg.containsKey("LIST_ACTION_INDEX")) {
            indexLst = (ArrayList) arg.get("LIST_ACTION_INDEX");
        }
        if (arg.containsKey("MAP_SERVER_GROUP")) {
            this.mapParent = (Map<String, String>) arg.get("MAP_SERVER_GROUP");
        }

        oldDTO = (ContractDTO) arg.get("CONTRACT");

        setData();
    }

    public void bindDataCbbContractStatus(String valueSelect) throws Exception {
        try {
            List<ContractStatusDTO> lstContractStatusDTO = contractStatusService.findAll();
            lstContractStatus = new ArrayList<>();
            lstContractStatus.add(new KeyValueBean(ParamUtils.DEFAULT_VALUE_STR,
                LanguageBundleUtils.getString("global.combobox.choose")));
            if (lstContractStatusDTO != null && lstContractStatusDTO.size() > 0) {
                for (ContractStatusDTO contractStatusDTO : lstContractStatusDTO) {
                    KeyValueBean key = new KeyValueBean();
                    key.setKey(contractStatusDTO.getCode());
                    key.setValue(contractStatusDTO.getName());
                    lstContractStatus.add(key);
                }
            }
            kvbContractStatus = new ListModelList(lstContractStatus);
            int i = 0;
            for (KeyValueBean itemParam : kvbContractStatus.getInnerList()) {
                if (itemParam.getKey() != null && String.valueOf(itemParam.getKey())
                    .equals(valueSelect)) {
                    kvbContractStatus.addToSelection(kvbContractStatus.getElementAt(i));
                    break;
                }
                i++;
            }
            cbbContractStatus.setModel(kvbContractStatus);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            Clients
                .showNotification(LanguageBundleUtils.getString("global.message.error"), "warning",
                    null, "middle_center", 3000);
        }
    }

    public void setData() {
        if (ParamUtils.ACTION_CREATE.equals(action)) {
            ((Window) self).setTitle(LanguageBundleUtils.getString("global.add"));

        } else if (ParamUtils.ACTION_UPDATE.equals(action)) {

            ((Window) self).setTitle(LanguageBundleUtils.getString("global.edit"));
            if (oldDTO != null) {
                bindDataToTabContractFiles(oldDTO.getContractId());
                bindDataToTabContractHis(oldDTO.getContractId());
                bindDataToTabContractFilesPartner(oldDTO.getContractId());
                txtCompanyName.setValue(oldDTO.getCompanyName());
                txtContractNumber.setValue(oldDTO.getAddressHeadOffice());
                txtHeadOfService.setValue(oldDTO.getPartnerCode());
                txtServiceCode.setValue(oldDTO.getAddressTradingOffice());
                String date = "dd/MM/yyyy";
                String dateStrStart = null;
                String dateStrEnd = null;
                DateFormat df = new SimpleDateFormat(date);
                dateStrStart = df.format(oldDTO.getStartDate());
                dateStrEnd = df.format(oldDTO.getEndDate());
                dbStartDate.setValue(dateStrStart);
                dbEndDate.setValue(dateStrEnd);
                txtCtSignForm.setValue(oldDTO.getCtSignForm());
                txtContractStatus.setValue(oldDTO.getContractStatusName());
                txtContractType.setValue(oldDTO.getContractType());
                this.contractPointDTOs = this.contactPointService.getList(contractId);
                if (this.contractPointDTOs != null && !this.contractPointDTOs.isEmpty()) {
                    ListModelList model = new ListModelList(this.contractPointDTOs);
                    model.setMultiple(false);
                    grdContactPoint.setModel(model);
                }
            }
        }
    }

    public void onDownloadFileContract(ForwardEvent evt) {
        evt.stopPropagation();
        try {
            Image btn = (Image) evt.getOrigin().getTarget();
            Listitem litem = (Listitem) btn.getParent().getParent();
            List<ContractFilesDTO> lstContractFilesResult = new ArrayList<>();
            lstContractFilesResult.add(litem.getValue());
            File file = new File(lstContractFilesResult.get(0).getPathFile(),
                lstContractFilesResult.get(0).getPathFile());
            Filedownload.save(file, null);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(ContractComposer.class.getName())
                .log(Level.SEVERE, null, ex);
        }
    }

    private void bindDataToTabContractFiles(Long contractId) {
        try {
            ContractFilesDTO objectContractFiles = new ContractFilesDTO();
            objectContractFiles.setContractId(contractId);
            List<ContractFilesDTO> lstContractFilesResult = contractFilesService
                .getListContractFiles(objectContractFiles);
            if (lstContractFilesResult != null && !lstContractFilesResult.isEmpty()) {
                ListModelList model = new ListModelList(lstContractFilesResult);
                lbxContractFiles.setModel(model);
            } else {
                lbxContractFiles.setModel(new ListModelList());
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    private void bindDataToTabContractFilesPartner(Long contractId) {
        try {
            ContractFilesDTO objectContractFiles = new ContractFilesDTO();
            objectContractFiles.setContractId(contractId);
            List<ContractFilesDTO> lstContractFilesResult = contractFilesService
                .getListContractFiles(objectContractFiles);
            if (lstContractFilesResult != null && !lstContractFilesResult.isEmpty()) {
                ListModelList model = new ListModelList(lstContractFilesResult);
                lbxContractFilesPartner.setModel(model);
            } else {
                lbxContractFilesPartner.setModel(new ListModelList());
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    private void bindDataToTabContractHis(Long contractId) {
        try {
            ContractHisDTO objectContractHis = new ContractHisDTO();
            objectContractHis.setContractId(contractId);
            List<ContractHisDTO> lstContractHisResult = contractHisService
                .getListContractHis(objectContractHis);
            if (lstContractHisResult != null && !lstContractHisResult.isEmpty()) {
                ListModelList model = new ListModelList(lstContractHisResult);
                lbxContractFilesHistory.setModel(model);
            } else {
                lbxContractFilesHistory.setModel(new ListModelList());
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    public ContractDTO getDataFromClient() {
        oldDTO.setCompanyName(txtCompanyName.getValue().trim().toLowerCase());
        oldDTO.setContractNumber(txtContractNumber.getValue().trim());
        oldDTO.setHeadOfService(txtHeadOfService.getValue().trim());
        oldDTO.setServiceCode(txtServiceCode.getValue().trim());
        oldDTO.setContractStatusCode(txtContractStatus.getValue().trim());
        oldDTO.setContractType(txtContractType.getValue().trim());
        oldDTO.setCtSignForm(txtCtSignForm.getValue().trim());
        return oldDTO;
    }

    public PartnerDTO getDataFromClientPartner(ContractDTO contractDTO) {
        if (ParamUtils.ACTION_CREATE.equals(action)) {
            partNerDTO = new PartnerDTO();
            partNerDTO.setCompanyName(txtCompanyName.getValue().trim().toLowerCase());
        } else {
            if (ParamUtils.ACTION_UPDATE.equals(action)) {
                partNerDTO = new PartnerDTO();
                partNerDTO.setId(contractDTO.getPartnerId());
                partNerDTO.setCompanyName(contractDTO.getCompanyName());
                partNerDTO.setAddressHeadOffice(contractDTO.getAddressHeadOffice());
                partNerDTO.setPartnerCode(contractDTO.getPartnerCode());
                partNerDTO.setAddressTradingOffice(contractDTO.getAddressTradingOffice());
                partNerDTO.setCompanyNameShort(contractDTO.getCompanyNameShort());
                partNerDTO.setPhone(contractDTO.getPhone());
                partNerDTO.setRepName(contractDTO.getRepName());
            }
        }

        return partNerDTO;
    }

    public void onClick$btnClose() {
        self.detach();
    }

    public void onClick$btnReject() {
        if (validate()) {
            Map<String, Object> argu = new HashMap();
            argu.put(ParamUtils.ACTION, ParamUtils.ACTION_UPDATE);
            argu.put("CONTRACT", this.oldDTO);
            argu.put(ParamUtils.COMPOSER, this);
            Window wd = (Window) Executions
                .createComponents("/controls/contractManagement/contractPCReasonPopup.zul", null,
                    argu);
            wd.doModal();
        }
    }

    public void onClick$btnAccept() {
        if (validate()) {
            Map<String, Object> argu = new HashMap();
            argu.put(ParamUtils.ACTION, ParamUtils.ACTION_UPDATE);
            argu.put("CONTRACT", oldDTO);
            argu.put(ParamUtils.COMPOSER, this);
            Window wd = (Window) Executions.createComponents(
                "/controls/contractManagement/contractManagementSignPdfReasonPopup.zul", null,
                argu);
            wd.doModal();
            self.detach();
            contractManagementComposer.bindData();
        }
    }

    public void onPCAccept(Event event) {
        boolean result = contractService.pCAccept(oldDTO);
        if (result) {
            Clients
                .showNotification(LanguageBundleUtils.getString("global.message.create.successful"),
                    "info", null, "middle_center", 1000);
            self.detach();
        } else {
            Clients
                .showNotification(LanguageBundleUtils.getString("global.message.error"), "warning",
                    null, "middle_center", 3000);
        }
    }

    public void onClick$btnCreateSignaturePartner() {
        try {
            String messageContract = "";
            int flat = 12;
            if (validateFile()) {
                if (filesInfo.size() > 0) {
                    for (int i = 0; i < filesInfo.size(); i++) {
                        oldDTO = getDataFromClient();
                        contractFilesDTO = getDataFromClientContractFiles(oldDTO, filesInfo.get(i));
                        partNerDTO = getDataFromClientPartner(oldDTO);
                        partnerService.insert(partNerDTO);
                        contractFilesService.insert(oldDTO, contractFilesDTO);
                        messageContract = contractService.update(oldDTO, action, flat);
                    }
                }

                if (ParamUtils.SUCCESS.equals(messageContract)) {
                    contractManagementComposer.bindDataToGrid();
                    Clients.showNotification(
                        LanguageBundleUtils.getString("global.message.create.successful"), "info",
                        null, "middle_center", 1000);
                    self.detach();
                } else {
                    Clients
                        .showNotification(messageContract, "warning", null, "middle_center", 3000);
                }
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients
                .showNotification(LanguageBundleUtils.getString("global.message.error"), "warning",
                    null, "middle_center", 3000);
        }
    }

    public void onClick$btnCreateReplayContract() {
        try {
            Map<String, Object> argu = new HashMap();
            argu.put(ParamUtils.ACTION, ParamUtils.ACTION_UPDATE_CONTRACT);
            argu.put("LIST_CONTRACT", oldDTO);
            argu.put("ID", oldDTO.getContractId());
            argu.put(ParamUtils.COMPOSER, this);
            Window wd;
            wd = (Window) Executions
                .createComponents("/controls/contractManagement/contractManagerFile.zul", null,
                    argu);
            wd.doModal();
            contractManagementComposer.bindDataToGrid();
            self.detach();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    public ContractHisDTO getDataFromClientContractHis(ContractDTO contractDTO) {

        contractHisDTO = new ContractHisDTO();
        contractHisDTO.setContractId(oldDTO.getContractId());
        contractHisDTO.setContent(txtContenReason.getValue());
        return contractHisDTO;
    }

    public ContractFilesDTO getDataFromClientContractFiles(ContractDTO contractDTO,
        FileInfo fileInfo) {

        contractFilesDTO = new ContractFilesDTO();
        contractFilesDTO.setContractId(oldDTO.getContractId());
        contractFilesDTO.setFileName(new java.io.File(fileInfo.getLocation()).getName());
        contractFilesDTO.setPathFile(new java.io.File(fileInfo.getLocation()).getPath());
        contractFilesDTO.setDescription(fileInfo.getDescription());
        return contractFilesDTO;
    }

    private boolean validateFile() {
        if (filesInfo == null) {
            Clients.showNotification(LanguageBundleUtils.getString("global.message.errorFile"),
                "warning", null, "middle_center", 3000);
            return false;
        }
        return true;
    }

    private boolean validate() {
        return true;
    }

    public String formatDateTime(Date date) throws Exception {
        return StringUtils.formatDateTime(date);
    }

    public String getContractStatusHisName(String paramType) throws Exception {
        return ContractStatus.getContractStatusName(paramType);
    }

    public Listbox getLbxContractFiles() {
        return lbxContractFiles;
    }

    public void setLbxContractFiles(Listbox lbxContractFiles) {
        this.lbxContractFiles = lbxContractFiles;
    }

    public Listbox getLbxContractFilesPartner() {
        return lbxContractFilesPartner;
    }

    public void setLbxContractFilesPartner(Listbox lbxContractFilesPartner) {
        this.lbxContractFilesPartner = lbxContractFilesPartner;
    }

    public Listbox getLbxContractFilesHistory() {
        return lbxContractFilesHistory;
    }

    public void setLbxContractFilesHistory(Listbox lbxContractFilesHistory) {
        this.lbxContractFilesHistory = lbxContractFilesHistory;
    }

    public ListModelList<KeyValueBean> getKvbContractStatus() {
        return kvbContractStatus;
    }

    public void setKvbContractStatus(ListModelList<KeyValueBean> kvbContractStatus) {
        this.kvbContractStatus = kvbContractStatus;
    }

    public List<KeyValueBean> getLstContractStatus() {
        return lstContractStatus;
    }

    public void setLstContractStatus(List<KeyValueBean> lstContractStatus) {
        this.lstContractStatus = lstContractStatus;
    }

    public Combobox getCbbContractStatus() {
        return cbbContractStatus;
    }

    public void setCbbContractStatus(Combobox cbbContractStatus) {
        this.cbbContractStatus = cbbContractStatus;
    }

    public ContactPointService getContactPointService() {
        return contactPointService;
    }

    public void setContactPointService(ContactPointService contactPointService) {
        this.contactPointService = contactPointService;
    }
}
