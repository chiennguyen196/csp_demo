/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.composer.contract;

import com.viettel.csp.DTO.*;
import com.viettel.csp.entity.ContractHisEntity;
import com.viettel.csp.service.*;
import com.viettel.csp.util.*;
import com.viettel.eafs.util.SpringUtil;
import com.viettel.logincsp.AuthenticationService;
import com.viettel.plc.widget.FileInfo;
import com.viettel.plc.widget.IUploaderCallback;
import com.viettel.plc.widget.UploadComponent;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.ForwardEvent;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.*;

import java.io.File;
import java.util.*;

/**
 * @author chitv@gemvietnam.com
 */
public class ContractCovenantFileComposer extends GenericForwardComposer<Component> implements
        IUploaderCallback {

    private AuthenticationService authenticationService = SpringUtil.getBean("authenticationService", AuthenticationService.class);

    private Label txtAddressHeadOffice, txtPartnerCode, txtAddressTradingOffice, txtCompanyNameShort, txtPhone, txtRepName, dbFilingDate, txtContractStatus;
    private Textbox txtContractNumber, txtCompanyName, txtHeadOfService;
    private Combobox cbServiceCode, cboContractType;
    private Textbox txtContenReason;
    private List<KeyValueBean> lstReason;
    private ListModelList<KeyValueBean> kvbGroupServiceTree;
    private ListModelList<KeyValueBean> kvbContractType;
    private List<KeyValueBean> lstServiceName;
    private ListModelList<KeyValueBean> kvbServiceName;
    private Datebox dbStartDate, dbEndDate;
    private String action;
    private Long contractId;
    private ArrayList indexLst;
    private ContractDTO oldDTO;
    private PartnerDTO partNerDTO;
    private ContractService contractService = SpringUtil.getBean("contractService", ContractService.class);
    private PartnerService partnerService = SpringUtil.getBean("partnerService", PartnerService.class);
    private ContractStatusService contractStatusService = SpringUtil.getBean("contractStatusService", ContractStatusService.class);
    private ContractHisService contractHisService = SpringUtil.getBean("contractHisService", ContractHisService.class);
    private ContractFilesService contractFilesService = SpringUtil.getBean("contractFilesService", ContractFilesService.class);
    private ServiceService service = SpringUtil.getBean("serviceService", ServiceService.class);
    final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ContractCovenantFileComposer.class);
    private ContractComposer contractComposer;
    private ContractPopupEditComposer contractPopupEditComposer;
    private Listbox lbxViewAddContract;
    private List<ContractFilesDTO> lstContractFiles = new ArrayList<>();
    private UploadComponent uploadComponent;
    private ContractFilesDTO contractFilesDTO;
    private ContractHisDTO contractHisDTO;
    private List<FileInfo> filesInfo;
//    private Datebox dbFilingDate;

    public List<ContractFilesDTO> getLstContractFiles() {
        return lstContractFiles;
    }

    public void setLstContractFiles(List<ContractFilesDTO> lstContractFiles) {
        this.lstContractFiles = lstContractFiles;
    }

    public List<KeyValueBean> getLstReason() {
        return lstReason;
    }

    public void setLstReason(List<KeyValueBean> lstReason) {
        this.lstReason = lstReason;
    }

    public ListModelList<KeyValueBean> getKvbGroupServiceTree() {
        return kvbGroupServiceTree;
    }

    public void setKvbGroupServiceTree(ListModelList<KeyValueBean> kvbGroupServiceTree) {
        this.kvbGroupServiceTree = kvbGroupServiceTree;
    }

    public ListModelList<KeyValueBean> getKvbContractType() {
        return kvbContractType;
    }

    public void setKvbContractType(ListModelList<KeyValueBean> kvbContractType) {
        this.kvbContractType = kvbContractType;
    }

    public List<KeyValueBean> getLstServiceName() {
        return lstServiceName;
    }

    public void setLstServiceName(List<KeyValueBean> lstServiceName) {
        this.lstServiceName = lstServiceName;
    }

    public ListModelList<KeyValueBean> getKvbServiceName() {
        return kvbServiceName;
    }

    public void setKvbServiceName(ListModelList<KeyValueBean> kvbServiceName) {
        this.kvbServiceName = kvbServiceName;
    }

    public Listbox getLbxViewAddContract() {
        return lbxViewAddContract;
    }

    public void setLbxViewAddContract(Listbox lbxViewAddContract) {
        this.lbxViewAddContract = lbxViewAddContract;
    }

    @Override
    public void doAfterCompose(Component comp) {
        try {
            super.doAfterCompose(comp);
            if (arg.containsKey(ParamUtils.COMPOSER)) {
                contractPopupEditComposer = (ContractPopupEditComposer) arg.get(ParamUtils.COMPOSER);
            }
            if (arg.containsKey(ParamUtils.ACTION)) {
                action = arg.get(ParamUtils.ACTION).toString();
            }
            if (arg.containsKey("ID") && StringUtils.isValidString(String.valueOf(arg.get("ID")))) {
                contractId = (Long) arg.get("ID");
            }
            if (arg.containsKey("LISTFILE")) {
                lstContractFiles = (ArrayList<ContractFilesDTO>) arg.get("LISTFILE");
            }
            uploadComponent.setUploaderCallback(this);
            setData();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    public void setData() throws Exception {
        ContractDTO objectSearch = new ContractDTO();
        objectSearch.setContractId(contractId);
        List<ContractDTO> list = contractService.getListContractProfile(objectSearch, authenticationService.getUserCredential());
        oldDTO = list.get(0);
        if (ParamUtils.ACTION_CREATE.equals(action)) {
            ((Window) self).setTitle(LanguageBundleUtils.getString("global.add"));
            loadComboboxServiceCode(ParamUtils.DEFAULT_VALUE_STR);
            cboContractType.setSelectedIndex(0);
            txtCompanyName.setValue(oldDTO.getCompanyName());
            txtContractStatus.setValue(LanguageBundleUtils.getString("global.AddContractFiles"));
        } else if (ParamUtils.ACTION_UPDATE.equals(action)) {
            ((Window) self).setTitle(LanguageBundleUtils.getString("global.create.covenanfile"));
            if (oldDTO != null) {
                txtCompanyName.setValue(oldDTO.getCompanyName());
                txtContractStatus.setValue(oldDTO.getContractStatusName());
                txtHeadOfService.setValue(oldDTO.getHeadOfService());
                loadComboboxServiceCode(oldDTO.getServiceCode());
                if (StringUtils.isValidString(oldDTO.getContractType())) {
                    GenComponent component = new GenComponent();
                    component.setSelectedCbo(cboContractType, oldDTO.getContractType());
                }
            }
        } else if (ParamUtils.ACTION_UPDATE_FILE.equals(action)) {
            if (lstContractFiles != null && !lstContractFiles.isEmpty()) {
                ListModelList model = new ListModelList(lstContractFiles);
                model.setMultiple(true);
                lbxViewAddContract.setModel(model);
            }
        }
    }

    public String formatDateTime(Date date) throws Exception {
        return StringUtils.formatDateTime(date);
    }

    public void loadComboboxServiceCode(String valueSelect) throws Exception {
        try {
            List<ServiceDTO> listServiceDTO = service.getListServiceChild();
            lstServiceName = new ArrayList<>();
            lstServiceName.add(new KeyValueBean(ParamUtils.DEFAULT_VALUE_STR, LanguageBundleUtils.getString("global.combobox.choose")));
            if (listServiceDTO != null && listServiceDTO.size() > 0) {
                for (ServiceDTO serviceDTO : listServiceDTO) {
                    KeyValueBean key = new KeyValueBean();
                    key.setKey(serviceDTO.getCode());
                    key.setValue(serviceDTO.getName());
                    lstServiceName.add(key);
                }
            }
            kvbServiceName = new ListModelList(lstServiceName);
            int i = 0;
            for (KeyValueBean itemParam : kvbServiceName.getInnerList()) {
                if (itemParam.getKey() != null && String.valueOf(itemParam.getKey()).equals(valueSelect)) {
                    kvbServiceName.addToSelection(kvbServiceName.getElementAt(i));
                    break;
                }
                i++;
            }
            cbServiceCode.setModel(kvbServiceName);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    public ContractDTO getDataFromClient() {
        oldDTO.setCompanyName(txtCompanyName.getValue().trim().toLowerCase());
        oldDTO.setContractNumber(txtContractNumber.getValue().trim().toLowerCase());
        if (cbServiceCode.getSelectedIndex() > 0) {
            oldDTO.setServiceCode(cbServiceCode.getSelectedItem().getValue());
        }
        if (cboContractType.getSelectedIndex() > 0) {
            oldDTO.setContractType(cboContractType.getSelectedItem().getValue());
        }
        oldDTO.setStartDate(dbStartDate.getValue());
        oldDTO.setEndDate(dbEndDate.getValue());
        oldDTO.setHeadOfService(txtHeadOfService.getValue().trim());

        return oldDTO;
    }

    public void setDataGrid(ContractFilesDTO contractFilesDTO) {

        lstContractFiles.add(contractFilesDTO);
        ListModelList model = new ListModelList(lstContractFiles);
        model.setMultiple(true);
        lbxViewAddContract.setModel(model);
    }

    public void onClick$btnCreateCovenant() {
        try {
            String message = "";
            int flat = ParamUtils.TYPE_ACTION.WAITING_SEND_CONTRACT_FILES;
            uploadComponent.onClick$doSave();
            if (validate()) {
                oldDTO = getDataFromClient();
                if (filesInfo != null && filesInfo.size() > 0) {
                    for (int i = 0; i < filesInfo.size(); i++) {
                        contractFilesDTO = getDataFromClientContractFiles(oldDTO, filesInfo.get(i));
                        oldDTO.setContractStatus(ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_OK_CONTRACT_FILES_FOR_PARTNER);
                        contractFilesService.insert(oldDTO, contractFilesDTO);
                    }
                }
                if (lbxViewAddContract != null && lbxViewAddContract.getSelectedCount() > 0) {
                    for (int i = 0; i < lbxViewAddContract.getSelectedCount(); i++) {
                        oldDTO.setContractStatus(ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_CREATE_REGIS_OK_CONTRACT_FILES);
                        contractFilesService.insert(oldDTO, lstContractFiles.get(i));
                    }
                }
                ContractHisEntity hisEntity = new ContractHisEntity(contractId, LanguageBundleUtils.getString("message.profile.ok.create.covenantfile"));
                contractHisService.insertEntity(hisEntity);
                message = contractService.updateContract(oldDTO, ParamUtils.ACTION_UPDATE, flat);
                if (ParamUtils.SUCCESS.equals(message)) {
                    Clients.showNotification(LanguageBundleUtils.getString("global.message.create.successful"), "info", null, "middle_center", 1000);
                    self.detach();
                    contractPopupEditComposer.onClick$btnClose();
                } else {
                    Clients.showNotification(message, "warning", null, "middle_center", 3000);
                }
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    public void onRemoveFilePopup(ForwardEvent evt) {
        evt.stopPropagation();
        try {
            Image btn = (Image) evt.getOrigin().getTarget();
            Listitem litem = (Listitem) btn.getParent().getParent();
            if (litem.getIndex() >= 0) {
                lstContractFiles.remove(lstContractFiles.get(litem.getIndex()));
                lbxViewAddContract.removeItemAt(litem.getIndex());
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    public void onClick$btnAddFilePopup() {
        try {
            Map<String, Object> argu = new HashMap();
            argu.put(ParamUtils.ACTION, ParamUtils.ACTION_CREATE);
            argu.put(ParamUtils.COMPOSER, this);
            Window wd;
            wd = (Window) Executions.createComponents("/controls/contract/contractAddFile.zul", null, argu);
            wd.doModal();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    public void onViewFilePopup(ForwardEvent evt) {
        evt.stopPropagation();
        try {
            Image btn = (Image) evt.getOrigin().getTarget();
            Listitem litem = (Listitem) btn.getParent().getParent();
            Map<String, Object> argu = new HashMap();
            argu.put(ParamUtils.ACTION, ParamUtils.ACTION_UPDATE_FILE);
            argu.put("CONTRACT_FILE", litem.getValue());
            argu.put(ParamUtils.COMPOSER, this);
            lstContractFiles.remove(lstContractFiles.get(litem.getIndex()));
            lbxViewAddContract.removeItemAt(litem.getIndex());
            Window wd;
            wd = (Window) Executions.createComponents("/controls/contract/contractAddFile.zul", null, argu);
            wd.doModal();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    public ContractHisDTO getDataFromClientContractHis(ContractDTO contractDTO) {

        contractHisDTO = new ContractHisDTO();
        contractHisDTO.setContractId(oldDTO.getContractId());
        contractHisDTO.setContent(txtContenReason.getValue());
        return contractHisDTO;
    }

    public void onClick$btnCloseContract() {
        try {
            self.detach();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    private boolean validate() {
        if (!CtrlValidator.checkValidTextbox(txtContractNumber, true, true, LanguageBundleUtils.getString("contract.contractNumber"))) {
            return false;
        }

        if (!StringUtils.isValidDate(dbStartDate.getValue())) {
            Clients.wrongValue(dbStartDate, LanguageBundleUtils.getMessage("global.validate.notformat.date.ddMMyyyy", LanguageBundleUtils.getString("contract.startDate")));
            return false;
        }

        if (!StringUtils.isValidDate(dbEndDate.getValue())) {
            Clients.wrongValue(dbEndDate, LanguageBundleUtils.getMessage("global.validate.notformat.date.ddMMyyyy", LanguageBundleUtils.getString("contract.endDate")));
            return false;
        }

        if (!CtrlValidator.checkStartEndDate(dbStartDate, dbEndDate, true, LanguageBundleUtils.getString("contract.startDate"), LanguageBundleUtils.getString("contract.endDate"))) {
            return false;
        }


        if (!CtrlValidator.checkValidComboValue(cbServiceCode, true, true, LanguageBundleUtils.getString("reflect.cbServiceName"))) {
            return false;
        }


        if (!CtrlValidator.checkValidComboValue(cboContractType, true, true, LanguageBundleUtils.getString("contract.contractType"))) {
            return false;
        }


        if (!CtrlValidator.checkValidTextbox(txtHeadOfService, true, true, LanguageBundleUtils.getString("contract.headOfService"))) {
            return false;
        }

        if (!contractService.checkValidateContractNumber(txtContractNumber.getValue())) {
            Clients.wrongValue(txtContractNumber, LanguageBundleUtils.getMessage("contract.profile.contractNumber.is.exist", LanguageBundleUtils.getString("contract.endDate")));
            return false;
        }

        if (this.filesInfo == null) {
            Clients.showNotification(LanguageBundleUtils.getString("global.message.errorFile"), "warning", null, "middle_center", 3000);
            return false;
        }
        return true;
    }

    public ContractFilesDTO getDataFromClientContractFiles(ContractDTO contractDTO, FileInfo fileInfo) {
        contractFilesDTO = new ContractFilesDTO();
        contractFilesDTO.setContractId(oldDTO.getContractId());
        contractFilesDTO.setFileName(new File(fileInfo.getLocation()).getName());
        contractFilesDTO.setPathFile(new File(fileInfo.getLocation()).getPath());
        contractFilesDTO.setDescription(fileInfo.getDescription());
        return contractFilesDTO;
    }

    @Override
    public void uploadedFilesInfoCallback(List<FileInfo> filesInfo) {
        this.filesInfo = filesInfo;
        System.out.println(filesInfo);
    }

    @Override
    public void uploadedFilesLocationCallback(List<String> filesLocation) {
        System.out.println(filesLocation);
    }
}
