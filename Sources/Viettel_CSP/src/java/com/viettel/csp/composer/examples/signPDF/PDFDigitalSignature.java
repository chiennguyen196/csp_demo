/*
 * Copyright (C) 2010 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.viettel.csp.composer.examples.signPDF;

import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfDate;
import com.itextpdf.text.pdf.PdfDictionary;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfPKCS7;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfSignature;
import com.itextpdf.text.pdf.PdfSignatureAppearance;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfString;
import com.viettel.csp.composer.examples.signPDF.certificate.OcspClient;
import com.viettel.csp.composer.examples.signPDF.certificate.OcspClientBouncyCastle;
import com.viettel.csp.composer.examples.signPDF.certificate.X509ExtensionUtil;
import com.viettel.csp.composer.examples.signPDF.certificate.X509VerificationUtil;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.security.KeyException;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bouncycastle.ocsp.CertificateStatus;
import org.bouncycastle.ocsp.RevokedStatus;
import org.bouncycastle.ocsp.UnknownStatus;

/**
 * this class does all signing to PDF
 *
 * @author TuanNM33@viettel.com.vn
 * @since 17/09/2013
 * @version 1.0
 */
public class PDFDigitalSignature extends ClientServerSignature implements DigitalSignature {

    Logger logger = Logger.getLogger(PDFDigitalSignature.class.getName());
    private PdfSignatureAppearance psa;
    private ResourceBundle resbundConfigICTemplate = ResourceBundle.getBundle("configICTemplate");

    @Override
    public void sign(URL fileURL, PrivateKey key, Certificate[] chain, SignatureParameter parameter, OutputStream os)
            throws Exception {
        checkInput(key, chain, os);
        PdfReader reader = new PdfReader(fileURL);
        sign(reader, key, chain, parameter, os);
    }

    @Override
    public void sign(String file, PrivateKey key, Certificate[] chain, SignatureParameter parameter, OutputStream os)
            throws Exception {
        //check the input
        checkInput(key, chain, os);
        PdfReader reader = new PdfReader(file);
        sign(reader, key, chain, parameter, os);

    }

    private void sign(PdfReader reader, PrivateKey key, Certificate[] chain, SignatureParameter parameter, OutputStream os)
            throws Exception {
        PdfStamper stamper = PdfStamper.createSignature(reader, null, '\0', null, true);
        PdfSignatureAppearance sap = stamper.getSignatureAppearance();
        Font font2 = new Font(BaseFont.createFont(this.getClass().getResource("times.ttf").getPath(), "Identity-H", false));
        font2.setColor(0xff, 0, 0);
        sap.setLayer2Font(font2);
        sap.setCrypto(key, chain, null, PdfSignatureAppearance.WINCER_SIGNED);
        if (parameter != null) {
            if (parameter.getComment() != null
                    && !parameter.getComment().equals("")) {
                sap.setReason(parameter.getComment());
            }
            // position of signature
            if (parameter instanceof PdfSignatureParameter) {
                PdfSignatureParameter psp = (PdfSignatureParameter) parameter;

                if (psp.getSignerLocation() != null
                        && !psp.getSignerLocation().equals("")) {
                    sap.setLocation(psp.getSignerLocation());
                }

                if (psp.getPosition() != null) {
                    if (psp.getPage() > 0 && psp.getPage() <= reader.getNumberOfPages()) {

                        if (psp.getSignerName() != null
                                && !psp.getSignerName().equals("")) {
                            // comment next line to have an invisible signature
                            sap.setVisibleSignature(psp.getPosition(), psp.getPage(), psp.getSignerName());
                        } else {
                            sap.setVisibleSignature(psp.getPosition(), psp.getPage(), null);
                        }
                    } else if (psp.getPage() == 0) {
                        stamper.insertPage(reader.getNumberOfPages() + 1, reader.getPageSize(1));
                        if (psp.getSignerName() != null
                                && !psp.getSignerName().equals("")) {

                            // comment next line to have an invisible signature
                            sap.setVisibleSignature(psp.getPosition(), reader.getNumberOfPages(), psp.getSignerName());

                        } else {
                            sap.setVisibleSignature(psp.getPosition(), reader.getNumberOfPages(), null);
                        }

                    }
                }
            }

        }
        sap.setOriginalout(os);
        stamper.close();
    }

    @Override
    public List<Verifier> verify(URL fileURL, Certificate certificate) throws Exception {
        if (fileURL == null) {
            throw new NullPointerException("PDF file is not null");
        }
        PdfReader reader = new PdfReader(fileURL);
        return verify(reader, certificate);
    }

    @Override
    public List<Verifier> verify(String fileName, Certificate certificate) throws Exception {
        if (fileName == null || fileName.equals("")) {
            throw new NullPointerException("PDF file is not null");
        }
        PdfReader reader = new PdfReader(fileName);
        return verify(reader, certificate);
    }

    private List<Verifier> verify(PdfReader reader, Certificate certificate) throws Exception {
        if (null == certificate) {
            throw new NullPointerException("Certificate is not null");
        }
        if (false == (certificate instanceof X509Certificate)) {
            throw new DigitalSignatureException(" only support X509 certificate");
        }
        List<Verifier> verifiers = new ArrayList<Verifier>();
        AcroFields af = reader.getAcroFields();
        ArrayList names = getSortedName(af);
        for (int k = 0; k < names.size(); ++k) {
            String name = (String) names.get(k);

            // End revision extraction
            PdfPKCS7 pk = af.verifySignature(name);
            Calendar calendar = pk.getSignDate();
            Certificate[] chain = pk.getSignCertificateChain();
            checkCertificateChain(chain);

            String signer = X509ExtensionUtil.getSubject((X509Certificate) chain[0]);
            String location = X509ExtensionUtil.getLocation((X509Certificate) chain[0]);
            Object[] res = X509VerificationUtil.verifyCertificates(chain, certificate);

            //new verifier
            Verifier verifier = new EngVerifier(signer, calendar.getTime(), !pk.verify(), location);
            verifier.setComment(pk.getReason());
            if (res != null) {
                String valid = "";
                for (Object o : res) {
                    if (o instanceof X509Certificate) {
                        X509Certificate cert = (X509Certificate) o;
                        valid += cert.getSubjectDN();
                    } else if (o instanceof String) {
                        valid += " " + o;
                    }

                }
                verifier.setInvalidReason(valid);
            } else {
                if (chain.length > 1) {
                    X509Certificate inner = (X509Certificate) chain[0];
                    X509Certificate ca = (X509Certificate) chain[1];
                    String ocspurl = X509ExtensionUtil.getOCSPURL(inner);
                    if (ocspurl != null) {
                        try {
                            OcspClient oc = new OcspClientBouncyCastle(inner, ca, ocspurl);
                            CertificateStatus cs = oc.checkOCSP();
                            if (cs == CertificateStatus.GOOD) {
                                verifier.setValid(true);
                            } else if (cs instanceof RevokedStatus) {
                                verifier.setInvalidReason("revoked certificate");
                            } else if (cs instanceof UnknownStatus) {
                                verifier.setInvalidReason("unknown online status");
                            }
                        } catch (Exception ex) {
                            verifier.setInvalidReason("can not check certificate online because of connection");
                            logger.log(Level.SEVERE, ex.getMessage(), ex);
                        }
                    } else {
                        verifier.setInvalidReason("certificate is not support check online");
                    }
                } else {
                    verifier.setInvalidReason("Not enough certificates to check online");
                }
            }
            verifiers.add(verifier);
        }

        return verifiers;
    }

    @Override
    public List<Verifier> verify(URL fileURL, KeyStore keystore) throws Exception {
        PdfReader reader = new PdfReader(fileURL);
        return verify(reader, keystore);
    }

    @Override
    public List<Verifier> verify(String fileName, KeyStore keystore) throws Exception {
        PdfReader reader = new PdfReader(fileName);
        return verify(reader, keystore);
    }

    private List<Verifier> verify(PdfReader reader, KeyStore keystore) throws Exception {
        List<Verifier> verifiers = new ArrayList<Verifier>();
        AcroFields af = reader.getAcroFields();
        ArrayList names = getSortedName(af);
        for (int k = 0; k < names.size(); ++k) {
            String name = (String) names.get(k);

            // End revision extraction
            PdfPKCS7 pk = af.verifySignature(name);
            Calendar calendar = pk.getSignDate();
            Certificate[] chain = pk.getSignCertificateChain();
            checkCertificateChain(chain);

            String signer = X509ExtensionUtil.getSubject((X509Certificate) chain[0]);
            String location = X509ExtensionUtil.getLocation((X509Certificate) chain[0]);
            Verifier verifier = new EngVerifier(signer, calendar.getTime(), !pk.verify(), location);
            verifier.setComment(pk.getReason());
            String res = X509VerificationUtil.verifyCertificates(chain, keystore);
            if (res != null) {
                verifier.setInvalidReason(res);
            } else {
                if (chain.length > 1) {
                    X509Certificate inner = (X509Certificate) chain[0];
                    X509Certificate ca = (X509Certificate) chain[1];
                    String ocspurl = X509ExtensionUtil.getOCSPURL(inner);
                    if (ocspurl != null) {
                        try {
                            OcspClient oc = new OcspClientBouncyCastle(inner, ca, ocspurl);
                            CertificateStatus cs = oc.checkOCSP();
                            if (cs == CertificateStatus.GOOD) {
                                verifier.setValid(true);
                            } else if (cs instanceof RevokedStatus) {
                                verifier.setInvalidReason("revoked certificate");
                            } else if (cs instanceof UnknownStatus) {
                                verifier.setInvalidReason("unknown online status");
                            }
                        } catch (Exception ex) {
                            verifier.setInvalidReason("can not check certificate online because of connection");
                            logger.log(Level.SEVERE, ex.getMessage(), ex);
                        }
                    } else {
                        verifier.setInvalidReason("certificate is not support check online");
                    }
                } else {
                    verifier.setInvalidReason("Not enough certificates to check online");
                }
            }
            verifiers.add(verifier);
        }

        return verifiers;
    }

    @Override
    public String getFirstIssuerCertificate(String fileName) throws Exception {
        return getFirstIssuerCertificate(new PdfReader(fileName));
    }

    @Override
    public String getFirstIssuerCertificate(URL fileURL) throws Exception {
        return getFirstIssuerCertificate(new PdfReader(fileURL));
    }

    public int getSignatureNumbers(String fileName) throws Exception {
        return getSignatureNumbers(new PdfReader(fileName));
    }

    public int getSignatureNumbers(URL fileURL) throws Exception {
        return getSignatureNumbers(new PdfReader(fileURL));
    }

    private int getSignatureNumbers(PdfReader reader) {
        AcroFields af = reader.getAcroFields();
        return getSortedName(af).size();
    }

    private String getFirstIssuerCertificate(PdfReader reader) throws Exception {
        AcroFields af = reader.getAcroFields();
        ArrayList names = getSortedName(af);
        if (names.size() >= 1) {
            String name = (String) names.get(0);

            // End revision extraction
            PdfPKCS7 pk = af.verifySignature(name);
            Certificate[] chain = pk.getSignCertificateChain();
            X509Certificate certificate = (X509Certificate) chain[0];
            return X509ExtensionUtil.getIssuerName(certificate);
        }
        return null;
    }

    @Override
    public SignatureObject createDigest(URL fileURL, SignatureParameter parameter, Certificate[] chain) throws Exception {
        if (fileURL == null) {
            throw new NullPointerException("Pdf file is not null");
        }
        PdfReader reader = new PdfReader(fileURL);
        return createDigest(reader, parameter, chain, false);
    }

    @Override
    public SignatureObject createDigest(String inFile, SignatureParameter parameter, Certificate[] chain, boolean checkLogo) throws Exception {
        if (inFile == null || inFile.equals("")) {
            throw new NullPointerException("Pdf file is not null");
        }
        PdfReader reader = new PdfReader(inFile);
        return createDigest(reader, parameter, chain, checkLogo);
    }

    @Override
    public byte[] createHash(String inFile, SignatureParameter parameter, Certificate[] chain) throws Exception {
        if (inFile == null || inFile.equals("")) {
            throw new NullPointerException("Pdf file is not null");
        }
        PdfReader reader = new PdfReader(inFile);
        return createHash(reader, parameter, chain);
    }

    @Override
    public byte[] createHash(InputStream is, SignatureParameter parameter, Certificate[] chain) throws Exception {
        if (is == null) {
            throw new NullPointerException("Pdf file is not null");
        }
        PdfReader reader = new PdfReader(is);
        return createHash(reader, parameter, chain);
    }

    // Them moi
    public SignatureObject createDigest(byte[] arrayBytes, SignatureParameter parameter, Certificate[] chain) throws Exception {
        if (arrayBytes == null || arrayBytes.length == 0) {
            throw new NullPointerException("Pdf file is not null");
        }
        PdfReader reader = new PdfReader(arrayBytes);
        return createDigest(reader, parameter, chain, false);
    }

    private SignatureObject createDigest(PdfReader reader, SignatureParameter parameter, Certificate[] chain, boolean checkLogo) throws Exception {
        PdfStamper pdfStamper = PdfStamper.createSignature(reader, null, '\0', null, true);
        PdfSignatureAppearance pdfSignatureAppearance = pdfStamper.getSignatureAppearance();
        //======= Them moi ngay 22/01/2014
        Font font2 = new Font(BaseFont.createFont(resbundConfigICTemplate.getString("font_sign_ca"), "Identity-H", false));
        font2.setColor(0xff, 0, 0);
        pdfSignatureAppearance.setLayer2Font(font2);
        if (checkLogo) {
            Image img = Image.getInstance("/images/icon-delete.png");
            pdfSignatureAppearance.setAcro6Layers(true);
            pdfSignatureAppearance.setImage(null);
            pdfSignatureAppearance.setSignatureGraphic(img);
            pdfSignatureAppearance.setRenderingMode(PdfSignatureAppearance.RenderingMode.GRAPHIC);
        }
        //=======
        pdfSignatureAppearance.setCrypto(null, chain, null, PdfSignatureAppearance.WINCER_SIGNED);
        if (parameter != null) {
            if (parameter.getComment() != null
                    && !parameter.getComment().equals("")) {
                pdfSignatureAppearance.setReason(parameter.getComment());
            }
            // position of signature
            if (parameter instanceof PdfSignatureParameter) {
                PdfSignatureParameter psp = (PdfSignatureParameter) parameter;

                if (psp.getSignerLocation() != null
                        && !psp.getSignerLocation().equals("")) {
                    pdfSignatureAppearance.setLocation(psp.getSignerLocation());
                }

                if (psp.getPosition() != null) {
                    if (psp.getPage() > 0 && psp.getPage() <= reader.getNumberOfPages()) {
                        if (psp.getSignerPage().equals("top")) {
                            if (psp.getSignerName() != null
                                    && !psp.getSignerName().equals("")) {
                                // TuanNM33: comment next line to have an invisible signature
                                pdfSignatureAppearance.setVisibleSignature(psp.getPosition(), psp.getPage(), psp.getSignerName());
                            } else {
                                // comment
                                pdfSignatureAppearance.setVisibleSignature(psp.getPosition(), psp.getPage(), null);
                            }
                        } else if (psp.getSignerPage().equals("top.middle")) {
                            if (psp.getSignerName() != null
                                    && !psp.getSignerName().equals("")) {
                                // TuanNM33: comment next line to have an invisible signature
                                pdfSignatureAppearance.setVisibleSignature(psp.getPosition(), reader.getNumberOfPages(), psp.getSignerName());
                            } else {
                                // comment
                                pdfSignatureAppearance.setVisibleSignature(psp.getPosition(), reader.getNumberOfPages(), null);
                            }
                        }

                        if (psp.getSignerPage().equals("bottom")) {
                            if (psp.getSignerName() != null
                                    && !psp.getSignerName().equals("")) {
                                // TuanNM33: comment next line to have an invisible signature
                                pdfSignatureAppearance.setVisibleSignature(psp.getPosition(), reader.getNumberOfPages(), psp.getSignerName());
                            } else {
                                // comment
                                pdfSignatureAppearance.setVisibleSignature(psp.getPosition(), reader.getNumberOfPages(), null);
                            }
                        } else if (psp.getSignerPage().equals("bottom.middle")) {
                            if (psp.getSignerName() != null
                                    && !psp.getSignerName().equals("")) {
                                // TuanNM33: comment next line to have an invisible signature
                                pdfSignatureAppearance.setVisibleSignature(psp.getPosition(), reader.getNumberOfPages() - 1, psp.getSignerName());
                            } else {
                                // comment
                                pdfSignatureAppearance.setVisibleSignature(psp.getPosition(), reader.getNumberOfPages() - 1, null);
                            }
                        }

                    } else if (psp.getPage() == 0) {
                        if (psp.getSignerPage().equals("top")) {
                            pdfStamper.insertPage(1, reader.getPageSize(1)); // test ky vao trang chen phia truoc

                            if (psp.getSignerName() != null
                                    && !psp.getSignerName().equals("")) {

                                // TuanNM33: comment next line to have an invisible signature
                                pdfSignatureAppearance.setVisibleSignature(psp.getPosition(), 1, psp.getSignerName());
                            } else {
                                pdfSignatureAppearance.setVisibleSignature(psp.getPosition(), 1, null);
                            }
                        } else if (psp.getSignerPage().equals("top.directorInsert")) {
                            pdfStamper.insertPage(1, reader.getPageSize(1)); // test ky vao trang chen phia truoc

                            if (psp.getSignerName() != null
                                    && !psp.getSignerName().equals("")) {

                                // TuanNM33: comment next line to have an invisible signature
                                pdfSignatureAppearance.setVisibleSignature(psp.getPosition(), reader.getNumberOfPages(), psp.getSignerName());
                            } else {
                                pdfSignatureAppearance.setVisibleSignature(psp.getPosition(), reader.getNumberOfPages(), null);
                            }
                        }

                        if (psp.getSignerPage().equals("bottom")) {
                            pdfStamper.insertPage(reader.getNumberOfPages() + 1, reader.getPageSize(1));

                            if (psp.getSignerName() != null
                                    && !psp.getSignerName().equals("")) {

                                // TuanNM33: comment next line to have an invisible signature
                                pdfSignatureAppearance.setVisibleSignature(psp.getPosition(), reader.getNumberOfPages(), psp.getSignerName());
                            } else {
                                pdfSignatureAppearance.setVisibleSignature(psp.getPosition(), reader.getNumberOfPages(), null);
                            }
                        }

                    }
                }
            }

        }
        PdfSignature dic = new PdfSignature(PdfName.ADOBE_PPKLITE, new PdfName("adbe.pkcs7.detached"));
        dic.setReason(pdfSignatureAppearance.getReason());
        dic.setLocation(pdfSignatureAppearance.getLocation());
        dic.setDate(new PdfDate(pdfSignatureAppearance.getSignDate()));
        pdfSignatureAppearance.setCryptoDictionary(dic);
        // preserve some space for the contents
        final int contentEstimated = 15000;
        HashMap<PdfName, Integer> exc = new HashMap<PdfName, Integer>();
        exc.put(PdfName.CONTENTS, new Integer(contentEstimated * 2 + 2));

        /*
         * preclose, not close
         */
        pdfSignatureAppearance.preClose(exc);

        /*
         * save the global variable to use in session
         */
        this.psa = pdfSignatureAppearance;

        // make the digest
        InputStream data = pdfSignatureAppearance.getRangeStream();

        //LOGGER.debug(out.toString());
        MessageDigest messageDigest = MessageDigest.getInstance("SHA1");
        /*
         * buffer size
         */
        final int size = 8192;
        byte[] buf = new byte[size];
        int n;
        while ((n = data.read(buf)) > 0) {
            messageDigest.update(buf, 0, n);
        }
        byte[] hash = messageDigest.digest();

        SignatureObject object = new SignatureObject();
        object.setDigest(hash);
        object.setOOXML(false);
        return object;
    }

    private byte[] createHash(PdfReader reader, SignatureParameter parameter, Certificate[] chain) throws Exception {
        PdfStamper pdfStamper = PdfStamper.createSignature(reader, null, '\0', null, true);
        PdfSignatureAppearance pdfSignatureAppearance = pdfStamper.getSignatureAppearance();
        pdfSignatureAppearance.setCrypto(null, chain, null, PdfSignatureAppearance.WINCER_SIGNED);
        if (parameter != null) {
            if (parameter.getComment() != null
                    && !parameter.getComment().equals("")) {
                pdfSignatureAppearance.setReason(parameter.getComment());
            }
            // position of signature
            if (parameter instanceof PdfSignatureParameter) {
                PdfSignatureParameter psp = (PdfSignatureParameter) parameter;

                if (psp.getSignerLocation() != null
                        && !psp.getSignerLocation().equals("")) {
                    pdfSignatureAppearance.setLocation(psp.getSignerLocation());
                }

                if (psp.getPosition() != null) {
                    if (psp.getPage() > 0 && psp.getPage() <= reader.getNumberOfPages()) {

                        if (psp.getSignerName() != null
                                && !psp.getSignerName().equals("")) {
                            // comment next line to have an invisible signature
                            pdfSignatureAppearance.setVisibleSignature(psp.getPosition(), psp.getPage(), psp.getSignerName());
                        } else {
                            //comment
                            pdfSignatureAppearance.setVisibleSignature(psp.getPosition(), psp.getPage(), null);
                        }
                    } else if (psp.getPage() == 0) {
                        pdfStamper.insertPage(reader.getNumberOfPages() + 1, reader.getPageSize(1));
                        if (psp.getSignerName() != null
                                && !psp.getSignerName().equals("")) {

                            // comment next line to have an invisible signature
                            pdfSignatureAppearance.setVisibleSignature(psp.getPosition(), reader.getNumberOfPages(), psp.getSignerName());
                        } else {
                            pdfSignatureAppearance.setVisibleSignature(psp.getPosition(), reader.getNumberOfPages(), null);
                        }

                    }
                }
            }

        }
        PdfSignature dic = new PdfSignature(PdfName.ADOBE_PPKLITE, new PdfName("adbe.pkcs7.detached"));
        dic.setReason(pdfSignatureAppearance.getReason());
        dic.setLocation(pdfSignatureAppearance.getLocation());
        dic.setDate(new PdfDate(pdfSignatureAppearance.getSignDate()));
        pdfSignatureAppearance.setCryptoDictionary(dic);
        // preserve some space for the contents
        final int contentEstimated = 15000;
        HashMap<PdfName, Integer> exc = new HashMap<PdfName, Integer>();
        exc.put(PdfName.CONTENTS, new Integer(contentEstimated * 2 + 2));

        /*
         * preclose, not close
         */
        pdfSignatureAppearance.preClose(exc);

        /*
         * save the global variable to use in session
         */
        this.psa = pdfSignatureAppearance;

        // make the digest
        InputStream data = pdfSignatureAppearance.getRangeStream();

        //LOGGER.debug(out.toString());
        MessageDigest messageDigest = MessageDigest.getInstance("SHA1");
        /*
         * buffer size
         */
        final int size = 8192;
        byte[] buf = new byte[size];
        int n;
        while ((n = data.read(buf)) > 0) {
            messageDigest.update(buf, 0, n);
        }
        byte[] hash = messageDigest.digest();

        return hash;
    }

    @Override
    public void insertSignature(SignatureObject object, String outFile) throws Exception {
        byte[] signature = object.getSignature();
        if (signature == null) {
            throw new DigitalSignatureException("digest has not signed");
        }
        if (object.isOOXML()) {
            throw new DigitalSignatureException("OOXML signature object");
        }
        FileOutputStream os = new FileOutputStream(outFile);
        this.psa.setOriginalout(os);
        PdfDictionary dictionary = new PdfDictionary();
        dictionary.put(PdfName.CONTENTS, new PdfString(signature).setHexWriting(true));
        this.psa.close(dictionary);
    }

    public FileOutputStream getSignedFileAfterInsertSignature(SignatureObject object, String outFile) throws Exception {
        byte[] signature = object.getSignature();
        if (signature == null) {
            throw new DigitalSignatureException("digest has not signed");
        }
        if (object.isOOXML()) {
            throw new DigitalSignatureException("OOXML signature object");
        }
        FileOutputStream os = new FileOutputStream(outFile);
        this.psa.setOriginalout(os);
        PdfDictionary dictionary = new PdfDictionary();
        dictionary.put(PdfName.CONTENTS, new PdfString(signature).setHexWriting(true));
        this.psa.close(dictionary);
        return os;
    }

    @Override
    public void insertSignature(byte[] hash, String outFile) throws Exception {
        if (hash == null) {
            throw new DigitalSignatureException("digest has not signed");
        }
        FileOutputStream os = new FileOutputStream(outFile);
        this.psa.setOriginalout(os);
        PdfDictionary dictionary = new PdfDictionary();
        dictionary.put(PdfName.CONTENTS, new PdfString(hash).setHexWriting(true));
        this.psa.close(dictionary);
    }

    private ArrayList<String> getSortedName(AcroFields af) {
        ArrayList<String> names = af.getSignatureNames();
        int numberOfName = af.getTotalRevisions();
        ArrayList results = new ArrayList<String>();
        for (int i = numberOfName; i >= 1; --i) {
            for (int j = 0; j < names.size(); ++j) {
                if (af.getRevision(names.get(j)) == i) {
                    results.add(names.get(j));
                }
            }
        }
        return results;
    }

    private void checkInput(PrivateKey key, Certificate[] chain, OutputStream fos)
            throws DigitalSignatureException, KeyException {
        if (key == null) {
            throw new NullPointerException("Private key is not null");
        }

        if (fos == null) {
            throw new NullPointerException("Output stream is not null");
        }

        if (!key.getAlgorithm().equals("RSA")) {
            throw new KeyException("Just use RSA key for safety reason");
        }

        checkCertificateChain(chain);

    }

    private void checkCertificateChain(Certificate[] chain) throws DigitalSignatureException {
        if (chain == null
                || chain.length < 1) {
            throw new NullPointerException("Certificate chain is not null");
        }

        for (int i = 0; i < chain.length; ++i) {
            if (false == (chain[i] instanceof X509Certificate)) {
                throw new DigitalSignatureException("Certificate chain is not X.509 type");
            }
        }
    }

    public List<X509Certificate> getCertificate(String fileName) throws Exception {
        return null;
    }
}
