package com.viettel.csp.composer.liquidation;

import com.viettel.csp.DTO.ContractDTO;
import com.viettel.csp.DTO.ContractStatusDTO;
import com.viettel.csp.composer.partner.PartnerComposer;
import com.viettel.csp.service.ContractService;
import com.viettel.csp.service.ContractStatusService;
import com.viettel.csp.service.PartnerService;
import com.viettel.csp.service.UserService;
import com.viettel.csp.util.*;
import com.viettel.eafs.util.SpringUtil;
import com.viettel.logincsp.AuthenticationService;
import org.apache.log4j.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.*;

import java.util.*;

/**
 * @author Chitv@gemvietnam.com
 */
public class LiquidationComposer extends GenericForwardComposer<Component> {

    private static final Logger logger = Logger.getLogger(PartnerComposer.class);
    private ListModelList<ContractDTO> listContract = new ListModelList<>();
    private ListModelList<KeyValueBean> kvbContractStatus;
    private List<ContractDTO> lstResult;
    private Listbox lbxLiquidation;
    private Textbox txtCompanyName, txtCompanyNameShort, txtContractNumber;
    private Grid gridSearch;
    private Combobox cbbContractStatus;
    private Map<String, String> mapParent = new HashMap<>();
    private AuthenticationService authenticationService = SpringUtil
            .getBean("authenticationService", AuthenticationService.class);
    private ContractService contractService = SpringUtil
            .getBean("contractService", ContractService.class);
    private PartnerService partnerService = SpringUtil
            .getBean("partnerService", PartnerService.class);
    private UserService userService = SpringUtil.getBean("userService", UserService.class);
    private ContractStatusService contractStatusService = SpringUtil
            .getBean("contractStatusService", ContractStatusService.class);

    public ListModelList<ContractDTO> getListContract() {
        return this.listContract;
    }

    public void setListContract(ListModelList<ContractDTO> listContract) {
        this.listContract = listContract;
    }

    public ListModelList<KeyValueBean> getKvbContractStatus() {
        return this.kvbContractStatus;
    }

    public void setKvbContractStatus(ListModelList<KeyValueBean> kvbContractStatus) {
        this.kvbContractStatus = kvbContractStatus;
    }

    public List<ContractDTO> getLstResult() {
        return this.lstResult;
    }

    public void setLstResult(List<ContractDTO> lstResult) {
        this.lstResult = lstResult;
    }

    public Listbox getlbxLiquidation() {
        return this.lbxLiquidation;
    }

    public void setlbxLiquidation(Listbox lbxLiquidation) {
        this.lbxLiquidation = lbxLiquidation;
    }

    public Textbox getTxtCompanyNameShort() {
        return this.txtCompanyNameShort;
    }

    public void setTxtCompanyNameShort(Textbox txtCompanyNameShort) {
        this.txtCompanyNameShort = txtCompanyNameShort;
    }

    public Textbox getTxtContractNumber() {
        return this.txtContractNumber;
    }

    public void setTxtContractNumber(Textbox txtContractNumber) {
        this.txtContractNumber = txtContractNumber;
    }

    public PartnerService getPartnerService() {
        return this.partnerService;
    }

    public void setPartnerService(PartnerService partnerService) {
        this.partnerService = partnerService;
    }

    public Textbox getTxtCompanyName() {
        return this.txtCompanyName;
    }

    public void setTxtCompanyName(Textbox txtCompanyName) {
        this.txtCompanyName = txtCompanyName;
    }

    public Grid getGridSearch() {
        return this.gridSearch;
    }

    public void setGridSearch(Grid gridSearch) {
        this.gridSearch = gridSearch;
    }

    public Combobox getCbbContractStatus() {
        return this.cbbContractStatus;
    }

    public void setCbbContractStatus(Combobox cbbContractStatus) {
        this.cbbContractStatus = cbbContractStatus;
    }

    public ContractService getContractService() {
        return this.contractService;
    }

    public void setContractService(ContractService contractService) {
        this.contractService = contractService;
    }

    public UserService getUserService() {
        return this.userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public ContractStatusService getContractStatusService() {
        return this.contractStatusService;
    }

    public void setContractStatusService(ContractStatusService contractStatusService) {
        this.contractStatusService = contractStatusService;
    }

    @Override
    public void doAfterCompose(Component comp) {
        try {
            super.doAfterCompose(comp);
            this.lbxLiquidation.setMultiple(true);
            bindDataToCombo();
//            onClick$btnSearch();
            bindData();
        } catch (Throwable ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    public void bindDataToCombo() {
        try {
            List<KeyValueBean> lstKey = new ArrayList<KeyValueBean>();
            List<KeyValueBean> lstKeyValueBean = contractStatusService.getListToKeyValueBean(true);
            if (lstKeyValueBean != null && lstKeyValueBean.size() > 0) {
                for (KeyValueBean bean : lstKeyValueBean) {
                    String key = String.valueOf(bean.getKey());
                    List<String> list = Arrays.asList(ParamUtils.TYPE_ACTION.LIST_ACTION_LIQUIDATION);
                    if (list.contains(key)) {
                        lstKey.add(bean);
                    }
                }
                kvbContractStatus = new ListModelList(lstKey);
                kvbContractStatus.addToSelection(kvbContractStatus.getElementAt(0));
                cbbContractStatus.setModel(kvbContractStatus);
            } else {
                cbbContractStatus.setModel(new ListModelList());
            }
            this.cbbContractStatus
                    .setValue(LanguageBundleUtils.getString("global.combobox.choose"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ContractDTO getDataFromClient() {
        ContractDTO object = new ContractDTO();

        if (StringUtils.isValidString(this.txtCompanyName.getValue())) {
            object.setCompanyName(this.txtCompanyName.getValue().trim());
        }
        if (StringUtils.isValidString(this.txtCompanyNameShort.getValue())) {
            object.setCompanyNameShort(this.txtCompanyNameShort.getValue().trim());
        }
        if (StringUtils.isValidString(this.txtContractNumber.getValue())) {
            object.setContractNumber(this.txtContractNumber.getValue().trim());
        }
        if (this.cbbContractStatus.getSelectedIndex() > 0) {
            object.setContractStatusCode(this.cbbContractStatus.getSelectedItem().getValue());
        }
        return object;
    }

    public void onClick$btnSearch() {
        bindData();
    }

    public void bindData() {
        ContractDTO contractDTO = getDataFromClient();
        if (UserTokenUtils.getPersonType().equals(ParamUtils.USER_TYPE.VIETTEL)) {

        }
        this.lstResult = this.contractService.getListLiquidation(contractDTO);
        if (this.lstResult != null && !this.lstResult.isEmpty()) {
            ListModelList model = new ListModelList(this.lstResult);
            model.setMultiple(true);
            this.lbxLiquidation.setModel(model);
            if (this.gridSearch != null) {
                this.gridSearch.invalidate();
            }
        } else {
            this.lbxLiquidation.setModel(new ListModelList());
        }
    }

    public void onClick$btnEdit() {

        try {
            if (this.lbxLiquidation.getSelectedCount() == 1) {
                Map<String, Object> argu = new HashMap();
                argu.put(ParamUtils.ACTION, ParamUtils.ACTION_UPDATE);
                List<Integer> indexs = new ArrayList();
                indexs.add(this.lbxLiquidation.getSelectedItem().getIndex());
                ContractDTO contactDTO = (ContractDTO) this.lbxLiquidation
                        .getItemAtIndex(this.lbxLiquidation.getSelectedItem().getIndex())
                        .getValue();
                String contractStatus;
                contractStatus =
                        this.lstResult.get(this.lbxLiquidation.getSelectedIndex())
                                .getContractStatus();
                argu.put("LIST_ACTION_INDEX", indexs);
                argu.put("LISTBOX", this.lbxLiquidation);
                argu.put("ID",
                        this.lstResult.get(this.lbxLiquidation.getSelectedIndex())
                                .getContractId());
                argu.put("CONTRACT_ID", contactDTO.getContractId());
                argu.put("PARTNER_ID", contactDTO.getPartnerId());
                argu.put("CONTRACTDTO", contactDTO);
                argu.put(ParamUtils.COMPOSER, this);
                Window wd;
                switch (contractStatus) {

                    case ParamUtils.LI_STATUS.LIQUIDATION_CREAT:
                        if (UserTokenUtils.getPersonType().equals(ParamUtils.USER_TYPE.VIETTEL)) {
                            wd = (Window) Executions
                                    .createComponents(
                                            "/controls/liquidation/liquidationUpdateContract.zul", null,
                                            argu);
                            wd.doModal();
                        }
                        break;

                    case ParamUtils.LI_STATUS.LIQUIDATION_SENDTOVT:
                        if (UserTokenUtils.getPersonType().equals(ParamUtils.USER_TYPE.PARTNER)) {
                            wd = (Window) Executions
                                    .createComponents(
                                            "/controls/liquidation/liquidationSendToVT.zul", null,
                                            argu);
                            wd.doModal();
                        }
                        break;

                    case ParamUtils.LI_STATUS.LIQUIDATION_ASSIGN_HD:
                        if (UserTokenUtils.getPersonType().equals(ParamUtils.USER_TYPE.VIETTEL)) {
                            wd = (Window) Executions
                                    .createComponents(
                                            "/controls/liquidation/liquidationSuggestAssign.zul", null,
                                            argu);
                            wd.doModal();
                        }
                        break;

                    case ParamUtils.LI_STATUS.LIQUIDATION_ASSIGN_REGISTER:
                        if (UserTokenUtils.getPersonType().equals(ParamUtils.USER_TYPE.PARTNER)) {
                            wd = (Window) Executions
                                    .createComponents(
                                            "/controls/liquidation/liquidationRegisted.zul", null,
                                            argu);
                            wd.doModal();
                        }
                        break;

                    case ParamUtils.LI_STATUS.LIQUIDATION_REGISTERED:
                        if (UserTokenUtils.getPersonType().equals(ParamUtils.USER_TYPE.VIETTEL)) {
                            wd = (Window) Executions
                                    .createComponents(
                                            "/controls/liquidation/liquidationSendToPartner.zul", null,
                                            argu);
                            wd.doModal();
                        }
                        break;

                    case ParamUtils.LI_STATUS.LIQUIDATION_SEND_TO_PARTNER:
                        if (UserTokenUtils.getPersonType().equals(ParamUtils.USER_TYPE.PARTNER)) {
                            wd = (Window) Executions
                                    .createComponents(
                                            "/controls/liquidation/liquidationPartnerRegisted.zul", null,
                                            argu);
                            wd.doModal();
                        }
                        break;

                    case ParamUtils.LI_STATUS.LIQUIDATION_PARTNER_REGISTER:
                        if (UserTokenUtils.getPersonType().equals(ParamUtils.USER_TYPE.VIETTEL)) {
                            wd = (Window) Executions
                                    .createComponents(
                                            "/controls/liquidation/liquidationEndContract.zul", null,
                                            argu);
                            wd.doModal();
                        }
                        break;
                }
            } else {
                Clients
                        .showNotification(
                                LanguageBundleUtils.getString("global.action.confirm.is_select.edit"),
                                "warning", null, "middle_center", 3000);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    public void onClick$btnDelete() {
        try {
            if (this.lbxLiquidation.getSelectedCount() > 0) {
                Messagebox.show(LanguageBundleUtils.getString("global.action.confirm.multi.delete"),
                        "Confirm Dialog", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
                        new org.zkoss.zk.ui.event.EventListener() {
                            @Override
                            public void onEvent(Event evt) throws InterruptedException, Exception {
                                if ("onOK".equals(evt.getName())) {
                                    List<ContractDTO> lstDeleteDTO = new ArrayList<>();
                                    for (Listitem item : LiquidationComposer.this.lbxLiquidation
                                            .getSelectedItems()) {
                                        ContractDTO deleteDTO = ((ContractDTO) item.getValue());
                                        lstDeleteDTO.add(deleteDTO);
                                    }
                                    String message = LiquidationComposer.this.contractService
                                            .deleteContract(lstDeleteDTO);
//                            String message1 = partnerService.deletePartner(lstDeleteDTO);
                                    bindDataToGrid();
                                    Clients
                                            .showNotification(message, "info", null, "middle_center", 3000);
                                    return;
                                }
                            }
                        });
            } else {
                Clients.showNotification(
                        LanguageBundleUtils.getString("global.action.confirm.is_select"),
                        "warning", null, "middle_center", 3000);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients
                    .showNotification(LanguageBundleUtils.getString("global.message.error"), "warning",
                            null,
                            "middle_center", 3000);
        }
    }

    public void bindDataToGrid() {
        onClick$btnSearch();
    }

    public String formatDateTime(Date date) throws Exception {
        return StringUtils.formatDateTime(date);
    }

//    public void onClick$btnAdd() {
//        try {
//
//            if (this.lbxLiquidation.getSelectedCount() == 1) {
//                Map<String, Object> argu = new HashMap();
//                argu.put(ParamUtils.ACTION, ParamUtils.ACTION_UPDATE);
//                List<Integer> indexs = new ArrayList();
//                indexs.add(this.lbxLiquidation.getSelectedItem().getIndex());
//                ContractDTO contactDTO = (ContractDTO) this.lbxLiquidation
//                        .getItemAtIndex(this.lbxLiquidation.getSelectedItem().getIndex())
//                        .getValue();
//                argu.put("LIST_ACTION_INDEX", indexs);
//                argu.put("LISTBOX", this.lbxLiquidation);
//                argu.put("CONTRACT_ID", contactDTO.getContractId());
//                argu.put(ParamUtils.COMPOSER, this);
//                Window wd;
//                wd = (Window) Executions
//                        .createComponents("/controls/liquidation/liquidationAddContract.zul", null, argu);
//                wd.doModal();
//            } else {
//                Clients
//                        .showNotification(
//                                LanguageBundleUtils.getString("global.action.confirm.is_select.edit"),
//                                "warning", null, "middle_center", 3000);
//            }
//        } catch (Exception ex) {
//            logger.error(ex.getMessage(), ex);
//        }
//    }

    public String getContractStatusHisName(String paramType) throws Exception {
        ContractStatusDTO contractStatusDTO = contractStatusService.getObjectDTOByCode(paramType);
        return contractStatusDTO.getName();
    }

}
