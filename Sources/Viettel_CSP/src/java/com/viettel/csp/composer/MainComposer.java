/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.composer;

import com.viettel.csp.DTO.NotifyDTO;
import com.viettel.csp.DTO.UserDTO;
import com.viettel.csp.constant.ScreenVisibleService;
import com.viettel.csp.service.MessageService;
import com.viettel.csp.service.UserService;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.util.UserTokenUtils;
import com.viettel.eafs.util.SpringUtil;
import com.viettel.logincsp.AuthenticationService;
import com.viettel.logincsp.UserCredential;
import java.awt.BorderLayout;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import org.apache.log4j.Logger;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.ForwardEvent;
import org.zkoss.zk.ui.event.SelectEvent;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Div;
import org.zkoss.zul.Html;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Menu;
import org.zkoss.zul.Menubar;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Menupopup;
import org.zkoss.zul.Popup;
import org.zkoss.zul.Window;
import viettel.passport.client.ObjectToken;
import viettel.passport.client.UserToken;

//import com.viettel.csp.entity.UserEntity;
//import com.viettel.csp.service.catalogue.AppParamsService;

/**
 * breadcumbDiv
 *
 * @author cn_linhntn3
 */
public class MainComposer extends CSPBaseCustomComposer {

    private static Logger logger = Logger.getLogger(MainComposer.class);
    private static Logger exceptionLog = Logger.getLogger(MainComposer.class);
    public Menubar mainMenu;
    private AuthenticationService authenticationService = SpringUtil
        .getBean("authenticationService", AuthenticationService.class);
    private ScreenVisibleService screenVisibleService = SpringUtil
        .getBean("screenVisibleService", ScreenVisibleService.class);
    private BorderLayout mainLayout;
    private org.zkoss.zul.Include bodyLayout;
    private Label user;
    private Include menuinclude;
    private Div breadcumbDiv;
    private Listbox notifyList;
    private Div home;
    private List<NotifyDTO> allNotify;
    private Popup showMessage;
    private List<String> strList;
    private List<NotifyDTO> nonSeeNotifyList;
    private int countNewNotify;
    //TuanNM33
    private boolean isPrivilege;
    //End TuanNM33
//    private static UtilService utilService = SpringUtil.getBean("UtilService", UtilService.class);
    private UserService userService = SpringUtil.getBean("userService", UserService.class);
    private MessageService messageService = SpringUtil
        .getBean("messageService", MessageService.class);

    public Listbox getNotifyList() {
        return notifyList;
    }

    public void setNotifyList(Listbox notifyList) {
        this.notifyList = notifyList;
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        UserCredential userCredential = UserTokenUtils.getCustomUserToken();
        if (userCredential != null && userCredential.getUserId() != null) {
            user.setValue(UserTokenUtils.getFullName());
        } else {
            logger.info("timeout!");
            authenticationService.logout();
            Executions.sendRedirect("/login.zul");
            return;
        }
        session.setAttribute(ParamUtils.VSA_USER_TOKEN, new UserToken());
        session.setAttribute("Demo_Locale", "en");

//        //Start Privilege
//        isPrivilege = appParamsService.isPrivilege(session.getAttribute("netID").toString());
//        if (!isPrivilege) {
//            Clients.showNotification(LanguageBundleUtils.getString("global.notPrivilege"), "warning", null, "middle_center", 10000);
//            return;
//        }
        this.menuCreate();
        //End Privilege
        //Nguoi Dung Viettel, con nguoi dung doi tac thi sua o AuthenticationServiceImpl.java
        UserToken vsaUserToken = (UserToken) Sessions.getCurrent()
            .getAttribute(ParamUtils.VSA_USER_TOKEN);
        if (vsaUserToken != null && vsaUserToken.getUserID() != null) {
            UserCredential userToken = new UserCredential();
            userToken.setIp(session.getRemoteAddr());
            userToken.setUserId(vsaUserToken.getUserID());
            userToken.setUserName(vsaUserToken.getUserName());
            userToken.setFullName(vsaUserToken.getFullName());
            userToken.setPersonType(ParamUtils.USER_TYPE.VIETTEL);
            session.setAttribute(ParamUtils.USER_TOKEN, userToken);
        }
    }

    public void onClick$user() {
        Sessions.getCurrent().setAttribute("contactPopup", "ok");
        //Load data to form        
        String username = UserTokenUtils.getUserName();
        UserDTO userDTO = new UserDTO();
        userDTO = userService.getUserDTOByUserName(username);
        Map<String, Object> argu = new HashMap();
        argu.put(ParamUtils.ACTION, ParamUtils.ACTION_UPDATE);
        argu.put("USERNAME", userDTO.getUserName());
        String template = "";
        if (ParamUtils.USER_TYPE.CONTACT.equals(userDTO.getPersonType())) {
            template = "controls/contact/contactPopup.zul";
        } else if (ParamUtils.USER_TYPE.PARTNER.equals(userDTO.getPersonType())) {
            argu.put("PARTNER_ID", userDTO.getId());
            template = "controls/profile/profilePartnerPopup.zul";
        } else {
            return;
        }
        Window window = (Window) Executions.createComponents(template, null, argu);
        window.doModal();
    }

    public void onClick$password() {
        String template = "controls/profile/passwordPopup.zul";
        Window window = (Window) Executions.createComponents(template, null, null);
        window.doModal();
    }

    public void setLanguage(String langLocal) {
        logger.info("Set language " + langLocal);
        Locale prefer_locale = langLocal.length() > 2
            ? new Locale(langLocal.substring(0, 2), langLocal.substring(3)) : new Locale(langLocal);
        session.setAttribute(org.zkoss.web.Attributes.PREFERRED_LOCALE, prefer_locale);
        session.setAttribute("Demo_Locale", langLocal);
        menuCreate();
        execution.sendRedirect("");
    }

    public void onClick$logout() throws UnsupportedEncodingException {
        logger.info("Logout!");
        authenticationService.logout();
//        Locale locale = new Locale("en", "US");
//        ResourceBundle bundle = ResourceBundle.getBundle("cas", locale);
//        String redirectUrl = bundle.getString("logoutUrl") + "?appCode=" + Connector.domainCode + "&service=" + URLEncoder.encode(Connector.serviceURL, "UTF-8");
        execution.sendRedirect("/login.zul");

        //session.removeAttribute("vsaUserToken");
        // Locale locale = new Locale("en", "US");
        //  ResourceBundle bundle = ResourceBundle.getBundle("cas", locale);
        // String redirectUrl = bundle.getString("logoutUrl") + "?appCode=" + Connector.domainCode + "&service=" + URLEncoder.encode(Connector.serviceURL, "UTF-8");
        // execution.sendRedirect(redirectUrl);
    }

    public void onClick$langVN() {
        setLanguage("vi_VN");
    }

    public void onClick$langEN() {
        setLanguage("en_US");
    }

    public void getNumberOfNewNotify() {
        countNewNotify = 0;
        allNotify = messageService.getNotifyList(UserTokenUtils.getUserId());

        for (int i = 0; i < allNotify.size(); i++) {
            if (allNotify.get(i).getStatus() == 0) {
                countNewNotify++;
            }
        }

        nonSeeNotifyList = new ArrayList<>();
        for (NotifyDTO dto : allNotify) {
            if (dto.getStatus() == 0) {
                nonSeeNotifyList.add(dto);
            }
        }
    }

    public void show() {
        getNumberOfNewNotify();
        if (countNewNotify != 0) {
            String message1 = LanguageBundleUtils.getString("notify.message1");
            String message2 = LanguageBundleUtils.getString("notify.message2");
            Clients.showNotification(message1 + " " + String.valueOf(countNewNotify)
                    + " " + message2, Clients.NOTIFICATION_TYPE_WARNING, home, "after_center", 4000,
                true);
        }
    }

    public void showNotifyPopup() {
        strList = new ArrayList<String>();
        String message3 = LanguageBundleUtils.getString("notify.message3");

        if (allNotify == null) {
            getNumberOfNewNotify();
        }

        if (allNotify != null && allNotify.size() != 0) {
            for (NotifyDTO dto : allNotify) {
                dto.setStatus(ParamUtils.NOTIFY_STATUS);
                String message = new String();

                message = dto.getSender().toUpperCase()
                    + " " + dto.getContent().toLowerCase() + " " + message3 + " "
                    + dto.getCreateAt().toString();
                strList.add(message);
            }

            //Show all notify
            ListModelList model = new ListModelList(strList);
            model.setMultiple(true);
            notifyList.setModel(model);
            showMessage.open(5000, 100);

            //update to DB
            if (nonSeeNotifyList.size() != 0) {
                messageService.updateStatus(nonSeeNotifyList);
            }
        }
    }

    public void onSelectItemList(ForwardEvent event) {
        try {
            SelectEvent me = (SelectEvent) event.getOrigin();
            Listbox listbox = (Listbox) me.getTarget();
            Listitem item = listbox.getSelectedItem();
            String url = item.getValue();
            if (url != null) {
                bodyLayout.setSrc(null);
                bodyLayout.setSrc(url);
            }
            listbox.clearSelection();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    //    private Locale getLocale() {
//        Locale locale;
//        try {
//            String langLocal = session.getAttribute("Demo_Locale").toString();
//            locale = langLocal.length() > 2
//                    ? new Locale(langLocal.substring(0, 2), langLocal.substring(3)) : new Locale(langLocal);
//        } catch (Exception e) {
//            locale = Locale.ENGLISH;
//        }
//        return locale;
//    }
    public void menuCreate() {
        logger.info("Menu's created!");
        Component comp;
        while (!mainMenu.getChildren().isEmpty()) {
            comp = mainMenu.getChildren().get(0);
            mainMenu.removeChild(comp);
            comp.detach();
        }
        Locale locale = getLocale();
        ResourceBundle bundle = ResourceBundle.getBundle("cas", new Locale("en", "US"));
        if ("0".equals(bundle.getString("loadMenu"))) {
            menuinclude.setSrc("/controls/menu/menu.zul");
        } else {
            bodyLayout.setSrc("/controls/partner/partner.zul");
            UserToken userToken = (UserToken) Sessions.getCurrent().getAttribute("vsaUserToken");
            ArrayList<ObjectToken> parentMenu = userToken.getParentMenu();

            UserCredential userLogin = (UserCredential) session.getAttribute(ParamUtils.USER_TOKEN);

            //hungpt
            parentMenu.addAll(screenVisibleService.getMenu(userLogin));

            for (ObjectToken obj : parentMenu) {
                if (obj.getChildObjects() != null && obj.getChildObjects().size() != 0) {
                    Menu rootMenu = new Menu();
                    //rootMenu.setLabel(LanguageBundleUtils.getString(locale, obj.getObjectName()));
                    rootMenu.setLabel(Labels.getLabel(obj.getObjectName()));
                    rootMenu.setParent(mainMenu);
                    Menupopup popupAdmin = new Menupopup();
                    popupAdmin.setAttribute("NAME", Labels.getLabel(obj.getObjectName()));
                    popupAdmin.setParent(rootMenu);
                    menuCreation(popupAdmin, obj.getChildObjects(), locale);
                } else {
                    Menuitem itemName = new Menuitem();
                    //itemName.setLabel(LanguageBundleUtils.getString(locale, obj.getObjectName()));
                    itemName.setLabel(Labels.getLabel(obj.getObjectName()));
                    itemName.setLabel(obj.getObjectName());
                    itemName.setAttribute("NAME", Labels.getLabel(obj.getObjectName()));
                    itemName.setAttribute("URL", obj.getObjectUrl());
                    itemName.addEventListener("onClick", new MyListener());
                    itemName.setId("item_" + obj.getObjectId());
                    itemName.setParent(mainMenu);
                }
            }
        }
    }

    public void menuCreation(Menupopup popup, ArrayList<ObjectToken> childObjects, Locale locale) {
        for (ObjectToken obj : childObjects) {
            if (obj.getChildObjects().size() != 0) {
                Menu childMenu = new Menu();
                Menupopup childPopup = new Menupopup();
                //childMenu.setLabel(LanguageBundleUtils.getString(locale, obj.getObjectName()));
                childMenu.setLabel(Labels.getLabel(obj.getObjectName()));
                childPopup.setAttribute("NAME", Labels.getLabel(obj.getObjectName()));
                childPopup.setParent(childMenu);
                childMenu.setParent(popup);
                menuCreation(childPopup, obj.getChildObjects(), locale);

            } else {
                Menuitem itemName = new Menuitem();
                //itemName.setLabel(LanguageBundleUtils.getString(locale, obj.getObjectName()));
                itemName.setLabel(Labels.getLabel(obj.getObjectName()));
                itemName.setAttribute("URL", obj.getObjectUrl());
                itemName.setAttribute("NAME", LanguageBundleUtils.getString(obj.getObjectName()));
                itemName.setAttribute("DES", obj.getDescription());
                itemName.addEventListener("onClick", new MyListener());
                itemName.setId("item_" + obj.getObjectId());
                itemName.setParent(popup);

            }
        }
    }

    class MyListener implements org.zkoss.zk.ui.event.EventListener {

        //        public void onEvent(org.zkoss.zk.ui.event.Event event) {
//            try {
//                Sessions.getCurrent().removeAttribute("ACTION_NOTIFY");
//                Sessions.getCurrent().removeAttribute("ACTION_NOTIFY_OBJ");
//                Sessions.getCurrent().removeAttribute("ACTION_PROCESS_APPROVE");
//                Sessions.getCurrent().removeAttribute("ACTION_PROCESS_APPROVE_OBJ");
////                String controlID = event.getTarget().getId();
//                //Lay URL
//                String URL = (String) event.getTarget().getAttribute("URL");
//                //20150401 TuanNA67: fixed code for report new
//                if ("/controls/report/isdnTopImpactReport.zul".equalsIgnoreCase(URL)) {
//                    URL = "/controls/report/zkReport.zul";
//                }
//                //END 20150401 TuanNA67
//                //Lay danh sach param
//                Map<String, Object> params = null;
//                if (URL != null) {
//                    String[] splitted = URL.split("\\?");
//                    if (splitted.length > 1) {
//                        URL = splitted[0];
//                        params = new HashMap<String, Object>();
//                        splitted = splitted[1].split("=");
//                        for (int i = 0; i < splitted.length - 1; i = i + 2) {
//                            params.put(splitted[i], splitted[i + 1]);
//                        }
//                    }
//                }
//                //Set URL cho bodyLayout
//                if (URL != null) {
//                    if (params != null) {
//                        bodyLayout.setSrc(null);
//                        bodyLayout.setDynamicProperty(ParamUtils.REQUEST_PARAMS, params);
//                    }
//                    bodyLayout.setSrc(URL);
//                    createBreadCumbs((Menuitem) event.getTarget());
//                }
//                //Messagebox.show(URL);
//            } catch (Exception ex) {
//                StringBuilder strError = new StringBuilder(new Date().toString())
//                        .append("|").append(this.getClass().getSimpleName())
//                        .append("|").append(ParamUtils.DEFAULT_EXCEPTION_LEVEL)
//                        .append("|").append(ex.getMessage()).append("|").append(ParamUtils.DEFAULT_ERR_CODE);
//                exceptionLog.error(strError);
//                logger.error(ex.getMessage(), ex);
//                Window window = (Window) Executions.createComponents("/error.zul", self, null);
//                window.doModal();
//            }
//        }
        public void onEvent(org.zkoss.zk.ui.event.Event event) {
            try {
                String controlID = event.getTarget().getId();
                String URL = (String) event.getTarget().getAttribute("URL");
                if (URL != null) {
                    bodyLayout.setSrc(URL);
                }

            } catch (Exception ex) {
                ex.getMessage();
            }
        }

        private void createBreadCumbs(Menuitem menuItem) {
            String caption = (String) menuItem.getAttribute("NAME");
            if (caption != null && menuItem.getParent() instanceof Menupopup) {
                //Remove all child of current breadcrumbs
                List<Component> lstChilds = breadcumbDiv.getChildren();
                for (int i = lstChilds.size() - 1; i >= 0; --i) {
                    Component child = lstChilds.get(i);
                    breadcumbDiv.removeChild(child);
                }
                //Build breadcrumbs inner html
                StringBuilder liHtml = new StringBuilder();
                liHtml.append("<li><div class=\"current\">").append(caption).append("</div></li>");
                //Menupopup menuPopup = (Menupopup) menuItem.getParent();
                Component tmpComponent = menuItem.getParent();
                String tmp;
                while (tmpComponent != null) {
                    //Neu parent dang la Menu -> trace them 1 muc nua
                    if (tmpComponent instanceof Menu) {
                        tmpComponent = tmpComponent.getParent();
                    }
                    //Neu parent la MenuPopup -> lay name de build breadcrumbs
                    if (tmpComponent != null && tmpComponent instanceof Menupopup) {
                        caption = (String) tmpComponent.getAttribute("NAME");
                        if (caption != null) {
                            tmp = "<li><div>" + caption + "</div></li>";
                            liHtml.insert(0, tmp);
                        } else {
                            break;
                        }
                    } else {
                        break;
                    }
                    tmpComponent = tmpComponent.getParent();
                }
                //Inner html of breadcumbdiv
                String html = "<ul id=\"breadcrumbs\">" + liHtml.toString() + "</ul>";
                breadcumbDiv.appendChild(new Html(html));
            }
        }
    }
}
