/*
 * Copyright (C) 2010 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.viettel.csp.composer.examples.signPDF;

import java.io.OutputStream;
import java.net.URL;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.util.List;
import java.security.cert.X509Certificate;
/**
 * this interface provides basic signing features to digitally sign
 * @author sonnt38@viettel.com.vn
 * @since 1.1
 * @version 1.1
 */
public interface DigitalSignature {
    /**
     * sign a PDF file using PAdES and X.509 certificate
     * @param fileURL file to sign
     * @param key private key of signer
     * @param chain certificate chain associates with private key
     * @param parameter sign parameter, example Comment of signer
     * @param os return stream if sign successfully
     * @throws Exception if error
     */
    void sign(URL fileURL, PrivateKey key, Certificate[] chain, SignatureParameter parameter, OutputStream os)
            throws Exception;
    /**
     * sign a PDF file using PAdES and X.509 certificate
     * @param fileName string path of file
     * @param key private key of signer
     * @param chain certificate chain associates with private key
     * @param parameter sign parameter, example Comment of signer
     * @param os return stream if sign successfully
     * @throws Exception if error
     */
    void sign(String fileName, PrivateKey key, Certificate[] chain, SignatureParameter parameter, OutputStream os)
            throws Exception;
    /**
     * Verify the signed file, check the signer identity at the current time
     * @param fileURL file to verify
     * @param certificate CA certificate
     * @return list of verifier
     * @throws Exception if error
     * @see Verifier
     */
    List<Verifier> verify(URL fileURL, Certificate certificate) throws Exception;
    /**
     * Verify the signed file, check the signer identity at the current time
     * @param fileName file to verify
     * @param certificate CA certificate
     * @return list of verifier
     * @throws Exception if error
     * @see Verifier
     */
    List<Verifier> verify(String fileName, Certificate certificate) throws Exception;
    /**
     * Verify the signed file, check the signer identity at the current time
     * @param fileURL file to verify
     * @param keystore store contains some certificates
     * @return list of verifier
     * @throws Exception if error
     * @see Verifier
     */
    List<Verifier> verify(URL fileURL, KeyStore keystore) throws Exception;
    /**
     * Verify the signed file, check the signer identity at the current time
     * @param fileName path of signed file
     * @param keystore store contains some certificates
     * @return list of verifier
     * @throws Exception if error
     * @see Verifier
     */
    List<Verifier> verify(String fileName, KeyStore keystore) throws Exception;
    /**
     * get the first issuer name
     * @param fileURL URL of file
     * @return <CODE>String</CODE> if document was signed or <CODE>null</CODE> if no signature
     * @throws Exception if error
     */
    String getFirstIssuerCertificate(URL fileURL)throws Exception;
    /**
     * get the first issuer name
     * @param fileName pathname of file
     * @return <CODE>String</CODE> if document was signed or <CODE>null</CODE> if no signature
     * @throws Exception if error
     */
    List<X509Certificate> getCertificate(String fileName)  throws Exception;
    String getFirstIssuerCertificate(String fileName)throws Exception;
    int getSignatureNumbers(String fileName) throws Exception;
    int getSignatureNumbers(URL fileURL) throws Exception;
    
}

