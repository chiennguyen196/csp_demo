package com.viettel.csp.composer.reason;

import com.viettel.csp.DTO.ReasonDTO;
import com.viettel.csp.service.ReasonService;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.util.StringUtils;
import com.viettel.eafs.util.SpringUtil;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.*;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by admin on 7/10/2017.
 */
public class ReasonComposer extends GenericForwardComposer<Component> {

    private ReasonService reasonService = SpringUtil.getBean("reasonService", ReasonService.class);

    private Window wdReason;

    private Button btnAdd, btnEdit, btnDelete, btnReset;

    private Textbox txtReasonCode, txtReasonName;

    private Listbox lbxReasonList;

    private Combobox cbbReasonType;


    @Override
    public void doAfterCompose(Component component) throws Exception {
        super.doAfterCompose(component);
        showDataToGrip();
    }

    public void showDataToGrip() {
        showData(reasonService.findAll());
    }

    public void showData(List<ReasonDTO> reasonDTOList) {
        ListModelList model;
        if (reasonDTOList != null) {
            model = new ListModelList(reasonDTOList);
            model.setMultiple(true);
        } else {
            model = new ListModelList();
        }

        lbxReasonList.setModel(model);
    }

    protected ReasonDTO getDataSearchFromClient() {
        ReasonDTO reasonDTO = new ReasonDTO("","","","");

        if (StringUtils.isValidString(txtReasonCode.getValue())) {
            reasonDTO.setCode(txtReasonCode.getValue().trim());
        }

        if (StringUtils.isValidString(txtReasonName.getValue())) {
            reasonDTO.setName(txtReasonName.getValue().trim());
        }

        if (cbbReasonType.getSelectedIndex() > 0) {
            reasonDTO.setType(cbbReasonType.getSelectedItem().getValue().toString());
        }
        return reasonDTO;
    }

    public void onClick$btnSearch() {
        List<ReasonDTO> result = reasonService.search(getDataSearchFromClient());
        showData(result);
    }


    public void onClick$btnAdd() {
        Map<String, Object> arg = new HashMap<>();
        arg.put("OBJECT", new ReasonDTO());
        arg.put(ParamUtils.COMPOSER, this);
        arg.put(ParamUtils.ACTION, ParamUtils.ACTION_CREATE);
        Window wd = (Window) Executions.createComponents("/controls/reason/reasonPopup.zul", null, arg);
        wd.doModal();
    }

    public void onClick$btnEdit() {
        if (lbxReasonList.getSelectedCount() != 1) {
            Clients.showNotification(
                    LanguageBundleUtils.getString("global.action.confirm.is_select.edit"),
                    "warning", null, "middle_center", 3000);
        }
        else {
            Map<String, Object> arg = new HashMap<>();
            arg.put(ParamUtils.ACTION, ParamUtils.ACTION_UPDATE);
            arg.put(ParamUtils.COMPOSER, this);
            ReasonDTO reasonDTO = (ReasonDTO) lbxReasonList.getItemAtIndex(lbxReasonList.getSelectedIndex()).getValue();
            arg.put("OBJECT", reasonDTO);
            Window wd = (Window) Executions.createComponents("/controls/reason/reasonPopup.zul", null, arg);
            wd.doModal();
        }
    }

    public void onClick$btnDelete() {
        if (lbxReasonList.getSelectedCount() > 0) {
            Messagebox.show(LanguageBundleUtils.getString("global.action.confirm.multi.delete") + "\r\n" +
                            lbxReasonList.getSelectedCount() + " " +
                            LanguageBundleUtils.getString("global.record"),
                    "Confirm Dialog",
                    Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
                    new org.zkoss.zk.ui.event.EventListener() {
                        public void onEvent(Event evt) {
                            if (Messagebox.ON_OK.equals(evt.getName())) {
                                List<ReasonDTO> deleteDTOList = new LinkedList<>();
                                for (Listitem item : lbxReasonList.getSelectedItems()) {
                                    deleteDTOList.add((ReasonDTO) item.getValue());
                                }
                                reasonService.delete(deleteDTOList);
                                showDataToGrip();
                                Clients.showNotification("Xóa thành công!",
                                        "info", null, "middle_center", 3000);
                            }
                        }
                    });
        }
    }

    public void onClick$btnReset(){
        showDataToGrip();
        this.txtReasonCode.setValue("".toString());
        this.txtReasonName.setValue("".toString());
        this.cbbReasonType.setSelectedIndex(0);
    }
}
