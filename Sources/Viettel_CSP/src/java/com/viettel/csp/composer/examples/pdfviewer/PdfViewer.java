/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.composer.examples.pdfviewer;

import com.viettel.csp.util.ParamUtils;
import com.viettel.plc.widget.FileViews;
import com.viettel.plc.widget.PdfFilesViewerComponent;
import com.viettel.plc.widget.PdfViewerComponent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Window;

/**
 *
 * @author GEM
 */
public class PdfViewer extends GenericForwardComposer<Component> {

    private static final Logger logger = Logger.getLogger(PdfViewer.class);
    private PdfFilesViewerComponent pdfFilesViewerComponent;
    private PdfViewerComponent pdfViewerComponent;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        List<FileViews> l = new ArrayList();
        FileViews f = new FileViews("demo", "demo.pdf");
        l.add(f);
        f = new FileViews("demo2", "demo2.pdf");
        l.add(f);
        pdfFilesViewerComponent.setFiles(l);

//        pdfViewerComponent.setSrc("demo.pdf");
    }

    public void onClick$btnUpload() {
        try {
            List<FileViews> l = new ArrayList();
            FileViews f = new FileViews("demo", "demo.pdf");
            l.add(f);
            f = new FileViews("demo2", "demo2.pdf");
            l.add(f);
            f = new FileViews("The.pdf", "C:\\Users\\admin\\Desktop");
            l.add(f);

            Map<String, Object> argu = new HashMap();
            argu.put(ParamUtils.ACTION, ParamUtils.ACTION_CREATE);
            argu.put(ParamUtils.COMPOSER, this);
            argu.put(ParamUtils.DATA, l);
            Window wd = (Window) Executions.createComponents("/controls/widget/pdfFilesViewerPopup.zul", null, argu);
            wd.doModal();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }
}
