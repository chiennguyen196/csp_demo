/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.composer.liquidation;

import com.viettel.csp.DTO.*;
import com.viettel.csp.composer.contract.ContractComposer;
import com.viettel.csp.constant.ContractStatus;
import com.viettel.csp.entity.UserEntity;
import com.viettel.csp.service.*;
import com.viettel.csp.util.GenComponent;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.util.StringUtils;
import com.viettel.eafs.util.SpringUtil;
import com.viettel.plc.widget.FileInfo;
import com.viettel.plc.widget.IUploaderCallback;
import com.viettel.plc.widget.UploadComponent;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.ForwardEvent;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.*;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;

import static jxl.biff.BaseCellFeatures.logger;

/**
 * @author admin
 */
public class LiquidationSendToPartner extends GenericForwardComposer<Component> implements
        IUploaderCallback {

    private final ContactTypeService contactTypeService = SpringUtil
            .getBean("contactTypeService", ContactTypeService.class);
    private final ContactService contactService = SpringUtil
            .getBean("contactService", ContactService.class);
    private final ContractFilesService contractFilesService = SpringUtil
            .getBean("contractFilesService", ContractFilesService.class);
    private final ContractHisService contractHisService = SpringUtil
            .getBean("contractHisService", ContractHisService.class);
    private final ContactPointService contactPointService = SpringUtil
            .getBean("contactPointService", ContactPointService.class);
    private final ContractService contractService = SpringUtil
            .getBean("contractService", ContractService.class);
    private UserService userService = SpringUtil
            .getBean("userService", UserService.class);
    Long partner_ID, contractId;
    String contractType;
    private Label txtContractNumber, txtCompanyName, txtHeadOfService, txtServiceCode, dbStartDate, dbEndDate, txtContractStatus, txtContractType, txtCtSignForm;
    private Long totalRow = 0L;
    private GenComponent genComponent = new GenComponent();
    private List<Component> lstInputCbbContact = new ArrayList<>();
    private List<ContactTypeDTO> lstInputContactType = new ArrayList<>();
    private List<Component> lstInputTextContactType = new ArrayList<>();
    private String action;
    private Rows rows;
    private Row lastRow;
    private ContractDTO contractdto;
    private ContactPointDTO pointDTO;
    private Textbox txtFileContractNumber, txtContent, txtDescription, txtFileName;
    //    private Datebox dbStartDate, dbEndDate;
    private List<ContractFilesDTO> lstContractFiles = new ArrayList<>();
    private Listbox lbxViewAddContractManagement, lbxViewAddContract, lbxContractHis, lbxContractUser, lbxContractUserViettel,lbxContractFiles;
    private List<ContractFilesDTO> lstContractFilesDTO = new ArrayList<>();
    private List<ContractHisDTO> lstContractHisDTO = new ArrayList<>();
    private ContractDTO oldDTO;
    private List<String> filesLocation;
    private List<FileInfo> filesInfo;
    private UploadComponent uploadComponent;
    private LiquidationComposer liquidationComposer;
    private Grid grdContactPoint;
    List<ContactPointDTO> contractPointDTOs;
    private ContractFilesDTO contractFilesDTO;
    private Textbox txtSearchBandBox;
    private Bandbox bdUser;

    public LiquidationComposer getLiquidationComposer() {
        return liquidationComposer;
    }

    public void setLiquidationComposer(LiquidationComposer liquidationComposer) {
        this.liquidationComposer = liquidationComposer;
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);

        if (this.arg.containsKey("PARTNER_ID")) {
            this.partner_ID = (Long) this.arg.get("PARTNER_ID");
        }
        if (this.arg.containsKey("CONTRACT_ID")) {
            this.contractId = (Long) this.arg.get("CONTRACT_ID");
        }
        if (this.arg.containsKey(ParamUtils.ACTION)) {
            this.action = this.arg.get(ParamUtils.ACTION).toString();
        }
        if (this.arg.containsKey(ParamUtils.COMPOSER)) {
            this.liquidationComposer = (LiquidationComposer) this.arg
                    .get(ParamUtils.COMPOSER);
        }

        if (this.arg.containsKey("CONTRACTDTO")) {
            this.contractdto = (ContractDTO) this.arg.get("CONTRACTDTO");
        }

        if ("002".equals(this.contractType)) {
            this.uploadComponent.setVisible(false);
        } else {
            this.uploadComponent.setUploaderCallback(this);
        }
        loadLBXContractFiles(this.contractId);
        loadLBXContractHis(this.contractId);
        setData();
    }

    public void loadLBXContractFiles(Long contractId) {
        ContractFilesDTO contractFilesDTO = new ContractFilesDTO();
        contractFilesDTO.setContractId(contractId);
        this.lstContractFilesDTO = this.contractFilesService
                .getListMenuContractFilesLiquidation(contractFilesDTO);
        if (this.lstContractFilesDTO != null) {
            ListModelList model = new ListModelList(this.lstContractFilesDTO);
            model.setMultiple(true);
            this.lbxViewAddContractManagement.setModel(model);
        }
    }

    public void onDeleteContract(ForwardEvent evt) {
        evt.stopPropagation();

        try {
            List<ContractFilesDTO> lstDeleteDTO = new ArrayList<>();
            Image btn = (Image) evt.getOrigin().getTarget();
            Listitem litem = (Listitem) btn.getParent().getParent();
            ContractFilesDTO deleteDTO = ((ContractFilesDTO) litem.getValue());
            lstDeleteDTO.add(deleteDTO);
            String message = this.contractFilesService.deleteContractFiles(lstDeleteDTO);
            Clients.showNotification(message, "suscess", null, "middle_center", 3000);
            loadLBXContractFiles(this.contractId);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(ContractComposer.class.getName())
                    .log(Level.SEVERE, null, ex);
        }
    }

    public void onDownloadFileContract(ForwardEvent evt) {
        evt.stopPropagation();
        Image btn = (Image) evt.getOrigin().getTarget();
        Listitem litem = (Listitem) btn.getParent().getParent();
        onClick$btnDownloadFile(litem);
    }

    public void onClick$btnDownloadFile(Listitem litem) {
        try {
            List<ContractFilesDTO> lstContractFilesResult = new ArrayList<>();
            lstContractFilesResult.add(litem.getValue());
            File file = new File(lstContractFilesResult.get(0).getPathFile());
            Filedownload.save(file, null);
        } catch (Exception ex) {
            Messagebox.show(LanguageBundleUtils.getMessage("messageUploadFileError"),
                    LanguageBundleUtils.getMessage("Messagebox.warning"), Messagebox.OK,
                    Messagebox.ERROR,
                    null);
        }
    }

    private void bindDataToBandboxLaw() {
        try {
            UserEntity objectUserEntity = new UserEntity();
            objectUserEntity.setPersonType(ParamUtils.USER_TYPE.VIETTEL);
            List<UserDTO> userDTOs = userService.fillByPersonType(ParamUtils.USER_TYPE.VIETTEL, txtSearchBandBox.getValue());
            if (userDTOs != null) {
                ListModelList model = new ListModelList(userDTOs);
                lbxContractUser.setModel(model);
            } else {
                lbxContractUser.setModel(new ListModelList());
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    public void loadLBXContractHis(Long contract_id) {
        ContractHisDTO contractHisDTO = new ContractHisDTO();
        contractHisDTO.setContractId(contract_id);
        this.lstContractHisDTO = this.contractHisService.getListContractHisAddUser(contractHisDTO);
        if (this.lstContractHisDTO != null) {
            ListModelList model = new ListModelList(this.lstContractHisDTO);
            model.setMultiple(true);
            this.lbxContractHis.setModel(model);
        }
    }


    public ContactPointDTO getDataFromClient() {
        this.pointDTO = new ContactPointDTO();
        List<ContactPointDTO> lstContactPointDTO = new ArrayList<>();
        List<String> lstCbbContactDTO = new ArrayList(
                this.genComponent.getDataFromControls(this.lstInputCbbContact).values());
        for (int i = 0; i < this.lstInputContactType.size(); i++) {
            ContactPointDTO contactPointDTO = new ContactPointDTO();
            contactPointDTO.setContractId(this.contractId);
            contactPointDTO.setContactId(Long.parseLong(lstCbbContactDTO.get(i)));
            contactPointDTO.setContactType(this.lstInputContactType.get(i).getCode());
            contactPointDTO.setId(this.lstInputContactType.get(i).getContactPointID());
            lstContactPointDTO.add(contactPointDTO);
        }
        this.pointDTO.setListContactPointDTO(lstContactPointDTO);
        return this.pointDTO;
    }

    public void onClick$btnclosePopup() {
        self.detach();
        liquidationComposer.bindDataToGrid();
    }


    public void onClick$btnAddFilePopup() {
        try {
            Map<String, Object> argu = new HashMap();
            argu.put(ParamUtils.ACTION, ParamUtils.ACTION_CREATE);
            argu.put(ParamUtils.COMPOSER, this);
            Window wd;
            wd = (Window) Executions
                    .createComponents("controls/liquidation/liquidationAddFileSendToPartner.zul",
                            null,
                            argu);
            wd.doModal();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    public void onClick$btnViewFilePopup() {
        try {
            if (lbxViewAddContract.getSelectedItem().getIndex() > 0) {
                Map<String, Object> argu = new HashMap();
                argu.put(ParamUtils.ACTION, ParamUtils.ACTION_UPDATE_FILE);
                List<Integer> indexs = new ArrayList();
                indexs.add(this.lbxViewAddContract.getSelectedItem().getIndex());
                argu.put("LIST_ACTION_INDEX", indexs);
                argu.put("LISTBOX", this.lbxViewAddContract.getSelectedItem().getValue());
                argu.put(ParamUtils.COMPOSER, this);
                this.lstContractFiles
                        .remove(this.lstContractFiles.get(this.lbxViewAddContract.getSelectedIndex()));
                this.lbxViewAddContract.removeItemAt(this.lbxViewAddContract.getSelectedIndex());
                Window wd;
                wd = (Window) Executions
                        .createComponents("controls/liquidation/liquidationAddFileSendToPartner.zul",
                                null,
                                argu);
                wd.doModal();
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    public void setDataGrid(ContractFilesDTO contractFilesDTO) {
        this.lstContractFiles.add(contractFilesDTO);
        ListModelList model = new ListModelList(this.lstContractFiles);
        model.setMultiple(true);
        this.lbxViewAddContract.setModel(model);
    }

    public String formatDateTime(Date date) throws Exception {
        return StringUtils.formatDateTime(date);
    }

    public void setData() throws Exception {
        if (ParamUtils.ACTION_CREATE.equals(this.action)) {
            ((Window) this.self).setTitle(LanguageBundleUtils.getString("global.add"));

        } else if (ParamUtils.ACTION_UPDATE.equals(this.action)) {
            this.oldDTO = contractdto;
            ((Window) this.self).setTitle(LanguageBundleUtils.getString("global.edit"));
            if (this.oldDTO != null) {
                bindDataToBandboxLaw();
                this.txtCompanyName.setValue(this.oldDTO.getCompanyName());
                this.txtContractNumber.setValue(this.oldDTO.getAddressHeadOffice());
                this.txtHeadOfService.setValue(this.oldDTO.getPartnerCode());
                this.txtServiceCode.setValue(this.oldDTO.getAddressTradingOffice());
                String date = "dd/MM/yyyy";
                String dateStr = null;
                String dateStrEnd = null;
                DateFormat df = new SimpleDateFormat(date);
                dateStr = df.format(this.oldDTO.getStartDate());
                dateStrEnd = df.format(this.oldDTO.getEndDate());
                this.dbStartDate.setValue(dateStr);
                this.dbEndDate.setValue(dateStr);
                this.txtCtSignForm.setValue(this.oldDTO.getCtSignForm());
                this.txtContractStatus.setValue(this.oldDTO.getContractStatusName());
                this.txtContractType.setValue(this.oldDTO.getContractType());
                this.contractPointDTOs = this.contactPointService.getList(contractId);
                if (this.contractPointDTOs != null && !this.contractPointDTOs.isEmpty()) {
                    ListModelList model = new ListModelList(this.contractPointDTOs);
                    model.setMultiple(false);
                    grdContactPoint.setModel(model);
                }
            }
        }
    }

    @Override
    public void uploadedFilesInfoCallback(List<FileInfo> filesInfo) {
        this.filesInfo = filesInfo;
        System.out.println(filesInfo);
    }

    public ContractFilesDTO getDataFromClientContractFilesDTO(ContractDTO contractdto, FileInfo filesInfo) {
        ContractFilesDTO contractFilesDTO = new ContractFilesDTO();
        contractFilesDTO.setFileName(new File(filesInfo.getLocation()).getName());
        contractFilesDTO.setPathFile(new File(filesInfo.getLocation()).getPath());
        contractFilesDTO.setFileType(ParamUtils.FILE_TYPE.FILE_TYPE_TLHĐ);
        contractFilesDTO.setContractId(this.contractId);
        contractFilesDTO.setUploadDate(new Date());
        contractFilesDTO.setDescription(filesInfo.getDescription());
        if (contractdto.getStartDate() != null) {
            contractFilesDTO.setStartDate(contractdto.getStartDate());
        }
        if (contractdto.getEndDate() != null) {
            contractFilesDTO.setEndDate(contractdto.getEndDate());
        }
        return contractFilesDTO;
    }

    @Override
    public void uploadedFilesLocationCallback(List<String> filesLocation) {
        this.filesLocation = filesLocation;
    }

    public ContractDTO getDataFromClientContract() {
        oldDTO.setCompanyName(txtCompanyName.getValue().trim().toLowerCase());
        oldDTO.setContractNumber(txtContractNumber.getValue().trim());
        oldDTO.setHeadOfService(txtHeadOfService.getValue().trim());
        oldDTO.setServiceCode(txtServiceCode.getValue().trim());
        oldDTO.setContractStatusCode(txtContractStatus.getValue().trim());
        oldDTO.setContractType(txtContractType.getValue().trim());
        oldDTO.setCtSignForm(txtCtSignForm.getValue().trim());
        if (lbxContractUser.getSelectedItem() != null) {
            UserEntity userEntity = new UserEntity();
            userEntity.setUserName(bdUser.getValue());
            List<UserEntity> lstUserName = userService.fillByUsername(userEntity.getUserName());
            oldDTO.setSignLiExpertiseLaw(lstUserName.get(0).getId());
        }
        return this.oldDTO;
    }

    public ContractFilesDTO getDataFromClientContractFiles(ContractDTO contractDTO, FileInfo fileInfo) {

        contractFilesDTO = new ContractFilesDTO();
        contractFilesDTO.setContractId(oldDTO.getContractId());
        contractFilesDTO.setFileName(new File(fileInfo.getLocation()).getName());
        contractFilesDTO.setPathFile(new File(fileInfo.getLocation()).getPath());
        contractFilesDTO.setDescription(fileInfo.getDescription());
        return contractFilesDTO;
    }

    public void onClick$btnSendToPartner() {
        try {
            long id;
            this.oldDTO = getDataFromClientContract();
            id = this.contractService.updateCreatLiquidation(this.oldDTO, this.action, ParamUtils.LI_STATUS.LIQUIDATION_SEND_TO_PARTNER);
            this.uploadComponent.onClick$doSave();
            if (validateFile()) {
                if (filesInfo.size() > 0) {
                    for (int i = 0; i < filesInfo.size(); i++) {
                        ContractFilesDTO filesDTO = getDataFromClientContractFilesDTO(this.oldDTO, filesInfo.get(i));
                        if (oldDTO.getContractNumber() != null) {
                            filesDTO.setFileContractNumber(oldDTO.getContractNumber());
                        }
                        contractFilesService.insertFilesContactMenu(filesDTO, ParamUtils.FILE_TYPE.FILE_TYPE_TLHĐ);
                    }
                }
            }
            if (lstContractFiles != null) {
                for (int i = 0; i < lstContractFiles.size(); i++) {
                    ContractFilesDTO filesDTO1 = lstContractFiles.get(i);
                    filesDTO1.setFileType(ParamUtils.FILE_TYPE.FILE_TYPE_PHULUC_TLHĐ);
                    filesDTO1.setContractId(id);
                    contractFilesService.insertFilesContactMenu(filesDTO1, ParamUtils.FILE_TYPE.FILE_TYPE_PHULUC_TLHĐ);
                }
            }

            if (id > 0) {
                this.liquidationComposer.bindDataToGrid();
                Clients
                        .showNotification(
                                LanguageBundleUtils.getString("global.message.create.successful"),
                                "info", null, "middle_center", 1000);
                this.self.detach();
            } else {
                Clients
                        .showNotification(ParamUtils.SUCCESS, "warning", null, "middle_center", 3000);
            }

        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients
                    .showNotification(LanguageBundleUtils.getString("global.message.error"), "warning",
                            null,
                            "middle_center", 3000);
        }
    }

    private boolean validateFile() {
        if (this.filesInfo == null) {
            Clients.showNotification(LanguageBundleUtils.getString("global.message.errorFile"),
                    "warning",
                    null, "middle_center", 3000);
            return false;
        }
        return true;
    }

    public void onClick$btnRemoveFilePopup() {
        try {
            if (this.lbxViewAddContract.getSelectedCount() > 0) {
                this.lstContractFiles
                        .remove(this.lstContractFiles.get(this.lbxViewAddContract.getSelectedIndex()));
                this.lbxViewAddContract.removeItemAt(this.lbxViewAddContract.getSelectedIndex());
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients
                    .showNotification(LanguageBundleUtils.getString("global.message.error"), "warning",
                            null,
                            "middle_center", 3000);
        }
    }

    public String getContractStatusHisName(String paramType) {
        return ContractStatus.getContractStatusName(paramType);
    }


    public void onClick$btnChooseBandBox() {
        try {
            if (lbxContractUser.getSelectedItem() == null && lbxContractUser.getSelectedItem().getValue() == null) {
                Clients.showNotification(LanguageBundleUtils.getString("global.message.choose.row"), "warning", null, "middle_center", 3000);
                return;
            }
            UserDTO userDTO = lbxContractUser.getSelectedItem().getValue();
            bdUser.setValue(userDTO.getUserName());
            bdUser.close();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients
                    .showNotification(LanguageBundleUtils.getString("global.message.error"), "warning",
                            null,
                            "middle_center", 3000);
        }
    }

    public void onClick$btnSearchBandBox() {
        try {
            UserEntity objectUserEntity = new UserEntity();
            objectUserEntity.setPersonType(ParamUtils.USER_TYPE.VIETTEL);
            List<UserDTO> userDTOs = userService.fillByPersonType(ParamUtils.USER_TYPE.VIETTEL, txtSearchBandBox.getValue());
            if (userDTOs != null) {
                ListModelList model = new ListModelList(userDTOs);
                lbxContractUser.setModel(model);
            } else {
                lbxContractUser.setModel(new ListModelList());
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients
                    .showNotification(LanguageBundleUtils.getString("global.message.error"), "warning",
                            null,
                            "middle_center", 3000);
        }
    }
}
