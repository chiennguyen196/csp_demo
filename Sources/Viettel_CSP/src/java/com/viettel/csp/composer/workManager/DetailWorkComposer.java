/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.composer.workManager;

import com.viettel.csp.DTO.*;
import com.viettel.csp.composer.contract.ContractComposer;
import com.viettel.csp.entity.TaskTypeEntity;
import com.viettel.csp.entity.TasksEntity;
import com.viettel.csp.service.*;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.util.StringUtils;
import com.viettel.csp.util.UserTokenUtils;
import com.viettel.eafs.util.SpringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.ForwardEvent;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.*;

import java.io.File;
import java.util.*;
import java.util.logging.Level;

/**
 * @author admin
 */
public class DetailWorkComposer extends GenericForwardComposer<Component> {

    final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(DetailWorkComposer.class);
    private String action = "";
    private Listbox lbxContactPoint;
    private Listbox lbxContact;
    private Listbox lbxContractFiles;
    private Listbox lbxTasks;
    private Grid grdContactPoint;
    private Button btnDoneTask;
    private Button btnAcceptActiveContract;

    private List<Integer> indexs = new ArrayList();
    private Map<String, String> mapParent = new HashMap<>();

    private Label lbContracNumber, lbNamePartner, lbServiceGroup, lbServiceGroupName, lbService, lbServiceName, lbHeadOfService, lbDateBegin, lbDateEnd, lbContracType, lbContracTypeName, lbContractStatusName, lbContractStatus;

    private ContractDTO contractDTO = new ContractDTO();
    private List<ContactPointDTO> contractPointDTOs;
    private List<ContractFilesDTO> contractFilesDTOs;
    private List<TasksDTO> tasksDTOS;
    private List<TaskTypeEntity> taskTypeEntities;
    private ContractFilesService contractFilesService = SpringUtil.getBean("contractFileService", ContractFilesService.class);
    private ContactPointService contactPointService = SpringUtil.getBean("contactPointService", ContactPointService.class);
    private ContractService contractService = SpringUtil.getBean("contractService", ContractService.class);
    private ContractHisService contractHisService = SpringUtil.getBean("contractHisService", ContractHisService.class);
    private MessageService messageService = SpringUtil.getBean("messageService", MessageService.class);
    private TasksService tasksService = SpringUtil.getBean("tasksService", TasksService.class);
    private TaskTypeService taskTypeService = SpringUtil.getBean("taskTypeService", TaskTypeService.class);
    private Long contractId;

    @Autowired
    private WorkManagerComposer managerComposer;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        if (arg.containsKey(ParamUtils.ACTION)) {
            this.action = arg.get(ParamUtils.ACTION).toString();
        }
        if (arg.containsKey("CONTRACT_DETAIL")) {
            this.lbxContact = (Listbox) arg.get("CONTRACT_DETAIL");
        }
        if (arg.containsKey("COMPOSER")) {
            this.managerComposer = (WorkManagerComposer) arg.get("COMPOSER");
        }
        if (arg.containsKey("LIST_ACTION_INDEX")) {
            this.indexs = (List<Integer>) arg.get("LIST_ACTION_INDEX");
        }
        if (arg.containsKey("MAP_SERVER_GROUP")) {
            this.mapParent = (Map<String, String>) arg.get("MAP_SERVER_GROUP");
        }

        if (ParamUtils.ACTION_UPDATE.equals(action)) {
            ((Window) self).setTitle(LanguageBundleUtils.getString("global.edit"));
            contractDTO = lbxContact.getItemAtIndex(indexs.get(0)).getValue();
            this.bindData(contractDTO);
        } else {
            ((Window) self).setTitle(LanguageBundleUtils.getString("global.add"));
        }
        if(contractDTO.getContractStatusCode().equals(ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_OK_CONTRACT_EFFECTING_CONTRACT))
            btnAcceptActiveContract.setDisabled(true);
        this.taskTypeEntities = this.taskTypeService.findAlles();

        if (this.taskTypeEntities.size() != this.tasksDTOS.size())
            this.btnDoneTask.setDisabled(true);
        else {
            int i = 0;
            for (TasksDTO tasksDTO : this.tasksDTOS)
                if (ParamUtils.TASKS_STATUS.DONE_TASK.equals(tasksDTO.getStatus())) i++;
            if (i == this.taskTypeEntities.size())
                this.btnDoneTask.setDisabled(false);
            else this.btnDoneTask.setDisabled(true);
        }

    }

    public void onDownloadFileContract(ForwardEvent evt) {
        evt.stopPropagation();
        try {
            Image btn = (Image) evt.getOrigin().getTarget();
            Listitem litem = (Listitem) btn.getParent().getParent();
            onClick$btnDownloadFile(litem);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            java.util.logging.Logger.getLogger(ContractComposer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onClick$btnDownloadFile(Listitem litem) {
        try {
            List<ContractFilesDTO> lstContractFilesResult = new ArrayList<>();
            lstContractFilesResult.add(litem.getValue());
            File file = new File(lstContractFilesResult.get(0).getPathFile());
            Filedownload.save(file, null);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Messagebox.show(LanguageBundleUtils.getMessage("messageUploadFileError"), LanguageBundleUtils.getMessage("Messagebox.warning"), Messagebox.OK, Messagebox.ERROR, null);
        }
    }

    private void insertContractHis(String code) throws Exception {
        try {
            ContractHisDTO contractHisDTO = new ContractHisDTO();
            contractHisDTO.setContractId(contractDTO.getContractId());
            contractHisDTO.setCreateBy(UserTokenUtils.getUserId());
            contractHisDTO.setStatusBefore(contractDTO.getContractStatusCode());
            contractHisDTO.setStatusAfter(code);
            contractHisDTO.setCreateAt(new Date());
            contractHisDTO.setContent("Thay đổi trạng thái tiếp nhận công việc");
            contractHisService.insert(contractDTO.getContractId(), contractHisDTO);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
            throw ex;
        }
    }

    public void onClick$btnDeleteTask() throws Exception {
        try {
            if (lbxTasks.getSelectedCount() == 1) {
                TasksDTO tasksDTO = lbxTasks.getItemAtIndex(lbxTasks.getSelectedItem().getIndex()).getValue();
                TasksEntity tasksEntity = new TasksEntity();
                tasksEntity.setId(tasksDTO.getId());
                String message = this.tasksService.delete(tasksEntity);
                if (ParamUtils.SUCCESS.equals(message)) {
                    Clients.showNotification("Xóa thành công", "info", null, "middle_center", 1000);
                    this.binDataTasks();
                    btnAcceptActiveContract.setDisabled(false);
                    //self.detach();
                } else
                    Clients.showNotification("Xóa đang gặp vấn đề", "warning", null, "middle_center", 3000);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
            throw ex;
        }
    }

    public void onClick$btnDoneTask() throws Exception {
        try {
            int i = 0;
            for (TasksDTO tasksDTO : this.tasksDTOS)
                if (ParamUtils.TASKS_STATUS.DONE_TASK.equals(tasksDTO.getStatus())) i++;
            if (i != this.taskTypeEntities.size()){
                Clients.showNotification("Cần hoàn thành tất cả các công việc", "info", null, "middle_center", 1000);
                return;
            }
            ContractDTO contractDTO = new ContractDTO();
            contractDTO.setContractId(this.contractId);
            String message = contractService.update(contractDTO, ParamUtils.ACTION_UPDATE, ParamUtils.TYPE_ACTION.EFFECTING_CONTRACT);
            if (ParamUtils.SUCCESS.equals(message)) {
                Clients.showNotification("Cập nhật thành công", "info", null, "middle_center", 1000);
                this.managerComposer.onClick$btnSearch();
                self.detach();
            } else
                Clients.showNotification("Cập nhật không thành công", "warning", null, "middle_center", 3000);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    public void onClick$btnAcceptActiveContract() throws Exception {
        try {
            Map<String, Object> argu = new HashMap();
            argu.put(ParamUtils.COMPOSER, this);
            argu.put("CONTRACT_ID", this.contractId);
            Window wd = (Window) Executions.createComponents("/controls/workManager/handoverTask.zul", null, argu);
            wd.doModal();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    public void onClick$btnAcceptOKContract() throws Exception {
        try {
            this.insertContractHis(ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_OK_ACCEPT);
            contractService.update(contractDTO, ParamUtils.ACTION_UPDATE, ParamUtils.TYPE_ACTION.OK_ACCEPT);
            messageService.insert("Chấp nhận công việc");
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    public void onClick$btnAcceptNOContract() throws Exception {
        try {
            this.insertContractHis(ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_NO_ACCEPT);
            contractService.update(contractDTO, ParamUtils.ACTION_UPDATE, ParamUtils.TYPE_ACTION.NOT_ACTIVE_TASK_CONTRACT);
            messageService.insert("Từ chối công việc");
            this.managerComposer.onClick$btnSearch();
            Clients.showNotification(LanguageBundleUtils.getString("global.message.save.successful"), "info", null, "middle_center", 1000);
            self.detach();
            return;
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            //Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning", null, "middle_center", 3000);
        }
    }

    private void bindData(ContractDTO contractDTO) throws Exception {
        this.contractId = contractDTO.getContractId();
        this.lbContracNumber.setValue(contractDTO.getContractNumber());
        this.lbNamePartner.setValue(contractDTO.getCompanyName());
        this.lbServiceGroupName.setValue(mapParent.get(contractDTO.getServiceGroup()) != null ? mapParent.get(contractDTO.getServiceGroup()) : "");
        this.lbService.setValue(contractDTO.getServiceCode());
        this.lbServiceName.setValue(contractDTO.getServiceName());
        //this.lbHeadOfService.setValue(contractDTO.getHeadOfService());
        this.lbDateBegin.setValue(StringUtils.formatDateTime(contractDTO.getStartDate()));
        this.lbDateEnd.setValue(StringUtils.formatDateTime(contractDTO.getEndDate()));
        //this.lbContracType.setValue(contractDTO.getContractType());
        this.lbContracTypeName.setValue(contractDTO.getContractTypeName());
        //this.lbContractStatus.setValue(contractDTO.getContractStatusCode());
        this.lbContractStatusName.setValue(contractDTO.getContractStatusName());

        this.contractPointDTOs = this.contactPointService.getList(this.contractId);
        if (this.contractPointDTOs != null && !this.contractPointDTOs.isEmpty()) {
            ListModelList model = new ListModelList(this.contractPointDTOs);
            model.setMultiple(false);
            grdContactPoint.setModel(model);
        } else {
            grdContactPoint.setModel(new ListModelList());
        }

        ContractFilesDTO contractFilesDTO = new ContractFilesDTO();
        contractFilesDTO.setContractId(contractDTO.getContractId());
        this.contractFilesDTOs = this.contractFilesService.getListContractFiles(contractFilesDTO);
        if (this.contractFilesDTOs != null && !this.contractFilesDTOs.isEmpty()) {
            ListModelList model = new ListModelList(this.contractFilesDTOs);
            model.setMultiple(false);
            lbxContractFiles.setModel(model);
        } else {
            lbxContractFiles.setModel(new ListModelList());
        }

        this.binDataTasks();
    }

    public void binDataTasks() throws Exception {
        TasksDTO tasksDTO = new TasksDTO();
        tasksDTO.setContactId(this.contractId);
        this.tasksDTOS = this.tasksService.findAllToDTO(tasksDTO);
        if (this.tasksDTOS != null && !this.tasksDTOS.isEmpty()) {
            ListModelList model = new ListModelList(this.tasksDTOS);
            model.setMultiple(false);
            lbxTasks.setModel(model);
        } else {
            lbxTasks.setModel(new ListModelList());
        }
    }

    public String formatDateTime(Date date) throws Exception {
        return StringUtils.formatDateTime(date);
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Listbox getLbxContactPoint() {
        return lbxContactPoint;
    }

    public void setLbxContactPoint(Listbox lbxContactPoint) {
        this.lbxContactPoint = lbxContactPoint;
    }

    public Listbox getLbxContact() {
        return lbxContact;
    }

    public void setLbxContact(Listbox lbxContact) {
        this.lbxContact = lbxContact;
    }

    public Listbox getLbxContractFiles() {
        return lbxContractFiles;
    }

    public void setLbxContractFiles(Listbox lbxContractFiles) {
        this.lbxContractFiles = lbxContractFiles;
    }

    public List<Integer> getIndexs() {
        return indexs;
    }

    public void setIndexs(List<Integer> indexs) {
        this.indexs = indexs;
    }

    public Map<String, String> getMapParent() {
        return mapParent;
    }

    public void setMapParent(Map<String, String> mapParent) {
        this.mapParent = mapParent;
    }

    public ContractDTO getContractDTO() {
        return contractDTO;
    }

    public void setContractDTO(ContractDTO contractDTO) {
        this.contractDTO = contractDTO;
    }

    public Label getLbContracNumber() {
        return lbContracNumber;
    }

    public void setLbContracNumber(Label lbContracNumber) {
        this.lbContracNumber = lbContracNumber;
    }

    public Label getLbNamePartner() {
        return lbNamePartner;
    }

    public void setLbNamePartner(Label lbNamePartner) {
        this.lbNamePartner = lbNamePartner;
    }

    public Label getLbServiceGroup() {
        return lbServiceGroup;
    }

    public void setLbServiceGroup(Label lbServiceGroup) {
        this.lbServiceGroup = lbServiceGroup;
    }

    public Label getLbServiceGroupName() {
        return lbServiceGroupName;
    }

    public void setLbServiceGroupName(Label lbServiceGroupName) {
        this.lbServiceGroupName = lbServiceGroupName;
    }

    public Label getLbService() {
        return lbService;
    }

    public void setLbService(Label lbService) {
        this.lbService = lbService;
    }

    public Label getLbServiceName() {
        return lbServiceName;
    }

    public void setLbServiceName(Label lbServiceName) {
        this.lbServiceName = lbServiceName;
    }

    public Label getLbHeadOfService() {
        return lbHeadOfService;
    }

    public void setLbHeadOfService(Label lbHeadOfService) {
        this.lbHeadOfService = lbHeadOfService;
    }

    public Label getLbDateBegin() {
        return lbDateBegin;
    }

    public void setLbDateBegin(Label lbDateBegin) {
        this.lbDateBegin = lbDateBegin;
    }

    public Label getLbDateEnd() {
        return lbDateEnd;
    }

    public void setLbDateEnd(Label lbDateEnd) {
        this.lbDateEnd = lbDateEnd;
    }

    public Label getLbContracType() {
        return lbContracType;
    }

    public void setLbContracType(Label lbContracType) {
        this.lbContracType = lbContracType;
    }

    public Label getLbContracTypeName() {
        return lbContracTypeName;
    }

    public void setLbContracTypeName(Label lbContracTypeName) {
        this.lbContracTypeName = lbContracTypeName;
    }

    public Label getLbContractStatusName() {
        return lbContractStatusName;
    }

    public void setLbContractStatusName(Label lbContractStatusName) {
        this.lbContractStatusName = lbContractStatusName;
    }

    public Label getLbContractStatus() {
        return lbContractStatus;
    }

    public void setLbContractStatus(Label lbContractStatus) {
        this.lbContractStatus = lbContractStatus;
    }

    public List<ContactPointDTO> getContractPointDTOs() {
        return contractPointDTOs;
    }

    public void setContractPointDTOs(List<ContactPointDTO> contractPointDTOs) {
        this.contractPointDTOs = contractPointDTOs;
    }

    public List<ContractFilesDTO> getContractFilesDTOs() {
        return contractFilesDTOs;
    }

    public void setContractFilesDTOs(List<ContractFilesDTO> contractFilesDTOs) {
        this.contractFilesDTOs = contractFilesDTOs;
    }

    public ContractFilesService getContractFilesService() {
        return contractFilesService;
    }

    public void setContractFilesService(ContractFilesService contractFilesService) {
        this.contractFilesService = contractFilesService;
    }

    public ContactPointService getContactPointService() {
        return contactPointService;
    }

    public void setContactPointService(ContactPointService contactPointService) {
        this.contactPointService = contactPointService;
    }

    public WorkManagerComposer getManagerComposer() {
        return managerComposer;
    }

    public void setManagerComposer(WorkManagerComposer managerComposer) {
        this.managerComposer = managerComposer;
    }

    public ContractService getContractService() {
        return contractService;
    }

    public void setContractService(ContractService contractService) {
        this.contractService = contractService;
    }

    public ContractHisService getContractHisService() {
        return contractHisService;
    }

    public void setContractHisService(ContractHisService contractHisService) {
        this.contractHisService = contractHisService;
    }

    public MessageService getMessageService() {
        return messageService;
    }

    public void setMessageService(MessageService messageService) {
        this.messageService = messageService;
    }

}
