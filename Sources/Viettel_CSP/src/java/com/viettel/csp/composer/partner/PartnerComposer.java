/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.composer.partner;

import com.viettel.csp.DTO.ContactDTO;
import com.viettel.csp.DTO.PartnerDTO;
import com.viettel.csp.service.ContactService;
import com.viettel.csp.service.PartnerBankService;
import com.viettel.csp.service.PartnerService;
import com.viettel.csp.service.UserService;
import com.viettel.csp.util.KeyValueBean;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.util.StringUtils;
import com.viettel.eafs.util.SpringUtil;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Grid;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author TienNV@gemvietnam.com
 */
public class PartnerComposer extends GenericForwardComposer<Component> {

    private static final Logger logger = Logger.getLogger(PartnerComposer.class);
    private ListModelList<KeyValueBean> listPartnerStatus = new ListModelList<KeyValueBean>();
    private ListModelList<KeyValueBean> listPartnerType = new ListModelList<KeyValueBean>();
    private ListModelList<PartnerDTO> listPartner = new ListModelList<PartnerDTO>();
    private List<PartnerDTO> lstResult;
    private Combobox cbbPartnerType, cbbPartnerStatus;
    private Listbox lbxAttribute;
    private Textbox txtCode, txtShortName, txtAddress, txtName;
    private Grid gridSearch;

    private PartnerService partnerService = SpringUtil.getBean("partnerService", PartnerService.class);
    private ContactService contactService = SpringUtil.getBean("contactService", ContactService.class);
    private UserService userService = SpringUtil.getBean("userService", UserService.class);
    private PartnerBankService partnerBankService = SpringUtil.getBean("partnerBankService", PartnerBankService.class);

    public ListModelList<KeyValueBean> getListPartnerStatus() {
        return listPartnerStatus;
    }

    public void setListPartnerStatus(ListModelList<KeyValueBean> listPartnerStatus) {
        this.listPartnerStatus = listPartnerStatus;
    }

    public ListModelList<KeyValueBean> getListPartnerType() {
        return listPartnerType;
    }

    public void setListPartnerType(ListModelList<KeyValueBean> listPartnerType) {
        this.listPartnerType = listPartnerType;
    }

    public ListModelList<PartnerDTO> getListPartner() {
        return listPartner;
    }

    public void setListPartner(ListModelList<PartnerDTO> listPartner) {
        this.listPartner = listPartner;
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        try {
            super.doAfterCompose(comp);
            bindDataToCombo();
            bindDataToGrid();
        } catch (Throwable ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    public void onClick$btnSearch() {
        bindDataToGrid();
    }

    public void onClick$btnEdit() {
        try {
            if (lbxAttribute.getSelectedCount() == 1) {
                PartnerDTO partnerDTO = getDataFromClient();
                Map<String, Object> argu = new HashMap();
                argu.put(ParamUtils.ACTION, ParamUtils.ACTION_UPDATE);
                List<Integer> indexs = new ArrayList();
                indexs.add(lbxAttribute.getSelectedItem().getIndex());
                argu.put("LIST_ACTION_INDEX", indexs);
                argu.put("LISTBOX", lbxAttribute);
                argu.put("COMPOSER", this);

                Window wd;//partnerPopup
                wd = (Window) Executions.createComponents("/controls/partner/partnerPopup.zul", null, argu);
                wd.doModal();
            } else {
                Clients.showNotification(LanguageBundleUtils.getString("global.action.confirm.is_select.edit"), "warning", null, "middle_center", 3000);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    public void onClick$btnDelete() throws Exception {
        try {
            if (lbxAttribute.getSelectedCount() > 0) {
                Messagebox.show(LanguageBundleUtils.getString("global.action.confirm.multi.delete"), "Confirm Dialog", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION, new org.zkoss.zk.ui.event.EventListener() {
                    public void onEvent(Event evt) throws Exception {
                        if ("onOK".equals(evt.getName())) {
                            List<PartnerDTO> lstPartner = (List<PartnerDTO>) ((ListModelList) lbxAttribute.getModel()).getInnerList();
                            List<Long> lstId = new ArrayList<Long>();
                            int total = lbxAttribute.getSelectedCount();
                            int ok = 0;
                            for (Listitem item : lbxAttribute.getSelectedItems()) {
                                PartnerDTO partnerDTO = ((PartnerDTO) item.getValue());
                                Long id = partnerDTO.getId();
                                if (partnerService.checkValidateDelete(id)) {
                                    lstId.add(id);
                                    lstPartner.remove(partnerDTO);
                                    ok++;
                                }
                            }
                            if (lstId != null && lstId.size() > 0) {
                                partnerService.deletes(lstId);
                                contactService.deleteContactByPartnerId(lstId);
                                userService.deleteUser(lstId, ParamUtils.USER_TYPE.PARTNER);
                                partnerBankService.deleteByLstPartnerBank(lstId);
                            }
                            ListModelList model = new ListModelList(lstPartner);
                            model.setMultiple(true);
                            lbxAttribute.setModel(model);
                            Clients.showNotification(LanguageBundleUtils.getMessage(LanguageBundleUtils.getString("global.message.delete.multi.successful"),
                                    String.valueOf(ok),
                                    String.valueOf(total)), null, null, "middle_center", 10000);
                        }
                    }
                });
            } else {
                Clients.showNotification(LanguageBundleUtils.getString("global.action.confirm.is_select"), "warning", null, "middle_center", 3000);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    public void loadComboboxParntnerType(Combobox id) throws Exception {
        List<KeyValueBean> lstDataType = new ArrayList<KeyValueBean>();
        lstDataType.addAll(partnerService.getListPartnerType());
        listPartnerType = new ListModelList<KeyValueBean>();
        listPartnerType.addAll(lstDataType);
        if (listPartnerType != null && listPartnerType.size() > 0) {
            listPartnerType.addToSelection(listPartnerType.getElementAt(0));
        }
        id.setModel(listPartnerType);
        id.setValue(LanguageBundleUtils.getString("global.combobox.choose"));
    }

    public void loadComboboxParntnerStatus(Combobox id) throws Exception {
        List<KeyValueBean> lstDataStatus = new ArrayList<KeyValueBean>();
        lstDataStatus.addAll(partnerService.getListPartnerStatus());
        listPartnerStatus = new ListModelList<KeyValueBean>();
        listPartnerStatus.addAll(lstDataStatus);
        if (listPartnerStatus != null && listPartnerStatus.size() > 0) {
            listPartnerStatus.addToSelection(listPartnerStatus.getElementAt(0));
        }
        id.setModel(listPartnerStatus);
        id.setValue(LanguageBundleUtils.getString("global.combobox.choose"));
    }

    public void bindDataToCombo() throws Exception {
        // add data to combobox ParntnerSatus
        loadComboboxParntnerStatus(cbbPartnerStatus);
        // add data to combobox ParntnerType
        loadComboboxParntnerType(cbbPartnerType);
    }

    public PartnerDTO getDataFromClient() {
        PartnerDTO object = new PartnerDTO();

        if (StringUtils.isValidString(txtCode.getValue())) {
            object.setPartnerCode(txtCode.getValue().trim());
        }

        if (StringUtils.isValidString(txtName.getValue())) {
            object.setCompanyName(txtName.getValue().trim());
        }

        if (cbbPartnerType.getSelectedIndex() > 0) {
            object.setPartnerTypeCode(cbbPartnerType.getSelectedItem().getValue().toString());
        }

        if (cbbPartnerStatus.getSelectedIndex() > 0) {
            object.setPartnerStatusCode(cbbPartnerStatus.getSelectedItem().getValue().toString());
        }

        if (StringUtils.isValidString(txtShortName.getValue())) {
            object.setCompanyNameShort(txtShortName.getValue().trim());
        }

        if (StringUtils.isValidString(txtAddress.getValue())) {
            object.setAddressOffice(txtAddress.getValue().trim());
        }
        return object;
    }

    public String formatDateTime(Date date) throws Exception {
        return StringUtils.formatDateTime(date);
    }

    public void bindDataToGrid() {
        try {
            PartnerDTO partnerDTO = getDataFromClient();
            lstResult = partnerService.getListPartner(partnerDTO);
            if (lstResult != null && !lstResult.isEmpty()) {
                ListModelList model = new ListModelList(lstResult);
                model.setMultiple(true);
                lbxAttribute.setModel(model);
                if (gridSearch != null) {
                    gridSearch.invalidate();
                }
            } else {
                lbxAttribute.setModel(new ListModelList());
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    //Code by Kien
    public void onClick$btnView() throws Exception {
        if (lbxAttribute.getSelectedCount() == 1) {

            List<PartnerDTO> lstPartner = (List<PartnerDTO>) ((ListModelList) lbxAttribute.getModel()).getInnerList();
            List<Long> lstId = new ArrayList<Long>();
            List<ContactDTO> listContact = null;
            int total = lbxAttribute.getSelectedCount();
            Long id;
            int ok = 0;
            PartnerDTO partnerDTO = new PartnerDTO();
            for (Listitem item : lbxAttribute.getSelectedItems()) {
                partnerDTO = ((PartnerDTO) item.getValue());
                id = partnerDTO.getId();
                if (!partnerService.checkValidateDelete(id)) {
                    listContact = contactService.getListByID(partnerDTO);
                }
            }

            Map<String, List<ContactDTO>> argu = new HashMap();
//               argu.put("ACTION", "");
//                  List<ContactDTO> indexs = new ArrayList();

//                indexs.add((ContactDTO) listContact);
//                argu.put("LIST_ACTION_INDEX", indexs);
            argu.put("LISTBOX", listContact);
            Window wd;
            wd = (Window) Executions.createComponents("/controls/partner/popupListContact.zul", null, argu);
            wd.doModal();

        } else {
            Clients.showNotification(LanguageBundleUtils.getString("global.action.confirm.is_select"), "warning", null, "middle_center", 3000);
        }
    }

    public void setCbbPartnerType(Combobox cbbPartnerType) {
        this.cbbPartnerType = cbbPartnerType;
    }
}
