/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.composer.contractManagement;

import com.viettel.csp.DTO.*;
import com.viettel.csp.composer.contract.ContractComposer;
import com.viettel.csp.service.*;
import com.viettel.csp.util.*;
import com.viettel.eafs.util.SpringUtil;
import com.viettel.plc.widget.FileInfo;
import com.viettel.plc.widget.IUploaderCallback;
import com.viettel.plc.widget.UploadComponent;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.ForwardEvent;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.*;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;

import static jxl.biff.BaseCellFeatures.logger;

/**
 * @author admin
 */
public class ContractEditManagementTypeOneOrTwoComposer extends GenericForwardComposer<Component> implements
        IUploaderCallback {

    private final ContactTypeService contactTypeService = SpringUtil.getBean("contactTypeService", ContactTypeService.class);
    private final ContactService contactService = SpringUtil.getBean("contactService", ContactService.class);
    private final ContractFilesService contractFilesService = SpringUtil.getBean("contractFilesService", ContractFilesService.class);
    private final ContractHisService contractHisService = SpringUtil.getBean("contractHisService", ContractHisService.class);
    private final ContactPointService contactPointService = SpringUtil.getBean("contactPointService", ContactPointService.class);
    private final ContractService contractService = SpringUtil.getBean("contractService", ContractService.class);
    Long partner_ID, contract_id;
    String contractType;
    ContractManagementComposer managementComposer;
    private Label txtContractNumber, txtCompanyName, txtHeadOfService, txtServiceCode, dbStartDate, dbEndDate, txtContractStatus, txtContractType, txtCtSignForm;
    private Long totalRow = 0L;
    private GenComponent genComponent = new GenComponent();
    private List<Component> lstInputCbbContact = new ArrayList<>();
    private List<ContactTypeDTO> lstInputContactType = new ArrayList<>();
    private List<Component> lstInputTextContactType = new ArrayList<>();
    private String action;
    private Rows rows;
    private Row lastRow;
    private ContractDTO cdto;
    private ContactPointDTO pointDTO;
    private Textbox txtFileContractNumber, txtContent, txtDescription, txtFileName;
    //    private Datebox dbStartDate, dbEndDate;
    private List<ContractFilesDTO> lstContractFiles = new ArrayList<>();
    private Listbox lbxViewAddContractManagement, lbxViewAddContract, lbxContractHis;
    private List<ContractFilesDTO> lstContractFilesDTO = new ArrayList<>();
    private List<ContractHisDTO> lstContractHisDTO = new ArrayList<>();
    private ContractDTO oldDTO;
    private List<String> filesLocation;
    private List<FileInfo> filesInfo;
    private UploadComponent uploadComponent;
    private ContractManagementComposer contractManagementComposer;
    private Button btnAddFilePopup, btnRemoveFilePopup, btnViewFilePopup, btnSavePopup, btnSendPartner;
    private Tabpanel tabLBXViewAddContract;
    private Tab tabFileContractNumberNameNew;
    private Groupbox formContactEdit;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);

        if (this.arg.containsKey("PARTNER_ID")) {
            this.partner_ID = (Long) this.arg.get("PARTNER_ID");
        }
        if (this.arg.containsKey("CONTRACT_ID")) {
            this.contract_id = (Long) this.arg.get("CONTRACT_ID");
        }
        if (this.arg.containsKey("contractType")) {
            this.contractType = (String) this.arg.get("contractType");
        }
        if (this.arg.containsKey(ParamUtils.ACTION)) {
            this.action = this.arg.get(ParamUtils.ACTION).toString();
        }
        if (this.arg.containsKey(ParamUtils.COMPOSER)) {
            this.managementComposer = (ContractManagementComposer) this.arg
                    .get(ParamUtils.COMPOSER);
        }
        if (this.arg.containsKey(ParamUtils.COMPOSER)) {
            this.contractManagementComposer = (ContractManagementComposer) this.arg
                    .get(ParamUtils.COMPOSER);
        }

        setVisibiale(UserTokenUtils.getPersonType());

        if ("002".equals(contractType)) {
            tabLBXViewAddContract.setVisible(false);
            uploadComponent.setVisible(false);
            lbxViewAddContract.setVisible(false);
            btnAddFilePopup.setVisible(false);
            btnRemoveFilePopup.setVisible(false);
            btnViewFilePopup.setVisible(false);
            tabFileContractNumberNameNew.setVisible(false);
        } else {
            uploadComponent.setUploaderCallback(this);
        }

        loadDataContactType();
        loadLBXContractFiles(this.contract_id);
        loadLBXContractHis(this.contract_id);
//        loadLBXContract(contract_id);
        setData();
    }

    public ContractManagementComposer getContractManagementComposer() {
        return this.contractManagementComposer;
    }

    public void setContractManagementComposer(
            ContractManagementComposer contractManagementComposer) {
        this.contractManagementComposer = contractManagementComposer;
    }

    public void onClick$btnSavePopup() {
        ContractDTO contractDTO = new ContractDTO();
        ContactPointDTO contactPointDTO = getDataFromClient();
        contactPointService.update(contactPointDTO.getListContactPointDTO());
        if (!"002".equals(contractType)) {
//            if (null == filesLocation || null == filesInfo) {
//                Clients.showNotification(LanguageBundleUtils.getString("contractFiles.messageFiles.error"), "warning", null, "middle_center", 3000);
//                return;
//            }
            if (filesInfo != null) {
                uploadComponent.onClick$doSave();
                for (int i = 0; i < filesInfo.size(); i++) {
                    ContractFilesDTO filesDTO = getDataFromClientContractFilesDTO(filesInfo.get(i).getLocation());
                    contractFilesService.insertFiles(filesDTO);
                }
            }
            if (lstContractFiles.size() > 0) {
                for (int i = 0; i < lstContractFiles.size(); i++) {
                    ContractFilesDTO filesDTO = lstContractFiles.get(i);
                    filesDTO.setFileType(ParamUtils.CONTRACT_PARTNER.CONTRACTFILESNAMEMENU);
                    filesDTO.setContractId(contract_id);
                    contractFilesService.insertFiles(filesDTO);
                }
            }
        }
        this.self.detach();

    }

    public void loadLBXContractFiles(Long contract_id) {
        ContractFilesDTO contractFilesDTO = new ContractFilesDTO();
        contractFilesDTO.setContractId(contract_id);
        this.lstContractFilesDTO = this.contractFilesService.getListMenuContractFiles(contractFilesDTO);
        if (this.lstContractFilesDTO != null) {
            ListModelList model = new ListModelList(this.lstContractFilesDTO);
            model.setMultiple(true);
            this.lbxViewAddContractManagement.setModel(model);
        }
    }

    public void onDeleteContract(ForwardEvent evt) {
        evt.stopPropagation();

        try {
            List<ContractFilesDTO> lstDeleteDTO = new ArrayList<>();
            Image btn = (Image) evt.getOrigin().getTarget();
            Listitem litem = (Listitem) btn.getParent().getParent();
            ContractFilesDTO deleteDTO = ((ContractFilesDTO) litem.getValue());
            lstDeleteDTO.add(deleteDTO);
            String message = this.contractFilesService.deleteContractFiles(lstDeleteDTO);
            Clients.showNotification(message, "suscess", null, "middle_center", 3000);
            loadLBXContractFiles(this.contract_id);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(ContractComposer.class.getName())
                    .log(Level.SEVERE, null, ex);
        }
    }

    public void onDownloadFileContract(ForwardEvent evt) {
        evt.stopPropagation();
        try {
            Image btn = (Image) evt.getOrigin().getTarget();
            Listitem litem = (Listitem) btn.getParent().getParent();
            onClick$btnDownloadFile(litem);
        } catch (Exception ex) {
//            java.util.logging.Logger.getLogger(ContractEditManagementType1.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onClick$btnDownloadFile(Listitem litem) {
        try {
            List<ContractFilesDTO> lstContractFilesResult = new ArrayList<>();
            lstContractFilesResult.add(litem.getValue());
            File file = new File(lstContractFilesResult.get(0).getPathFile());
            Filedownload.save(file, null);
        } catch (Exception ex) {
            Messagebox.show(LanguageBundleUtils.getMessage("messageUploadFileError"),
                    LanguageBundleUtils.getMessage("Messagebox.warning"), Messagebox.OK,
                    Messagebox.ERROR,
                    null);
        }
    }


    public void loadLBXContractHis(Long contract_id) {
        ContractHisDTO contractHisDTO = new ContractHisDTO();
        contractHisDTO.setContractId(contract_id);
        this.lstContractHisDTO = this.contractHisService.getListContractHisAddUser(contractHisDTO);
        if (this.lstContractHisDTO != null) {
            ListModelList model = new ListModelList(this.lstContractHisDTO);
            model.setMultiple(true);
            this.lbxContractHis.setModel(model);
        }
    }

    public void loadLBXContract(Long contract_id) {
        ContractFilesDTO contractFilesDTO = new ContractFilesDTO();
        contractFilesDTO.setContractId(contract_id);
        this.lstContractFilesDTO = this.contractFilesService.getMenuContractFile(contractFilesDTO);
        if (this.lstContractFilesDTO != null) {
            ListModelList model = new ListModelList(this.lstContractFilesDTO);
            model.setMultiple(true);
            this.lbxViewAddContract.setModel(model);
        }
    }

    public void loadDataContactType() {
        ContactDTO contactDTO = new ContactDTO();
        contactDTO.setPartnerId(this.partner_ID);
        Row tmpRow;
        Component tmpComp;
        Component firstComp = null;
        try {
            //Bank
            List<ContactTypeDTO> lstContactTypeDTO = this.contactTypeService.findAll();
            List<ContactDTO> lstContactDTO = this.contactService.getList(contactDTO);
            StringBuilder parValue = new StringBuilder();
            for (ContactDTO cdto : lstContactDTO) {
                parValue.append(cdto.getId());
                parValue.append(";");
                parValue.append(cdto.getName());
                parValue.append(";");
            }
            for (ContactTypeDTO contactTypeDTO : lstContactTypeDTO) {
                ContactPointDTO cpdto = new ContactPointDTO();
                cpdto.setContactType(contactTypeDTO.getCode());
                cpdto.setContractId(contract_id);
                List<ContactPointDTO> list = contactService.checkList(cpdto);
                totalRow++;
                tmpRow = new Row();
                ComponentDTO componentDTO = new ComponentDTO();
                componentDTO.setAttributeType(ParamUtils.ReportDataType.COMBOBOX);
                componentDTO.setAttributeType(ParamUtils.ReportDataType.COMBOVALUE);
                componentDTO.setIsRequired(false);
                componentDTO.setStandardAttributeId(String.valueOf("cbb" + this.totalRow));
                componentDTO
                        .setDefaultValue(LanguageBundleUtils.getString("global.combobox.choose"));
                for (ContactPointDTO dTO : list) {
                    componentDTO.setSelectItemValue(String.valueOf(
                            list != null && list.size() > 0 ? dTO.getContactId()
                                    : ParamUtils.SELECT_NOTHING_VALUE_STR));
                    contactTypeDTO.setContactPointID(dTO.getId());
                }
                componentDTO.setAttributeName(contactTypeDTO.getName());

                componentDTO.setInitValue(parValue.toString());
                tmpComp = this.genComponent.createComponent(componentDTO);
                this.genComponent.createCellLabel(componentDTO.getAttributeName(), componentDTO.getIsRequired(), tmpRow);
                if (tmpComp != null) {
                    tmpRow.appendChild(tmpComp);
                    this.lstInputCbbContact.add(tmpComp);
                    this.lstInputContactType.add(contactTypeDTO);
                    if (firstComp == null) {
                        firstComp = tmpComp;
                    }
                }
                this.rows.insertBefore(tmpRow, this.lastRow);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    public ContactPointDTO getDataFromClient() {
        Long contactID = 0L;
        pointDTO = new ContactPointDTO();
        List<ContactPointDTO> lstContactPointDTO = new ArrayList<>();
        Map<Object, Object> t = this.genComponent.getDataFromControls(this.lstInputCbbContact);
        List<Combobox> lstCbbContactDTO = new ArrayList(t.keySet());
        Collections.sort(lstCbbContactDTO, new Comparator<Combobox>() {
            @Override
            public int compare(Combobox o1, Combobox o2) {
                return o1.getId().compareTo(o2.getId());
            }
        });
        for (int i = 0; i < lstCbbContactDTO.size(); i++) {
            ContactPointDTO contactPointDTO = new ContactPointDTO();
            String v = (String) t.get(lstCbbContactDTO.get(i));
            contactID = Long.parseLong(v);
//            if (contactID != -99) {
            contactPointDTO.setContractId(contract_id);
            contactPointDTO.setContactId(contactID);
            contactPointDTO.setContactType(lstInputContactType.get(i).getCode());
            contactPointDTO.setId(lstInputContactType.get(i).getContactPointID());
            lstContactPointDTO.add(contactPointDTO);
//            }
        }
        pointDTO.setListContactPointDTO(lstContactPointDTO);
        return pointDTO;
    }

    public void onClick$btnAddFilePopup() {
        try {
            Map<String, Object> argu = new HashMap();
            argu.put(ParamUtils.ACTION, ParamUtils.ACTION_CREATE);
            argu.put(ParamUtils.COMPOSER, this);
            Window wd;
            wd = (Window) Executions.createComponents("controls/contractManagement/contractManagerAddFilePopup.zul", null, argu);
            wd.doModal();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    public void onClick$btnViewFilePopup() {
        try {
            Map<String, Object> argu = new HashMap();
            argu.put(ParamUtils.ACTION, ParamUtils.ACTION_UPDATE_FILE);
            List<Integer> indexs = new ArrayList();
            indexs.add(this.lbxViewAddContract.getSelectedItem().getIndex());
            argu.put("LIST_ACTION_INDEX", indexs);
            argu.put("LISTBOX", this.lbxViewAddContract.getSelectedItem().getValue());
            argu.put(ParamUtils.COMPOSER, this);
            this.lstContractFiles.remove(this.lstContractFiles.get(this.lbxViewAddContract.getSelectedIndex()));
            this.lbxViewAddContract.removeItemAt(this.lbxViewAddContract.getSelectedIndex());
            Window wd;
            wd = (Window) Executions.createComponents("controls/contractManagement/contractManagerAddFilePopup.zul", null, argu);
            wd.doModal();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    public void setDataGrid(ContractFilesDTO contractFilesDTO) {
        this.lstContractFiles.add(contractFilesDTO);
        ListModelList model = new ListModelList(this.lstContractFiles);
        model.setMultiple(true);
        this.lbxViewAddContract.setModel(model);
    }

    public String formatDateTime(Date date) throws Exception {
        return StringUtils.formatDateTime(date);
    }

    public void setData() throws Exception {
        if (ParamUtils.ACTION_CREATE.equals(this.action)) {
            ((Window) this.self).setTitle(LanguageBundleUtils.getString("global.add"));

        } else if (ParamUtils.ACTION_UPDATE.equals(this.action)) {
            ContractDTO objectSearch = new ContractDTO();
            objectSearch.setContractId(this.contract_id);
            List<ContractDTO> list = this.contractService.getListContract(objectSearch);
            this.oldDTO = list.get(0);
            ((Window) this.self).setTitle(LanguageBundleUtils.getString("global.edit"));
            if (this.oldDTO != null) {
                this.txtCompanyName.setValue(this.oldDTO.getCompanyName());
                this.txtContractNumber.setValue(this.oldDTO.getAddressHeadOffice());
                this.txtHeadOfService.setValue(this.oldDTO.getPartnerCode());
                this.txtServiceCode.setValue(this.oldDTO.getAddressTradingOffice());
                String date = "dd/MM/yyyy";
                String dateStr = null;
                String dateStrEnd = null;
                DateFormat df = new SimpleDateFormat(date);
                dateStr = df.format(this.oldDTO.getStartDate());
                dateStrEnd = df.format(this.oldDTO.getEndDate());
                this.dbStartDate.setValue(dateStr);
                this.dbEndDate.setValue(dateStr);
                this.txtCtSignForm.setValue(this.oldDTO.getCtSignForm());
                this.txtContractStatus.setValue(this.oldDTO.getContractStatusName());
                this.txtContractType.setValue(this.oldDTO.getContractType());
            }
        }
    }

    @Override
    public void uploadedFilesInfoCallback(List<FileInfo> filesInfo) {
        this.filesInfo = filesInfo;
        System.out.println(filesInfo);
    }

    public ContractFilesDTO getDataFromClientContractFilesDTO(String fileLocation) {
        ContractFilesDTO contractFilesDTO = new ContractFilesDTO();
        contractFilesDTO.setFileName(new File(fileLocation).getName());
        contractFilesDTO.setPathFile(new File(fileLocation).getPath());
        contractFilesDTO.setFileType(ParamUtils.CONTRACT_PARTNER.CONTRACTFILESNAME);
        contractFilesDTO.setContractId(this.contract_id);
        return contractFilesDTO;
    }

    @Override
    public void uploadedFilesLocationCallback(List<String> filesLocation) {
        this.filesLocation = filesLocation;
    }

    public void onClick$btnSendPartner() throws Exception {

//        ContractDTO contractDTO = new ContractDTO();
        ContactPointDTO contactPointDTO = getDataFromClient();
        contactPointService.update(contactPointDTO.getListContactPointDTO());
//        List<ContractFilesDTO> contractFilesDTOs = new ArrayList<>();
        if (!"002".equals(contractType)) {
            uploadComponent.onClick$doSave();
            if (null == filesLocation || null == filesInfo) {
                Clients.showNotification(LanguageBundleUtils.getString("contractFiles.messageFiles.error"), "warning", null, "middle_center", 3000);
                return;
            }
            if (filesInfo != null) {
                for (int i = 0; i < filesInfo.size(); i++) {

                    ContractFilesDTO filesDTO = getDataFromClientContractFilesDTO(filesInfo.get(i).getLocation());
                    contractFilesService.insertFiles(filesDTO);
                }
            }
            if (lstContractFiles.size() > 0) {
                for (int i = 0; i < lstContractFiles.size(); i++) {
                    ContractFilesDTO filesDTO = lstContractFiles.get(i);
                    filesDTO.setFileType(ParamUtils.CONTRACT_PARTNER.CONTRACTFILESNAMEMENU);
                    filesDTO.setContractId(contract_id);
                    contractFilesService.insertFiles(filesDTO);
                }
            }
        }
        ContractDTO contractDTO = new ContractDTO();
        contractDTO.setContractId(this.contract_id);
        List<ContractDTO> lstContractDTO = this.contractService.getListContract(contractDTO);
        ContractDTO cdto = lstContractDTO.get(0);
        cdto.setContractStatus(String.valueOf(ParamUtils.CONTRACTSENDPARTNER));
        this.contractService.updateContractSendPartner(contractDTO, this.action,
                ParamUtils.CONTRACT_PARTNER.CONTRACTSENDPARTNER);
        this.contractManagementComposer.bindData();
        this.self.detach();
    }

    public void onClick$btnRemoveFilePopup() {
        try {
            if (this.lbxViewAddContract.getSelectedCount() > 0) {
                this.lstContractFiles
                        .remove(this.lstContractFiles.get(this.lbxViewAddContract.getSelectedIndex()));
                this.lbxViewAddContract.removeItemAt(this.lbxViewAddContract.getSelectedIndex());
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients
                    .showNotification(LanguageBundleUtils.getString("global.message.error"), "warning",
                            null,
                            "middle_center", 3000);
        }
    }

//    public void onClick$btnContractRegister() throws Exception {
//        Map<String, Object> argu = new HashMap();
//        argu.put(ParamUtils.ACTION, ParamUtils.ACTION_UPDATE_FILE);
//        argu.put("CONTRACT_ID", contract_id);
//        argu.put(ParamUtils.COMPOSER, this);
//        Window wd;
//        wd = (Window) Executions.createComponents("controls/contractManagement/contracManagertRegisterViettelB.zul", null, argu);
//        wd.doModal();
//    }

    public void close() {
        self.detach();
    }

    private void setVisibiale(String contractStatus) {
        if (contractStatus.equals(ParamUtils.USER_TYPE.CONTACT)) {
            tabLBXViewAddContract.setVisible(false);
            uploadComponent.setVisible(false);
            lbxViewAddContract.setVisible(false);
            btnAddFilePopup.setVisible(false);
            btnRemoveFilePopup.setVisible(false);
            btnViewFilePopup.setVisible(false);
            tabFileContractNumberNameNew.setVisible(false);
            btnSavePopup.setVisible(false);
            btnSendPartner.setVisible(false);
        } else if (contractStatus.equals(ParamUtils.USER_TYPE.VIETTEL)) {
            tabLBXViewAddContract.setVisible(false);
            uploadComponent.setVisible(false);
            lbxViewAddContract.setVisible(false);
            btnAddFilePopup.setVisible(false);
            btnRemoveFilePopup.setVisible(false);
            btnViewFilePopup.setVisible(false);
            tabFileContractNumberNameNew.setVisible(false);
            btnSavePopup.setVisible(false);
            btnSendPartner.setVisible(false);
        }
    }

    private void setDisable(String contractStatus) {
        if (contractStatus.equals(ParamUtils.USER_TYPE.CONTACT)) {
        } else if (contractStatus.equals(ParamUtils.USER_TYPE.VIETTEL)) {

        }
    }

    public void onClick$btnclosePopup() {
        self.detach();
    }
}
