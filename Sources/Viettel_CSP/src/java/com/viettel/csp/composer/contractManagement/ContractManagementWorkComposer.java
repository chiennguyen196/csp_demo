package com.viettel.csp.composer.contractManagement;

import com.viettel.csp.DTO.*;
import com.viettel.csp.composer.contract.ContractComposer;
import com.viettel.csp.entity.UserEntity;
import com.viettel.csp.service.*;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.util.StringUtils;
import com.viettel.csp.util.UserTokenUtils;
import com.viettel.eafs.util.SpringUtil;
import org.apache.log4j.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.ForwardEvent;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.*;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.stream.Collectors;

public class ContractManagementWorkComposer extends GenericForwardComposer<Component> {

    private static final Logger logger = Logger.getLogger(ContractManagementWorkComposer.class);

    private Label lbContractNumber, lbNamePartner, lbServiceGroup, lbServiceGroupName, lbService, lbServiceName, lbHeadOfService, lbDateBegin, lbDateEnd, lbContracType, lbContracTypeName, lbContractStatusName, lbContractStatus;
    private Label txtContractNumber, txtCompanyName, txtHeadOfService, txtServiceCode, dbStartDate, dbEndDate, txtContractStatus, txtContractType, txtCtSignForm;
    private Grid grdContactPoint;
    private Bandbox bbSearchStaff;

    private ContractDTO contractDTO;
    private List<ContactPointDTO> contractPointDTOs;
    private List<ContractFilesDTO> contractFilesDTOs;
    private List<UserEntity> userEntities;
    private Listbox lbxContractFiles;
    private Listbox lbxStaff;
    private Map<String, String> mapParent;
    private Long contractId;
    private ContractManagementComposer managementComposer;

    private ContactPointService contactPointService = SpringUtil.getBean("contactPointService", ContactPointService.class);
    private ContractService contractService = SpringUtil.getBean("contractService", ContractService.class);
    private UserService userService = SpringUtil.getBean("userService", UserService.class);
    private ContractHisService contractHisService = SpringUtil.getBean("contractHisService", ContractHisService.class);
    private ContractFilesService contractFilesService = SpringUtil.getBean("contractFileService", ContractFilesService.class);
    private List<UserDTO> userDTOs;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        if (this.arg.containsKey("CONTRACT")) {
            this.contractDTO = (ContractDTO) this.arg.get("CONTRACT");
        }
        if (this.arg.containsKey(ParamUtils.COMPOSER)) {
            this.managementComposer = (ContractManagementComposer) this.arg.get(ParamUtils.COMPOSER);
        }
        if (arg.containsKey("MAP_SERVER_GROUP")) {
            this.mapParent = (Map<String, String>) arg.get("MAP_SERVER_GROUP");
        }

        this.bindData();
    }

    public void onClick$btnDone() {
        try {
            if (this.lbxStaff.getSelectedCount() == 1) {
                contractDTO.setDeployUserId(((UserEntity) this.lbxStaff.getSelectedItem().getValue()).getId());
                contractDTO.setDeployAssignDate(Calendar.getInstance().getTime());
                String message = this.contractService.update(contractDTO, ParamUtils.ACTION_UPDATE, ParamUtils.TYPE_ACTION.SAVE_AND_DEPLOYMENT_CONTRACT);
                ContractHisDTO hisEntity = new ContractHisDTO();
                hisEntity.setContractId(this.contractId);
                hisEntity.setStatusAfter(contractDTO.getContractStatus());
                hisEntity.setStatusBefore(String.valueOf(ParamUtils.TYPE_ACTION.SAVE_AND_DEPLOYMENT_CONTRACT));
                hisEntity.setCreateBy(UserTokenUtils.getUserId());
                hisEntity.setCreateAt(Calendar.getInstance().getTime());
                hisEntity.setContent(LanguageBundleUtils.getString("message.profile.create.contract.profile.approve"));
                String messageHis = this.contractHisService.insert(this.contractId, hisEntity);
                if (ParamUtils.SUCCESS.equals(message) && ParamUtils.SUCCESS.equals(messageHis)) {
                    Clients.showNotification(LanguageBundleUtils.getString("global.message.create.successful"), "info", null, "middle_center", 1000);
                    self.detach();
                } else {
                    Clients.showNotification("Thất bại cập nhật", "warning", null, "middle_center", 3000);
                }
            } else {
                Clients.showNotification(LanguageBundleUtils.getString("contract.staff.select.error"),
                        "warning", null, "middle_center", 3000);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"), "warning",
                    null, "middle_center", 3000);
        }
    }

    private void bindData() throws Exception {
        this.contractId = contractDTO.getContractId();

        this.txtCompanyName.setValue(this.contractDTO.getCompanyName());
        this.txtContractNumber.setValue(this.contractDTO.getAddressHeadOffice());
        this.txtHeadOfService.setValue(this.contractDTO.getPartnerCode());
        this.txtServiceCode.setValue(this.contractDTO.getAddressTradingOffice());
        String date = "dd/MM/yyyy";
        DateFormat df = new SimpleDateFormat(date);
        String dateStr = df.format(this.contractDTO.getStartDate());
        String dateStrEnd = df.format(this.contractDTO.getEndDate());
        this.dbStartDate.setValue(dateStr);
        this.dbEndDate.setValue(dateStrEnd);
        this.txtCtSignForm.setValue(this.contractDTO.getCtSignForm());
        this.txtContractStatus.setValue(this.contractDTO.getContractStatusName());
        this.txtContractType.setValue(this.contractDTO.getContractType());

        this.contractPointDTOs = this.contactPointService.getList(this.contractId);
        if (this.contractPointDTOs != null && !this.contractPointDTOs.isEmpty()) {
            ListModelList model = new ListModelList(this.contractPointDTOs);
            model.setMultiple(false);
            grdContactPoint.setModel(model);
        } else {
            grdContactPoint.setModel(new ListModelList());
        }

        //this.userEntities = this.userService.findAllEntities();
        this.userDTOs = this.userService.findAllEntitiesWithPersonType(ParamUtils.USER_TYPE.VIETTEL);
        if (this.userDTOs != null && !this.userDTOs.isEmpty()) {
            ListModelList model = new ListModelList(this.userDTOs);
            model.setMultiple(false);
            lbxStaff.setModel(model);
        } else {
            lbxStaff.setModel(new ListModelList());
        }
        ContractFilesDTO contractFilesDTO = new ContractFilesDTO();
        contractFilesDTO.setContractId(this.contractId);
        this.contractFilesDTOs = this.contractFilesService.getListContractFiles(contractFilesDTO);
        if (this.contractFilesDTOs != null && !this.contractFilesDTOs.isEmpty()) {
            ListModelList model = new ListModelList(this.contractFilesDTOs);
            model.setMultiple(false);
            lbxContractFiles.setModel(model);
        } else {
            lbxContractFiles.setModel(new ListModelList());
        }
    }

    public void onDownloadFileContract(ForwardEvent evt) {
        evt.stopPropagation();
        try {
            Image btn = (Image) evt.getOrigin().getTarget();
            Listitem litem = (Listitem) btn.getParent().getParent();
            onClick$btnDownloadFile(litem);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            java.util.logging.Logger.getLogger(ContractComposer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onClick$btnDownloadFile(Listitem litem) {
        try {
            List<ContractFilesDTO> lstContractFilesResult = new ArrayList<>();
            lstContractFilesResult.add(litem.getValue());
            File file = new File(lstContractFilesResult.get(0).getPathFile());
            Filedownload.save(file, null);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Messagebox.show(LanguageBundleUtils.getMessage("messageUploadFileError"), LanguageBundleUtils.getMessage("Messagebox.warning"), Messagebox.OK, Messagebox.ERROR, null);
        }
    }

    public String formatDateTime(Date date) throws Exception {
        return StringUtils.formatDateTime(date);
    }
}
