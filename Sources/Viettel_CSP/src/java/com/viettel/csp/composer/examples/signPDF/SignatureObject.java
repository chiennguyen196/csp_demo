/*
 * Copyright (C) 2010 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.viettel.csp.composer.examples.signPDF;

import java.io.Serializable;
import java.security.cert.Certificate;
import java.util.List;
//import javax.xml.ws.Holder;

/**
 * this object is used in client - server signing
 * @author $sonnt38@viettel.com.vn
 * @since since_text
 * @version 1.0
 */
public class SignatureObject implements Serializable {

    private boolean isOOXML;
    private byte[] digest;
    private byte[] signature;
    private List<String> _certificateChain;

    public boolean isOOXML() {
        return isOOXML;
    }

    public void setOOXML(boolean isOOXML) {
        this.isOOXML = isOOXML;
    }

    public void setDigest(byte[] digest) {
        this.digest = digest;
    }

    public byte[] getDigest() {
        return digest;
    }

    public List<String> getCertificateChain() {
        return _certificateChain;
    }

    public void setCertificateChain(List<String> _certificateChain) {
        this._certificateChain = _certificateChain;
    }

    public void setSignature(byte[] signature) {
        this.signature = signature;
    }

    public byte[] getSignature() {
        return signature;
    }
}
