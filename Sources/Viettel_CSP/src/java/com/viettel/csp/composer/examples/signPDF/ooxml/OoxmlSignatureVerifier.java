/*
 * Copyright (C) 2010 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.viettel.csp.composer.examples.signPDF.ooxml;

import be.fedict.eid.applet.service.signer.jaxb.opc.relationships.CTRelationship;
import be.fedict.eid.applet.service.signer.jaxb.opc.relationships.CTRelationships;
import be.fedict.eid.applet.service.signer.jaxb.xades132.ObjectFactory;
import be.fedict.eid.applet.service.signer.ooxml.OOXMLProvider;
import be.fedict.eid.applet.service.signer.ooxml.OOXMLURIDereferencer;
import com.viettel.csp.composer.examples.signPDF.EngVerifier;
import com.viettel.csp.composer.examples.signPDF.Verifier;
import com.viettel.csp.composer.examples.signPDF.certificate.OcspClient;
import com.viettel.csp.composer.examples.signPDF.certificate.OcspClientBouncyCastle;
import com.viettel.csp.composer.examples.signPDF.certificate.X509ExtensionUtil;
import com.viettel.csp.composer.examples.signPDF.certificate.X509VerificationUtil;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.crypto.dsig.XMLSignature;
import javax.xml.crypto.dsig.XMLSignatureFactory;
import javax.xml.crypto.dsig.dom.DOMValidateContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.io.IOUtils;
import org.bouncycastle.ocsp.CertificateStatus;
import org.bouncycastle.ocsp.RevokedStatus;
import org.bouncycastle.ocsp.UnknownStatus;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * signature verifier for OOXML type.
 *
 * @author sonnt38@viettel.com.vn
 * @version 1.1
 * @since 14-01-2011
 */
public class OoxmlSignatureVerifier {

    Logger logger = Logger.getLogger(OoxmlSignatureVerifier.class.getName());
    /**
     * origin type of Signature, see XMLDsig for detail
     */
    public static final String DIGITAL_SIGNATURE_ORIGIN_REL_TYPE
            = "http://schemas.openxmlformats.org/package/2006/relationships/digital-signature/origin";
    /**
     * type of Signature, see XMLDsig for detail
     */
    public static final String DIGITAL_SIGNATURE_REL_TYPE
            = "http://schemas.openxmlformats.org/package/2006/relationships/digital-signature/signature";
    /**
     * See <tt>Unmarshaller</tt> class for detail
     */
    private final Unmarshaller relationshipsUnmarshaller;

    /**
     * constructor to create Unmarshaller object
     */
    public OoxmlSignatureVerifier() {
        try {
            JAXBContext relationshipsJAXBContext = JAXBContext.newInstance(ObjectFactory.class);
            this.relationshipsUnmarshaller = relationshipsJAXBContext.createUnmarshaller();
        } catch (JAXBException e) {
            throw new RuntimeException("JAXB error: " + e.getMessage(), e);
        }
    }

    /**
     * Class initialed
     */
    private static boolean initialed = false;

    /**
     * Must call initial method at least one time.
     */
    public static void initial() {
        if (!initialed) {
            OOXMLProvider.install();
            initialed = true;
        }
    }

    /**
     * Checks whether the file referred by the given URL is an OOXML document.
     *
     * @param url the URL of file
     * @return <CODE>true</CODE> if OOXML, <CODE>false</CODE> if not
     * @throws IOException if error
     */
    public static boolean isOOXML(URL url) throws IOException {
        ZipInputStream zipInputStream = new ZipInputStream(url.openStream());
        ZipEntry zipEntry;
        while (null != (zipEntry = zipInputStream.getNextEntry())) {
            if (!"[Content_Types].xml".equals(zipEntry.getName())) {
                continue;
            }
            return true;
        }
        return false;
    }

    /**
     * Checks whether the file referred by the given URL is an OOXML document.
     *
     * @param name the name of file
     * @return <CODE>true</CODE> if OOXML, <CODE>false</CODE> if not
     * @throws IOException if error
     */
    public static boolean isOOXML(String name) throws IOException {
        ZipInputStream zipInputStream = new ZipInputStream(new FileInputStream(name));
        ZipEntry zipEntry;
        while (null != (zipEntry = zipInputStream.getNextEntry())) {
            if (!"[Content_Types].xml".equals(zipEntry.getName())) {
                continue;
            }
            return true;
        }
        return false;
    }

    /**
     * Checks whether the file referred by the given URL is an OOXML document.
     *
     * @param is <CODE>InputStream</CODE> of file
     * @return <CODE>true</CODE> if OOXML, <CODE>false</CODE> if not
     * @throws IOException if error
     */
    public static boolean isOOXML(InputStream is) throws IOException {
        ZipInputStream zipInputStream = new ZipInputStream(is);
        ZipEntry zipEntry;
        while (null != (zipEntry = zipInputStream.getNextEntry())) {
            if (!"[Content_Types].xml".equals(zipEntry.getName())) {
                continue;
            }
            return true;
        }
        return false;
    }

    /**
     * Checks whether the file referred by the given URL is an OOXML document.
     *
     * @param file byte array of file
     * @return <CODE>true</CODE> if OOXML, <CODE>false</CODE> if not
     * @throws IOException if error
     */
    public static boolean isOOXML(byte[] file) throws IOException {
        ZipInputStream zipInputStream = new ZipInputStream(new ByteArrayInputStream(file));
        ZipEntry zipEntry;
        while (null != (zipEntry = zipInputStream.getNextEntry())) {
            if (!"[Content_Types].xml".equals(zipEntry.getName())) {
                continue;
            }
            return true;
        }
        return false;
    }

    /**
     * verify against the CA certificate
     *
     * @param url file to verify
     * @param certificate CA certificate
     * @param time time to verify or <CODE>null</CODE> if current time
     * @return <CODE>List</CODE> of verifier
     * @throws Exception if error
     */
    public List<Verifier> verify(URL url, Certificate certificate, Calendar time) throws Exception {
        if (url == null) {
            throw new NullPointerException("File OOXML is not null");
        }
        if (!isOOXML(url)) {
            throw new DigitalSignatureException("File type is not OOXML");
        }

        if (false == (certificate instanceof X509Certificate)) {
            throw new DigitalSignatureException("Supported X509 Certificate only");
        }

        if (certificate == null) {
            throw new NullPointerException("Certificate is not null");
        }
        /*
         * get all signature resources
         */
        List<String> signatureResourceNames = getSignatureResourceNames(url);

        /*
         * get all verifier
         */
        List<Verifier> verifiers = new ArrayList<Verifier>();
        for (String signatureResourceName : signatureResourceNames) {
            Document signatureDocument = getSignatureDocument(url,
                    signatureResourceName);
            if (null == signatureDocument) {
                continue;
            }

            OOXMLURIDereferencer dereferencer = new OOXMLURIDereferencer(url);
            verifiers.add(verifySingleSignature(dereferencer, signatureDocument, certificate, time));

        }
        return verifiers;
    }

    private Verifier verifySingleSignature(OOXMLURIDereferencer dereferencer, Document signatureDocument, Certificate certificate, Calendar calendar) throws Exception {

        NodeList signatureNodeList = signatureDocument.getElementsByTagNameNS(XMLSignature.XMLNS, "Signature");
        NodeList signatureNodeLists = signatureDocument.getElementsByTagNameNS(XMLSignature.XMLNS, "Signature");
        if (0 == signatureNodeList.getLength()) {
            return null;
        }

        Node signatureNode = signatureNodeList.item(0);

        Date signedDate = parserTime(getTime(signatureNode));

        KeyInfoKeySelector keySelector = new KeyInfoKeySelector();
        DOMValidateContext domValidateContext = new DOMValidateContext(
                keySelector, signatureNode);
        domValidateContext.setProperty(
                "org.jcp.xml.dsig.validateManifests", Boolean.TRUE);
        domValidateContext.setURIDereferencer(dereferencer);

        XMLSignatureFactory xmlSignatureFactory = XMLSignatureFactory.getInstance();
        XMLSignature xmlSignature = xmlSignatureFactory.unmarshalXMLSignature(domValidateContext);
        boolean validity = xmlSignature.validate(domValidateContext);
        // TODO: check what has been signed.

        Certificate[] chain = keySelector.getCertificateChain();
        String signer = X509ExtensionUtil.getSubject((X509Certificate) chain[0]);
        String location = X509ExtensionUtil.getLocation((X509Certificate) chain[0]);
        Verifier verifier = new EngVerifier(signer, signedDate, !validity, location);
        Object[] res = X509VerificationUtil.verifyCertificates(chain, certificate, calendar);
        if (res != null) {
            String valid = "";
            for (Object o : res) {
                if (o instanceof X509Certificate) {
                    X509Certificate cert = (X509Certificate) o;
                    valid += cert.getSubjectDN();
                } else if (o instanceof String) {
                    valid += " " + o;
                }
            }
            verifier.setInvalidReason(valid);
        } else {
            if (chain.length > 1) {
                X509Certificate inner = (X509Certificate) chain[0];
                X509Certificate ca = (X509Certificate) chain[1];
                String ocspurl = X509ExtensionUtil.getOCSPURL(inner);
                if (ocspurl != null) {
                    try {
                        OcspClient oc = new OcspClientBouncyCastle(inner, ca, ocspurl);
                        CertificateStatus cs = oc.checkOCSP();
                        if (cs == CertificateStatus.GOOD) {
                            verifier.setValid(true);
                        } else if (cs instanceof RevokedStatus) {
                            verifier.setInvalidReason("revoked certificate");
                        } else if (cs instanceof UnknownStatus) {
                            verifier.setInvalidReason("unknown online status");
                        }
                    } catch (Exception ex) {
                        logger.log(Level.SEVERE, ex.getMessage(), ex);
                        verifier.setInvalidReason("can not check certificate online because of connection");
                    }
                } else {
                    verifier.setInvalidReason("certificate is not support check online");
                }
            } else {
                verifier.setInvalidReason("Not enough certificates to check online");
            }
        }
        return verifier;
    }

    /**
     * verify against the store
     *
     * @param url file to verify
     * @param store store contains some certificates
     * @param time time to verify or <CODE>null</CODE> if current time
     * @return <CODE>List</CODE> of verifier
     * @throws Exception
     */
    public List<Verifier> verify(URL url, KeyStore store, Calendar time) throws Exception {
        if (!isOOXML(url)) {
            throw new DigitalSignatureException("File type is not OOXML");
        }
        if (url == null) {
            throw new NullPointerException("File OOXML is not null");
        }
        /*
         * get all signature resources
         */
        List<String> signatureResourceNames = getSignatureResourceNames(url);
        List<Verifier> verifiers = new ArrayList<Verifier>();
        for (String signatureResourceName : signatureResourceNames) {
            Document signatureDocument = getSignatureDocument(url,
                    signatureResourceName);
            if (null == signatureDocument) {
                continue;
            }

            OOXMLURIDereferencer dereferencer = new OOXMLURIDereferencer(url);
            verifiers.add(verifySingleSignature(dereferencer, signatureDocument, store, time));

        }
        return verifiers;
    }

    private Verifier verifySingleSignature(OOXMLURIDereferencer dereferencer, Document signatureDocument, KeyStore store, Calendar calendar) throws Exception {

        NodeList signatureNodeList = signatureDocument.getElementsByTagNameNS(XMLSignature.XMLNS, "Signature");
        NodeList signatureNodeLists = signatureDocument.getElementsByTagNameNS(XMLSignature.XMLNS, "Signature");
        if (0 == signatureNodeList.getLength()) {
            return null;
        }

        Node signatureNode = signatureNodeList.item(0);

        Date signedDate = parserTime(getTime(signatureNode));

        KeyInfoKeySelector keySelector = new KeyInfoKeySelector();
        DOMValidateContext domValidateContext = new DOMValidateContext(
                keySelector, signatureNode);
        domValidateContext.setProperty(
                "org.jcp.xml.dsig.validateManifests", Boolean.TRUE);
        domValidateContext.setURIDereferencer(dereferencer);

        XMLSignatureFactory xmlSignatureFactory = XMLSignatureFactory.getInstance();
        XMLSignature xmlSignature = xmlSignatureFactory.unmarshalXMLSignature(domValidateContext);
        boolean validity = xmlSignature.validate(domValidateContext);
        // TODO: check what has been signed.

        Certificate[] chain = keySelector.getCertificateChain();
        String signer = X509ExtensionUtil.getSubject((X509Certificate) chain[0]);
        String location = X509ExtensionUtil.getLocation((X509Certificate) chain[0]);
        Verifier verifier = new EngVerifier(signer, signedDate, !validity, location);
        String res = X509VerificationUtil.verifyCertificates(chain, store, null, calendar);
        if (res != null) {
            verifier.setInvalidReason(res);
        } else {
            if (chain.length > 1) {
                X509Certificate inner = (X509Certificate) chain[0];
                X509Certificate ca = (X509Certificate) chain[1];
                String ocspurl = X509ExtensionUtil.getOCSPURL(inner);
                if (ocspurl != null) {
                    try {
                        OcspClient oc = (OcspClient) new OcspClientBouncyCastle(inner, ca, ocspurl);
                        CertificateStatus cs = oc.checkOCSP();
                        if (cs == CertificateStatus.GOOD) {
                            verifier.setValid(true);
                        } else if (cs instanceof RevokedStatus) {
                            verifier.setInvalidReason("revoked certificate");
                        } else if (cs instanceof UnknownStatus) {
                            verifier.setInvalidReason("unknown online status");
                        }
                    } catch (Exception ex) {
                        logger.log(Level.SEVERE, ex.getMessage(), ex);
                        verifier.setInvalidReason("can not check certificate online because of connection");
                    }
                } else {
                    verifier.setInvalidReason("certificate is not support check online");
                }
            } else {
                verifier.setInvalidReason("Not enough certificates to check online");
            }
        }
        return verifier;
    }

    public List<X509Certificate> getCertificate(URL fileURL) throws Exception {
        List<X509Certificate> arrReturn = new LinkedList<X509Certificate>();
        if (!isOOXML(fileURL)) {
            throw new DigitalSignatureException("File type is not OOXML");
        }
        /*
         * get all signature resources
         */
        List<String> signatureResourceNames = getSignatureResourceNames(fileURL);
        for (String signatureResourceName : signatureResourceNames) {
            Document signatureDocument = getSignatureDocument(fileURL,
                    signatureResourceName);
            if (null == signatureDocument) {
                continue;
            }

            OOXMLURIDereferencer dereferencer = new OOXMLURIDereferencer(fileURL);
            NodeList signatureNodeList = signatureDocument.getElementsByTagNameNS(XMLSignature.XMLNS, "Signature");
            NodeList signatureNodeLists = signatureDocument.getElementsByTagNameNS(XMLSignature.XMLNS, "Signature");
            if (0 == signatureNodeList.getLength()) {
                return null;
            }

            Node signatureNode = signatureNodeList.item(0);

            Date signedDate = parserTime(getTime(signatureNode));

            KeyInfoKeySelector keySelector = new KeyInfoKeySelector();
            DOMValidateContext domValidateContext = new DOMValidateContext(
                    keySelector, signatureNode);
            domValidateContext.setProperty(
                    "org.jcp.xml.dsig.validateManifests", Boolean.TRUE);
            domValidateContext.setURIDereferencer(dereferencer);

            XMLSignatureFactory xmlSignatureFactory = XMLSignatureFactory.getInstance();
            XMLSignature xmlSignature = xmlSignatureFactory.unmarshalXMLSignature(domValidateContext);
            boolean validity = xmlSignature.validate(domValidateContext);
            // TODO: check what has been signed.

            X509Certificate certificate = keySelector.getCertificate();
            arrReturn.add(certificate);
        }
        return arrReturn;
    }

    public String getFirstIssuer(URL fileURL) throws Exception {
        if (!isOOXML(fileURL)) {
            throw new DigitalSignatureException("File type is not OOXML");
        }
        /*
         * get all signature resources
         */
        List<String> signatureResourceNames = getSignatureResourceNames(fileURL);
        for (String signatureResourceName : signatureResourceNames) {
            Document signatureDocument = getSignatureDocument(fileURL,
                    signatureResourceName);
            if (null == signatureDocument) {
                continue;
            }

            OOXMLURIDereferencer dereferencer = new OOXMLURIDereferencer(fileURL);
            NodeList signatureNodeList = signatureDocument.getElementsByTagNameNS(XMLSignature.XMLNS, "Signature");
            NodeList signatureNodeLists = signatureDocument.getElementsByTagNameNS(XMLSignature.XMLNS, "Signature");
            if (0 == signatureNodeList.getLength()) {
                return null;
            }

            Node signatureNode = signatureNodeList.item(0);

            Date signedDate = parserTime(getTime(signatureNode));

            KeyInfoKeySelector keySelector = new KeyInfoKeySelector();
            DOMValidateContext domValidateContext = new DOMValidateContext(
                    keySelector, signatureNode);
            domValidateContext.setProperty(
                    "org.jcp.xml.dsig.validateManifests", Boolean.TRUE);
            domValidateContext.setURIDereferencer(dereferencer);

            XMLSignatureFactory xmlSignatureFactory = XMLSignatureFactory.getInstance();
            XMLSignature xmlSignature = xmlSignatureFactory.unmarshalXMLSignature(domValidateContext);
            boolean validity = xmlSignature.validate(domValidateContext);
            // TODO: check what has been signed.

            X509Certificate certificate = keySelector.getCertificate();
            return X509ExtensionUtil.getIssuerName(certificate);
        }
        return null;
    }

    /**
     * get signature XML from OOXML file
     *
     * @param url URL of file
     * @param signatureResourceName XML signature name
     * @return <CODE>Document</CODE> XML file
     * @throws IOException error in reading file
     * @throws ParserConfigurationException error in parsing file
     * @throws SAXException SAX error
     */
    public Document getSignatureDocument(URL url, String signatureResourceName)
            throws IOException, ParserConfigurationException, SAXException {
        return getSignatureDocument(url.openStream(), signatureResourceName);
    }

    /**
     * get signature XML from OOXML file
     *
     * @param file OOXML byte array
     * @param signatureResourceName XML signature name
     * @return <CODE>Document</CODE> XML file
     * @throws IOException error in reading file
     * @throws ParserConfigurationException error in parsing file
     * @throws SAXException SAX error
     */
    public Document getSignatureDocument(byte[] file, String signatureResourceName)
            throws IOException, ParserConfigurationException, SAXException {
        return getSignatureDocument(new ByteArrayInputStream(file), signatureResourceName);
    }

    /**
     * get signature XML from OOXML file
     *
     * @param documentInputStream OOXML stream
     * @param signatureResourceName XML signature name
     * @return <CODE>Document</CODE> XML file
     * @throws IOException error in reading file
     * @throws ParserConfigurationException error in parsing file
     * @throws SAXException SAX error
     */
    public Document getSignatureDocument(InputStream documentInputStream, String signatureResourceName)
            throws IOException, ParserConfigurationException, SAXException {
        ZipInputStream zipInputStream = new ZipInputStream(documentInputStream);
        ZipEntry zipEntry;
        while (null != (zipEntry = zipInputStream.getNextEntry())) {
            if (!signatureResourceName.equals(zipEntry.getName())) {
                continue;
            }
            Document signatureDocument = loadDocument(zipInputStream);
            return signatureDocument;
        }
        return null;
    }

    /**
     * get signature resources from OOXML file
     *
     * @param is input stream of file
     * @return <CODE>List</CODE> of resources name
     * @throws IOException if error in read stream
     * @throws JAXBException if error
     */
    public List<String> getSignatureResourceNames(byte[] document)
            throws IOException, JAXBException {
        List<String> signatureResourceNames = new LinkedList<String>();

        ZipInputStream zipInputStream = new ZipInputStream(
                new ByteArrayInputStream(document));

        ZipEntry zipEntry;
        while (null != (zipEntry = zipInputStream.getNextEntry())) {
            if ("_rels/.rels".equals(zipEntry.getName())) {
                break;
            }
        }
        if (null == zipEntry) {

            return signatureResourceNames;
        }

        String dsOriginPart = null;
        JAXBElement<CTRelationships> packageRelationshipsElement
                = (JAXBElement<CTRelationships>) this.relationshipsUnmarshaller.unmarshal(zipInputStream);

        CTRelationships packageRelationships = packageRelationshipsElement.getValue();

        List<CTRelationship> packageRelationshipList = packageRelationships.getRelationship();
        for (CTRelationship packageRelationship : packageRelationshipList) {
            if (DIGITAL_SIGNATURE_ORIGIN_REL_TYPE.equals(packageRelationship.getType())) {
                dsOriginPart = packageRelationship.getTarget();
                break;
            }
        }
        if (null == dsOriginPart) {

            return signatureResourceNames;
        }

        String dsOriginName = dsOriginPart.substring(dsOriginPart.lastIndexOf("/") + 1);

        String dsOriginSegment = dsOriginPart.substring(0,
                dsOriginPart.lastIndexOf("/"))
                + "/";

        String dsOriginRels = dsOriginSegment + "_rels/" + dsOriginName
                + ".rels";

        zipInputStream = new ZipInputStream(new ByteArrayInputStream(document));
        while (null != (zipEntry = zipInputStream.getNextEntry())) {
            if (dsOriginRels.equals(zipEntry.getName())) {
                break;
            }
        }
        if (null == zipEntry) {

            return signatureResourceNames;
        }

        JAXBElement<CTRelationships> dsoRelationshipsElement
                = (JAXBElement<CTRelationships>) this.relationshipsUnmarshaller.unmarshal(zipInputStream);
        CTRelationships dsoRelationships = dsoRelationshipsElement.getValue();
        List<CTRelationship> dsoRelationshipList = dsoRelationships.getRelationship();
        for (CTRelationship dsoRelationship : dsoRelationshipList) {
            if (DIGITAL_SIGNATURE_REL_TYPE.equals(dsoRelationship.getType())) {
                String signatureResourceName = dsOriginSegment
                        + dsoRelationship.getTarget();
                signatureResourceNames.add(signatureResourceName);
            }
        }

        return signatureResourceNames;
    }

    /**
     * get signature resources from OOXML file
     *
     * @param url URL of file
     * @return <CODE>List</CODE> of resources name
     * @throws IOException if error in read stream
     * @throws JAXBException if error
     */
    public List<String> getSignatureResourceNames(URL url) throws IOException, JAXBException {
        byte[] document = IOUtils.toByteArray(url.openStream());
        return getSignatureResourceNames(document);
    }

    private Document loadDocument(InputStream documentInputStream)
            throws ParserConfigurationException, SAXException, IOException {
        InputSource inputSource = new InputSource(documentInputStream);
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        documentBuilderFactory.setNamespaceAware(true);
        documentBuilderFactory.setExpandEntityReferences(false);
        documentBuilderFactory.setXIncludeAware(false);
        String FEATURE = "http://apache.org/xml/features/disallow-doctype-decl";
        documentBuilderFactory.setFeature(FEATURE, true);
        FEATURE = "http://xml.org/sax/features/external-general-entities";
        documentBuilderFactory.setFeature(FEATURE, false);
        FEATURE = "http://xml.org/sax/features/external-parameter-entities";
        documentBuilderFactory.setFeature(FEATURE, false);
        documentBuilderFactory.setNamespaceAware(true);
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.parse(inputSource);
        return document;
    }

    private String getTime(Node node) {
        Node n0 = node.getChildNodes().item(3);
        /*Signature properties*/
        Node n = n0.getChildNodes().item(1);
        /*Signature property*/
        Node n1 = n.getChildNodes().item(0);
        /*Mdssi:SignatureTime*/
        Node n2 = n1.getChildNodes().item(0);
        /*Mdssi:Value*/
        Node n3 = n2.getChildNodes().item(1);
        return n3.getTextContent();
    }

    private Date parserTime(String time) {
        DateTimeFormatter dtf = ISODateTimeFormat.dateTimeNoMillis();
        DateTime dt = dtf.parseDateTime(time);
        Date date = dt.toDate();
        return date;
    }

    private static class DigitalSignatureException extends Exception {

        public DigitalSignatureException(String file_type_is_not_OOXML) {
        }
    }
}
