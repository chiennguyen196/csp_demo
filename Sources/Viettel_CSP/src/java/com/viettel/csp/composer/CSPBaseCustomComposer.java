package com.viettel.csp.composer;

import org.apache.log4j.Logger;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Window;
import viettel.passport.util.Connector;

import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Created by tuannm33 on 6/29/2016.
 */
public class CSPBaseCustomComposer extends BaseCustomComposer {

    public void doLogOut() throws Exception {
        Locale locale = new Locale("en", "US");
        ResourceBundle bundle = ResourceBundle.getBundle("cas", locale);

        String redirectUrl = bundle.getString("logoutUrl") + "?appCode=" + Connector.domainCode + "&service=" + URLEncoder.encode(bundle.getString("service"), "UTF-8");

        HttpServletResponse res = (HttpServletResponse) execution.getNativeResponse();
        if (res != null) {
            res.sendRedirect(redirectUrl);
        } else {
            execution.sendRedirect(redirectUrl);
        }
        session.invalidate();
    }


    //<editor-fold desc="Description">
    @Override
    protected Logger getLogger() {
        return null;
    }

    @Override
    protected Window getWindow() {
        return null;
    }

    @Override
    protected Listbox getGridData() {
        return null;
    }

    @Override
    protected String getArgDTOKey() {
        return null;
    }

    @Override
    protected void buildSearchObject() {

    }
    //</editor-fold>

}
