/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.composer.contractManagement;

import com.viettel.csp.DTO.ComponentDTO;
import com.viettel.csp.DTO.ContactDTO;
import com.viettel.csp.DTO.ContactPointDTO;
import com.viettel.csp.DTO.ContactTypeDTO;
import com.viettel.csp.DTO.ContractDTO;
import com.viettel.csp.DTO.ContractFilesDTO;
import com.viettel.csp.DTO.ContractHisDTO;
import com.viettel.csp.DTO.ContractStatusDTO;
import com.viettel.csp.DTO.PartnerDTO;
import com.viettel.csp.composer.contract.ContractComposer;
import com.viettel.csp.constant.ContractStatus;
import com.viettel.csp.service.ContactPointService;
import com.viettel.csp.service.ContactService;
import com.viettel.csp.service.ContactTypeService;
import com.viettel.csp.service.ContractFilesService;
import com.viettel.csp.service.ContractHisService;
import com.viettel.csp.service.ContractService;
import com.viettel.csp.service.ContractStatusService;
import com.viettel.csp.service.PartnerService;
import com.viettel.csp.util.GenComponent;
import com.viettel.csp.util.KeyValueBean;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.util.ParamUtils.TYPE_ACTION;
import com.viettel.csp.util.StringUtils;
import com.viettel.eafs.util.SpringUtil;
import com.viettel.logincsp.AuthenticationService;
import com.viettel.plc.widget.FileInfo;
import com.viettel.plc.widget.IUploaderCallback;
import com.viettel.plc.widget.UploadComponent;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.ForwardEvent;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Button;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 * @author chitv@gemvietnam.com
 */
public class ContractManagerPopupEditEffectingContractComposer extends
    GenericForwardComposer<Component> implements IUploaderCallback {

    final static org.apache.log4j.Logger logger = org.apache.log4j.Logger
        .getLogger(ContractManagerPopupEditEffectingContractComposer.class);
    ContactPointService contactPointService = SpringUtil
        .getBean("contactPointService", ContactPointService.class);
    List<ContactPointDTO> contractPointDTOs;
    private AuthenticationService authenticationService = SpringUtil
        .getBean("authenticationService", AuthenticationService.class);
    private Label txtContractNumber, txtCompanyName, txtHeadOfService, txtServiceCode, dbStartDate, dbEndDate, txtContractStatus, txtContractType, txtCtSignForm;
    private Combobox cbbContractStatus;
    private Textbox txtContenReason;
    private ListModelList<KeyValueBean> kvbContractStatus;
    private List<KeyValueBean> lstContractStatus;
    private String action;
    private Long contractId, partner_ID;
    private ArrayList indexLst;
    private ContractDTO oldDTO;
    private PartnerDTO partNerDTO;
    private ContractFilesDTO contractFilesDTO;
    private Button btnAddContractFiles, btnSendContractFiles, btnSendReplayContractFiles, btnOKContractFiles;
    private Button btnCancleContract, btnOkContract, btnDownloadFile;
    private Listbox lbxContractFiles, lbxContractFilesPartner, lbxContractFilesHistory;
    private ContractService contractService = SpringUtil
        .getBean("contractService", ContractService.class);
    private PartnerService partnerService = SpringUtil
        .getBean("partnerService", PartnerService.class);
    private ContractStatusService contractStatusService = SpringUtil
        .getBean("contractStatusService", ContractStatusService.class);
    private ContractHisService contractHisService = SpringUtil
        .getBean("contractHisService", ContractHisService.class);
    private ContractFilesService contractFilesService = SpringUtil
        .getBean("contractFilesService", ContractFilesService.class);
    private ContactTypeService contactTypeService = SpringUtil
        .getBean("contactTypeService", ContactTypeService.class);
    private ContactService contactService = SpringUtil
        .getBean("contactService", ContactService.class);
    private ContractManagementComposer contractManagementComposer;
    private UploadComponent uploadComponent;
    private List<String> filesLocation;
    private List<FileInfo> filesInfo;
    private Long totalRow = 0L;
    private GenComponent genComponent = new GenComponent();
    private Rows rows;
    private Row lastRow;
    private List<Component> lstInputCbbContact = new ArrayList<>();
    private Map<String, String> mapParent = new HashMap<>();
    private Grid grdContactPoint;
    private ContractHisDTO contractHisDTO;

    @Override
    public void doAfterCompose(Component comp) {
        try {
            super.doAfterCompose(comp);
            if (this.arg.containsKey(ParamUtils.COMPOSER)) {
                this.contractManagementComposer = (ContractManagementComposer) this.arg
                    .get(ParamUtils.COMPOSER);
            }
            if (this.arg.containsKey(ParamUtils.ACTION)) {
                this.action = this.arg.get(ParamUtils.ACTION).toString();
            }
            if (this.arg.containsKey("ID") && StringUtils
                .isValidString(String.valueOf(this.arg.get("ID")))) {
                this.contractId = (Long) this.arg.get("ID");
            }
            if (this.arg.containsKey("PARTNER_ID")) {
                this.partner_ID = (Long) this.arg.get("PARTNER_ID");
            }
            if (this.arg.containsKey("LIST_ACTION_INDEX")) {
                this.indexLst = (ArrayList) this.arg.get("LIST_ACTION_INDEX");
            }
            if (this.arg.containsKey("MAP_SERVER_GROUP")) {
                this.mapParent = (Map<String, String>) this.arg.get("MAP_SERVER_GROUP");
            }

            this.uploadComponent.setUploaderCallback(this);
            setData(this.indexLst.get(0));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            Clients
                .showNotification(LanguageBundleUtils.getString("global.message.error"), "warning",
                    null, "middle_center", 3000);
        }
    }

    public void bindDataCbbContractStatus(String valueSelect) throws Exception {
        try {
            List<ContractStatusDTO> lstContractStatusDTO = this.contractStatusService.findAll();
            this.lstContractStatus = new ArrayList<>();
            this.lstContractStatus.add(new KeyValueBean(ParamUtils.DEFAULT_VALUE_STR,
                LanguageBundleUtils.getString("global.combobox.choose")));
            if (lstContractStatusDTO != null && lstContractStatusDTO.size() > 0) {
                for (ContractStatusDTO contractStatusDTO : lstContractStatusDTO) {
                    KeyValueBean key = new KeyValueBean();
                    key.setKey(contractStatusDTO.getCode());
                    key.setValue(contractStatusDTO.getName());
                    this.lstContractStatus.add(key);
                }
            }
            this.kvbContractStatus = new ListModelList(this.lstContractStatus);
            int i = 0;
            for (KeyValueBean itemParam : this.kvbContractStatus.getInnerList()) {
                if (itemParam.getKey() != null && String.valueOf(itemParam.getKey())
                    .equals(valueSelect)) {
                    this.kvbContractStatus.addToSelection(this.kvbContractStatus.getElementAt(i));
                    break;
                }
                i++;
            }
            this.cbbContractStatus.setModel(this.kvbContractStatus);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            Clients
                .showNotification(LanguageBundleUtils.getString("global.message.error"), "warning",
                    null, "middle_center", 3000);
        }
    }

    public void setData(Object indexLst) throws Exception {
        if (ParamUtils.ACTION_CREATE.equals(this.action)) {
            ((Window) this.self).setTitle(LanguageBundleUtils.getString("global.add"));

        } else if (ParamUtils.ACTION_UPDATE.equals(this.action)) {
            ContractDTO objectSearch = new ContractDTO();
            objectSearch.setContractId(this.contractId);
            List<ContractDTO> list = this.contractService
                .getListContract(objectSearch, this.authenticationService.getUserCredential());
            this.oldDTO = list.get(0);
            ((Window) this.self).setTitle(LanguageBundleUtils.getString("global.edit"));
            if (this.oldDTO != null) {
                bindDataToTabContractFiles(this.oldDTO.getContractId());
                bindDataToTabContractHis(this.oldDTO.getContractId());
                bindDataToTabContractFilesPartner(this.oldDTO.getContractId());
                this.txtCompanyName.setValue(this.oldDTO.getCompanyName());
                this.txtContractNumber.setValue(this.oldDTO.getAddressHeadOffice());
                this.txtHeadOfService.setValue(this.oldDTO.getPartnerCode());
                this.txtServiceCode.setValue(this.oldDTO.getAddressTradingOffice());
                String date = "dd/MM/yyyy";
                String dateStr = null;
                String dateStrEnd = null;
                DateFormat df = new SimpleDateFormat(date);
                dateStr = df.format(this.oldDTO.getStartDate());
                dateStrEnd = df.format(this.oldDTO.getEndDate());
                this.dbStartDate.setValue(dateStr);
                this.dbEndDate.setValue(dateStr);
                this.txtCtSignForm.setValue(this.oldDTO.getCtSignForm());
                this.txtContractStatus.setValue(this.oldDTO.getContractStatusName());
                this.txtContractType.setValue(this.oldDTO.getContractType());
                this.contractPointDTOs = this.contactPointService.getList(this.contractId);
                if (this.contractPointDTOs != null && !this.contractPointDTOs.isEmpty()) {
                    ListModelList model = new ListModelList(this.contractPointDTOs);
                    model.setMultiple(false);
                    this.grdContactPoint.setModel(model);
                }
            }
        }
    }

    public void onDownloadFileContract(ForwardEvent evt) {
        evt.stopPropagation();
        try {
            Image btn = (Image) evt.getOrigin().getTarget();
            Listitem litem = (Listitem) btn.getParent().getParent();
            List<ContractFilesDTO> lstContractFilesResult = new ArrayList<>();
            lstContractFilesResult.add(litem.getValue());
            File file = new File(lstContractFilesResult.get(0).getPathFile(),
                lstContractFilesResult.get(0).getPathFile());
            Filedownload.save(file, null);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(ContractComposer.class.getName())
                .log(Level.SEVERE, null, ex);
        }
    }


    private void bindDataToTabContractFiles(Long contractId) {
        try {
            ContractFilesDTO objectContractFiles = new ContractFilesDTO();
            objectContractFiles.setContractId(contractId);
            List<ContractFilesDTO> lstContractFilesResult = this.contractFilesService
                .getListContractFiles(objectContractFiles);
            if (lstContractFilesResult != null && !lstContractFilesResult.isEmpty()) {
                ListModelList model = new ListModelList(lstContractFilesResult);
                this.lbxContractFiles.setModel(model);
            } else {
                this.lbxContractFiles.setModel(new ListModelList());
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    private void bindDataToTabContractFilesPartner(Long contractId) {
        try {
            ContractFilesDTO objectContractFiles = new ContractFilesDTO();
            objectContractFiles.setContractId(contractId);
            List<ContractFilesDTO> lstContractFilesResult = this.contractFilesService
                .getListContractFiles(objectContractFiles);
            if (lstContractFilesResult != null && !lstContractFilesResult.isEmpty()) {
                ListModelList model = new ListModelList(lstContractFilesResult);
                this.lbxContractFilesPartner.setModel(model);
            } else {
                this.lbxContractFilesPartner.setModel(new ListModelList());
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    private void bindDataToTabContractHis(Long contractId) {
        try {
            ContractHisDTO objectContractHis = new ContractHisDTO();
            objectContractHis.setContractId(contractId);
            List<ContractHisDTO> lstContractHisResult = this.contractHisService
                .getListContractHis(objectContractHis);
            if (lstContractHisResult != null && !lstContractHisResult.isEmpty()) {
                ListModelList model = new ListModelList(lstContractHisResult);
                this.lbxContractFilesHistory.setModel(model);
            } else {
                this.lbxContractFilesHistory.setModel(new ListModelList());
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    public ContractDTO getDataFromClient() {
        this.oldDTO.setCompanyName(this.txtCompanyName.getValue().trim().toLowerCase());
        this.oldDTO.setContractNumber(this.txtContractNumber.getValue().trim());
        this.oldDTO.setHeadOfService(this.txtHeadOfService.getValue().trim());
        this.oldDTO.setServiceCode(this.txtServiceCode.getValue().trim());
        this.oldDTO.setContractStatusCode(this.txtContractStatus.getValue().trim());
        this.oldDTO.setContractType(this.txtContractType.getValue().trim());
        this.oldDTO.setCtSignForm(this.txtCtSignForm.getValue().trim());

        return this.oldDTO;
    }

    public PartnerDTO getDataFromClientPartner(ContractDTO contractDTO) {
        if (ParamUtils.ACTION_CREATE.equals(this.action)) {
            this.partNerDTO = new PartnerDTO();
            this.partNerDTO.setCompanyName(this.txtCompanyName.getValue().trim().toLowerCase());

        } else {
            if (ParamUtils.ACTION_UPDATE.equals(this.action)) {
                this.partNerDTO = new PartnerDTO();
                this.partNerDTO.setId(contractDTO.getPartnerId());
                this.partNerDTO.setCompanyName(contractDTO.getCompanyName());
                this.partNerDTO.setAddressHeadOffice(contractDTO.getAddressHeadOffice());
                this.partNerDTO.setPartnerCode(contractDTO.getPartnerCode());
                this.partNerDTO.setAddressTradingOffice(contractDTO.getAddressTradingOffice());
                this.partNerDTO.setCompanyNameShort(contractDTO.getCompanyNameShort());
                this.partNerDTO.setPhone(contractDTO.getPhone());
                this.partNerDTO.setRepName(contractDTO.getRepName());
            }
        }

        return this.partNerDTO;
    }

    public void onClick$btnPauseContract() {
        String messageContract = "";
        if (validate()) {
            this.oldDTO = getDataFromClient();
            messageContract = this.contractService
                .update(this.oldDTO, this.action, TYPE_ACTION.TERMINATED_CONTRACT);
            if (ParamUtils.SUCCESS.equals(messageContract)) {
                this.contractManagementComposer.bindDataToGrid();
                Clients.showNotification(
                    LanguageBundleUtils.getString("global.message.save.successful"), "info",
                    null, "middle_center", 1000);
                this.self.detach();
            } else {
                Clients
                    .showNotification(messageContract, "warning", null, "middle_center", 3000);
            }
        }
    }

    public void onClosePopup(Event event) {
        //nothing here
    }

    public void onSelectedReason(Event event) throws  Exception {
        int flat = ParamUtils.TYPE_ACTION.TERMINATED_CONTRACT;
        ContractHisDTO contractHisDTO = new ContractHisDTO();
        contractHisDTO.setContent(event.getData().toString());
        String messageContract = this.contractService.update(this.oldDTO, this.action, flat);
        this.contractHisService.insert(this.contractId, contractHisDTO);
        if (ParamUtils.SUCCESS.equals(messageContract)) {
            this.contractManagementComposer.bindDataToGrid();
            Clients.showNotification(
                LanguageBundleUtils.getString("global.message.create.successful"), "info",
                null, "middle_center", 1000);
            this.self.detach();
        } else {
            Clients
                .showNotification(messageContract, "warning", null, "middle_center", 3000);
        }
    }


    public void onClick$btnTerminateContract() {
        if (validate()) {
            Map<String, Object> argu = new HashMap();
            argu.put(ParamUtils.ACTION, ParamUtils.ACTION_UPDATE);
            argu.put("LIST_CONTRACT", this.oldDTO);
            argu.put(ParamUtils.COMPOSER, this);
            Window wd = (Window) Executions
                .createComponents("/controls/contract/effectingContractReasonPopup.zul", null,
                    argu);
            wd.doModal();
        }
    }

    public void onClick$btnLiquidateContract() {
        String messageContract = "";
        String messagePartner = "";
        int flat = 14;
        if (validate()) {
            messageContract = this.contractService.update(this.oldDTO, this.action, flat);
            if (ParamUtils.SUCCESS.equals(messagePartner)) {
                Clients.showNotification(
                    LanguageBundleUtils.getString("global.message.create.successful"), "info",
                    null, "middle_center", 1000);
                this.self.detach();
            } else {
                Clients
                    .showNotification(messageContract, "warning", null, "middle_center", 3000);
            }
        }
    }

    @Override
    public void uploadedFilesLocationCallback(List<String> filesLocation) {
        this.filesLocation = filesLocation;
        System.out.println(filesLocation);
    }

    @Override
    public void uploadedFilesInfoCallback(List<FileInfo> filesInfo) {
        this.filesInfo = filesInfo;
        System.out.println(filesInfo);
    }

    public void onClick$btnSendContractFiles() {
        try {
            String messageContract = "";
            String messagePartner = "";
            String messageHis = "";
            int flat = 2;
            if (validateFile()) {
                if (this.filesInfo.size() > 0) {
                    for (int i = 0; i < this.filesInfo.size(); i++) {
                        this.oldDTO = getDataFromClient();
                        this.contractFilesDTO = getDataFromClientContractFiles(this.oldDTO,
                            this.filesInfo.get(i));
                        messageHis = this.contractFilesService.insert(this.oldDTO,
                            this.contractFilesDTO);
                        messageContract = this.contractService
                            .update(this.oldDTO, this.action, flat);
                    }
                }
                if (ParamUtils.SUCCESS.equals(messageContract)) {
                    this.contractManagementComposer.bindDataToGrid();
                    Clients.showNotification(
                        LanguageBundleUtils.getString("global.message.create.successful"), "info",
                        null, "middle_center", 1000);
                    this.self.detach();
                } else {
                    Clients
                        .showNotification(messageContract, "warning", null, "middle_center", 3000);
                }
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients
                .showNotification(LanguageBundleUtils.getString("global.message.error"), "warning",
                    null, "middle_center", 3000);
        }
    }

    public ContractHisDTO getDataFromClientContractHis(ContractDTO contractDTO) {

        this.contractHisDTO = new ContractHisDTO();
        this.contractHisDTO.setContractId(this.oldDTO.getContractId());
        this.contractHisDTO.setContent(this.txtContenReason.getValue());
        return this.contractHisDTO;
    }

    public ContractFilesDTO getDataFromClientContractFiles(ContractDTO contractDTO,
        FileInfo fileInfo) {

        this.contractFilesDTO = new ContractFilesDTO();
        this.contractFilesDTO.setContractId(this.oldDTO.getContractId());
        this.contractFilesDTO.setFileName(new File(fileInfo.getLocation()).getName());
        this.contractFilesDTO.setPathFile(new File(fileInfo.getLocation()).getPath());
        this.contractFilesDTO.setDescription(fileInfo.getDescription());
        return this.contractFilesDTO;
    }

    public void onClick$btnCloseCSP() {
        try {
            this.self.detach();
            this.contractManagementComposer.bindDataToGrid();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients
                .showNotification(LanguageBundleUtils.getString("global.message.error"), "warning",
                    null, "middle_center", 3000);
        }
    }

    private boolean validateFile() {
        if (this.filesInfo == null) {
            Clients.showNotification(LanguageBundleUtils.getString("global.message.errorFile"),
                "warning", null, "middle_center", 3000);
            return false;
        }
        return true;
    }

    private boolean validate() {
        return true;
    }

    public String formatDateTime(Date date) throws Exception {
        return StringUtils.formatDateTime(date);
    }

    public void onClick$btnUpload() {
        try {
            Map<String, Object> argu = new HashMap();
            argu.put(ParamUtils.ACTION, ParamUtils.ACTION_CREATE);
            argu.put(ParamUtils.COMPOSER, this);
            Window wd = (Window) Executions
                .createComponents("/controls/widget/uploadPopup.zul", null, argu);
            wd.doModal();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    public void loadDataContactType() {
        ContactDTO contactDTO = new ContactDTO();
        contactDTO.setPartnerId(this.partner_ID);
        Row tmpRow;
        Component tmpComp;
        Component firstComp = null;
        try {
            //Bank
            List<ContactTypeDTO> lstContactTypeDTO = this.contactTypeService.findAll();
            List<ContactDTO> lstContactDTO = this.contactService.getList(contactDTO);

            for (ContactTypeDTO contactTypeDTO : lstContactTypeDTO) {
                this.totalRow++;
                tmpRow = new Row();
                ComponentDTO componentDTO = new ComponentDTO();
                componentDTO.setAttributeType(ParamUtils.ReportDataType.STRING);
                componentDTO.setAttributeLength("50");
                componentDTO.setIsRequired(false);
                componentDTO.setStandardAttributeId(String.valueOf("txt" + this.totalRow));
                componentDTO.setDefaultValue("");
                componentDTO.setAttributeName(contactTypeDTO.getName());
                StringBuilder parValue = new StringBuilder();
                for (ContactDTO cdto : lstContactDTO) {
                    parValue.append(cdto.getEmail());
                    parValue.append(";");
                }
                componentDTO.setInitValue(parValue.toString());
                tmpComp = this.genComponent.createComponent(componentDTO);
                this.genComponent
                    .createCellLabel(componentDTO.getAttributeName(), componentDTO.getIsRequired(),
                        tmpRow);
                if (tmpComp != null) {
                    tmpRow.appendChild(tmpComp);
                    this.lstInputCbbContact.add(tmpComp);
                    if (firstComp == null) {
                        firstComp = tmpComp;
                    }
                }
                this.rows.insertBefore(tmpRow, this.lastRow);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            Clients.showNotification(LanguageBundleUtils.getString("global.message.error"));
        }
    }

    public Listbox getLbxContractFiles() {
        return this.lbxContractFiles;
    }

    public void setLbxContractFiles(Listbox lbxContractFiles) {
        this.lbxContractFiles = lbxContractFiles;
    }

    public Listbox getLbxContractFilesPartner() {
        return this.lbxContractFilesPartner;
    }

    public void setLbxContractFilesPartner(Listbox lbxContractFilesPartner) {
        this.lbxContractFilesPartner = lbxContractFilesPartner;
    }

    public Listbox getLbxContractFilesHistory() {
        return this.lbxContractFilesHistory;
    }

    public void setLbxContractFilesHistory(Listbox lbxContractFilesHistory) {
        this.lbxContractFilesHistory = lbxContractFilesHistory;
    }

    public ListModelList<KeyValueBean> getKvbContractStatus() {
        return this.kvbContractStatus;
    }

    public void setKvbContractStatus(ListModelList<KeyValueBean> kvbContractStatus) {
        this.kvbContractStatus = kvbContractStatus;
    }

    public List<KeyValueBean> getLstContractStatus() {
        return this.lstContractStatus;
    }

    public void setLstContractStatus(List<KeyValueBean> lstContractStatus) {
        this.lstContractStatus = lstContractStatus;
    }

    public Combobox getCbbContractStatus() {
        return this.cbbContractStatus;
    }

    public void setCbbContractStatus(Combobox cbbContractStatus) {
        this.cbbContractStatus = cbbContractStatus;
    }

    public String getContractStatusHisName(String paramType) {
        return ContractStatus.getContractStatusName(paramType);
    }

}
