/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.DAO;

import com.viettel.csp.DTO.UserDTO;
import com.viettel.csp.entity.UserEntity;
import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.util.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;

import java.util.ArrayList;
import java.util.List;

/**
 * @author tiennv02
 */
public class UserDAO extends BaseCustomDAO<UserEntity> {

    private static Logger log = Logger.getLogger(UserDAO.class);

    public UserDAO() {
        tEntity = new UserEntity();
        tEntityClass = UserEntity.class;
    }

    public UserEntity fillByPersonInfoId(Long personInfoId, String personType) {
        Session session = getCurrentSession();
        session.beginTransaction();
        Criteria criteria = session.createCriteria(UserEntity.class);
        criteria.add(Restrictions.eq("personInfoId", personInfoId));
        criteria.add(Restrictions.eq("personType", personType));
        List<UserEntity> lst = criteria.list();
        session.getTransaction().commit();
        return lst != null & !lst.isEmpty() ? lst.get(0) : null;
    }

    public List<UserDTO> fillByPersonType(String personType, String search) {
        Session session = getCurrentSession();
        session.beginTransaction();
        StringBuilder sql = new StringBuilder();
        sql.append("select a.id id, a.USERNAME userName,a.FULLNAME fullName, a.MOBILE_NUMBER mobileNumber, b.ORGANIZATIONID organizationId, b.NAME departmentName");
        sql.append(" from TBL_USER a left join TBL_DEPARTMENT b on a.ORGANIZATIONID = b.ORGANIZATIONID ");
        sql.append(" where 1=1");
        if(StringUtils.isValidString(personType)){
            sql.append(" AND  a.person_Type = :personType  ");
        }
        if(StringUtils.isValidString(search)){
            sql.append(" AND (a.USERNAME like :search or a.FULLNAME like :search or b.NAME like :search)");
        }
        Query query = session.createSQLQuery(sql.toString())
                .addScalar("id", new LongType())
                .addScalar("userName", new StringType())
                .addScalar("fullName", new StringType())
                .addScalar("mobileNumber", new StringType())
                .addScalar("organizationId", new LongType())
                .addScalar("departmentName", new StringType())
                .setResultTransformer(Transformers.aliasToBean(UserDTO.class));
        query.setMaxResults(ParamUtils.SELECT_MAX_ROW);
        if(StringUtils.isValidString(personType)){
            query.setParameter("personType", personType);
        }
        if(StringUtils.isValidString(search)){
            query.setParameter("search", StringUtils.getParamLike(search));
        }
        List<UserDTO> lst = query.list();
        session.getTransaction().commit();
        return lst != null & !lst.isEmpty() ? lst : null;
    }

    public List<UserEntity> fillByUsername(String userName) {
        Session session = getCurrentSession();
        session.beginTransaction();
        Criteria criteria = session.createCriteria(UserEntity.class);
        criteria.add(Restrictions.eq("userName", userName));
        List<UserEntity> lst = criteria.list();
        session.getTransaction().commit();
        return lst != null & !lst.isEmpty() ? lst : null;
    }

    public boolean checkExistUserName(String userName) {
        Session session = getCurrentSession();
        session.beginTransaction();
        Criteria criteria = session.createCriteria(UserEntity.class);
        criteria.add(Restrictions.eq("userName", userName).ignoreCase());
        List<UserEntity> lst = criteria.list();
        session.getTransaction().commit();
        return lst != null & !lst.isEmpty() ? true : false;
    }

    public boolean checkExistEmail(String email) {
        Session session = getCurrentSession();
        session.beginTransaction();
        Criteria criteria = session.createCriteria(UserEntity.class);
        criteria.add(Restrictions.eq("email", email).ignoreCase());
        List<UserEntity> lst = criteria.list();
        session.getTransaction().commit();
        return lst != null & !lst.isEmpty() ? true : false;
    }

    public boolean checkExistMobileNumber(String mobileNumber) {
        Session session = getCurrentSession();
        session.beginTransaction();
        Criteria criteria = session.createCriteria(UserEntity.class);
        criteria.add(Restrictions.eq("mobileNumber", mobileNumber).ignoreCase());
        List<UserEntity> lst = criteria.list();
        session.getTransaction().commit();
        return lst != null & !lst.isEmpty() ? true : false;
    }

    public List<UserDTO> checkuser(UserDTO udto) {
        List<UserDTO> list = new ArrayList<>();
        try {
            Session session = getCurrentSession();
            session.beginTransaction();
            StringBuilder sql = new StringBuilder();
            sql.append("select USERNAME userName from TBL_USER"
                    + " where USERNAME = :USERNAME AND PASSWORD = :PASSWORD");
            Query query = session.createSQLQuery(sql.toString())
                    .addScalar("userName", new StringType())
                    .addScalar("email", new StringType());
            query.setParameter("USERNAME", udto.getUserName());
            query.setParameter("PASSWORD", udto.getPassword());
            list = query.list();
            session.getTransaction().commit();
        } catch (Exception e) {
        }
        return list;
    }

    public List<UserDTO> checkLogin(UserDTO udto) {
        List<UserDTO> list = new ArrayList<>();
        try {
            Session session = getCurrentSession();
            session.beginTransaction();
            StringBuilder sql = new StringBuilder();
//            sql.append("select USERNAME userName,EMAIL email,ID id from TBL_USER");
//            if (StringUtils.isValidString(udto.getUserName())) {
//                sql.append(" where UPPER(USERNAME) = UPPER(:USERNAME)");
//            }
//            if (StringUtils.isValidString(udto.getPassword())) {
//                sql.append(" AND PASSWORD = :PASSWORD");
//            }
//            Query query = session.createSQLQuery(sql.toString())
//                    .addScalar("userName", new StringType())
//                    .addScalar("email", new StringType())
//                    .addScalar("id", new LongType())
//                    .setResultTransformer(Transformers.aliasToBean(UserDTO.class));
//            if (StringUtils.isValidString(udto.getUserName())) {
//                query.setParameter("USERNAME", udto.getUserName());
//            }
//            if (StringUtils.isValidString(udto.getPassword())) {
//                query.setParameter("PASSWORD", udto.getPassword());
//            }
//            list = query.list();
            session.getTransaction().commit();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return list;
    }

    public UserEntity checkEntity(UserEntity userEntity) {
        UserEntity ue = new UserEntity();
        try {
            Session session = getCurrentSession();
            session.beginTransaction();
            Criteria criteria = session.createCriteria(UserEntity.class);
            criteria.add(Restrictions.eq("userName", userEntity.getUserName()).ignoreCase());
            List<UserEntity> list = criteria.list();
            if (list != null && list.size() > 0) {
                ue = list.get(0);
            } else {
                return null;
            }
            session.getTransaction().commit();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ue;
    }

    public List<UserDTO> findAllEntitiesWithPersonType(String personType) {
        Session session = getCurrentSession();
        session.beginTransaction();
        StringBuilder sql = new StringBuilder();
        sql.append("select a.id id, a.USERNAME userName,a.FULLNAME fullName, a.MOBILE_NUMBER mobileNumber, b.ORGANIZATIONID organizationId, b.NAME departmentName");
        sql.append(" from TBL_USER a left join TBL_DEPARTMENT b on a.ORGANIZATIONID = b.ORGANIZATIONID ");
        sql.append(" where a.person_Type = :personType ");
        Query query = session.createSQLQuery(sql.toString())
                .addScalar("id", Hibernate.LONG)
                .addScalar("userName", Hibernate.STRING)
                .addScalar("fullName", Hibernate.STRING)
                .addScalar("mobileNumber", Hibernate.STRING)
                .addScalar("organizationId", Hibernate.LONG)
                .addScalar("departmentName", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(UserDTO.class));
        query.setParameter("personType", personType);
        List<UserDTO> lst = query.list();
        session.getTransaction().commit();
        return lst;
    }

    public List<UserEntity> fillByFullname(String personType) {
        Session session = getCurrentSession();
        session.beginTransaction();
        Criteria criteria = session.createCriteria(UserEntity.class);
        criteria.add(Restrictions.eq("personType", personType));
        List<UserEntity> lst = criteria.list();
        session.getTransaction().commit();
        return lst != null & !lst.isEmpty() ? lst : null;
    }
}
