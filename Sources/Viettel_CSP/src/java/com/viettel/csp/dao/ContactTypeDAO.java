/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.viettel.csp.DAO;

import com.viettel.csp.DAO.BaseCustomDAO;
import com.viettel.csp.entity.BankEntity;
import com.viettel.csp.entity.ContactTypeEntity;
import org.apache.log4j.Logger;

/**
 *
 * @author GEM
 */
public class ContactTypeDAO  extends BaseCustomDAO<ContactTypeEntity> {
     private static Logger log = Logger.getLogger(ContactTypeDAO.class);

    public ContactTypeDAO() {
        tEntity = new ContactTypeEntity();
        tEntityClass = ContactTypeEntity.class;
    }
}
