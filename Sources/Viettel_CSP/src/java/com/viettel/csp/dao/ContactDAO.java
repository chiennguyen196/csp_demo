/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.DAO;

import com.viettel.csp.DTO.ContactDTO;
import com.viettel.csp.DTO.ContactPointDTO;
import com.viettel.csp.DTO.PartnerDTO;
import com.viettel.csp.entity.ContactEntity;
import com.viettel.csp.util.StringUtils;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.hibernate.type.DateType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;

/**
 *
 * @author tiennv02
 */
public class ContactDAO extends BaseCustomDAO<ContactEntity> {

    private static Logger log = Logger.getLogger(ContactDAO.class);

    public ContactDAO() {
        tEntity = new ContactEntity();
        tEntityClass = ContactEntity.class;
    }

    public List<ContactDTO> getList(ContactDTO objectSearch) {
        try {
            Session session = getCurrentSession();
            session.beginTransaction();
            StringBuilder sql = new StringBuilder("");
            sql.append("SELECT");
            sql.append("	c.username userName,");
            sql.append("	a.iD id,");
            sql.append("	a.contact_type contactType,");
            sql.append("	b.name contactTypeName,");
            sql.append("	a.partner_id partnerId,");
            sql.append("	a.NAME NAME,");
            sql.append("	a.email email,");
            sql.append("	a.FIXED_PHONE_NUMBER fixedPhoneNumber,");
            sql.append("	a.MOBILE_PHONE_NUMBER mobilePhoneNumber,");
            sql.append("	a.sex,");
            sql.append("	a.position,");
            sql.append("	a.department,");
            sql.append("	a.other_info otherInfo,");
            sql.append("	a.contact_status contactStatus,");
            sql.append("	a.create_at createAt,");
            sql.append("	a.create_by createBy,");
            sql.append("	a.update_at updateAt,");
            sql.append("	a.update_by updateBy");
            sql.append(" FROM");
            sql.append("	tbl_contact a");
            sql.append(" LEFT JOIN TBL_CONTACT_TYPE b ON b.CODE = a.contact_type");
            sql.append(" LEFT JOIN tbl_user c ON c.person_info_id = a.id");
            sql.append(" WHERE");
            sql.append(" 1 = 1");
            if (objectSearch != null) {
                if (StringUtils.isValidString(objectSearch.getUserName())) {
                    sql.append(" and LOWER(c.username) like :username ");
                }
                if (StringUtils.isValidString(objectSearch.getName())) {
                    sql.append(" and LOWER(a.name) like :name ");
                }
                if (StringUtils.isValidString(objectSearch.getContactStatus())) {
                    sql.append(" and LOWER(a.contact_status) = :contactStatus ");
                }
                if (StringUtils.isValidString(objectSearch.getContactType())) {
                    sql.append(" and LOWER(a.contact_type) = :contactType ");
                }
                if (StringUtils.isValidString(objectSearch.getPartnerId())) {
                    sql.append(" and a.PARTNER_ID = :partnerId ");
                }
                if (StringUtils.isValidLong(String.valueOf(objectSearch.getId()))) {
                    sql.append(" and a.iD = :aId ");
                }
            }
            //Set name param search
            Query query = session.createSQLQuery(sql.toString())
                    .addScalar("id", new LongType())
                    .addScalar("userName", new StringType())
                    .addScalar("contactType", new StringType())
                    .addScalar("contactTypeName", new StringType())
                    .addScalar("partnerId", new LongType())
                    .addScalar("name", new StringType())
                    .addScalar("email", new StringType())
                    .addScalar("fixedPhoneNumber", new StringType())
                    .addScalar("mobilePhoneNumber", new StringType())
                    .addScalar("sex", new StringType())
                    .addScalar("position", new StringType())
                    .addScalar("department", new StringType())
                    .addScalar("otherInfo", new StringType())
                    .addScalar("contactStatus", new StringType())
                    .addScalar("createAt", new DateType())
                    .addScalar("createBy", new LongType())
                    .addScalar("updateAt", new DateType())
                    .addScalar("updateBy", new LongType())
                    .setResultTransformer(Transformers.aliasToBean(ContactDTO.class));
            //Set value param search
            if (objectSearch != null) {
                if (StringUtils.isValidString(objectSearch.getUserName())) {
                    query.setParameter("username", StringUtils.getParamLike(objectSearch.getUserName()));
                }
                if (StringUtils.isValidString(objectSearch.getName())) {
                    query.setParameter("name", StringUtils.getParamLike(objectSearch.getName()));
                }
                if (StringUtils.isValidString(objectSearch.getContactStatus())) {
                    query.setParameter("contactStatus", StringUtils.getParamEquals(objectSearch.getContactStatus()));
                }
                if (StringUtils.isValidString(objectSearch.getContactType())) {
                    query.setParameter("contactType", StringUtils.getParamEquals(objectSearch.getContactType()));
                }
                if (StringUtils.isValidLong(String.valueOf(objectSearch.getPartnerId()))) {
                    query.setParameter("partnerId", StringUtils.getLong(objectSearch.getPartnerId()));
                }
                if (StringUtils.isValidLong(String.valueOf(objectSearch.getId()))) {
                    query.setParameter("aId", StringUtils.getLong(objectSearch.getId()));
                }
            }
            List<ContactDTO> list = query.list();
            session.getTransaction().commit();
            return list;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }

    public void deleteContactByPartnerId(Long partnerId) throws HibernateException {
        Session session = getCurrentSession();
        session.beginTransaction();

        StringBuilder sql = new StringBuilder()
                .append("  DELETE  FROM tbl_contact")
                .append("   WHERE   partner_id= :partnerId");

        Query query = session.createSQLQuery(sql.toString());
        query.setParameter("partnerId", partnerId);
        query.executeUpdate();
        session.getTransaction().commit();
    }

    //code By Kien
    public List<ContactDTO> getListByID(PartnerDTO partnerDTO) {
        try {
            Session session = getCurrentSession();
            session.beginTransaction();
            StringBuilder sql = new StringBuilder("");
            sql.append("SELECT");
            sql.append("	a.iD,");
            sql.append("	a.contact_type contactType,");
            sql.append("	b.name contactTypeName,");
            sql.append("	a.partner_id partnerId,");
            sql.append("	a.NAME NAME,");
            sql.append("	a.email email,");
            sql.append("	a.FIXED_PHONE_NUMBER fixedPhoneNumber,");
            sql.append("	a.MOBILE_PHONE_NUMBER mobilePhoneNumber,");
            sql.append("	a.sex,");
            sql.append("	a.position,");
            sql.append("	a.department,");
            sql.append("	a.other_info otherInfo,");
            sql.append("	a.contact_status contactStatus,");
            sql.append("	a.create_at createAt,");
            sql.append("	a.create_by createBy,");
            sql.append("	a.update_at updateAt,");
            sql.append("	a.update_by updateBy");
            sql.append(" FROM");
            sql.append("	tbl_contact a");
            sql.append(" LEFT JOIN TBL_CONTACT_TYPE b ON b.CODE = a.contact_type");
            //sql.append(" LEFT JOIN tbl_user c ON c.person_info_id = a.id");
            sql.append(" WHERE");
            sql.append(" a.partner_id = :PartnerId ");
            //Set name param search
            Query query = session.createSQLQuery(sql.toString())
                    .addScalar("id", new LongType())
                    .addScalar("contactType", new StringType())
                    .addScalar("contactTypeName", new StringType())
                    .addScalar("partnerId", new LongType())
                    .addScalar("name", new StringType())
                    .addScalar("email", new StringType())
                    .addScalar("fixedPhoneNumber", new StringType())
                    .addScalar("mobilePhoneNumber", new StringType())
                    .addScalar("sex", new StringType())
                    .addScalar("position", new StringType())
                    .addScalar("department", new StringType())
                    .addScalar("otherInfo", new StringType())
                    .addScalar("contactStatus", new StringType())
                    .addScalar("createAt", new DateType())
                    .addScalar("createBy", new LongType())
                    .addScalar("updateAt", new DateType())
                    .addScalar("updateBy", new LongType())
                    .setResultTransformer(Transformers.aliasToBean(ContactDTO.class));
            //Set value param search
            query.setParameter("PartnerId", partnerDTO.getId());
//            if (StringUtils.isLong(String.valueOf(objectSearch.getId()))) {
//                    query.setParameter("PartnerId", StringUtils.getLong(partnerDTO.getId()));
//                }
            List<ContactDTO> list = query.list();
            session.getTransaction().commit();
            return list;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return null;

    }

    public List<ContactPointDTO> checkList(ContactPointDTO pointDTO) {
        List<ContactPointDTO> list = new ArrayList();
        try {
            Session session = getCurrentSession();
            session.beginTransaction();
            StringBuilder sql = new StringBuilder("");
            sql.append("select a.NAME contactName, a.ID contactId, p.ID id, p.CONTACT_TYPE contactType from TBL_CONTACT a");
            sql.append(" INNER JOIN TBL_CONTACT_POINT p On p.CONTACT_ID = a.ID");
            sql.append(" INNER JOIN TBL_CONTACT_TYPE b On b.CODE = p.CONTACT_TYPE");
            sql.append(" INNER JOIN TBL_CONTRACT c On c.ID = p.CONTRACT_ID");
            if (StringUtils.isValidString(pointDTO.getContractId())) {
                sql.append(" WHERE p.CONTRACT_ID = :CONTRACT_ID");
            }
            if (StringUtils.isValidString(pointDTO.getContactType())) {
                sql.append(" and p.CONTACT_TYPE = :CONTACT_TYPE ");
            }
            Query query = session.createSQLQuery(sql.toString())
                    .addScalar("contactId", new LongType())
                    .addScalar("contactName", new StringType())
                    .addScalar("id", new LongType())
                    .addScalar("contactType", new StringType())
                    .setResultTransformer(Transformers.aliasToBean(ContactPointDTO.class));
            if (StringUtils.isValidString(pointDTO.getContractId())) {
                query.setParameter("CONTRACT_ID", pointDTO.getContractId());
            }
            if (StringUtils.isValidString(pointDTO.getContactType())) {
                query.setParameter("CONTACT_TYPE", pointDTO.getContactType());
            }
            list = query.list();
            session.getTransaction().commit();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return list;
    }
}
