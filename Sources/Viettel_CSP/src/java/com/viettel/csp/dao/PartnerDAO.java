/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.DAO;

import com.viettel.csp.DTO.PartnerDTO;
import com.viettel.csp.entity.PartnerEntity;
import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.util.StringUtils;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.viettel.csp.util.UserTokenUtils;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;

/**
 *
 * @author GEM
 */
public class PartnerDAO extends BaseCustomDAO<PartnerEntity> {

    private static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(PartnerDAO.class);

    public PartnerDAO() {
        tEntity = new PartnerEntity();
        tEntityClass = PartnerEntity.class;
    }

    public List<PartnerDTO> getListPartner(PartnerDTO partnerDTO) {
        List<PartnerDTO> lstPartner = getListPartnerNotViolation(partnerDTO);
        Session session = getCurrentSession();
        session.beginTransaction();
        long id;
        if (lstPartner != null && lstPartner.size() > 0) {
            for (int i = 0; i < lstPartner.size(); i++) {

                if (lstPartner.get(i).getId() != null && !lstPartner.get(i).getId().equals("")) {
                    id = lstPartner.get(i).getId();
                    StringBuilder sql = new StringBuilder()
                            .append("  SELECT    count(b.id)")
                            .append("    FROM   tbl_partner a   ")
                            .append("              LEFT JOIN   ")
                            .append("            tbl_contract_breach b")
                            .append("            ON  a.id = b.partner_id")
                            .append("   WHERE   1 = 1")
                            .append("            AND a.id = :id");
                    Query query = session.createSQLQuery(sql.toString());
                    query.setParameter("id", id);
                    Long countViolation = ((Number) query.uniqueResult()).longValue();
                    if (countViolation != 0) {
                        lstPartner.get(i).setViolation(countViolation);
                    }
                }
            }
        }
        session.getTransaction().commit();
        return lstPartner;
    }

    public List<PartnerDTO> getListPartnerNotViolation(PartnerDTO partnerDTO) throws HibernateException {
        List<PartnerDTO> lstResult = new ArrayList<PartnerDTO>();
        Session session = getCurrentSession();
        session.beginTransaction();
        StringBuilder sql = new StringBuilder()
                .append("  SELECT   a.id id,")
                .append("           d.username       userName, ")
                .append("           a.is_lock       isLock, ")
                .append("           a.partner_code       partnerCode, ")
                .append("           a.company_name       companyName, ")
                .append("           a.company_name_short companyNameShort, ")
                .append("           a.address_head_office  addressHeadOffice, ")
                .append("           a.address_trading_office   addressTradingOffice, ")
                .append("           a.email   email, ")
                .append("           a.business_regis_number   businessRegisNumber, ")
                .append("           a.business_regis_date   businessRegisDate, ")
                .append("           a.business_regis_count   businessRegisCount, ")
                .append("           a.partner_group   partnerGroup, ")
                .append("           a.partner_group_province   partnerGroupProvince, ")
                .append("           a.phone phone, ")
                .append("           a.rep_name repName, ")
                .append("           a.rep_position repPosition, ")
                .append("           a.tax_code taxCode, ")
                .append("           a.update_at updateAt, ")
                .append("           a.create_at createAt, ")
                .append("           a.create_by createBy, ")
                .append("           a.update_by updateBy, ")
                .append("           b.code  partnerStatusCode, ")
                .append("           b.name  partnerStatusName, ")
                .append("           c.code   partnerTypeCode, ")
                .append("           c.name   partnerTypeName ")
                .append("    FROM   tbl_partner a   ")
                .append("    LEFT JOIN tbl_partner_status b ON a.partner_status = b.code ")
                .append("    LEFT JOIN tbl_partner_type c ON a.partner_type = c.code ")
                .append("    LEFT JOIN tbl_user d ON d.person_info_id = a.id ")
                .append("    WHERE   1 = 1 and d.person_type = :personType");
        if (partnerDTO != null) {
            if (StringUtils.isValidString(partnerDTO.getPartnerCode())) {
                sql.append("            AND LOWER(partner_code) like :partnerCode ");
            }
            if (StringUtils.isValidString(partnerDTO.getCompanyName())) {
                sql.append("            AND LOWER(company_name) like :companyName ");
            }
            if (StringUtils.isValidString(partnerDTO.getPartnerTypeCode())) {
                sql.append("           AND LOWER(partner_type) like :partnerTypeCode ");
            }
            if (StringUtils.isValidString(partnerDTO.getCompanyNameShort())) {
                sql.append("            AND LOWER(company_name_short)  like :companyNameShort");
            }
            if (StringUtils.isValidString(partnerDTO.getAddressHeadOffice())) {
                sql.append("            AND LOWER(address_head_office) = :addressHeadOffice ");
            }
            if (StringUtils.isValidString(partnerDTO.getPartnerStatusCode())) {
                sql.append("            AND LOWER(partner_status) = :partnerStatusCode ");
            }
            if (StringUtils.isValidString(partnerDTO.getId())) {
                sql.append("            AND a.id = :id ");
            }
        }
        Query query = session.createSQLQuery(sql.toString())
                .addScalar("id", Hibernate.LONG)
                .addScalar("userName", Hibernate.STRING)
                .addScalar("isLock", Hibernate.LONG)
                .addScalar("partnerCode", Hibernate.STRING)
                .addScalar("companyName", Hibernate.STRING)
                .addScalar("companyNameShort", Hibernate.STRING)
                .addScalar("addressHeadOffice", Hibernate.STRING)
                .addScalar("addressTradingOffice", Hibernate.STRING)
                .addScalar("email", Hibernate.STRING)
                .addScalar("businessRegisNumber", Hibernate.STRING)
                .addScalar("businessRegisDate", Hibernate.DATE)
                .addScalar("businessRegisCount", Hibernate.STRING)
                .addScalar("partnerGroup", Hibernate.STRING)
                .addScalar("partnerGroupProvince", Hibernate.STRING)
                .addScalar("phone", Hibernate.STRING)
                .addScalar("repName", Hibernate.STRING)
                .addScalar("repPosition", Hibernate.STRING)
                .addScalar("taxCode", Hibernate.STRING)
                .addScalar("updateAt", Hibernate.DATE)
                .addScalar("createAt", Hibernate.DATE)
                .addScalar("createBy", Hibernate.LONG)
                .addScalar("updateBy", Hibernate.LONG)
                .addScalar("partnerStatusCode", Hibernate.STRING)
                .addScalar("partnerStatusName", Hibernate.STRING)
                .addScalar("partnerTypeCode", Hibernate.STRING)
                .addScalar("partnerTypeName", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(PartnerDTO.class));
        query.setParameter("personType", ParamUtils.USER_TYPE.PARTNER);
        if (partnerDTO != null) {
            if (StringUtils.isValidString(partnerDTO.getPartnerCode())) {
                query.setParameter("partnerCode", StringUtils.getParamLike(partnerDTO.getPartnerCode()));
            }
            if (StringUtils.isValidString(partnerDTO.getCompanyName())) {
                query.setParameter("companyName", StringUtils.getParamLike(partnerDTO.getCompanyName()));
            }
            if (StringUtils.isValidString(partnerDTO.getPartnerTypeCode())) {
                query.setParameter("partnerTypeCode", StringUtils.getParamLike(partnerDTO.getPartnerTypeCode()));
            }

            if (StringUtils.isValidString(partnerDTO.getCompanyNameShort())) {
                query.setParameter("companyNameShort", StringUtils.getParamLike(partnerDTO.getCompanyNameShort()));
            }

            if (StringUtils.isValidString(partnerDTO.getAddressHeadOffice())) {
                query.setParameter("addressHeadOffice", partnerDTO.getAddressHeadOffice().toLowerCase());
            }
            if (StringUtils.isValidString(partnerDTO.getPartnerStatusCode())) {
                query.setParameter("partnerStatusCode", partnerDTO.getPartnerStatusCode().toLowerCase());
            }
            if (StringUtils.isValidString(partnerDTO.getId())) {
                query.setParameter("id", partnerDTO.getId());
            }
        }
        lstResult = query.list();
        session.getTransaction().commit();
        return lstResult;
    }

    public void update(PartnerDTO partnerDTO) throws HibernateException {
        Session session = getCurrentSession();
        session.beginTransaction();
        Criteria criteria = session.createCriteria(PartnerEntity.class);
        criteria.add(Restrictions.eq("id", partnerDTO.getId()));
        List<PartnerEntity> lst = criteria.list();
        if (lst != null && !lst.isEmpty()) {
            PartnerEntity partnerEntity = lst.get(0);
            partnerEntity.setCompanyName(partnerDTO.getCompanyName());
            partnerEntity.setCompanyNameShort(partnerDTO.getCompanyNameShort());
            partnerEntity.setAddressHeadOffice(partnerDTO.getAddressOffice());
            partnerEntity.setAddressTradingOffice(partnerDTO.getAddressTradingOffice());
            partnerEntity.setPhone(partnerDTO.getPhone());
            partnerEntity.setRepName(partnerDTO.getRepName());
            partnerEntity.setRepPosition(partnerDTO.getRepPosition());
            partnerEntity.setEmail(partnerDTO.getEmail());
            partnerEntity.setTaxCode(partnerDTO.getTaxCode());
            partnerEntity.setBusinessRegisNumber(partnerDTO.getBusinessRegisNumber());
            partnerEntity.setBusinessRegisDate(partnerDTO.getBusinessRegisDate());
            partnerEntity.setBusinessRegisCount(partnerDTO.getBusinessRegisCount());
            partnerEntity.setPartnerGroup(partnerDTO.getPartnerGroup());
            partnerEntity.setPartnerGroupProvince(partnerDTO.getPartnerGroupProvince());
            if(StringUtils.isValidString(partnerDTO.getPartnerTypeCode())) {
                partnerEntity.setPartnerType(partnerDTO.getPartnerTypeCode());
            }
            if(StringUtils.isValidString(partnerDTO.getPartnerStatusCode())) {
                partnerEntity.setPartnerStatus(partnerDTO.getPartnerStatusCode());
            }
            partnerEntity.setUpdateBy(UserTokenUtils.getUserId());
            partnerEntity.setUpdateAt(new Date());
            session.saveOrUpdate(partnerEntity);
        }
        session.getTransaction().commit();
    }

    public void deletes(Long id) throws HibernateException {
        Session session = getCurrentSession();
        session.beginTransaction();

        StringBuilder sql = new StringBuilder()
                .append("  DELETE  FROM tbl_partner")
                .append("   WHERE   id= :id");

        Query query = session.createSQLQuery(sql.toString());
        query.setParameter("id", id);
        query.executeUpdate();
        session.getTransaction().commit();
    }

    public boolean checkExitsTblContract(Long id) throws HibernateException {
        Session session = getCurrentSession();
        session.beginTransaction();

        StringBuilder sql = new StringBuilder()
                .append("  SELECT   1")
                .append("    FROM   tbl_partner a,   ")
                .append("            tbl_contract b")
                .append("          WHERE a.id = :id and a.id = b.partner_id");

        Query query = session.createSQLQuery(sql.toString());
        query.setParameter("id", id);
        return (query.list() == null || query.list().size() == 0) ? true : false;
    }

    public boolean checkExitsTblContractBreach(Long id) throws HibernateException {
        Session session = getCurrentSession();
        session.beginTransaction();

        StringBuilder sql = new StringBuilder()
                .append("  SELECT   1")
                .append("    FROM   tbl_partner a,   ")
                .append("            tbl_contract b")
                .append("          WHERE a.id = :id and a.id = b.partner_id");

        Query query = session.createSQLQuery(sql.toString());
        query.setParameter("id", id);
        return (query.list() == null || query.list().size() == 0) ? true : false;
    }

    public List<PartnerEntity> getListPartnerContract(Long id) {
        Session session = getCurrentSession();
        session.beginTransaction();
        Criteria cr = session.createCriteria(PartnerEntity.class);
        cr.add(Restrictions.eq("id", id));
        List results = cr.list();
        return (results.size() > 0) ? results : null;
    }
}
