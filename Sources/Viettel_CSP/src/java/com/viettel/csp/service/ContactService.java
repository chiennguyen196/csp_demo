package com.viettel.csp.service;

import com.viettel.csp.DTO.ContactDTO;
import com.viettel.csp.DTO.ContactPointDTO;
import com.viettel.csp.DTO.PartnerDTO;
import com.viettel.csp.DTO.ResultInsertContactDTO;

import java.util.List;

public interface ContactService {

    public List<ContactDTO> getList(ContactDTO cdto);

    // fix by luong
    public ResultInsertContactDTO saveContact(ContactDTO cdto, String string);



    public String deleteContact(List<ContactDTO> list);

    public void deleteContactByPartnerId(List<Long> list);

    public String updatePassword(String string, String string1, String string2);

    public List<ContactDTO> getListByID(PartnerDTO partnerDTO);

    public ContactDTO getObjectDTOById(Long personInfoId);

    public List<ContactPointDTO> checkList(ContactPointDTO pointDTO);
}
