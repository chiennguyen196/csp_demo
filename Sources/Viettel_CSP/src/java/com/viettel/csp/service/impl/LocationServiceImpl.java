/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.service.impl;

import com.viettel.csp.DAO.LocationDAO;
import com.viettel.csp.DTO.LocationDTO;
import com.viettel.csp.entity.LocationEntity;
import com.viettel.csp.service.LocationService;
import com.viettel.csp.util.KeyValueBean;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.ParamUtils;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author GEM
 */
public class LocationServiceImpl extends BaseCustomServiceImpl<LocationDAO, LocationEntity, LocationDTO> implements LocationService {

    @Autowired
    private LocationDAO locationDAO;

    public LocationDAO getLocationDAO() {
        return locationDAO;
    }

    public void setLocationDAO(LocationDAO locationDAO) {
        this.locationDAO = locationDAO;
        super.set(null, new LocationEntity(), locationDAO, LocationDTO.class);
    }

    @Override
    public List<LocationDTO> getListProvince() throws Exception {
        List<LocationDTO> listLocation = locationDAO.getListProvince();
        return listLocation;
    }

    public List<KeyValueBean> getProvinceByCode(String provinceCode) throws Exception {
        List<KeyValueBean> listLocation = locationDAO.getProvinceByCode(provinceCode);
        return listLocation;
    }

    public List<KeyValueBean> getListDataToCombobox(String level, String codeParent) throws Exception {
        List<KeyValueBean> lstLocation = new ArrayList<KeyValueBean>();
        lstLocation.add(new KeyValueBean(ParamUtils.DEFAULT_VALUE_STR, LanguageBundleUtils.getString("global.combobox.choose")));
        if (level.equalsIgnoreCase("PROVINCE")) {
            List<LocationDTO> listLocation = locationDAO.getListProvince();
            for (LocationDTO object : listLocation) {
                KeyValueBean k = new KeyValueBean();
                k.setKey(object.getProvinceCode());
                k.setValue(object.getProvinceName());
                lstLocation.add(k);
            }
        } else if (level.equalsIgnoreCase("DISTRICT")) {
        } else {
        }
        return lstLocation;
    }
}
