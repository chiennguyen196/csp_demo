/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.service;

import com.viettel.csp.DTO.ReflectDTO;
import com.viettel.csp.entity.ReflectEntity;
import com.viettel.csp.entity.ReflectTypeEntity;
import com.viettel.csp.util.KeyValueBean;
import java.util.List;

/**
 *
 * @author GEM
 */
public interface ReflectService {

    public List<ReflectDTO> getListReflect(ReflectDTO reflectDTO) throws Exception;

    public List<KeyValueBean> getListReflectType() throws Exception;

    public ReflectTypeEntity getListReflectTypeByField(String colume, String value) throws Exception;

    public String insert(ReflectDTO reflectDTO) throws Exception;

    public String update(ReflectDTO reflectDTO) throws Exception;

    public long checkCode(String code);

    public String deleteReflect(List<ReflectDTO> lstDeleteDTO);

    public ReflectDTO getObjectDTOById(Long reflectId);

    public String saveChangeStatus(ReflectDTO reflectDTO);

    public String sendPartner(ReflectDTO reflectDTO) throws Exception;

    public ReflectEntity getById(Long reflectId);
}
