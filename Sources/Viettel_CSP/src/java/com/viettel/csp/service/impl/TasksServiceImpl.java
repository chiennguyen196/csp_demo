/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.service.impl;

import com.viettel.csp.DAO.TasksDAO;
import com.viettel.csp.DTO.TasksDTO;
import com.viettel.csp.entity.TasksEntity;
import com.viettel.csp.service.TasksService;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.util.SendMailUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Calendar;
import java.util.List;

/**
 * @author admin
 */
public class TasksServiceImpl extends BaseCustomServiceImpl<TasksDAO, TasksEntity, TasksDTO> implements TasksService {

    @Autowired
    TasksDAO tasksDAO;

    public TasksDAO getTasksDAO() {
        return tasksDAO;
    }

    public void setTasksDAO(TasksDAO tasksDAO) {
        super.set(locale, new TasksEntity(), tasksDAO, TasksDTO.class);
        this.tasksDAO = tasksDAO;
    }

    @Override
    public TasksEntity insert(TasksEntity tBO) {
        return tasksDAO.insert(tBO);
    }

    @Override
    public String delete(TasksEntity tasksEntity) throws Exception {
        return tasksDAO.delete(tasksEntity);
    }

    @Override
    public List<TasksEntity> findAlls(TasksDTO tasksDTO) throws Exception {
        return tasksDAO.findAlls(tasksDTO);
    }

    @Override
    public List<TasksDTO> findAllToDTO(TasksDTO tasksDTO) throws Exception {
        return tasksDAO.findAllToDTO(tasksDTO);
    }

    @Override
    public String updateStatus(TasksDTO tasksDTO) throws Exception {
        String message = ParamUtils.SUCCESS;
        try {
            TasksEntity tasksEntity = this.tasksDAO.findById(tasksDTO.getId());
            if (tasksEntity != null) {
                tasksEntity.setStatus(tasksDTO.getStatus());
                tasksDAO.saveOrUpdate(tasksEntity);
                SendMailUtils.sendMailAndNotification(null, null, null, null);
            } else
                logger.info("Sao lai co cong viec tai day....");
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            message = LanguageBundleUtils.getString("global.message.error");
        }

        return message;
    }

    @Override
    public String updateDoneTask(TasksDTO tasksDTO) throws Exception {
        String message = ParamUtils.SUCCESS;
        try {
            TasksEntity tasksEntity = this.tasksDAO.findById(tasksDTO.getId());
            if (tasksEntity != null) {
                tasksEntity.setStatus(tasksDTO.getStatus());
                tasksEntity.setCompleteDate(Calendar.getInstance().getTime());
                tasksDAO.saveOrUpdate(tasksEntity);
                SendMailUtils.sendMailAndNotification(null, null, null, null);
            } else
                logger.info("Sao lai co cong viec tai day....");
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            message = LanguageBundleUtils.getString("global.message.error");
        }

        return message;
    }

}
