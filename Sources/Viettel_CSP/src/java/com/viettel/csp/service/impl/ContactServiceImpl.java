/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.service.impl;

import com.viettel.csp.DAO.ContactDAO;
import com.viettel.csp.DAO.ContactPointDAO;
import com.viettel.csp.DAO.UserDAO;
import com.viettel.csp.DTO.*;
import com.viettel.csp.entity.ContactEntity;
import com.viettel.csp.entity.UserEntity;
import com.viettel.csp.service.ContactService;
import com.viettel.csp.service.UserService;
import com.viettel.csp.util.ConditionBean;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.util.SendMailUtils;
import com.viettel.csp.util.StringUtils;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author TienNV
 */
public class ContactServiceImpl extends
    BaseCustomServiceImpl<ContactDAO, ContactEntity, ContactDTO> implements ContactService {

    private final static org.apache.log4j.Logger logger = org.apache.log4j.Logger
        .getLogger(ContactServiceImpl.class);
//    private UserService userService = SpringUtil.getBean("userService", UserService.class);

    @Autowired
    private ContactDAO contactDAO;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private ContactPointDAO contactPointDAO;

    @Autowired
    private UserService userService;


    public ContactDAO getContactDAO() {
        return this.contactDAO;
    }

    public void setContactDAO(ContactDAO contactDAO) {
        super.set(this.locale, new ContactEntity(), contactDAO, ContactDTO.class);
        this.contactDAO = contactDAO;
    }

    public UserService getUserService() {
        return this.userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public UserDAO getUserDAO() {
        return this.userDAO;
    }

    public void setUserDAO(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @Override
    public List<ContactDTO> getList(ContactDTO contactDTO) {
        return this.contactDAO.getList(contactDTO);
    }

    public ContactPointDAO getContactPointDAO() {
        return this.contactPointDAO;
    }

    public void setContactPointDAO(ContactPointDAO contactPointDAO) {
        this.contactPointDAO = contactPointDAO;
    }

    @Override
    public ResultInsertContactDTO saveContact(ContactDTO contactDTO, String action) {
        ResultInsertContactDTO resultInsertContactDTO=new ResultInsertContactDTO();
        String message = ParamUtils.SUCCESS;
        resultInsertContactDTO.setMessage(message);

        ContactEntity contactEntity = null;
        UserEntity userEntity = null;
        UserDTO userDTO = new UserDTO();
        //Check mail is exist
        List<ConditionBean> list = new ArrayList<>();
        list.add(
            new ConditionBean("email", " = ", contactDTO.getEmail(), ParamUtils.TYPE_STRING));
        List<ContactDTO> lst = find("ContactEntity", list);
        if (lst != null && !lst.isEmpty()) {
            if (lst.get(0).getId().equals(contactDTO.getId())) {

            }
        }
        //Check userName is exist
        switch (action) {
            case ParamUtils.ACTION_CREATE:
                contactEntity = new ContactEntity();
                userEntity = new UserEntity();
                userDTO.setStatus(ParamUtils.USER_STATUS.INACTIVE);
                if (this.userDAO.checkExistUserName(contactDTO.getUserName())) {

                   resultInsertContactDTO.setMessage(LanguageBundleUtils.getString("contact.message.is.exist.userName"));
                    return resultInsertContactDTO;
                }
                if (lst != null && !lst.isEmpty()) {

                    resultInsertContactDTO.setMessage(LanguageBundleUtils.getString("contact.message.is.exist.email"));
                    return resultInsertContactDTO;
                }
                break;
            case ParamUtils.ACTION_UPDATE:
                contactEntity = this.contactDAO.findById(contactDTO.getId());
                userEntity = this.userDAO
                    .fillByPersonInfoId(contactDTO.getId(), ParamUtils.USER_TYPE.CONTACT);
                userDTO.setStatus(contactDTO.getContactStatus());
                if (lst != null && !lst.isEmpty()) {
                    if (!lst.get(0).getId().equals(contactDTO.getId())) {

                        resultInsertContactDTO.setMessage(LanguageBundleUtils.getString("contact.message.is.exist.email"));
                        return resultInsertContactDTO;

                    }
                }
                break;
        }
        //Create object Contact
        contactEntity = ContactEntity.setEntity(contactDTO, contactEntity);
        //Save Contact
        //code by luong
        resultInsertContactDTO.setIdContact(this.contactDAO.saveOrUpdate(contactEntity).getId());
        //Create object User
        userDTO.setFullName(contactDTO.getName());
        userDTO.setUserName(contactDTO.getUserName());
        userDTO.setEmail(contactEntity.getEmail());
        userDTO.setMobileNumber(contactDTO.getMobilePhoneNumber());
        userDTO.setPersonInfoId(contactEntity.getId());
        userDTO.setPersonType(ParamUtils.USER_TYPE.CONTACT);
        userEntity = UserEntity.setEntity(userDTO, userEntity);
        //Save User
        this.userDAO.saveOrUpdate(userEntity);
        //Send mail and notification
        SendMailUtils.sendMailAndNotification(null, null, null, null);
        return resultInsertContactDTO;
    }




    @Override
    public String deleteContact(List<ContactDTO> lstDeleteDTO) {
        String message = ParamUtils.SUCCESS;
        int total = lstDeleteDTO.size();
        int ok = 0;
        for (ContactDTO contactDTO : lstDeleteDTO) {
            long contactId = contactDTO.getId();
            ContactEntity contactEntity = this.contactDAO.findById(contactId);
            UserEntity userEntity = this.userDAO
                .fillByPersonInfoId(contactId, ParamUtils.USER_TYPE.CONTACT);
            //Check userName is exist data reference
            List lstContactPoint = this.contactPointDAO.getListByContactId(contactId);
            if (lstContactPoint != null && lstContactPoint.size() > 0) {
                break;
            }
            //Delete object Contact
            if (userEntity != null) {
                this.userDAO.delete(userEntity);
            }
            this.contactDAO.delete(contactEntity);
            //Send mail  and notification
            SendMailUtils.sendMailAndNotification(null, null, null, null);
            ok++;
        }
        if (ok == total) {
            message = LanguageBundleUtils
                .getMessage(LanguageBundleUtils.getString("global.message.delete.multi.successful"),
                    String.valueOf(ok),
                    String.valueOf(total),
                    String.valueOf(total - ok));
        }
        return message;
    }

    @Override
    public void deleteContactByPartnerId(List<Long> lstPartnerId) {
        for (int i = 0, total = lstPartnerId.size(); i < total; i++) {
            Long partnerId = lstPartnerId.get(i);
            this.contactDAO.deleteContactByPartnerId(partnerId);
            //Remove user of contact
            UserEntity userEntity = this.userDAO
                .fillByPersonInfoId(partnerId, ParamUtils.USER_TYPE.CONTACT);
            if (userEntity != null) {
                this.userDAO.delete(userEntity);
            }
        }
    }

    @Override
    public String updatePassword(String username, String newPassword, String oldPassword) {
        return this.userService.updatePasswordUser(username, newPassword, oldPassword);
    }

    public String formatDateTime(Date date) {
        return StringUtils.formatDateTime(date);
    }

    //code By Kien
    @Override
    public List<ContactDTO> getListByID(PartnerDTO partnerDTO) {
        return this.contactDAO.getListByID(partnerDTO);
    }

    @Override
    public ContactDTO getObjectDTOById(Long contactId) {
        List<ContactDTO> lst = find("ContactEntity",
            new ConditionBean("id", "=", StringUtils.getString(contactId),
                ParamUtils.DataType.LONG));
        return lst != null && lst.size() > 0 ? lst.get(0) : null;
    }

    @Override
    public List<ContactPointDTO> checkList(ContactPointDTO pointDTO) {
        return this.contactDAO.checkList(pointDTO);
    }
}
