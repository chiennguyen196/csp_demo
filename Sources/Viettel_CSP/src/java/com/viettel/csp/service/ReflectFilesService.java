/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.service;

import com.viettel.csp.DTO.ReflectFilesDTO;
import com.viettel.csp.entity.ReflectEntity;
import java.util.List;

/**
 *
 * @author admin
 */
public interface ReflectFilesService {

    public List<ReflectFilesDTO> findAll();

    public List<ReflectFilesDTO> getList(ReflectFilesDTO reflectFilesDTO);

    public String insert(ReflectFilesDTO reflectFilesDTO) throws Exception;
}
