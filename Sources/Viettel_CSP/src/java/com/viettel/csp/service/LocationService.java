/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.service;

import com.viettel.csp.DTO.LocationDTO;
import com.viettel.csp.entity.LocationEntity;
import com.viettel.csp.util.KeyValueBean;
import java.util.List;

/**
 *
 * @author GEM
 */
public interface LocationService extends BaseCustomService<LocationEntity, LocationDTO> {

    public List<LocationDTO> getListProvince() throws Exception;

    public List<KeyValueBean> getListDataToCombobox(String level, String codeParent) throws Exception;
}
