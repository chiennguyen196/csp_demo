/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.service.impl;

import com.viettel.csp.DAO.ContactPointDAO;
import com.viettel.csp.DTO.ContactPointDTO;
import com.viettel.csp.entity.ContactPointEntity;
import com.viettel.csp.service.ContactPointService;
import com.viettel.csp.util.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author admin
 */
public class ContactPointServiceImpl extends BaseCustomServiceImpl<ContactPointDAO, ContactPointEntity, ContactPointDTO> implements ContactPointService
{

    @Autowired
    private ContactPointDAO contactPointDAO;

    public ContactPointDAO getContactPointDAO()
    {
        return contactPointDAO;
    }

    public void setContactPointDAO(ContactPointDAO contactPointDAO)
    {
        super.set(locale, new ContactPointEntity(), contactPointDAO, ContactPointDTO.class);
        this.contactPointDAO = contactPointDAO;
    }

    @Override
    public List<ContactPointDTO> getList(Long contractID)
    {
        return contactPointDAO.getList(contractID);
    }

    @Override
    public String saveOrUpdate(ContactPointDTO contactPointDTO)
    {
        String message = ParamUtils.SUCCESS;
        ContactPointEntity contactPointEntity = null;
        try
        {
            if (contactPointDTO.getId() != null)
            {
                List<ContactPointEntity> lst = findEntity("ContactPointEntity", new ConditionBean("id", " = ", StringUtils.getString(contactPointDTO.getId()), ParamUtils.DataType.LONG));
                contactPointEntity = lst.get(0);
            }
            contactPointEntity = ContactPointEntity.setEntity(contactPointDTO, contactPointEntity);
            //Save contractPoint
//            for (int i = 0; i < listContactPointDTO.size(); i++) {
//                ModelMapperUtil mapperUtil = new ModelMapperUtil();
//                ContactPointEntity contactPointEntity1 = mapperUtil.mapper(listContactPointDTO.get(i), ContactPointEntity.class);
//                contactPointDAO.saveOrUpdate(partnerBankEntity);
//            }
            contactPointDAO.saveOrUpdate(contactPointEntity);
            SendMailUtils.sendMailAndNotification(null, null, null, null);
        }
        catch (Exception e)
        {
            logger.error(e.getMessage(), e);
            message = LanguageBundleUtils.getString("global.message.error");
        }
        return message;
    }

    @Override
    public void update(List<ContactPointDTO> listContactPointDTO)
    {
        for (int i = 0; i < listContactPointDTO.size(); i++)
        {
            ModelMapperUtil mapperUtil = new ModelMapperUtil();
            ContactPointEntity contactPointEntity = mapperUtil.mapper(listContactPointDTO.get(i), ContactPointEntity.class);
            contactPointDAO.saveOrUpdate(contactPointEntity);
        }
    }
}
