/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.service;

import com.viettel.csp.DTO.TasksDTO;
import com.viettel.csp.entity.TasksEntity;

import java.util.List;

/**
 * @author admin
 */
public interface TasksService {
    List<TasksEntity> findAlls(TasksDTO tasksDTO) throws Exception;

    List<TasksDTO> findAllToDTO(TasksDTO tasksDTO) throws Exception;

    String updateStatus(TasksDTO tasksDTO) throws Exception;

    String updateDoneTask(TasksDTO tasksDTO) throws Exception;

    TasksEntity insert(TasksEntity tBO) throws Exception;

    public String delete(TasksEntity tasksEntity) throws Exception;
}
