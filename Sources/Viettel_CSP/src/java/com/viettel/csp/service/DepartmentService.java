/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.service;

import com.viettel.csp.DTO.DepartmentDTO;
import com.viettel.csp.entity.DepartmentEntity;
import java.util.List;

/**
 *
 * @author admin
 */
public interface DepartmentService {

    public DepartmentDTO findParent(Long childId) throws Exception;

    public List<DepartmentDTO> findChild(Long parentId) throws Exception;

    public List<DepartmentEntity> getAlls() throws Exception;
    
    public List<DepartmentDTO> getAllsWithOrgnization(Long orgId) throws Exception;
}
