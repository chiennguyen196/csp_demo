/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.service.impl;

import com.viettel.csp.DAO.ContractDAO;
import com.viettel.csp.DAO.ContractFilesDAO;
import com.viettel.csp.DTO.ContractDTO;
import com.viettel.csp.DTO.ContractFilesDTO;
import com.viettel.csp.entity.ContractFilesEntity;
import com.viettel.csp.service.ContractFilesService;
import com.viettel.csp.util.ConditionBean;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.util.SendMailUtils;
import org.hibernate.HibernateException;
//import org.openxmlformats.schemas.wordprocessingml.x2006.main.STBorder;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ContractFilesServiceImpl extends BaseCustomServiceImpl<ContractFilesDAO, ContractFilesEntity, ContractFilesDTO> implements ContractFilesService {

    @Autowired
    private ContractFilesDAO contractFilesDAO;

    @Autowired
    private ContractDAO contractDAO;

    public ContractFilesDAO getContractFilesDAO() {
        return contractFilesDAO;
    }

    public void setContractFilesDAO(ContractFilesDAO contractFilesDAO) {
        this.contractFilesDAO = contractFilesDAO;
    }

    @Override
    public List<ContractFilesDTO> getListContractFiles(ContractFilesDTO contractFilesDTO) throws Exception {
        return contractFilesDAO.getListContractFiles(contractFilesDTO);
    }

    @Override
    public String insert(ContractDTO contractDTO, ContractFilesDTO contractFilesDto) throws Exception {
        String message = ParamUtils.SUCCESS;
        ContractFilesEntity contractFilesEntity = null;
        try {
            contractFilesEntity = new ContractFilesEntity();
            //Create object Contrract
            contractFilesEntity = ContractFilesEntity.setEntity(contractDTO, contractFilesDto, contractFilesEntity);
            contractFilesDAO.insert(contractFilesEntity);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            message = LanguageBundleUtils.getString("global.message.error");
        }
        return message;
    }

    @Override
    public List<ContractFilesDTO> getListMenuContractFiles(ContractFilesDTO contractFilesDTO) {
        return contractFilesDAO.getListMenuContractFiles(contractFilesDTO);
    }

    @Override
    public String insertFiles(ContractFilesDTO contractFilesDTO) {
        String message = ParamUtils.SUCCESS;
        ContractFilesEntity contractFilesEntity = null;
        try {
            contractFilesEntity = new ContractFilesEntity();
            //Create object Contrract
            contractFilesEntity = contractFilesEntity.setEntity(contractFilesDTO, contractFilesEntity);
            contractFilesDAO.insert(contractFilesEntity);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            message = LanguageBundleUtils.getString("global.message.error");
        }
        return message;
    }

    @Override
    public String insertFilesContactMenu(ContractFilesDTO contractFilesDTO, String flat) {
        String message = ParamUtils.SUCCESS;
        ContractFilesEntity contractFilesEntity = null;
        try {
            contractFilesEntity = new ContractFilesEntity();
            //Create object Contrract
            contractFilesEntity = contractFilesEntity.setEntityContactMenu(contractFilesDTO, contractFilesEntity, flat);
            contractFilesDAO.insert(contractFilesEntity);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            message = LanguageBundleUtils.getString("global.message.error");
        }
        return message;
    }

    @Override
    public List<ContractFilesDTO> getMenuContractFile(ContractFilesDTO contractFilesDTO) throws HibernateException {
        return contractFilesDAO.getMenuContractFile(contractFilesDTO);
    }

    @Override
    public String deleteContractFiles(List<ContractFilesDTO> lstContractFilesDTO) {
        String message = ParamUtils.SUCCESS;
        try {
            int total = lstContractFilesDTO.size();
            int ok = 0;
            for (ContractFilesDTO contractFilesDTO : lstContractFilesDTO) {
                Long contractFilesId = contractFilesDTO.getId();
                ContractFilesEntity contractFilesEntity = new ContractFilesEntity();
                contractFilesEntity.setId(contractFilesId);
                contractFilesDAO.delete(contractFilesEntity);
                //Send mail  and notification
                SendMailUtils.sendMailAndNotification(null, null, null, null);
                ok++;
            }
            if (ok == total) {
                message = LanguageBundleUtils.getMessage(LanguageBundleUtils.getString("global.message.delete.multi.successful"),
                        String.valueOf(ok),
                        String.valueOf(total),
                        String.valueOf(total - ok));
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            message = LanguageBundleUtils.getString("global.message.error");
        }
        return message;
    }
    public void updateFilesNotUsing(ContractDTO oldDTO, String contractType){
        List<ConditionBean> beanList = new ArrayList<>();
        beanList.add(new ConditionBean("fileType", "=", contractType, ParamUtils.DataType.STRING));
        beanList.add(new ConditionBean("contractId", "=", String.valueOf(oldDTO.getContractId()), ParamUtils.DataType.LONG));
        List<ContractFilesEntity> filesEntityList = contractFilesDAO.find("ContractFilesEntity", beanList);
        for (ContractFilesEntity item: filesEntityList){
            item.setFileStatus(ParamUtils.FILE_STATUS.DELETE);
            contractFilesDAO.update(item);
        }
    }

    @Override
    public List<ContractFilesDTO> getListMenuContractFilesLiquidation(ContractFilesDTO contractFilesDTO) {
        return contractFilesDAO.getListMenuContractFilesLiquidation(contractFilesDTO);
    }
}
