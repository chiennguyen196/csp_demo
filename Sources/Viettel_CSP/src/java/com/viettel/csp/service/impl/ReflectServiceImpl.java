/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.service.impl;

import com.viettel.csp.DAO.ReflectDAO;
import com.viettel.csp.DAO.ReflectTypeDAO;
import com.viettel.csp.DTO.ReflectDTO;
import com.viettel.csp.DTO.ReflectFilesDTO;
import com.viettel.csp.entity.ReflectEntity;
import com.viettel.csp.entity.ReflectTypeEntity;
import com.viettel.csp.service.ReflectFilesService;
import com.viettel.csp.service.ReflectService;
import com.viettel.csp.service.SolutionService;
import com.viettel.csp.util.ConditionBean;
import com.viettel.csp.util.KeyValueBean;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.util.SendMailUtils;
import com.viettel.csp.util.UserTokenUtils;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author GEM
 */
public class ReflectServiceImpl extends BaseCustomServiceImpl<ReflectDAO, ReflectEntity, ReflectDTO> implements ReflectService {

    @Autowired
    private ReflectDAO reflectDAO;

    @Autowired
    private SolutionService solutionService;

    @Autowired
    private ReflectTypeDAO reflectTypeDAO;

    @Autowired
    private ReflectFilesService reflectFilesService;

    public ReflectFilesService getReflectFilesService() {
        return reflectFilesService;
    }

    public void setReflectFilesService(ReflectFilesService reflectFilesService) {
        this.reflectFilesService = reflectFilesService;
    }

    public ReflectDAO getReflectDAO() {
        return reflectDAO;
    }

    public void setReflectDAO(ReflectDAO reflectDAO) {
        this.reflectDAO = reflectDAO;
        super.set(locale, new ReflectEntity(), reflectDAO, ReflectDTO.class);
    }

    public ReflectTypeDAO getReflectTypeDAO() {
        return reflectTypeDAO;
    }

    public void setReflectTypeDAO(ReflectTypeDAO reflectTypeDAO) {
        this.reflectTypeDAO = reflectTypeDAO;
    }

    public SolutionService getSolutionService() {
        return solutionService;
    }

    public void setSolutionService(SolutionService solutionService) {
        this.solutionService = solutionService;
    }

    @Override
    public List<KeyValueBean> getListReflectType() throws Exception {
        List<ReflectTypeEntity> list = reflectTypeDAO.findAll();
        List<KeyValueBean> keyValueBeans = new ArrayList<KeyValueBean>();
        keyValueBeans.add(new KeyValueBean(ParamUtils.SELECT_NOTHING_VALUE, LanguageBundleUtils.getString("global.combobox.choose")));
        for (ReflectTypeEntity item : list) {
            KeyValueBean keyValueBean = new KeyValueBean(item.getCode(), item.getName());
            keyValueBeans.add(keyValueBean);
        }
        return keyValueBeans;
    }

    @Override
    public List<ReflectDTO> getListReflect(ReflectDTO reflectDTO) throws Exception {
        return reflectDAO.getListReflect(reflectDTO);
    }

    @Override
    public ReflectTypeEntity getListReflectTypeByField(String colume, String value) throws Exception {
        return reflectTypeDAO.getReflectTypeByField(colume, value);
    }

    @Override
    public String insert(ReflectDTO reflectDTO) throws Exception {
        String message = ParamUtils.SUCCESS;
        ReflectEntity reflectEntity = null;
            try {
            reflectEntity = new ReflectEntity();
            //Create object Contrract
            reflectEntity = ReflectEntity.setEntityInsert(reflectDTO, reflectEntity);
            //Save Contract
            reflectEntity = reflectDAO.insert(reflectEntity);
            int ab=reflectDTO.getLstReflectFilesDTO().size();

            Long id = reflectEntity.getId();
            for (ReflectFilesDTO reflectFile : reflectDTO.getLstReflectFilesDTO()) {
                reflectFile.setReflectId(id);
                reflectFilesService.insert(reflectFile);
            }
            //Send mail and notification
            SendMailUtils.sendMailAndNotification(null, null, null, null);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            message = LanguageBundleUtils.getString("global.message.error");
        }
        return message;
    }

    @Override
    public String update(ReflectDTO reflectDTO) throws Exception {
        String message = ParamUtils.SUCCESS;
        ReflectEntity reflectEntity = null;
        try {
            //Create object Contrract
            reflectEntity = findOnlyEntity("ReflectEntity", new ConditionBean("id", "=", String.valueOf(reflectDTO.getId()), ParamUtils.DataType.LONG));
            reflectEntity = ReflectEntity.setEntityUpdate(reflectDTO, reflectEntity);
            //Save Contract
            reflectDAO.update(reflectEntity);
            //Send mail and notification
            SendMailUtils.sendMailAndNotification(null, null, null, null);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            message = LanguageBundleUtils.getString("global.message.error");
        }
        return message;
    }

    @Override
    public long checkCode(String code) {
        return reflectDAO.checkCode(code);
    }

    @Override
    public String deleteReflect(List<ReflectDTO> lstDeleteDTO) {
        String message = ParamUtils.SUCCESS;
        try {
            int total = lstDeleteDTO.size();
            int ok = 0;
            for (ReflectDTO reflectDTO : lstDeleteDTO) {
                Long reflectId = reflectDTO.getId();
                ReflectEntity reflectEntity = reflectDAO.findById(reflectId);
                reflectEntity.setId(reflectDTO.getId());
                reflectDAO.delete(reflectEntity);
                //Send mail  and notification
                SendMailUtils.sendMailAndNotification(null, null, null, null);
                ok++;
            }
            if (ok == total) {
                message = LanguageBundleUtils.getMessage(LanguageBundleUtils.getString("global.message.delete.multi.successful"),
                        String.valueOf(ok),
                        String.valueOf(total),
                        String.valueOf(total - ok));
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            message = LanguageBundleUtils.getString("global.message.error");
        }
        return message;
    }

    @Override
    public String sendPartner(ReflectDTO reflectDTO) throws Exception {
        String message = ParamUtils.SUCCESS;
        ReflectEntity reflectEntity = null;
        try {
            //Create object Contrract
            reflectEntity = findOnlyEntity("ReflectEntity", new ConditionBean("id", "=", String.valueOf(reflectDTO.getId()), ParamUtils.DataType.LONG));
            reflectEntity = ReflectEntity.setEntityUpdate(reflectDTO, reflectEntity);
            reflectEntity.setReflectStatus(ParamUtils.REFLECT_STATUS.SEND);
            reflectEntity = reflectDAO.saveOrUpdate(reflectEntity);
            //Send mail and notification
            SendMailUtils.sendMailAndNotification(null, null, null, null);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            message = LanguageBundleUtils.getString("global.message.error");
        }
        return message;
    }

    @Override
    public ReflectDTO getObjectDTOById(Long reflectId) {
        List<ConditionBean> list = new ArrayList<>();
        list.add(new ConditionBean("id", " = ", String.valueOf(reflectId), ParamUtils.DataType.LONG));
        List<ReflectDTO> lst = find("ReflectEntity", list);
        return lst != null && lst.size() > 0 ? lst.get(0) : null;
    }

    @Override
    public ReflectEntity getById(Long reflectId) {
        List<ConditionBean> list = new ArrayList<>();
        list.add(new ConditionBean("id", " = ", String.valueOf(reflectId), ParamUtils.DataType.LONG));
        List<ReflectEntity> lst = findEntity("ReflectEntity", list);
        return lst != null && lst.size() > 0 ? lst.get(0) : null;
    }

    @Override
    public String saveChangeStatus(ReflectDTO reflectDTO) {
        String message = ParamUtils.SUCCESS;
        try {
            List<ReflectEntity> lst = findEntity("ReflectEntity", new ConditionBean("id", " = ", String.valueOf(reflectDTO.getId()), ParamUtils.DataType.LONG));
            if (lst != null && lst.size() > 0) {
                ReflectEntity reflectEntity = lst.get(0);
                switch (reflectDTO.getReflectStatusCode()) {
                    case ParamUtils.REFLECT_STATUS.SEND:
                        reflectEntity.setReflectStatus(reflectDTO.getReflectStatusCode());
                        break;
                    case ParamUtils.REFLECT_STATUS.NEW:
                        reflectEntity.setReflectStatus(reflectDTO.getReflectStatusCode());
                        break;
                    case ParamUtils.REFLECT_STATUS.REPONSE_CUSTOMER:
                        reflectEntity.setReflectStatus(reflectDTO.getReflectStatusCode());
                        break;
                    case ParamUtils.REFLECT_STATUS.CLOSE:
                        reflectEntity.setReasonCode(reflectDTO.getReasonCode());
                        reflectEntity.setReasonDescription(reflectDTO.getReasonDescription());
                        reflectEntity.setReflectStatus(reflectDTO.getReflectStatusCode());
                        reflectEntity.setCloseDate(new Date());
                        break;
                }
                reflectEntity.setUpdateAt(new Date());
                reflectEntity.setUpdateBy(UserTokenUtils.getUserId());
                reflectDAO.saveOrUpdate(reflectEntity);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            message = LanguageBundleUtils.getString("global.message.error");
        }
        return message;
    }

}
