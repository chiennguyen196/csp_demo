/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.service.impl;

import com.viettel.csp.DAO.PartnerLockDAO;
import com.viettel.csp.DTO.PartnerLockDTO;
import com.viettel.csp.DTO.UserDTO;
import com.viettel.csp.entity.PartnerEntity;
import com.viettel.csp.entity.PartnerLockEntity;
import com.viettel.csp.entity.UserEntity;
import com.viettel.csp.service.PartnerLockService;
import com.viettel.csp.service.PartnerService;
import com.viettel.csp.util.ConditionBean;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.util.SendMailUtils;
import com.viettel.csp.util.StringUtils;
import com.viettel.csp.util.UserTokenUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author GEM
 */
public class PartnerLockServiceImpl extends BaseCustomServiceImpl<PartnerLockDAO, PartnerLockEntity, PartnerLockDTO> implements PartnerLockService {

    @Autowired
    private PartnerLockDAO partnerLockDAO;

    public PartnerLockDAO getPartnerLockDAO() {
        return partnerLockDAO;
    }

    public void setPartnerLockDAO(PartnerLockDAO partnerLockDAO) {
        this.partnerLockDAO = partnerLockDAO;
        this.tDAO = partnerLockDAO;
        this.classTDto = PartnerLockDTO.class;
    }

    @Autowired
    private PartnerService partnerService;

    public PartnerService getPartnerService() {
        return partnerService;
    }

    public void setPartnerService(PartnerService partnerService) {
        this.partnerService = partnerService;
    }

    @Override
    public void insert(PartnerLockDTO partnerBank) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void insertList(List<PartnerLockDTO> lstPartnerBank) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deletePartnerId(Long partnerId) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String savePartnerLock(PartnerLockDTO partnerLockDTO, String action) {
        String message = ParamUtils.SUCCESS;
        PartnerEntity partnerEntity = null;
        PartnerLockEntity partnerLockEntity = null;
        UserEntity userEntity = null;
        UserDTO userDTO = new UserDTO();
        try {
            partnerEntity = partnerService.getById(partnerLockDTO.getId());
            switch (action) {
                case ParamUtils.ACTION_PARTNER_LOCK:
                    partnerLockEntity = PartnerLockEntity.setEntity(null, partnerLockDTO);
                    partnerEntity.setIsLock(1L);
                    break;
                case ParamUtils.ACTION_PARTNER_OPEN:
                    partnerLockDTO.setEndDate(new Date());
                    partnerLockEntity = (PartnerLockEntity) partnerLockDAO.find("PartnerLockEntity", new ConditionBean("id", "=", StringUtils.getString(partnerLockDTO.getId()), ParamUtils.ReportDataType.LONG));
                    if (partnerLockEntity == null) {
                        message = LanguageBundleUtils.getString("global.message.error");
                        return message;
                    }
                    partnerLockEntity = PartnerLockEntity.setEntity(partnerLockEntity, partnerLockDTO);
                    partnerEntity.setIsLock(2L);
                    break;
            }
            //Save PartnerLock
            partnerLockDAO.saveOrUpdate(partnerLockEntity);
            //Save Partner
            partnerEntity.setUpdateAt(new Date());
            partnerEntity.setUpdateBy(UserTokenUtils.getUserId());
            partnerService.update(partnerEntity);
            //Send mail and notification
            SendMailUtils.sendMailAndNotification(null, null, null, null);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            message = LanguageBundleUtils.getString("global.message.error");
        }
        return message;
    }

    @Override
    public PartnerLockDTO findByPartnerId(Long partnerId) {
        PartnerLockDTO lockDTO = partnerLockDAO.findByPartnerId(partnerId);
        return lockDTO;
    }

    public void update(PartnerLockDTO partnerLockDTO){
        PartnerLockEntity entity = convertToEntity(partnerLockDTO);
        partnerLockDAO.update(entity);
    }

    private PartnerLockEntity convertToEntity(PartnerLockDTO partnerLockDTO){
        PartnerLockEntity entity = new PartnerLockEntity();
        entity.setPartnerId(partnerLockDTO.getPartnerId());
        entity.setContent(partnerLockDTO.getContent());
        entity.setCreateAt(partnerLockDTO.getCreateAt());
        entity.setCreateBy(partnerLockDTO.getCreateBy());
        entity.setEndDate(partnerLockDTO.getEndDate());
        entity.setReasonCode(partnerLockDTO.getReasonCode());
        entity.setStartDate(partnerLockDTO.getStartDate());
        entity.setUpdateAt(partnerLockDTO.getUpdateAt());
        entity.setId(partnerLockDTO.getId());
        entity.setUpdateBy(partnerLockDTO.getUpdateBy());
        return entity;
    }
}
