package com.viettel.csp.service;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.viettel.csp.DTO.ContractDTO;
import com.viettel.csp.DTO.ContractFilesDTO;
import com.viettel.csp.entity.ContractFilesEntity;
import org.hibernate.HibernateException;

import java.util.List;

public interface ContractFilesService extends BaseCustomService<ContractFilesEntity, ContractFilesDTO> {

    public List<ContractFilesDTO> getListContractFiles(ContractFilesDTO contractFilesDTO) throws Exception;

    public String insert(ContractDTO contractDTO, ContractFilesDTO contractFilesDto) throws Exception;

    public List<ContractFilesDTO> findAll();

    public List<ContractFilesDTO> getListMenuContractFiles(ContractFilesDTO contractFilesDTO);

    public String insertFiles(ContractFilesDTO contractFilesDTO);

    public List<ContractFilesDTO> getMenuContractFile(ContractFilesDTO contractFilesDTO) throws HibernateException;

    public String deleteContractFiles(List<ContractFilesDTO> lstContractFilesDTO);

    public String insertFilesContactMenu(ContractFilesDTO contractFilesDTO, String flat);

    public void updateFilesNotUsing(ContractDTO oldDTO, String contractType);

    public List<ContractFilesDTO> getListMenuContractFilesLiquidation(ContractFilesDTO contractFilesDTO);
}
