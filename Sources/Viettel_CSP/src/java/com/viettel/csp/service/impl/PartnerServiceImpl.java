/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.service.impl;

import com.viettel.csp.DAO.PartnerDAO;
import com.viettel.csp.DAO.PartnerStatusDAO;
import com.viettel.csp.DAO.PartnerTypeDAO;
import com.viettel.csp.DTO.PartnerDTO;
import com.viettel.csp.DTO.UserDTO;
import com.viettel.csp.entity.PartnerEntity;
import com.viettel.csp.entity.PartnerStatusEntity;
import com.viettel.csp.entity.PartnerTypeEntity;
import com.viettel.csp.entity.UserEntity;
import com.viettel.csp.service.PartnerService;
import com.viettel.csp.service.UserService;
import com.viettel.csp.util.ConditionBean;
import com.viettel.csp.util.KeyValueBean;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.util.SendMailUtils;
import com.viettel.csp.util.StringUtils;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author GEM
 */
public class PartnerServiceImpl extends BaseCustomServiceImpl<PartnerDAO, PartnerEntity, PartnerDTO> implements PartnerService {

    @Autowired
    private PartnerDAO partnerDAO;

    @Autowired
    private PartnerTypeDAO partnerTypeDAO;

    @Autowired
    private PartnerStatusDAO partnerStatusDAO;
    
    @Autowired
    private UserService userService;

    public PartnerStatusDAO getPartnerStatusDAO() {
        return partnerStatusDAO;
    }

    public void setPartnerStatusDAO(PartnerStatusDAO partnerStatusDAO) {
        this.partnerStatusDAO = partnerStatusDAO;
        super.set(locale, new PartnerEntity(), partnerDAO, PartnerDTO.class);
    }

    public PartnerDAO getPartnerDAO() {
        return partnerDAO;
    }

    public void setPartnerDAO(PartnerDAO partnerDAO) {
        this.partnerDAO = partnerDAO;
    }

    public PartnerTypeDAO getPartnerTypeDAO() {
        return partnerTypeDAO;
    }

    public void setPartnerTypeDAO(PartnerTypeDAO partnerTypeDAO) {
        this.partnerTypeDAO = partnerTypeDAO;
    }

    @Override
    public List<PartnerDTO> getListPartner(PartnerDTO partnerDTO) throws Exception {
        return partnerDAO.getListPartner(partnerDTO);
    }

    @Override
    public List<KeyValueBean> getListPartnerType() throws Exception {
        List<PartnerTypeEntity> list = partnerTypeDAO.findAll();
        List<KeyValueBean> keyValueBeans = new ArrayList<KeyValueBean>();
        keyValueBeans.add(new KeyValueBean(ParamUtils.SELECT_NOTHING_VALUE, LanguageBundleUtils.getString("global.combobox.choose")));
        for (PartnerTypeEntity item : list) {
            KeyValueBean keyValueBean = new KeyValueBean(item.getCode(), item.getName());
            keyValueBeans.add(keyValueBean);
        }
        return keyValueBeans;
    }

    @Override
    public List<KeyValueBean> getListPartnerStatus() throws Exception {
        List<PartnerStatusEntity> list = partnerStatusDAO.findAll();
        List<KeyValueBean> keyValueBeans = new ArrayList<KeyValueBean>();
        keyValueBeans.add(new KeyValueBean(ParamUtils.SELECT_NOTHING_VALUE, LanguageBundleUtils.getString("global.combobox.choose")));
        for (PartnerStatusEntity item : list) {
            KeyValueBean keyValueBean = new KeyValueBean(item.getCode(), item.getName());
            keyValueBeans.add(keyValueBean);
        }
        return keyValueBeans;
    }

    @Override
    public void update(PartnerDTO partnerDTO) throws Exception {
        partnerDAO.update(partnerDTO);
    }

    @Override
    public void updateProfile(PartnerDTO partnerDTO) throws Exception {
        partnerDTO.setPartnerStatusCode(ParamUtils.PARTNER_STATUS.REQUEST_VIETTEL_APPROVE);
        partnerDAO.update(partnerDTO);
    }

    @Override
    public List<KeyValueBean> getListPartnerTypeByCode(String partnerTypeCode) throws Exception {
        List<KeyValueBean> listPartnerType = partnerTypeDAO.getListPartnerTypeByCode(partnerTypeCode);
        return listPartnerType;
    }

    @Override
    public List<KeyValueBean> getListPartnerTypeByName(String partnerTypeName) throws Exception {
        List<KeyValueBean> listPartnerType = partnerTypeDAO.getListPartnerTypeByName(partnerTypeName);
        return listPartnerType;
    }

    @Override
    public void deletes(List<Long> lstId) throws Exception {
        for (int i = 0; i < lstId.size(); i++) {
            partnerDAO.deletes(lstId.get(i));
        }

    }

    @Override
    public boolean checkValidateDelete(Long id) throws Exception {
        return (partnerDAO.checkExitsTblContract(id) && partnerDAO.checkExitsTblContract(id)) ? true : false;
    }

    @Override
    public String update(PartnerDTO partnerDTO, String action) throws Exception {
        String message = ParamUtils.SUCCESS;
        PartnerEntity partnerEntity = null;
        try {
            switch (action) {
                case ParamUtils.ACTION_CREATE:
                    partnerEntity = new PartnerEntity();
                    break;
                case ParamUtils.ACTION_UPDATE:
                    partnerEntity = partnerDAO.findById(partnerDTO.getId());
                    break;
            }
            //Create object Contrract
            partnerEntity = PartnerEntity.setEntity(partnerDTO, partnerEntity);
            //Save Partner
            partnerDAO.saveOrUpdate(partnerEntity);
            //Send mail and notification
            SendMailUtils.sendMailAndNotification(null, null, null, null);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            message = LanguageBundleUtils.getString("global.message.error");
        }
        return message;
    }

    @Override
    public PartnerDTO findInfoById(PartnerDTO partnerDTO) throws Exception {
        List<PartnerDTO> listPartnerDTO = getListPartner(partnerDTO);
        return (listPartnerDTO != null && listPartnerDTO.size() > 0) ? listPartnerDTO.get(0) : null;
    }

    @Override
    public String deletePartner(List<PartnerDTO> lstDeleteDTO) {
        String message = ParamUtils.SUCCESS;
        try {
            int total = lstDeleteDTO.size();
            int ok = 0;
            for (PartnerDTO partnerDTO : lstDeleteDTO) {
                Long contractId = partnerDTO.getId();
                PartnerEntity partnerEntity = partnerDAO.findById(contractId);
                partnerDAO.delete(partnerEntity);
                //Send mail  and notification
                SendMailUtils.sendMailAndNotification(null, null, null, null);
                ok++;
            }
            if (ok == total) {
                message = LanguageBundleUtils.getMessage(LanguageBundleUtils.getString("global.message.delete.multi.successful"),
                        String.valueOf(ok),
                        String.valueOf(total),
                        String.valueOf(total - ok));
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            message = LanguageBundleUtils.getString("global.message.error");
        }
        return message;
    }

    @Override
    public PartnerEntity getById(Long partnerId) throws Exception {
        List<PartnerEntity> lst = partnerDAO.find("PartnerEntity", new ConditionBean("id", "=", StringUtils.getString(partnerId), ParamUtils.DataType.LONG));
        return lst != null && lst.size() > 0 ? lst.get(0) : null;
    }

    @Override
    public  List<PartnerDTO> getListPartnerActive() throws Exception {
        StringBuilder partnerStatus = new StringBuilder();
        partnerStatus.append("('");
        partnerStatus.append(ParamUtils.PARTNER_STATUS.ACTIVE);
        partnerStatus.append("','");
        partnerStatus.append(ParamUtils.PARTNER_STATUS.LOCK);
        partnerStatus.append("')");
        return find("PartnerEntity", new ConditionBean("PARTNER_STATUS", ParamUtils.OP_IN, partnerStatus.toString(), null));
    }

    @Override
    public PartnerDTO getObjectDTOById(Long partnerId) throws Exception {
        List<PartnerDTO> lst = find("PartnerEntity", new ConditionBean("id", "=", StringUtils.getString(partnerId), ParamUtils.DataType.LONG));
        return lst != null && lst.size() > 0 ? lst.get(0) : null;
    }

    public String insert(PartnerDTO partnerDTO) throws Exception {
        String message = ParamUtils.SUCCESS;
        PartnerEntity partnerEntity = null;
        try {
            partnerEntity = new PartnerEntity();
            //Create object Contrract
            partnerEntity = PartnerEntity.setEntity(partnerDTO, partnerEntity);
            //Save Contract
         partnerDAO.insert(partnerEntity);
            //Send mail and notification
            SendMailUtils.sendMailAndNotification(null, null, null, null);       
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            message = LanguageBundleUtils.getString("global.message.error");
        }
        return message;
    }

    @Override
    public List<PartnerEntity> getListPartnerContract(Long partnerId) throws Exception {
        return partnerDAO.getListPartnerContract(partnerId);
    }
}
