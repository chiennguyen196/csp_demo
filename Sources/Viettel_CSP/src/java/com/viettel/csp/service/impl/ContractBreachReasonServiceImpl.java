package com.viettel.csp.service.impl;

import com.viettel.csp.DAO.ContractBreachReasonDAO;
import com.viettel.csp.DTO.ContractBreachReasonDTO;
import com.viettel.csp.entity.ContractBreachReasonEntity;
import com.viettel.csp.service.ContractBreachReasonService;
import com.viettel.csp.service.impl.BaseCustomServiceImpl;
import com.viettel.csp.util.ConditionBean;
import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by GEM on 6/16/2017.
 */
public class ContractBreachReasonServiceImpl extends BaseCustomServiceImpl<ContractBreachReasonDAO, ContractBreachReasonEntity, ContractBreachReasonDTO> implements ContractBreachReasonService {

    @Autowired
    ContractBreachReasonDAO contractBreachReasonDAO;

    public ContractBreachReasonDAO getContractBreachReasonDAO() {
        return contractBreachReasonDAO;
    }

    public void setContractBreachReasonDAO(ContractBreachReasonDAO contractBreachReasonDAO) {
        super.set(this.locale, new ContractBreachReasonEntity(), contractBreachReasonDAO, ContractBreachReasonDTO.class);
        this.contractBreachReasonDAO = contractBreachReasonDAO;
    }

    public void insertList(List<ContractBreachReasonDTO> listReason ){
        if (listReason == null || listReason.size() == 0)
            return;

        for (ContractBreachReasonDTO dto:listReason){
            insert(dto);
        }
    }

    public void insert(ContractBreachReasonDTO dto){
        ContractBreachReasonEntity entity = converToEntity(dto);
        contractBreachReasonDAO.insert(entity);
    }

    private ContractBreachReasonEntity converToEntity(ContractBreachReasonDTO dto){
        ContractBreachReasonEntity entity = new ContractBreachReasonEntity();
        entity.setOther(dto.getOther());
        entity.setReasonCode(dto.getReasonCode());
        entity.setContractBreachId(dto.getContractBreachId());

        return entity;
    }

    public List<ContractBreachReasonDTO> getByContractBreachId(long id){
        List<ContractBreachReasonDTO> lst = find("ContractBreachReasonEntity", new ConditionBean("CONTRACT_BREACH_ID", "=", StringUtils.getString(id), ParamUtils.DataType.LONG));
        return lst;
    }
}
