package com.viettel.csp.service;

import com.viettel.csp.DTO.TaskFilesDTO;
import com.viettel.csp.entity.TaskFilesEntity;

import java.util.List;

public interface TaskFilesSevice {
    List<TaskFilesEntity> findAlles(Long taskId) throws Exception;
    String insert(TaskFilesDTO taskFilesDTO) throws Exception;
}
