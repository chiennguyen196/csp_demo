/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.service.impl;

import com.viettel.csp.DAO.ContractStatusDAO;
import com.viettel.csp.DTO.ContractStatusDTO;
import com.viettel.csp.entity.ContractStatusEntity;
import com.viettel.csp.service.ContractStatusService;
import com.viettel.csp.util.ConditionBean;
import com.viettel.csp.util.ParamUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Locale;

/**
 * @author GEM
 */
public class ContractStatusServiceImpl extends BaseCustomServiceImpl<ContractStatusDAO, ContractStatusEntity, ContractStatusDTO> implements ContractStatusService {

    @Autowired
    private ContractStatusDAO contactStatusDAO;

    public ContractStatusDAO getContractStatusDAO() {
        return contactStatusDAO;
    }

    public void setContractStatusDAO(ContractStatusDAO contactStatusDAO) {
        this.contactStatusDAO = contactStatusDAO;
        set(locale, new ContractStatusEntity(), contactStatusDAO,  ContractStatusDTO.class);
    }

    public ContractStatusDTO getObjectDTOByCode(String paramType) throws Exception {
        return findOnly("ContractStatusEntity", new ConditionBean("code", "=", paramType, ParamUtils.DataType.STRING));
    }
}
