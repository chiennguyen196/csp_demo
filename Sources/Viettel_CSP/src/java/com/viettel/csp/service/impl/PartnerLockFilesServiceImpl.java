/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.service.impl;

import com.viettel.csp.DAO.PartnerLockFilesDAO;
import com.viettel.csp.DTO.PartnerLockFilesDTO;
import com.viettel.csp.entity.PartnerLockFilesEntity;
import com.viettel.csp.service.PartnerLockFilesService;

import java.util.ArrayList;
import java.util.List;

import com.viettel.csp.util.ConditionBean;
import com.viettel.csp.util.ParamUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author GEM
 */
public class PartnerLockFilesServiceImpl extends BaseCustomServiceImpl<PartnerLockFilesDAO, PartnerLockFilesEntity, PartnerLockFilesDTO> implements PartnerLockFilesService{

    @Autowired
    private PartnerLockFilesDAO partnerLockFilesDAO;

    public PartnerLockFilesDAO getPartnerLockFilesDAO() {
        return partnerLockFilesDAO;
    }

    public void setPartnerLockFilesDAO(PartnerLockFilesDAO partnerLockFilesDAO) {
        this.partnerLockFilesDAO = partnerLockFilesDAO;
    }

    public void insert(PartnerLockFilesDTO filesDTO) throws Exception {
        PartnerLockFilesEntity entity = convertDTOtoEntity(filesDTO);
        partnerLockFilesDAO.insert(entity);
    }

    public void insertList(List<PartnerLockFilesDTO> list) throws Exception {
        for (PartnerLockFilesDTO filesDTO:list){
            insert(filesDTO);
        }
    }
//PARTNER_LOCK_ID
    public List<PartnerLockFilesDTO> findByPartnerLockId(Long partnerLockId) throws Exception {
        List<ConditionBean> list = new ArrayList<>();
        list.add(new ConditionBean("PARTNER_LOCK_ID", " = ", String.valueOf(partnerLockId), ParamUtils.TYPE_NUMBER));
        List<PartnerLockFilesEntity> lst = partnerLockFilesDAO.find("PartnerLockFilesEntity", list);

        List<PartnerLockFilesDTO> filesDTOs = new ArrayList<>();
        for(PartnerLockFilesEntity entity:lst){
            PartnerLockFilesDTO dto = new PartnerLockFilesDTO();
            dto = convertEntityToDTO(entity);
            filesDTOs.add(dto);
        }
        return filesDTOs;
    }

    public void deletePartnerLockId(Long partnerLockId) throws Exception {

    }

    private PartnerLockFilesEntity convertDTOtoEntity(PartnerLockFilesDTO filesDTO){
        PartnerLockFilesEntity entity = new PartnerLockFilesEntity();
        entity.setUploadDate(filesDTO.getUploadDate());
        entity.setPathFile(filesDTO.getPathFile());
        entity.setDescription(filesDTO.getDescription());
        entity.setFileName(filesDTO.getFileName());
        entity.setPartnerLockId(filesDTO.getPartnerLockId());
        return entity;
    }

    private PartnerLockFilesDTO convertEntityToDTO(PartnerLockFilesEntity entity){
        PartnerLockFilesDTO dto = new PartnerLockFilesDTO();
        dto.setFileName(entity.getFileName());
        dto.setUploadDate(entity.getUploadDate());
        dto.setPathFile(entity.getPathFile());
        dto.setDescription(entity.getDescription());
        dto.setPartnerLockId(entity.getPartnerLockId());
        return dto;
    }
}
