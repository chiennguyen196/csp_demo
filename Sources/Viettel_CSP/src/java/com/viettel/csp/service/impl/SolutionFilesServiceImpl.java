package com.viettel.csp.service.impl;

import com.viettel.csp.DAO.SolutionDAO;
import com.viettel.csp.DAO.SolutionFilesDAO;
import com.viettel.csp.DTO.SolutionDTO;
import com.viettel.csp.DTO.SolutionFilesDTO;
import com.viettel.csp.entity.SolutionEntity;
import com.viettel.csp.entity.SolutionFilesEntity;
import com.viettel.csp.service.SolutionFilesService;
import com.viettel.csp.service.SolutionService;
import com.viettel.csp.util.ConditionBean;
import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by GEM on 6/9/2017.
 */
public class SolutionFilesServiceImpl extends BaseCustomServiceImpl<SolutionFilesDAO, SolutionFilesEntity, SolutionFilesDTO> implements SolutionFilesService {
    @Autowired
    private SolutionFilesDAO solutionFilesDAO;

    public SolutionFilesDAO getSolutionFilesDAO() {
        return solutionFilesDAO;
    }

    public void setSolutionFilesDAO(SolutionFilesDAO solutionFilesDAO) {
        this.solutionFilesDAO = solutionFilesDAO;
        super.set(locale, new SolutionFilesEntity(), solutionFilesDAO, SolutionFilesDTO.class);
    }

    @Override
    public List<SolutionFilesDTO> getListSolutionFiles(SolutionFilesDTO solutionFilesDTO) throws Exception {
        return find("SolutionFilesEntity", new ConditionBean("solution_id", " = ", StringUtils.getString(solutionFilesDTO.getSolutionId()), ParamUtils.DataType.LONG));
    }
}

