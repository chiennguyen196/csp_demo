package com.viettel.csp.service.impl;

import com.viettel.csp.DAO.TaskFilesDAO;
import com.viettel.csp.DTO.TaskFilesDTO;
import com.viettel.csp.entity.TaskFilesEntity;
import com.viettel.csp.service.TaskFilesSevice;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.util.SendMailUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class TaskFilesSeviceImpl extends BaseCustomServiceImpl<TaskFilesDAO, TaskFilesEntity, TaskFilesDTO> implements TaskFilesSevice {

    @Autowired
    TaskFilesDAO taskFilesDAO;

    @Override
    public List<TaskFilesEntity> findAlles(Long taskId) throws Exception {
        return taskFilesDAO.findAlles(taskId);
    }

    @Override
    public String insert(TaskFilesDTO taskFilesDTO) throws Exception {
        String message = ParamUtils.SUCCESS;
        TaskFilesEntity taskFilesEntity;
        try {
            taskFilesEntity = new TaskFilesEntity();
            taskFilesEntity = TaskFilesEntity.setEntityInsert(taskFilesDTO, taskFilesEntity);
            taskFilesDAO.insert(taskFilesEntity);
            SendMailUtils.sendMailAndNotification(null, null, null, null);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            message = LanguageBundleUtils.getString("global.message.error");
        }
        return message;
    }

    public TaskFilesDAO getTaskFilesDAO() {
        return taskFilesDAO;
    }

    public void setTaskFilesDAO(TaskFilesDAO taskFilesDAO) {
        this.taskFilesDAO = taskFilesDAO;
    }
}
