/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.service;

import com.viettel.csp.entity.DisciplineEntity;
import java.util.List;

/**
 *
 * @author GEM
 */
public interface DisciplineService  {
     public List<DisciplineEntity> findAllDiscipline() throws Exception;
     
}
