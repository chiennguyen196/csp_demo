/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.service.impl;

import com.viettel.csp.DAO.SolutionDAO;
import com.viettel.csp.DAO.SolutionFilesDAO;
import com.viettel.csp.DTO.ReflectDTO;
import com.viettel.csp.DTO.SolutionDTO;
import com.viettel.csp.DTO.SolutionFilesDTO;
import com.viettel.csp.entity.SolutionEntity;
import com.viettel.csp.entity.SolutionFilesEntity;
import com.viettel.csp.service.ReflectService;
import com.viettel.csp.service.SolutionService;
import com.viettel.csp.util.ConditionBean;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.ModelMapperUtil;
import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.util.SendMailUtils;
import com.viettel.csp.util.StringUtils;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author admin
 */
public class SolutionServiceImpl extends BaseCustomServiceImpl<SolutionDAO, SolutionEntity, SolutionDTO> implements SolutionService {

    @Autowired
    private SolutionDAO solutionDAO;

    @Autowired
    private ReflectService reflectService;

    @Autowired
    private SolutionFilesDAO solutionFilesDAO;

    public SolutionFilesDAO getSolutionFilesDAO() {
        return solutionFilesDAO;
    }

    public void setSolutionFilesDAO(SolutionFilesDAO solutionFilesDAO) {
        this.solutionFilesDAO = solutionFilesDAO;
    }

    public SolutionDAO getSolutionDAO() {
        return solutionDAO;
    }

    public void setSolutionDAO(SolutionDAO solutionDAO) {
        super.set(locale, new SolutionEntity(), solutionDAO, SolutionDTO.class);
        this.solutionDAO = solutionDAO;
    }

    public ReflectService getReflectService() {
        return reflectService;
    }

    public void setReflectService(ReflectService reflectService) {
        this.reflectService = reflectService;
    }

//    @Override
//    public String sendPartner(SolutionDTO solutionDTO) throws Exception {
//        String message = ParamUtils.SUCCESS;
//        SolutionEntity solutionEntity = null;
//        try {
//            solutionEntity = new SolutionEntity();
//            solutionEntity = SolutionEntity.setEntity(solutionDTO, solutionEntity, ParamUtils.SOLUTION_STATUS.SEND_PARTNER);
//            //Save Contract
//            solutionDAO.saveOrUpdate(solutionEntity);
//            //Send mail and notification
//            SendMailUtils.sendMailAndNotification(null, null, null, null);
//        } catch (Exception e) {
//            logger.error(e.getMessage(), e);
//            message = LanguageBundleUtils.getString("global.message.error");
//        }
//        return message;
//    }
    @Override
    public String sendSolution(SolutionDTO solutionDTO) throws Exception {
        String message = ParamUtils.SUCCESS;
        SolutionEntity solutionEntity = null;
        try {
            solutionEntity = SolutionEntity.setEntity(solutionDTO, new SolutionEntity(), ParamUtils.SOLUTION_STATUS.REPONSE);
            //Save Solution
            solutionEntity = solutionDAO.saveOrUpdate(solutionEntity);
            //Save SolutionFiles
            for (SolutionFilesDTO solutionFilesDTO : solutionDTO.getLstSolutionFilesDTO()) {
                solutionFilesDTO.setSolutionId(solutionEntity.getId());
                solutionFilesDAO.insert(new ModelMapperUtil().mapper(solutionFilesDTO, SolutionFilesEntity.class));
            }
            //Save Reflect
            ReflectDTO reflectDTO = new ReflectDTO();
            reflectDTO.setId(solutionDTO.getReflectId());
            reflectDTO.setReflectStatusCode(ParamUtils.REFLECT_STATUS.REPONSE_CUSTOMER);
            reflectService.saveChangeStatus(reflectDTO);
            //Send mail and notification
            SendMailUtils.sendMailAndNotification(null, null, null, null);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            message = LanguageBundleUtils.getString("global.message.error");
        }
        return message;
    }

    @Override
    public SolutionDTO getById(Long solutionId) {
        List<SolutionDTO> lst = find("SolutionEntity", new ConditionBean("id", " = ", StringUtils.getString(solutionId), ParamUtils.DataType.LONG));
        return lst != null && lst.size() > 0 ? lst.get(0) : null;
    }

    @Override
    public String sendEvaluatePartner(SolutionDTO solutionDTO, String solutionStatus) {
        String message = ParamUtils.SUCCESS;
        SolutionEntity solutionEntity = null;
        try {
            List<SolutionEntity> lst = findEntity("SolutionEntity", new ConditionBean("id", " = ", StringUtils.getString(solutionDTO.getId()), ParamUtils.DataType.LONG));
            solutionEntity = lst.get(0);
            solutionEntity = SolutionEntity.setEntity(solutionDTO, solutionEntity, solutionStatus);
            //Save Solution
            solutionDAO.saveOrUpdate(solutionEntity);
            //Save Reflect
            ReflectDTO reflectDTO = new ReflectDTO();
            reflectDTO.setId(solutionDTO.getReflectId());
            reflectDTO.setReflectStatusCode(ParamUtils.REFLECT_STATUS.SEND);
            reflectService.saveChangeStatus(reflectDTO);
            //Send mail and notification
            SendMailUtils.sendMailAndNotification(null, null, null, null);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            message = LanguageBundleUtils.getString("global.message.error");
        }
        return message;
    }

    @Override
    public List<SolutionDTO> getListSolution(SolutionDTO solutionDTO) throws Exception {
        return solutionDAO.getListSolution(solutionDTO);
    }

    @Override
    public SolutionDTO getSolutionReponse(Long reflectId) throws Exception {
        List<ConditionBean> lst = new ArrayList<>();
        lst.add(new ConditionBean("reflectId", " = ", StringUtils.getString(reflectId), ParamUtils.DataType.LONG));
        lst.add(new ConditionBean("solutionStatus", " = ", ParamUtils.SOLUTION_STATUS.REPONSE, ParamUtils.DataType.STRING));
        logger.info("reflectId: "+reflectId);
        logger.info("solutionStatus: "+ParamUtils.SOLUTION_STATUS.REPONSE);
        List<SolutionDTO> lstSolutionDTO = find("SolutionEntity", lst);
        return lstSolutionDTO != null && lstSolutionDTO.size() > 0 ? lstSolutionDTO.get(0) : null;
    }

}
