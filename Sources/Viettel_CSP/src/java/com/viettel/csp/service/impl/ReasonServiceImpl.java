/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.service.impl;

import com.viettel.csp.DAO.ReasonDAO;
import com.viettel.csp.DTO.ReasonDTO;
import com.viettel.csp.entity.ReasonEntity;
import com.viettel.csp.service.ReasonService;
import com.viettel.csp.util.ConditionBean;
import com.viettel.csp.util.KeyValueBean;
import com.viettel.csp.util.ModelMapperUtil;
import com.viettel.csp.util.ParamUtils;
import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author GEM
 */
public class ReasonServiceImpl extends BaseCustomServiceImpl<ReasonDAO, ReasonEntity, ReasonDTO> implements ReasonService {

    @Autowired
    private ReasonDAO reasonDAO;

    public ReasonDAO getReasonDAO() {
        return reasonDAO;
    }

    public void setReasonDAO(ReasonDAO reasonDAO) {
        this.reasonDAO = reasonDAO;
        super.set(locale, new ReasonEntity(), reasonDAO, ReasonDTO.class);
    }

    public List<KeyValueBean> getListToKeyValueBeanByType(String type) throws Exception {
        List<KeyValueBean> listKeyValueBean = new ArrayList<>();
        List<ReasonDTO> list = findListByType(type);
        for (ReasonDTO reasonDTO : list) {
            KeyValueBean kvb = new KeyValueBean(reasonDTO.getCode(), reasonDTO.getName());
            listKeyValueBean.add(kvb);
        }
        return listKeyValueBean;
    }

    public List<ReasonDTO> findListByType(String type) throws Exception {
        List<ConditionBean> lstCondition = new ArrayList<ConditionBean>();
        lstCondition.add(new ConditionBean("type", "=", type, ParamUtils.TYPE_STRING));
        return find("ReasonEntity", lstCondition);
    }

    @Override
    public void save(ReasonDTO reasonDTO) {
        ModelMapperUtil modelMapperUtil = new ModelMapperUtil();

        System.out.println("save service");

        ReasonEntity reasonEntity = modelMapperUtil.mapper(reasonDTO, ReasonEntity.class);

        reasonDAO.save(reasonEntity);
    }

    @Override
    public void update(ReasonDTO reasonDTO) {
        ModelMapperUtil modelMapperUtil = new ModelMapperUtil();
        reasonDAO.update(modelMapperUtil.mapper(reasonDTO, ReasonEntity.class));
    }

    @Override
    public void delete(List<ReasonDTO> dtoList) {
        ModelMapperUtil mapperUtil = new ModelMapperUtil();
        for(ReasonDTO reasonDTO : dtoList){
            reasonDAO.delete(mapperUtil.mapper(reasonDTO, ReasonEntity.class));
        }
    }

    public List<ReasonDTO> search(ReasonDTO reasonDTO){
        ModelMapperUtil mapperUtil = new ModelMapperUtil();
        return convertListEntitytoDTO(
                reasonDAO.search(mapperUtil.mapper(reasonDTO, ReasonEntity.class)));
    }


}
