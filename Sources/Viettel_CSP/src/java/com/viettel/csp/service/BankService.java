/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.service;

import com.viettel.csp.DTO.BankDTO;
import com.viettel.csp.util.KeyValueBean;
import java.util.List;

/**
 *
 * @author GEM
 */
public interface BankService {
    public List<KeyValueBean> getListBank() throws Exception;
    public List<BankDTO> findAll() throws Exception;
}
