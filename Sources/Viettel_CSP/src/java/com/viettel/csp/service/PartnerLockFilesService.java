/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.service;

import com.viettel.csp.DTO.PartnerLockFilesDTO;
import java.util.List;

/**
 *
 * @author GEM
 */
public interface PartnerLockFilesService {

    public void insert(PartnerLockFilesDTO filesDTO) throws Exception;

    public void insertList(List<PartnerLockFilesDTO> list) throws Exception;

    public List<PartnerLockFilesDTO> findByPartnerLockId(Long partnerLockId) throws Exception;

    public void deletePartnerLockId(Long partnerLockId) throws Exception;
}
