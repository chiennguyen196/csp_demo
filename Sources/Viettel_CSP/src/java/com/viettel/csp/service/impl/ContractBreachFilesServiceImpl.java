package com.viettel.csp.service.impl;

import com.viettel.csp.DAO.ContractBreachFilesDAO;
import com.viettel.csp.DTO.ContractBreachFileDTO;
import com.viettel.csp.entity.ContractBreachFilesEntity;
import com.viettel.csp.service.ContractBreachFilesService;
import org.apache.tools.ant.types.resources.comparators.Date;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by GEM on 6/16/2017.
 */
public class ContractBreachFilesServiceImpl extends BaseCustomServiceImpl<ContractBreachFilesDAO, ContractBreachFilesEntity, ContractBreachFileDTO> implements ContractBreachFilesService {
    @Autowired
    ContractBreachFilesDAO contractBreachFilesDAO;

    public ContractBreachFilesDAO getContractBreachFilesDAO() {
        return contractBreachFilesDAO;
    }

    public void setContractBreachFilesDAO(ContractBreachFilesDAO contractBreachFilesDAO) {
        super.set(this.locale, new ContractBreachFilesEntity(), contractBreachFilesDAO, ContractBreachFileDTO.class);
        this.contractBreachFilesDAO = contractBreachFilesDAO;
    }

    public void insert(ContractBreachFileDTO dto){
        ContractBreachFilesEntity entity = convertToEntity(dto);
        contractBreachFilesDAO.insert(entity);
    }

    public void insertList(List<ContractBreachFileDTO> dtoList){
        for (ContractBreachFileDTO dto:dtoList){
            insert(dto);
        }
    }

    private ContractBreachFilesEntity convertToEntity(ContractBreachFileDTO dto){
        ContractBreachFilesEntity entity = new ContractBreachFilesEntity();
        entity.setUploadDate(dto.getUploadDate());
        entity.setPartnerBreachId(dto.getPartnerBreachId());
        entity.setPathFile(dto.getPathFile());
        entity.setFileName(dto.getFileName());
        entity.setDescription(dto.getDescription());

        return entity;
    }

}
