/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.service;

import com.viettel.csp.DTO.ContactPointDTO;
import java.util.List;

/**
 * @author admin
 */
public interface ContactPointService {

    public String saveOrUpdate(ContactPointDTO contactPointDTO);

    public List<ContactPointDTO> getList(Long contractID);

    public void update(List<ContactPointDTO> listContactPointDTO);
}
