package com.viettel.csp.service.impl;

import com.viettel.csp.DAO.BaseCustomDAO;
import com.viettel.csp.util.ConditionBean;
import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.entity.BaseCustomEntity;
import com.viettel.csp.DTO.BaseCustomDTO;
import com.viettel.csp.DTO.DTOInterface;
import com.viettel.csp.service.BaseCustomService;
import com.viettel.csp.util.KeyValueBean;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.ModelMapperUtil;
import java.util.*;
import org.hibernate.Session;

/**
 *
 * @author tiennv@gemvietnam.com
 */
public class BaseCustomServiceImpl<TDAO extends BaseCustomDAO, TEntity extends BaseCustomEntity, TDto extends BaseCustomDTO>
        implements BaseCustomService<TEntity, TDto> {

    protected Session session;
    protected TDAO tDAO;
    protected TEntity tEntity;
    protected Locale locale;
    protected String sql;
    protected String fileName;
    protected String separator;
    protected Class<TDto> classTDto;

    org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(BaseCustomServiceImpl.class);

    public void set(Locale locale, TEntity bo, TDAO dao, Class<TDto> classTDto) {
        setLocale(locale);
        settEntity(bo);
        settDAO(dao);
        setClassTDto(classTDto);
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public TEntity gettEntity() {
        return tEntity;
    }

    public void settEntity(TEntity tBO) {
        this.tEntity = tBO;
    }

    public TDAO gettDAO() {
        return tDAO;
    }

    public void settDAO(TDAO tDAO) {
        this.tDAO = tDAO;
    }

    public Class<TDto> getClassTDto() {
        return classTDto;
    }

    public void setClassTDto(Class<TDto> classTDto) {
        this.classTDto = classTDto;
    }

    @Override
    public TEntity insert(TEntity tBO) {
        return (TEntity) tDAO.saveOrUpdate(tBO);
    }

    @Override
    public TEntity update(TEntity tBO) {
        return (TEntity) tDAO.update(tBO);
    }

    @Override
    public List findAll() {
        logger.info("Set super.set(locale, new entity(), dao, dto.class) trong ServiceImpl, vi du: PartnerServiceImpl.class");
        return convertListEntitytoDTO(tDAO.getAllData(null));
    }

    /*
     * @parameter: An interface Form @return: A list Condition made of form
     * attributes if attribute is null then ignored, else string will be
     * compared by "like", number by "=" date by ">=" or "<=" depend on
     * attribute name
     */
    @Override
    public List search(TDto tForm, int start, int maxResult, String sortType, String sortField) {
        return convertListEntitytoDTO(tDAO.search(tForm.toBO(), start, maxResult, sortType, sortField));
    }

    @Override
    public long count(TDto tForm) {
        return tDAO.count(tForm.toBO());
    }

    @Override
    public List<TDto> search(TDto ts) {
        return convertListEntitytoDTO(tDAO.search(ts.toBO(),
                0, 10000,
                ParamUtils.ORDER_ASC, ts.getDefaultSortField()));
    }

    @Override
    public List<TDto> search(TDto form, List<ConditionBean> lstCondition) {
        return convertListEntitytoDTO(tDAO.search(form.toBO(), lstCondition, 0, 10000, ParamUtils.ORDER_ASC, form.getDefaultSortField()));
    }

    public List<KeyValueBean> getListToKeyValueBean(Boolean isChoose) {
        List<TDto> lsttForm = findAll();
        List<KeyValueBean> lstBean = new ArrayList<KeyValueBean>();
        if (isChoose) {
            lstBean.add(new KeyValueBean(ParamUtils.SELECT_NOTHING_VALUE_STR, LanguageBundleUtils.getString("global.combobox.choose")));
        }
        if (lsttForm != null) {
            for (DTOInterface form : lsttForm) {
                lstBean.add(form.toBean());
            }
        }
        return lstBean;
    }

    public List convertListEntitytoDTO(List<TEntity> listBO) {
        ModelMapperUtil modelMapperUtil = new ModelMapperUtil();
        List<TDto> lstForm = new ArrayList<TDto>();
        if (listBO != null) {
            for (TEntity bo : listBO) {
                TDto dto = modelMapperUtil.mapper(bo, classTDto);
                lstForm.add(dto);
            }
        }
        return lstForm;
    }

    public TDto findOnly(String boName, ConditionBean lstCondition) {
        List<TDto> lst = convertListEntitytoDTO(tDAO.find(boName, lstCondition));
        return lst != null && lst.size() > 0 ? lst.get(0) : null;
    }

    public List find(String boName, ConditionBean lstCondition) {
        return convertListEntitytoDTO(tDAO.find(boName, lstCondition));
    }

    public List find(String boName, List<ConditionBean> lstCondition) {
        return convertListEntitytoDTO(tDAO.find(boName, lstCondition));
    }

    public TEntity findOnlyEntity(String boName, ConditionBean lstCondition) {
        List<TEntity> lst = tDAO.find(boName, lstCondition);
        return lst != null && lst.size() > 0 ? lst.get(0) : null;
    }

    public List findEntity(String boName, ConditionBean lstCondition) {
        return tDAO.find(boName, lstCondition);
    }

    public List findEntity(String boName, List<ConditionBean> lstCondition) {
        return tDAO.find(boName, lstCondition);
    }
}
