/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.service.impl;

import com.viettel.csp.DAO.ContactTypeDAO;
import com.viettel.csp.DTO.ContactTypeDTO;
import com.viettel.csp.entity.ContactTypeEntity;
import com.viettel.csp.service.ContactTypeService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author GEM
 */
public class ContactTypeServiceImpl extends BaseCustomServiceImpl<ContactTypeDAO, ContactTypeEntity, ContactTypeDTO> implements ContactTypeService {

    @Autowired
    private ContactTypeDAO contactTypeDAO;

    public ContactTypeDAO getContactTypeDAO() {
        return contactTypeDAO;
    }

    public void setContactTypeDAO(ContactTypeDAO contactTypeDAO) {
        this.contactTypeDAO = contactTypeDAO;
        this.tDAO = contactTypeDAO;
        this.classTDto = ContactTypeDTO.class;
    }
}
