package com.viettel.csp.service;

import com.viettel.csp.DTO.ContractBreachFileDTO;

import java.util.List;

/**
 * Created by GEM on 6/16/2017.
 */
public interface ContractBreachFilesService {

    public void insert(ContractBreachFileDTO dto);

    public void insertList(List<ContractBreachFileDTO> dtoList);
}
