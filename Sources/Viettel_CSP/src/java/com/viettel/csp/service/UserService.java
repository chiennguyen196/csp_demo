/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.service;

import com.viettel.csp.DTO.DepartmentDTO;
import com.viettel.csp.DTO.PartnerDTO;
import com.viettel.csp.DTO.UserDTO;
import com.viettel.csp.entity.UserEntity;

import java.util.List;

/**
 * @author GEM
 */
public interface UserService {

    public void deleteUser(List<Long> lstPersonInfoId, String personType) throws Exception;

    public List<UserDTO> checkLogin(UserDTO udto);

    public List<UserDTO> findUserByOrgzanition(Long orgId);

    public UserEntity updateUser(UserEntity userEntity);

    public UserEntity insert(UserEntity userEntity);

    public UserDTO getUserDTOByUserName(String username);

    public UserEntity getUserEntityByUserName(String username);

    public UserEntity checkEntity(UserEntity userEntity);

    public UserEntity sendEmail(UserEntity UserEntity);

    public String updatePasswordUser(String username, String newPassword, String oldPassword);

    public String genUID();

    public UserEntity getUserByID(Long id);

    public boolean existedEmail(String email);

    public boolean existedUsername(String username);

    public boolean existedMobileNumber(String mobileNumber);

    public List<UserDTO> fillByPersonType(String personType, String search);

    public void updateUserByPartner(PartnerDTO newPartnerDTO);

    public DepartmentDTO getDepartmentByOrganizationId();

    public List<UserEntity> findAllEntities();

    List<UserDTO> findAllEntitiesWithPersonType(String personType);

    public List<UserEntity> fillByUsername(String username);

    public List<UserEntity> fillByFullname(String viettel);

    public List<UserEntity> fillByUserId(String fullName);
}
