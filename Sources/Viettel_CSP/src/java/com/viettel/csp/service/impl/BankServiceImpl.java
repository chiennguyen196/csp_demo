/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.service.impl;

import com.viettel.csp.DAO.BankDAO;
import com.viettel.csp.DTO.BankDTO;
import com.viettel.csp.entity.BankEntity;
import com.viettel.csp.service.BankService;
import com.viettel.csp.util.KeyValueBean;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author GEM
 */
public class BankServiceImpl extends BaseCustomServiceImpl<BankDAO, BankEntity, BankDTO> implements BankService {

    @Autowired
    private BankDAO bankDAO;

    public BankDAO getBankDAO() {
        return bankDAO;
    }

    public void setBankDAO(BankDAO bankDAO) {
        this.bankDAO = bankDAO;
        this.tDAO = bankDAO;
        this.classTDto = BankDTO.class;
    }

    @Override
    public List<KeyValueBean> getListBank() throws Exception {

        List<BankEntity> listBank = bankDAO.findAll();
        List<KeyValueBean> lstKeyValue = new ArrayList<KeyValueBean>();
        for (BankEntity item : listBank) {
            KeyValueBean keyValueBean = new KeyValueBean(item.getCode(), item.getName());
            lstKeyValue.add(keyValueBean);
        }
        return lstKeyValue;
    }

}
