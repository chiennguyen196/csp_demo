/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.service;

import com.viettel.csp.DTO.DepartmentDTO;
import com.viettel.csp.DTO.ServiceDTO;
import com.viettel.csp.DTO.SolutionFilesDTO;

import java.util.List;

/**
 *
 * @author admin
 */
public interface SolutionFilesService  {
    public List<SolutionFilesDTO> getListSolutionFiles(SolutionFilesDTO solutionFilesDTO) throws Exception;
}
