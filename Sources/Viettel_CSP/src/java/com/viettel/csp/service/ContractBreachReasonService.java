package com.viettel.csp.service;

import com.viettel.csp.DTO.ContractBreachReasonDTO;

import java.util.List;

/**
 * Created by GEM on 6/16/2017.
 */
public interface ContractBreachReasonService {

    public void insertList(List<ContractBreachReasonDTO> listReason );

    public void insert(ContractBreachReasonDTO dto);

    public List<ContractBreachReasonDTO> getByContractBreachId(long id);
}
