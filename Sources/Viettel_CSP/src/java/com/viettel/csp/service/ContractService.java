package com.viettel.csp.service;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.viettel.csp.DTO.ContactPointDTO;
import com.viettel.csp.DTO.ContractDTO;
import com.viettel.csp.DTO.ContractHisDTO;
import com.viettel.csp.entity.ContractEntity;
import com.viettel.csp.util.KeyValueBean;
import com.viettel.logincsp.UserCredential;
import java.util.List;

/**
 * @author GEM
 */
public interface ContractService extends BaseCustomService<ContractEntity, ContractDTO> {

    public List<ContractDTO> getListContract(ContractDTO contractDTO);

    public List<ContractDTO> getListContractFiles(ContractDTO contractDTO) throws Exception;

    public List<ContractDTO> getListContractForWorkManager(ContractDTO partnerDTO) throws Exception;

    public List<KeyValueBean> getListContractType() throws Exception;

    public List<KeyValueBean> getListContractStatus() throws Exception;

    public List<KeyValueBean> getListContractTypeByCode(String partnerTypeCode) throws Exception;

    public List<KeyValueBean> getListContractTypeByName(String partnerTypeName) throws Exception;

    public String update(ContractDTO contractDTO, String action, int flat);

    public String deleteContract(List<ContractDTO> lstDeleteDTO);

    public boolean checkValidateDelete(Long id) throws Exception;

    public String insert(ContractDTO ContractDto, int flat) throws Exception;

    public long insertContract(ContractDTO ContractDto, int flat);

    public List<ContractEntity> getListContractNumber(Long partnerId) throws Exception;

    public String updateContractSendPartner(ContractDTO contractDTO, String action, int flat)
        throws Exception;

    public List<ContractDTO> getListContract(ContractDTO contractDTO, UserCredential credential);

    public List<ContractDTO> getListContractForPartnerLock(ContractDTO contractDTO);

    public void updateContractLock(List<ContractDTO> lstResult, long value);

    public List<ContractDTO> getListLiquidation(ContractDTO contractDTO);

    public String insertContractAndContactPoint(ContractDTO contractDTO,
        ContactPointDTO contactPointDTO, ContractHisDTO contractHisDTO) throws Exception;

    public List<ContractDTO> getListContractProfile(ContractDTO contractDTO,
        UserCredential credential);

    boolean signContract(ContractDTO contractDTO);

    public boolean rejectContract(ContractDTO contractDTO, ContractHisDTO contractHisDTO);

    public long updateCreatLiquidation(ContractDTO contractDTO, String action, String liStatus);

    public List<ContractDTO> getContractByContractId(List<ContractDTO> list, long contractId);


    public boolean rejectSignContract(ContractDTO contractDTO, ContractHisDTO contractHisDTO);

    boolean generalManagerSign(ContractDTO contractDTO);

    public ContractEntity getContractEntity(Long contractId);

    boolean pCReject(ContractDTO contractDTO, ContractHisDTO contractHisDTO);

    boolean pCAccept(ContractDTO oldDTO);

    public boolean checkValidateContractNumber(String contractNumber);

    public String updateContract(ContractDTO contractDTO, String action, int flat);
}
