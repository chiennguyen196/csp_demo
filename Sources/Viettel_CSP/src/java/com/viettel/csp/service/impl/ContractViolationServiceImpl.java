package com.viettel.csp.service.impl;

import com.viettel.csp.DAO.ContractBreachDAO;
import com.viettel.csp.DAO.ContractDAO;
import com.viettel.csp.DTO.ContractBreachDTO;
import com.viettel.csp.DTO.ContractDTO;
import com.viettel.csp.entity.ContractBreachEntity;
import com.viettel.csp.service.ContractViolationService;
import com.viettel.csp.util.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by GEM on 6/15/2017.
 */
public class ContractViolationServiceImpl extends BaseCustomServiceImpl<ContractBreachDAO, ContractBreachEntity, ContractBreachDTO> implements ContractViolationService {

    @Autowired
    ContractBreachDAO contractBreachDAO;

    public ContractBreachDAO getContractBreachDAO() {
        return contractBreachDAO;
    }

    public void setContractBreachDAO(ContractBreachDAO contractBreachDAO) {
        super.set(this.locale, new ContractBreachEntity(), contractBreachDAO, ContractBreachDTO.class);
        this.contractBreachDAO = contractBreachDAO;
    }

    public List<KeyValueBean> getListToKeyValueBeanByType(List<ContractDTO> contractDTOs) {
        List<KeyValueBean> listKeyValueBean = new ArrayList<>();
        for (ContractDTO dto : contractDTOs) {
            KeyValueBean kvb = new KeyValueBean(dto.getContractNumber(), dto.getContractNumber());
            listKeyValueBean.add(kvb);
        }
        return listKeyValueBean;
    }

    public List<KeyValueBean> getListToKeyValueBeanByType() {
        List<KeyValueBean> listKeyValueBean = new ArrayList<>();

        return listKeyValueBean;
    }


    public List<ContractDTO> fillContractListByStatus(List<ContractDTO> contractDTOList, int statusNumber){
        List<ContractDTO> lst = new ArrayList<>();
       for (ContractDTO dto:contractDTOList){
           if(dto.getContractStatus().equals(String.valueOf(statusNumber))){
               lst.add(dto);
           }
       }
        contractDTOList = null;
        return lst;
    }

    public List<KeyValueBean> bindDataToCombobox(List<String> lst){
        List<KeyValueBean> listKeyValueBean = new ArrayList<>();
        for (String str:lst){
            int key = getKey(str);
            if (key == 0)
                continue;
            KeyValueBean kvb = new KeyValueBean(key, str);
            listKeyValueBean.add(kvb);
        }
        return listKeyValueBean;
    }

    private int getKey(String str){
        if (str.equals(LanguageBundleUtils.getString("partner.violation.blockNumber")))
            return 1;
        if (str.equals(LanguageBundleUtils.getString("partner.violation.stopService")))
            return 2;
        if (str.equals(LanguageBundleUtils.getString("partner.violation.stopContract")))
            return 3;
        if(str.equals(LanguageBundleUtils.getString("partner.violation.dropContract")))
            return 4;
        if (str.equals(LanguageBundleUtils.getString("partner.violation.money")))
            return 5;
        if (str.equals(LanguageBundleUtils.getString("partner.violation.notify")))
            return 6;
        if (str.equals(LanguageBundleUtils.getString("partner.violation.violateContract")))
            return 7;
        if (str.equals(LanguageBundleUtils.getString("partner.violation.required")))
            return 8;
        return 0;
    }



    public void insert(ContractBreachDTO contractBreachDTO){
        ContractBreachEntity entity = convertToEntity(contractBreachDTO);
        //Ham insert tu DAO
        //check ton tai
        contractBreachDAO.insert(entity);
    }

    public boolean checkExistByContractId(Long contractId){
        ContractBreachDTO contractBreachDTO = getContractBreachByContractId(contractId);
        if (contractBreachDTO != null)
            return true;
        return false;
    }

    public void update(ContractBreachDTO contractBreachDTO){
        ContractBreachEntity entity = convertToEntity(contractBreachDTO);
        contractBreachDAO.update(entity);
    }

    public ContractBreachDTO getContractBreachByContractId(Long contractId){
        //Ham select tu DAO theo partnerID
        List<ContractBreachDTO> lst = find("ContractBreachEntity", new ConditionBean("CONTRACT_ID", "=", StringUtils.getString(contractId), ParamUtils.DataType.LONG));
        if (lst == null || lst.size() == 0)
            return null;

        ContractBreachEntity entity = convertToEntity(lst.get(0));
        return convertToDTO(entity);
    }


    private ContractBreachDTO convertToDTO(ContractBreachEntity entity){
        ContractBreachDTO dto = new ContractBreachDTO();
        dto.setContractId(entity.getContractId());
        dto.setStartDate(entity.getStartDate());
        dto.setEndDate(entity.getEndDate());
        dto.setUpdateBY(entity.getUpdateBY());
        dto.setPartnerId(entity.getPartnerId());
        dto.setUpdateAt(entity.getUpdateAt());
        dto.setHeadOfService(entity.getHeadOfService());
        dto.setReasonDisciplineCode(entity.getReasonDisciplineCode());
        dto.setIsPublic(entity.getIsPublic());
        dto.setCreateAt(entity.getCreateAt());
        dto.setCreateBy(entity.getCreateBy());
        dto.setId(entity.getId());

        return dto;
    }

    private ContractBreachEntity convertToEntity(ContractBreachDTO contractBreachDTO){
        ContractBreachEntity entity = new ContractBreachEntity();
        entity.setContractId(contractBreachDTO.getContractId());
        entity.setStartDate(contractBreachDTO.getStartDate());
        entity.setEndDate(contractBreachDTO.getEndDate());
        entity.setUpdateBY(contractBreachDTO.getUpdateBY());
        entity.setPartnerId(contractBreachDTO.getPartnerId());
        entity.setUpdateAt(contractBreachDTO.getUpdateAt());
        entity.setHeadOfService(contractBreachDTO.getHeadOfService());
        entity.setReasonDisciplineCode(contractBreachDTO.getReasonDisciplineCode());
        entity.setIsPublic(contractBreachDTO.getIsPublic());
        entity.setCreateAt(contractBreachDTO.getCreateAt());
        entity.setCreateBy(contractBreachDTO.getCreateBy());
        entity.setId(contractBreachDTO.getId());

        return entity;
    }

}
