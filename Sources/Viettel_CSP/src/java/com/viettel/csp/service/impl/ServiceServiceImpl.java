/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.service.impl;

import com.viettel.csp.DAO.ServiceDAO;
import com.viettel.csp.DTO.ServiceDTO;
import com.viettel.csp.entity.ServiceEntity;
import com.viettel.csp.service.ServiceService;

import java.util.ArrayList;
import java.util.List;

import com.viettel.csp.util.KeyValueBean;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.util.SendMailUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author admin
 */
public class ServiceServiceImpl extends BaseCustomServiceImpl<ServiceDAO, ServiceEntity, ServiceDTO> implements ServiceService {

    @Autowired
    private ServiceDAO serviceDAO;

    public ServiceDAO getServiceDAO() {
        return serviceDAO;
    }

    public void setServiceDAO(ServiceDAO serviceDAO) {
        super.set(locale, new ServiceEntity(), serviceDAO, ServiceDTO.class);
        this.serviceDAO = serviceDAO;
    }

    @Override
    public ServiceEntity getServiceEntityByCode(String code) {
        return serviceDAO.getDetailServiceByCode(code);
    }

    @Override
    public ServiceDTO insertService(ServiceDTO serviceDTO) {
//       return serviceDAO.insertService(serviceDTO);
        ServiceEntity serviceEntity = null;
        serviceEntity = new ServiceEntity();
        serviceEntity = ServiceEntity.setEntityInsert(serviceDTO, serviceEntity);
        serviceEntity = serviceDAO.insert(serviceEntity);
        return serviceDTO;
    }

    @Override
    public ServiceEntity findServiceById(Long serviceId) {
        ServiceEntity serviceEntity = null;
        serviceEntity = serviceDAO.findById(serviceId);
        return serviceEntity;
    }

    @Override
    public List<ServiceDTO> getServiceByCode(String code) {
        return serviceDAO.getServiceByCode(code);
    }

    @Override
    public void updateService(ServiceDTO serviceDTO) {
        ServiceEntity serviceEntity = serviceDAO.getDetailServiceByCode(serviceDTO.getCode());
        serviceEntity.setName(serviceDTO.getName());
        serviceEntity.setCode(serviceDTO.getCode());
        serviceEntity.setParentCode(serviceDTO.getParentCode());
        serviceEntity.setPathFile(serviceDTO.getPathFile());
        serviceDAO.update(serviceEntity);
    }

    @Override
    public boolean insert(ServiceDTO serviceDTO) {
        ServiceEntity serviceEntity = new ServiceEntity();
        if (serviceDAO.getDetailServiceByCode(serviceDTO.getCode()) == null) {
            serviceEntity.setName(serviceDTO.getName());
            serviceEntity.setCode(serviceDTO.getCode());
            serviceEntity.setParentCode(serviceDTO.getParentCode());
            serviceEntity.setPathFile(serviceDTO.getPathFile());
            serviceDAO.insert(serviceEntity);
            return true;
        } else return false;
    }

    @Override
    public String deleteService(List<ServiceDTO> lstDeleteDTO) {
        String message = ParamUtils.SUCCESS;
        try {
            int total = lstDeleteDTO.size();
            int ok = 0;
            for (ServiceDTO serviceDTO : lstDeleteDTO) {
                String serviceCode = serviceDTO.getCode();
                ServiceEntity serviceEntity = serviceDAO.getDetailServiceByCode(serviceCode);
                serviceEntity.setCode(serviceDTO.getCode());
                serviceDAO.delete(serviceEntity);
                //Send mail  and notification
                SendMailUtils.sendMailAndNotification(null, null, null, null);
                ok++;
            }
            if (ok == total) {
                message = LanguageBundleUtils.getMessage(LanguageBundleUtils.getString("global.message.delete.multi.successful"),
                        String.valueOf(ok),
                        String.valueOf(total),
                        String.valueOf(total - ok));
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            message = LanguageBundleUtils.getString("global.message.error");
        }
        return message;
    }

    @Override
    public List<KeyValueBean> listParrentCode() {
        List<ServiceEntity> listService = serviceDAO.findAll();
        List<KeyValueBean> keyValueBeans = new ArrayList<KeyValueBean>();
        keyValueBeans.add(new KeyValueBean(ParamUtils.SELECT_NOTHING_VALUE, LanguageBundleUtils.getString("global.combobox.choose")));
        for (ServiceEntity item : listService) {
            if (item.getParentCode() == null) {
                KeyValueBean keyValueBean = new KeyValueBean(item.getCode(), item.getName());
                keyValueBeans.add(keyValueBean);
            }
        }
        return keyValueBeans;
    }

    @Override
    public List<ServiceDTO> getListChilService(String parentCode) {
        List<ServiceDTO> listChilService = serviceDAO.getListChilService(parentCode);
        return listChilService;
    }

    @Override
    public List<ServiceDTO> getList() {
        return serviceDAO.getList();
    }

    @Override
    public List<ServiceDTO> getListServiceByDTO(ServiceDTO serviceDTO) {
        return serviceDAO.getListServiceByDTO(serviceDTO);
    }

    @Override
    public List<ServiceDTO> getListWithNullParent() {
        return serviceDAO.getListWithNullParent();
    }

    @Override
    public List<ServiceDTO> getListType() {
        return serviceDAO.getListType();
    }

    @Override
    public List<ServiceDTO> getListServiceChild() throws  Exception{
        return serviceDAO.getListServiceChild();
    }
}
