/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.service;

import com.viettel.csp.DTO.SolutionDTO;
import com.viettel.csp.DTO.SolutionFilesDTO;
import java.util.List;

/**
 *
 * @author admin
 */
public interface SolutionService {

    public SolutionDTO getById(Long solutionId);

    public String sendEvaluatePartner(SolutionDTO oldDto, String solutionStatus);

    public String sendSolution(SolutionDTO solutionDTO) throws Exception;

    public List<SolutionDTO> getListSolution(SolutionDTO solutionDTO) throws Exception;

    public SolutionDTO getSolutionReponse(Long reflectId) throws Exception;
}
