package com.viettel.csp.service;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.viettel.csp.DTO.ContractHisDTO;
import com.viettel.csp.entity.ContractHisEntity;
import org.hibernate.HibernateException;

import java.util.List;

/**
 * @author GEM
 */
public interface ContractHisService extends BaseCustomService<ContractHisEntity, ContractHisDTO>
{

    public List<ContractHisDTO> getListContractHis(ContractHisDTO partnerDTO);

    public String insert(Long contractId, ContractHisDTO ContractHisDto) throws Exception;

    public List<ContractHisDTO> getListContractHisAddUser(ContractHisDTO contractHisDTO) throws HibernateException;

    public String insertSendContract(Long contractId, ContractHisDTO contractHisDTO) throws Exception;

    public void insertEntity(ContractHisEntity contractHis) throws Exception;

    public String insertResonCancleRegisterLiquidation(Long contractId, ContractHisDTO ContractHisDto);

    public String insertResonCancleAssignLiquidation(Long contractId, ContractHisDTO ContractHisDto);

    public String insertResonCanclePartnerRegister(Long contractId, ContractHisDTO ContractHisDto);
}
