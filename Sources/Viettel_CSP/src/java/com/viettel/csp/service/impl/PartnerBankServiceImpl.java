/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.service.impl;

import com.viettel.csp.DAO.PartnerBankDAO;
import com.viettel.csp.DTO.PartnerBankDTO;
import com.viettel.csp.entity.PartnerBankEntity;
import com.viettel.csp.service.PartnerBankService;
import com.viettel.csp.util.ModelMapperUtil;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author GEM
 */
public class PartnerBankServiceImpl extends BaseCustomServiceImpl<PartnerBankDAO, PartnerBankEntity, PartnerBankDTO> implements PartnerBankService {

    @Autowired
    private PartnerBankDAO partnerBankDAO;

    public PartnerBankDAO getPartnerBankDAO() {
        return partnerBankDAO;
    }

    public void setPartnerBankDAO(PartnerBankDAO partnerBankDAO) {
        this.partnerBankDAO = partnerBankDAO;
        super.set(null, new PartnerBankEntity(), new PartnerBankDAO(), PartnerBankDTO.class);
    }

    @Override
    public void insert(List<PartnerBankDTO> lstPartnerBank) throws Exception {
        for (int i = 0; i < lstPartnerBank.size(); i++) {
            ModelMapperUtil mapperUtil = new ModelMapperUtil();
            PartnerBankEntity partnerBankEntity = mapperUtil.mapper(lstPartnerBank.get(i), PartnerBankEntity.class);
            partnerBankDAO.insert(partnerBankEntity);
        }
    }

    @Override
    public List<PartnerBankDTO> getInfoBankByPartnerId(Long partnerId) throws Exception {
        return partnerBankDAO.getInfoBankByPartnerId(partnerId);
    }

    @Override
    public void deletePartnerBank(Long partnerId) throws Exception {
        partnerBankDAO.deleteByPartnerId(partnerId);
    }

    @Override
    public void deleteByLstPartnerBank(List<Long> lstPartnerId) throws Exception {
        for (int i = 0, total = lstPartnerId.size(); i < total; i++) {
            partnerBankDAO.deleteByPartnerId(lstPartnerId.get(i));
        }
    }
}
