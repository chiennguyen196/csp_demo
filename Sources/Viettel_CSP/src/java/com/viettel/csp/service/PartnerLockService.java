/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.service;

import com.viettel.csp.DTO.PartnerLockDTO;
import java.util.List;

/**
 *
 * @author GEM
 */
public interface PartnerLockService {

    public void insert(PartnerLockDTO partnerBank) throws Exception;

    public void insertList(List<PartnerLockDTO> lstPartnerBank) throws Exception;

    public List<PartnerLockDTO> findAll();

    public void deletePartnerId(Long partnerId) throws Exception;

    public String savePartnerLock(PartnerLockDTO partnerLockDTO, String action);

    public PartnerLockDTO findByPartnerId(Long id);

    public void update(PartnerLockDTO partnerLockDTO);
}
