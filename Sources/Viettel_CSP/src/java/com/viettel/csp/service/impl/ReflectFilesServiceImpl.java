/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.service.impl;

import com.viettel.csp.DAO.ReflectFilesDAO;
import com.viettel.csp.DTO.ReflectFilesDTO;
import com.viettel.csp.entity.ReflectFilesEntity;
import com.viettel.csp.service.ReflectFilesService;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.ParamUtils;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author admin
 */
public class ReflectFilesServiceImpl extends BaseCustomServiceImpl<ReflectFilesDAO, ReflectFilesEntity, ReflectFilesDTO> implements ReflectFilesService {

    @Autowired
    private ReflectFilesDAO reflectFilesDAO;

    public ReflectFilesDAO getReflectFilesDAO() {
        return reflectFilesDAO;
    }

    public void setReflectFilesDAO(ReflectFilesDAO reflectFilesDAO) {
        super.set(locale, new ReflectFilesEntity(), reflectFilesDAO, ReflectFilesDTO.class);
        this.reflectFilesDAO = reflectFilesDAO;
    }

    @Override
    public List<ReflectFilesDTO> getList(ReflectFilesDTO reflectFilesDTO) {
        return reflectFilesDAO.getList(reflectFilesDTO);
    }

    @Override
    public String insert(ReflectFilesDTO reflectFilesDTO) throws Exception {
        String message = ParamUtils.SUCCESS;
        ReflectFilesEntity reflectFilesEntity = new ReflectFilesEntity();
        try {
            //Create object Contrract
            reflectFilesEntity = reflectFilesEntity.getEntity(reflectFilesDTO);

            reflectFilesDAO.insert(reflectFilesEntity);

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            message = LanguageBundleUtils.getString("global.message.error");
        }
        return message;
    }

}
