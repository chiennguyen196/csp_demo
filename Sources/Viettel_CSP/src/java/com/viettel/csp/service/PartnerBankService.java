/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.service;

import com.viettel.csp.DTO.PartnerBankDTO;
import com.viettel.csp.entity.PartnerBankEntity;
import java.util.List;

/**
 *
 * @author TienNV@gemvietnam.com
 */
public interface PartnerBankService {

    public List<PartnerBankDTO> getInfoBankByPartnerId(Long partnerId) throws Exception;

    public void insert(List<PartnerBankDTO> lstPartnerBank) throws Exception;

    public List<PartnerBankDTO> findAll();
    
     public void deletePartnerBank(Long partnerId) throws Exception ;

    public void deleteByLstPartnerBank(List<Long> lstPartnerId) throws Exception ;
}
