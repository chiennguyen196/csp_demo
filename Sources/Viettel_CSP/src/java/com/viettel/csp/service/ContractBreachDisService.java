package com.viettel.csp.service;

import com.viettel.csp.DTO.ContractBreachDisDTO;
import com.viettel.csp.util.KeyValueBean;
import org.zkoss.zul.Combobox;

import java.util.Date;
import java.util.List;

/**
 * Created by GEM on 6/17/2017.
 */
public interface ContractBreachDisService {

    public ContractBreachDisDTO createBreachDisDTO(int key, Date startDate, Date endDate, String content, String contentValue, String headOfService, long contractId);

    public void insert(ContractBreachDisDTO contractBreachDisDTO);

    public void insertDataList(List<ContractBreachDisDTO> contractBreachDisDTOs);

    public List<KeyValueBean> getListToKeyValueBeanByType(ContractBreachDisDTO dto, String value);

    public List<ContractBreachDisDTO> getBreachContractId(long contractId);

    public ContractBreachDisDTO setValue(int key, ContractBreachDisDTO contractBreachDisDTO,
                                         Date startDate, Date endDate, String content, String contentValue ,String headOfService, long contractId);
}
