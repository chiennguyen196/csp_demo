/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.service.impl;

import com.viettel.csp.DAO.MessageDAO;
import com.viettel.csp.DTO.NotifyDTO;
import com.viettel.csp.entity.MessageEntity;
import com.viettel.csp.entity.UserEntity;
import com.viettel.csp.service.MessageService;
import com.viettel.csp.service.UserService;
import com.viettel.csp.util.ConditionBean;
import com.viettel.csp.util.ParamUtils;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.hadoop.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author GEM
 */
public class MessageServiceImpl implements MessageService {

    @Autowired
    private MessageDAO messageDAO;

    @Autowired
    private UserService userService;

    public MessageDAO getMessageDAO() {
        return messageDAO;
    }

    public void setMessageDAO(MessageDAO messageDAO) {
        this.messageDAO = messageDAO;
    }

    public UserService getUserService() {
        return userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void insert() throws Exception {
        messageDAO.insert();
    }
    
    @Override
    public void insert(String mesg) throws Exception {
        try {
            messageDAO.insert(mesg);
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public void insertMessage(MessageEntity messageEntity) {
        messageDAO.insert(messageEntity);
    }

    public List<MessageEntity> getEntityListByReceiveID(Long receiveUserId) {
        List<ConditionBean> list = new ArrayList<>();
        list.add(new ConditionBean("RECEIVE_USER_ID", " = ", String.valueOf(receiveUserId), ParamUtils.TYPE_NUMBER));
        List<MessageEntity> lst = messageDAO.find("MessageEntity", list);
        return lst;
    }

    public List<NotifyDTO> getNotifyList(Long receiveUserId) {
        List<MessageEntity> lst = getEntityListByReceiveID(receiveUserId);
        return convertToDTO(lst);
    }

    private List<NotifyDTO> convertToDTO(List<MessageEntity> lst) {
        List<NotifyDTO> notifyList = new ArrayList<NotifyDTO>();
        if (lst != null && lst.size() > 0) {
            UserEntity receiver = userService.getUserByID(lst.get(0).getReceiveUserId());
            for (MessageEntity entity : lst) {
                NotifyDTO dto = new NotifyDTO();
                dto.setContent(entity.getContent());
                dto.setCreateAt(entity.getCreatAt());
                dto.setDescription(entity.getDescription());
                dto.setStatus(entity.getStatus());
                UserEntity sender = userService.getUserByID(entity.getSendUserId());
                dto.setSender(sender.getUserName());
                dto.setId(entity.getId());
                dto.setCreateBy(entity.getCreateBy());
                dto.setSenderID(entity.getSendUserId());
                dto.setReceiverID(entity.getReceiveUserId());
                dto.setReceiver(receiver.getUserName());
                notifyList.add(dto);
            }
        }
        return notifyList;
    }

    private List<MessageEntity> convertToEntity(List<NotifyDTO> lst) {
        List<MessageEntity> entity = new ArrayList<>();
        for (NotifyDTO dto : lst) {
            MessageEntity en = new MessageEntity();
            en.setId(dto.getId()); // demo
            en.setSendUserId(dto.getSenderID());
            en.setReceiveUserId(dto.getReceiverID());
            en.setContent(dto.getContent());
            en.setCreatAt(dto.getCreateAt());
            en.setStatus(dto.getStatus());
            en.setDescription(dto.getDescription());
            en.setCreateBy(dto.getCreateBy());
            entity.add(en);
        }

        return entity;
    }

    public String updateStatus(List<NotifyDTO> lst) {
        try {
            List<MessageEntity> entity = convertToEntity(lst);
            for (int i = 0; i < entity.size(); i++) {
                messageDAO.update(entity.get(i));
            }
            return "success";
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return "";
    }
}
