/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.viettel.csp.service;

import com.viettel.csp.DTO.ReasonDTO;
import com.viettel.csp.util.KeyValueBean;
import java.util.List;

/**
 *
 * @author TienNV@gemvietnam.com
 */
public interface ReasonService {
    List<KeyValueBean> getListToKeyValueBean(Boolean isChoose) throws Exception;

    List<KeyValueBean> getListToKeyValueBeanByType(String type) throws Exception;
    
    List<ReasonDTO> findListByType(String type) throws Exception;

    List<ReasonDTO> findAll();

    List<ReasonDTO> search(ReasonDTO reasonDTO);

    void save(ReasonDTO reasonDTO);

    void update(ReasonDTO reasonDTO);

    void delete(List<ReasonDTO> dtoList);

}
