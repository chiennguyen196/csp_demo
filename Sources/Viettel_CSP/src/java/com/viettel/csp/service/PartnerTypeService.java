/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.viettel.csp.service;

import com.viettel.csp.DTO.ContactTypeDTO;
import com.viettel.csp.util.KeyValueBean;
import java.util.List;

/**
 *
 * @author tiennv02
 */
public interface PartnerTypeService {
    public List<KeyValueBean> getListToKeyValueBean(Boolean isChoose) throws Exception;
    
    public List<ContactTypeDTO> findAll() throws Exception;
}
