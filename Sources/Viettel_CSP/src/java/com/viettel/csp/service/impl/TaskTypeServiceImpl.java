package com.viettel.csp.service.impl;

import com.viettel.csp.DAO.TaskTypeDAO;
import com.viettel.csp.DTO.TaskTypeDTO;
import com.viettel.csp.entity.TaskTypeEntity;
import com.viettel.csp.service.TaskTypeService;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.util.SendMailUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class TaskTypeServiceImpl extends BaseCustomServiceImpl<TaskTypeDAO, TaskTypeEntity, TaskTypeDTO> implements TaskTypeService {
    @Autowired
    TaskTypeDAO taskTypeDAO;

    public TaskTypeDAO getTaskTypeDAO() {
        return taskTypeDAO;
    }

    public void setTaskTypeDAO(TaskTypeDAO taskTypeDAO) {
        this.taskTypeDAO = taskTypeDAO;
    }

    @Override
    public List<TaskTypeEntity> findAlles() throws Exception {
        return this.taskTypeDAO.findAll();
    }

    @Override
    public List<TaskTypeEntity> findAllWithoutInTask(Long contractId) throws Exception {
        return this.taskTypeDAO.findAllWithoutInTask(contractId);
    }

    @Override
    public TaskTypeEntity insert(TaskTypeEntity tBO) {
        return taskTypeDAO.insert(tBO);
    }
}
