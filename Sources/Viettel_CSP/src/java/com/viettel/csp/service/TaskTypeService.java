package com.viettel.csp.service;

import com.viettel.csp.entity.TaskTypeEntity;

import java.util.List;

public interface TaskTypeService {
    List<TaskTypeEntity> findAlles() throws Exception;

    List<TaskTypeEntity> findAllWithoutInTask(Long contractId) throws Exception;

    public TaskTypeEntity insert(TaskTypeEntity tBO) throws Exception;
}
