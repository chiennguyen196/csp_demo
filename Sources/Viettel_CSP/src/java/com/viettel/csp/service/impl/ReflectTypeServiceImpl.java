/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.service.impl;

import com.viettel.csp.DAO.ReflectTypeDAO;
import com.viettel.csp.DTO.ReflectTypeDTO;
import com.viettel.csp.entity.ReflectTypeEntity;
import com.viettel.csp.service.ReflectTypeService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author admin
 */
public class ReflectTypeServiceImpl extends BaseCustomServiceImpl<ReflectTypeDAO, ReflectTypeEntity, ReflectTypeDTO> implements ReflectTypeService {

    @Autowired
    private ReflectTypeDAO reflectTypeDAO;

    public ReflectTypeDAO getReflectTypeDAO() {
        return reflectTypeDAO;
    }

    public void setReflectTypeDAO(ReflectTypeDAO reflectTypeDAO) {
        super.set(locale, new ReflectTypeEntity(), reflectTypeDAO, ReflectTypeDTO.class);
        this.reflectTypeDAO = reflectTypeDAO;
    }

}
