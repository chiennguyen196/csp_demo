/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.service.impl;

import com.viettel.csp.DAO.DisciplineDAO;
import com.viettel.csp.DTO.DisciplineDTO;
import com.viettel.csp.entity.DisciplineEntity;
import com.viettel.csp.service.DisciplineService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author GEM
 */
public class DisciplineServiceImpl extends BaseCustomServiceImpl<DisciplineDAO, DisciplineEntity, DisciplineDTO> implements DisciplineService {

    @Autowired
    private DisciplineDAO disciplineDAO;

    public DisciplineDAO getDisciplineDAO() {
        return disciplineDAO;
    }

    public void setDisciplineDAO(DisciplineDAO disciplineDAO) {
        this.disciplineDAO = disciplineDAO;
    }



    @Override
    public List<DisciplineEntity> findAllDiscipline() throws Exception {
        return  disciplineDAO.findAll();
    }

}
