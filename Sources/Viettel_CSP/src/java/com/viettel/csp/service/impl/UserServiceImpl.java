/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.service.impl;

import com.viettel.csp.DAO.DepartmentDAO;
import com.viettel.csp.DAO.UserDAO;
import com.viettel.csp.DTO.ContactDTO;
import com.viettel.csp.DTO.DepartmentDTO;
import com.viettel.csp.DTO.PartnerDTO;
import com.viettel.csp.DTO.UserDTO;
import com.viettel.csp.entity.UserEntity;
import com.viettel.csp.service.UserService;
import com.viettel.csp.util.*;
import org.springframework.beans.factory.annotation.Autowired;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.*;

/**
 * @author GEM
 */
public class UserServiceImpl extends BaseCustomServiceImpl<UserDAO, UserEntity, UserDTO> implements UserService {

    @Autowired
    private UserDAO userDAO;
    @Autowired
    private DepartmentDAO departmentDAO;

    public DepartmentDAO getDepartmentDAO() {
        return departmentDAO;
    }

    public void setDepartmentDAO(DepartmentDAO departmentDAO) {
        this.departmentDAO = departmentDAO;
    }

    public UserDAO getUserDAO() {
        return userDAO;
    }

    public void setUserDAO(UserDAO userDAO) {
        super.set(locale, new UserEntity(), userDAO, UserDTO.class);
        this.userDAO = userDAO;
    }

    @Override
    public void deleteUser(List<Long> lstPersonInfoId, String personType) throws Exception {
        for (int i = 0; i < lstPersonInfoId.size(); i++) {
            userDAO.fillByPersonInfoId(lstPersonInfoId.get(i), personType);
        }
    }

    @Override
    public List<UserDTO> checkLogin(UserDTO udto) {
        List<ConditionBean> lstCondition = new ArrayList<>();
        lstCondition.add(new ConditionBean("username", "=", udto.getUserName(), ParamUtils.DataType.STRING));
        return find("UserEntity", lstCondition);
    }

    @Override
    public List<UserDTO> findUserByOrgzanition(Long orgId) {
        List<ConditionBean> lstCondition = new ArrayList<>();
        lstCondition.add(new ConditionBean("organizationId", "=", String.valueOf(orgId), ParamUtils.DataType.LONG));
        return find("UserEntity", lstCondition);
    }

    @Override
    public UserEntity updateUser(UserEntity userEntity) {
        return userDAO.update(userEntity);
    }

    @Override
    public UserEntity insert(UserEntity userEntity) {
        return userDAO.insert(userEntity);
    }

    @Override
    public UserEntity checkEntity(UserEntity userEntity) {
        return userDAO.checkEntity(userEntity);
    }

    @Override
    public UserEntity sendEmail(UserEntity userEntity) {
        UserEntity userEntity1 = new UserEntity();
        try {
            String host = "smtp.gmail.com";
            String user = "csp.viettel123@gmail.com";
            String pass = "Viettel123";
            String from = "cps.viettel123@gmail.com";
            String subject = "This is confirmation number for your expertprogramming account. Please insert this number to activate your account.";
            UserEntity checkEntity = checkEntity(userEntity);
            
            String messageText = "" + checkEntity.getVerifyCode();
            boolean sessionDebug = false;
            Properties props = System.getProperties();
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", host);
            props.put("mail.smtp.port", "587");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.required", "true");
            //userEntity.setUserName(userEntity.getUserName());
            //UserEntity checkEntity = checkEntity(userEntity);
            checkEntity.setVerifyCode(messageText);
            if (null != checkEntity) {
                System.out.println("" + messageText);
                //updateUser(checkEntity);
                System.out.println("da xong");
                java.security.Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
                Session mailSession = Session.getDefaultInstance(props, null);
                mailSession.setDebug(sessionDebug);
                Message msg = new MimeMessage(mailSession);
                msg.setFrom(new InternetAddress(from));
                InternetAddress[] address = {new InternetAddress(checkEntity.getEmail())};
                msg.setRecipients(Message.RecipientType.TO, address);
                msg.setSubject(subject);
                msg.setSentDate(new Date());
                msg.setText(messageText);

                Transport transport = mailSession.getTransport("smtp");
                
                transport.connect(host, user, pass);
                transport.sendMessage(msg, msg.getAllRecipients());
                transport.close();
                System.out.println("message send successfully");
                userEntity1 = checkEntity;
            }

        } catch (Exception ex) {
            System.out.println(ex);
        }
        return userEntity1;
    }

    public UserDTO getUserDTOByUserName(String username) {
        List<ConditionBean> list = new ArrayList<>();
        list.add(new ConditionBean("username", " = ", username, ParamUtils.TYPE_STRING));
        //  List<UserEntity> lst = findEntity("UserEntity", list);
        List<UserDTO> lst = find("UserEntity", list);
        return lst.get(0);
    }

    public UserEntity getUserEntityByUserName(String username) {
        List<ConditionBean> list = new ArrayList<>();
        list.add(new ConditionBean("username", " = ", username, ParamUtils.TYPE_STRING));
        List<UserEntity> lst = findEntity("UserEntity", list);
        return lst.get(0);
    }

    @Override
    public String updatePasswordUser(String username, String newPassword, String oldPassword) {
        UserEntity user = getUserEntityByUserName(username);
        String uid = user.getRandomKey();

        oldPassword = EncryptionUtils.sha256WithSalt(oldPassword, uid);
        if (!oldPassword.equals(user.getPassword())) {
            return "error";
        }

        newPassword = EncryptionUtils.sha256WithSalt(newPassword, uid);
        user.setPassword(newPassword);

        userDAO.update(user);
        return "success";
    }

    public UserEntity getUserByID(Long id) {
        return userDAO.findById(id);
    }

    //    @Overrid
//    public String getEncryptedPassword(String password, String randomKey) {
//        try {
//            MessageDigest md = MessageDigest.getInstance("SHA-256");
//            md.update(password.getBytes());
//            return new sun.misc.BASE64Encoder().encode(md.digest());
//        } catch (NoSuchAlgorithmException e) {
//            System.out.println(e.getMessage());
//        }
//        return "error";
//    }
    @Override
    public String genUID() {
        return UUID.randomUUID().toString();
    }

    @Override
    public boolean existedEmail(String email) {
        return userDAO.checkExistEmail(email);
    }

    @Override
    public boolean existedUsername(String username) {
        return userDAO.checkExistUserName(username);
    }

    @Override
    public boolean existedMobileNumber(String mobileNumber) {
        return userDAO.checkExistMobileNumber(mobileNumber);
    }

    @Override
    public List<UserDTO> fillByPersonType(String personType, String search) {
        return userDAO.fillByPersonType(personType, search);
    }

    @Override
    public List<UserEntity> fillByUsername(String fullname) {
        return userDAO.fillByUsername(fullname);
    }

    @Override
    public List<UserEntity> fillByFullname(String viettel) {
        return null;
    }

    @Override
    public List<UserEntity> fillByUserId(String fullName) {
        return null;
    }

    @Autowired
    public void updateUserByPartner(PartnerDTO newPartnerDTO) {
        UserEntity userEntity = getUserByID(UserTokenUtils.getUserId());
        userEntity.setCurrentAddress(newPartnerDTO.getAddressOffice());
        userEntity.setEmail(newPartnerDTO.getEmail());
        userEntity.setTelephoneNumber(newPartnerDTO.getPhone());
        userEntity.setMobileNumber(newPartnerDTO.getPhone());
        userEntity.setFullName(newPartnerDTO.getCompanyName());
        userDAO.update(userEntity);
    }

    @Override
    public List<UserEntity> findAllEntities() {
        return userDAO.findAll();
    }

    @Override
    public List<UserDTO> findAllEntitiesWithPersonType(String personType) {
        return userDAO.findAllEntitiesWithPersonType(personType);
    }

    @Autowired
    public void updateUserByContact(ContactDTO contactDTO) {
        UserEntity userEntity = getUserByID(UserTokenUtils.getUserId());
        userEntity.setEmail(contactDTO.getEmail());
        userEntity.setTelephoneNumber(contactDTO.getFixedPhoneNumber());
        userEntity.setMobileNumber(contactDTO.getMobilePhoneNumber());
        userEntity.setFullName(contactDTO.getName());
        userEntity.setGender(contactDTO.getSex());
        userDAO.update(userEntity);
    }

    @Autowired
    public DepartmentDTO getDepartmentByOrganizationId() {
        UserEntity user = this.userDAO.findById(UserTokenUtils.getUserId());
        return new ModelMapperUtil().mapper(departmentDAO.getByOrganizationId(user.getOrganizationId()), DepartmentDTO.class);
    }
}
