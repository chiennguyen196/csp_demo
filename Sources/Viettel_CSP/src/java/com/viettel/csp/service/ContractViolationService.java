package com.viettel.csp.service;

import com.viettel.csp.DTO.ContractBreachDTO;
import com.viettel.csp.DTO.ContractDTO;
import com.viettel.csp.util.KeyValueBean;

import java.util.List;

/**
 * Created by GEM on 6/15/2017.
 */
public interface ContractViolationService {
    List<KeyValueBean> getListToKeyValueBeanByType(List<ContractDTO> contractDTOs);

    public List<ContractDTO> fillContractListByStatus(List<ContractDTO> contractDTOList, int statusNumber);

    List<KeyValueBean> bindDataToCombobox(List<String> lst);

    public void insert(ContractBreachDTO contractBreachDTO);

    public ContractBreachDTO getContractBreachByContractId(Long partnerId);

    public boolean checkExistByContractId(Long contractId);

    public void update(ContractBreachDTO contractBreachDTO);
}
