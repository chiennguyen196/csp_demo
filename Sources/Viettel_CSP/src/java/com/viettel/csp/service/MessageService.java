/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.service;

import com.viettel.csp.DTO.NotifyDTO;
import com.viettel.csp.entity.MessageEntity;
import java.util.List;

/**
 *
 * @author GEM
 */
public interface MessageService {

    public void insert() throws Exception;
    
    public void insert(String mesg) throws Exception;

    public void insertMessage(MessageEntity messageEntity);

    public List<MessageEntity> getEntityListByReceiveID(Long receiveUserId);

    public List<NotifyDTO> getNotifyList(Long receiveUserId);

    public String updateStatus(List<NotifyDTO> lst);

}
