/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.service.impl;

import com.viettel.csp.DAO.ContractDAO;
import com.viettel.csp.DAO.ContractHisDAO;
import com.viettel.csp.DTO.ContractHisDTO;
import com.viettel.csp.entity.ContractHisEntity;
import com.viettel.csp.service.ContractHisService;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.ParamUtils;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author GEM
 */
public class ContractHisServiceImpl extends BaseCustomServiceImpl<ContractHisDAO, ContractHisEntity, ContractHisDTO> implements ContractHisService {

    @Autowired
    private ContractHisDAO contractHisDAO;
    @Autowired
    private ContractDAO contractDAO;

    public ContractHisDAO getContractHisDAO() {
        return contractHisDAO;
    }

    public void setContractHisDAO(ContractHisDAO contractHisDAO) {
        this.contractHisDAO = contractHisDAO;
    }

    public ContractDAO getContractDAO() {
        return contractDAO;
    }

    public void setContractDAO(ContractDAO contractDAO) {
        this.contractDAO = contractDAO;
    }

    @Override
    public List<ContractHisDTO> getListContractHis(ContractHisDTO contractHisDTO) {
        return contractHisDAO.getListContractHis(contractHisDTO);
    }

    @Override
    public String insert(Long contractId, ContractHisDTO contractHisDTO) throws Exception {
        String message = ParamUtils.SUCCESS;
        ContractHisEntity contractHisEntity = null;
        try {
            contractHisEntity = new ContractHisEntity();
            //Create object Contrract
            contractHisEntity = ContractHisEntity.setEntity(contractId, contractHisDTO, contractHisEntity);
            contractHisDAO.insert(contractHisEntity);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            message = LanguageBundleUtils.getString("global.message.error");
        }
        return message;
    }

    @Override
    public String insertSendContract(Long contractId, ContractHisDTO contractHisDTO) throws Exception {
        String message = ParamUtils.SUCCESS;
        ContractHisEntity contractHisEntity = null;
        try {
            contractHisEntity = new ContractHisEntity();
            //Create object Contrract
            contractHisEntity = ContractHisEntity.setEntitySendContract(contractId, contractHisDTO, contractHisEntity);
            contractHisDAO.insert(contractHisEntity);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            message = LanguageBundleUtils.getString("global.message.error");
        }
        return message;
    }

    @Override
    public List<ContractHisDTO> getListContractHisAddUser(ContractHisDTO contractHisDTO) throws HibernateException {
        return contractHisDAO.getListContractHisAddUser(contractHisDTO);
    }

    @Override
    public void insertEntity(ContractHisEntity contractHis) throws Exception {
        contractHisDAO.insert(contractHis);
    }

    @Override
    public String insertResonCancleRegisterLiquidation(Long contractId, ContractHisDTO ContractHisDto) {
        String message = ParamUtils.SUCCESS;
        ContractHisEntity contractHisEntity = null;
        try {
            contractHisEntity = new ContractHisEntity();
            //Create object Contrract
            contractHisEntity = ContractHisEntity.setEntityResonCancleRegisterLiquidation(contractId, ContractHisDto, contractHisEntity);
            contractHisDAO.insert(contractHisEntity);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            message = LanguageBundleUtils.getString("global.message.error");
        }
        return message;
    }

    @Override
    public String insertResonCancleAssignLiquidation(Long contractId, ContractHisDTO ContractHisDto) {
        String message = ParamUtils.SUCCESS;
        ContractHisEntity contractHisEntity = null;
        try {
            contractHisEntity = new ContractHisEntity();
            //Create object Contrract
            contractHisEntity = ContractHisEntity.setEntityResonCancleAssignLiquidation(contractId, ContractHisDto, contractHisEntity);
            contractHisDAO.insert(contractHisEntity);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            message = LanguageBundleUtils.getString("global.message.error");
        }
        return message;
    }

    @Override
    public String insertResonCanclePartnerRegister(Long contractId, ContractHisDTO ContractHisDto) {
        String message = ParamUtils.SUCCESS;
        ContractHisEntity contractHisEntity = null;
        try {
            contractHisEntity = new ContractHisEntity();
            //Create object Contrract
            contractHisEntity = ContractHisEntity.setEntityResonCanclePartnerRegister(contractId, ContractHisDto, contractHisEntity);
            contractHisDAO.insert(contractHisEntity);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            message = LanguageBundleUtils.getString("global.message.error");
        }
        return message;
    }
}
