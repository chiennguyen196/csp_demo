/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.service;

import com.viettel.csp.DTO.ServiceDTO;
import com.viettel.csp.entity.ServiceEntity;
import com.viettel.csp.util.KeyValueBean;
import java.util.List;

/**
 *
 * @author admin
 */
public interface ServiceService {

    public List<KeyValueBean> getListToKeyValueBean(Boolean isChoose) throws Exception;

    public List<ServiceDTO> findAll();

//    public List<ServiceEntity> finAllServices();

    public List<ServiceDTO> getList();

    public List<ServiceDTO> getListServiceByDTO(ServiceDTO serviceDTO);

    public ServiceEntity getServiceEntityByCode(String code);

    public ServiceDTO insertService(ServiceDTO serviceDTO);

    public ServiceEntity findServiceById(Long serviceId);

    public List<ServiceDTO> getListType();

    public List<ServiceDTO> getListWithNullParent();
    
    public List<ServiceDTO> getServiceByCode(String code);

    public List<ServiceDTO> getListServiceChild() throws  Exception;

    public void updateService(ServiceDTO serviceDTO);

    public boolean insert(ServiceDTO serviceDTO);

    public String deleteService(List<ServiceDTO> serviceDTOS);

    public List<KeyValueBean> listParrentCode();

    public List<ServiceDTO> getListChilService(String parentCode);
}
