package com.viettel.csp.service;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.viettel.csp.DTO.ContactTypeDTO;
import com.viettel.csp.util.KeyValueBean;
import java.util.List;

/**
 * @author TienNV
 */
public interface ContactTypeService {

    public List<KeyValueBean> getListToKeyValueBean(Boolean isChoose);

    public List<ContactTypeDTO> findAll();
}
