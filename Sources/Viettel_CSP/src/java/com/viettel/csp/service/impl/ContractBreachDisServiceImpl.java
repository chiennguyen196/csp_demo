package com.viettel.csp.service.impl;

import com.viettel.csp.DAO.ContractBreachDisDAO;
import com.viettel.csp.DTO.ContractBreachDisDTO;
import com.viettel.csp.entity.ContractBreachDisEntity;
import com.viettel.csp.entity.ContractBreachEntity;
import com.viettel.csp.service.ContractBreachDisService;
import com.viettel.csp.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.zkoss.zul.Combobox;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by GEM on 6/17/2017.
 */
public class ContractBreachDisServiceImpl extends BaseCustomServiceImpl<ContractBreachDisDAO, ContractBreachDisEntity, ContractBreachDisDTO> implements ContractBreachDisService {
    @Autowired
    private ContractBreachDisDAO contractBreachDisDAO;

    public ContractBreachDisDAO getContractBreachDisDAO() {
        return contractBreachDisDAO;
    }

    public void setContractBreachDisDAO(ContractBreachDisDAO contractBreachDisDAO) {
        super.set(this.locale, new ContractBreachDisEntity(), contractBreachDisDAO, ContractBreachDisDTO.class);
        this.contractBreachDisDAO = contractBreachDisDAO;
    }

    public ContractBreachDisDTO createBreachDisDTO(int key, Date startDate, Date endDate, String content, String contentValue, String headOfService, long contractId){
        ContractBreachDisDTO contractBreachDisDTO = new ContractBreachDisDTO();
        contractBreachDisDTO = setValue(key, contractBreachDisDTO, startDate, endDate, content, contentValue , headOfService, contractId);

        return contractBreachDisDTO;
    }

    public ContractBreachDisDTO setValue(int key, ContractBreachDisDTO contractBreachDisDTO,
                                          Date startDate, Date endDate, String content, String contentValue ,String headOfService, long contractId){
        contractBreachDisDTO.setContractBreachId(contractId);
        contractBreachDisDTO.setHeadOfService(headOfService);
        String nameViolate = "";
        switch (key){
            case 1:
                nameViolate = LanguageBundleUtils.getString("partner.violation.blockNumber");
                contractBreachDisDTO.setPauseStartDate(startDate);
                contractBreachDisDTO.setPauseEndDate(endDate);
                contractBreachDisDTO.setDisCode("1");
                break;
            case 2:
                nameViolate = LanguageBundleUtils.getString("partner.violation.stopService");
                contractBreachDisDTO.setPauseStartDate(startDate);
                contractBreachDisDTO.setPauseEndDate(endDate);
                contractBreachDisDTO.setDisCode("2");
                break;
            case 3:
                nameViolate = LanguageBundleUtils.getString("partner.violation.stopContract");
                contractBreachDisDTO.setPauseStartDate(startDate);
                contractBreachDisDTO.setPauseEndDate(endDate);
                contractBreachDisDTO.setDisCode("3");
                break;
            case 4:
                nameViolate = LanguageBundleUtils.getString("partner.violation.dropContract");
                contractBreachDisDTO.setDateOff(startDate);
                contractBreachDisDTO.setPauseStartDate(startDate);
                contractBreachDisDTO.setDisCode("4");
                break;
            case 5:
                nameViolate = LanguageBundleUtils.getString("partner.violation.money");
                contractBreachDisDTO.setContent(content);
                contractBreachDisDTO.setContentValue(contentValue);
                contractBreachDisDTO.setDisCode("5");
                break;
            case 6:
                nameViolate = LanguageBundleUtils.getString("partner.violation.notify");
                contractBreachDisDTO.setDoc(content);
                contractBreachDisDTO.setViolationNumber("1");//So lan
                contractBreachDisDTO.setDisCode("6");
                break;
        }
        contractBreachDisDTO.setViolationName(nameViolate);
        return contractBreachDisDTO;
    }

    public List<ContractBreachDisDTO> getBreachContractId(long contractId){
        List<ContractBreachDisDTO> lst = find("ContractBreachDisEntity", new ConditionBean("CONTRACT_BREACH_ID", "=", StringUtils.getString(contractId), ParamUtils.DataType.LONG));
        if (lst == null || lst.size() == 0)
            return null;

        return lst;
    }

    public void insert(ContractBreachDisDTO contractBreachDisDTO){
        contractBreachDisDAO.insert(convertToEntity(contractBreachDisDTO));
    }

    public void insertDataList(List<ContractBreachDisDTO> contractBreachDisDTOs){
       for (int i = 0; i < contractBreachDisDTOs.size(); i++){
           insert(contractBreachDisDTOs.get(i));
       }
    }

    private ContractBreachDisEntity convertToEntity(ContractBreachDisDTO contractBreachDisDTO){
        ContractBreachDisEntity entity = new ContractBreachDisEntity();
        entity.setDisCode(contractBreachDisDTO.getDisCode());
        entity.setContentValue(contractBreachDisDTO.getContentValue());
        entity.setContent(contractBreachDisDTO.getContent());
        entity.setPauseStartDate(contractBreachDisDTO.getPauseStartDate());
        entity.setPauseEndDate(contractBreachDisDTO.getPauseEndDate());
        entity.setContent(contractBreachDisDTO.getContent());
        entity.setContractBreachId(contractBreachDisDTO.getContractBreachId());

        return entity;
    }

    public List<KeyValueBean> getListToKeyValueBeanByType(ContractBreachDisDTO dto, String value) {
        List<KeyValueBean> listKeyValueBean = new ArrayList<>();
        KeyValueBean kvb = new KeyValueBean(dto.getDisCode(), value);
        listKeyValueBean.add(kvb);
        return listKeyValueBean;
    }

}
