/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.service.impl;

import com.viettel.csp.DAO.ContractDAO;
import com.viettel.csp.DAO.ContractFilesDAO;
import com.viettel.csp.DAO.ContractHisDAO;
import com.viettel.csp.DAO.DepartmentDAO;
import com.viettel.csp.DAO.UserDAO;
import com.viettel.csp.DTO.ContactPointDTO;
import com.viettel.csp.DTO.ContractDTO;
import com.viettel.csp.DTO.ContractFilesDTO;
import com.viettel.csp.DTO.ContractHisDTO;
import com.viettel.csp.constant.ContractStatus;
import com.viettel.csp.entity.ContactPointEntity;
import com.viettel.csp.entity.ContractEntity;
import com.viettel.csp.entity.ContractHisEntity;
import com.viettel.csp.entity.DepartmentEntity;
import com.viettel.csp.entity.UserEntity;
import com.viettel.csp.service.ContactPointService;
import com.viettel.csp.service.ContractFilesService;
import com.viettel.csp.service.ContractHisService;
import com.viettel.csp.service.ContractService;
import com.viettel.csp.util.*;
import com.viettel.csp.util.ParamUtils.CONTRACT_TYPE;
import com.viettel.logincsp.UserCredential;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.zkoss.zk.ui.Sessions;


/**
 * @author GEM
 */

public class ContractServiceImpl extends
    BaseCustomServiceImpl<ContractDAO, ContractEntity, ContractDTO> implements ContractService {


    @Autowired
    private ContractDAO contractDAO;
    @Autowired
    private DepartmentDAO departmentDAO;
    @Autowired
    private UserDAO userDAO;
    @Autowired
    private ContactPointService contactPointService;
    @Autowired
    private ContractFilesService contractFilesService;
    @Autowired
    private ContractHisService contractHisService;
    @Autowired
    private ContractHisDAO contractHisDAO;
    @Autowired
    private ContractFilesDAO contractFilesDAO;

    public ContractHisDAO getContractHisDAO() {
        return contractHisDAO;
    }

    public void setContractHisDAO(ContractHisDAO contractHisDAO) {
        this.contractHisDAO = contractHisDAO;
    }

    public ContractFilesDAO getContractFilesDAO() {
        return contractFilesDAO;
    }

    public void setContractFilesDAO(ContractFilesDAO contractFilesDAO) {
        this.contractFilesDAO = contractFilesDAO;
    }


    public ContractHisService getContractHisService() {
        return contractHisService;
    }

    public void setContractHisService(ContractHisService contractHisService) {
        this.contractHisService = contractHisService;
    }

    public ContractFilesService getContractFilesService() {
        return contractFilesService;
    }

    public void setContractFilesService(ContractFilesService contractFilesService) {
        this.contractFilesService = contractFilesService;
    }

    public ContactPointService getContactPointService() {
        return contactPointService;
    }

    public void setContactPointService(ContactPointService contactPointService) {
        this.contactPointService = contactPointService;
    }

    public DepartmentDAO getDepartmentDAO() {
        return this.departmentDAO;
    }

    public void setDepartmentDAO(DepartmentDAO departmentDAO) {
        this.departmentDAO = departmentDAO;
    }

    public UserDAO getUserDAO() {
        return this.userDAO;
    }

    public void setUserDAO(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    public ContractDAO getContractDAO() {
        return this.contractDAO;
    }

    public void setContractDAO(ContractDAO contractDAO) {
        this.contractDAO = contractDAO;
    }

//    @Override

    public List<ContractDTO> getListContractForPartnerLock(ContractDTO contractDTO){
        List<ContractDTO> lstResult = contractDAO.getListContract(contractDTO, "CONTRACT");
        return fillByPartnerID(lstResult);
    }

    private List<ContractDTO> fillByPartnerID(List<ContractDTO> lstResult) {
        Long partnerID = (Long) Sessions.getCurrent().getAttribute("partnerID");
        List<ContractDTO> list = new ArrayList<>();
        for (ContractDTO contractDTO : lstResult) {
            if (contractDTO.getPartnerId().equals(partnerID)) {
                list.add(contractDTO);
            }
        }
        return list;
    }

    public List<ContractDTO> getListContract(ContractDTO contractDTO) {
        return contractDAO.getListContract(contractDTO, "CONTRACT");
    }

    @Override
    public List<ContractDTO> getListContract(ContractDTO contractDTO, UserCredential credential) {
        List<ContractDTO> result = this.contractDAO.getListContract(contractDTO, "CONTRACT");
        UserEntity user = this.userDAO.findById(credential.getUserId());
        DepartmentEntity department = this.departmentDAO
            .getByOrganizationId(user.getOrganizationId());
        if (department.getCode().equals(ParamUtils.DEPARTMENT_VIETTEL_CSP)) {
            return result;
        } else {
            List<ContractDTO> afterFillter = new ArrayList<>();
            result.forEach((contract) ->
            {
                if (user.getId().equals(contract.getSignCtCompleteViettel())
                    || user.getId().equals(contract.getSignCtExpertiseLaw())
                    || (contract.getPartnerId() != null && contract.getPartnerId()
                    .equals(credential.getPartnerId()))) {
                    afterFillter.add(contract);
                }
            });
            return afterFillter;
        }
    }

    @Override
    public List<ContractDTO> getListLiquidation(ContractDTO contractDTO) {
        return contractDAO.getListLiquidation(contractDTO);
    }

    public String insertContractAndContactPoint(ContractDTO contractDTO,
        ContactPointDTO contactPointDTO, ContractHisDTO contractHisDTO) throws Exception {
        String message = ParamUtils.SUCCESS;
        ContractEntity contractEntity = null;
        ContactPointEntity pointEntity = null;
        ContractHisEntity contractHisEntity = null;
        try {
            contractEntity = new ContractEntity();
            //Create object Contrract
            contractEntity = ContractEntity.setEntityInsertStatus(contractDTO, contractEntity,
                ParamUtils.CONTRACT_PARTNER.CONTRACTSENDPARTNER);
            //Save Contract
            contractDAO.insert(contractEntity);
            Long id = contractEntity.getId();

            if (contractDTO.getLstContractFilesDTO().size() > 0) {
                for (ContractFilesDTO dto : contractDTO.getLstContractFilesDTO()) {
                    dto.setContractId(id);
                    contractFilesService
                        .insertFilesContactMenu(dto, ParamUtils.CONTRACT_PARTNER.CONTRACTFILESNAME);
                }
            }
            contractHisDTO.setContractId(id);
            contractHisService.insertSendContract(id, contractHisDTO);

            if (contractDTO.getLstContractFilesDTOMenu().size() > 0) {
                for (ContractFilesDTO contractFilesDTO : contractDTO.getLstContractFilesDTOMenu()) {
                    contractFilesDTO.setContractId(id);
                    contractFilesService.insertFilesContactMenu(contractFilesDTO,
                        ParamUtils.CONTRACT_PARTNER.CONTRACTFILESNAMEMENU);
                }
            }
            for (ContactPointDTO pointDTO : contactPointDTO.getListContactPointDTO()) {
                pointDTO.setContractId(id);
                contactPointService.saveOrUpdate(pointDTO);
            }
            //Send mail and notification
            SendMailUtils.sendMailAndNotification(null, null, null, null);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            message = LanguageBundleUtils.getString("global.message.error");
        }
        return message;
    }


    @Override
    public long updateCreatLiquidation(ContractDTO contractDTO, String action, String liStatus) {
        ContractEntity contractEntity = null;
        switch (action) {
            case ParamUtils.ACTION_CREATE:
                contractEntity = new ContractEntity();
                break;
            case ParamUtils.ACTION_UPDATE:
                contractEntity = this.contractDAO.find("ContractEntity",
                    new ConditionBean("id", "=", String.valueOf(contractDTO.getContractId()),
                        ParamUtils.DataType.LONG)).get(0);
                break;
        }
        //Create object Contrract
        contractEntity = ContractEntity
            .setEntityLiquidationCreat(contractDTO, contractEntity, liStatus);
        //Save Contract
        this.contractDAO.saveOrUpdate(contractEntity);
        long id = contractEntity.getId();
        //Send mail and notification
        SendMailUtils.sendMailAndNotification(null, null, null, null);

        return id;
    }


    @Override
    public String update(ContractDTO contractDTO, String action, int flat) {
        String message = ParamUtils.SUCCESS;
        ContractEntity contractEntity = null;
        switch (action) {
            case ParamUtils.ACTION_CREATE:
                contractEntity = new ContractEntity();
                break;
            case ParamUtils.ACTION_UPDATE:
                contractEntity = this.contractDAO.find("ContractEntity",
                    new ConditionBean("id", "=", String.valueOf(contractDTO.getContractId()),
                        ParamUtils.DataType.LONG)).get(0);
                break;
        }
        //Create object Contrract
        contractEntity = ContractEntity.setEntity(contractDTO, contractEntity, flat);
        //Save Contract
        this.contractDAO.saveOrUpdate(contractEntity);
        //Send mail and notification
        SendMailUtils.sendMailAndNotification(null, null, null, null);
        return message;
    }

    @Override
    public String updateContractSendPartner(ContractDTO contractDTO, String action, int flat) {
        String message = ParamUtils.SUCCESS;
        ContractEntity contractEntity = null;
        switch (action) {
            case ParamUtils.ACTION_CREATE:
                contractEntity = new ContractEntity();
                break;
            case ParamUtils.ACTION_UPDATE:
                contractEntity = this.contractDAO.find("ContractEntity",
                    new ConditionBean("id", "=", String.valueOf(contractDTO.getContractId()),
                        ParamUtils.DataType.LONG)).get(0);
                break;
        }
        //Create object Contrract
        contractEntity = ContractEntity.setEntityUpdate(contractDTO, contractEntity, flat);
        //Save Contract
        this.contractDAO.saveOrUpdate(contractEntity);
        //Send mail and notification
        SendMailUtils.sendMailAndNotification(null, null, null, null);
        return message;
    }

    @Override
    public boolean checkValidateDelete(Long id) throws Exception {
        throw new UnsupportedOperationException(
            "Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<KeyValueBean> getListContractType() throws Exception {
        throw new UnsupportedOperationException(
            "Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<KeyValueBean> getListContractStatus() throws Exception {
        throw new UnsupportedOperationException(
            "Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<KeyValueBean> getListContractTypeByCode(String partnerTypeCode) throws Exception {
        throw new UnsupportedOperationException(
            "Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<KeyValueBean> getListContractTypeByName(String partnerTypeName) throws Exception {
        throw new UnsupportedOperationException(
            "Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String deleteContract(List<ContractDTO> lstDeleteDTO) {
        String message = ParamUtils.SUCCESS;
        try {
            int total = lstDeleteDTO.size();
            int ok = 0;
            for (ContractDTO contractDTO : lstDeleteDTO) {
                Long contractId = contractDTO.getContractId();
                ContractEntity contractEntity = this.contractDAO.findById(contractId);
                this.contractDAO.delete(contractEntity);
                //Send mail  and notification
                SendMailUtils.sendMailAndNotification(null, null, null, null);
                ok++;
            }
            if (ok == total) {
                message = LanguageBundleUtils.getMessage(
                    LanguageBundleUtils.getString("global.message.delete.multi.successful"),
                    String.valueOf(ok),
                    String.valueOf(total),
                    String.valueOf(total - ok));
            }
        } catch (Exception e) {
            this.logger.error(e.getMessage(), e);
            message = LanguageBundleUtils.getString("global.message.error");
        }
        return message;
    }

    @Override
    public String insert(ContractDTO contractDTO, int flat) throws Exception {
        String message = ParamUtils.SUCCESS;
        ContractEntity contractEntity;
        try {
            contractEntity = new ContractEntity();
            //Create object Contrract
            contractEntity = ContractEntity.setEntityInsert(contractDTO, contractEntity, flat);
            //Save Contract
            ContractEntity contract = this.contractDAO.insert(contractEntity);
            long contractId = contract.getId();
            //Send mail and notification
            SendMailUtils.sendMailAndNotification(null, null, null, null);
        } catch (Exception e) {
            this.logger.error(e.getMessage(), e);
            message = LanguageBundleUtils.getString("global.message.error");
        }
        return message;
    }

    @Override
    public long insertContract(ContractDTO contractDTO, int flat) {
        long contractId = 0;
        ContractEntity contractEntity = null;
        try {
            contractEntity = new ContractEntity();
            //Create object Contrract
            contractEntity = ContractEntity.setEntityInsert(contractDTO, contractEntity, flat);
            //Save Contract
            ContractEntity contract = this.contractDAO.insert(contractEntity);
            contractId = contract.getId();
            //Send mail and notification
            ContractHisEntity hisEntity = new ContractHisEntity(contractId,
                LanguageBundleUtils.getString("message.profile.create.contract.profile"));
            contractHisService.insertEntity(hisEntity);
            SendMailUtils.sendMailAndNotification(null, null, null, null);
        } catch (Exception e) {
            this.logger.error(e.getMessage(), e);
        }
        return contractId;
    }

    @Override
    public List<ContractEntity> getListContractNumber(Long partnerId) throws Exception {
        return contractDAO.getListContractNumber(partnerId);
    }

    public List<ContractDTO> getListContractForWorkManager(ContractDTO partnerDTO)
        throws Exception {
        return contractDAO.getListContractForWorkManager(partnerDTO);
    }

    @Override
    public List<ContractDTO> getListContractFiles(ContractDTO contractDTO) throws Exception {
        return contractDAO.getListContractFiles(contractDTO);
    }


    public void updateContractLock(List<ContractDTO> lstResult, long value) {
        for (int i = 0; i < lstResult.size(); i++) {
            long id = lstResult.get(i).getContractId();
            ContractEntity contractEntity = new ContractEntity();
            contractEntity = contractDAO.findById(id);
            contractEntity.setIsLock(value);
            contractDAO.update(contractEntity);

        }
    }

    @Override
    public boolean signContract(ContractDTO contractDTO) {
        ContractEntity contractEntity = contractDAO.findById(contractDTO.getContractId());
        if (contractEntity != null) {
            contractEntity.setContractStatus(contractDTO.getContractStatus());
            contractEntity.setSignCtCompleteViettel(contractDTO.getSignCtCompleteViettel());
            contractEntity.setSignCtExpertiseLaw(contractDTO.getSignCtExpertiseLaw());
            this.contractDAO.saveOrUpdate(contractEntity);
            return true;
        }
        //Send mail and notification
        SendMailUtils.sendMailAndNotification(null, null, null, null);
        return false;
    }

    @Override
    public boolean rejectContract(ContractDTO contractDTO, ContractHisDTO contractHisDTO) {
        String message = ParamUtils.SUCCESS;
        ContractEntity contractEntity = this.contractDAO.find("ContractEntity",
            new ConditionBean("id", "=", String.valueOf(contractDTO.getContractId()),
                ParamUtils.DataType.LONG)).get(0);
        if (contractEntity != null) {
            long count = contractFilesDAO.markAsDeleteByContractId(contractEntity.getId());
            contractEntity.setContractStatus(String.valueOf(ParamUtils.TYPE_ACTION.NO_ACCEPT));
            contractEntity.setContractNumber(contractDTO.getContractNumber());
            contractEntity.setStartDate(contractDTO.getStartDate());
            contractEntity.setEndDate(contractDTO.getEndDate());
            contractEntity.setServiceCode(contractDTO.getServiceCode());
            contractEntity.setContractType(contractDTO.getContractType());
            contractEntity.setContractType(contractDTO.getContractType());
            contractEntity.setHeadOfService(contractDTO.getHeadOfService());
            contractEntity.setCtSignForm(contractDTO.getCtSignForm());

            //Save Contract
            this.contractDAO.saveOrUpdate(contractEntity);
            //Send mail and notification
            SendMailUtils.sendMailAndNotification(null, null, null, null);
            return true;
        }

        return false;
    }

    @Override
    public List<ContractDTO> getListContractProfile(ContractDTO contractDTO,
        UserCredential credential) {
        List<ContractDTO> result = this.contractDAO.getListContract(contractDTO, "PROFILE");
        UserEntity user = this.userDAO.findById(credential.getUserId());
        DepartmentEntity department = this.departmentDAO
            .getByOrganizationId(user.getOrganizationId());
        if (department != null && ParamUtils.DEPARTMENT_VIETTEL_CSP.equals(department.getCode())) {
            return result;
        } else {
            List<ContractDTO> afterFillter = new ArrayList<>();
            result.forEach((contract) ->
            {
                System.out.println("contract.getPartnerId() " + contract.getPartnerId());
                System.out
                    .println("UserTokenUtils.getPartnerId() " + UserTokenUtils.getPartnerId());
                if (UserTokenUtils.getUserId().equals(contract.getSignCtCompleteViettel())
                    || UserTokenUtils.getUserId().equals(contract.getSignCtCompleteViettel())
                    || (contract.getPartnerId() != null
                    && UserTokenUtils.getPartnerId() != null
                    && contract.getPartnerId().equals(UserTokenUtils.getPartnerId()))) {
                    afterFillter.add(contract);
                }
            });
            return afterFillter;
        }
    }

    public List<ContractDTO> getContractByContractId(List<ContractDTO> list, long contractId) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getContractId() != contractId) {
                list.remove(i);
                i--;
            }
        }

        return list;
    }

    @Override
    public boolean rejectSignContract(ContractDTO contractDTO, ContractHisDTO contractHisDTO) {
        ContractEntity contractEntity = this.contractDAO.find("ContractEntity",
            new ConditionBean("id", "=", String.valueOf(contractDTO.getContractId()),
                ParamUtils.DataType.LONG)).get(0);

        if (contractEntity != null) {
            long count = contractFilesDAO.markAsDeleteByContractId(contractEntity.getId());
            contractEntity.setContractStatus(CONTRACT_TYPE.CONTRACT_TYPE_REJECT_SIGN);

            ContractHisEntity contractHisEntity = ContractHisEntity
                .setEntity(contractEntity.getId(), contractHisDTO, new ContractHisEntity());
            contractHisDAO.insert(contractHisEntity);
            //Save Contract
            this.contractDAO.saveOrUpdate(contractEntity);
            //Send mail and notification
            SendMailUtils.sendMailAndNotification(null, null, null, null);

            return true;
        }
        return false;
    }

    @Override
    public boolean generalManagerSign(ContractDTO contractDTO) {
        ContractEntity contractEntity = this.contractDAO.find("ContractEntity",
            new ConditionBean("id", "=", String.valueOf(contractDTO.getContractId()),
                ParamUtils.DataType.LONG)).get(0);

        if (contractEntity != null) {
            long count = contractFilesDAO.markAsDeleteByContractId(contractEntity.getId());
            contractEntity
                .setContractStatus(CONTRACT_TYPE.CONTRACT_TYPE_OK_CONTRACT_FILES_REGISTED);

            //Save Contract
            this.contractDAO.saveOrUpdate(contractEntity);
            //Send mail and notification
            SendMailUtils.sendMailAndNotification(null, null, null, null);

            return true;
        }
        return false;
    }

    @Override
    public boolean pCReject(ContractDTO contractDTO,
        ContractHisDTO contractHisDTO) {
        ContractEntity contractEntity = this.contractDAO.find("ContractEntity",
            new ConditionBean("id", "=", String.valueOf(contractDTO.getContractId()),
                ParamUtils.DataType.LONG)).get(0);

        if (contractEntity != null) {
            contractEntity
                .setContractStatus(ContractStatus.REJECTED_CONTRACT_BY_LEGISLATION.getStatusCode());

            ContractHisEntity contractHisEntity = new ContractHisEntity();
            contractHisEntity.setContractId(contractEntity.getId());
            contractHisEntity.setContent(contractHisDTO.getContent());

            //Save Contract
            this.contractDAO.saveOrUpdate(contractEntity);
            contractHisDAO.saveOrUpdate(contractHisEntity);
            //Send mail and notification
            SendMailUtils.sendMailAndNotification(null, null, null, null);

            return true;
        }
        return false;
    }

    @Override
    public boolean pCAccept(ContractDTO contractDTO) {
        ContractEntity contractEntity = this.contractDAO.find("ContractEntity",
            new ConditionBean("id", "=", String.valueOf(contractDTO.getContractId()),
                ParamUtils.DataType.LONG)).get(0);

        if (contractEntity != null) {
            contractEntity
                .setContractStatus(ContractStatus.SUBMITTED_GENERAL_MANAGER.getStatusCode());
            //Save Contract
            this.contractDAO.saveOrUpdate(contractEntity);
            //Send mail and notification
            SendMailUtils.sendMailAndNotification(null, null, null, null);

            return true;
        }
        return false;
    }

    @Override
    public ContractEntity getContractEntity(Long contractId) {
        ContractEntity contractEntity = contractDAO.findById(contractId);
        return contractEntity;
    }

    public boolean checkValidateContractNumber(String contractNumber) {
        List<ContractEntity> contractEntity = contractDAO.find("ContractEntity", new ConditionBean("contractNumber", "=", String.valueOf(contractNumber), ParamUtils.DataType.STRING));

        if (contractEntity != null && contractEntity.size() > 0) {
            return false;
        }
        return true;
    }

    @Override
    public String updateContract(ContractDTO contractDTO, String action, int flat) {
        String message = ParamUtils.SUCCESS;
        ContractEntity contractEntity = null;
        switch (action) {
            case ParamUtils.ACTION_CREATE:
                contractEntity = new ContractEntity();
                break;
            case ParamUtils.ACTION_UPDATE:
                contractEntity = this.contractDAO.find("ContractEntity", new ConditionBean("id", "=", String.valueOf(contractDTO.getContractId()), ParamUtils.DataType.LONG)).get(0);
                break;
        }
        //Mapping dto to entity
        contractEntity = new ModelMapperUtil().mapper(contractDTO, ContractEntity.class);
        //Create object Contrract
        contractEntity = ContractEntity.setEntity(contractDTO, contractEntity, flat);
        //Save Contract
        this.contractDAO.saveOrUpdate(contractEntity);
        //Send mail and notification
        SendMailUtils.sendMailAndNotification(null, null, null, null);
        return message;
    }
}