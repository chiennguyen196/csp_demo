/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.service.impl;

import com.viettel.csp.DAO.BankDAO;
import com.viettel.csp.DAO.PartnerTypeDAO;
import com.viettel.csp.DTO.BankDTO;
import com.viettel.csp.DTO.ContactTypeDTO;
import com.viettel.csp.DTO.PartnerTypeDTO;
import com.viettel.csp.entity.PartnerTypeEntity;
import com.viettel.csp.service.PartnerService;
import com.viettel.csp.service.PartnerTypeService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author tiennv02
 */
public class PartnerTypeServiceImpl extends BaseCustomServiceImpl< PartnerTypeDAO, PartnerTypeEntity, PartnerTypeDTO> implements PartnerTypeService {

    @Autowired
    private PartnerTypeDAO partnerTypeDAO;

    public PartnerTypeDAO getPartnerTypeDAO() {
        return partnerTypeDAO;
    }

    public void setPartnerTypeDAO(PartnerTypeDAO partnerTypeDAO) {
        this.tDAO = partnerTypeDAO;
        this.classTDto = PartnerTypeDTO.class;
        this.partnerTypeDAO = partnerTypeDAO;
    }
}
