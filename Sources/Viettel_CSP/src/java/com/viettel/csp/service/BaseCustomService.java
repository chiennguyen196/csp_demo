/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.service;

import com.viettel.csp.DTO.BaseCustomDTO;
import com.viettel.csp.entity.BaseCustomEntity;
import com.viettel.csp.util.ConditionBean;
import com.viettel.csp.util.KeyValueBean;
import java.util.List;

/**
 *
 * @author tiennv@gemvietnam.com
 */
public interface BaseCustomService<TBO extends BaseCustomEntity, TForm extends BaseCustomDTO> {


    public List<TForm> findAll();

    public List<TForm> search(TForm form);

    public List<TForm> search(TForm form, List<ConditionBean> lstCondition);

    public List<TForm> search(TForm tForm, int start, int maxResult, String sortType, String sortField);

    public long count(TForm tForm);

    public TBO insert(TBO bo);

    public TBO update(TBO bo);
    
//    public String delete(Long id);

//    public String delete(List<TBO> tFormOnGrid);

//    public TBO findById(Long id);

//    public boolean isDuplicate(Long id, String... name);

//    public boolean isDuplicate(TForm form);

    public List<KeyValueBean> getListToKeyValueBean(Boolean isChoose) throws Exception;
}
