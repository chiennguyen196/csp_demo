package com.viettel.csp.service;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.viettel.csp.DTO.PartnerDTO;
import com.viettel.csp.entity.PartnerEntity;
import com.viettel.csp.util.KeyValueBean;
import java.util.List;

/**
 *
 * @author GEM
 */
public interface PartnerService extends BaseCustomService<PartnerEntity, PartnerDTO> {

    public  List<PartnerDTO> getListPartnerActive() throws Exception;

    public List<PartnerDTO> getListPartner(PartnerDTO partnerDTO) throws Exception;

    public List<PartnerEntity> getListPartnerContract(Long partnerId) throws Exception;

    public List<KeyValueBean> getListPartnerType() throws Exception;

    public List<KeyValueBean> getListPartnerStatus() throws Exception;

    public List<KeyValueBean> getListPartnerTypeByCode(String partnerTypeCode) throws Exception;

    public List<KeyValueBean> getListPartnerTypeByName(String partnerTypeName) throws Exception;

    public void update(PartnerDTO partnerDTO) throws Exception;

    public void deletes(List<Long> id) throws Exception;

    public boolean checkValidateDelete(Long id) throws Exception;

    public String update(PartnerDTO partnerDTO, String action) throws Exception;

    public String deletePartner(List<PartnerDTO> lstDeleteDTO);

    public PartnerDTO findInfoById(PartnerDTO dto) throws Exception;

    public PartnerEntity getById(Long partnerId) throws Exception;

    public PartnerDTO getObjectDTOById(Long partnerId) throws Exception;

    public List<PartnerDTO> findAll();

    public String insert(PartnerDTO partnerDTO) throws Exception;

    public void updateProfile(PartnerDTO partnerDTO) throws Exception;
}
