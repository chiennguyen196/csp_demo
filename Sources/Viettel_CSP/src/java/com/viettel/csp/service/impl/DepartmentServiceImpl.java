/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.service.impl;

import com.viettel.csp.DAO.DepartmentDAO;
import com.viettel.csp.DTO.DepartmentDTO;
import com.viettel.csp.entity.DepartmentEntity;
import com.viettel.csp.service.DepartmentService;
import com.viettel.csp.util.ConditionBean;
import com.viettel.csp.util.ParamUtils;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author admin
 */
public class DepartmentServiceImpl extends
    BaseCustomServiceImpl<DepartmentDAO, DepartmentEntity, DepartmentDTO> implements
    DepartmentService {

    @Autowired
    DepartmentDAO departmentDAO;

    @Override
    public DepartmentDTO findParent(Long childId) throws Exception {
        List<ConditionBean> list = new ArrayList<>();
        list.add(
            new ConditionBean("ORGANIZATIONID", " = ", childId.toString(), ParamUtils.TYPE_NUMBER));
        List<DepartmentDTO> lst = find("DepartmentEntity", list);
        return lst != null && !lst.isEmpty() ? lst.get(0) : null;
    }

    @Override
    public List<DepartmentDTO> findChild(Long parentId) throws Exception {
        List<ConditionBean> list = new ArrayList<>();
        list.add(
            new ConditionBean("ORGPARENTID", " = ", parentId.toString(), ParamUtils.TYPE_NUMBER));
        return find("DepartmentEntity", list);
    }

    public DepartmentDAO getDepartmentDAO() {
        return this.departmentDAO;
    }

    public void setDepartmentDAO(DepartmentDAO departmentDAO) {
        super.set(this.locale, new DepartmentEntity(), departmentDAO, DepartmentDTO.class);
        this.departmentDAO = departmentDAO;
    }

    @Override
    public List<DepartmentEntity> getAlls() throws Exception {
        return this.departmentDAO.getAllData(null);
    }

    @Override
    public List<DepartmentDTO> getAllsWithOrgnization(Long orgId) throws Exception {
        return this.departmentDAO.getAllsWithOrgnization(orgId);
    }

}
