package com.viettel.csp.constant;

/**
 * Created by admin on 6/16/2017.
 */
public class ContractManagerURL {
    public static String CREATED_PROFILE = "";
    public static String APPROVED_PROFILE = "";
    public static String INVALID_PROFILE = "";
    public static String APPROVING_STATEMENT = "";
    public static String REJECTED_STATEMENT = "";
    public static String SIGNED_STATEMENT = "";
    public static String CREATED_DRAFT_CONTRACT = "";
    public static String REJECTED_DRAFT_CONTRACT = "";
    public static String ACCEPTED_DRAFT_CONTRACT = "";
    public static String ACCEPTED_STATEMENT = "";
    public static String SUBMITTED_CONTRACT_TO_PARTNER = "/controls/contractManagement/contractEditManagementTypeOneOrTwo.zul";
    public static String SUBMITTED_CONTRACT_TO_VIETTEL = "/controls/contractManagement/contractToCSPRegisterPopup.zul";
    public static String SUBMITTED_CONTRACT_TO_LEGISLATION = "/controls/contractManagement/submittedContractToLegislation.zul";
    public static String REJECTED_CONTRACT_BY_LEGISLATION = "";
    public static String SUBMITTED_GENERAL_MANAGER = "/controls/contractManagement/contractToCSPRegisterPopupTGD.zul";
    public static String REJECTED_CONTRACT_BY_GENERAL_MANAGER = "";
    public static String SIGNED_BY_GENERAL_MANAGER = "/controls/contractManagement/contracManagertSendRegisterViettelB.zul";
    public static String SUBMITTED_SIGNED_CONTRACT_TO_PARTNER = "/controls/contractManagement/contracManagertRegisterViettelB.zul";
    public static String SIGNED_CONTRACT_BY_PARTNER = "/controls/contractManagement/contractManagerAssignPopup.zul";
    public static String REJECTED_TO_SIGN_CONTRACT_BY_PARTNER = "";
    public static String EFFECTING_CONTRACT = "/controls/contractManagement/effectingContractPopup.zul";
    public static String TERMINATED_CONTRACT = "";
    public static String CONTRACT_TYPE_SAVE_AND_DEPLOYMENT = "/controls/contractManagement/contractManagerWork.zul";
    public static String CONTRACT_TYPE_NO_ACCEPT = "/controls/contractManagement/contractManagerWork.zul";

    public static String getByContractStatus(ContractStatus contractStatus){
        switch (contractStatus){
            case CREATED_PROFILE:
                return CREATED_PROFILE;
			case APPROVED_PROFILE:
                return APPROVED_PROFILE;
			case INVALID_PROFILE:
			    return INVALID_PROFILE;
			case APPROVING_STATEMENT:
                return APPROVING_STATEMENT;
			case REJECTED_STATEMENT:
                return REJECTED_STATEMENT;
			case SIGNED_STATEMENT:
                return SIGNED_STATEMENT;
			case CREATED_DRAFT_CONTRACT:
                return CREATED_DRAFT_CONTRACT;
			case REJECTED_DRAFT_CONTRACT:
                return REJECTED_DRAFT_CONTRACT;
			case ACCEPTED_DRAFT_CONTRACT:
                return ACCEPTED_DRAFT_CONTRACT;
			case ACCEPTED_STATEMENT:
                return ACCEPTED_STATEMENT;
			case SUBMITTED_CONTRACT_TO_PARTNER:
                return SUBMITTED_CONTRACT_TO_PARTNER;
			case SUBMITTED_CONTRACT_TO_VIETTEL:
                return SUBMITTED_CONTRACT_TO_VIETTEL;
			case SUBMITTED_CONTRACT_TO_LEGISLATION:
                return SUBMITTED_CONTRACT_TO_LEGISLATION;
			case REJECTED_CONTRACT_BY_LEGISLATION:
                return REJECTED_CONTRACT_BY_LEGISLATION;
			case SUBMITTED_GENERAL_MANAGER:
                return SUBMITTED_GENERAL_MANAGER;
			case REJECTED_CONTRACT_BY_GENERAL_MANAGER:
                return REJECTED_CONTRACT_BY_GENERAL_MANAGER;
			case SIGNED_BY_GENERAL_MANAGER:
                return SIGNED_BY_GENERAL_MANAGER;
			case SUBMITTED_SIGNED_CONTRACT_TO_PARTNER:
                return SUBMITTED_SIGNED_CONTRACT_TO_PARTNER;
			case SIGNED_CONTRACT_BY_PARTNER:
                return SIGNED_CONTRACT_BY_PARTNER;
			case REJECTED_TO_SIGN_CONTRACT_BY_PARTNER:
                return REJECTED_TO_SIGN_CONTRACT_BY_PARTNER;
			case EFFECTING_CONTRACT:
                return EFFECTING_CONTRACT;
			case TERMINATED_CONTRACT:
                return TERMINATED_CONTRACT;
//            case CONTRACT_TYPE_SAVE_AND_DEPLOYMENT:
//                return CONTRACT_TYPE_SAVE_AND_DEPLOYMENT;
//            case CONTRACT_TYPE_NO_ACCEPT:
//                return CONTRACT_TYPE_NO_ACCEPT;
            default: return null;
        }
    }
}
