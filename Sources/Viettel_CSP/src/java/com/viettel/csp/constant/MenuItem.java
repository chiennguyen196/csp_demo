package com.viettel.csp.constant;

/**
 * Created by admin on 6/16/2017.
 */
public class MenuItem {

    private String menuCode;
    private String label;
    private String url;

    public MenuItem(String menuCode, String url, String label) {
        this.menuCode = menuCode;
        this.label = label;
        this.url = url;
    }

    public String getMenuCode() {
        return menuCode;
    }

    public String getLabel() {
        return label;
    }

    public String getUrl() {
        return url;
    }

}
