package com.viettel.csp.constant;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import viettel.passport.client.ObjectToken;

/**
 * Created by admin on 6/16/2017.
 */
public class Menu {

    List<MenuItem> menuItemList;

    public static ObjectToken createMenuItem(int id, String name, String url) {
        ObjectToken item = new ObjectToken();
        item.setObjectId(id);
        item.setObjectName(name);
        item.setObjectUrl(url);
        return item;
    }

    public static ObjectToken createMenuItem(String name, String url) {
        ObjectToken item = new ObjectToken();
        item.setObjectName(name);
        item.setObjectUrl(url);
        return item;
    }

    public static MenuItem createMenuItem(String menuCode, String url, String label) {
        MenuItem item = new MenuItem(menuCode, url, label);
        return item;
    }

    public static List<MenuItem> createMenu() {
        List<MenuItem> menu = Arrays.stream(MenuConfig.values()).map((menuItem) -> {
            return createMenuItem(menuItem.getCode(), menuItem.getUrl(),
                menuItem.getLabel());
        }).collect(Collectors.toList());

        return menu;
    }
}
