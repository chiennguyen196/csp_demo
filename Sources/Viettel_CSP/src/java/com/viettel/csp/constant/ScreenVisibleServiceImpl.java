package com.viettel.csp.constant;

import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.util.ParamUtils.USER_TYPE;
import com.viettel.logincsp.UserCredential;
import viettel.passport.client.ObjectToken;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by gem on 6/16/2017.
 */
public class ScreenVisibleServiceImpl implements ScreenVisibleService {

    @Override
    public boolean isVisible(UserCredential userCredential, String menuCode) {
        List<String> CONTACT_MENU_LIST = Arrays
                .asList("PARTNER_MANAGEMENT", "PARTNER_USER_MANAGEMENT", "REFLECT_MANAGEMENT",
                        "PROFILE_MANAGEMENT", "CONTRACT_MANAGEMENT", "LIQUIDATION_MANAGEMENT",
                        "SERVICE_MANAGEMENT", "REASON_MANAGEMENT");
        List<String> PARTNER_MENU_LIST = Arrays
                .asList("PARTNER_MANAGEMENT", "PARTNER_USER_MANAGEMENT", "REFLECT_MANAGEMENT",
                        "PROFILE_MANAGEMENT", "CONTRACT_MANAGEMENT", "LIQUIDATION_MANAGEMENT",
                        "SERVICE_MANAGEMENT", "REASON_MANAGEMENT");
        List<String> VIETTEL_MENU_LIST = Arrays
                .asList("PARTNER_MANAGEMENT", "PARTNER_USER_MANAGEMENT", "REFLECT_MANAGEMENT",
                        "PROFILE_MANAGEMENT", "CONTRACT_MANAGEMENT", "LIQUIDATION_MANAGEMENT",
                        "SERVICE_MANAGEMENT", "REASON_MANAGEMENT");
        switch (userCredential.getPersonType()) {
            case ParamUtils.USER_TYPE.CONTACT:
                return CONTACT_MENU_LIST.contains(menuCode);
            case USER_TYPE.PARTNER:
                return PARTNER_MENU_LIST.contains(menuCode);
            case USER_TYPE.VIETTEL:
                return VIETTEL_MENU_LIST.contains(menuCode);
            default:
                return false;
        }
    }

    @Override
    public List<ObjectToken> getMenu(UserCredential userCredential) {
        List<MenuItem> menu = Menu.createMenu();
        List<ObjectToken> afterFiltered;
        afterFiltered = menu.stream()
                .filter((item) -> isVisible(userCredential, item.getMenuCode()))
                .map((item) -> {
                    return createMenuItem(item.getMenuCode(), item.getUrl(), item.getLabel());
                }).collect(Collectors.toList());

        return afterFiltered;
    }


    private ObjectToken createMenuItem(String menuCode, String url, String label) {
        ObjectToken item = new ObjectToken();
        item.setObjectId(menuCode.hashCode());
        item.setObjectName(label);
        item.setObjectUrl(url);
        return item;
    }

}
