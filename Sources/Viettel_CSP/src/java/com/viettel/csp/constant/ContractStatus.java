package com.viettel.csp.constant;

import com.viettel.csp.util.LanguageBundleUtils;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by GEM on 6/16/2017.
 */
public enum ContractStatus {
    //Status code, bundle key
    CREATED_PROFILE("1", "contractStatus.createdProfile"),
    APPROVED_PROFILE("2", "contractStatus.approvedProfile"),
    INVALID_PROFILE("3", "contractStatus.invalidProfile"),
    APPROVING_STATEMENT("4", "contractStatus.approvingStatement"),
    REJECTED_STATEMENT("5", "contractStatus.rejectedStatement"),
    SIGNED_STATEMENT("6", "contractStatus.signedStatement"),
    CREATED_DRAFT_CONTRACT("7", "contractStatus.createdDraftContract"),
    REJECTED_DRAFT_CONTRACT("8", "contractStatus.rejectedDraftContract"),
    ACCEPTED_DRAFT_CONTRACT("9", "contractStatus.acceptedDraftContract"),
    ACCEPTED_STATEMENT("10", "contractStatus.acceptedStatement"),
    SUBMITTED_CONTRACT_TO_PARTNER("11", "contractStatus.submittedContractToPartner"),
    SUBMITTED_CONTRACT_TO_VIETTEL("12", "contractStatus.submittedContractToViettel"),
    SUBMITTED_CONTRACT_TO_LEGISLATION("13", "contractStatus.submittedContractToLegislation"),
    REJECTED_CONTRACT_BY_LEGISLATION("14", "rejectedContractByLegislation"),
    SUBMITTED_GENERAL_MANAGER("15", "contractStatus.submittedGeneralManager"),
    REJECTED_CONTRACT_BY_GENERAL_MANAGER("16", "contractStatus.rejectedContractByGeneralManager"),
    SIGNED_BY_GENERAL_MANAGER("17", "contractStatus.signedByGeneralManager"),
    SUBMITTED_SIGNED_CONTRACT_TO_PARTNER("18", "contractStatus.submittedSignedContractToPartner"),
    SIGNED_CONTRACT_BY_PARTNER("19", "contractStatus.signedContractByPartner"),
    REJECTED_TO_SIGN_CONTRACT_BY_PARTNER("20", "contractStatus.rejectedToSignContractByPartner"),
    EFFECTING_CONTRACT("26", "contractStatus.effectingContract"),
    TERMINATED_CONTRACT("28", "contractStatus.terminatedContract"),
    SIGN_CONTRACT_TO_HD("TL04", "contractStatus.liquidation.signContractToHd"),
    CANCEL_LIQUIDATION("TL05", "contractStatus.liquidation.cancelLiquidation"),
    SUGGEST_ASSIGN("TL06", "contractStatus.liquidation.suggestAssign"),
    SEND_TO_PARTNER("TL08", "contractStatus.liquidation.sendToPartner"),
    CANCEL_REGISTER_PARTNER("TL09", "contractStatus.liquidation.cancelRegisterPartner"),
    CANCEL_REGISTER("TL11", "contractStatus.liquidation.cancelRegister");

    private final String statusCode;
    private final String statusNameBundle;


    ContractStatus(String statusCode, String statusNameBundle) {
        this.statusCode = statusCode;
        this.statusNameBundle = statusNameBundle;
    }

    public static ContractStatus getContractStatus(String contractStatusCode) {
        List<ContractStatus> contractStatusList = Arrays.stream(ContractStatus.values())
            .filter((contractStatus) -> contractStatus.getStatusCode().equals(contractStatusCode))
            .collect(
                Collectors.toList());
        return contractStatusList.size() > 0 ? contractStatusList.get(0) : null;
    }

    public static String getContractStatusName(ContractStatus contractStatus) {
        return LanguageBundleUtils.getString(contractStatus.getStatusNameBundle());
    }

    public static String getContractStatusName(String statusCode) {
        ContractStatus contractStatus = Arrays.stream(ContractStatus.values())
            .filter((status) -> statusCode.equals(status.getStatusCode()))
            .findFirst().get();
        return LanguageBundleUtils.getString(contractStatus.getStatusNameBundle());
    }

    public String getStatusCode() {
        return this.statusCode;
    }

    public String getStatusNameBundle() {
        return statusNameBundle;
    }
}
