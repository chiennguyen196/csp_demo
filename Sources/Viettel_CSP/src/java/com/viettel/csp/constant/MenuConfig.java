package com.viettel.csp.constant;

import com.viettel.csp.util.LanguageBundleUtils;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by admin on 6/16/2017.
 */
public enum MenuConfig {
    //code, url, label
    PARTNER_MANAGEMENT("PARTNER_MANAGEMENT", "/controls/partner/partner.zul",
        "menu.label.partner"),
    PARTNER_USER_MANAGEMENT("PARTNER_USER_MANAGEMENT", "/controls/contact/contact.zul",
        "menu.label.partnerUser"),
    REFLECT_MANAGEMENT("REFLECT_MANAGEMENT", "/controls/reflect/reflect.zul",
        "menu.label.reflect"),
    PROFILE_MANAGEMENT("PROFILE_MANAGEMENT", "/controls/contract/contract.zul",
        "menu.label.profile"),
    CONTRACT_MANAGEMENT("CONTRACT_MANAGEMENT",
        "/controls/contractManagement/contractManagement.zul",
        "menu.label.contract"),
    LIQUIDATION_MANAGEMENT("LIQUIDATION_MANAGEMENT", "/controls/liquidation/liquidation.zul",
        "menu.label.liquidation"),
    TASK_MANAGEMENT("TASK_MANAGEMENT", "/controls/workManager/workManager.zul",
        "menu.label.task"),
    SERVICE_MANAGEMENT("SERVICE_MANAGEMENT", "/controls/service/serviceManager.zul",
        "menu.label.service"),
    REASON_MANAGEMENT("REASON_MANAGEMENT", "/controls/reason/reason.zul", "menu.label.reason");

    private String code;
    private String url;
    private String labelBundle;

    MenuConfig(String code, String url, String labelBundle) {
        this.code = code;
        this.url = url;
        this.labelBundle = labelBundle;
    }

    public static String getLabel(String menuCode) {
        MenuConfig menuConfig = getMenuBycode(menuCode);
        return LanguageBundleUtils.getString(menuConfig.getLabelBundle());
    }

    public static MenuConfig getMenuBycode(String menuCode) {
        List<MenuConfig> menuList = Arrays.stream(MenuConfig.values()).filter(
            (menu) -> menu.getCode().equals(menuCode))
            .collect(
                Collectors.toList());
        return menuList.size() > 0 ? menuList.get(0) : null;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLabelBundle() {
        return labelBundle;
    }

    public void setLabelBundle(String labelBundle) {
        this.labelBundle = labelBundle;
    }

    public String getLabel() {
        return MenuConfig.getLabel(code);
    }
}
