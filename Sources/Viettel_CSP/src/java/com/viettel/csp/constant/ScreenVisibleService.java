package com.viettel.csp.constant;

import com.viettel.logincsp.UserCredential;
import java.util.List;
import viettel.passport.client.ObjectToken;

/**
 * Created by admin on 6/16/2017.
 */
public interface ScreenVisibleService {

    boolean isVisible(UserCredential userCredential, String menuCode);

    List<ObjectToken> getMenu(UserCredential userCredential);
}
