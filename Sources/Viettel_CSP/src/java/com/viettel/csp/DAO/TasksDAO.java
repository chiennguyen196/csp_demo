/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.DAO;

import com.viettel.csp.DTO.TasksDTO;
import com.viettel.csp.entity.TasksEntity;
import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.util.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.hibernate.type.DateType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;

import java.util.ArrayList;
import java.util.List;

/**
 * @author admin
 */
public class TasksDAO extends BaseCustomDAO<TasksEntity> {

    private static Logger log = Logger.getLogger(TasksDAO.class);

    public TasksDAO() {
        tEntity = new TasksEntity();
        tEntityClass = TasksEntity.class;
    }

    @Override
    public String delete(TasksEntity tasksEntity) {
        TasksEntity obj = this.findById(tasksEntity.getId());
        if(obj == null)
            return ParamUtils.FAIL;
        return super.delete(tasksEntity);
    }

    public List<TasksEntity> findAlls(TasksDTO tasksDTO) {
        try {
            Session session = getCurrentSession();
            session.beginTransaction();
            Criteria cr = session.createCriteria(TasksEntity.class);
            cr.add(Restrictions.eq("assignBy", tasksDTO.getAssignBy()));
            if (tasksDTO.getStatus() != null && !tasksDTO.getStatus().isEmpty())
                cr.add(Restrictions.eq("status", tasksDTO.getStatus()));
            return cr.list();
        } catch (Exception e) {
            log.error("erro when find all entity", e);
            throw e;
        }
    }


    public List<TasksDTO> findAllsToDTO(TasksDTO tasksDTO) {
        List<TasksDTO> list = new ArrayList();
        try {
            Session session = getCurrentSession();
            session.beginTransaction();
            StringBuilder sql = new StringBuilder("");
            sql.append("SELECT T.BEFORE_TASK beforeTask, T.COMPLETE_DATE  completeDate, TT.NAME taskTypeName, T.CONTRACT_ID contactId, T.ID id , T.REQUIRED_COMPLETE_DATE requiredCompleteDate, TT.CODE taskType,  U.USERNAME username, T.CONTENT content, C.START_DATE startDate, C.END_DATE endDate, T.STATUS status");
            sql.append(" FROM TBL_TASKS T INNER JOIN TBL_TASK_TYPE TT ON T.TASK_TYPE = TT.CODE INNER JOIN  TBL_USER U ON T.CREATE_BY = U.ID INNER JOIN TBL_CONTRACT C ON T.CONTRACT_ID = C.ID WHERE C.CONTRACT_STATUS != '24'");
            if (StringUtils.isValidString(tasksDTO.getStatus())) {
                sql.append(" AND T.STATUS =:STATUS ");
            }
            if (StringUtils.isValidString(tasksDTO.getAssignBy())) {
                sql.append(" AND T.ASSIGN_BY =:ASSIGN_BY ");
            }
            if (StringUtils.isValidString(tasksDTO.getContactId())) {
                sql.append(" AND T.CONTRACT_ID  =:CONTRACT_ID ");
            }
            Query query = session.createSQLQuery(sql.toString())
                    .addScalar("id", new LongType())
                    .addScalar("contactId", new LongType())
                    .addScalar("taskTypeName", new StringType())
                    .addScalar("requiredCompleteDate", new DateType())
                    .addScalar("completeDate", new DateType())
                    .addScalar("startDate", new DateType())
                    .addScalar("endDate", new DateType())
                    .addScalar("username", new StringType())
                    .addScalar("content", new StringType())
                    .addScalar("taskType", new StringType())
                    .addScalar("beforeTask", new StringType())
                    .addScalar("status", new StringType())
                    .setResultTransformer(Transformers.aliasToBean(TasksDTO.class));
            if (StringUtils.isValidString(tasksDTO.getStatus())) {
                query.setParameter("STATUS", tasksDTO.getStatus());
            }
            if (StringUtils.isValidString(tasksDTO.getAssignBy())) {
                query.setParameter("ASSIGN_BY", tasksDTO.getAssignBy());
            }
            if (StringUtils.isValidString(tasksDTO.getContactId())) {
                query.setParameter("CONTRACT_ID", tasksDTO.getContactId());
            }
            list = query.list();
            session.getTransaction().commit();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return list;
    }

    public List<TasksDTO> findAllToDTO(TasksDTO tasksDTO) {
        List<TasksDTO> list = new ArrayList();
        try {
            Session session = getCurrentSession();
            session.beginTransaction();
            StringBuilder sql = new StringBuilder("")
                    .append("SELECT TT.NAME taskTypeName, T.CONTRACT_ID contactId, T.ID   id, T.REQUIRED_COMPLETE_DATE requiredCompleteDate, T.COMPLETE_DATE completeDate, TT.CODE   taskType, U.USERNAME  username, T.CONTENT  content,  C.START_DATE  startDate, C.END_DATE  endDate, T.STATUS status,")
                    .append(" DECODE(T.STATUS,4,'Hoàn thành',3,'Công việc lỗi',2,'Không kích hoạt',1,'Đang thực hiện','Chưa kích hoạt') statusName")
                    .append(" FROM TBL_TASKS T INNER JOIN TBL_TASK_TYPE TT ON T.TASK_TYPE = TT.CODE INNER JOIN TBL_USER U ON T.CREATE_BY = U.ID INNER JOIN TBL_CONTRACT C ON T.CONTRACT_ID = C.ID WHERE C.CONTRACT_STATUS != '24'");
            if (StringUtils.isValidString(tasksDTO.getStatus())) {
                sql.append(" AND T.STATUS =:STATUS ");
            }
            if (StringUtils.isValidString(tasksDTO.getAssignBy())) {
                sql.append(" AND T.ASSIGN_BY =:ASSIGN_BY ");
            }
            if (StringUtils.isValidString(tasksDTO.getContactId())) {
                sql.append(" AND T.CONTRACT_ID  =:CONTRACT_ID ");
            }
            Query query = session.createSQLQuery(sql.toString())
                    .addScalar("id", new LongType())
                    .addScalar("contactId", new LongType())
                    .addScalar("taskTypeName", new StringType())
                    .addScalar("completeDate", new DateType())
                    .addScalar("requiredCompleteDate", new DateType())
                    .addScalar("startDate", new DateType())
                    .addScalar("endDate", new DateType())
                    .addScalar("username", new StringType())
                    .addScalar("content", new StringType())
                    .addScalar("taskType", new StringType())
                    .addScalar("status", new StringType())
                    .addScalar("statusName", new StringType())
                    .setResultTransformer(Transformers.aliasToBean(TasksDTO.class));
            if (StringUtils.isValidString(tasksDTO.getStatus())) {
                query.setParameter("STATUS", tasksDTO.getStatus());
            }
            if (StringUtils.isValidString(tasksDTO.getAssignBy())) {
                query.setParameter("ASSIGN_BY", tasksDTO.getAssignBy());
            }
            if (StringUtils.isValidString(tasksDTO.getContactId())) {
                query.setParameter("CONTRACT_ID", tasksDTO.getContactId());
            }
            list = query.list();
            session.getTransaction().commit();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return list;
    }
}
