package com.viettel.csp.DAO;

import com.viettel.csp.entity.TaskFilesEntity;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.List;

public class TaskFilesDAO extends BaseCustomDAO<TaskFilesEntity> {

    private static Logger log = Logger.getLogger(TaskFilesDAO.class);

    public TaskFilesDAO() {
        tEntity = new TaskFilesEntity();
        tEntityClass = TaskFilesEntity.class;
    }

    public List<TaskFilesEntity> findAlles(Long taskId) throws Exception{
        try {
            Session session = getCurrentSession();
            session.beginTransaction();
            Criteria cr = session.createCriteria(TaskFilesEntity.class);
            cr.add(Restrictions.eq("taskId", taskId));
            return cr.list();
        } catch (Exception e) {
            log.error("error when find all entity", e);
            throw e;
        }
    }
}
