/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.DAO;

import com.viettel.csp.DTO.PartnerBankDTO;
import com.viettel.csp.entity.PartnerBankEntity;
import com.viettel.csp.util.StringUtils;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

/**
 *
 * @author GEM
 */
public class PartnerBankDAO extends BaseCustomDAO<PartnerBankEntity> {

    private static Logger log = Logger.getLogger(ContactDAO.class);

    public PartnerBankDAO() {
        tEntity = new PartnerBankEntity();
        tEntityClass = PartnerBankEntity.class;
    }

    public void deleteByPartnerId(Long partnerId) throws Exception {
        Session session = getCurrentSession();
        session.beginTransaction();
        StringBuilder sql = new StringBuilder()
                .append("  DELETE  FROM tbl_partner_bank")
                .append("   WHERE   partner_id= :partnerId");
        Query query = session.createSQLQuery(sql.toString());
        query.setParameter("partnerId", partnerId);
        query.executeUpdate();
        session.getTransaction().commit();

    }

    public List<PartnerBankDTO> getInfoBankByPartnerId(Long partnerId) throws HibernateException {
        Session session = getCurrentSession();
        session.beginTransaction();

        StringBuilder sql = new StringBuilder()
                .append("  select   a.id id, a.partner_id  partnerId, a.bank_code  bankCode, a.bank_branch bankBranch, a.account_number accountNumber, b.name bankName")
                .append("    from tbl_partner_bank a, tbl_bank b  ")
                .append("    where a.partner_id = :partnerId and a.bank_code= b.code order by a.id asc");

        Query query = session.createSQLQuery(sql.toString())
                .addScalar("id", Hibernate.LONG)
                .addScalar("partnerId", Hibernate.LONG)
                .addScalar("bankCode", Hibernate.STRING)
                .addScalar("bankBranch", Hibernate.STRING)
                .addScalar("accountNumber", Hibernate.STRING)
                .addScalar("bankName", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(PartnerBankDTO.class));

        query.setParameter("partnerId", partnerId);
        return query.list();
    }
}
