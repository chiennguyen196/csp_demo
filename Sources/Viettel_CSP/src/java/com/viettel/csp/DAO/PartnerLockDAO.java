/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.DAO;

import com.viettel.csp.DTO.PartnerLockDTO;
import com.viettel.csp.config.HibernateUtils;
import com.viettel.csp.entity.PartnerLockEntity;
import com.viettel.csp.util.ModelMapperUtil;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author tiennv02
 */
public class PartnerLockDAO extends BaseCustomDAO<PartnerLockEntity> {

    private static Logger logger = Logger.getLogger(PartnerLockDAO.class);

    public PartnerLockDAO() {
        tEntity = new PartnerLockEntity();
        tEntityClass = PartnerLockEntity.class;
    }

    public PartnerLockDTO findByPartnerId(Long partnerId) {
        PartnerLockDTO partnerLockDTO = null;
        try {
            Session session = HibernateUtils.getCurrentSession();
            session.getTransaction().begin();
            Criteria criteria = session.createCriteria(PartnerLockEntity.class);
            criteria.add(Restrictions.eq("partnerId", partnerId));
            criteria.addOrder(Order.desc("partnerId"));
            List<PartnerLockEntity> list = criteria.list();
            if (list != null && list.size() > 0) {
                ModelMapperUtil mapperUtil = new ModelMapperUtil();
                partnerLockDTO = mapperUtil.mapper(list.get(0), PartnerLockDTO.class);
            }
            session.getTransaction().commit();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return partnerLockDTO;
    }
}
