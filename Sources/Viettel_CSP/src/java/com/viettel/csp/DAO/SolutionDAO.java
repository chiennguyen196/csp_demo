/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.DAO;

import com.viettel.csp.DTO.SolutionDTO;
import com.viettel.csp.entity.SolutionEntity;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.util.StringUtils;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

/**
 *
 * @author admin
 */
public class SolutionDAO extends BaseCustomDAO<SolutionEntity> {

    private static Logger log = Logger.getLogger(SolutionDAO.class);

    public SolutionDAO() {
        tEntity = new SolutionEntity();
        tEntityClass = SolutionEntity.class;
    }

    public List<SolutionDTO> getListSolution(SolutionDTO solutionDTO) {
        List<SolutionDTO> listResult = new ArrayList<>();
        Session session = getCurrentSession();
        session.beginTransaction();
        StringBuilder sql = new StringBuilder();
        sql.append("select a.id, "
                + " UPDATE_BY updateBy,"
                + " UPDATE_AT updateAt,"
                + " CONTENT content,"
                + " REPONSE_DATE reponseDate,"
                + " SOLUTION_STATUS solutionStatus,"
                + " REQUEST_DATE requestDate,"
                + " EVAL_REASON_CODE evalReasonCode,"
                + " EVAL_DESCRIPTION evalDescription,"
                + " b.name evalReasonName"
                + " from TBL_SOLUTION a left join TBL_REASON b on a.EVAL_REASON_CODE = b.CODE");
        if (solutionDTO != null) {
            if (StringUtils.isValidString(solutionDTO.getReflectId())) {
                sql.append(" where REFLECT_ID = :REFLECT_ID");
            }
        }
        Query query = session.createSQLQuery(sql.toString())
                .addScalar("id", Hibernate.LONG)
                .addScalar("updateBy", Hibernate.LONG)
                .addScalar("updateAt", Hibernate.DATE)
                .addScalar("content", Hibernate.STRING)
                .addScalar("reponseDate", Hibernate.DATE)
                .addScalar("solutionStatus", Hibernate.STRING)
                .addScalar("requestDate", Hibernate.DATE)
                .addScalar("evalReasonCode", Hibernate.STRING)
                .addScalar("evalDescription", Hibernate.STRING)
                .addScalar("evalReasonName", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(SolutionDTO.class));
        if (solutionDTO != null) {
            if (StringUtils.isValidString(solutionDTO.getReflectId())) {
                query.setParameter("REFLECT_ID", solutionDTO.getReflectId());
            }
        }
        listResult = query.list();
        for (SolutionDTO solution : listResult) {
            switch (solution.getSolutionStatus()) {
                case ParamUtils.SOLUTION_STATUS.REPONSE:
                    solution.setSolutionStatusName(LanguageBundleUtils.getString("reflect.solution.reponse"));
                    break;
                case ParamUtils.SOLUTION_STATUS.OK:
                    solution.setSolutionStatusName(LanguageBundleUtils.getString("reflect.solution.ok"));
                    break;
                case ParamUtils.SOLUTION_STATUS.NOK:
                    solution.setSolutionStatusName(LanguageBundleUtils.getString("reflect.solution.nok"));
                    break;
                case ParamUtils.SOLUTION_STATUS.MORE_INFO:
                    solution.setSolutionStatusName(LanguageBundleUtils.getString("reflect.solution.moreinfo"));
                    break;
            }
        }
        session.getTransaction().commit();
        return listResult;
    }
}
