package com.viettel.csp.DAO;

import com.viettel.csp.entity.ContractBreachEntity;
import com.viettel.csp.entity.ContractBreachReasonEntity;
import org.apache.log4j.Logger;

/**
 * Created by GEM on 6/16/2017.
 */
public class ContractBreachReasonDAO extends BaseCustomDAO {
    private static Logger log = Logger.getLogger(ContractBreachReasonDAO.class);

    public ContractBreachReasonDAO(){
        tEntity = new ContractBreachReasonEntity();
        tEntityClass = ContractBreachReasonEntity.class;
    }

}
