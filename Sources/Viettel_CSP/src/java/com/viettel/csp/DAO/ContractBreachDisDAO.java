package com.viettel.csp.DAO;

import com.viettel.csp.entity.ContractBreachDisEntity;

/**
 * Created by GEM on 6/17/2017.
 */
public class ContractBreachDisDAO extends BaseCustomDAO {
    private static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ContractBreachDisDAO.class);

    public ContractBreachDisDAO(){
        tEntity = new ContractBreachDisEntity();
        tEntityClass = ContractBreachDisEntity.class;
    }

}
