/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.DAO;

import com.viettel.csp.DTO.ReflectDTO;
import com.viettel.csp.DTO.ReflectFilesDTO;
import com.viettel.csp.entity.ReflectFilesEntity;
import com.viettel.csp.util.StringUtils;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

/**
 *
 * @author admin
 */
public class ReflectFilesDAO extends BaseCustomDAO<ReflectFilesEntity> {

    private static Logger log = Logger.getLogger(ReflectFilesDAO.class);

    public ReflectFilesDAO() {
        tEntity = new ReflectFilesEntity();
        tEntityClass = ReflectFilesEntity.class;
    }

    public List<ReflectFilesDTO> getList(ReflectFilesDTO reflectFilesDTO) {
        List<ReflectFilesDTO> list = new ArrayList<>();
        try {
            Session session = getCurrentSession();
            session.beginTransaction();

            StringBuilder sql = new StringBuilder();
            sql.append("select ID id,FILE_NAME fileName,"
                    + "DESCRIPTION description,REFLECT_ID reflectId,"
                    + "UPLOAD_DATE uploadDate,PATH_FILE pathFile"
                    + " from TBL_REFLECT_FILES");
            if (reflectFilesDTO != null) {
                if (StringUtils.isValidString(reflectFilesDTO.getReflectId())) {
                    sql.append(" where REFLECT_ID = :REFLECT_ID");
                }
            }

            Query query = session.createSQLQuery(sql.toString())
                    .addScalar("id", Hibernate.LONG)
                    .addScalar("fileName", Hibernate.STRING)
                    .addScalar("description", Hibernate.STRING)
                    .addScalar("reflectId", Hibernate.LONG)
                    .addScalar("uploadDate", Hibernate.DATE)
                    .addScalar("pathFile", Hibernate.STRING)
                    .setResultTransformer(Transformers.aliasToBean(ReflectFilesDTO.class));
            if (reflectFilesDTO != null) {
                if (StringUtils.isValidString(reflectFilesDTO.getReflectId())) {
                    query.setParameter("REFLECT_ID", reflectFilesDTO.getReflectId());
                }
            }
            list = query.list();
            session.getTransaction().commit();
        } catch (Exception e) {
            log.error(e.getMessage(),e);
        }
        return list;
    }

}
