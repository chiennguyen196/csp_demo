/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.DAO;

import com.viettel.csp.entity.SolutionFilesEntity;
import org.apache.log4j.Logger;

/**
 *
 * @author admin
 */
public class SolutionFilesDAO extends BaseCustomDAO<SolutionFilesEntity> {

    private static Logger log = Logger.getLogger(SolutionFilesDAO.class);

    public SolutionFilesDAO() {
        tEntity = new SolutionFilesEntity();
        tEntityClass = SolutionFilesEntity.class;
    }
}
