/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.DAO;

import com.viettel.csp.DTO.LocationDTO;
import com.viettel.csp.entity.LocationEntity;
import com.viettel.csp.util.KeyValueBean;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

/**
 *
 * @author GEM
 */
public class LocationDAO extends BaseCustomDAO<LocationEntity> {

    private static Logger log = Logger.getLogger(LocationEntity.class);

    public LocationDAO() {
        tEntity = new LocationEntity();
        this.tEntityClass = LocationEntity.class;
    }

    public List<LocationDTO> getListProvince() {
        List<LocationDTO> lst = new ArrayList<LocationDTO>();

        Session session = getCurrentSession();
        session.beginTransaction();
        StringBuilder sql = new StringBuilder()
                .append("  SELECT distinct   province_code provinceCode, province_name provinceName")
                .append("    FROM   tbl_location order by province_name asc");
        Query query = session.createSQLQuery(sql.toString())
                .addScalar("provinceCode", Hibernate.STRING)
                .addScalar("provinceName", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(LocationDTO.class));
        lst = query.list();
        session.getTransaction().commit();
        return lst;

    }

    public List<KeyValueBean> getProvinceByCode(String provinceCode) {
        List<KeyValueBean> lstKeyValueBean = new ArrayList<KeyValueBean>();

        Session session = getCurrentSession();
        session.beginTransaction();
        StringBuilder sql = new StringBuilder()
                .append("  SELECT distinct   province_code provinceCode, province_name provinceName")
                .append("    FROM   tbl_location ")
                .append("    WHERE   province_code=:provinceCode");

        Query query = session.createSQLQuery(sql.toString())
                .addScalar("provinceCode", Hibernate.STRING)
                .addScalar("provinceName", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(KeyValueBean.class));

        query.setParameter("provinceCode", provinceCode);
        lstKeyValueBean = query.list();
        session.getTransaction().commit();
        return lstKeyValueBean;

    }
}
