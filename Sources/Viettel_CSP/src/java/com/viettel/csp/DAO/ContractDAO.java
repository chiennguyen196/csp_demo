/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.DAO;

import com.viettel.csp.DTO.ContractDTO;
import com.viettel.csp.entity.ContractEntity;
import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.util.StringUtils;
import org.hibernate.*;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;

import java.util.ArrayList;
import java.util.List;

import static com.viettel.csp.util.StringUtils.getParamLike;

/**
 * @author TranChi
 */
public class ContractDAO extends BaseCustomDAO<ContractEntity> {

    private static org.apache.log4j.Logger log = org.apache.log4j.Logger
            .getLogger(ContractDAO.class);

    public ContractDAO() {
        this.tEntity = new ContractEntity();
        this.tEntityClass = ContractEntity.class;
    }

    public List<ContractDTO> getListContract(ContractDTO contractDTO, String screen) {
        List<ContractDTO> lstResult = new ArrayList<>();
        Session session = getCurrentSession();
        session.beginTransaction();
        StringBuilder sql = new StringBuilder()
                .append("select  ")
                .append("a.id id,")
                .append("a.id contractId,")
                .append("a.contract_type contractType,")
                .append("a.contract_status contractStatus,")
                .append("a.contract_number contractNumber,")
                .append("a.partner_id partnerId,")
                .append("a.service_code serviceCode,")
                .append("a.head_of_service headOfService,")
                .append("a.ct_sign_form ctSignForm,")
                .append("a.start_date startDate,")
                .append("a.end_date endDate,")
                .append("a.create_by  createBy,")
                .append("a.create_at createAt,")
                .append("a.update_by updateBy,")
                .append("a.update_at updateAt,")
                .append("a.filing_date filingDate,")
                .append("a.filing_by  filingBy,")
                .append("a.ct_user_csp ctUserCsp,")
                .append("a.sign_ct_approve signCtApprove,")
                .append("a.sign_ct_approve_date  signCtApproveDate,")
                .append("a.sign_ct_expertise_law signCtExpertiseLaw,")
                .append("a.sign_ct_expertise_law_date signCtExpertiseLawDate,")
                .append("a.sign_ct_complete_viettel signCtCompleteViettel,")
                .append("a.sign_ct_complete_viettel_date signCtCompleteViettelDate,")
                .append("a.sign_ct_complete_partner signCtCompletePartner,")
                .append("a.sign_ct_complete_partner_date signCtCompletePartnerDate,")
                .append("a.deploy_user_id deployUserId,")
                .append("a.deploy_assign_date deployAssignDate,")
                .append("a.deploy_close_date deployCloseDate,")
                .append("a.is_lock isLock,")
                .append("a.li_status liStatus,")
                .append("a.li_user_csp liUserCsp,")
                .append("a.sign_li signLi,")
                .append("a.sign_li_date signLiDate,")
                .append("a.sign_li_expertise_law      signLiExpertiseLaw,")
                .append("a.sign_li_expertise_law_date   signLiExpertiseLawDate,")
                .append("a.sign_li_complete_viettel     signLiCompleteViettel,")
                .append("a.sign_li_complete_viettel_date  signLiCompleteViettelDate,")
                .append("a.sign_li_partner signLiPartner,")
                .append("a.sign_li_partner_date  signLiPartnerDate,")
                .append("a.li_start_date  liStartDate,")
                .append("a.li_sign_form  liSignForm,")
                .append("a.single_liquidation  singleLiquidation,")
                .append("a.single_liquidation_date  singleLiquidationDate,")
                .append("a.single_liquidation_start_date  singleLiquidationStartDate,")
                .append("b.id Idpartner,")
                .append("b.partner_code  partnerCode, ")
                .append("b.company_name  companyName, ")
                .append("b.company_name_short companyNameShort, ")
                .append("b.address_head_office  addressOffice, ")
                .append("b.address_trading_office   addressTradingOffice, ")
                .append("b.email   email, ")
                .append("b.business_regis_number   businessRegisNumber, ")
                .append("b.business_regis_date businessRegisDate, ")
                .append("b.business_regis_count businessRegisCount, ")
                .append("b.partner_group partnerGroup, ")
                .append("b.partner_group_province partnerGroupProvince, ")
                .append("b.phone phone, ")
                .append("b.rep_name repName, ")
                .append("b.rep_position repPosition, ")
                .append("b.tax_code taxCode, ")
                .append("b.update_at updateAt, ")
                .append("b.create_at createAt, ")
                .append("b.create_by createBy, ")
                .append("b.update_by updateBy, ")
                .append("b.address_head_office addressHeadOffice, ")
                .append("c.code contractStatusCode, ")
                .append("c.name contractStatusName  ")
                .append(" from ")
                .append(" tbl_contract a left join tbl_partner b on a.partner_id = b.id left join tbl_contract_status c on a.contract_status = c.code")
                .append("   WHERE   1 = 1  ");
        if (contractDTO != null) {
            if (StringUtils.isValidString(contractDTO.getCompanyName())) {
                sql.append("AND UPPER(b.company_name) LIKE UPPER(:companyName) ");
            }
            if (StringUtils.isValidString(contractDTO.getContractStatusCode())) {
                sql.append(" AND LOWER(a.contract_status) = :contractStatus ");
            }
            if (StringUtils.isValidString(contractDTO.getCompanyNameShort())) {
                sql.append(" AND UPPER(b.company_name_short) LIKE UPPER(:companyNameShort) ");
            }
            if (StringUtils.isValidString(contractDTO.getContractNumber())) {
                sql.append(" AND UPPER(a.contract_number) LIKE UPPER(:contractNumber) ");
            }
            if (StringUtils.isValidString(contractDTO.getPartnerId())) {
                sql.append(" AND a.partner_id = :partnerId ");
            }
            if (StringUtils.isValidString(contractDTO.getContractId())) {
                sql.append(" AND a.id = :contractId ");
            }
        }
        //Ho so
        if (screen.equalsIgnoreCase("PROFILE")) {
            sql.append(" AND a.contract_status in ('-9999'");
            for (String i : ParamUtils.TYPE_ACTION.LIST_ACTION_PROFILE) {
                sql.append(",'" + i + "'");
            }
            sql.append(" )");
        }
        //Hop dong
        if (screen.equalsIgnoreCase("CONTRACT")) {
            sql.append(" AND a.contract_status in ('-9999'");
            for (String i : ParamUtils.TYPE_ACTION.LIST_ACTION_CONTRACT) {
                sql.append(",'" + i + "'");
            }
            sql.append(" )");
        }

        sql.append(" order by a.update_at desc");
        Query query = session.createSQLQuery(sql.toString())
                .addScalar("id", Hibernate.LONG)
                .addScalar("contractId", Hibernate.LONG)
                .addScalar("contractType", Hibernate.STRING)
                .addScalar("contractStatus", Hibernate.STRING)
                .addScalar("contractNumber", Hibernate.STRING)
                .addScalar("partnerId", Hibernate.LONG)
                .addScalar("serviceCode", Hibernate.STRING)
                .addScalar("headOfService", Hibernate.STRING)
                .addScalar("ctSignForm", Hibernate.STRING)
                .addScalar("startDate", Hibernate.TIMESTAMP)
                .addScalar("endDate", Hibernate.TIMESTAMP)
                .addScalar("createBy", Hibernate.LONG)
                .addScalar("createAt", Hibernate.TIMESTAMP)
                .addScalar("updateBy", Hibernate.LONG)
                .addScalar("updateAt", Hibernate.TIMESTAMP)
                .addScalar("filingDate", Hibernate.TIMESTAMP)
                .addScalar("filingBy", Hibernate.LONG)
                .addScalar("ctUserCsp", Hibernate.LONG)
                .addScalar("signCtApprove", Hibernate.LONG)
                .addScalar("signCtApproveDate", Hibernate.TIMESTAMP)
                .addScalar("signCtExpertiseLaw", Hibernate.LONG)
                .addScalar("signCtExpertiseLawDate", Hibernate.TIMESTAMP)
                .addScalar("signCtCompleteViettel", Hibernate.LONG)
                .addScalar("signCtCompleteViettelDate", Hibernate.TIMESTAMP)
                .addScalar("signCtCompletePartner", Hibernate.LONG)
                .addScalar("signCtCompletePartnerDate", Hibernate.TIMESTAMP)
                .addScalar("deployUserId", Hibernate.LONG)
                .addScalar("deployAssignDate", Hibernate.TIMESTAMP)
                .addScalar("deployCloseDate", Hibernate.TIMESTAMP)
                .addScalar("isLock", Hibernate.STRING)
                .addScalar("liStatus", Hibernate.STRING)
                .addScalar("liUserCsp", Hibernate.LONG)
                .addScalar("signLi", Hibernate.LONG)
                .addScalar("signLiDate", Hibernate.TIMESTAMP)
                .addScalar("signLiExpertiseLaw", Hibernate.LONG)
                .addScalar("signLiExpertiseLawDate", Hibernate.TIMESTAMP)
                .addScalar("signLiCompleteViettel", Hibernate.LONG)
                .addScalar("signLiCompleteViettelDate", Hibernate.TIMESTAMP)
                .addScalar("signLiPartner", Hibernate.LONG)
                .addScalar("signLiPartnerDate", Hibernate.TIMESTAMP)
                .addScalar("liStartDate", Hibernate.TIMESTAMP)
                .addScalar("liSignForm", Hibernate.STRING)
                .addScalar("singleLiquidation", Hibernate.LONG)
                .addScalar("singleLiquidationDate", Hibernate.TIMESTAMP)
                .addScalar("singleLiquidationStartDate", Hibernate.TIMESTAMP)
                .addScalar("Idpartner", Hibernate.LONG)
                .addScalar("partnerCode", Hibernate.STRING)
                .addScalar("companyName", Hibernate.STRING)
                .addScalar("companyNameShort", Hibernate.STRING)
                .addScalar("addressOffice", Hibernate.STRING)
                .addScalar("addressTradingOffice", Hibernate.STRING)
                .addScalar("email", Hibernate.STRING)
                .addScalar("businessRegisNumber", Hibernate.STRING)
                .addScalar("businessRegisDate", Hibernate.TIMESTAMP)
                .addScalar("businessRegisCount", Hibernate.STRING)
                .addScalar("partnerGroup", Hibernate.STRING)
                .addScalar("partnerGroupProvince", Hibernate.STRING)
                .addScalar("phone", Hibernate.STRING)
                .addScalar("repName", Hibernate.STRING)
                .addScalar("repPosition", Hibernate.STRING)
                .addScalar("taxCode", Hibernate.STRING)
                .addScalar("updateAt", Hibernate.TIMESTAMP)
                .addScalar("createAt", Hibernate.TIMESTAMP)
                .addScalar("createBy", Hibernate.LONG)
                .addScalar("updateBy", Hibernate.LONG)
                .addScalar("addressHeadOffice", Hibernate.STRING)
                .addScalar("contractStatusCode", Hibernate.STRING)
                .addScalar("contractStatusName", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(ContractDTO.class));
        if (contractDTO != null) {
            if (StringUtils.isValidString(contractDTO.getCompanyName())) {
                query.setParameter("companyName", getParamLike(contractDTO.getCompanyName()));
            }
            if (StringUtils.isValidString(contractDTO.getContractStatusCode())) {
                query.setParameter("contractStatus", contractDTO.getContractStatusCode());
            }
            if (StringUtils.isValidString(contractDTO.getCompanyNameShort())) {
                query.setParameter("companyNameShort",
                        getParamLike(contractDTO.getCompanyNameShort()));
            }
            if (StringUtils.isValidString(contractDTO.getContractNumber())) {
                query.setParameter("contractNumber", getParamLike(contractDTO.getContractNumber()));
            }
            if (StringUtils.isValidString(contractDTO.getPartnerId())) {
                query.setParameter("partnerId", contractDTO.getPartnerId());
            }
            if (StringUtils.isValidString(contractDTO.getContractId())) {
                query.setParameter("contractId", contractDTO.getContractId());
            }
        }
        lstResult = query.list();
        session.getTransaction().commit();
        return lstResult;
    }

    public List<ContractEntity> getListContractNumber(Long id) {
        Session session = getCurrentSession();
        session.beginTransaction();
        Criteria cr = session.createCriteria(ContractEntity.class);
        cr.add(Restrictions.eq("partnerId", id));
        List results = cr.list();
        return (results.size() > 0) ? results : null;
    }

    //    public static void main(String[] a) {
//        Session session = HibernateUtils.getCurrentSession();
//        session.beginTransaction();
//        ContractDAO con = new ContractDAO();
//        con.getListContract(null);
//        session.getTransaction().commit();
//        session.close();
//    }
    public List<ContractDTO> getListContractForWorkManager(ContractDTO contractDTO) {
        List<ContractDTO> lstResult;
        Session session = getCurrentSession();
        session.beginTransaction();
        StringBuilder sql = new StringBuilder()
                .append("SELECT P.PARTNER_CODE partnerCode,")
                .append("C.ID contractId,")
                .append("P.COMPANY_NAME companyName,")
                .append("C.START_DATE startDate,")
                .append("C.CONTRACT_NUMBER contractNumber,")
                .append("C.END_DATE endDate,")
                .append("C.FILING_DATE filingDate,")
                .append("C.HEAD_OF_SERVICE headOfService,")
                .append("ST.NAME contractStatusName,")
                .append("ST.CODE contractStatusCode,")
                .append("SV.CODE serviceCode,")
                .append("SV.NAME serviceName,")
                .append("SV.PARENT_CODE serviceGroup,")
                .append("CT.CODE contractType,")
                .append("CT.NAME contractTypeName,")
                .append("C.CREATE_AT createAt,")
                .append("C.DEPLOY_ASSIGN_DATE deployAssignDate")
                .append(" FROM TBL_CONTRACT C INNER JOIN TBL_PARTNER P ON C.PARTNER_ID = P.ID")
                .append(" INNER JOIN TBL_SERVICE S ON C.SERVICE_CODE = S.CODE")
                .append(" INNER JOIN TBL_CONTRACT_STATUS ST ON ST.CODE = C.CONTRACT_STATUS")
                .append(" INNER JOIN TBL_CONTRACT_TYPE CT ON CT.CODE = C.CONTRACT_TYPE")
                .append(" INNER JOIN TBL_SERVICE SV ON SV.CODE = C.SERVICE_CODE WHERE C.CONTRACT_STATUS != '24' ");
        if (contractDTO != null) {
            if (StringUtils.isValidString(contractDTO.getDeployUserId())) {
                sql.append(" AND C.DEPLOY_USER_ID = :DEPLOY_USER_ID");
            }
            if (StringUtils.isValidString(contractDTO.getPartnerCode())) {
                sql.append(" AND LOWER(P.PARTNER_CODE) LIKE :PARTNER_CODE");
            }
            if (StringUtils.isValidString(contractDTO.getCompanyName())) {
                sql.append(" AND LOWER(P.COMPANY_NAME) LIKE :COMPANY_NAME");
            }
            if (StringUtils.isValidString(contractDTO.getHeadOfService())) {
                sql.append(" AND LOWER(C.HEAD_OF_SERVICE) LIKE :HEAD_OF_SERVICE");
            }
            if (StringUtils.isValidString(contractDTO.getContractNumber())) {
                sql.append(" AND LOWER(C.CONTRACT_NUMBER) LIKE :CONTRACT_NUMBER");
            }
            if (StringUtils.isValidString(contractDTO.getServiceGroup())) {
                sql.append(" AND SV.PARENT_CODE = :PARENT_CODE");
            }
            if (StringUtils.isValidString(contractDTO.getServiceCode())) {
                sql.append(" AND SV.CODE = :CODE");
            }
            if (StringUtils.isValidString(contractDTO.getContractId())) {
                sql.append(" AND C.ID = :CONTRACT_ID");
            }
        }
        sql.append(" ORDER BY TO_NUMBER( to_char( C.update_at, 'RRRRMMDDHH24MISS'), 'XXXXXXXXXXXXXXXXXXXXXXXXX') DESC");
        Query query = session.createSQLQuery(sql.toString())
                .addScalar("partnerCode", Hibernate.STRING)
                .addScalar("contractId", Hibernate.LONG)
                .addScalar("companyName", Hibernate.STRING)
                .addScalar("contractNumber", Hibernate.STRING)
                .addScalar("startDate", Hibernate.TIMESTAMP)
                .addScalar("endDate", Hibernate.TIMESTAMP)
                .addScalar("createAt", Hibernate.TIMESTAMP)
                .addScalar("filingDate", Hibernate.TIMESTAMP)
                .addScalar("headOfService", Hibernate.STRING)
                .addScalar("contractStatusName", Hibernate.STRING)
                .addScalar("contractStatusCode", Hibernate.STRING)
                .addScalar("serviceCode", Hibernate.STRING)
                .addScalar("serviceName", Hibernate.STRING)
                .addScalar("serviceGroup", Hibernate.STRING)
                .addScalar("contractType", Hibernate.STRING)
                .addScalar("contractTypeName", Hibernate.STRING)
                .addScalar("deployAssignDate", Hibernate.TIMESTAMP)
                .setResultTransformer(Transformers.aliasToBean(ContractDTO.class));

        if (contractDTO != null) {
            if (StringUtils.isValidString(contractDTO.getDeployUserId())) {
                query.setParameter("DEPLOY_USER_ID", contractDTO.getDeployUserId());
            }
            if (StringUtils.isValidString(contractDTO.getPartnerCode())) {
                query.setParameter("PARTNER_CODE", StringUtils.getParamLike(contractDTO.getPartnerCode()));
            }
            if (StringUtils.isValidString(contractDTO.getCompanyName())) {
                query.setParameter("COMPANY_NAME", StringUtils.getParamLike(contractDTO.getCompanyName()));
            }
            if (StringUtils.isValidString(contractDTO.getHeadOfService())) {
                query.setParameter("HEAD_OF_SERVICE", StringUtils.getParamLike(contractDTO.getHeadOfService()));
            }
            if (StringUtils.isValidString(contractDTO.getContractNumber())) {
                query.setParameter("CONTRACT_NUMBER", StringUtils.getParamLike(contractDTO.getContractNumber()));
            }
            if (StringUtils.isValidString(contractDTO.getServiceGroup())) {
                query.setParameter("PARENT_CODE", contractDTO.getServiceGroup());
            }
            if (StringUtils.isValidString(contractDTO.getServiceCode())) {
                query.setParameter("CODE", contractDTO.getServiceCode());
            }
            if (StringUtils.isValidString(contractDTO.getContractId())) {
                query.setParameter("CONTRACT_ID", contractDTO.getContractId());
            }
        }
        lstResult = query.list();
        session.getTransaction().commit();
        return lstResult;
    }

    public List<ContractDTO> getListContractFiles(ContractDTO contractDTO) throws HibernateException {
        List<ContractDTO> lstResult = new ArrayList<>();
        Session session = getCurrentSession();
        session.beginTransaction();
        StringBuilder sql = new StringBuilder()
                .append("select  ")
                .append("a.id contractId,")
                .append("a.contract_type contractType,")
                .append("a.contract_status contractStatus,")
                .append("a.contract_number contractNumber,")
                .append("a.partner_id partnerId,")
                .append("a.service_code serviceCode,")
                .append("a.head_of_service headOfService,")
                .append("a.ct_sign_form ctSignForm,")
                .append("a.start_date startDate,")
                .append("a.end_date endDate,")
                .append("a.create_by  createBy,")
                .append("a.create_at createAt,")
                .append("a.update_by updateBy,")
                .append("a.update_at updateAt,")
                .append("a.filing_date filingDate,")
                .append("a.filing_by  filingBy,")
                .append("a.ct_user_csp ctUserCsp,")
                .append("a.sign_ct_approve signCtApprove,")
                .append("a.sign_ct_approve_date  signCtApproveDate,")
                .append("a.sign_ct_expertise_law signCtExpertiseLaw,")
                .append("a.sign_ct_expertise_law_date signCtExpertiseLawDate,")
                .append("a.sign_ct_complete_viettel signCtCompleteViettel,")
                .append("a.sign_ct_complete_viettel_date signCtCompleteViettelDate,")
                .append("a.sign_ct_complete_partner signCtCompletePartner,")
                .append("a.sign_ct_complete_partner_date signCtCompletePartnerDate,")
                .append("a.deploy_user_id deployUserId,")
                .append("a.deploy_assign_date deployAssignDate,")
                .append("a.deploy_close_date deployCloseDate,")
                .append("a.is_lock isLock,")
                .append("a.li_status liStatus,")
                .append("a.li_user_csp liUserCsp,")
                .append("a.sign_li signLi,")
                .append("a.sign_li_date signLiDate,")
                .append("a.sign_li_expertise_law      signLiExpertiseLaw,")
                .append("a.sign_li_expertise_law_date   signLiExpertiseLawDate,")
                .append("a.sign_li_complete_viettel     signLiCompleteViettel,")
                .append("a.sign_li_complete_viettel_date  signLiCompleteViettelDate,")
                .append("a.sign_li_partner signLiPartner,")
                .append("a.sign_li_partner_date  signLiPartnerDate,")
                .append("a.li_start_date  liStartDate,")
                .append("a.li_sign_form  liSignForm,")
                .append("a.single_liquidation  singleLiquidation,")
                .append("a.single_liquidation_date  singleLiquidationDate,")
                .append("a.single_liquidation_start_date  singleLiquidationStartDate,")
                .append("b.id Idpartner,")
                .append("b.partner_code  partnerCode, ")
                .append("b.company_name  companyName, ")
                .append("b.company_name_short companyNameShort, ")
                .append("b.address_head_office  addressOffice, ")
                .append("b.address_trading_office   addressTradingOffice, ")
                .append("b.email   email, ")
                .append("b.business_regis_number   businessRegisNumber, ")
                .append("b.business_regis_date businessRegisDate, ")
                .append("b.business_regis_count businessRegisCount, ")
                .append("b.partner_group partnerGroup, ")
                .append("b.partner_group_province partnerGroupProvince, ")
                .append("b.phone phone, ")
                .append("b.rep_name repName, ")
                .append("b.rep_position repPosition, ")
                .append("b.tax_code taxCode, ")
                .append("b.update_at updateAt, ")
                .append("b.create_at createAt, ")
                .append("b.create_by createBy, ")
                .append("b.update_by updateBy, ")
                .append("b.address_head_office addressHeadOffice, ")
                .append("c.code contractStatusCode, ")
                .append("c.name contractStatusName  ")
                .append(" from ")
                .append("tbl_contract a left join tbl_partner b on a.partner_id = b.id left join tbl_contract_status c on a.contract_status = c.code")
                .append("   WHERE   1 = 1  and a.contract_status in (1,2,3,4,5,6,7,8,9,10,11)");
        if (contractDTO != null) {
            if (StringUtils.isValidString(contractDTO.getCompanyName())) {
                sql.append("AND UPPER(b.company_name) LIKE UPPER(:companyName) ");
            }
            if (StringUtils.isValidString(contractDTO.getContractStatusCode())) {
                sql.append(" AND LOWER(a.contract_status) = :contractStatus ");
            }
            if (StringUtils.isValidString(contractDTO.getCompanyNameShort())) {
                sql.append(" AND UPPER(b.company_name_short) LIKE UPPER(:companyNameShort) ");
            }
            if (StringUtils.isValidString(contractDTO.getContractNumber())) {
                sql.append(" AND UPPER(a.contract_number) LIKE UPPER(:contractNumber) ");
            }
            if (StringUtils.isValidString(contractDTO.getContractId())) {
                sql.append(" AND a.id = :contractId");
            }
            if (StringUtils.isValidString(contractDTO.getPartnerId())) {
                sql.append(" AND a.partner_id = :partnerId ");
            }
        }
        sql.append(" order by a.update_at desc");
        Query query = session.createSQLQuery(sql.toString())
                .addScalar("contractId", Hibernate.LONG)
                .addScalar("contractType", Hibernate.STRING)
                .addScalar("contractStatus", Hibernate.STRING)
                .addScalar("contractNumber", Hibernate.STRING)
                .addScalar("partnerId", Hibernate.LONG)
                .addScalar("serviceCode", Hibernate.STRING)
                .addScalar("headOfService", Hibernate.STRING)
                .addScalar("ctSignForm", Hibernate.STRING)
                .addScalar("startDate", Hibernate.TIMESTAMP)
                .addScalar("endDate", Hibernate.TIMESTAMP)
                .addScalar("createBy", Hibernate.LONG)
                .addScalar("createAt", Hibernate.TIMESTAMP)
                .addScalar("updateBy", Hibernate.LONG)
                .addScalar("updateAt", Hibernate.TIMESTAMP)
                .addScalar("filingDate", Hibernate.TIMESTAMP)
                .addScalar("filingBy", Hibernate.LONG)
                .addScalar("ctUserCsp", Hibernate.LONG)
                .addScalar("signCtApprove", Hibernate.LONG)
                .addScalar("signCtApproveDate", Hibernate.TIMESTAMP)
                .addScalar("signCtExpertiseLaw", Hibernate.LONG)
                .addScalar("signCtExpertiseLawDate", Hibernate.TIMESTAMP)
                .addScalar("signCtCompleteViettel", Hibernate.LONG)
                .addScalar("signCtCompleteViettelDate", Hibernate.TIMESTAMP)
                .addScalar("signCtCompletePartner", Hibernate.LONG)
                .addScalar("signCtCompletePartnerDate", Hibernate.TIMESTAMP)
                .addScalar("deployUserId", Hibernate.LONG)
                .addScalar("deployAssignDate", Hibernate.TIMESTAMP)
                .addScalar("deployCloseDate", Hibernate.TIMESTAMP)
                .addScalar("isLock", Hibernate.STRING)
                .addScalar("liStatus", Hibernate.STRING)
                .addScalar("liUserCsp", Hibernate.LONG)
                .addScalar("signLi", Hibernate.LONG)
                .addScalar("signLiDate", Hibernate.TIMESTAMP)
                .addScalar("signLiExpertiseLaw", Hibernate.LONG)
                .addScalar("signLiExpertiseLawDate", Hibernate.TIMESTAMP)
                .addScalar("signLiCompleteViettel", Hibernate.LONG)
                .addScalar("signLiCompleteViettelDate", Hibernate.TIMESTAMP)
                .addScalar("signLiPartner", Hibernate.LONG)
                .addScalar("signLiPartnerDate", Hibernate.TIMESTAMP)
                .addScalar("liStartDate", Hibernate.TIMESTAMP)
                .addScalar("liSignForm", Hibernate.STRING)
                .addScalar("singleLiquidation", Hibernate.LONG)
                .addScalar("singleLiquidationDate", Hibernate.TIMESTAMP)
                .addScalar("singleLiquidationStartDate", Hibernate.TIMESTAMP)
                .addScalar("Idpartner", Hibernate.LONG)
                .addScalar("partnerCode", Hibernate.STRING)
                .addScalar("companyName", Hibernate.STRING)
                .addScalar("companyNameShort", Hibernate.STRING)
                .addScalar("addressOffice", Hibernate.STRING)
                .addScalar("addressTradingOffice", Hibernate.STRING)
                .addScalar("email", Hibernate.STRING)
                .addScalar("businessRegisNumber", Hibernate.STRING)
                .addScalar("businessRegisDate", Hibernate.TIMESTAMP)
                .addScalar("businessRegisCount", Hibernate.STRING)
                .addScalar("partnerGroup", Hibernate.STRING)
                .addScalar("partnerGroupProvince", Hibernate.STRING)
                .addScalar("phone", Hibernate.STRING)
                .addScalar("repName", Hibernate.STRING)
                .addScalar("repPosition", Hibernate.STRING)
                .addScalar("taxCode", Hibernate.STRING)
                .addScalar("updateAt", Hibernate.TIMESTAMP)
                .addScalar("createAt", Hibernate.TIMESTAMP)
                .addScalar("createBy", Hibernate.LONG)
                .addScalar("updateBy", Hibernate.LONG)
                .addScalar("addressHeadOffice", Hibernate.STRING)
                .addScalar("contractStatusCode", Hibernate.STRING)
                .addScalar("contractStatusName", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(ContractDTO.class));
        if (contractDTO != null) {
            if (StringUtils.isValidString(contractDTO.getCompanyName())) {
                query.setParameter("companyName", getParamLike(contractDTO.getCompanyName()));
            }
            if (StringUtils.isValidString(contractDTO.getContractStatusCode())) {
                query.setParameter("contractStatus", contractDTO.getContractStatusCode());
            }
            if (StringUtils.isValidString(contractDTO.getCompanyNameShort())) {
                query.setParameter("companyNameShort",
                        getParamLike(contractDTO.getCompanyNameShort()));
            }
            if (StringUtils.isValidString(contractDTO.getContractNumber())) {
                query.setParameter("contractNumber", getParamLike(contractDTO.getContractNumber()));
            }
            if (StringUtils.isValidString(contractDTO.getContractId())) {
                query.setParameter("contractId", contractDTO.getContractId());
            }

            if (StringUtils.isValidString(contractDTO.getPartnerId())) {
                query.setParameter("partnerId", contractDTO.getPartnerId());
            }
        }
        lstResult = query.list();
        session.getTransaction().commit();
        return lstResult;
    }

    public List<ContractDTO> getListLiquidation(ContractDTO contractDTO) throws HibernateException {
        List<ContractDTO> lstResult = new ArrayList<>();
        Session session = getCurrentSession();
        session.beginTransaction();
        StringBuilder sql = new StringBuilder()
                .append("select  ")
                .append("a.id contractId,")
                .append("a.contract_type contractType,")
                .append("a.contract_status contractStatus,")
                .append("a.contract_number contractNumber,")
                .append("a.partner_id partnerId,")
                .append("a.service_code serviceCode,")
                .append("a.head_of_service headOfService,")
                .append("a.ct_sign_form ctSignForm,")
                .append("a.start_date startDate,")
                .append("a.end_date endDate,")
                .append("a.create_by  createBy,")
                .append("a.create_at createAt,")
                .append("a.update_by updateBy,")
                .append("a.update_at updateAt,")
                .append("a.filing_date filingDate,")
                .append("a.filing_by  filingBy,")
                .append("a.ct_user_csp ctUserCsp,")
                .append("a.sign_ct_approve signCtApprove,")
                .append("a.sign_ct_approve_date  signCtApproveDate,")
                .append("a.sign_ct_expertise_law signCtExpertiseLaw,")
                .append("a.sign_ct_expertise_law_date signCtExpertiseLawDate,")
                .append("a.sign_ct_complete_viettel signCtCompleteViettel,")
                .append("a.sign_ct_complete_viettel_date signCtCompleteViettelDate,")
                .append("a.sign_ct_complete_partner signCtCompletePartner,")
                .append("a.sign_ct_complete_partner_date signCtCompletePartnerDate,")
                .append("a.deploy_user_id deployUserId,")
                .append("a.deploy_assign_date deployAssignDate,")
                .append("a.deploy_close_date deployCloseDate,")
                .append("a.is_lock isLock,")
                .append("a.li_status liStatus,")
                .append("a.li_user_csp liUserCsp,")
                .append("a.sign_li signLi,")
                .append("a.sign_li_date signLiDate,")
                .append("a.sign_li_expertise_law      signLiExpertiseLaw,")
                .append("a.sign_li_expertise_law_date   signLiExpertiseLawDate,")
                .append("a.sign_li_complete_viettel     signLiCompleteViettel,")
                .append("a.sign_li_complete_viettel_date  signLiCompleteViettelDate,")
                .append("a.sign_li_partner signLiPartner,")
                .append("a.sign_li_partner_date  signLiPartnerDate,")
                .append("a.li_start_date  liStartDate,")
                .append("a.li_sign_form  liSignForm,")
                .append("a.single_liquidation  singleLiquidation,")
                .append("a.single_liquidation_date  singleLiquidationDate,")
                .append("a.single_liquidation_start_date  singleLiquidationStartDate,")
                .append("b.id Idpartner,")
                .append("b.partner_code  partnerCode, ")
                .append("b.company_name  companyName, ")
                .append("b.company_name_short companyNameShort, ")
                .append("b.address_head_office  addressOffice, ")
                .append("b.address_trading_office   addressTradingOffice, ")
                .append("b.email   email, ")
                .append("b.business_regis_number   businessRegisNumber, ")
                .append("b.business_regis_date businessRegisDate, ")
                .append("b.business_regis_count businessRegisCount, ")
                .append("b.partner_group partnerGroup, ")
                .append("b.partner_group_province partnerGroupProvince, ")
                .append("b.phone phone, ")
                .append("b.rep_name repName, ")
                .append("b.rep_position repPosition, ")
                .append("b.tax_code taxCode, ")
                .append("b.update_at updateAt, ")
                .append("b.create_at createAt, ")
                .append("b.create_by createBy, ")
                .append("b.update_by updateBy, ")
                .append("b.address_head_office addressHeadOffice, ")
                .append("c.code contractStatusCode, ")
                .append("c.name contractStatusName  ")
                .append(" from ")
                .append("tbl_contract a left join tbl_partner b on a.partner_id = b.id left join tbl_contract_status c on a.contract_status = c.code")
                .append("   WHERE   1 = 1  and a.contract_status in ('26','TL01','TL02','TL03','TL04','TL05','TL06','TL07','TL08','TL09','TL10','TL11','TL12')");
        if (contractDTO != null) {
            if (StringUtils.isValidString(contractDTO.getCompanyName())) {
                sql.append("AND UPPER(b.company_name) LIKE UPPER(:companyName) ");
            }
            if (StringUtils.isValidString(contractDTO.getContractStatusCode())) {
                sql.append(" AND LOWER(a.contract_status) = :contractStatus ");
            }
            if (StringUtils.isValidString(contractDTO.getCompanyNameShort())) {
                sql.append(" AND UPPER(b.company_name_short) LIKE UPPER(:companyNameShort) ");
            }
            if (StringUtils.isValidString(contractDTO.getContractNumber())) {
                sql.append(" AND UPPER(a.contract_number) LIKE UPPER(:contractNumber) ");
            }
            if (StringUtils.isValidString(contractDTO.getContractId())) {
                sql.append(" AND a.id = :contractId");
            }
            if (StringUtils.isValidString(contractDTO.getPartnerId())) {
                sql.append(" AND a.partner_id = :partnerId ");
            }
        }
        sql.append(" order by a.update_at desc");

        Query query = session.createSQLQuery(sql.toString())
                .addScalar("contractId", Hibernate.LONG)
                .addScalar("contractType", Hibernate.STRING)
                .addScalar("contractStatus", Hibernate.STRING)
                .addScalar("contractNumber", Hibernate.STRING)
                .addScalar("partnerId", Hibernate.LONG)
                .addScalar("serviceCode", Hibernate.STRING)
                .addScalar("headOfService", Hibernate.STRING)
                .addScalar("ctSignForm", Hibernate.STRING)
                .addScalar("startDate", Hibernate.TIMESTAMP)
                .addScalar("endDate", Hibernate.TIMESTAMP)
                .addScalar("createBy", Hibernate.LONG)
                .addScalar("createAt", Hibernate.TIMESTAMP)
                .addScalar("updateBy", Hibernate.LONG)
                .addScalar("updateAt", Hibernate.TIMESTAMP)
                .addScalar("filingDate", Hibernate.TIMESTAMP)
                .addScalar("filingBy", Hibernate.LONG)
                .addScalar("ctUserCsp", Hibernate.LONG)
                .addScalar("signCtApprove", Hibernate.LONG)
                .addScalar("signCtApproveDate", Hibernate.TIMESTAMP)
                .addScalar("signCtExpertiseLaw", Hibernate.LONG)
                .addScalar("signCtExpertiseLawDate", Hibernate.TIMESTAMP)
                .addScalar("signCtCompleteViettel", Hibernate.LONG)
                .addScalar("signCtCompleteViettelDate", Hibernate.TIMESTAMP)
                .addScalar("signCtCompletePartner", Hibernate.LONG)
                .addScalar("signCtCompletePartnerDate", Hibernate.TIMESTAMP)
                .addScalar("deployUserId", Hibernate.LONG)
                .addScalar("deployAssignDate", Hibernate.TIMESTAMP)
                .addScalar("deployCloseDate", Hibernate.TIMESTAMP)
                .addScalar("isLock", Hibernate.STRING)
                .addScalar("liStatus", Hibernate.STRING)
                .addScalar("liUserCsp", Hibernate.LONG)
                .addScalar("signLi", Hibernate.LONG)
                .addScalar("signLiDate", Hibernate.DATE)
                .addScalar("signLiExpertiseLaw", Hibernate.LONG)
                .addScalar("signLiExpertiseLawDate", Hibernate.DATE)
                .addScalar("signLiCompleteViettel", Hibernate.LONG)
                .addScalar("signLiCompleteViettelDate", Hibernate.DATE)
                .addScalar("signLiPartner", Hibernate.LONG)
                .addScalar("signLiPartnerDate", Hibernate.DATE)
                .addScalar("liStartDate", Hibernate.DATE)
                .addScalar("liSignForm", Hibernate.STRING)
                .addScalar("singleLiquidation", Hibernate.LONG)
                .addScalar("singleLiquidationDate", Hibernate.DATE)
                .addScalar("singleLiquidationStartDate", Hibernate.DATE)
                .addScalar("Idpartner", Hibernate.LONG)
                .addScalar("partnerCode", Hibernate.STRING)
                .addScalar("companyName", Hibernate.STRING)
                .addScalar("companyNameShort", Hibernate.STRING)
                .addScalar("addressOffice", Hibernate.STRING)
                .addScalar("addressTradingOffice", Hibernate.STRING)
                .addScalar("email", Hibernate.STRING)
                .addScalar("businessRegisNumber", Hibernate.STRING)
                .addScalar("businessRegisDate", Hibernate.DATE)
                .addScalar("businessRegisCount", Hibernate.STRING)
                .addScalar("partnerGroup", Hibernate.STRING)
                .addScalar("partnerGroupProvince", Hibernate.STRING)
                .addScalar("phone", Hibernate.STRING)
                .addScalar("repName", Hibernate.STRING)
                .addScalar("repPosition", Hibernate.STRING)
                .addScalar("taxCode", Hibernate.STRING)
                .addScalar("updateAt", Hibernate.DATE)
                .addScalar("createAt", Hibernate.DATE)
                .addScalar("createBy", Hibernate.LONG)
                .addScalar("updateBy", Hibernate.LONG)
                .addScalar("addressHeadOffice", Hibernate.STRING)
                .addScalar("contractStatusCode", Hibernate.STRING)
                .addScalar("contractStatusName", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(ContractDTO.class));
        if (contractDTO != null) {
            if (StringUtils.isValidString(contractDTO.getCompanyName())) {
                query.setParameter("companyName", getParamLike(contractDTO.getCompanyName()));
            }
            if (StringUtils.isValidString(contractDTO.getContractStatusCode())) {
                query.setParameter("contractStatus", contractDTO.getContractStatusCode());
            }
            if (StringUtils.isValidString(contractDTO.getCompanyNameShort())) {
                query.setParameter("companyNameShort",
                        getParamLike(contractDTO.getCompanyNameShort()));
            }
            if (StringUtils.isValidString(contractDTO.getContractNumber())) {
                query.setParameter("contractNumber", getParamLike(contractDTO.getContractNumber()));
            }
            if (StringUtils.isValidString(contractDTO.getContractId())) {
                query.setParameter("contractId", contractDTO.getContractId());
            }
        }
        lstResult = query.list();
        session.getTransaction().commit();
        return lstResult;
    }
}
