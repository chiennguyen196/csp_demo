/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.DAO;

import com.viettel.csp.DTO.ContractHisDTO;
import com.viettel.csp.entity.ContractHisEntity;
import com.viettel.csp.util.StringUtils;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

/**
 *
 * @author TranChi
 */
public class ContractHisDAO extends BaseCustomDAO<ContractHisEntity> {

    private static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ContractHisDAO.class);

    public ContractHisDAO() {
        tEntity = new ContractHisEntity();
        tEntityClass = ContractHisEntity.class;
    }

    public List<ContractHisDTO> getListContractHis(ContractHisDTO contractHisDTO) throws HibernateException {
        List<ContractHisDTO> lstResult = new ArrayList<>();
        Session session = getCurrentSession();
        session.beginTransaction();
        StringBuilder sql = new StringBuilder()
                .append("select  ")
                .append("a.id id,")
                .append("a.create_By createBy,")
                .append("b.create_At createAt,")
                .append("b.status_Before  statusBefore, ")
                .append("b.status_After statusAfter, ")
                .append("b.content  content ")
                .append(" from ")
                .append("tbl_contract_his b , tbl_contract a where  b.contract_id = a.id and b.contract_id=:contractId order by b.create_At desc");

        Query query = session.createSQLQuery(sql.toString())
                .addScalar("id", Hibernate.LONG)
                .addScalar("createBy", Hibernate.LONG)
                .addScalar("createAt", Hibernate.TIMESTAMP)
                .addScalar("statusBefore", Hibernate.STRING)
                .addScalar("statusAfter", Hibernate.STRING)
                .addScalar("content", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(ContractHisDTO.class));
        query.setParameter("contractId", contractHisDTO.getContractId());
        lstResult = query.list();
        session.getTransaction().commit();
        return lstResult;
    }

    public List<ContractHisDTO> getListContractHisAddUser(ContractHisDTO contractHisDTO) throws HibernateException {
        List<ContractHisDTO> lstResult = new ArrayList<>();
        Session session = getCurrentSession();
        session.beginTransaction();
        StringBuilder sql = new StringBuilder()
                .append("select a.id id,u.FULLNAME fullName,")
                .append("  a.create_By createBy,")
                .append("  b.create_At createAt,")
                .append("  b.status_Before  statusBefore,")
                .append("  b.status_After statusAfter,")
                .append("  b.content  content")
                .append("  from")
                .append("  tbl_contract_his b")
                .append("  INNER JOIN TBL_USER u On u.ID = b.CREATE_BY")
                .append("  INNER JOIN  tbl_contract a on a.ID = b.CONTRACT_ID");
        if (StringUtils.isValidString(contractHisDTO.getContractId())) {
            sql.append("  where  b.contract_id= :contractId");
        }
        Query query = session.createSQLQuery(sql.toString())
                .addScalar("id", Hibernate.LONG)
                .addScalar("createBy", Hibernate.LONG)
                .addScalar("createAt", Hibernate.DATE)
                .addScalar("statusBefore", Hibernate.STRING)
                .addScalar("statusAfter", Hibernate.STRING)
                .addScalar("content", Hibernate.STRING)
                .addScalar("fullName", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(ContractHisDTO.class));
        if (StringUtils.isValidString(contractHisDTO.getContractId())) {
            query.setParameter("contractId", contractHisDTO.getContractId());
        }
        lstResult = query.list();
        session.getTransaction().commit();
        return lstResult;
    }
}
