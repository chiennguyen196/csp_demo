/*
 * Copyright (C) 2011 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.viettel.csp.DAO;

import com.viettel.csp.config.HibernateUtils;
import com.viettel.csp.entity.BaseCustomEntity;
import com.viettel.csp.util.ConditionBean;
import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.util.ReflectUtils;
import com.viettel.csp.util.StringUtils;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;

/**
 * @author TienNV@gemvietnam.com
 * @version:
 */
public class BaseCustomDAO<T extends BaseCustomEntity> {

    private static final String selectTocharSysdate = "select to_char(sysdate,'dd/mm/yyyy hh24:mi:ss') from dual";
    protected static Logger log = Logger.getLogger(BaseCustomDAO.class);
    protected T tEntity;
    protected Class<T> tEntityClass;

    public SessionFactory getSessionFactory() {
        return HibernateUtils.getSessionFactory();
    }

    protected Session getCurrentSession() {
        Session session = null;
        try {
            session = HibernateUtils.getCurrentSession();
            if (session == null) {
                log.error("Can not create session in hibernate");
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return session;
    }

    protected Session getSession() {
        Session session = HibernateUtils.getSession();
        return session;
    }

    public String deleteById(Long id) {
        try {
            Session session = getCurrentSession();
            session.beginTransaction();
            session.delete(findById(id));
            session.getTransaction().commit();
            return ParamUtils.SUCCESS;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    public T findById(Long id) {
        Session session = getCurrentSession();
        session.beginTransaction();
        StringBuilder sql = new StringBuilder("from ").append(tEntity.getBOName())
                .append(" where ").append(tEntity.getColId()).append("=:id");
        Query q = session.createQuery(sql.toString());
        q.setLong("id", id);
        List<T> lst = q.list();
        session.getTransaction().commit();
        if (lst.isEmpty()) {
            return null;
        } else {
            return lst.get(0);
        }
    }

    public List<T> find(String boName, List<ConditionBean> lstCondition) {
        return find(boName, lstCondition, null, 0, 0, null);
    }

    public List<T> find(String boName, ConditionBean... lstCondition) {
        return find(boName, Arrays.asList(lstCondition), null, 0, 0, null);
    }

    public List<T> find(String boName, List<ConditionBean> lstCondition, String logic) {
        return find(boName, lstCondition, null, 0, 0, logic);
    }

    public List<T> find(String boName, List<ConditionBean> lstCondition, String order, int start, int maxResult) {
        return find(boName, lstCondition, order, start, maxResult, null);
    }

    public List<T> find(String boName, List<ConditionBean> lstCondition, String order, int start, int maxResult, String logic) {
        try {
            StringBuilder sql = new StringBuilder();
            sql.append(" from ");
            sql.append(boName);
            sql.append(" where 1=1 ");
            if (lstCondition != null) {
                buildConditionQuery(sql, lstCondition);
            }
            if (order != null) {
                sql.append(" order by ");
                sql.append(order);
            }
            System.out.println("GENENATE SQL: " + sql.toString());
            Session session = getCurrentSession();
            session.beginTransaction();
            Query query = session.createQuery(sql.toString());
            if (maxResult != 0) {
                query.setFirstResult(start);
                query.setMaxResults(maxResult);
            }
            fillConditionQuery(query, lstCondition);
            List lst = query.list();
            session.getTransaction().commit();
            return lst;
        } catch (HibernateException he) {
            log.error(he.getMessage(), he);
            throw he;
        }
    }

    public List<T> findAll() {
        return findAll("default session", new Order[0]);
    }

    public List<T> findAll(Order order) {
        Order orders[] = new Order[1];
        orders[0] = order;
        return findAll("default session", orders);
    }

    public T saveOrUpdate(T obj) {
        try {
            Session session = getCurrentSession();
            session.beginTransaction();
            session.saveOrUpdate(obj);
            session.getTransaction().commit();
            BaseCustomEntity.putToMap(obj.getBOName(), new Date());
            return obj;
        } catch (HibernateException he) {
            log.error(he.getMessage(), he);
        }
        return null;
    }

    public T insert(T obj) {
        try {
            Session session = getCurrentSession();
            session.beginTransaction();
            session.save(obj);
            session.getTransaction().commit();
            BaseCustomEntity.putToMap(obj.getBOName(), new Date());
            return obj;
        } catch (HibernateException he) {
            log.error(he.getMessage(), he);
        }
        return null;
    }

    public T update(T obj) {
        try {
            Session session = getCurrentSession();
            session.beginTransaction();
            session.update(obj);
            session.getTransaction().commit();
            BaseCustomEntity.putToMap(obj.getBOName(), new Date());
            return obj;
        } catch (HibernateException he) {
            log.error(he.getMessage(), he);
        }
        return null;
    }

    public String delete(T obj) {
        try {
            Session session = getCurrentSession();
            session.beginTransaction();
            session.delete(obj);
            session.getTransaction().commit();
            return ParamUtils.SUCCESS;
        } catch (HibernateException he) {
            log.error(he.getMessage(), he);
            return he.getMessage();
        }
    }

    public String merge(T obj) throws Exception {
        try {
            Session session = getCurrentSession();
            session.beginTransaction();
            session.merge(obj);
            session.getTransaction().commit();
            BaseCustomEntity.putToMap(obj.getBOName(), new Date());
            return ParamUtils.SUCCESS;
        } catch (HibernateException he) {
            log.error(he.getMessage(), he);
            return he.getMessage();
        }
    }

    public List<T> findByCriteria(
            String sessionName, int firstResult, int maxResult,
            ProjectionList projs, Criterion[] criterion, Order[] orders) {
        try {
            Session session = getCurrentSession();
            session.beginTransaction();
            Criteria crit = session.createCriteria(tEntityClass);
            session.beginTransaction();
            if (projs != null) {
                crit.setProjection(projs);
            }
            if (criterion != null && criterion.length > 0) {
                for (Criterion c : criterion) {
                    crit.add(c);
                }
            }
            if (orders != null && orders.length > 0) {
                for (Order o : orders) {
                    crit.addOrder(o);
                }
            }
            if (firstResult > 0) {
                crit.setFirstResult(firstResult);
            }
            if (maxResult > 0) {
                crit.setMaxResults(maxResult);
            }
            List lst = crit.list();
            session.getTransaction().commit();
            return lst;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }

    public List<T> findByCriteria(ProjectionList projections,
                                  Criterion[] criterions, Order[] orders) {
        return findByCriteria("default session", 0, 0,
                projections, criterions, orders);
    }

    public List<T> findByCriteria(ProjectionList projections,
                                  Criterion[] criterions, Order order) {
        Order orders[] = new Order[1];
        orders[0] = order;
        return findByCriteria("default session", 0, 0,
                projections, criterions, orders);
    }

    public List<T> findByCriteria(ProjectionList projections, Order order) {
        Order orders[] = new Order[1];
        orders[0] = order;
        return findByCriteria("default session", 0, 0,
                projections, null, orders);
    }

    public List<T> findByCriteria(
            String sessionName, int firstResult, int maxResult,
            Criterion[] criterions, Order[] orders) {
        return findByCriteria(sessionName, firstResult, maxResult,
                null, criterions, orders);
    }

    public List<T> findByCriteria(int firstResult, int maxResult,
                                  Criterion[] criterion, Order[] orders) {
        return findByCriteria("default session",
                firstResult, maxResult, criterion, orders);
    }

    public List<T> findByCriteria(
            String sessionName, Criterion[] criterion, Order[] orders) {
        return findByCriteria(sessionName, 0, 0, criterion, orders);
    }

    public List<T> findByCriteria(Criterion[] criterion, Order[] orders) {
        return findByCriteria("default session", criterion, orders);
    }

    public List<T> findByCriteria(Criterion[] criterion, Order order) {
        Order orders[] = new Order[1];
        orders[0] = order;
        return findByCriteria("default session", criterion, orders);
    }

    public List<T> findByCriteria(Criterion crit, Order order) {
        Order orders[] = new Order[1];
        orders[0] = order;
        Criterion[] criterion = new Criterion[1];
        criterion[0] = crit;
        return findByCriteria("default session", criterion, orders);
    }

    public List<T> findByCriteria(String sessionName,
                                  int firstResult, int maxResult, Order[] orders) {
        return findByCriteria(sessionName,
                firstResult, maxResult, new Criterion[0], orders);
    }

    public List<T> findByCriteria(int firstResult, int maxResult, Order[] orders) {
        return findByCriteria("default session",
                firstResult, maxResult, new Criterion[0], orders);
    }

    public List<T> findByCriteria(String sessionName,
                                  int firstResult, int maxResult) {
        return findByCriteria(sessionName, firstResult, maxResult, new Order[0]);
    }

    public List<T> findByCriteria(int firstResult, int maxResult) {
        return findByCriteria("default session", firstResult, maxResult);
    }

    public List<T> findAll(String sessionName, Order[] orders) {
        return findByCriteria(sessionName, new Criterion[0], orders);
    }

    public List<T> findAll(Order[] orders) {
        return findAll("default session", orders);
    }

    public List<T> findDistinct(String[] pros, Criterion[] crits) {
        ProjectionList listProjections = Projections.projectionList();
        ProjectionList listProperties = Projections.projectionList();
        Order[] orders = new Order[pros.length];
        int index = 0;
        for (String property : pros) {
            listProperties.add(Projections.property(property));
            orders[index++] = Order.asc(property);
        }
        listProjections.add(Projections.distinct(listProperties));
        return findByCriteria("default session", 0, 0, listProjections, crits, orders);
    }

    public List<T> findDistinct(String[] pros) {
        return findDistinct(pros, new Criterion[0]);
    }

    public List<T> findByCriteria(Criterion[] crits) {
        return findByCriteria(crits, new Order[0]);
    }

    public List<T> findByCondition(int firstResult, int maxResult, Criterion[] crits) {
        return findByCriteria("default session", firstResult, maxResult, crits, new Order[0]);
    }

    public String delete(String boName, List<ConditionBean> lstCondition) {
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("delete from ");
            sql.append(boName);
            sql.append(" where 1=1 ");
            int result = 0;
            if (lstCondition != null) {
                Session session = getCurrentSession();
                session.beginTransaction();
                buildConditionQuery(sql, lstCondition);
                Query query = session.createQuery(sql.toString());
                fillConditionQuery(query, lstCondition);
                result = query.executeUpdate();
                session.getTransaction().commit();
            }
            if (result == 0) {
                return ParamUtils.FAIL;
            } else {
                BaseCustomEntity.putToMap(boName, new Date());
                return ParamUtils.SUCCESS;
            }
        } catch (HibernateException he) {
            log.error(he.getMessage(), he);
            return he.getMessage();
        }
    }

    public String delete(String boName, List<ConditionBean> lstCondition, Long transId) {
        try {
            List<T> listDelete = find(boName, lstCondition);
            if (listDelete != null && !listDelete.isEmpty()) {
                for (T obj : listDelete) {
                    Session session = getCurrentSession();
                    session.beginTransaction();
                    session.delete(obj);
                    session.getTransaction().commit();
                }
            }
            BaseCustomEntity.putToMap(boName, new Date());
            return ParamUtils.SUCCESS;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return e.getMessage();
        }

    }

    public long count(String boName, List<ConditionBean> lstCondition) {
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("select count(*) from ");
            sql.append(boName);
            sql.append(" where 1=1 ");
            long result = 0;
            if (lstCondition != null) {
                buildConditionQuery(sql, lstCondition);

                Session session = getCurrentSession();
                session.beginTransaction();
                Query query = session.createQuery(sql.toString());
                fillConditionQuery(query, lstCondition);
                result = (Long) query.list().get(0);
                session.getTransaction().commit();
            }
            return result;
        } catch (HibernateException he) {
            log.error(he.getMessage(), he);
            return 0l;
        }
    }

    public long count(T bo) {
        return count(bo.getClass().getSimpleName(), prepareCondition(bo));
    }

    public void buildConditionQuery(StringBuilder sql, List<ConditionBean> lstCondition) {
        if (lstCondition != null) {
            int index = 0;
            for (ConditionBean con : lstCondition) {
                sql.append(ParamUtils.LOGIC_AND);
                //Param
                if (ParamUtils.TYPE_STRING.equals(con.getType())) {
                    sql.append(" lower(").append(con.getField()).append(") ");
                } else {
                    sql.append(con.getField());
                }
                //Operator
                if (StringUtils.isValidString(con.getOperator())) {
                    sql.append(con.getOperator());
                } else {
                    sql.append(" = ");
                }
                if (con.getOperator().equals(ParamUtils.OP_IS_NULL) || con.getOperator().equals(ParamUtils.OP_IS_NOT_NULL)) {
                } else if (ParamUtils.TYPE_STRING.equals(con.getType())) {
                    sql.append("lower(:idx").append(String.valueOf(index++)).append(") ");
                    if (con.getOperator().equals(ParamUtils.OP_LIKE)) {
                        sql.append(" ESCAPE '\\' ");
                    }
                } else if (con.getOperator().equals(ParamUtils.OP_BETWEEN) && con.getType().equals(ParamUtils.TYPE_DATE)) {
                    sql.append(" to_date(:idx").append(String.valueOf(index++)).append(", '").append(ParamUtils.ddMMyyyy).append("')");
                    sql.append(" and ");
                    sql.append(" to_date(:idx").append(String.valueOf(index++)).append(", '").append(ParamUtils.ddMMyyyy).append("')");
                } else if (ParamUtils.TYPE_DATE.equals(con.getType())) {
                    sql.append(" to_date(:idx").append(String.valueOf(index++)).append(", '").append(ParamUtils.ddMMyyyy).append("')");
                } else if (!con.getOperator().equals(ParamUtils.OP_IN)) {
                    sql.append(":idx").append(String.valueOf(index++));
                } else {
                    sql.append(con.getValue());
                }
            }
        }
    }

    public void fillConditionQuery(Query query, List<ConditionBean> lstCondition) {
        int index = 0;
        for (ConditionBean con : lstCondition) {
            if (ParamUtils.OP_IS_NULL.equals(con.getOperator()) || ParamUtils.OP_IS_NOT_NULL.equals(con.getOperator())) {
            } else if (ParamUtils.TYPE_NUMBER.equals(con.getType()) || ParamUtils.DataType.LONG.equals(con.getType())) {
                if (!ParamUtils.OP_IN.equals(con.getOperator())) {
                    query.setParameter("idx" + String.valueOf(index++), Long.parseLong(con.getValue()));
                }
            } else if (ParamUtils.OP_BETWEEN.equals(con.getOperator()) && ParamUtils.TYPE_DATE.equals(con.getType())) {
                query.setParameter("idx" + String.valueOf(index++), con.getValue());
                query.setParameter("idx" + String.valueOf(index++), con.getValue2());
            } else if (!ParamUtils.OP_IN.equals(con.getOperator())) {
                query.setParameter("idx" + String.valueOf(index++), con.getValue());
            }
        }
    }

    public static String genSubQuery(String tableName, String colId, String colName, String value) {
        return "(select " + colId + " from com.viettel.csp.BO." + tableName + "Entity where " + StringUtils.formatFunction("lower", colName) + " like '" + StringUtils.formatLike(value) + "'  ESCAPE '\\' )";
    }

    public List<T> getAllData(String orderColumn) {
        if (StringUtils.isNullOrEmptyOrSpace(orderColumn)) {
            return findAll();
        } else {
            return findAll(Order.asc(orderColumn).ignoreCase());
        }
    }

    public T findById(Long id, String entityName, String colId) {
        Session session = getCurrentSession();
        session.beginTransaction();
        Query q = session.createQuery("from " + entityName + " where " + colId + " =:id");
        q.setLong("id", id);
        List<T> lst = q.list();
        session.getTransaction().commit();
        if (lst.isEmpty()) {
            return null;
        } else {
            return lst.get(0);
        }
    }

    public List prepareCondition(T obj) {
        List<ConditionBean> lstCondition = new ArrayList<ConditionBean>();
        if (obj == null) {
            return lstCondition;
        }
        Method methods[] = obj.getClass().getDeclaredMethods();
        Method methodForms[] = obj.getClass().getDeclaredMethods();
        for (int i = 0; i < methods.length; i++) {
            if (ReflectUtils.isGetter(methods[i])) {
                try {
                    Object value = methods[i].invoke(obj);
                    String returnType = methods[i].getReturnType().getSimpleName().toUpperCase();
                    if (value != null && !"".equals(value)) {
                        if (ParamUtils.TYPE_STRING.indexOf(returnType) >= 0) {
                            String stringValue = value.toString();
                            String opCompare = ParamUtils.OP_LIKE;
                            String valueCompare = StringUtils.formatLike(stringValue);
                            Column column = methods[i].getAnnotation(Column.class);
                            if (StringUtils.isValidString(column.columnDefinition())
                                    && "param".equals(column.columnDefinition())) {
                                opCompare = ParamUtils.OP_EQUAL;
                                valueCompare = stringValue.toLowerCase();
                            }
                            if (!"".equals(stringValue.trim())) {
                                lstCondition.add(new ConditionBean(
                                        StringUtils.formatFunction("lower", ReflectUtils.getColumnBeanName(methods[i])),
                                        opCompare,
                                        valueCompare,
                                        ParamUtils.TYPE_STRING));
                            }
//                            }
                        } else if (ParamUtils.TYPE_NUMBER.indexOf(returnType) >= 0) {
                            if (!value.toString().equals(String.valueOf(ParamUtils.DEFAULT_VALUE))) {
                                lstCondition.add(new ConditionBean(
                                        ReflectUtils.getColumnBeanName(methods[i]),
                                        ParamUtils.OP_EQUAL,
                                        value.toString(),
                                        ParamUtils.TYPE_NUMBER));
                            }
                        } else if (ParamUtils.TYPE_DATE.indexOf(returnType) >= 0) {
                            Date dateValue = (Date) value;
                            String methodName = methods[i].getName();
                            String operator = ParamUtils.OP_EQUAL;
                            if (methodName.indexOf("From") >= 0
                                    || methodName.indexOf("Begin") >= 0
                                    || methodName.indexOf("Effect") >= 0
                                    || methodName.indexOf("Sta") >= 0) {
                                operator = ParamUtils.OP_LESS_EQUAL;
                            } else if (methodName.indexOf("To") >= 0
                                    || methodName.indexOf("End") >= 0
                                    || methodName.indexOf("Expire") >= 0
                                    || methodName.indexOf("Last") >= 0) {
                                operator = ParamUtils.OP_GREATER_EQUAL;
                            }
                            lstCondition.add(new ConditionBean(
                                    StringUtils.formatFunction("trunc", ReflectUtils.getColumnBeanName(methods[i])),
                                    operator,
                                    StringUtils.formatDate(dateValue),
                                    ParamUtils.TYPE_DATE));
                        }
                    } else if (ParamUtils.TYPE_NUMBER.indexOf(returnType) >= 0) {
                        try {
                            Column column = methods[i].getAnnotation(Column.class);
                            if (StringUtils.isValidString(column.columnDefinition())) {
                                String colId = "";
                                String colName = "";
                                for (int j = 0; j < methodForms.length; j++) {
                                    if (methodForms[j].getName().equals(methods[i].getName() + "Name")) {
                                        value = methodForms[j].invoke(obj);
                                        if (!StringUtils.isValidString(value)) {
                                            break;
                                        }
                                        Class cl = Class.forName("com.viettel.plc.BO." + column.columnDefinition() + "BO");
                                        Constructor ct = cl.getConstructor();
                                        Object newObj = ct.newInstance();
                                        Method mtd[] = cl.getMethods();
                                        for (int k = 0; k < mtd.length; k++) {
                                            if ("getColId".equals(mtd[k].getName())) {
                                                colId = mtd[k].invoke(newObj).toString();
                                            }
                                            if ("getColName".equals(mtd[k].getName())) {
                                                colName = mtd[k].invoke(newObj).toString();
                                            }
                                        }
                                        break;
                                    }
                                }
                                if (StringUtils.isValidString(value)) {
                                    lstCondition.add(new ConditionBean(
                                            ReflectUtils.getColumnBeanName(methods[i]),
                                            ParamUtils.OP_IN,
                                            BaseCustomDAO.genSubQuery(column.columnDefinition(), colId, colName, value.toString()),
                                            ParamUtils.TYPE_NUMBER));
                                }
                            }
                        } catch (Exception e) {
                            log.error(e.getMessage(), e);
                            System.out.println("PrepareCondition Error: " + e.getMessage());
                        }
                    }
                } catch (IllegalAccessException iae) {
                    log.error(iae.getMessage(), iae);
                    System.out.println("PrepareCondition Error: " + iae.getMessage());
                } catch (InvocationTargetException ite) {
                    log.error(ite.getMessage(), ite);
                    System.out.println("PrepareCondition Error: " + ite.getMessage());
                }
            }
        }
        return lstCondition;
    }

    public List search(T bo, int start, int maxResult, String sortType, String sortField) {
        return find(bo.getClass().getSimpleName(),
                prepareCondition(bo),
                StringUtils.formatOrder(sortField, sortType),
                start, maxResult);
    }

    public List search(T bo, List<ConditionBean> lstInputCondition, int start, int maxResult, String sortType, String sortField) {
        List<ConditionBean> lstCondition = prepareCondition(bo);
        lstCondition.addAll(lstInputCondition);
        return find(bo.getClass().getSimpleName(),
                lstCondition,
                StringUtils.formatOrder(sortField, sortType),
                start, maxResult);
    }

    public boolean isDuplicate(Long id, String... name) {
        List<ConditionBean> lstCondition = new ArrayList<ConditionBean>();
        if (id != null) {
            lstCondition.add(new ConditionBean(
                    tEntity.getColId(),
                    ParamUtils.OP_NOT_EQUAL,
                    String.valueOf(id),
                    ParamUtils.TYPE_NUMBER));
        }

        for (int i = 0; i < name.length; i++) {
            Field field = null;
            try {
                field = tEntityClass.getDeclaredField(tEntity.getUniqueColumn()[i]);
            } catch (Exception e) {
                log.error("ERROR: Wrong declared unique column " + tEntity.getUniqueColumn()[i] + " in BO file " + tEntity.getBOName(), e);
            }
            String type = ParamUtils.TYPE_STRING;
            if (field != null) {
                if (ParamUtils.TYPE_NUMBER.contains(field.getType().getSimpleName().toUpperCase())) {
                    type = ParamUtils.TYPE_NUMBER;
                } else if (ParamUtils.TYPE_DATE.contains(field.getType().getSimpleName().toUpperCase())) {
                    type = ParamUtils.TYPE_DATE;
                }
            }
            lstCondition.add(new ConditionBean(
                    tEntity.getUniqueColumn()[i],
                    ParamUtils.OP_EQUAL,
                    name[i],
                    type));
        }
        return !find(tEntity.getBOName(), lstCondition).isEmpty();
    }

    public List<T> findByIdList(List<Long> ids, T bo) throws Exception {
        List<T> result = new ArrayList<T>();
        if (ids != null && !ids.isEmpty()) {
            StringBuilder sql = new StringBuilder();
            sql.append(" from ").append(bo.getBOName());
            sql.append(" where ").append(bo.getColId()).append(" in (");
            for (int i = 0; i < ids.size(); i++) {
                sql.append(ids.get(i)).append(",");
            }
            sql.deleteCharAt(sql.length() - 1);
            sql.append(")");

            Session session = getCurrentSession();
            session.beginTransaction();
            Query query = session.createQuery(sql.toString());
            result = query.list();
            session.getTransaction().commit();
        }
        return result;
    }
}
