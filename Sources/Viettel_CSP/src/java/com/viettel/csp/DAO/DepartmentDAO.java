/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.DAO;

import com.viettel.csp.DTO.DepartmentDTO;
import com.viettel.csp.entity.DepartmentEntity;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author admin
 */
public class DepartmentDAO extends BaseCustomDAO<DepartmentEntity> {

    public DepartmentDAO() {
        tEntity = new DepartmentEntity();
        tEntityClass = DepartmentEntity.class;
    }

    public List<DepartmentDTO> getAllsWithOrgnization(Long orgId) {
        List<DepartmentDTO> lstResult;
        Session session = getCurrentSession();
        session.beginTransaction();
        String sql = "SELECT ORGANIZATIONID organizationId, NAME name FROM TBL_DEPARTMENT WHERE REGEXP_LIKE(PATH, (SELECT REGEXP_REPLACE(PATH, '/$') FROM TBL_DEPARTMENT WHERE ORGANIZATIONID = :ORG_ID)) ORDER BY LENGTH(PATH), ORGANIZATIONID";
        Query query = session.createSQLQuery(sql)
                .addScalar("organizationId", Hibernate.LONG)
                .addScalar("name", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(DepartmentDTO.class))
                .setParameter("ORG_ID", orgId);
        lstResult = query.list();
        session.getTransaction().commit();
        return lstResult;
    }

    public DepartmentEntity getByOrganizationId(Long id) {
        Session session = getCurrentSession();
        session.beginTransaction();
        Criteria cr = session.createCriteria(DepartmentEntity.class);
        cr.add(Restrictions.eq("organizationId", id));
        List results = cr.list();
        return (results.size() > 0) ? ((DepartmentEntity) results.get(0)) : null;
    }
}
