/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.viettel.csp.DAO;

import com.viettel.csp.DTO.PartnerDTO;
import com.viettel.csp.entity.BankEntity;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author GEM
 */
public class BankDAO  extends BaseCustomDAO<BankEntity> {
     private static Logger log = Logger.getLogger(BankEntity.class);

    public BankDAO() {
        tEntity = new BankEntity(); 
        tEntityClass = BankEntity.class;
    }
    
}
