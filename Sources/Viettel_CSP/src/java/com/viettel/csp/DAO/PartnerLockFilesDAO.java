/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.DAO;

import com.viettel.csp.entity.PartnerLockFilesEntity;
import org.apache.log4j.Logger;

/**
 *
 * @author tiennv02
 */
public class PartnerLockFilesDAO extends BaseCustomDAO<PartnerLockFilesEntity> {

    private static Logger logger = Logger.getLogger(PartnerLockFilesDAO.class);

    public PartnerLockFilesDAO() {
        tEntity = new PartnerLockFilesEntity();
        tEntityClass = PartnerLockFilesEntity.class;
    }
}
