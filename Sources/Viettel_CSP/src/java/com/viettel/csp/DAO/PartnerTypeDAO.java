/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.DAO;

import com.viettel.csp.entity.PartnerTypeEntity;
import com.viettel.csp.util.KeyValueBean;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

/**
 *
 * @author GEM
 */
public class PartnerTypeDAO extends BaseCustomDAO<PartnerTypeEntity> {

    private static Logger log = Logger.getLogger(PartnerTypeDAO.class);

    public PartnerTypeDAO() {
        tEntity = new PartnerTypeEntity();
        tEntityClass = PartnerTypeEntity.class;
    }

    public List<KeyValueBean> getListPartnerTypeByCode(String partnerTypeCode) {
        List<KeyValueBean> lstKeyValueBean = new ArrayList<KeyValueBean>();
        Session session = getCurrentSession();
        session.beginTransaction();
        StringBuilder sql = new StringBuilder()
                .append("  SELECT distinct code key, name value")
                .append("    FROM   tbl_partner_type ")
                .append("    WHERE   code=:code ");

        Query query = session.createSQLQuery(sql.toString())
                .addScalar("key", Hibernate.STRING)
                .addScalar("value", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(KeyValueBean.class));

        query.setParameter("code", partnerTypeCode);
        lstKeyValueBean = query.list();
        session.getTransaction().commit();
        return lstKeyValueBean;

    }

    public List<KeyValueBean> getListPartnerTypeByName(String partnerTypeName) {
        List<KeyValueBean> lstKeyValueBean = new ArrayList<KeyValueBean>();
        Session session = getCurrentSession();
        session.beginTransaction();
        StringBuilder sql = new StringBuilder()
                .append("  SELECT distinct code key, name value")
                .append("    FROM   tbl_partner_type ")
                .append("    WHERE   name=:name ");

        Query query = session.createSQLQuery(sql.toString())
                .addScalar("key", Hibernate.STRING)
                .addScalar("value", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(KeyValueBean.class));

        query.setParameter("name", partnerTypeName);

        lstKeyValueBean = query.list();
        session.getTransaction().commit();
        return lstKeyValueBean;
    }

}
