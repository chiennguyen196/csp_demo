/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.DAO;

import com.viettel.csp.DTO.ServiceDTO;
import com.viettel.csp.entity.ServiceEntity;
import com.viettel.csp.util.StringUtils;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StringType;

import static com.viettel.csp.util.StringUtils.getParamLike;

/**
 *
 * @author admin
 */
public class ServiceDAO extends BaseCustomDAO<ServiceEntity> {

    private static Logger log = Logger.getLogger(ServiceDAO.class);

    public ServiceDAO() {
        tEntity = new ServiceEntity();
        tEntityClass = ServiceEntity.class;
    }

    public List<ServiceDTO> getListWithNullParent() {
        List<ServiceDTO> list = new ArrayList<>();
        try {
            Session session = getCurrentSession();
            session.beginTransaction();
            StringBuilder sql = new StringBuilder();
            sql.append("select CODE code,NAME name from TBL_SERVICE WHERE PARENT_CODE IS NULL");
            Query query = session.createSQLQuery(sql.toString())
                    .addScalar("code", new StringType())
                    .addScalar("name", new StringType())
                    .setResultTransformer(Transformers.aliasToBean(ServiceDTO.class));
            list = query.list();
            session.getTransaction().commit();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return list;
    }

    public List<ServiceDTO> getServiceByCode(String code) {
        List<ServiceDTO> list = new ArrayList<>();
        try {
            Session session = getCurrentSession();
            session.beginTransaction();
            StringBuilder sql = new StringBuilder();
            sql.append("select CODE code,NAME name from TBL_SERVICE");
            if (code != null && StringUtils.isValidString(code)) {
                sql.append(" WHERE PARENT_CODE = :PARENT_CODE");
            } else {
                return list;
            }
            Query query = session.createSQLQuery(sql.toString())
                    .addScalar("code", new StringType())
                    .addScalar("name", new StringType())
                    .setResultTransformer(Transformers.aliasToBean(ServiceDTO.class))
                    .setParameter("PARENT_CODE", code);
            list = query.list();
            session.getTransaction().commit();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return list;
    }

    public List<ServiceDTO> getList() {
        List<ServiceDTO> list = new ArrayList<>();
        try {
            Session session = getCurrentSession();
            session.beginTransaction();
            StringBuilder sql = new StringBuilder();
            sql.append("select CODE code,NAME name,PATH_FILE pathFile from TBL_SERVICE where PARENT_CODE is not null");
            Query query = session.createSQLQuery(sql.toString())
                    .addScalar("code", new StringType())
                    .addScalar("name", new StringType())
                    .addScalar("pathFile", new StringType())
                    .setResultTransformer(Transformers.aliasToBean(ServiceDTO.class));
            list = query.list();
            session.getTransaction().commit();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return list;
    }

    public List<ServiceDTO> getListType() {
        List<ServiceDTO> list = new ArrayList<>();
        try {
            Session session = getCurrentSession();
            session.beginTransaction();
            StringBuilder sql = new StringBuilder();
            sql.append("select CODE codeReflectType,NAME nameReflectType from TBL_REFLECT_TYPE");
            Query query = session.createSQLQuery(sql.toString())
                    .addScalar("codeReflectType", new StringType())
                    .addScalar("nameReflectType", new StringType())
                    .setResultTransformer(Transformers.aliasToBean(ServiceDTO.class));
            list = query.list();
            session.getTransaction().commit();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return list;
    }

    public List<ServiceDTO> getListChilService(String parentCode) {

        List<ServiceDTO> lstResult = new ArrayList<ServiceDTO>();
        Session session = getCurrentSession();
        session.beginTransaction();
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT code code,"
                + "parent_code parentCode,"
                + "name name,"
                + "path_file pathFile"
                + "             FROM tbl_service")
                .append("   WHERE   1 = 1")
                .append("            AND parent_code = :parentCode");

        Query query = session.createSQLQuery(sql.toString())
                .addScalar("code", Hibernate.STRING)
                .addScalar("name", Hibernate.STRING)
                .addScalar("pathFile", Hibernate.STRING)
                .addScalar("parentCode", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(ServiceDTO.class));

        query.setParameter("parentCode", parentCode);
        lstResult = query.list();
        session.getTransaction().commit();
        return lstResult;
    }

    public ServiceEntity getDetailServiceByCode(String code) {
        Session session = getCurrentSession();
        session.beginTransaction();
        Criteria cr = session.createCriteria(ServiceEntity.class);
        cr.add(Restrictions.eq("code", code));
        List<ServiceEntity> results = cr.list();
        return (results.size() > 0) ? results.get(0) : null;
    }


    public List<ServiceDTO> getListServiceByDTO(ServiceDTO serviceDTO) {
        List<ServiceDTO> lstResult = new ArrayList<ServiceDTO>();
        Session session = getCurrentSession();
        session.beginTransaction();
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT code code,"
                + "parent_code parentCode,"
                + "name name,"
                + "path_file pathFile"
                + "             FROM tbl_service")
                .append("   WHERE   1 = 1");
        if (serviceDTO != null) {
            if (StringUtils.isValidString(serviceDTO.getCode())) {
                sql.append("            AND code = :code ");
            }
            if (StringUtils.isValidString(serviceDTO.getName())) {
                sql.append("            AND UPPER(name) LIKE UPPER(:name) ");
            }
            if (StringUtils.isValidString(serviceDTO.getParentCode())) {
                sql.append("            AND parent_code = :parentCode ");
            }
        }
        Query query = session.createSQLQuery(sql.toString())
                .addScalar("code", Hibernate.STRING)
                .addScalar("name", Hibernate.STRING)
                .addScalar("parentCode", Hibernate.STRING)
                .addScalar("pathFile", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(ServiceDTO.class));

        if (serviceDTO != null) {
            if (StringUtils.isValidString(serviceDTO.getCode())) {
                query.setParameter("code", serviceDTO.getCode());
            }
            if (StringUtils.isValidString(serviceDTO.getName())) {
                query.setParameter("name", getParamLike(serviceDTO.getName().trim()));
            }
            if (StringUtils.isValidString(serviceDTO.getParentCode())) {
                query.setParameter("parentCode", serviceDTO.getParentCode());
            }
            if (StringUtils.isValidString(serviceDTO.getPathFile())) {
                query.setParameter("pathFile", serviceDTO.getPathFile());
            }
        }
        lstResult = query.list();
        session.getTransaction().commit();
        return lstResult;
    }


    public List<ServiceDTO> getListServiceChild() throws Exception {
        List<ServiceDTO> list = new ArrayList<>();
        Session session = getCurrentSession();
        session.beginTransaction();
        StringBuilder sql = new StringBuilder();
        sql.append("select a.CODE, a.NAME, a.PATH_FILE pathFile, b.NAME parentName from TBL_SERVICE a left join TBL_SERVICE b on a.PARENT_CODE = b.CODE where a.PATH_FILE is not null");
        Query query = session.createSQLQuery(sql.toString())
                .addScalar("code", new StringType())
                .addScalar("name", new StringType())
                .addScalar("pathFile", new StringType())
                .addScalar("parentName", new StringType())
                .setResultTransformer(Transformers.aliasToBean(ServiceDTO.class));
        list = query.list();
        session.getTransaction().commit();
        return list;
    }
}
