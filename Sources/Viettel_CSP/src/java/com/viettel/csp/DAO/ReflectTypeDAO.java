/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.DAO;

import com.viettel.csp.DTO.ReflectDTO;
import com.viettel.csp.entity.ReflectTypeEntity;
import com.viettel.csp.util.KeyValueBean;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author GEM
 */
public class ReflectTypeDAO extends BaseCustomDAO<ReflectTypeEntity> {

    public ReflectTypeDAO() {
        tEntity = new ReflectTypeEntity();
        tEntityClass = ReflectTypeEntity.class;
    }

    public ReflectTypeEntity getReflectTypeByField(String colume, String value) {
        List<ReflectTypeEntity> lstReflectType = new ArrayList<ReflectTypeEntity>();
        Session session = getCurrentSession();
        session.beginTransaction();
        Criteria criteria = session.createCriteria(ReflectTypeEntity.class)
                .add(Restrictions.eq(colume, value));
        lstReflectType = criteria.list();
        session.getTransaction().commit();
        return lstReflectType != null & !lstReflectType.isEmpty() ? lstReflectType.get(0) : null;

    }
}
