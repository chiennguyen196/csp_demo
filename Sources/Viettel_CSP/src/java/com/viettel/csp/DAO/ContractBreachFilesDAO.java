package com.viettel.csp.DAO;

import com.viettel.csp.entity.ContractBreachFilesEntity;
import com.viettel.csp.entity.ContractBreachReasonEntity;

import java.util.logging.Logger;

/**
 * Created by GEM on 6/16/2017.
 */
public class ContractBreachFilesDAO extends BaseCustomDAO {
    private static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ContractBreachFilesDAO.class);

    public ContractBreachFilesDAO(){
        tEntity = new ContractBreachFilesEntity();
        tEntityClass = ContractBreachFilesEntity.class;
    }

}
