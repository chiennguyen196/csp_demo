/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.viettel.csp.DAO;

import com.viettel.csp.DTO.ReasonDTO;
import com.viettel.csp.entity.ReasonEntity;
import com.viettel.csp.util.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import java.util.List;

/**
 *
 * @author GEM
 */
public class ReasonDAO  extends BaseCustomDAO<ReasonEntity> {
     private static Logger log = Logger.getLogger(ReasonDAO.class);

    public ReasonDAO() {
        tEntity = new ReasonEntity();
        tEntityClass = ReasonEntity.class;
    }

    public ReasonEntity save(ReasonEntity reasonEntity){
        Session session = getSession();
        session.beginTransaction();
        session.save(reasonEntity);
        session.getTransaction().commit();
        return reasonEntity;
    }

    public List<ReasonEntity> search(ReasonEntity reasonEntity){
        Session session = getSession();
        session.beginTransaction();
        StringBuilder hql = new StringBuilder();
        hql.append("from ").append(ReasonEntity.class.getName())
                .append(" where 1 = 1 ");
        if(StringUtils.isValidString(reasonEntity.getCode())){
            hql.append("AND CODE LIKE '"+reasonEntity.getCode()+"%'");
        }
        if(StringUtils.isValidString(reasonEntity.getName())){
            hql.append("AND LOWER(NAME) LIKE '%"+reasonEntity.getName().toLowerCase()+"%'");
        }
        if(StringUtils.isValidString(reasonEntity.getType())){
            hql.append("AND TYPE = '" + reasonEntity.getType()+"'");
        }
        System.out.println(hql.toString());
        Query query = session.createQuery(hql.toString());
        List<ReasonEntity> list = (List<ReasonEntity>) query.list();
        session.getTransaction().commit();
        return list;
    }

}
