/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.DAO;

import com.viettel.csp.DTO.ContactPointDTO;
import com.viettel.csp.entity.ContactPointEntity;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;

/**
 * @author GEM
 */
public class ContactPointDAO extends BaseCustomDAO<ContactPointEntity> {

    private static Logger log = Logger.getLogger(ContactPointDAO.class);

    public ContactPointDAO() {
        this.tEntity = new ContactPointEntity();
        this.tEntityClass = ContactPointEntity.class;
    }

    public List<ContactPointDTO> getList(Long contractID) {
        Session session = getCurrentSession();
        session.beginTransaction();
        String sql = "SELECT C.NAME name,C.EMAIL email,"
            + "C.MOBILE_PHONE_NUMBER mobilePhoneNumber,"
            + "C.FIXED_PHONE_NUMBER fixedPhoneNumber,"
            + "C.POSITION position,CT.NAME contactTypeName "
            + " FROM TBL_CONTACT_POINT CP, TBL_CONTACT C,TBL_CONTACT_TYPE CT "
            + " WHERE CP.CONTACT_ID = C.ID AND C.CONTACT_TYPE = CT.CODE AND CP.CONTRACT_ID = :CONTRACT_ID";
        Query query = session.createSQLQuery(sql)
            .addScalar("name", Hibernate.STRING)
            .addScalar("email", Hibernate.STRING)
            .addScalar("mobilePhoneNumber", Hibernate.STRING)
            .addScalar("fixedPhoneNumber", Hibernate.STRING)
            .addScalar("contactTypeName", Hibernate.STRING)
            .addScalar("position", Hibernate.STRING)
            .setResultTransformer(Transformers.aliasToBean(ContactPointDTO.class));
        query.setParameter("CONTRACT_ID", contractID);

        List<ContactPointDTO> lst = query.list();
        session.getTransaction().commit();
        return lst;
    }

    public List<ContactPointDTO> getListByContactId(Long contactId) {
        Session session = getCurrentSession();
        session.beginTransaction();
        Criteria criteria = session.createCriteria(ContactPointEntity.class);
        criteria.add(Restrictions.eq("contactId", contactId));
        List<ContactPointDTO> lst = criteria.list();
        session.getTransaction().commit();
        return lst;
    }

    public Boolean deleteByContactId(Long contactId) throws Exception {
        Session session = getCurrentSession();
        session.beginTransaction();
        Criteria criteria = session.createCriteria(ContactPointEntity.class);
        criteria.add(Restrictions.eq("contactId", contactId));
        List<ContactPointEntity> lst = criteria.list();
        for (ContactPointEntity object : lst) {
            session.delete(object);
        }
        session.getTransaction().commit();
        return true;
    }
}
