/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.DAO;

import com.viettel.csp.DTO.ReflectDTO;
import com.viettel.csp.entity.ReflectEntity;
import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.util.StringUtils;

import static com.viettel.csp.util.StringUtils.getParamLike;

import com.viettel.csp.util.UserTokenUtils;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

/**
 * @author GEM
 */
public class ReflectDAO extends BaseCustomDAO<ReflectEntity> {

    private static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ReflectDAO.class);

    public ReflectDAO() {
        tEntity = new ReflectEntity();
        tEntityClass = ReflectEntity.class;
    }


    public long checkCode(String code){


        List<Long> idList=new ArrayList<>();

        Session session = getCurrentSession();
        session.beginTransaction();

        String sql="Select r.id FROM ReflectEntity r WHERE r.code= :code";

        idList= session.createQuery(sql).setParameter("code", code).list();

       if (idList.size()>0){
           return idList.get(0);
       }else
           return -1;

    }

    public List<ReflectDTO> getListReflect(ReflectDTO reflectDTO) throws HibernateException {
        List<ReflectDTO> lstResult = new ArrayList<ReflectDTO>();
        Session session = getCurrentSession();
        session.beginTransaction();

        StringBuilder sql = new StringBuilder()
                .append("SELECT a.id id,")
                .append("                   a.code code,")
                .append("                   a.reflect_status reflectStatusCode,")
                .append("                   a.reflect_type reflectTypeCode,")
                .append("                   a.customer_name customerName,")
                .append("                   a.customer_phone customerPhone,")
                .append("                   a.service_code serviceCode,")
                .append("                   a.head_of_service headOfService,")
                .append("                   a.content content,")
                .append("                   a.partner_id partnerId,")
                .append("                   a.create_by createBy,")
                .append("                   a.create_at createAt,")
                .append("                   a.update_by updateBy,")
                .append("                   a.update_at updateAt,")
                .append("                   a.close_date closeDate,")
                .append("                   a.reason_code reasonCode,")
                .append("                   f.name reasonName,")
                .append("                   a.reason_Description reasonDescription,")
                .append("                   a.close_date closeDate,")
                .append("                   a.customer_care_staff_name customerCareStaffName,")
                .append("                   a.customer_care_staff_phone customerCareStaffPhone, a.priority_level priorityLevel,")
                .append("                  b.name  reflectTypeName, c.name  reflectStatusName, d.name serviceName, a.required_reponse_date requiredReponseDate")
                .append("              FROM tbl_reflect a")
                .append("                   LEFT JOIN tbl_reflect_type b")
                .append("                       ON a.reflect_type = b.code")
                .append("                   LEFT JOIN tbl_reflect_status c")
                .append("                       ON a.reflect_status = c.code")
                .append("                   LEFT JOIN tbl_service d")
                .append("                   ON a.service_code=d.code")
                .append("                   LEFT JOIN tbl_reason f")
                .append("                   ON a.reason_code=f.code")
                .append("   WHERE   1 = 1");

        if (UserTokenUtils.getPersonType().equalsIgnoreCase(ParamUtils.USER_TYPE.CONTACT)
                || UserTokenUtils.getPersonType().equalsIgnoreCase(ParamUtils.USER_TYPE.PARTNER)) {
            sql.append("            AND  a.partner_id = :partnerId and a.reflect_status != :reflectStatusCode");
        } else {
            sql.append("            AND  a.create_by = :createBy");
        }
        if (reflectDTO != null) {
            if (StringUtils.isValidString(reflectDTO.getId())) {
                sql.append("            AND a.id = :id ");
            }
            if (StringUtils.isValidString(reflectDTO.getPartnerId())) {
                sql.append("            AND a.partner_id = :partnerId ");
            }
            if (StringUtils.isValidString(reflectDTO.getCode())) {
                sql.append("            AND LOWER(a.code) like :code ");
            }
            if (StringUtils.isValidString(reflectDTO.getCustomerName())) {
                sql.append("            AND LOWER(a.customer_name) like :customerName ");
            }
            if (StringUtils.isValidString(reflectDTO.getReflectTypeCode())) {
                sql.append("           AND LOWER(a.reflect_type) = :reflectTypeCode ");
            }
            if (StringUtils.isValidString(reflectDTO.getHeadOfService())) {
                sql.append("            AND LOWER(a.head_of_service)  like :headOfService");
            }
            if (StringUtils.isValidString(reflectDTO.getRequiredReponseDate())) {
                sql.append("            AND TRUCT(a.required_reponse_date)  like TRUCT(:requiredReponseDate)");
            }
            if (StringUtils.isValidString(reflectDTO.getServiceName())) {
                sql.append("            AND LOWER(d.name) like :serviceName");
            }
        }
        sql.append(" order by a.required_reponse_date asc");

        Query query = session.createSQLQuery(sql.toString())
                .addScalar("id", Hibernate.LONG)
                .addScalar("code", Hibernate.STRING)
                .addScalar("reflectStatusCode", Hibernate.STRING)
                .addScalar("reflectTypeCode", Hibernate.STRING)
                .addScalar("customerName", Hibernate.STRING)
                .addScalar("customerPhone", Hibernate.STRING)
                .addScalar("serviceCode", Hibernate.STRING)
                .addScalar("content", Hibernate.STRING)
                .addScalar("headOfService", Hibernate.STRING)
                .addScalar("partnerId", Hibernate.LONG)
                .addScalar("createBy", Hibernate.LONG)
                .addScalar("createAt", Hibernate.DATE)
                .addScalar("updateBy", Hibernate.LONG)
                .addScalar("updateAt", Hibernate.DATE)
                .addScalar("closeDate", Hibernate.DATE)
                .addScalar("customerCareStaffName", Hibernate.STRING)
                .addScalar("customerCareStaffPhone", Hibernate.STRING)
                .addScalar("priorityLevel", Hibernate.STRING)
                .addScalar("reflectTypeName", Hibernate.STRING)
                .addScalar("reflectStatusName", Hibernate.STRING)
                .addScalar("serviceName", Hibernate.STRING)
                .addScalar("requiredReponseDate", Hibernate.DATE)
                .addScalar("reasonCode", Hibernate.STRING)
                .addScalar("reasonName", Hibernate.STRING)
                .addScalar("reasonDescription", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(ReflectDTO.class));
        if (UserTokenUtils.getPersonType().equalsIgnoreCase(ParamUtils.USER_TYPE.CONTACT)
                || UserTokenUtils.getPersonType().equalsIgnoreCase(ParamUtils.USER_TYPE.PARTNER)) {
            query.setParameter("partnerId", UserTokenUtils.getPartnerId());
            query.setParameter("reflectStatusCode", ParamUtils.REFLECT_STATUS.NEW);
        } else {
            query.setParameter("createBy", UserTokenUtils.getUserId());
        }
        if (reflectDTO != null) {
            if (StringUtils.isValidString(reflectDTO.getId())) {
                query.setParameter("id", reflectDTO.getId());
            }
            if (StringUtils.isValidString(reflectDTO.getPartnerId())) {
                query.setParameter("partnerId", reflectDTO.getPartnerId());
            }
            if (StringUtils.isValidString(reflectDTO.getCode())) {
                query.setParameter("code", getParamLike(reflectDTO.getCode()));
            }
            if (StringUtils.isValidString(reflectDTO.getCustomerName())) {
                query.setParameter("customerName", getParamLike(reflectDTO.getCustomerName()));
            }
            if (StringUtils.isValidString(reflectDTO.getReflectTypeCode())) {
                query.setParameter("reflectTypeCode", reflectDTO.getReflectTypeCode().toLowerCase());
            }

            if (StringUtils.isValidString(reflectDTO.getHeadOfService())) {
                query.setParameter("headOfService", getParamLike(reflectDTO.getHeadOfService()));
            }

            if (StringUtils.isValidString(reflectDTO.getRequiredReponseDate())) {
                query.setParameter("requiredReponseDate", reflectDTO.getRequiredReponseDate());
            }
            if (StringUtils.isValidString(reflectDTO.getServiceName())) {
                query.setParameter("serviceName", getParamLike(reflectDTO.getServiceName()));
            }
        }
        lstResult = query.list();
        session.getTransaction().commit();
        return lstResult;
    }

    public List<ReflectDTO> getReflectList(ReflectDTO reflectDTO) {
        List<ReflectDTO> lstResult = new ArrayList<ReflectDTO>();
        Session session = getCurrentSession();
        session.beginTransaction();

        StringBuilder sql = new StringBuilder();
        sql.append("SELECT id id,"
                + "code code,"
                + "reflect_status reflectStatusCode,"
                + "reflect_type reflectTypeCode,"
                + "customer_name customerName,"
                + "customer_phone customerPhone,"
                + "service_code serviceCode,"
                + "head_of_service headOfService,"
                + "content content,"
                + "partner_id partnerId,"
                + "create_by createBy,"
                + "create_at createAt,"
                + "update_by updateBy,"
                + "update_at updateAt,"
                + "close_date closeDate,"
                + "customer_care_staff_name customerCareStaffName,"
                + "customer_care_staff_phone customerCareStaffPhone, priority_level priorityLevel,"
                + "(select max(required_reponse_date) from tbl_solution s where s.reflect_id =tbl_reflect.id ) requiredReponseDate"
                + " FROM tbl_reflect ");
        if (reflectDTO != null) {
            if (StringUtils.isValidString(reflectDTO.getId())) {
                sql.append(" where id = :id");
            }
        }

        Query query = session.createSQLQuery(sql.toString())
                .addScalar("id", Hibernate.LONG)
                .addScalar("code", Hibernate.STRING)
                .addScalar("reflectStatusCode", Hibernate.STRING)
                .addScalar("reflectTypeCode", Hibernate.STRING)
                .addScalar("customerName", Hibernate.STRING)
                .addScalar("customerPhone", Hibernate.STRING)
                .addScalar("serviceCode", Hibernate.STRING)
                .addScalar("headOfService", Hibernate.STRING)
                .addScalar("content", Hibernate.STRING)
                .addScalar("partnerId", Hibernate.LONG)
                .addScalar("createAt", Hibernate.DATE)
                .addScalar("updateBy", Hibernate.LONG)
                .addScalar("updateAt", Hibernate.DATE)
                .addScalar("closeDate", Hibernate.DATE)
                .addScalar("customerCareStaffName", Hibernate.STRING)
                .addScalar("customerCareStaffPhone", Hibernate.STRING)
                .addScalar("priorityLevel", Hibernate.STRING)
                .addScalar("requiredReponseDate", Hibernate.DATE)
                .setResultTransformer(Transformers.aliasToBean(ReflectDTO.class));

        if (reflectDTO != null) {
            if (StringUtils.isValidString(reflectDTO.getId())) {
                query.setParameter("id", reflectDTO.getId());
            }
        }
        lstResult = query.list();
        session.getTransaction().commit();
        return lstResult;
    }
}
