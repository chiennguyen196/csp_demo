/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.DAO;

import com.viettel.csp.entity.PartnerEntity;
import com.viettel.csp.entity.PartnerStatusEntity;
import org.apache.log4j.Logger;

/**
 *
 * @author GEM
 */
public class PartnerStatusDAO extends  BaseCustomDAO<PartnerStatusEntity>{
 private static Logger log = Logger.getLogger(PartnerTypeDAO.class);

  
    public PartnerStatusDAO() {
        tEntity=new PartnerStatusEntity();
        tEntityClass = PartnerStatusEntity.class;
    }
}
