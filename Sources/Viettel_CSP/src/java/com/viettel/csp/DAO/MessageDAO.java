/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.DAO;

import com.viettel.csp.entity.MessageEntity;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author GEM
 */
public class MessageDAO extends BaseCustomDAO<MessageEntity> {

    public MessageDAO() {
        tEntity = new MessageEntity();
        tEntityClass = MessageEntity.class;
    }
    
    public void insert() throws Exception {
        Session session = getCurrentSession();
        session.beginTransaction();
        String sql = "insert into tbl_message(id, send_user_id, receive_user_id, content, description, create_by, create_at)"
                + "values (tbl_message_seq.NEXTVAL, null, null,'Thay đổi thông tin đối tác' ,'Thay đổi thông tin đối tác' , null, sysdate)";

        Query query = session.createSQLQuery(sql);
        query.executeUpdate();
        session.getTransaction().commit();

    }

    public void insert(String mesg) {
        Session session = getCurrentSession();
        session.beginTransaction();
        String sql = "insert into tbl_message(id, send_user_id, receive_user_id, content, description, create_by, create_at)"
                + "values (tbl_message_seq.NEXTVAL, null, null,'" + mesg + "' ,'" + mesg + "' , null, sysdate)";

        Query query = session.createSQLQuery(sql);
        query.executeUpdate();
        session.getTransaction().commit();
    }

}
