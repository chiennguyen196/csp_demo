/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.viettel.csp.DAO;

import com.viettel.csp.entity.ContractStatusEntity;
import org.apache.log4j.Logger;

/**
 *
 * @author GEM
 */
public class ContractStatusDAO  extends BaseCustomDAO<ContractStatusEntity> {
     private static Logger log = Logger.getLogger(ContractStatusDAO.class);

    public ContractStatusDAO() {
        tEntity = new ContractStatusEntity();
        tEntityClass = ContractStatusEntity.class;
    }
}
