package com.viettel.csp.DAO;

import com.viettel.csp.entity.ContractBreachEntity;

/**
 * Created by GEM on 6/16/2017.
 */
public class ContractBreachDAO extends BaseCustomDAO{

    private static org.apache.log4j.Logger log = org.apache.log4j.Logger
            .getLogger(ContractBreachDAO.class);

    public ContractBreachDAO() {
        this.tEntity = new ContractBreachEntity();
        this.tEntityClass = ContractBreachEntity.class;
    }
}
