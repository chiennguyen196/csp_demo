/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.DAO;

import com.viettel.csp.DTO.ContractFilesDTO;
import com.viettel.csp.entity.ContractFilesEntity;
import com.viettel.csp.util.LanguageBundleUtils;
import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.util.StringUtils;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import java.util.ArrayList;
import java.util.List;

/**
 * @author TranChi
 */
public class ContractFilesDAO extends BaseCustomDAO<ContractFilesEntity> {

    private static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ContractFilesDAO.class);

    public ContractFilesDAO() {
        tEntity = new ContractFilesEntity();
        tEntityClass = ContractFilesEntity.class;
    }

    public List<ContractFilesDTO> getListContractFiles(ContractFilesDTO contractFilesDTO) throws HibernateException {
        Session session = getCurrentSession();
        session.beginTransaction();
        StringBuilder sql = new StringBuilder()
                .append("select  ")
                .append("a.id id,")
                .append("a.filing_date filingDate,")
                .append("b.file_contract_number fileContractNumber,")
                .append("b.file_name  fileName, ")
                .append("b.file_status  fileStatus, ")
                .append("b.file_type fileType, ")
                .append("b.contract_id  contractId, ")
                .append("b.upload_date   uploadDate, ")
                .append("b.start_date   startDate, ")
                .append("b.end_date   endDate, ")
                .append("b.content content, ")
                .append("b.description description, ")
                .append("b.path_file pathFile ")
                .append(" from ")
                .append("tbl_contract_files b , tbl_contract a where  b.contract_id = a.id ");
        if (contractFilesDTO != null) {
            if (contractFilesDTO.getFileType() != null) {
                sql.append(" AND  b.file_type = :fileType ");
            }
            if (contractFilesDTO.getContractId() != null) {
                sql.append(" AND b.contract_id = :contractId ");
            }
        }
        sql.append(" ORDER BY b.FILE_STATUS asc");
        Query query = session.createSQLQuery(sql.toString())
                .addScalar("id", Hibernate.LONG)
                .addScalar("filingDate", Hibernate.DATE)
                .addScalar("fileContractNumber", Hibernate.STRING)
                .addScalar("fileName", Hibernate.STRING)
                .addScalar("fileStatus", Hibernate.STRING)
                .addScalar("fileType", Hibernate.STRING)
                .addScalar("contractId", Hibernate.LONG)
                .addScalar("uploadDate", Hibernate.TIMESTAMP)
                .addScalar("startDate", Hibernate.DATE)
                .addScalar("endDate", Hibernate.DATE)
                .addScalar("content", Hibernate.STRING)
                .addScalar("description", Hibernate.STRING)
                .addScalar("pathFile", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(ContractFilesDTO.class));
        if (contractFilesDTO != null) {
            if (contractFilesDTO.getFileType() != null) {
                query.setParameter("fileType", contractFilesDTO.getFileType());
            }
            if (contractFilesDTO.getContractId() != null) {
                query.setParameter("contractId", contractFilesDTO.getContractId());
            }
        }
        List<ContractFilesDTO> lst = query.list();
        for (ContractFilesDTO item : lst) {
            if (ParamUtils.FILE_STATUS.DELETE.equalsIgnoreCase(item.getFileStatus())) {
                item.setFileStatusName(LanguageBundleUtils.getString("contract.file.status.active"));
            } else {
                item.setFileStatusName(LanguageBundleUtils.getString("contract.file.status.delete"));
            }
        }
        session.getTransaction().commit();
        return lst;
    }

    public List<ContractFilesDTO> getListMenuContractFiles(ContractFilesDTO contractFilesDTO) throws HibernateException {
        List<ContractFilesDTO> lstResult = new ArrayList<>();
        Session session = getCurrentSession();
        session.beginTransaction();
        StringBuilder sql = new StringBuilder()
                .append("select a.id id,a.FILE_CONTRACT_NUMBER fileContractNumber,a.PATH_FILE pathFile,")
                .append("a.START_DATE startDate,a.END_DATE endDate,")
                .append("a.CONTENT,a.FILE_TYPE fileType,")
                .append("a.UPLOAD_DATE uploadDate,a.CONTENT content")
                .append(" from TBL_CONTRACT_FILES a")
                .append(" INNER JOIN TBL_FILE_TYPE b On b.CODE= a.FILE_TYPE")
                .append(" WHERE (b.CODE='HOPDONG' or b.CODE='PHULUC')");

        if (StringUtils.isValidString(contractFilesDTO.getContractId())) {
            sql.append(" and a.CONTRACT_ID = :contractId");
        }

        Query query = session.createSQLQuery(sql.toString())
                .addScalar("id", Hibernate.LONG)
                .addScalar("fileContractNumber", Hibernate.STRING)
                .addScalar("fileType", Hibernate.STRING)
                .addScalar("uploadDate", Hibernate.DATE)
                .addScalar("startDate", Hibernate.DATE)
                .addScalar("endDate", Hibernate.DATE)
                .addScalar("pathFile", Hibernate.STRING)
                .addScalar("content", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(ContractFilesDTO.class));
        if (StringUtils.isValidString(contractFilesDTO.getContractId())) {
            query.setParameter("contractId", contractFilesDTO.getContractId());
        }
        lstResult = query.list();
        session.getTransaction().commit();
        return lstResult;
    }

    public List<ContractFilesDTO> getMenuContractFile(ContractFilesDTO contractFilesDTO) throws HibernateException {
        List<ContractFilesDTO> lstResult = new ArrayList<>();
        Session session = getCurrentSession();
        session.beginTransaction();
        StringBuilder sql = new StringBuilder()
                .append("SELECT a.id id,a.FILE_CONTRACT_NUMBER fileContractNumber,")
                .append("a.CONTENT content,a.DESCRIPTION description,")
                .append("a.START_DATE startDate,a.END_DATE  endDate ")
                .append("from TBL_CONTRACT_FILES a  WHERE FILE_TYPE='PHULUCHOPDONG'");

        if (StringUtils.isValidString(contractFilesDTO.getContractId())) {
            sql.append(" and a.CONTRACT_ID = :contractId");
        }

        Query query = session.createSQLQuery(sql.toString())
                .addScalar("id", Hibernate.LONG)
                .addScalar("fileContractNumber", Hibernate.STRING)
                .addScalar("content", Hibernate.STRING)
                .addScalar("startDate", Hibernate.DATE)
                .addScalar("endDate", Hibernate.DATE)
                .addScalar("description", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(ContractFilesDTO.class));
        if (StringUtils.isValidString(contractFilesDTO.getContractId())) {
            query.setParameter("contractId", contractFilesDTO.getContractId());
        }
        lstResult = query.list();
        session.getTransaction().commit();
        return lstResult;
    }

    public long markAsDeleteByContractId(Long contractId) {
        long count;
        Session session = getCurrentSession();
        session.beginTransaction();
        StringBuilder sql = new StringBuilder()
                .append("update TBL_CONTRACT_FILES ")
                .append(" set file_status = 'DELETE' ")
                .append(" where CONTRACT_ID = :contractId");

        Query query = session.createSQLQuery(sql.toString());
        query.setParameter("contractId", contractId);

        count = query.executeUpdate();
        session.getTransaction().commit();
        return count;
    }

    public List<ContractFilesDTO> getListMenuContractFilesLiquidation(ContractFilesDTO contractFilesDTO) throws HibernateException {
        List<ContractFilesDTO> lstResult = new ArrayList<>();
        Session session = getCurrentSession();
        session.beginTransaction();
        StringBuilder sql = new StringBuilder()
                .append("select a.id id,a.FILE_CONTRACT_NUMBER fileContractNumber,a.PATH_FILE pathFile,")
                .append("a.START_DATE startDate,a.END_DATE endDate,")
                .append("a.CONTENT,a.FILE_TYPE fileType,")
                .append("a.UPLOAD_DATE uploadDate,a.CONTENT content")
                .append(" from TBL_CONTRACT_FILES a")
                .append(" INNER JOIN TBL_FILE_TYPE b On b.CODE= a.FILE_TYPE")
                .append(" WHERE (b.CODE='HOPDONG' or b.CODE='THANHLI_HĐ')");

        if (StringUtils.isValidString(contractFilesDTO.getContractId())) {
            sql.append(" and a.CONTRACT_ID = :contractId");
        }

        Query query = session.createSQLQuery(sql.toString())
                .addScalar("id", Hibernate.LONG)
                .addScalar("fileContractNumber", Hibernate.STRING)
                .addScalar("fileType", Hibernate.STRING)
                .addScalar("uploadDate", Hibernate.DATE)
                .addScalar("startDate", Hibernate.DATE)
                .addScalar("endDate", Hibernate.DATE)
                .addScalar("pathFile", Hibernate.STRING)
                .addScalar("content", Hibernate.STRING)
                .setResultTransformer(Transformers.aliasToBean(ContractFilesDTO.class));
        if (StringUtils.isValidString(contractFilesDTO.getContractId())) {
            query.setParameter("contractId", contractFilesDTO.getContractId());
        }
        lstResult = query.list();
        session.getTransaction().commit();
        return lstResult;
    }

}
