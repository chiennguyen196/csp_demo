package com.viettel.csp.DAO;

import com.viettel.csp.entity.TaskTypeEntity;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StringType;

import java.util.ArrayList;
import java.util.List;

public class TaskTypeDAO extends BaseCustomDAO<TaskTypeEntity> {

    public TaskTypeDAO() {
        tEntity = new TaskTypeEntity();
        tEntityClass = TaskTypeEntity.class;
    }

    public List<TaskTypeEntity> findAllWithoutInTask(Long contractId) {
        List<TaskTypeEntity> list = new ArrayList<>();
        try {
            Session session = getCurrentSession();
            session.beginTransaction();
            Query query = session.createSQLQuery("SELECT CODE code, NAME name FROM TBL_TASK_TYPE WHERE CODE NOT IN (SELECT TASK_TYPE FROM TBL_TASKS WHERE CONTRACT_ID =:ID_C )")
                    .addScalar("code", new StringType())
                    .addScalar("name", new StringType())
                    .setResultTransformer(Transformers.aliasToBean(TaskTypeEntity.class))
                    .setParameter("ID_C", contractId);
            list = query.list();
            session.getTransaction().commit();
        } catch (Exception e) {
        }
        return list;
    }
}
