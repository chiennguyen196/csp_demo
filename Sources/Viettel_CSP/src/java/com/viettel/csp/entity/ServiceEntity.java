/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.entity;

import com.viettel.csp.DTO.ServiceDTO;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author GEM
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "TBL_SERVICE")
public class ServiceEntity extends BaseCustomEntity implements Serializable {

    @Id
    @Column(name = "code", unique = true, nullable = false)
    private String code;
    @Column(name = "name")
    private String name;
    @Column(name = "parent_code")
    private String parentCode;
    @Column(name = "path_file")
    private String pathFile;

    public ServiceEntity() {
    }

    public ServiceEntity(String code, String name, String parentCode) {
        this.code = code;
        this.name = name;
        this.parentCode = parentCode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParentCode() {
        return parentCode;
    }

    public void setParentCode(String parentCode) {
        this.parentCode = parentCode;
    }

    public String getPathFile() {
        return pathFile;
    }

    public void setPathFile(String pathFile) {
        this.pathFile = pathFile;
    }

    public static ServiceEntity setEntityInsert(ServiceDTO serviceDTO, ServiceEntity serviceEntity) {
        serviceEntity.setCode(serviceDTO.getCode());
        serviceEntity.setParentCode(serviceDTO.getParentCode());
        serviceEntity.setName(serviceDTO.getName());
        return serviceEntity;
    }
}
