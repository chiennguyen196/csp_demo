/**
 * @(#)TelecomServiceBO.java 07/05/2013 10:37 AM, Copyright 2011 Viettel Telecom.
 * All rights reserved VIETTEL PROPRIETARY/CONFIDENTIAL
 */
package com.viettel.csp.entity;

public class EditedDataInListArg {

    private int index;
    private Object data;

    public EditedDataInListArg() {
    }

    public EditedDataInListArg(int index, Object data) {
        this.index = index;
        this.data = data;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
