/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.viettel.csp.entity;

import com.viettel.csp.DTO.BaseCustomDTO;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author GEM
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "TBL_PARTNER_STATUS")
public class PartnerStatusEntity extends BaseCustomEntity implements Serializable{
    @Id
    @Column(name = "CODE")
    private String code;
    @Column(name="NAME")
    private String name;
    
    public PartnerStatusEntity(){
         setColId("code");
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
