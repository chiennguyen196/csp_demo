/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

/**
 *
 * @author admin
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "TBL_DEPARTMENT")
public class DepartmentEntity extends BaseCustomEntity implements Serializable {

    @Column(name = "ORGANIZATIONID")
    private Long organizationId;
    @Column(name = "ORGPARENTID")
    private Long orgparentId;
    @Column(name = "NAME")
    private String name;
    @Column(name = "CODE")
    private String code;
    @Column(name = "ISACTIVE")
    private String isActive;
    @Column(name = "PATH")
    private String path;
    @Id
    @GeneratedValue(generator = "sequence")
    @GenericGenerator(name = "sequence", strategy = "sequence",
            parameters = {
                @Parameter(name = "sequence", value = "TBL_DEPARTMENT_SEQ")
            }
    )
    @Column(name = "ID", unique = true, nullable = false)
    private Long id;

    public DepartmentEntity(Long organizationId, Long orgparentId, String name, String code, String isActive, String path) {
        this.organizationId = organizationId;
        this.orgparentId = orgparentId;
        this.name = name;
        this.code = code;
        this.isActive = isActive;
        this.path = path;
    }

    public DepartmentEntity() {
    }

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public Long getOrgparentId() {
        return orgparentId;
    }

    public void setOrgparentId(Long orgparentId) {
        this.orgparentId = orgparentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
