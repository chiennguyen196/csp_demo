/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.entity;

import com.viettel.csp.DTO.ContractHisDTO;
import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.util.UserTokenUtils;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author GEM
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "TBL_CONTRACT_HIS")
public class ContractHisEntity extends BaseCustomEntity implements Serializable {

    @Id
    @GeneratedValue(generator = "sequence")
    @GenericGenerator(name = "sequence", strategy = "sequence",
            parameters = {
                    @Parameter(name = "sequence", value = "TBL_CONTRACT_HIS_SEQ")
            }
    )
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @Column(name = "CONTRACT_ID")
    private Long contractId;
    @Column(name = "CREATE_BY")
    private Long createBy;
    @Column(name = "CREATE_AT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createAt;
    @Column(name = "STATUS_BEFORE")
    private String statusBefore;
    @Column(name = "STATUS_AFTER")
    private String statusAfter;
    @Column(name = "CONTENT")
    private String content;

    public ContractHisEntity() {
    }

    public ContractHisEntity(Long contractId, String content) {
        this.contractId = contractId;
        this.statusBefore = statusBefore;
        this.statusAfter = statusAfter;
        this.content = content;
        this.createBy = UserTokenUtils.getUserId();
        this.createAt = new Date();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getContractId() {
        return contractId;
    }

    public void setContractId(Long contractId) {
        this.contractId = contractId;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public String getStatusBefore() {
        return statusBefore;
    }

    public void setStatusBefore(String statusBefore) {
        this.statusBefore = statusBefore;
    }

    public String getStatusAfter() {
        return statusAfter;
    }

    public void setStatusAfter(String statusAfter) {
        this.statusAfter = statusAfter;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public static ContractHisEntity setEntity(Long contractId, ContractHisDTO contractHisDTO, ContractHisEntity contractHisEntity) {
        contractHisEntity.setContractId(contractId);
        contractHisEntity.setCreateBy(UserTokenUtils.getUserId());
//        contractHisEntity.setStatusBefore(ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_CANCLE_CONTRACT);// tiennv: bo di cho don gian
//        contractHisEntity.setStatusAfter(ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_OK_CONTRACT);// tiennv: bo di cho don gian
        contractHisEntity.setCreateAt(new Date());
        contractHisEntity.setContent(contractHisDTO.getContent());
        return contractHisEntity;
    }

    public static ContractHisEntity setEntitySendContract(Long contractId, ContractHisDTO contractHisDTO, ContractHisEntity contractHisEntity) {
        contractHisEntity.setContractId(contractId);
        contractHisEntity.setStatusBefore(contractHisDTO.getStatusBefore());
        contractHisEntity.setStatusAfter(contractHisDTO.getStatusAfter());
        contractHisEntity.setContent(contractHisDTO.getContent());
        contractHisEntity.setCreateBy(UserTokenUtils.getUserId());
        contractHisEntity.setCreateAt(new Date());
        return contractHisEntity;
    }

    public static ContractHisEntity setEntityResonCancleRegisterLiquidation(Long contractId, ContractHisDTO contractHisDTO, ContractHisEntity contractHisEntity) {
        contractHisEntity.setContractId(contractId);
        contractHisEntity.setStatusBefore(ParamUtils.LI_STATUS.LIQUIDATION_ASSIGN_REGISTER);
        contractHisEntity.setStatusAfter(ParamUtils.LI_STATUS.LIQUIDATION_REGISTERED_CACLE);
        contractHisEntity.setContent(contractHisDTO.getContent());
        contractHisEntity.setCreateBy(UserTokenUtils.getUserId());
        contractHisEntity.setCreateAt(new Date());
        return contractHisEntity;
    }

    public static ContractHisEntity setEntityResonCancleAssignLiquidation(Long contractId, ContractHisDTO contractHisDTO, ContractHisEntity contractHisEntity) {
        contractHisEntity.setContractId(contractId);
        contractHisEntity.setStatusBefore(ParamUtils.LI_STATUS.LIQUIDATION_ASSIGN_HD);
        contractHisEntity.setStatusAfter(ParamUtils.LI_STATUS.LIQUIDATION_CANCLE_REGISTER);
        contractHisEntity.setContent(contractHisDTO.getContent());
        contractHisEntity.setCreateBy(UserTokenUtils.getUserId());
        contractHisEntity.setCreateAt(new Date());
        return contractHisEntity;
    }

    public static ContractHisEntity setEntityResonCanclePartnerRegister(Long contractId, ContractHisDTO contractHisDTO, ContractHisEntity contractHisEntity) {
        contractHisEntity.setContractId(contractId);
        contractHisEntity.setStatusBefore(ParamUtils.LI_STATUS.LIQUIDATION_SEND_TO_PARTNER);
        contractHisEntity.setStatusAfter(ParamUtils.LI_STATUS.LIQUIDATION_PARTNER_CANCLE_REGISTER);
        contractHisEntity.setContent(contractHisDTO.getContent());
        contractHisEntity.setCreateBy(UserTokenUtils.getUserId());
        contractHisEntity.setCreateAt(new Date());
        return contractHisEntity;
    }

}
