/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

/**
 *
 * @author GEM
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "TBL_CONTRACT_BREACH_DISCIPLINE")
public class ContractBreachDisEntity extends BaseCustomEntity implements Serializable {

    @Id
    @GeneratedValue(generator = "sequence")
    @GenericGenerator(name = "sequence", strategy = "sequence",
            parameters = {
                @Parameter(name = "sequence", value = "TBL_CONTRACT_BREACH_DIS_SEQ")
            }
    )
    @Column(name = "id", unique = true, nullable = false)
    private Long id;
    @Column(name = "DISCIPLINE_CODE")
    private String disCode;
    @Column(name = "PAUSE_START_DATE")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date pauseStartDate;
    @Column(name = "PAUSE_END_DATE")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date pauseEndDate;
    @Column(name = "DATE_OFF")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dateOff;
    @Column(name = "CONTENT")
    private String content;
    @Column(name = "CONTENT_VALUE")
    private String contentValue;
    @Column(name = "CONTRACT_BREACH_ID")
    private Long contractBreachId;

    public ContractBreachDisEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDisCode() {
        return disCode;
    }

    public void setDisCode(String disCode) {
        this.disCode = disCode;
    }

    public Date getPauseStartDate() {
        return pauseStartDate;
    }

    public void setPauseStartDate(Date pauseStartDate) {
        this.pauseStartDate = pauseStartDate;
    }

    public Date getPauseEndDate() {
        return pauseEndDate;
    }

    public void setPauseEndDate(Date pauseEndDate) {
        this.pauseEndDate = pauseEndDate;
    }

    public Date getDateOff() {
        return dateOff;
    }

    public void setDateOff(Date dateOff) {
        this.dateOff = dateOff;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContentValue() {
        return contentValue;
    }

    public void setContentValue(String contentValue) {
        this.contentValue = contentValue;
    }

    public Long getContractBreachId() {
        return contractBreachId;
    }

    public void setContractBreachId(Long contractBreachId) {
        this.contractBreachId = contractBreachId;
    }

}
