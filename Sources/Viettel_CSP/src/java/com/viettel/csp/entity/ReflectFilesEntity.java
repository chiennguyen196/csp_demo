/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.entity;

import com.viettel.csp.DTO.ReflectDTO;
import com.viettel.csp.DTO.ReflectFilesDTO;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

/**
 *
 * @author GEM
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "TBL_REFLECT_FILES")
public class ReflectFilesEntity extends BaseCustomEntity implements Serializable {

    @Id
    @GeneratedValue(generator = "sequence")
    @GenericGenerator(name = "sequence", strategy = "sequence",
            parameters = {
                @Parameter(name = "sequence", value = "TBL_REFLECT_FILES_SEQ")
            }
    )
    @Column(name = "ID", unique = true, nullable = false)
    private Long id;
    @Column(name = "FILE_NAME")
    private String fileName;
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "REFLECT_ID")
    private Long reflectId;
    @Column(name = "UPLOAD_DATE")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date uploadDate;
    @Column(name = "PATH_FILE")
    private String pathFile;

    public ReflectFilesEntity() {
    }

    public ReflectFilesEntity(Long id, String fileName, String description, Long reflectId, Date uploadDate, String pathFile) {
        this.id = id;
        this.fileName = fileName;
        this.description = description;
        this.reflectId = reflectId;
        this.uploadDate = uploadDate;
        this.pathFile = pathFile;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getReflectId() {
        return reflectId;
    }

    public void setReflectId(Long reflectId) {
        this.reflectId = reflectId;
    }

    public Date getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(Date uploadDate) {
        this.uploadDate = uploadDate;
    }

    public String getPathFile() {
        return pathFile;
    }

    public void setPathFile(String pathFile) {
        this.pathFile = pathFile;
    }

    public ReflectFilesEntity getEntity(ReflectFilesDTO reflectFilesDTO) {

        ReflectFilesEntity reflectFilesEntity=new ReflectFilesEntity();

        reflectFilesEntity.setFileName(reflectFilesDTO.getFileName());
        reflectFilesEntity.setDescription(reflectFilesDTO.getDescription());
        reflectFilesEntity.setReflectId(reflectFilesDTO.getReflectId());
        reflectFilesEntity.setPathFile(reflectFilesDTO.getPathFile());
        reflectFilesEntity.setUploadDate(new Date());

        return reflectFilesEntity;

    }

}
