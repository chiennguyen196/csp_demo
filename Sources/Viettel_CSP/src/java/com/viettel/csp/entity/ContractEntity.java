/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.entity;

import com.viettel.csp.DTO.ContractDTO;
import com.viettel.csp.util.ModelMapperUtil;
import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.util.UserTokenUtils;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author GEM
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "TBL_CONTRACT")
public class ContractEntity extends BaseCustomEntity implements Serializable {

    @Id
    @GeneratedValue(generator = "sequence")
    @GenericGenerator(name = "sequence", strategy = "sequence",
            parameters = {
                    @Parameter(name = "sequence", value = "TBL_CONTRACT_SEQ")
            }
    )
    @Column(name = "id", unique = true, nullable = false)
    private Long id;
    @Column(name = "contract_status")
    private String contractStatus;
    @Column(name = "contract_type")
//    @JoinColumn(name ="contract_type")
    private String contractType;
    @Column(name = "contract_number")
    private String contractNumber;
    @Column(name = "partner_id")
    private Long partnerId;
    @Column(name = "service_code")
    private String serviceCode;
    @Column(name = "head_of_service")
    private String headOfService;
    @Column(name = "ct_sign_form")
    private String ctSignForm;
    @Column(name = "start_date")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date startDate;
    @Column(name = "end_date")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date endDate;
    @Column(name = "create_by")
    private Long createBy;
    @Column(name = "create_at")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date createAt;
    @Column(name = "update_by")
    private Long updateBy;
    @Column(name = "update_at")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date updateAt;
    @Column(name = "filing_date")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date filingDate;
    @Column(name = "filing_by")
    private Long filingBy;
    @Column(name = "ct_user_csp")
    private Long ctUserCsp;
    @Column(name = "sign_ct_approve")
    private Long signCtApprove;
    @Column(name = "sign_ct_approve_date")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date signCtApproveDate;
    @Column(name = "sign_ct_expertise_law")
    private Long signCtExpertiseLaw;
    @Column(name = "sign_ct_expertise_law_date")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date signCtExpertiseLawDate;
    @Column(name = "sign_ct_complete_viettel")
    private Long signCtCompleteViettel;
    @Column(name = "sign_ct_complete_viettel_date")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date signCtCompleteViettelDate;
    @Column(name = "sign_ct_complete_partner")
    private Long signCtCompletePartner;
    @Column(name = "sign_ct_complete_partner_date")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date signCtCompletePartnerDate;
    @Column(name = "deploy_user_id")
    private Long deployUserId;
    @Column(name = "deploy_assign_date")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date deployAssignDate;
    @Column(name = "deploy_close_date")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date deployCloseDate;
    @Column(name = "is_lock")
    private Long isLock;
    @Column(name = "li_user_csp")
    private Long liUserCsp;
    @Column(name = "sign_li")
    private Long signLi;
    @Column(name = "sign_li_date")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date signLiDate;
    @Column(name = "SIGN_LI_EXPERTISE_LAW")
    private Long signLiExpertiseLaw;
    @Column(name = "sign_li_expertise_law_date")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date signLiExpertiseLawDate;
    @Column(name = "sign_li_complete_viettel")
    private Long signLiCompleteViettel;
    @Column(name = "sign_li_complete_viettel_date")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date signLiCompleteViettelDate;
    @Column(name = "sign_li_partner")
    private Long signLiPartner;
    @Column(name = "sign_li_partner_date")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date signLiPartnerDate;
    @Column(name = "li_start_date")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date liStartDate;
    @Column(name = "LI_STATUS")
    private String liStatus;
    @Column(name = "LI_SIGN_FORM")
    private String liSignForm;
    @Column(name = "SINGLE_LIQUIDATION")
    private Long singleLiquidation;
    @Column(name = "SINGLE_LIQUIDATION_DATE")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date singleLiquidationDate;
    @Column(name = "SINGLE_LIQUIDATION_START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date singleLiquidationStartDate;

    public ContractEntity() {
    }

    public Long getSingleLiquidation() {
        return singleLiquidation;
    }

    public void setSingleLiquidation(Long singleLiquidation) {
        this.singleLiquidation = singleLiquidation;
    }

    public Date getSingleLiquidationDate() {
        return singleLiquidationDate;
    }

    public void setSingleLiquidationDate(Date singleLiquidationDate) {
        this.singleLiquidationDate = singleLiquidationDate;
    }

    public Date getSingleLiquidationStartDate() {
        return singleLiquidationStartDate;
    }

    public void setSingleLiquidationStartDate(Date singleLiquidationStartDate) {
        this.singleLiquidationStartDate = singleLiquidationStartDate;
    }

    @GenericGenerator(name = "sequence", strategy = "sequence",
            parameters = {
                    @Parameter(name = "sequence", value = "tbl_contract_seq")
            }
    )
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContractStatus() {
        return contractStatus;
    }

    public void setContractStatus(String contractStatus) {
        this.contractStatus = contractStatus;
    }

    public String getContractType() {
        return contractType;
    }

    public void setContractType(String contractType) {
        this.contractType = contractType;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public Long getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(Long partnerId) {
        this.partnerId = partnerId;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getHeadOfService() {
        return headOfService;
    }

    public void setHeadOfService(String headOfService) {
        this.headOfService = headOfService;
    }

    public String getCtSignForm() {
        return ctSignForm;
    }

    public void setCtSignForm(String ctSignForm) {
        this.ctSignForm = ctSignForm;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    public Date getFilingDate() {
        return filingDate;
    }

    public void setFilingDate(Date filingDate) {
        this.filingDate = filingDate;
    }

    public Long getFilingBy() {
        return filingBy;
    }

    public void setFilingBy(Long filingBy) {
        this.filingBy = filingBy;
    }

    public Long getCtUserCsp() {
        return ctUserCsp;
    }

    public void setCtUserCsp(Long ctUserCsp) {
        this.ctUserCsp = ctUserCsp;
    }

    public Long getSignCtApprove() {
        return signCtApprove;
    }

    public void setSignCtApprove(Long signCtApprove) {
        this.signCtApprove = signCtApprove;
    }

    public Date getSignCtApproveDate() {
        return signCtApproveDate;
    }

    public void setSignCtApproveDate(Date signCtApproveDate) {
        this.signCtApproveDate = signCtApproveDate;
    }

    public Long getSignCtExpertiseLaw() {
        return signCtExpertiseLaw;
    }

    public void setSignCtExpertiseLaw(Long signCtExpertiseLaw) {
        this.signCtExpertiseLaw = signCtExpertiseLaw;
    }

    public Date getSignCtExpertiseLawDate() {
        return signCtExpertiseLawDate;
    }

    public void setSignCtExpertiseLawDate(Date signCtExpertiseLawDate) {
        this.signCtExpertiseLawDate = signCtExpertiseLawDate;
    }

    public Long getSignCtCompleteViettel() {
        return signCtCompleteViettel;
    }

    public void setSignCtCompleteViettel(Long signCtCompleteViettel) {
        this.signCtCompleteViettel = signCtCompleteViettel;
    }

    public Date getSignCtCompleteViettelDate() {
        return signCtCompleteViettelDate;
    }

    public void setSignCtCompleteViettelDate(Date signCtCompleteViettelDate) {
        this.signCtCompleteViettelDate = signCtCompleteViettelDate;
    }

    public Long getSignCtCompletePartner() {
        return signCtCompletePartner;
    }

    public void setSignCtCompletePartner(Long signCtCompletePartner) {
        this.signCtCompletePartner = signCtCompletePartner;
    }

    public Date getSignCtCompletePartnerDate() {
        return signCtCompletePartnerDate;
    }

    public void setSignCtCompletePartnerDate(Date signCtCompletePartnerDate) {
        this.signCtCompletePartnerDate = signCtCompletePartnerDate;
    }

    public Long getDeployUserId() {
        return deployUserId;
    }

    public void setDeployUserId(Long deployUserId) {
        this.deployUserId = deployUserId;
    }

    public Date getDeployAssignDate() {
        return deployAssignDate;
    }

    public void setDeployAssignDate(Date deployAssignDate) {
        this.deployAssignDate = deployAssignDate;
    }

    public Date getDeployCloseDate() {
        return deployCloseDate;
    }

    public void setDeployCloseDate(Date deployCloseDate) {
        this.deployCloseDate = deployCloseDate;
    }

    public Long getIsLock() {
        return isLock;
    }

    public void setIsLock(Long isLock) {
        this.isLock = isLock;
    }

    public Long getLiUserCsp() {
        return liUserCsp;
    }

    public void setLiUserCsp(Long liUserCsp) {
        this.liUserCsp = liUserCsp;
    }

    public Long getSignLi() {
        return signLi;
    }

    public void setSignLi(Long signLi) {
        this.signLi = signLi;
    }

    public Date getSignLiDate() {
        return signLiDate;
    }

    public void setSignLiDate(Date signLiDate) {
        this.signLiDate = signLiDate;
    }

    public Long getSignLiExpertiseLaw() {
        return signLiExpertiseLaw;
    }

    public void setSignLiExpertiseLaw(Long signLiExpertiseLaw) {
        this.signLiExpertiseLaw = signLiExpertiseLaw;
    }

    public Date getSignLiExpertiseLawDate() {
        return signLiExpertiseLawDate;
    }

    public void setSignLiExpertiseLawDate(Date signLiExpertiseLawDate) {
        this.signLiExpertiseLawDate = signLiExpertiseLawDate;
    }

    public Long getSignLiCompleteViettel() {
        return signLiCompleteViettel;
    }

    public void setSignLiCompleteViettel(Long signLiCompleteViettel) {
        this.signLiCompleteViettel = signLiCompleteViettel;
    }

    public Date getSignLiCompleteViettelDate() {
        return signLiCompleteViettelDate;
    }

    public void setSignLiCompleteViettelDate(Date signLiCompleteViettelDate) {
        this.signLiCompleteViettelDate = signLiCompleteViettelDate;
    }

    public Long getSignLiPartner() {
        return signLiPartner;
    }

    public void setSignLiPartner(Long signLiPartner) {
        this.signLiPartner = signLiPartner;
    }

    public Date getSignLiPartnerDate() {
        return signLiPartnerDate;
    }

    public void setSignLiPartnerDate(Date signLiPartnerDate) {
        this.signLiPartnerDate = signLiPartnerDate;
    }

    public Date getLiStartDate() {
        return liStartDate;
    }

    public void setLiStartDate(Date liStartDate) {
        this.liStartDate = liStartDate;
    }

    public String getLiStatus() {
        return liStatus;
    }

    public void setLiStatus(String liStatus) {
        this.liStatus = liStatus;
    }

    public String getLiSignForm() {
        return liSignForm;
    }

    public void setLiSignForm(String liSignForm) {
        this.liSignForm = liSignForm;
    }

    public static ContractEntity setEntity(ContractDTO contractDTO, ContractEntity contractEntity, int flat) {
        switch (flat) {
            //6/15/2017_5:14 PM_START_tiennv_says: comment khong dung
//            case ParamUtils.TYPE_ACTION.OK_CONTRACT:
//                contractEntity.setContractStatus(ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_OK_CONTRACT);
//                break;
            //6/15/2017_5:14 PM_END_tiennv_says_end

            case ParamUtils.TYPE_ACTION.NOK_APPROVE_PROFILE:
                contractEntity.setContractStatus(ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_CANCLE_REGIS_OK_CONTRACT_FILES);
                break;
            case ParamUtils.TYPE_ACTION.CANCLE_CONTRACT:
                contractEntity.setContractStatus(ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_NOT_OK_CONTRACT);
                break;
            case ParamUtils.TYPE_ACTION.WAITING_OK_CONTRACT_FILES:
                contractEntity.setContractStatus(ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_WAITING_OK_CONTRACT_FILES);
                break;
            case ParamUtils.TYPE_ACTION.OK_ACCEPT:
                contractEntity.setContractStatus(ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_OK_ACCEPT);
                break;
            case ParamUtils.TYPE_ACTION.NO_ACCEPT:
                contractEntity.setContractStatus(ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_NO_ACCEPT);
                break;
            case ParamUtils.TYPE_ACTION.WAITING_REFUSE_CONTRACT_FILES:
                contractEntity.setContractStatus(ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_NOT_OK_CONTRACT);
                break;
            case ParamUtils.TYPE_ACTION.WAITING_SEND_CONTRACT_FILES:
                contractEntity.setContractStatus(ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_OK_CONTRACT_FILES_FOR_PARTNER);
                break;
            case ParamUtils.TYPE_ACTION.OK_SEND_CONTRACT_FILES:
                contractEntity.setContractStatus(ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_REGIS_OK_CONTRACT_FILES);
                break;
            case ParamUtils.TYPE_ACTION.OK_SEND_CONTRACT_TO_CSP_REGISTER:
                contractEntity.setContractStatus(ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_OK_CONTRACT_FILES_FOR_PARTNER);
                break;
            case ParamUtils.TYPE_ACTION.OK_SEND_CONTRACT_WAITING_TO_CSP_REGISTER:
                contractEntity.setContractStatus(ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_OK_CONTRACT_FILES_WAITING_FOR_PARTNER);
                break;
            case ParamUtils.TYPE_ACTION.CREAT_CONTRACT_REGISTER_PARTNER:
                contractEntity.setContractStatus(ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_OK_CONTRACT_FILES_REGISTED);
                break;
            case ParamUtils.TYPE_ACTION.CANCLE_CONTRACT_APPROVAL:
                contractEntity.setContractStatus(ParamUtils.LI_STATUS.LIQUIDATION_SENDTOVT);
                break;
            case ParamUtils.TYPE_ACTION.EFFECTING_CONTRACT:
                contractEntity.setContractStatus(ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_OK_CONTRACT_EFFECTING_CONTRACT);
                break;
            case ParamUtils.TYPE_ACTION.TERMINATED_CONTRACT:
                contractEntity.setContractStatus(ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_OK_CONTRACT_TERMINATED_CONTRACT);
                break;
            case ParamUtils.TYPE_ACTION.CREAT_CONTRACT:
                contractEntity.setContractStatus(ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_WAITING_OK_CONTRACT);
                break;
            case ParamUtils.TYPE_ACTION.SEND_CONTRACT_TO_VIETTEL:
                contractEntity.setContractStatus(ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_OK_CONTRACT_FILES_FOR_PARTNER_1);
                break;
            case ParamUtils.TYPE_ACTION.TGD_CANCLE_VIETTEL:
                contractEntity.setContractStatus(ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_TGD_CANCLE);
                break;
            case ParamUtils.TYPE_ACTION.TGD_CANCLE_VIETTEL_REGISTER:
                contractEntity.setContractStatus(ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_REGISTER);
                break;
            case ParamUtils.TYPE_ACTION.NOT_ACTIVE_TASK_CONTRACT:
                contractEntity.setContractStatus(ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_NO_ACCEPT);
                break;
            case ParamUtils.TYPE_ACTION.SAVE_AND_DEPLOYMENT_CONTRACT:
                contractEntity.setContractStatus(ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_SAVE_AND_DEPLOYMENT);
                contractEntity.setDeployAssignDate(contractDTO.getDeployAssignDate());
                contractEntity.setDeployUserId(contractDTO.getDeployUserId());
                break;
            default:
                break;
        }

        contractEntity.setFilingDate(new Date());
        contractEntity.setUpdateAt(new Date());
        contractEntity.setUpdateBy(UserTokenUtils.getUserId());
        if (contractDTO.getSignCtApprove() != null) {
            contractEntity.setSignCtApprove(contractDTO.getSignCtApprove());
        }
        return contractEntity;
    }

    public static ContractEntity setEntityLiquidationCreat(ContractDTO contractDTO, ContractEntity contractEntity, String liStatus) {
            contractEntity.setLiStatus(liStatus);
        contractEntity.setContractStatus(liStatus);
        contractEntity.setPartnerId(UserTokenUtils.getUserId());
        contractEntity.setFilingDate(new Date());
        contractEntity.setUpdateAt(new Date());
        contractEntity.setUpdateBy(UserTokenUtils.getUserId());
        contractEntity.setSignCtApprove(contractDTO.getSignCtApprove());
        contractEntity.setSignLi(UserTokenUtils.getUserId());
        contractEntity.setSignLiDate(new Date());
        contractEntity.setSignLiExpertiseLaw(contractDTO.getSignLiExpertiseLaw());
        contractEntity.setSignLiExpertiseLawDate(new Date());
        contractEntity.setSignLiCompleteViettel(contractDTO.getSignLiCompleteViettel());
        contractEntity.setSignLiCompleteViettelDate(new Date());
        contractEntity.setSignLiPartner(contractDTO.getSignLiPartner());
        contractEntity.setSignLiPartnerDate(contractDTO.getSignLiPartnerDate());
        contractEntity.setLiSignForm(contractDTO.getLiSignForm());
        contractEntity.setSingleLiquidation(contractDTO.getSingleLiquidation());
        contractEntity.setSingleLiquidationDate(contractDTO.getSingleLiquidationDate());
        contractEntity.setSingleLiquidationStartDate(contractDTO.getSingleLiquidationStartDate());
        return contractEntity;
    }

    public static ContractEntity setEntityUpdate(ContractDTO contractDTO, ContractEntity contractEntity, int flat) {
        switch (flat) {
            case ParamUtils.CONTRACT_PARTNER.CONTRACTSENDPARTNER:
                contractEntity.setContractStatus(ParamUtils.CONTRACT_PARTNER.CONTRACTSENDPARTNERSTATUS);
                break;
            case ParamUtils.CONTRACT_PARTNER.CONTRACT_STARUS_REGISTER:
                contractEntity.setContractStatus(ParamUtils.CONTRACT_PARTNER.CONTRACT_REGISTER);
                break;
            case ParamUtils.CONTRACT_PARTNER.CONTRACT_CANCEL_REGISTER:
                contractEntity.setContractStatus(ParamUtils.CONTRACT_PARTNER.CONTRACT_STATUS_CANCEL_REGISTER);
                break;
            case ParamUtils.CONTRACT_PARTNER.CONTRACT_SEND_PARNER:
                contractEntity.setContractStatus(ParamUtils.CONTRACT_PARTNER.CONTRACT_STATUS_SEND_PARNER);
                break;
            default:
                break;
        }
        contractEntity.setPartnerId(UserTokenUtils.getUserId());
        contractEntity.setFilingDate(contractDTO.getFilingDate());
        contractEntity.setUpdateAt(new Date());
        contractEntity.setUpdateBy(UserTokenUtils.getUserId());
        contractEntity.setSignCtApprove(contractDTO.getSignCtApprove());
        return contractEntity;
    }

    public static ContractEntity setEntityInsert(ContractDTO contractDTO, ContractEntity contractEntity, int flat) {
        switch (flat) {
            case ParamUtils.TYPE_ACTION.WAITING_OK_CONTRACT_FILES:
                contractEntity.setContractStatus(ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_WAITING_OK_CONTRACT);
                break;
            //6/15/2017_5:13 PM_START_tiennv_says: comment khong dung
//            case ParamUtils.TYPE_ACTION.OK_CONTRACT:
//                contractEntity.setContractStatus(ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_OK_CONTRACT);
//                break;
            //6/15/2017_5:13 PM_END_tiennv_says_end
            case ParamUtils.TYPE_ACTION.WAITING_SEND_CONTRACT_FILES:
                contractEntity.setContractStatus(ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_OK_CONTRACT_FILES_FOR_PARTNER);
                break;
            default:
                break;
        }
        contractEntity.setPartnerId(UserTokenUtils.getPartnerId());
        contractEntity.setFilingDate(new Date());
        contractEntity.setUpdateAt(new Date());
        contractEntity.setUpdateBy(UserTokenUtils.getUserId());
        contractEntity.setCreateBy(UserTokenUtils.getUserId());
        contractEntity.setCreateAt(new Date());
        contractEntity.setFilingBy(UserTokenUtils.getUserId());
        if (contractDTO.getContractNumber() != null) {
            contractEntity.setContractNumber(contractDTO.getContractNumber());
        }
        if (contractDTO.getServiceCode() != null) {
            contractEntity.setServiceCode(contractDTO.getServiceCode());
        }
        if (contractDTO.getContractType() != null) {
            contractEntity.setContractType(contractDTO.getContractType());
        }
        if (contractDTO.getCtSignForm() != null) {
            contractEntity.setCtSignForm(contractDTO.getCtSignForm());
        }
        if (contractDTO.getStartDate() != null) {
            contractEntity.setStartDate(contractDTO.getStartDate());
        }
        if (contractDTO.getEndDate() != null) {
            contractEntity.setEndDate(contractDTO.getEndDate());
        }
        if (contractDTO.getHeadOfService() != null) {
            contractEntity.setHeadOfService(contractDTO.getHeadOfService());
        }
        return contractEntity;
    }

    public static ContractEntity setEntityInsertStatus(ContractDTO contractDTO, ContractEntity contractEntity, int flat) {
        switch (flat) {
            case ParamUtils.CONTRACT_PARTNER.CONTRACTSENDPARTNER:
                contractEntity.setContractStatus(ParamUtils.CONTRACT_PARTNER.CONTRACTSENDPARTNERSTATUS);
                break;
            default:
                break;
        }
//        contractEntity.setContractStatus(ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_OK_CONTRACT);
        contractEntity.setPartnerId(UserTokenUtils.getUserId());
        contractEntity.setFilingDate(contractDTO.getFilingDate());
        contractEntity.setUpdateAt(new Date());
        contractEntity.setUpdateBy(0L);
        contractEntity.setCreateBy(UserTokenUtils.getUserId());
        contractEntity.setCreateAt(new Date());
        contractEntity.setFilingBy(UserTokenUtils.getUserId());
        if (contractDTO.getContractNumber() != null) {
            contractEntity.setContractNumber(contractDTO.getContractNumber());
        }
        if (contractDTO.getServiceCode() != null) {
            contractEntity.setServiceCode(contractDTO.getServiceCode());
        }
        if (contractDTO.getContractType() != null) {
            contractEntity.setContractType(contractDTO.getContractType());
        }
        if (contractDTO.getCtSignForm() != null) {
            contractEntity.setCtSignForm(contractDTO.getCtSignForm());
        }
        if (contractDTO.getStartDate() != null) {
            contractEntity.setStartDate(contractDTO.getStartDate());
        }
        if (contractDTO.getEndDate() != null) {
            contractEntity.setEndDate(contractDTO.getEndDate());
        }
        if (contractDTO.getHeadOfService() != null) {
            contractEntity.setHeadOfService(contractDTO.getHeadOfService());
        }
        if (contractDTO.getSignCtApprove() != null) {
            contractEntity.setSignCtApprove(contractDTO.getSignCtApprove());
        }
        return contractEntity;
    }
}
