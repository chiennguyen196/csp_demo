/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

/**
 *
 * @author GEM
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "TBL_CONTRACT_BREACH")
public class ContractBreachEntity extends BaseCustomEntity implements Serializable {

    @Id
    @GeneratedValue(generator = "sequence")
    @GenericGenerator(name = "sequence", strategy = "sequence",
            parameters = {
                @Parameter(name = "sequence", value = "TBL_CONTRACT_BREACH_SEQ")
            }
    )
    @Column(name = "id", unique = true, nullable = false)
    private Long id;
    @Column(name = "CONTRACT_ID")
    private Long contractId;
    @Column(name = "PARTNER_ID")
    private Long partnerId;
    @Column(name = "START_DATE")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date startDate;
    @Column(name = "END_DATE")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date endDate;
    @Column(name = "IS_PUBLIC")
    private Long isPublic;
    @Column(name = "CREATE_AT")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date createAt;
    @Column(name = "CREATE_BY")
    private Long createBy;
    @Column(name = "UPDATE_AT")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date updateAt;
    @Column(name = "UPDATE_BY")
    private Long updateBY;
    @Column(name = "REASON_DISCIPLINE_CODE") //ly do vi pham
    private String reasonDisciplineCode;
    @Column(name = "HEAD_OF_SERVICE")
    private String headOfService;

    public ContractBreachEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getContractId() {
        return contractId;
    }

    public void setContractId(Long contractId) {
        this.contractId = contractId;
    }

    public Long getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(Long partnerId) {
        this.partnerId = partnerId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Long getIsPublic() {
        return isPublic;
    }

    public void setIsPublic(Long isPublic) {
        this.isPublic = isPublic;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    public Long getUpdateBY() {
        return updateBY;
    }

    public void setUpdateBY(Long updateBY) {
        this.updateBY = updateBY;
    }

    public String getReasonDisciplineCode() {
        return reasonDisciplineCode;
    }

    public void setReasonDisciplineCode(String reasonDisciplineCode) {
        this.reasonDisciplineCode = reasonDisciplineCode;
    }

    public String getHeadOfService() {
        return headOfService;
    }

    public void setHeadOfService(String headOfService) {
        this.headOfService = headOfService;
    }
}
