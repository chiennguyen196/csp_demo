/**
 * @(#)FmApParamEntity.java , Copyright 2013 Viettel IT. All rights reserved
 * VIETTEL PROPRIETARY/CONFIDENTIAL
 */
package com.viettel.csp.entity;

import com.viettel.csp.DTO.BaseCustomDTO;
import javax.persistence.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import com.viettel.csp.util.KeyValueBean;
import com.viettel.csp.util.LanguageBundleUtils;
import java.io.Serializable;

/**
 * @author binhnt22
 * @version 1.0
 * @since 29/08/2013 9:07 PM
 */
public class FmApParamEntity extends BaseCustomEntity implements Serializable {

    //Fields
    private Long apParamId;
    private String paramGroup;
    private String paramCode;
    private String paramName;
    private String description;
    private Long status;
    private Long businessValue;

    //Constructors
    public FmApParamEntity() {
    }

    public FmApParamEntity(Long apParamId) {
        this.apParamId = apParamId;
    }

    public FmApParamEntity(Long apParamId, String paramGroup, String paramCode, String paramName, String description, Long status, Long businessValue) {
        this.apParamId = apParamId;
        this.paramGroup = paramGroup;
        this.paramCode = paramCode;
        this.paramName = paramName;
        this.description = description;
        this.status = status;
        this.businessValue = businessValue;
    }

    public Long getApParamId() {
        return this.apParamId;
    }

    public void setApParamId(final Long apParamId) {
        this.apParamId = apParamId;
    }

    public String getParamGroup() {
        return this.paramGroup;
    }

    public void setParamGroup(final String paramGroup) {
        this.paramGroup = paramGroup;
    }

    public String getParamCode() {
        return this.paramCode;
    }

    public void setParamCode(final String paramCode) {
        this.paramCode = paramCode;
    }

    public String getParamName() {
        return this.paramName;
    }

    public void setParamName(final String paramName) {
        this.paramName = paramName;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public Long getStatus() {
        return this.status;
    }

    public void setStatus(final Long status) {
        this.status = status;
    }

    public Long getBusinessValue() {
        return this.businessValue;
    }

    public void setBusinessValue(final Long businessValue) {
        this.businessValue = businessValue;
    }

//    @Override
    public KeyValueBean toBean() {
        KeyValueBean bean = new KeyValueBean(businessValue, LanguageBundleUtils.getString(paramName));
        return bean;
    }
}
