/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.entity;

import com.viettel.csp.DTO.PartnerDTO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

/**
 *
 * @author GEM
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "TBL_PARTNER")
public class PartnerEntity extends BaseCustomEntity implements Serializable {

    @Id
    @GeneratedValue(generator = "sequence")
    @GenericGenerator(name = "sequence", strategy = "sequence",
            parameters = {
                @Parameter(name = "sequence", value = "TBL_PARTNER_SEQ")
            }
    )
    @Column(name = "ID", unique = true, nullable = false)
    private Long id;
    @Column(name = "PARTNER_TYPE")
    private String partnerType;
    @Column(name = "PARTNER_STATUS")
    private String partnerStatus;
    @Column(name = "PARTNER_CODE")
    private String partnerCode;
    @Column(name = "COMPANY_NAME")
    private String companyName;
    @Column(name = "COMPANY_NAME_SHORT")
    private String companyNameShort;
    @Column(name = "ADDRESS_HEAD_OFFICE")
    private String addressHeadOffice;
    @Column(name = "ADDRESS_TRADING_OFFICE")
    private String addressTradingOffice;
    @Column(name = "PHONE")
    private String phone;
    @Column(name = "REP_NAME")
    private String repName;
    @Column(name = "REP_POSITION")
    private String repPosition;
    @Column(name = "EMAIL")
    private String email;
    @Column(name = "TAX_CODE")
    private String taxCode;
    @Column(name = "BUSINESS_REGIS_NUMBER")
    private String businessRegisNumber;
    @Column(name = "BUSINESS_REGIS_DATE")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date businessRegisDate;
    @Column(name = "BUSINESS_REGIS_COUNT")
    private String businessRegisCount;
    @Column(name = "PARTNER_GROUP")
    private String partnerGroup;
    @Column(name = "PARTNER_GROUP_PROVINCE")
    private String partnerGroupProvince;
    @Column(name = "CREATE_AT")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date createAt;
    @Column(name = "CREATE_BY")
    private Long createBy;
    @Column(name = "UPDATE_AT")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date updateAt;
    @Column(name = "UPDATE_BY")
    private Long updateBy;
    @Column(name = "is_Lock")
    private Long isLock;
    @OneToMany
    @Cascade(CascadeType.ALL)
    @JoinColumn(name = "PARTNER_ID")
    private List<PartnerBankEntity> partnerBankEntities;

    public List<PartnerBankEntity> getPartnerBankEntities() {
        return partnerBankEntities;
    }

    public void setPartnerBankEntities(List<PartnerBankEntity> partnerBankEntities) {
        this.partnerBankEntities = partnerBankEntities;
    }

    public PartnerEntity() {
    }

    public Long getIsLock() {
        return isLock;
    }

    public void setIsLock(Long isLock) {
        this.isLock = isLock;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPartnerType() {
        return partnerType;
    }

    public void setPartnerType(String partnerType) {
        this.partnerType = partnerType;
    }

    public String getPartnerStatus() {
        return partnerStatus;
    }

    public void setPartnerStatus(String partnerStatus) {
        this.partnerStatus = partnerStatus;
    }

    public String getPartnerCode() {
        return partnerCode;
    }

    public void setPartnerCode(String partnerCode) {
        this.partnerCode = partnerCode;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyNameShort() {
        return companyNameShort;
    }

    public void setCompanyNameShort(String companyNameShort) {
        this.companyNameShort = companyNameShort;
    }

    public String getAddressHeadOffice() {
        return addressHeadOffice;
    }

    public void setAddressHeadOffice(String addressHeadOffice) {
        this.addressHeadOffice = addressHeadOffice;
    }

    public String getAddressTradingOffice() {
        return addressTradingOffice;
    }

    public void setAddressTradingOffice(String addressTradingOffice) {
        this.addressTradingOffice = addressTradingOffice;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRepName() {
        return repName;
    }

    public void setRepName(String repName) {
        this.repName = repName;
    }

    public String getRepPosition() {
        return repPosition;
    }

    public void setRepPosition(String repPosition) {
        this.repPosition = repPosition;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTaxCode() {
        return taxCode;
    }

    public void setTaxCode(String taxCode) {
        this.taxCode = taxCode;
    }

    public String getBusinessRegisNumber() {
        return businessRegisNumber;
    }

    public void setBusinessRegisNumber(String businessRegisNumber) {
        this.businessRegisNumber = businessRegisNumber;
    }

    public Date getBusinessRegisDate() {
        return businessRegisDate;
    }

    public void setBusinessRegisDate(Date businessRegisDate) {
        this.businessRegisDate = businessRegisDate;
    }

    public String getBusinessRegisCount() {
        return businessRegisCount;
    }

    public void setBusinessRegisCount(String businessRegisCount) {
        this.businessRegisCount = businessRegisCount;
    }

    public String getPartnerGroup() {
        return partnerGroup;
    }

    public void setPartnerGroup(String partnerGroup) {
        this.partnerGroup = partnerGroup;
    }

    public String getPartnerGroupProvince() {
        return partnerGroupProvince;
    }

    public void setPartnerGroupProvince(String partnerGroupProvince) {
        this.partnerGroupProvince = partnerGroupProvince;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public static PartnerEntity setEntity(PartnerDTO partnerDTO, PartnerEntity partnerEntity) {
        partnerEntity.setCompanyName(partnerDTO.getCompanyName());
        partnerEntity.setCompanyNameShort(partnerDTO.getCompanyNameShort());
        partnerEntity.setAddressHeadOffice(partnerDTO.getAddressOffice());
        partnerEntity.setAddressTradingOffice(partnerDTO.getAddressTradingOffice());
        partnerEntity.setAddressTradingOffice(partnerDTO.getAddressTradingOffice());
        partnerEntity.setPhone(partnerDTO.getPhone());
        partnerEntity.setRepName(partnerDTO.getRepName());
        partnerEntity.setRepPosition(partnerDTO.getRepPosition());
        partnerEntity.setEmail(partnerDTO.getEmail());
        partnerEntity.setTaxCode(partnerDTO.getTaxCode());
        partnerEntity.setBusinessRegisNumber(partnerDTO.getBusinessRegisNumber());
        partnerEntity.setBusinessRegisDate(partnerDTO.getBusinessRegisDate());
        partnerEntity.setBusinessRegisCount(partnerDTO.getBusinessRegisCount());
        partnerEntity.setPartnerGroup(partnerDTO.getPartnerGroup());
        partnerEntity.setPartnerGroupProvince(partnerDTO.getPartnerGroupProvince());
        partnerEntity.setPartnerType(partnerDTO.getPartnerTypeCode());
//            partnerEntity.setPartnerStatus(partnerDTO.getPartnerStatusCode());
        partnerEntity.setUpdateAt(new Date());
        partnerEntity.setCreateAt(new Date());

        List<PartnerBankEntity> partnerBankEntities = new ArrayList<>();
        for (int i = 0; i < partnerDTO.getLstPartnerBank().size(); i++) {
            partnerBankEntities.add(PartnerBankEntity.setEntity(partnerDTO.getLstPartnerBank().get(i), new PartnerBankEntity()));
        }
        partnerEntity.setPartnerBankEntities(partnerBankEntities);
        return partnerEntity;
    }
}
