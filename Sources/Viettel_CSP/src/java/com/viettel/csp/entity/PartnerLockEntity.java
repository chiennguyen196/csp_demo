/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.entity;

import com.viettel.csp.DTO.PartnerLockDTO;
import com.viettel.csp.util.UserTokenUtils;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

/**
 *
 * @author TienNV@gemvietnam.com
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "TBL_PARTNER_LOCK")
public class PartnerLockEntity extends BaseCustomEntity implements Serializable {

    @Id
    @GeneratedValue(generator = "sequence")
    @GenericGenerator(name = "sequence", strategy = "sequence",
            parameters = {
                @Parameter(name = "sequence", value = "TBL_PARTNER_LOCK_SEQ")
            }
    )
    @Column(name = "id", unique = true, nullable = false)
    private Long id;
    @Column(name = "REASON_CODE")
    private String reasonCode;
    @Column(name = "START_DATE")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date startDate;
    @Column(name = "END_DATE")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date endDate;
    @Column(name = "CREATE_AT")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date createAt;
    @Column(name = "UPDATE_AT")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date updateAt;
    @Column(name = "UPDATE_BY")
    private Long updateBy;
    @Column(name = "CREATE_BY")
    private Long createBy;
    @Column(name = "CONTENT")
    private String content;
    @Column(name = "PARTNER_ID")
    private Long partnerId;

    public Long getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(Long partnerId) {
        this.partnerId = partnerId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public static PartnerLockEntity setEntity(PartnerLockEntity partnerLockEntity, PartnerLockDTO partnerLockDTO) {
        if (partnerLockEntity == null) {
            partnerLockEntity = new PartnerLockEntity();
            partnerLockEntity.setCreateAt(new Date());
            partnerLockEntity.setCreateBy(UserTokenUtils.getUserId());
        }
        partnerLockEntity.setUpdateAt(new Date());
        partnerLockEntity.setUpdateBy(UserTokenUtils.getUserId());
        partnerLockEntity.setEndDate(partnerLockDTO.getEndDate());
        partnerLockEntity.setStartDate(partnerLockDTO.getStartDate());
        partnerLockEntity.setReasonCode(partnerLockDTO.getReasonCode());
        partnerLockEntity.setContent(partnerLockDTO.getContent());
        partnerLockEntity.setPartnerId(partnerLockDTO.getPartnerId());
        return partnerLockEntity;
    }
}
