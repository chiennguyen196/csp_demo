package com.viettel.csp.entity;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by GEM on 6/16/2017.
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "TBL_CONTRACT_BREACH_FILES")
public class ContractBreachFilesEntity extends BaseCustomEntity {
    @Id
    @GeneratedValue(generator = "sequence")
    @GenericGenerator(name = "sequence", strategy = "sequence",
            parameters = {
                    @Parameter(name = "sequence", value = "TBL_CONTRACT_BREACH_FILES_SEQ")
            }
    )
    @Column(name = "id", unique = true, nullable = false)
    private Long id;
    @Column(name = "FILE_NAME")
    private String fileName;
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "PARTNER_BREACH_ID")
    private Long partnerBreachId;
    @Column(name = "UPLOAD_DATE")
    private Date uploadDate;
    @Column(name = "PATH_FILE")
    private String pathFile;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getPartnerBreachId() {
        return partnerBreachId;
    }

    public void setPartnerBreachId(Long partnerBreachId) {
        this.partnerBreachId = partnerBreachId;
    }

    public Date getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(Date uploadDate) {
        this.uploadDate = uploadDate;
    }

    public String getPathFile() {
        return pathFile;
    }

    public void setPathFile(String pathFile) {
        this.pathFile = pathFile;
    }
}
