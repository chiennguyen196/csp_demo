/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.entity;

import com.viettel.csp.DTO.ContactPointDTO;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author GEM
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "TBL_CONTACT_POINT")
public class ContactPointEntity extends BaseCustomEntity implements Serializable
{

    @Id
    @GeneratedValue(generator = "sequence")
    @GenericGenerator(name = "sequence", strategy = "sequence",
            parameters = {
                    @Parameter(name = "sequence", value = "TBL_CONTACT_POINT_SEQ")
            }
    )
    @Column(name = "id", unique = true, nullable = false)
    private Long id;
    @Column(name = "contact_id")
    private Long contactId;
    @Column(name = "contract_Id")
    private Long contractId;
    @Column(name = "contact_Type")
    private String contactType;

    public ContactPointEntity()
    {
    }

    public ContactPointEntity(Long id, Long contactId, Long contractId, String contactType)
    {
        this.id = id;
        this.contactId = contactId;
        this.contractId = contractId;
        this.contactType = contactType;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getContactId()
    {
        return contactId;
    }

    public void setContactId(Long contactId)
    {
        this.contactId = contactId;
    }

    public Long getContractId()
    {
        return contractId;
    }

    public void setContractId(Long contractId)
    {
        this.contractId = contractId;
    }

    public String getContactType()
    {
        return contactType;
    }

    public void setContactType(String contactType)
    {
        this.contactType = contactType;
    }

    public static ContactPointEntity setEntity(ContactPointDTO contactPointDTO, ContactPointEntity contactPointEntity)
    {
        contactPointEntity = new ContactPointEntity();
        contactPointEntity.setContactId(contactPointDTO.getContactId());
        contactPointEntity.setContractId(contactPointDTO.getContractId());
        contactPointEntity.setContactType(contactPointDTO.getContactType());
        if (contactPointDTO.getId() != null)
        {
            contactPointEntity.setId(contactPointDTO.getId());
        }
        return contactPointEntity;
    }

}
