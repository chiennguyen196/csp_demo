/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.entity;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import com.viettel.csp.DTO.TaskFilesDTO;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

/**
 *
 * @author GEM
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "TBL_TASK_FILES")
public class TaskFilesEntity extends BaseCustomEntity implements Serializable {

    @Id
    @GeneratedValue(generator = "sequence")
    @GenericGenerator(name = "sequence", strategy = "sequence",
            parameters = {
                @Parameter(name = "sequence", value = "TBL_TASK_FILES_SEQ")
            }
    )
    @Column(name = "ID", unique = true, nullable = false)
    private long id;
    @Column(name = "FILE_NAME")
    private String fileName;
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "TASK_ID")
    private long taskId;
    @Column(name = "UPLOAD_DATE")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date uploadDate;
    @Column(name = "PATH_FILE")
    private String pathFile;

    public TaskFilesEntity() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getTaskId() {
        return taskId;
    }

    public void setTaskId(long taskId) {
        this.taskId = taskId;
    }

    public Date getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(Date uploadDate) {
        this.uploadDate = uploadDate;
    }

    public String getPathFile() {
        return pathFile;
    }

    public void setPathFile(String pathFile) {
        this.pathFile = pathFile;
    }

    public static TaskFilesEntity setEntityInsert(TaskFilesDTO taskFilesDTO, TaskFilesEntity taskFilesEntity) {
        taskFilesEntity.setDescription(taskFilesDTO.getDescription());
        taskFilesEntity.setFileName(taskFilesDTO.getFileName());
        taskFilesEntity.setPathFile(taskFilesDTO.getPathFile());
        taskFilesEntity.setTaskId(taskFilesDTO.getTaskId());
        taskFilesEntity.setUploadDate(Calendar.getInstance().getTime());
        return taskFilesEntity;
    }
}
