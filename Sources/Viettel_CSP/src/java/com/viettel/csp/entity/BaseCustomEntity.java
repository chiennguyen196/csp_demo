/*
 * Copyright (C) 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.viettel.csp.entity;

import java.util.Date;
import java.util.HashMap;

/**
 *
 * @author kdvt_binhnt22@viettel.com.vn
 * @version 1.0
 * @since May 2012
 */
public abstract class BaseCustomEntity {

    private transient String colId = "ID";
    private transient String colName = "NAME";
    private transient String[] uniqueColumn = new String[0];
    private transient static HashMap<String, Date> hmLastUpdateBoTimes = new HashMap<String, Date>();

    public String getBOName() {
        return this.getClass().getSimpleName();
    }

    public String getColId() {
        return colId;
    }

    public void setColId(String colId) {
        this.colId = colId;
    }

    public String getColName() {
        return colName;
    }

    public void setColName(String colName) {
        this.colName = colName;
    }

    public String[] getUniqueColumn() {
        return uniqueColumn;
    }

    public void setUniqueColumn(String[] uniqueColumn) {
        this.uniqueColumn = uniqueColumn;
    }

    public static void putToMap(String boName, Date date) {
        hmLastUpdateBoTimes.put(boName, date);
    }

    public static Date getFromMap(String boName) {
        return hmLastUpdateBoTimes.get(boName);
    }

    public String listSelectColumn() {
        return "*";
    }

    public void setId(Long id) {
    }
}
