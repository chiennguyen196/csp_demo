/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.entity;

import com.viettel.csp.DTO.ContractDTO;
import com.viettel.csp.DTO.ContractFilesDTO;
import com.viettel.csp.util.ParamUtils;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author GEM
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "TBL_CONTRACT_FILES")
public class ContractFilesEntity extends BaseCustomEntity implements Serializable {

    @Id
    @GeneratedValue(generator = "sequence")
    @GenericGenerator(name = "sequence", strategy = "sequence",
            parameters = {
                    @Parameter(name = "sequence", value = "TBL_CONTRACT_FILES_SEQ")
            }
    )
    @Column(name = "id", unique = true, nullable = false)
    private Long id;
    @Column(name = "FILE_CONTRACT_NUMBER")
    private String fileContractNumber;
    @Column(name = "FILE_NAME")
    private String fileName;
    @Column(name = "FILE_STATUS")
    private String fileStatus;
    @Column(name = "FILE_TYPE")
    private String fileType;
    @Column(name = "CONTRACT_ID")
    private Long contractId;
    @Column(name = "UPLOAD_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date uploadDate;
    @Column(name = "START_DATE")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date startDate;
    @Column(name = "END_DATE")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date endDate;
    @Column(name = "CONTENT")
    private String content;
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "PATH_FILE")
    private String pathFile;

    public ContractFilesEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFileContractNumber() {
        return fileContractNumber;
    }

    public void setFileContractNumber(String fileContractNumber) {
        this.fileContractNumber = fileContractNumber;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileStatus() {
        return fileStatus;
    }

    public void setFileStatus(String fileStatus) {
        this.fileStatus = fileStatus;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public Long getContractId() {
        return contractId;
    }

    public void setContractId(Long contractId) {
        this.contractId = contractId;
    }

    public Date getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(Date uploadDate) {
        this.uploadDate = uploadDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPathFile() {
        return pathFile;
    }

    public void setPathFile(String pathFile) {
        this.pathFile = pathFile;
    }

    public static ContractFilesEntity setEntity(ContractDTO contractDTO, ContractFilesDTO contractFilesDTO, ContractFilesEntity contractFilesEntity) {
        contractFilesEntity.setContractId(contractDTO.getContractId());
        if (contractFilesDTO.getFileContractNumber() != null) {
            contractFilesEntity.setFileContractNumber(contractFilesDTO.getFileContractNumber());
        }
        if (contractFilesDTO.getStartDate() != null) {
            contractFilesEntity.setStartDate(contractFilesDTO.getStartDate());
        }
        if (contractFilesDTO.getEndDate() != null) {
            contractFilesEntity.setEndDate(contractFilesDTO.getEndDate());
        }
        if (contractFilesDTO.getDescription() != null) {
            contractFilesEntity.setDescription(contractFilesDTO.getDescription());
        }
        contractFilesEntity.setContent(contractFilesDTO.getContent());

        contractFilesEntity.setFileName(contractFilesDTO.getFileName());
        contractFilesEntity.setPathFile(contractFilesDTO.getPathFile());
        switch (contractDTO.getContractStatus()) {
            case ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_WAITING_OK_CONTRACT:
            case ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_NOT_OK_CONTRACT:
                contractFilesEntity.setFileType(ParamUtils.FILE_TYPE.FILE_TYPE_HS);
                break;
            case ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_WAITING_OK_CONTRACT_FILES:
            case ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_CANCLE_REGIS_OK_CONTRACT_FILES:
                contractFilesEntity.setFileType(ParamUtils.FILE_TYPE.FILE_TYPE_TOTRINH_HOSO);
                break;
            case ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_REGIS_OK_CONTRACT_FILES:
                contractFilesEntity.setFileType(ParamUtils.FILE_TYPE.FILE_TYPE_TOTRINH_HOPDONG);
                break;
            case ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_CREATE_REGIS_OK_CONTRACT_FILES:
                contractFilesEntity.setFileType(ParamUtils.FILE_TYPE.FILE_TYPE_PHULUC);
                break;
            case ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_OK_CONTRACT_FILES_FOR_PARTNER:
            case ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_OK_CONTRACT_FILES_REGISTED_PARTNER:
                contractFilesEntity.setFileType(ParamUtils.FILE_TYPE.FILE_TYPE_HD);
                break;
            case ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_TTHĐ:
                contractFilesEntity.setFileType(ParamUtils.FILE_TYPE.FILE_TYPE_TLHĐ);
                break;
        }
//        if (contractDTO.getContractStatus().toString().equals(ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_CREATE_CONTRACT)) {
//            contractFilesEntity.setFileType(ParamUtils.FILE_TYPE.FILE_TYPE_HS);
//        } else if (contractDTO.getContractStatus().toString().equals(ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_WAITING_OK_CONTRACT_FILES)) {
//            contractFilesEntity.setFileType(ParamUtils.FILE_TYPE.FILE_TYPE_TOTRINH_HOSO);
//        } else if (contractDTO.getContractStatus().toString().equals(ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_REGIS_OK_CONTRACT_FILES)) {
//            contractFilesEntity.setFileType(ParamUtils.FILE_TYPE.FILE_TYPE_TOTRINH_HOPDONG);
//        } else if (contractDTO.getContractStatus().toString().equals(ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_CREATE_REGIS_OK_CONTRACT_FILES)) {
//            contractFilesEntity.setFileType(ParamUtils.FILE_TYPE.FILE_TYPE_PHULUC);
//        } else if (contractDTO.getContractStatus().toString().equals(ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_OK_CONTRACT_FILES)) {
//            contractFilesEntity.setFileType(ParamUtils.FILE_TYPE.FILE_TYPE_HD);
//        } else if (contractDTO.getContractStatus().toString().equals(ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_OK_CONTRACT_FILES_REGISTED_PARTNER)) {
//            contractFilesEntity.setFileType(ParamUtils.FILE_TYPE.FILE_TYPE_HD);
//        } else if (contractDTO.getContractStatus().toString().equals(ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_TTHĐ)) {
//            contractFilesEntity.setFileType(ParamUtils.FILE_TYPE.FILE_TYPE_TLHĐ);
//        }if (contractDTO.getContractStatus().toString().equals(ParamUtils.CONTRACT_TYPE.CONTRACT_TYPE_CREATE_CONTRACT)) {
//            contractFilesEntity.setFileType(ParamUtils.FILE_TYPE.FILE_TYPE_HS);
//        }
        contractFilesEntity.setUploadDate(new Date());
        return contractFilesEntity;
    }

    public static ContractFilesEntity setEntity(ContractFilesDTO contractFilesDTO, ContractFilesEntity contractFilesEntity) {
        contractFilesEntity = new ContractFilesEntity();
        contractFilesEntity.setFileName(contractFilesDTO.getFileName());
        contractFilesEntity.setDescription(contractFilesDTO.getDescription());
        contractFilesEntity.setContractId(contractFilesDTO.getContractId());
        contractFilesEntity.setPathFile(contractFilesDTO.getPathFile());
        contractFilesEntity.setFileType(contractFilesDTO.getFileType());
        contractFilesEntity.setUploadDate(new Date());
        return contractFilesEntity;
    }

    public static ContractFilesEntity setEntityContactMenu(ContractFilesDTO contractFilesDTO, ContractFilesEntity contractFilesEntity, String flat) {
        contractFilesEntity = new ContractFilesEntity();
        contractFilesEntity.setFileName(contractFilesDTO.getFileName());
        contractFilesEntity.setDescription(contractFilesDTO.getDescription());
        contractFilesEntity.setContractId(contractFilesDTO.getContractId());
        contractFilesEntity.setPathFile(contractFilesDTO.getPathFile());
        switch (flat) {
            case ParamUtils.CONTRACT_PARTNER.CONTRACTFILESNAME:
                contractFilesEntity.setFileType(ParamUtils.CONTRACT_PARTNER.CONTRACTFILESNAME);
                break;
            case ParamUtils.CONTRACT_PARTNER.CONTRACTFILESNAMEMENU:
                contractFilesEntity.setFileType(ParamUtils.CONTRACT_PARTNER.CONTRACTFILESNAMEMENU);
                break;
            case ParamUtils.FILE_TYPE.FILE_TYPE_TLHĐ:
                contractFilesEntity.setFileType(ParamUtils.FILE_TYPE.FILE_TYPE_TLHĐ);
                break;
                case ParamUtils.FILE_TYPE.FILE_TYPE_PHULUC_TLHĐ:
                contractFilesEntity.setFileType(ParamUtils.FILE_TYPE.FILE_TYPE_PHULUC_TLHĐ);
                break;
            default:
                break;
        }
        contractFilesEntity.setUploadDate(new Date());
        if (contractFilesDTO.getFileContractNumber() != null) {
            contractFilesEntity.setFileContractNumber(contractFilesDTO.getFileContractNumber());
        }
            contractFilesEntity.setStartDate(contractFilesDTO.getStartDate());
            contractFilesEntity.setEndDate(contractFilesDTO.getEndDate());
            contractFilesEntity.setContent( contractFilesDTO.getContent());
            contractFilesEntity.setDescription( contractFilesDTO.getDescription());
        return contractFilesEntity;
    }
}
