/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.entity;

import com.viettel.csp.DTO.SolutionDTO;
import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.util.UserTokenUtils;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

/**
 *
 * @author GEM
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "TBL_SOLUTION")
public class SolutionEntity extends BaseCustomEntity implements Serializable {

    @Id
    @GeneratedValue(generator = "sequence")
    @GenericGenerator(name = "sequence", strategy = "sequence",
            parameters = {
                @Parameter(name = "sequence", value = "TBL_SOLUTION_SEQ")
            }
    )
    @Column(name = "ID", unique = true, nullable = false)
    private Long id;
    @Column(name = "REFLECT_ID")
    private Long reflectId;
    @Column(name = "CREATE_BY")
    private Long createBy;
    @Column(name = "CREATE_AT")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date createAt;
    @Column(name = "UPDATE_BY")
    private Long updateBy;
    @Column(name = "UPDATE_AT")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date updateAt;
    @Column(name = "CONTENT")
    private String content;
    @Column(name = "REQUEST_DATE")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date requestDate;
    @Column(name = "REPONSE_DATE")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date reponseDate;
    @Column(name = "SOLUTION_STATUS")
    private String solutionStatus;
    @Column(name = "eval_Reason_Code")
    private String evalReasonCode;
    @Column(name = "eval_Description")
    private String evalDescription;

    public SolutionEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getReflectId() {
        return reflectId;
    }

    public void setReflectId(Long reflectId) {
        this.reflectId = reflectId;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public Date getReponseDate() {
        return reponseDate;
    }

    public void setReponseDate(Date reponseDate) {
        this.reponseDate = reponseDate;
    }
    
    public String getSolutionStatus() {
        return solutionStatus;
    }

    public void setSolutionStatus(String solutionStatus) {
        this.solutionStatus = solutionStatus;
    }

    public String getEvalReasonCode() {
        return evalReasonCode;
    }

    public void setEvalReasonCode(String evalReasonCode) {
        this.evalReasonCode = evalReasonCode;
    }

    public String getEvalDescription() {
        return evalDescription;
    }

    public void setEvalDescription(String evalDescription) {
        this.evalDescription = evalDescription;
    }

    public static SolutionEntity setEntity(SolutionDTO solutionDTO, SolutionEntity solutionEntity, String action) {
//        if (action.equalsIgnoreCase(ParamUtils.SOLUTION_STATUS.SEND_PARTNER)) {
//        } else 
        if (action.equalsIgnoreCase(ParamUtils.SOLUTION_STATUS.REPONSE)) {
            solutionEntity.setReflectId(solutionDTO.getReflectId());
            solutionEntity.setCreateBy(UserTokenUtils.getUserId());
            solutionEntity.setCreateAt(new Date());
            solutionEntity.setRequestDate(new Date());
            solutionEntity.setContent(solutionDTO.getContent());
            solutionEntity.setReponseDate(new Date());
        } else if (action.equalsIgnoreCase(ParamUtils.SOLUTION_STATUS.NOK)) {
            solutionEntity.setEvalDescription(solutionDTO.getEvalDescription());
            solutionEntity.setEvalReasonCode(solutionDTO.getEvalReasonCode());
        } else if (action.equalsIgnoreCase(ParamUtils.SOLUTION_STATUS.MORE_INFO)) {
            solutionEntity.setEvalDescription(solutionDTO.getEvalDescription());
        }
        solutionEntity.setSolutionStatus(action);
        solutionEntity.setUpdateBy(UserTokenUtils.getUserId());
        solutionEntity.setUpdateAt(new Date());
        return solutionEntity;
    }
}
