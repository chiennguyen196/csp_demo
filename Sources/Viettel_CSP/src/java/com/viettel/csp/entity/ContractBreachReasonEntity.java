/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

/**
 *
 * @author GEM
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "TBL_CONTRACT_BREACH_REASON")
public class ContractBreachReasonEntity extends BaseCustomEntity implements Serializable {

    @Id
    @GeneratedValue(generator = "sequence")
    @GenericGenerator(name = "sequence", strategy = "sequence",
            parameters = {
                @Parameter(name = "sequence", value = "TBL_CT_BREACH_REASON_SEQ")
            }
    )
    @Column(name = "id", unique = true, nullable = false)
    private Long id;
    @Column(name = "REASON_CODE")
    private String reasonCode;
    @Column(name = "CONTRACT_BREACH_ID")
    private Long contractBreachId;
    @Column(name = "OTHER")
    private String other;

    public ContractBreachReasonEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    public Long getContractBreachId() {
        return contractBreachId;
    }

    public void setContractBreachId(Long contractBreachId) {
        this.contractBreachId = contractBreachId;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

}
