/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

/**
 *
 * @author GEM
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "TBL_PARTNER_LOCK_FILES")
public class PartnerLockFilesEntity extends BaseCustomEntity implements Serializable {

    @Id
    @GeneratedValue(generator = "sequence")
    @GenericGenerator(name = "sequence", strategy = "sequence",
            parameters = {
                @Parameter(name = "sequence", value = "TBL_PARTNER_LOCK_FILES_SEQ")
            }
    )
    @Column(name = "id", unique = true, nullable = false)
    private long id;
    @Column(name = "FILE_NAME")
    private String fileName;
    @Column(name = "PARTNER_LOCK_ID")
    private long partnerLockId;
    @Column(name = "UPLOAD_DATE")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date uploadDate;
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "PATH_FILE")
    private String pathFile;

    public PartnerLockFilesEntity() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public long getPartnerLockId() {
        return partnerLockId;
    }

    public void setPartnerLockId(long partnerLockId) {
        this.partnerLockId = partnerLockId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPathFile() {
        return pathFile;
    }

    public void setPathFile(String pathFile) {
        this.pathFile = pathFile;
    }

    public Date getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(Date uploadDate) {
        this.uploadDate = uploadDate;
    }
}
