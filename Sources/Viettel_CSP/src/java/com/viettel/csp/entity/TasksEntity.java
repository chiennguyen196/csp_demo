/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.entity;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author GEM
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "TBL_TASKS")
public class TasksEntity extends BaseCustomEntity implements Serializable {

    @Id
    @GeneratedValue(generator = "sequence")
    @GenericGenerator(name = "sequence", strategy = "sequence",
            parameters = {
                    @Parameter(name = "sequence", value = "TBL_TASKS_SEQ")
            }
    )
    @Column(name = "ID", unique = true, nullable = false)
    private Long id;
    @Column(name = "CONTRACT_ID")
    private Long contactId;
    @Column(name = "ASSIGN_BY")
    private Long assignBy;
    @Column(name = "ASSIGN_AT")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date assignAt;
    @Column(name = "TASK_USER_ID")
    private Long taskUserId;
    @Column(name = "CONTENT")
    private String content;
    @Column(name = "CREATE_BY")
    private Long createBy;
    @Column(name = "CREATE_AT")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date createAt;
    @Column(name = "UPDATE_BY")
    private Long updateBy;
    @Column(name = "UPDATE_AT")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date updateAt;
    @Column(name = "REQUIRED_COMPLETE_DATE")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date requiredCompleteDate;
    @Column(name = "COMPLETE_DATE")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date completeDate;
    @Column(name = "STATUS")
    private String status;
    @Column(name = "BEFORE_TASK")
    private String beforeTask;
    @Column(name = "TASK_TYPE")
    private String taskType;

    public TasksEntity() {
    }

    public String getBeforeTask() {
        return beforeTask;
    }

    public void setBeforeTask(String beforeTask) {
        this.beforeTask = beforeTask;
    }

    public String getTaskType() {
        return taskType;
    }

    public void setTaskType(String taskType) {
        this.taskType = taskType;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getContactId() {
        return contactId;
    }

    public void setContactId(Long contactId) {
        this.contactId = contactId;
    }

    public Long getAssignBy() {
        return assignBy;
    }

    public void setAssignBy(Long assignBy) {
        this.assignBy = assignBy;
    }

    public Date getAssignAt() {
        return assignAt;
    }

    public void setAssignAt(Date assignAt) {
        this.assignAt = assignAt;
    }

    public Long getTaskUserId() {
        return taskUserId;
    }

    public void setTaskUserId(Long taskUserId) {
        this.taskUserId = taskUserId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    public Date getRequiredCompleteDate() {
        return requiredCompleteDate;
    }

    public void setRequiredCompleteDate(Date requiredCompleteDate) {
        this.requiredCompleteDate = requiredCompleteDate;
    }

    public Date getCompleteDate() {
        return completeDate;
    }

    public void setCompleteDate(Date completeDate) {
        this.completeDate = completeDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
