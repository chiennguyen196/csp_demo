/*
 * To change this license header; choose License Headers in Project Properties.
 * To change this template file; choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.entity;

import com.viettel.csp.util.StringUtils;
import com.viettel.csp.DTO.ContactDTO;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

/**
 *
 * @author tiennv02
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "Tbl_Contact")
public class ContactEntity extends BaseCustomEntity implements Serializable {

    public ContactEntity() {
        setColId("id");
    }

    private Long id;
    private String contactType;
    private Long partnerId;
    private String name;
    private String email;
    private String fixedPhoneNumber;
    private String mobilePhoneNumber;
    private String sex;
    private String position;
    private String department;
    private String otherInfo;
    private String contactStatus;
    private Date createAt;
    private Long createBy;
    private Date updateAt;
    private Long updateBy;

    @Id
    @GeneratedValue(generator = "sequence")
    @GenericGenerator(name = "sequence", strategy = "sequence",
            parameters = {
                @Parameter(name = "sequence", value = "TBL_CONTACT_SEQ")
            }
    )
    @Column(name = "id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "contact_type")
    public String getContactType() {
        return contactType;
    }

    public void setContactType(String contactType) {
        this.contactType = contactType;
    }

    @Column(name = "partner_id")
    public Long getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(Long partnerId) {
        this.partnerId = partnerId;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column(name = "sex")
    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    @Column(name = "position")
    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Column(name = "department")
    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    @Column(name = "other_info")
    public String getOtherInfo() {
        return otherInfo;
    }

    public void setOtherInfo(String otherInfo) {
        this.otherInfo = otherInfo;
    }

    @Column(name = "create_at")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    @Column(name = "create_by")
    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    @Column(name = "update_at")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    @Column(name = "update_by")
    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    @Column(name = "FIXED_PHONE_NUMBER")
    public String getFixedPhoneNumber() {
        return fixedPhoneNumber;
    }

    public void setFixedPhoneNumber(String fixedPhoneNumber) {
        this.fixedPhoneNumber = fixedPhoneNumber;
    }

    @Column(name = "MOBILE_PHONE_NUMBER")
    public String getMobilePhoneNumber() {
        return mobilePhoneNumber;
    }

    public void setMobilePhoneNumber(String mobilePhoneNumber) {
        this.mobilePhoneNumber = mobilePhoneNumber;
    }

    @Column(name = "contact_status")
    public String getContactStatus() {
        return contactStatus;
    }

    public void setContactStatus(String contactStatus) {
        this.contactStatus = contactStatus;
    }

    public static ContactEntity setEntity(ContactDTO contactDTO, ContactEntity contactEntity) {
        contactEntity.setName(contactDTO.getName());
        contactEntity.setEmail(contactDTO.getEmail());
        contactEntity.setSex(contactDTO.getSex());
        contactEntity.setPartnerId(contactDTO.getPartnerId());
        contactEntity.setFixedPhoneNumber(contactDTO.getFixedPhoneNumber());
        contactEntity.setMobilePhoneNumber(contactDTO.getMobilePhoneNumber());
        contactEntity.setDepartment(contactDTO.getDepartment());
        contactEntity.setPosition(contactDTO.getPosition());
        contactEntity.setOtherInfo(contactDTO.getOtherInfo());
        contactEntity.setContactType(contactDTO.getContactType());
        contactEntity.setContactStatus(contactDTO.getContactStatus());
        if (contactEntity.getId() == null) {
            contactEntity.setCreateAt(new Date());
            contactEntity.setCreateBy(0L);
        }
        contactEntity.setUpdateAt(new Date());
        contactEntity.setUpdateBy(0L);
        return contactEntity;
    }
}
