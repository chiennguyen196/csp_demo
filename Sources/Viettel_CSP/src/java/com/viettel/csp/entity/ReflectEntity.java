/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.entity;

import com.viettel.csp.DTO.ReflectDTO;
import com.viettel.csp.util.UserTokenUtils;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

/**
 *
 * @author GEM
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "TBL_REFLECT")
public class ReflectEntity extends BaseCustomEntity implements Serializable {

    @Id
    @GeneratedValue(generator = "sequence")
    @GenericGenerator(name = "sequence", strategy = "sequence",
            parameters = {
                @Parameter(name = "sequence", value = "TBL_REFLECT_SEQ")
            }
    )
    @Column(name = "ID", unique = true, nullable = false)
    private Long id;
    @Column(name = "CODE")
    private String code;
    @Column(name = "REFLECT_STATUS")
    private String reflectStatus;
    @Column(name = "REFLECT_TYPE")
    private String reflectType;
    @Column(name = "CUSTOMER_NAME")
    private String customerName;
    @Column(name = "CUSTOMER_PHONE")
    private String customerPhone;
    @Column(name = "SERVICE_CODE")
    private String serviceCode;
    @Column(name = "HEAD_OF_SERVICE")
    private String headOfService;
    @Column(name = "CONTENT")
    private String content;
    @Column(name = "PARTNER_ID")
    private Long partnerId;
    @Column(name = "CREATE_BY")
    private Long createBy;
    @Column(name = "CREATE_AT")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date createAt;
    @Column(name = "UPDATE_BY")
    private Long updateBy;
    @Column(name = "UPDATE_AT")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date updateAt;
    @Column(name = "CLOSE_DATE")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date closeDate;
    @Column(name = "CUSTOMER_CARE_STAFF_NAME")
    private String customerCareStaffName;
    @Column(name = "CUSTOMER_CARE_STAFF_PHONE")
    private String customerCareStaffPhone;
    @Column(name = "PRIORITY_LEVEL")
    private String priorityLevel;
    @Column(name = "REQUIRED_REPONSE_DATE")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date requiredReponseDate;
    @Column(name = "REASON_CODE")
    private String reasonCode;
    @Column(name = "REASON_DESCRIPTION")
    private String reasonDescription;

    public ReflectEntity() {
        setColId("ID");
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getReflectStatus() {
        return reflectStatus;
    }

    public void setReflectStatus(String reflectStatus) {
        this.reflectStatus = reflectStatus;
    }

    public String getReflectType() {
        return reflectType;
    }

    public void setReflectType(String reflectType) {
        this.reflectType = reflectType;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getHeadOfService() {
        return headOfService;
    }

    public void setHeadOfService(String headOfService) {
        this.headOfService = headOfService;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(Long partnerId) {
        this.partnerId = partnerId;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    public Date getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(Date closeDate) {
        this.closeDate = closeDate;
    }

    public String getCustomerCareStaffName() {
        return customerCareStaffName;
    }

    public void setCustomerCareStaffName(String customerCareStaffName) {
        this.customerCareStaffName = customerCareStaffName;
    }

    public String getCustomerCareStaffPhone() {
        return customerCareStaffPhone;
    }

    public void setCustomerCareStaffPhone(String customerCareStaffPhone) {
        this.customerCareStaffPhone = customerCareStaffPhone;
    }

    public String getPriorityLevel() {
        return priorityLevel;
    }

    public void setPriorityLevel(String priorityLevel) {
        this.priorityLevel = priorityLevel;
    }

    public Date getRequiredReponseDate() {
        return requiredReponseDate;
    }

    public void setRequiredReponseDate(Date requiredReponseDate) {
        this.requiredReponseDate = requiredReponseDate;
    }

    public String getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    public String getReasonDescription() {
        return reasonDescription;
    }

    public void setReasonDescription(String reasonDescription) {
        this.reasonDescription = reasonDescription;
    }

    public static ReflectEntity setEntityInsert(ReflectDTO reflectDTO, ReflectEntity reflectEntity) {
        reflectEntity.setCode(reflectDTO.getCode());
        reflectEntity.setCustomerPhone(reflectDTO.getCustomerPhone());
        reflectEntity.setContent(reflectDTO.getContent());
        reflectEntity.setPriorityLevel(reflectDTO.getPriorityLevel());
        reflectEntity.setHeadOfService(reflectDTO.getHeadOfService());
        reflectEntity.setServiceCode(reflectDTO.getServiceCode());
        reflectEntity.setReflectType(reflectDTO.getReflectTypeCode());
        reflectEntity.setCustomerName(reflectDTO.getCustomerName());
        reflectEntity.setReflectStatus(reflectDTO.getReflectStatusCode());
        reflectEntity.setRequiredReponseDate(reflectDTO.getRequiredReponseDate());
        reflectEntity.setCustomerCareStaffName(reflectDTO.getCustomerCareStaffName());
        reflectEntity.setCustomerCareStaffPhone(reflectDTO.getCustomerCareStaffPhone());
        reflectEntity.setPartnerId(reflectDTO.getPartnerId());
        reflectEntity.setCreateAt(new Date());
        reflectEntity.setCreateBy(UserTokenUtils.getUserId());
        reflectEntity.setUpdateAt(new Date());
        reflectEntity.setUpdateBy(UserTokenUtils.getUserId());
        return reflectEntity;
    }

    public static ReflectEntity setEntityUpdate(ReflectDTO reflectDTO, ReflectEntity reflectEntity) {
        reflectEntity.setId(reflectDTO.getId());
        reflectEntity.setCode(reflectDTO.getCode());
        reflectEntity.setCustomerPhone(reflectDTO.getCustomerPhone());
        reflectEntity.setContent(reflectDTO.getContent());
        reflectEntity.setHeadOfService(reflectDTO.getHeadOfService());
        reflectEntity.setPriorityLevel(reflectDTO.getPriorityLevel());
        reflectEntity.setServiceCode(reflectDTO.getServiceCode());
        reflectEntity.setReflectType(reflectDTO.getReflectTypeCode());
        reflectEntity.setCustomerName(reflectDTO.getCustomerName());
        reflectEntity.setReflectStatus(reflectDTO.getReflectStatusCode());
        reflectEntity.setRequiredReponseDate(reflectDTO.getRequiredReponseDate());
        reflectEntity.setCustomerCareStaffName(reflectDTO.getCustomerCareStaffName());
        reflectEntity.setCustomerCareStaffPhone(reflectDTO.getCustomerCareStaffPhone());
        reflectEntity.setPartnerId(reflectDTO.getPartnerId());
        reflectEntity.setUpdateAt(new Date());
        reflectEntity.setUpdateBy(UserTokenUtils.getUserId());
        return reflectEntity;
    }
}
