/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.entity;

import com.viettel.csp.DTO.PartnerBankDTO;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

/**
 *
 * @author GEM
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "TBL_PARTNER_BANK")
public class PartnerBankEntity extends BaseCustomEntity implements Serializable {

    @Id
    @GeneratedValue(generator = "sequence")
    @GenericGenerator(name = "sequence", strategy = "sequence",
            parameters = {
                @Parameter(name = "sequence", value = "TBL_PARTNER_BANK_SEQ")
            }
    )
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @Column(name = "PARTNER_ID")
    private Long partnerId;
    @Column(name = "BANK_CODE")
    private String bankCode;
    @Column(name = "BANK_BRANCH")
    private String bankBranch;
    @Column(name = "ACCOUNT_NUMBER")
    private String accountNumber;

    public PartnerBankEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(Long partnerId) {
        this.partnerId = partnerId;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getBankBranch() {
        return bankBranch;
    }

    public void setBankBranch(String bankBranch) {
        this.bankBranch = bankBranch;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public static PartnerBankEntity setEntity(PartnerBankDTO partnerBankDTO, PartnerBankEntity partnerBankEntity) {
        partnerBankEntity.setId(partnerBankDTO.getId());
        partnerBankEntity.setPartnerId(partnerBankDTO.getPartnerId());
        partnerBankEntity.setBankCode(partnerBankDTO.getBankCode());
        partnerBankEntity.setBankBranch(partnerBankDTO.getBankBranch());
        partnerBankEntity.setAccountNumber(partnerBankDTO.getAccountNumber());
        return partnerBankEntity;
    }
}
