/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author GEM
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "TBL_LOCATION")
public class LocationEntity extends BaseCustomEntity implements Serializable  {

    @Id
    @Column(name = "PROVINCE_CODE", unique = true, nullable = false)
    private String provinceCode;
    @Column(name = "PROVINCE_NAME")
    private String provinceName;
    @Column(name = "DISTRICT_CODE")
    private String districtCode;
    @Column(name = "DISTRICT_NAME")
    private String districtName;
    @Column(name = "VILLAGE_CODE")
    private String villageCode;
    @Column(name = "VILLAGE_NAME")
    private String villageName;

    public LocationEntity() {
        setColId(provinceCode);
        
    }

    public String getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getDistrictCode() {
        return districtCode;
    }

    public void setDistrictCode(String districtCode) {
        this.districtCode = districtCode;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public String getVillageCode() {
        return villageCode;
    }

    public void setVillageCode(String villageCode) {
        this.villageCode = villageCode;
    }

    public String getVillageName() {
        return villageName;
    }

    public void setVillageName(String villageName) {
        this.villageName = villageName;
    }
}
