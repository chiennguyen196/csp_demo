/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.DTO;

import com.viettel.csp.entity.ServiceEntity;
import com.viettel.csp.entity.SolutionEntity;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Temporal;

/**
 *
 * @author admin
 */
public class SolutionDTO extends BaseCustomDTO<SolutionEntity> {

    private Long id;
    private Long reflectId;
    private Long createBy;
    private Date createAt;
    private Long updateBy;
    private Date updateAt;
    private String content;
    private Date requestDate;
    private Date reponseDate;
    private String evalReasonCode;
    private String solutionStatus;
    private String evalDescription;
    private String solutionStatusName;
    private String evalReasonName;
    private List<SolutionFilesDTO> lstSolutionFilesDTO;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getReflectId() {
        return reflectId;
    }

    public void setReflectId(Long reflectId) {
        this.reflectId = reflectId;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public Date getReponseDate() {
        return reponseDate;
    }

    public void setReponseDate(Date reponseDate) {
        this.reponseDate = reponseDate;
    }

    public String getSolutionStatus() {
        return solutionStatus;
    }

    public void setSolutionStatus(String solutionStatus) {
        this.solutionStatus = solutionStatus;
    }

    public String getEvalReasonCode() {
        return evalReasonCode;
    }

    public void setEvalReasonCode(String evalReasonCode) {
        this.evalReasonCode = evalReasonCode;
    }

    public String getEvalDescription() {
        return evalDescription;
    }

    public void setEvalDescription(String evalDescription) {
        this.evalDescription = evalDescription;
    }

    public String getSolutionStatusName() {
        return solutionStatusName;
    }

    public void setSolutionStatusName(String solutionStatusName) {
        this.solutionStatusName = solutionStatusName;
    }

    public String getEvalReasonName() {
        return evalReasonName;
    }

    public void setEvalReasonName(String evalReasonName) {
        this.evalReasonName = evalReasonName;
    }

    public List<SolutionFilesDTO> getLstSolutionFilesDTO() {
        return lstSolutionFilesDTO;
    }

    public void setLstSolutionFilesDTO(List<SolutionFilesDTO> lstSolutionFilesDTO) {
        this.lstSolutionFilesDTO = lstSolutionFilesDTO;
    }

    @Override
    public SolutionEntity toBO() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object catchId() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String catchName() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
