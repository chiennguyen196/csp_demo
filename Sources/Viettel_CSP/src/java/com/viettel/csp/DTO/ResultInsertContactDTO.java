package com.viettel.csp.DTO;

/**
 * Created by admin on 6/16/2017.
 */
public class ResultInsertContactDTO {
    private long idContact;
    private String message;

    public long getIdContact() {
        return idContact;
    }

    public void setIdContact(long idContact) {
        this.idContact = idContact;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
