package com.viettel.csp.DTO;

import com.viettel.csp.entity.BaseCustomEntity;

/**
 * Created by GEM on 6/16/2017.
 */
public class ContractBreachReasonDTO extends BaseCustomDTO {
    private Long id;
    private String reasonCode;
    private Long contractBreachId;
    private String other;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    public Long getContractBreachId() {
        return contractBreachId;
    }

    public void setContractBreachId(Long contractBreachId) {
        this.contractBreachId = contractBreachId;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    @Override
    public BaseCustomEntity toBO() {
        return null;
    }

    @Override
    public Object catchId() {
        return null;
    }

    @Override
    public String catchName() {
        return null;
    }
}
