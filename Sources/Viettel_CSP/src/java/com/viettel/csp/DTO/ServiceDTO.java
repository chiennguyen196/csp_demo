/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.DTO;

import com.viettel.csp.entity.ServiceEntity;

/**
 *
 * @author admin
 */
public class ServiceDTO extends BaseCustomDTO<ServiceEntity> {

    private String code;
    private String name;
    private String parentCode;
    private String pathFile;
    private String codeReflectType;
    private String nameReflectType;
    private String parentName;

    public ServiceDTO() {
    }

    public ServiceDTO(String code, String name, String parentCode) {
        this.code = code;
        this.name = name;
        this.parentCode = parentCode;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }


    public String getPathFile() {
        return pathFile;
    }

    public void setPathFile(String pathFile) {
        this.pathFile = pathFile;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParentCode() {
        return parentCode;
    }

    public void setParentCode(String parentCode) {
        this.parentCode = parentCode;
    }

    public String getCodeReflectType() {
        return codeReflectType;
    }

    public void setCodeReflectType(String codeReflectType) {
        this.codeReflectType = codeReflectType;
    }

    public String getNameReflectType() {
        return nameReflectType;
    }

    public void setNameReflectType(String nameReflectType) {
        this.nameReflectType = nameReflectType;
    }

    @Override
    public ServiceEntity toBO() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object catchId() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String catchName() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
