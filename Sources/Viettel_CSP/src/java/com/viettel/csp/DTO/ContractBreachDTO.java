package com.viettel.csp.DTO;

import com.viettel.csp.entity.BaseCustomEntity;

import java.util.Date;

/**
 * Created by GEM on 6/16/2017.
 */
public class ContractBreachDTO extends BaseCustomDTO {
    private Long id;
    private Long contractId;
    private Long partnerId;
    private Date startDate;
    private Date endDate;
    private Long isPublic;
    private Date createAt;
    private Long createBy;
    private Date updateAt;
    private Long updateBY;
    private String headOfService;
    private String reasonDisciplineCode;

    public String getReasonDisciplineCode() {
        return reasonDisciplineCode;
    }

    public void setReasonDisciplineCode(String reasonDisciplineCode) {
        this.reasonDisciplineCode = reasonDisciplineCode;
    }

    public String getHeadOfService() {
        return headOfService;
    }

    public void setHeadOfService(String headOfService) {
        this.headOfService = headOfService;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getContractId() {
        return contractId;
    }

    public void setContractId(Long contractId) {
        this.contractId = contractId;
    }

    public Long getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(Long partnerId) {
        this.partnerId = partnerId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Long getIsPublic() {
        return isPublic;
    }

    public void setIsPublic(Long isPublic) {
        this.isPublic = isPublic;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    public Long getUpdateBY() {
        return updateBY;
    }

    public void setUpdateBY(Long updateBY) {
        this.updateBY = updateBY;
    }

    @Override
    public BaseCustomEntity toBO() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Object catchId() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String catchName() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
