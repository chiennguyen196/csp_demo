/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.DTO;

import com.viettel.csp.entity.*;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author GEM
 */
public class PartnerLockFilesDTO extends BaseCustomDTO<PartnerLockFilesEntity> {

    private Long id;
    private String fileName;
    private Long partnerLockId;
    private Date uploadDate;
    private String description;
    private String pathFile;

    public PartnerLockFilesDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Long getPartnerLockId() {
        return partnerLockId;
    }

    public void setPartnerLockId(Long partnerLockId) {
        this.partnerLockId = partnerLockId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPathFile() {
        return pathFile;
    }

    public void setPathFile(String pathFile) {
        this.pathFile = pathFile;
    }

    public Date getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(Date uploadDate) {
        this.uploadDate = uploadDate;
    }

    @Override
    public PartnerLockFilesEntity toBO() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object catchId() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String catchName() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
