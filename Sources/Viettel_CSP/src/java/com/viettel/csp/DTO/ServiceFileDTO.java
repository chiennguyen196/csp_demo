package com.viettel.csp.DTO;

import com.viettel.csp.entity.ReflectFilesEntity;
import com.viettel.csp.entity.ServiceEntity;

import java.util.Date;

/**
 * Created by LINHDT on 06/14/2017.
 */
public class ServiceFileDTO extends BaseCustomDTO<ServiceEntity> {

    private Long id;
    private String fileName;
    private String description;
    private String serviceCode;
    private Date uploadDate;
    private String pathFile;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public Date getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(Date uploadDate) {
        this.uploadDate = uploadDate;
    }

    public String getPathFile() {
        return pathFile;
    }

    public void setPathFile(String pathFile) {
        this.pathFile = pathFile;
    }

    @Override
    public ServiceEntity toBO() {
        return null;
    }

    @Override
    public Object catchId() {
        return null;
    }

    @Override
    public String catchName() {
        return null;
    }
}
