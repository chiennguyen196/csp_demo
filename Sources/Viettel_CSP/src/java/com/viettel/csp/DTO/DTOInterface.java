/*
 * Copyright (C) 2011 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.viettel.csp.DTO;

import com.viettel.csp.DTO.*;
import com.viettel.csp.entity.BaseCustomEntity;
import com.viettel.csp.util.KeyValueBean;

/**
 *
 * @author kdvt_binhnt22@viettel.com.vn
 * @version 1.0
 * @since since_text
 */
public interface DTOInterface<TBO extends BaseCustomEntity> extends Comparable<DTOInterface> {

    TBO toBO();

    Object catchId();

    String catchName();

    KeyValueBean toBean();

    long getChangedTime();

    void setChangedTime(long changedTime);
}
