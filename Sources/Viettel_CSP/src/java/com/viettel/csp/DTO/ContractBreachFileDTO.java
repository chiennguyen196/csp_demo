package com.viettel.csp.DTO;

import com.viettel.csp.entity.BaseCustomEntity;

import java.util.Date;

/**
 * Created by GEM on 6/16/2017.
 */
public class ContractBreachFileDTO extends BaseCustomDTO {
    private Long id;
    private String fileName;
    private String description;
    private Long partnerBreachId;
    private Date uploadDate;
    private String pathFile;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getPartnerBreachId() {
        return partnerBreachId;
    }

    public void setPartnerBreachId(Long partnerBreachId) {
        this.partnerBreachId = partnerBreachId;
    }

    public Date getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(Date uploadDate) {
        this.uploadDate = uploadDate;
    }

    public String getPathFile() {
        return pathFile;
    }

    public void setPathFile(String pathFile) {
        this.pathFile = pathFile;
    }

    @Override
    public BaseCustomEntity toBO() {
        return null;
    }

    @Override
    public Object catchId() {
        return null;
    }

    @Override
    public String catchName() {
        return null;
    }
}
