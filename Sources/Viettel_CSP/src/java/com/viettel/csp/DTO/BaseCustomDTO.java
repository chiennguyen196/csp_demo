/*
 * Copyright (C) 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.viettel.csp.DTO;

import com.viettel.csp.entity.BaseCustomEntity;
import com.viettel.csp.util.KeyValueBean;
import com.viettel.csp.util.ParamUtils;
import com.viettel.csp.util.ReflectUtils;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import org.apache.log4j.Logger;

/**
 * @author kdvt_binhnt22@viettel.com.vn
 * @version 1.0
 * @since May 2012
 */
public abstract class BaseCustomDTO<TBO extends BaseCustomEntity> implements DTOInterface<TBO> {

    private static Logger log = Logger.getLogger(BaseCustomDTO.class);
    protected String defaultSortField = "name";
    private String objectId;

    public static List<KeyValueBean> getListToKeyValueBean(List<? extends BaseCustomDTO> listForm) {
        List<KeyValueBean> lstBean = new ArrayList<>();
        if (listForm != null) {
            for (DTOInterface form : listForm) {
                lstBean.add(form.toBean());
            }
        }
        return lstBean;
    }

    public String getDefaultSortField() {
        return this.defaultSortField;
    }

    public void setDefaultSortField(String defaultSortField) {
        this.defaultSortField = defaultSortField;
    }

    public String getStatus() {
        return null;
    }

    @Override
    public KeyValueBean toBean() {
        KeyValueBean bean = new KeyValueBean(catchId(), catchName());
        return bean;
    }

    @Override
    public int compareTo(DTOInterface o) {
        return String.valueOf(catchName()).compareTo(String.valueOf(o.catchName()));
    }

    @Override
    public long getChangedTime() {
        return 0;
    }

    @Override
    public synchronized void setChangedTime(long changedTime) {
    }

    public String getDetailString() {
        return getDetailedStr(toBO());
    }

    public String getDetailedStr(TBO bo) {
        StringBuilder result = new StringBuilder();
        Method methods[] = bo.getClass().getDeclaredMethods();
        String columnName;
        Object value;
        for (int i = 0; i < methods.length; i++) {
            if (ReflectUtils.isGetter(methods[i]) && methods[i].getName().toLowerCase()
                .endsWith("id")) {
                value = "";
                try {
                    value = methods[i].invoke(bo);
                    value = value == null ? "" : value;
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }
                columnName = methods[i].getAnnotation(Column.class).name();
                result.append(columnName).append(":")
                    .append(value).append(ParamUtils.LOG_DETAIL_SEPERATOR);
            }
        }
        return result.toString();
    }

    public String getObjectId() {
        String id = catchId() == null ? null : catchId().toString();
        this.objectId = this.objectId == null ? id : this.objectId;
        return this.objectId;
    }

    public void setObjectId(Object objectId) {
        this.objectId = objectId == null ? "" : objectId.toString();
    }
}
