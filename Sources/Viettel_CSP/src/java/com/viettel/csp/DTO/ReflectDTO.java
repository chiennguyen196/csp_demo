/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.DTO;

import com.viettel.csp.entity.ReflectEntity;

import java.util.Date;
import java.util.List;

/**
 * @author GEM
 */
public class ReflectDTO extends BaseCustomDTO<ReflectEntity> {

    private Long id;
    private String code;
    private String reflectStatusCode;
    private String reflectTypeCode;
    private String customerName;
    private String customerPhone;
    private String serviceCode;
    private String headOfService; // đầu số
    private String content;
    private Long partnerId;
    private Long createBy;
    private Date createAt;
    private Long updateBy;
    private Date updateAt;
    private Date closeDate;
    private String customerCareStaffName;
    private String customerCareStaffPhone;
    private String serviceName;
    private Date requiredReponseDate;
    private String reflectType;
    private String reflectStatusName;
    private String reflectTypeName;
    private String priorityLevel;
    private String reasonCode;
    private String reasonName;
    private String reasonDescription;
    private List<ReflectFilesDTO> lstReflectFilesDTO;

    public List<ReflectFilesDTO> getLstReflectFilesDTO() {
        return lstReflectFilesDTO;
    }

    public void setLstReflectFilesDTO(List<ReflectFilesDTO> lstReflectFilesDTO) {
        this.lstReflectFilesDTO = lstReflectFilesDTO;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getHeadOfService() {
        return headOfService;
    }

    public void setHeadOfService(String headOfService) {
        this.headOfService = headOfService;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(Long partnerId) {
        this.partnerId = partnerId;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    public Date getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(Date closeDate) {
        this.closeDate = closeDate;
    }

    public String getCustomerCareStaffName() {
        return customerCareStaffName;
    }

    public void setCustomerCareStaffName(String customerCareStaffName) {
        this.customerCareStaffName = customerCareStaffName;
    }

    public String getCustomerCareStaffPhone() {
        return customerCareStaffPhone;
    }

    public void setCustomerCareStaffPhone(String customerCareStaffPhone) {
        this.customerCareStaffPhone = customerCareStaffPhone;
    }

    public String getReflectStatusCode() {
        return reflectStatusCode;
    }

    public void setReflectStatusCode(String reflectStatusCode) {
        this.reflectStatusCode = reflectStatusCode;
    }

    public String getReflectTypeCode() {
        return reflectTypeCode;
    }

    public void setReflectTypeCode(String reflectTypeCode) {
        this.reflectTypeCode = reflectTypeCode;
    }

    public String getReflectStatusName() {
        return reflectStatusName;
    }

    public void setReflectStatusName(String reflectStatusName) {
        this.reflectStatusName = reflectStatusName;
    }

    public String getReflectTypeName() {
        return reflectTypeName;
    }

    public void setReflectTypeName(String reflectTypeName) {
        this.reflectTypeName = reflectTypeName;
    }

    public String getReflectType() {
        return reflectType;
    }

    public void setReflectType(String reflectType) {
        this.reflectType = reflectType;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public Date getRequiredReponseDate() {
        return requiredReponseDate;
    }

    public void setRequiredReponseDate(Date requiredReponseDate) {
        this.requiredReponseDate = requiredReponseDate;
    }

    public String getPriorityLevel() {
        return priorityLevel;
    }

    public void setPriorityLevel(String priorityLevel) {
        this.priorityLevel = priorityLevel;
    }

    public String getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    public String getReasonDescription() {
        return reasonDescription;
    }

    public void setReasonDescription(String reasonDescription) {
        this.reasonDescription = reasonDescription;
    }

    public String getReasonName() {
        return reasonName;
    }

    public void setReasonName(String reasonName) {
        this.reasonName = reasonName;
    }

    @Override
    public ReflectEntity toBO() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object catchId() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String catchName() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
