/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.DTO;

import com.viettel.csp.entity.*;

/**
 *
 * @author GEM
 */
public class ContactTypeDTO extends BaseCustomDTO {

    private String code;
    private String name;
    private Long contactPointID;

    public ContactTypeDTO() {
    }

    public ContactTypeDTO(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getContactPointID() {
        return contactPointID;
    }

    public void setContactPointID(Long contactPointID) {
        this.contactPointID = contactPointID;
    }

    @Override
    public BaseCustomEntity toBO() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object catchId() {
        return code;
    }

    @Override
    public String catchName() {
        return name;
    }

}
