/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.DTO;

import com.viettel.csp.entity.DepartmentEntity;

/**
 *
 * @author admin
 */
public class DepartmentDTO extends BaseCustomDTO<DepartmentEntity> {

    private Long organizationId;
    private Long orgparentId;
    private String name;
    private String code;
    private String isActive;
    private String path;

    public DepartmentDTO(Long organizationId, Long orgparentId, String name, String code, String isActive, String path) {
        this.organizationId = organizationId;
        this.orgparentId = orgparentId;
        this.name = name;
        this.code = code;
        this.isActive = isActive;
        this.path = path;
    }

    public DepartmentDTO() {
    }

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public Long getOrgparentId() {
        return orgparentId;
    }

    public void setOrgparentId(Long orgparentId) {
        this.orgparentId = orgparentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String ISACTIVE) {
        this.isActive = isActive;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public DepartmentEntity toBO() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object catchId() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String catchName() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
