/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.DTO;

import com.viettel.csp.entity.ReflectFilesEntity;
import java.util.Date;

/**
 *
 * @author admin
 */
public class ReflectFilesDTO extends BaseCustomDTO<ReflectFilesEntity> {

    private Long id;
    private String fileName;
    private String description;
    private Long reflectId;
    private Date uploadDate;
    private String pathFile;

    public ReflectFilesDTO() {
    }

    public ReflectFilesDTO(Long id, String fileName, String description, Long reflectId, Date uploadDate, String pathFile) {
        this.id = id;
        this.fileName = fileName;
        this.description = description;
        this.reflectId = reflectId;
        this.uploadDate = uploadDate;
        this.pathFile = pathFile;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getReflectId() {
        return reflectId;
    }

    public void setReflectId(Long reflectId) {
        this.reflectId = reflectId;
    }

    public Date getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(Date uploadDate) {
        this.uploadDate = uploadDate;
    }

    public String getPathFile() {
        return pathFile;
    }

    public void setPathFile(String pathFile) {
        this.pathFile = pathFile;
    }

    @Override
    public ReflectFilesEntity toBO() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object catchId() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String catchName() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
