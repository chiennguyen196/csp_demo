/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.DTO;

import com.viettel.csp.entity.TaskFilesEntity;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author GEM
 */
public class TaskFilesDTO extends BaseCustomDTO<TaskFilesEntity>  {

    private long id;
    private String fileName;
    private String description;
    private long taskId;
    private Date uploadDate;
    private String pathFile;

    public TaskFilesDTO() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getTaskId() {
        return taskId;
    }

    public void setTaskId(long taskId) {
        this.taskId = taskId;
    }

    public Date getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(Date uploadDate) {
        this.uploadDate = uploadDate;
    }

    public String getPathFile() {
        return pathFile;
    }

    public void setPathFile(String pathFile) {
        this.pathFile = pathFile;
    }

    @Override
    public TaskFilesEntity toBO() {
        return null;
    }

    @Override
    public Object catchId() {
        return null;
    }

    @Override
    public String catchName() {
        return null;
    }
}
