/**
 * @(#)FmBlackImeiDTO.java , Copyright (C) 2013 Viettel IT. All rights reserved
 * VIETTEL PROPRIETARY/CONFIDENTIAL
 */
package com.viettel.csp.DTO;

import com.viettel.csp.DTO.BaseCustomDTO;
import com.viettel.csp.util.StringUtils;
import org.apache.log4j.Logger;

/**
 * @author tuydv1
 * @version 1.0
 */
public class ComponentDTO {
    //Fields
    private String standardAttributeId;
    private String attributeCode;
    private String attributeName;
    private String attributeType;
    private String status;
    private String attributeFormat;
    private String defaultValue;
    private String selectItemValue;
    private String attributeLength;
    private Boolean isRequired;
    private String isHidden;
    private String isDefault;
    private String initValue;
    private String paramOrder;
    private String valPattern;
    private String docType;
    private static long changedTime = 0;

    private static Logger logger = Logger.getLogger(ComponentDTO.class);

    //Constructor
    public ComponentDTO() {
    }

    public ComponentDTO(String standardAttributeId, String attributeCode, String attributeName, String attributeType,
                                String status, String attributeFormat, String defaultValue, String attributeLength, Boolean isRequired, String isHidden,
                                String isDefault, String initValue, String paramOrder, String valPattern, String docType) {
        this.standardAttributeId = standardAttributeId;
        this.attributeCode = attributeCode;
        this.attributeName = attributeName;
        this.attributeType = attributeType;
        this.status = status;
        this.attributeFormat = attributeFormat;
        this.defaultValue = defaultValue;
        this.attributeLength = attributeLength;
        this.isRequired = isRequired;
        this.isHidden = isHidden;
        this.isDefault = isDefault;
        this.initValue = initValue;
        this.paramOrder = paramOrder;
        this.valPattern = valPattern;
        this.docType = docType;
    }

    //Getters and setters
    public String getStandardAttributeId() {
        return standardAttributeId;
    }

    public void setStandardAttributeId(String standardAttributeId) {
        this.standardAttributeId = standardAttributeId;
    }

    public String getAttributeCode() {
        return attributeCode;
    }

    public void setAttributeCode(String attributeCode) {
        this.attributeCode = attributeCode;
    }

    public String getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }

    public String getAttributeType() {
        return attributeType;
    }

    public void setAttributeType(String attributeType) {
        this.attributeType = attributeType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAttributeFormat() {
        return attributeFormat;
    }

    public void setAttributeFormat(String attributeFormat) {
        this.attributeFormat = attributeFormat;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getAttributeLength() {
        return attributeLength;
    }

    public void setAttributeLength(String attributeLength) {
        this.attributeLength = attributeLength;
    }

    public Boolean getIsRequired() {
        return isRequired;
    }

    public void setIsRequired(Boolean isRequired) {
        this.isRequired = isRequired;
    }

    public String getIsHidden() {
        return isHidden;
    }

    public void setIsHidden(String isHidden) {
        this.isHidden = isHidden;
    }

    public String getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(String isDefault) {
        this.isDefault = isDefault;
    }

    public String getInitValue() {
        return initValue;
    }

    public void setInitValue(String initValue) {
        this.initValue = initValue;
    }

    public String getParamOrder() {
        return paramOrder;
    }

    public void setParamOrder(String paramOrder) {
        this.paramOrder = paramOrder;
    }
    public String getValPattern() {
        return valPattern;
    }

    public void setValPattern(String valPattern) {
        this.valPattern = valPattern;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public void setAttribute(String ATTR_REQUIRED, boolean b) {
    }

    public String getSelectItemValue() {
        return selectItemValue;
    }

    public void setSelectItemValue(String selectItemValue) {
        this.selectItemValue = selectItemValue;
    }
}
