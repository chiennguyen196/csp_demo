/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.DTO;

import com.viettel.csp.entity.ContractFilesEntity;
import org.zkoss.util.Converter;

import java.util.Date;
import java.util.List;

/**
 * @author admin
 */
public class ContractFilesDTO extends BaseCustomDTO<ContractFilesEntity> implements Converter<Object, Object> {

    private Long id;
    private String fileContractNumber;
    private String fileName;
    private String fileStatus;
    private String fileStatusName;
    private String fileType;
    private Long contractId;
    private Date uploadDate;
    private Date startDate;
    private Date filingDate;
    private Date endDate;
    private String content;
    private String description;
    private String pathFile;
    private List<ContractFilesDTO> listContractFilesDTO;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<ContractFilesDTO> getListContractFilesDTO() {
        return listContractFilesDTO;
    }

    public void setListContractFilesDTO(List<ContractFilesDTO> listContractFilesDTO) {
        this.listContractFilesDTO = listContractFilesDTO;
    }

    public String getFileContractNumber() {
        return fileContractNumber;
    }

    public void setFileContractNumber(String fileContractNumber) {
        this.fileContractNumber = fileContractNumber;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileStatus() {
        return fileStatus;
    }

    public void setFileStatus(String fileStatus) {
        this.fileStatus = fileStatus;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public Long getContractId() {
        return contractId;
    }

    public void setContractId(Long contractId) {
        this.contractId = contractId;
    }

    public Date getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(Date uploadDate) {
        this.uploadDate = uploadDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getFilingDate() {
        return filingDate;
    }

    public void setFilingDate(Date filingDate) {
        this.filingDate = filingDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPathFile() {
        return pathFile;
    }

    public void setPathFile(String pathFile) {
        this.pathFile = pathFile;
    }

    public String getFileStatusName() {
        return fileStatusName;
    }

    public void setFileStatusName(String fileStatusName) {
        this.fileStatusName = fileStatusName;
    }

    @Override
    public ContractFilesEntity toBO() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object catchId() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String catchName() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object convert(Object f) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
