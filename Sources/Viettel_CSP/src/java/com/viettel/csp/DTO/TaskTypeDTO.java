package com.viettel.csp.DTO;

import com.viettel.csp.entity.TaskTypeEntity;

public class TaskTypeDTO extends BaseCustomDTO<TaskTypeEntity>{
    private Long id;
    private String code;
    private String name;

    public TaskTypeDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public TaskTypeEntity toBO() {
        return null;
    }

    @Override
    public Object catchId() {
        return null;
    }

    @Override
    public String catchName() {
        return null;
    }
}
