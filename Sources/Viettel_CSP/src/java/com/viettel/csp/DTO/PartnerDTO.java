/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.DTO;

import com.viettel.csp.entity.PartnerEntity;
import java.util.Date;
import java.util.List;

/**
 *
 * @author GEM
 */
public class PartnerDTO extends BaseCustomDTO<PartnerEntity> {

    private Long id;
    private String partnerCode; // mã đối tác
    private String companyName;  //tên đối tác
    private String companyNameShort;// tên viết tắt
    private String addressOffice;// địa chỉ trụ sở chính
    private String addressTradingOffice; // địa chỉ giao dịch
    private String email;
    private String businessRegisNumber;
    private Date businessRegisDate;
    private String businessRegisCount;
    private String partnerGroup;// mã nhóm đối tác
    private String partnerGroupProvince; //mã tỉnh thành
    private String phone;
    private String repName;
    private String repPosition;
    private Long violation;
    private String taxCode;
    private String partnerStatusCode;
    private String partnerStatusName;
    private String partnerTypeCode;
    private String partnerTypeName;
    private String accountNumber;
    private String bankName;
    private String bankCode;
    private Date createAt;
    private Long createBy;
    private Date updateAt;
    private Long updateBy;
    private List<PartnerBankDTO> lstPartnerBank;
    private String addressHeadOffice;
    private Long isLock;
    private String userName;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getAddressHeadOffice() {
        return addressHeadOffice;
    }

    public void setAddressHeadOffice(String addressHeadOffice) {
        this.addressHeadOffice = addressHeadOffice;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBusinessRegisNumber() {
        return businessRegisNumber;
    }

    public void setBusinessRegisNumber(String businessRegisNumber) {
        this.businessRegisNumber = businessRegisNumber;
    }

    public Date getBusinessRegisDate() {
        return businessRegisDate;
    }

    public void setBusinessRegisDate(Date businessRegisDate) {
        this.businessRegisDate = businessRegisDate;
    }

    public String getBusinessRegisCount() {
        return businessRegisCount;
    }

    public void setBusinessRegisCount(String businessRegisCount) {
        this.businessRegisCount = businessRegisCount;
    }

    public String getPartnerGroup() {
        return partnerGroup;
    }

    public void setPartnerGroup(String partnerGroup) {
        this.partnerGroup = partnerGroup;
    }

    public String getPartnerGroupProvince() {
        return partnerGroupProvince;
    }

    public void setPartnerGroupProvince(String partnerGroupProvince) {
        this.partnerGroupProvince = partnerGroupProvince;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getViolation() {
        return violation;
    }

    public void setViolation(Long violation) {
        this.violation = violation;
    }

    public String getAddressOffice() {
        return addressOffice;
    }

    public void setAddressOffice(String addressOffice) {
        this.addressOffice = addressOffice;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRepName() {
        return repName;
    }

    public void setRepName(String repName) {
        this.repName = repName;
    }

    public String getRepPosition() {
        return repPosition;
    }

    public void setRepPosition(String repPosition) {
        this.repPosition = repPosition;
    }

    public String getTaxCode() {
        return taxCode;
    }

    public void setTaxCode(String taxCode) {
        this.taxCode = taxCode;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    public String getPartnerCode() {
        return partnerCode;
    }

    public void setPartnerCode(String partnerCode) {
        this.partnerCode = partnerCode;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyNameShort() {
        return companyNameShort;
    }

    public void setCompanyNameShort(String companyNameShort) {
        this.companyNameShort = companyNameShort;
    }

    public String getPartnerStatusCode() {
        return partnerStatusCode;
    }

    public void setPartnerStatusCode(String partnerStatusCode) {
        this.partnerStatusCode = partnerStatusCode;
    }

    public String getPartnerStatusName() {
        return partnerStatusName;
    }

    public void setPartnerStatusName(String partnerStatusName) {
        this.partnerStatusName = partnerStatusName;
    }

    public String getPartnerTypeCode() {
        return partnerTypeCode;
    }

    public void setPartnerTypeCode(String partnerTypeCode) {
        this.partnerTypeCode = partnerTypeCode;
    }

    public String getPartnerTypeName() {
        return partnerTypeName;
    }

    public void setPartnerTypeName(String partnerTypeName) {
        this.partnerTypeName = partnerTypeName;
    }

    public String getAddressTradingOffice() {
        return addressTradingOffice;
    }

    public void setAddressTradingOffice(String addressTradingOffice) {
        this.addressTradingOffice = addressTradingOffice;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public List<PartnerBankDTO> getLstPartnerBank() {
        return lstPartnerBank;
    }

    public void setLstPartnerBank(List<PartnerBankDTO> lstPartnerBank) {
        this.lstPartnerBank = lstPartnerBank;
    }

    public Long isIsLock() {
        return isLock;
    }

    public void setIsLock(Long isLock) {
        this.isLock = isLock;
    }

    @Override
    public PartnerEntity toBO() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object catchId() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String catchName() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
