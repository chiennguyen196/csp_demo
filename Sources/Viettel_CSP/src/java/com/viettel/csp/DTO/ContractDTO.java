/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.DTO;

import com.viettel.csp.entity.ContractEntity;
import com.viettel.csp.util.ModelMapperUtil;
import org.zkoss.util.Converter;

import java.util.Date;
import java.util.List;

/**
 * @author chitv
 */
public class ContractDTO extends BaseCustomDTO<ContractEntity> implements Converter<Object, Object> {

    private Long id;
    private Long contractId;
    private String contractStatus;
    private String contractType;
    private String contractTypeName;
    private String contractNumber;
    private Long partnerId;
    private String serviceCode;
    private String serviceName;
    private String serviceGroup;
    private String headOfService;
    private String ctSignForm;
    private Date startDate;
    private Date endDate;
    private Long createBy;
    private Date createAt;
    private Long updateBy;
    private Date updateAt;
    private Date filingDate;
    private Long filingBy;
    private Long ctUserCsp;
    private Long signCtApprove;
    private Date signCtApproveDate;
    private Long signCtExpertiseLaw;
    private Date signCtExpertiseLawDate;
    private Long signCtCompleteViettel;
    private Date signCtCompleteViettelDate;
    private Long signCtCompletePartner;
    private Date signCtCompletePartnerDate;
    private Long deployUserId;
    private Date deployAssignDate;
    private Date deployCloseDate;
    private String isLock;
    private String liStatus;
    private Long liUserCsp;
    private Long signLi;
    private Date signLiDate;
    private Long signLiExpertiseLaw;
    private Date signLiExpertiseLawDate;
    private Long signLiCompleteViettel;
    private Date signLiCompleteViettelDate;
    private Long signLiPartner;
    private Date signLiPartnerDate;
    private Date liStartDate;
    private String liSignForm;
    private Long singleLiquidation;
    private Date singleLiquidationDate;
    private Date singleLiquidationStartDate;
    private String contractStatusCode;
    private String contractStatusName;
    private String partnerCode; // mã đối tác
    private Long Idpartner; // mã đối tác
    private String companyName;  //tên đối tác
    private String companyNameShort;// tên viết tắt
    private String addressOffice;// địa chỉ trụ sở chính
    private String addressTradingOffice; // địa chỉ giao dịch
    private String email;
    private String businessRegisNumber;
    private Date businessRegisDate;
    private String businessRegisCount;
    private String partnerGroup;// mã nhóm đối tác
    private String partnerGroupProvince; //mã tỉnh thành
    private String phone;
    private String repName;
    private String repPosition;
    private Long violation;
    private String taxCode;
    private String partnerStatusCode;
    private String partnerStatusName;
    private String partnerTypeCode;
    private String partnerTypeName;
    private String accountNumber;
    private String bankName;
    private String bankCode;
    private String addressHeadOffice;
    private Long idPoint;
    private List<ContractFilesDTO> lstContractFilesDTO;
    private List<ContractFilesDTO> lstContractFilesDTOMenu;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<ContractFilesDTO> getLstContractFilesDTOMenu() {
        return lstContractFilesDTOMenu;
    }

    public void setLstContractFilesDTOMenu(List<ContractFilesDTO> lstContractFilesDTOMenu) {
        this.lstContractFilesDTOMenu = lstContractFilesDTOMenu;
    }

    public List<ContractFilesDTO> getLstContractFilesDTO() {
        return lstContractFilesDTO;
    }

    public void setLstContractFilesDTO(List<ContractFilesDTO> lstContractFilesDTO) {
        this.lstContractFilesDTO = lstContractFilesDTO;
    }

    public String getContractTypeName() {
        return contractTypeName;
    }

    public void setContractTypeName(String contractTypeName) {
        this.contractTypeName = contractTypeName;
    }

    public Long getIdpartner() {
        return Idpartner;
    }

    public void setIdpartner(Long Idpartner) {
        this.Idpartner = Idpartner;
    }

    public String getContractStatusCode() {
        return contractStatusCode;
    }

    public void setContractStatusCode(String contractStatusCode) {
        this.contractStatusCode = contractStatusCode;
    }

    public String getContractStatusName() {
        return contractStatusName;
    }

    public void setContractStatusName(String contractStatusName) {
        this.contractStatusName = contractStatusName;
    }

    public String getAddressHeadOffice() {
        return addressHeadOffice;
    }

    public void setAddressHeadOffice(String addressHeadOffice) {
        this.addressHeadOffice = addressHeadOffice;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceGroup() {
        return serviceGroup;
    }

    public void setServiceGroup(String serviceGroup) {
        this.serviceGroup = serviceGroup;
    }

    public String getBusinessRegisNumber() {
        return businessRegisNumber;
    }

    public void setBusinessRegisNumber(String businessRegisNumber) {
        this.businessRegisNumber = businessRegisNumber;
    }

    public Date getBusinessRegisDate() {
        return businessRegisDate;
    }

    public void setBusinessRegisDate(Date businessRegisDate) {
        this.businessRegisDate = businessRegisDate;
    }

    public String getBusinessRegisCount() {
        return businessRegisCount;
    }

    public void setBusinessRegisCount(String businessRegisCount) {
        this.businessRegisCount = businessRegisCount;
    }

    public String getPartnerGroup() {
        return partnerGroup;
    }

    public void setPartnerGroup(String partnerGroup) {
        this.partnerGroup = partnerGroup;
    }

    public String getPartnerGroupProvince() {
        return partnerGroupProvince;
    }

    public void setPartnerGroupProvince(String partnerGroupProvince) {
        this.partnerGroupProvince = partnerGroupProvince;
    }

    public Long getContractId() {
        return contractId;
    }

    public void setContractId(Long contractId) {
        this.contractId = contractId;
    }

    public Long getViolation() {
        return violation;
    }

    public void setViolation(Long violation) {
        this.violation = violation;
    }

    public String getAddressOffice() {
        return addressOffice;
    }

    public void setAddressOffice(String addressOffice) {
        this.addressOffice = addressOffice;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRepName() {
        return repName;
    }

    public void setRepName(String repName) {
        this.repName = repName;
    }

    public String getRepPosition() {
        return repPosition;
    }

    public void setRepPosition(String repPosition) {
        this.repPosition = repPosition;
    }

    public String getTaxCode() {
        return taxCode;
    }

    public void setTaxCode(String taxCode) {
        this.taxCode = taxCode;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    public String getPartnerCode() {
        return partnerCode;
    }

    public void setPartnerCode(String partnerCode) {
        this.partnerCode = partnerCode;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyNameShort() {
        return companyNameShort;
    }

    public void setCompanyNameShort(String companyNameShort) {
        this.companyNameShort = companyNameShort;
    }

    public String getPartnerStatusCode() {
        return partnerStatusCode;
    }

    public void setPartnerStatusCode(String partnerStatusCode) {
        this.partnerStatusCode = partnerStatusCode;
    }

    public String getPartnerStatusName() {
        return partnerStatusName;
    }

    public void setPartnerStatusName(String partnerStatusName) {
        this.partnerStatusName = partnerStatusName;
    }

    public String getPartnerTypeCode() {
        return partnerTypeCode;
    }

    public void setPartnerTypeCode(String partnerTypeCode) {
        this.partnerTypeCode = partnerTypeCode;
    }

    public String getPartnerTypeName() {
        return partnerTypeName;
    }

    public void setPartnerTypeName(String partnerTypeName) {
        this.partnerTypeName = partnerTypeName;
    }

    public String getAddressTradingOffice() {
        return addressTradingOffice;
    }

    public void setAddressTradingOffice(String addressTradingOffice) {
        this.addressTradingOffice = addressTradingOffice;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public String getContractStatus() {
        return contractStatus;
    }

    public void setContractStatus(String contractStatus) {
        this.contractStatus = contractStatus;
    }

    public String getContractType() {
        return contractType;
    }

    public void setContractType(String contractType) {
        this.contractType = contractType;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public Long getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(Long partnerId) {
        this.partnerId = partnerId;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getHeadOfService() {
        return headOfService;
    }

    public void setHeadOfService(String headOfService) {
        this.headOfService = headOfService;
    }

    public String getCtSignForm() {
        return ctSignForm;
    }

    public void setCtSignForm(String ctSignForm) {
        this.ctSignForm = ctSignForm;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getFilingDate() {
        return filingDate;
    }

    public void setFilingDate(Date filingDate) {
        this.filingDate = filingDate;
    }

    public Long getFilingBy() {
        return filingBy;
    }

    public void setFilingBy(Long filingBy) {
        this.filingBy = filingBy;
    }

    public Long getCtUserCsp() {
        return ctUserCsp;
    }

    public void setCtUserCsp(Long ctUserCsp) {
        this.ctUserCsp = ctUserCsp;
    }

    public Long getSignCtApprove() {
        return signCtApprove;
    }

    public void setSignCtApprove(Long signCtApprove) {
        this.signCtApprove = signCtApprove;
    }

    public Date getSignCtApproveDate() {
        return signCtApproveDate;
    }

    public void setSignCtApproveDate(Date signCtApproveDate) {
        this.signCtApproveDate = signCtApproveDate;
    }

    public Long getSignCtExpertiseLaw() {
        return signCtExpertiseLaw;
    }

    public void setSignCtExpertiseLaw(Long signCtExpertiseLaw) {
        this.signCtExpertiseLaw = signCtExpertiseLaw;
    }

    public Date getSignCtExpertiseLawDate() {
        return signCtExpertiseLawDate;
    }

    public void setSignCtExpertiseLawDate(Date signCtExpertiseLawDate) {
        this.signCtExpertiseLawDate = signCtExpertiseLawDate;
    }

    public Long getSignCtCompleteViettel() {
        return signCtCompleteViettel;
    }

    public void setSignCtCompleteViettel(Long signCtCompleteViettel) {
        this.signCtCompleteViettel = signCtCompleteViettel;
    }

    public Date getSignCtCompleteViettelDate() {
        return signCtCompleteViettelDate;
    }

    public void setSignCtCompleteViettelDate(Date signCtCompleteViettelDate) {
        this.signCtCompleteViettelDate = signCtCompleteViettelDate;
    }

    public Long getSignCtCompletePartner() {
        return signCtCompletePartner;
    }

    public void setSignCtCompletePartner(Long signCtCompletePartner) {
        this.signCtCompletePartner = signCtCompletePartner;
    }

    public Date getSignCtCompletePartnerDate() {
        return signCtCompletePartnerDate;
    }

    public void setSignCtCompletePartnerDate(Date signCtCompletePartnerDate) {
        this.signCtCompletePartnerDate = signCtCompletePartnerDate;
    }

    public Long getDeployUserId() {
        return deployUserId;
    }

    public void setDeployUserId(Long deployUserId) {
        this.deployUserId = deployUserId;
    }

    public Date getDeployAssignDate() {
        return deployAssignDate;
    }

    public void setDeployAssignDate(Date deployAssignDate) {
        this.deployAssignDate = deployAssignDate;
    }

    public Date getDeployCloseDate() {
        return deployCloseDate;
    }

    public void setDeployCloseDate(Date deployCloseDate) {
        this.deployCloseDate = deployCloseDate;
    }

    public String getIsLock() {
        return isLock;
    }

    public void setIsLock(String isLock) {
        this.isLock = isLock;
    }

    public String getLiStatus() {
        return liStatus;
    }

    public void setLiStatus(String liStatus) {
        this.liStatus = liStatus;
    }

    public Long getLiUserCsp() {
        return liUserCsp;
    }

    public void setLiUserCsp(Long liUserCsp) {
        this.liUserCsp = liUserCsp;
    }

    public Long getSignLi() {
        return signLi;
    }

    public void setSignLi(Long signLi) {
        this.signLi = signLi;
    }

    public Date getSignLiDate() {
        return signLiDate;
    }

    public void setSignLiDate(Date signLiDate) {
        this.signLiDate = signLiDate;
    }

    public Long getSignLiExpertiseLaw() {
        return signLiExpertiseLaw;
    }

    public void setSignLiExpertiseLaw(Long signLiExpertiseLaw) {
        this.signLiExpertiseLaw = signLiExpertiseLaw;
    }

    public Date getSignLiExpertiseLawDate() {
        return signLiExpertiseLawDate;
    }

    public void setSignLiExpertiseLawDate(Date signLiExpertiseLawDate) {
        this.signLiExpertiseLawDate = signLiExpertiseLawDate;
    }

    public Long getSignLiCompleteViettel() {
        return signLiCompleteViettel;
    }

    public void setSignLiCompleteViettel(Long signLiCompleteViettel) {
        this.signLiCompleteViettel = signLiCompleteViettel;
    }

    public Date getSignLiCompleteViettelDate() {
        return signLiCompleteViettelDate;
    }

    public void setSignLiCompleteViettelDate(Date signLiCompleteViettelDate) {
        this.signLiCompleteViettelDate = signLiCompleteViettelDate;
    }

    public Long getSignLiPartner() {
        return signLiPartner;
    }

    public void setSignLiPartner(Long signLiPartner) {
        this.signLiPartner = signLiPartner;
    }

    public Date getSignLiPartnerDate() {
        return signLiPartnerDate;
    }

    public void setSignLiPartnerDate(Date signLiPartnerDate) {
        this.signLiPartnerDate = signLiPartnerDate;
    }

    public Date getLiStartDate() {
        return liStartDate;
    }

    public void setLiStartDate(Date liStartDate) {
        this.liStartDate = liStartDate;
    }

    public String getLiSignForm() {
        return liSignForm;
    }

    public void setLiSignForm(String liSignForm) {
        this.liSignForm = liSignForm;
    }

    public Long getSingleLiquidation() {
        return singleLiquidation;
    }

    public void setSingleLiquidation(Long singleLiquidation) {
        this.singleLiquidation = singleLiquidation;
    }

    public Date getSingleLiquidationDate() {
        return singleLiquidationDate;
    }

    public void setSingleLiquidationDate(Date singleLiquidationDate) {
        this.singleLiquidationDate = singleLiquidationDate;
    }

    public Date getSingleLiquidationStartDate() {
        return singleLiquidationStartDate;
    }

    public void setSingleLiquidationStartDate(Date singleLiquidationStartDate) {
        this.singleLiquidationStartDate = singleLiquidationStartDate;
    }

    public Long getIdPoint() {
        return idPoint;
    }

    public void setIdPoint(Long idPoint) {
        this.idPoint = idPoint;
    }

    @Override
    public ContractEntity toBO() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object catchId() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String catchName() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object convert(Object f) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public static ContractEntity convert(ContractDTO contractDTO) {
        ContractEntity contractEntity = new ContractEntity();
        ModelMapperUtil mapperUtil = new ModelMapperUtil();
        contractEntity = mapperUtil.mapper(contractDTO, ContractEntity.class);
        contractEntity.setId(contractDTO.getContractId());
        return contractEntity;
    }
}
