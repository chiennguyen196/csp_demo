package com.viettel.csp.DTO;

import com.viettel.csp.entity.BaseCustomEntity;

import java.util.Date;

/**
 * Created by GEM on 6/16/2017.
 */
public class ContractBreachDisDTO extends BaseCustomDTO {
    private long id;
    private String disCode;
    private String headOfService;
    private String violationName;
    private Date pauseStartDate;
    private Date pauseEndDate;
    private Date dateOff;
    private String content;
    private String contentValue;
    private String doc;
    private String violationNumber;
    private Long contractBreachId;

    public ContractBreachDisDTO(){
        disCode = "";
        headOfService = "";
        violationName = "";
        content = "";
        contentValue = "";
        doc = "";
        violationNumber = "";
    }

    public String getDoc() {
        return doc;
    }

    public void setDoc(String doc) {
        this.doc = doc;
    }

    public String getViolationNumber() {
        return violationNumber;
    }

    public void setViolationNumber(String violationNumber) {
        this.violationNumber = violationNumber;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getHeadOfService() {
        return headOfService;
    }

    public void setHeadOfService(String headOfService) {
        this.headOfService = headOfService;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDisCode() {
        return disCode;
    }

    public void setDisCode(String disCode) {
        this.disCode = disCode;
    }

    public Date getPauseStartDate() {
        return pauseStartDate;
    }

    public void setPauseStartDate(Date pauseStartDate) {
        this.pauseStartDate = pauseStartDate;
    }

    public Date getPauseEndDate() {
        return pauseEndDate;
    }

    public void setPauseEndDate(Date pauseEndDate) {
        this.pauseEndDate = pauseEndDate;
    }

    public Date getDateOff() {
        return dateOff;
    }

    public void setDateOff(Date dateOff) {
        this.dateOff = dateOff;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContentValue() {
        return contentValue;
    }

    public void setContentValue(String contentValue) {
        this.contentValue = contentValue;
    }

    public Long getContractBreachId() {
        return contractBreachId;
    }

    public void setContractBreachId(Long contractBreachId) {
        this.contractBreachId = contractBreachId;
    }

    public String getViolationName() {
        return violationName;
    }

    public void setViolationName(String violationName) {
        this.violationName = violationName;
    }

    @Override
    public BaseCustomEntity toBO() {
        return null;
    }

    @Override
    public Object catchId() {
        return null;
    }

    @Override
    public String catchName() {
        return null;
    }
}
