/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.csp.config;

import com.viettel.csp.entity.ContactEntity;
import com.viettel.security.PassTranformer;
import java.io.File;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author GEM
 */
public class HibernateUtils {

    private static final Logger logger = Logger.getLogger(HibernateUtils.class);
    private static final SessionFactory sessionFactory;

    private static final String pathConfig;

    static {
        try {
            pathConfig = "hibernate.cfg.xml";
            AnnotationConfiguration config = new AnnotationConfiguration().configure(pathConfig);
//            String url = "";
//            String userName = "";
//            String pass = "";
//            try {
////                System.out.println("hibernate.connection.url: "+PassTranformer.encrypt(config.getProperty("hibernate.connection.url")));
////                System.out.println("hibernate.connection.username: "+PassTranformer.encrypt(config.getProperty("hibernate.connection.username")));
////                System.out.println("hibernate.connection.password: "+PassTranformer.encrypt(config.getProperty("hibernate.connection.password")));
//
//                url = PassTranformer.decrypt(config.getProperty("hibernate.connection.url"));
////                url = config.getProperty("hibernate.connection.url");
//            } catch (Exception e) {
//                logger.error(e);
//            }
//            try {
//                userName = PassTranformer.decrypt(config.getProperty("hibernate.connection.username"));
////                userName = config.getProperty("hibernate.connection.username");
//            } catch (Exception e) {
//                logger.error(e);
//                userName = config.getProperty("hibernate.connection.username");
//            }
//            try {
//                pass = PassTranformer.decrypt(config.getProperty("hibernate.connection.password"));
////                pass = config.getProperty("hibernate.connection.password");
//            } catch (Exception e) {
//                logger.error(e);
//                pass = config.getProperty("hibernate.connection.password");
//            }
//            config.setProperty("hibernate.connection.url", url);
//            config.setProperty("hibernate.connection.username", userName);
//            config.setProperty("hibernate.connection.password", pass);
//            sessionFactory = new AnnotationConfiguration().configure(new File(pathConfig)).buildSessionFactory();
            sessionFactory = config.buildSessionFactory();
        } catch (Throwable ex) {
            logger.error(ex, ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    /**
     * Get our sessionfactory.
     *
     * @return SessionFactory
     */
    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    /**
     * Get current session.
     *
     * @return Session
     */
    public static Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    /**
     * Handle sessions and close them at end of HTTP transaction instead of
     * after tx.commit()
     *
     * @return the session
     */
    public static Session getSession() {
        Session session = sessionFactory.openSession();
        return session;
    }

    /**
     * Close open session.
     */
    public static void closeSession(Session session) {
        if (session != null) {
            session.close();
        }
    }

    /**
     * Get item count for Entity.
     *
     * @param clazz Target class
     * @param whereClauses Optional where clause
     * @return Count of rows for target entity
     */
    public static int count(Class<?> clazz,
            String whereClauses) {
        Session session = getSession();

        Criteria criteria = session.createCriteria(clazz);
        criteria.setProjection(Projections.rowCount());

        return ((Integer) criteria.list().get(0)).intValue();
    }

    /**
     * Save/update all in list.
     *
     * @param list List of entities to save
     */
    public static void saveAll(List<?> list) {
        if (list == null || list.isEmpty()) {
            logger.info("List null or empty, nothing saved!");
            return;
        }

        Session session = getSession();
        Transaction tx = session.beginTransaction();
        for (Object entity : list) {
            session.saveOrUpdate(entity);
        }
        tx.commit();
    }

    /**
     * Save/update entity.
     *
     * @param entity Entity to save
     */
    public static void save(Object entity) {
        Session session = getSession();
        Transaction tx = session.beginTransaction();
        session.evict(entity);
        session.saveOrUpdate(entity);
        tx.commit();
    }

    /**
     * Update entity references to session.
     *
     * @param entity Entity class
     */
    public static void update(Object entity) {
        if (!getSession().isOpen()) {
            logger.info("update(" + entity.getClass().getSimpleName()
                    + ")id while session closed");
            return;
        }

        if (!getSession().contains(entity)) {
            getSession().update(entity);
        }
    }

    public static void main(String[] args) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtils.getSession();
            transaction = session.beginTransaction();
            Criteria cri = session.createCriteria(ContactEntity.class);
            if (!cri.list().isEmpty()) {
                System.out.println("Connect successful! size list: " + cri.list().size());
            } else {
                System.out.println("Connect successful, no data!");
            }
            transaction.commit();
            session = HibernateUtils.getCurrentSession();
            transaction = session.beginTransaction();
            cri = session.createCriteria(ContactEntity.class);
            if (!cri.list().isEmpty()) {
                System.out.println("Connect successful! size list: " + cri.list().size());
            } else {
                System.out.println("Connect successful, no data!");
            }
            transaction.commit();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
