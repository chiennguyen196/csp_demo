﻿/*
Navicat Oracle Data Transfer
Oracle Client Version : 10.2.0.5.0

Source Server         : 172.16.10.159
Source Server Version : 120200
Source Host           : 172.16.10.159:1521
Source Schema         : C##VIETTEL_CSP

Target Server Type    : ORACLE
Target Server Version : 120200
File Encoding         : 65001

Date: 2017-06-12 13:38:44
*/


-- ----------------------------
-- Table structure for TBL_BANK
-- ----------------------------
DROP TABLE "TBL_BANK";
CREATE TABLE "TBL_BANK" (
"CODE" VARCHAR2(256 BYTE) NOT NULL ,
"NAME" VARCHAR2(256 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_BANK
-- ----------------------------
INSERT INTO "TBL_BANK" VALUES ('BIDV', 'Ngân hàng Đầu tư và Phát triển Việt Nam');
INSERT INTO "TBL_BANK" VALUES ('AGB', 'Ngân hàng nông nghiệp và phát triển nông thôn');
INSERT INTO "TBL_BANK" VALUES ('VTB', 'Ngân hàng công thương Việt Nam');
INSERT INTO "TBL_BANK" VALUES ('TPB', 'Ngân hàng Tiên Phong');
INSERT INTO "TBL_BANK" VALUES ('DAB', 'Ngân hàng Đông Á');

-- ----------------------------
-- Table structure for TBL_CONTACT
-- ----------------------------
DROP TABLE "TBL_CONTACT";
CREATE TABLE "TBL_CONTACT" (
"ID" NUMBER NOT NULL ,
"CONTACT_TYPE" VARCHAR2(256 BYTE) NULL ,
"PARTNER_ID" NUMBER NULL ,
"NAME" VARCHAR2(256 BYTE) NULL ,
"EMAIL" VARCHAR2(256 BYTE) NULL ,
"FIXED_PHONE_NUMBER" VARCHAR2(256 BYTE) NULL ,
"MOBILE_PHONE_NUMBER" VARCHAR2(256 BYTE) NULL ,
"SEX" VARCHAR2(256 BYTE) NULL ,
"POSITION" VARCHAR2(256 BYTE) NULL ,
"DEPARTMENT" VARCHAR2(256 BYTE) NULL ,
"OTHER_INFO" VARCHAR2(256 BYTE) NULL ,
"CONTACT_STATUS" VARCHAR2(256 BYTE) NULL ,
"CREATE_AT" DATE NULL ,
"CREATE_BY" NUMBER NULL ,
"UPDATE_AT" DATE NULL ,
"UPDATE_BY" NUMBER NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_CONTACT
-- ----------------------------
<<<<<<< HEAD
INSERT INTO "TBL_CONTACT" VALUES ('22', '002', '3', 'nguyễn văn tiến', 'tien@gmail.com', '098227827', '093938388', 'MALE', 'test1111', 'tiennv', 'other information', 'ACTIVE', TO_DATE('2017-05-23 15:36:51', 'YYYY-MM-DD HH24:MI:SS'), '0', TO_DATE('2017-06-01 10:31:13', 'YYYY-MM-DD HH24:MI:SS'), '0');
INSERT INTO "TBL_CONTACT" VALUES ('26', '002', '5', '123123', '1232312@gmail.com', '123231212', '123234567', 'MALE', null, null, null, 'ACTIVE', TO_DATE('2017-05-23 16:17:26', 'YYYY-MM-DD HH24:MI:SS'), '0', TO_DATE('2017-05-23 16:58:54', 'YYYY-MM-DD HH24:MI:SS'), '0');
INSERT INTO "TBL_CONTACT" VALUES ('24', '006', '3', 'lại thanh', 'hoavt11@viettel.com.vn', '123456789', '1234567891', 'FEMALE', 'test', 'htrqđ', 'abc123', 'ACTIVE', TO_DATE('2017-05-23 16:09:52', 'YYYY-MM-DD HH24:MI:SS'), '0', TO_DATE('2017-05-23 16:59:09', 'YYYY-MM-DD HH24:MI:SS'), '0');
INSERT INTO "TBL_CONTACT" VALUES ('27', '004', '3', '123123', 'aaaar@gmail.com', '123242342', '234234234', 'FEMALE', null, null, null, 'ACTIVE', TO_DATE('2017-05-23 17:07:11', 'YYYY-MM-DD HH24:MI:SS'), '0', TO_DATE('2017-05-23 17:07:11', 'YYYY-MM-DD HH24:MI:SS'), '0');
=======
INSERT INTO "TBL_CONTACT" VALUES ('22', '002', '21', 'nguyễn văn tiến', 'tien@gmail.com', '098227827', '093938388', 'MALE', 'test1111', 'tiennv', 'other information', 'ACTIVE', TO_DATE('2017-05-23 15:36:51', 'YYYY-MM-DD HH24:MI:SS'), '0', TO_DATE('2017-06-10 13:37:22', 'YYYY-MM-DD HH24:MI:SS'), '0');
INSERT INTO "TBL_CONTACT" VALUES ('26', '002', '21', '123123', '1232312@gmail.com', '123231212', '123234567', 'MALE', null, null, null, 'ACTIVE', TO_DATE('2017-05-23 16:17:26', 'YYYY-MM-DD HH24:MI:SS'), '0', TO_DATE('2017-05-23 16:58:54', 'YYYY-MM-DD HH24:MI:SS'), '0');
INSERT INTO "TBL_CONTACT" VALUES ('24', '002', '21', 'lại thanh', 'hoavt11@viettel.com.vn', '123456789', '1234567891', 'FEMALE', 'test', 'htrqđ', 'abc123', 'ACTIVE', TO_DATE('2017-05-23 16:09:52', 'YYYY-MM-DD HH24:MI:SS'), '0', TO_DATE('2017-05-23 16:59:09', 'YYYY-MM-DD HH24:MI:SS'), '0');
INSERT INTO "TBL_CONTACT" VALUES ('27', '004', '21', '123123', 'aaaar@gmail.com', '123242342', '234234234', 'FEMALE', null, null, null, 'ACTIVE', TO_DATE('2017-05-23 17:07:11', 'YYYY-MM-DD HH24:MI:SS'), '0', TO_DATE('2017-05-23 17:07:11', 'YYYY-MM-DD HH24:MI:SS'), '0');
>>>>>>> 4f1464f3ea01ff1d3e2fb0f514f6fda65674f3f6

-- ----------------------------
-- Table structure for TBL_CONTACT_POINT
-- ----------------------------
DROP TABLE "TBL_CONTACT_POINT";
CREATE TABLE "TBL_CONTACT_POINT" (
"ID" NUMBER(10) NOT NULL ,
"CONTACT_ID" NUMBER NULL ,
"CONTRACT_ID" NUMBER NULL ,
"CONTACT_TYPE" VARCHAR2(256 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_CONTACT_POINT
-- ----------------------------
INSERT INTO "TBL_CONTACT_POINT" VALUES ('175', '24', '172', '003');
INSERT INTO "TBL_CONTACT_POINT" VALUES ('176', '26', '172', '004');
INSERT INTO "TBL_CONTACT_POINT" VALUES ('177', '22', '172', '005');
INSERT INTO "TBL_CONTACT_POINT" VALUES ('178', '22', '172', '006');
INSERT INTO "TBL_CONTACT_POINT" VALUES ('173', '22', '172', '001');
INSERT INTO "TBL_CONTACT_POINT" VALUES ('174', '22', '172', '002');

-- ----------------------------
-- Table structure for TBL_CONTACT_TYPE
-- ----------------------------
DROP TABLE "TBL_CONTACT_TYPE";
CREATE TABLE "TBL_CONTACT_TYPE" (
"CODE" VARCHAR2(256 BYTE) NOT NULL ,
"NAME" VARCHAR2(256 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_CONTACT_TYPE
-- ----------------------------
INSERT INTO "TBL_CONTACT_TYPE" VALUES ('001', 'Đầu mối liên hệ về Kinh doanh');
INSERT INTO "TBL_CONTACT_TYPE" VALUES ('002', 'Đầu mối liên hệ về Kỹ thuật');
INSERT INTO "TBL_CONTACT_TYPE" VALUES ('003', 'Đầu mối liên hệ về Đối soát');
INSERT INTO "TBL_CONTACT_TYPE" VALUES ('004', 'Đầu mối liên hệ về Thanh toán');
INSERT INTO "TBL_CONTACT_TYPE" VALUES ('005', 'Đầu mối liên hệ về Đối soát');
INSERT INTO "TBL_CONTACT_TYPE" VALUES ('006', 'Đầu mối liên hệ về Hợp đồng');

-- ----------------------------
-- Table structure for TBL_CONTRACT
-- ----------------------------
DROP TABLE "TBL_CONTRACT";
CREATE TABLE "TBL_CONTRACT" (
"ID" NUMBER(10) NOT NULL ,
"CONTRACT_STATUS" VARCHAR2(256 BYTE) NULL ,
"CONTRACT_TYPE" VARCHAR2(256 BYTE) NULL ,
"CONTRACT_NUMBER" VARCHAR2(256 BYTE) NULL ,
"PARTNER_ID" NUMBER NULL ,
"SERVICE_CODE" VARCHAR2(256 BYTE) NULL ,
"HEAD_OF_SERVICE" VARCHAR2(256 BYTE) NULL ,
"CT_SIGN_FORM" VARCHAR2(256 BYTE) DEFAULT NULL  NULL ,
"START_DATE" DATE NULL ,
"END_DATE" DATE NULL ,
"CREATE_BY" NUMBER NULL ,
"CREATE_AT" DATE DEFAULT NULL  NULL ,
"UPDATE_BY" NUMBER NULL ,
"UPDATE_AT" DATE DEFAULT NULL  NULL ,
"FILING_DATE" DATE NULL ,
"FILING_BY" NUMBER NULL ,
"CT_USER_CSP" NUMBER NULL ,
"SIGN_CT_APPROVE" NUMBER NULL ,
"SIGN_CT_APPROVE_DATE" DATE NULL ,
"SIGN_CT_EXPERTISE_LAW" NUMBER NULL ,
"SIGN_CT_EXPERTISE_LAW_DATE" DATE NULL ,
"SIGN_CT_COMPLETE_VIETTEL" NUMBER NULL ,
"SIGN_CT_COMPLETE_VIETTEL_DATE" DATE NULL ,
"SIGN_CT_COMPLETE_PARTNER" NUMBER NULL ,
"SIGN_CT_COMPLETE_PARTNER_DATE" DATE NULL ,
"DEPLOY_USER_ID" NUMBER NULL ,
"DEPLOY_ASSIGN_DATE" DATE NULL ,
"DEPLOY_CLOSE_DATE" DATE NULL ,
"IS_LOCK" NUMBER NULL ,
"LI_USER_CSP" NUMBER NULL ,
"SIGN_LI" NUMBER NULL ,
"SIGN_LI_DATE" DATE NULL ,
"SIGN_LI_EXPERTISE_LAW" NUMBER NULL ,
"SIGN_LI_EXPERTISE_LAW_DATE" DATE NULL ,
"SIGN_LI_COMPLETE_VIETTEL" NUMBER NULL ,
"SIGN_LI_COMPLETE_VIETTEL_DATE" DATE NULL ,
"SIGN_LI_PARTNER" NUMBER NULL ,
"SIGN_LI_PARTNER_DATE" DATE NULL ,
"SINGLE_LI" NUMBER NULL ,
"SINGLE_LI_DATE" DATE NULL ,
"SINGLE_LI_START_DATE" DATE NULL ,
"LI_STATUS" NUMBER(10) NULL ,
"SINGLE_LIQUIDATION" NUMBER(10) NULL ,
"SINGLE_LIQUIDATION_DATE" DATE NULL ,
"SINGLE_LIQUIDATION_START_DATE" DATE NULL ,
"LI_SIGN_FORM" VARCHAR2(256 BYTE) NULL ,
"LI_START_DATE" DATE NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_CONTRACT
-- ----------------------------
<<<<<<< HEAD
INSERT INTO "TBL_CONTRACT" VALUES ('62', '9', null, '2', '24', '2', '456', '0', null, null, '24', TO_DATE('2017-05-30 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '24', TO_DATE('2017-06-02 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2017-05-14 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '24', '0', '0', null, '0', null, '0', null, '0', null, '0', null, null, '0', '0', '0', null, '0', null, '0', null, '0', null, null, null, null, null, null, null, null, null, null);
INSERT INTO "TBL_CONTRACT" VALUES ('61', '5', null, '3', '24', '3', '123', '0', null, null, '24', TO_DATE('2017-05-30 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '24', TO_DATE('2017-06-02 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2017-05-29 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '24', '0', '0', null, '0', null, '0', null, '0', null, '0', null, null, '0', '0', '0', null, '0', null, '0', null, '0', null, null, null, null, null, null, null, null, null, null);
INSERT INTO "TBL_CONTRACT" VALUES ('5', '10', '1', '1', '24', '1', '1', '1', TO_DATE('2017-05-25 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2017-05-25 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '444', null, '24', TO_DATE('2017-06-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2017-05-12 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '1', '453', '7897', null, '1', TO_DATE('2017-05-26 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '1', TO_DATE('2017-05-26 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '546', TO_DATE('2017-05-26 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '123123', TO_DATE('2017-05-26 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2017-05-26 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '123', '1232', '12312', TO_DATE('2017-05-26 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '23423', TO_DATE('2017-05-26 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '123', TO_DATE('2017-05-26 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '234', TO_DATE('2017-05-26 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '23423', TO_DATE('2017-05-26 13:35:23', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2017-05-26 13:35:25', 'YYYY-MM-DD HH24:MI:SS'), '2', '2', TO_DATE('2017-05-26 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2017-05-26 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '1', TO_DATE('2017-05-26 00:00:00', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "TBL_CONTRACT" VALUES ('42', '2', null, '4', '24', '4', '789', '0', null, null, '24', TO_DATE('2017-05-29 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '24', TO_DATE('2017-06-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2017-05-25 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '24', '0', '0', null, '0', null, '0', null, '0', null, '0', null, null, '0', '0', '0', null, '0', null, '0', null, '0', null, null, null, null, null, null, null, null, null, null);
=======
INSERT INTO "TBL_CONTRACT" VALUES ('241', '9', 'CONTRACTYPE_TWO', '4534', '45', '2', '456', 'HAND', TO_DATE('2017-05-30 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2017-06-22 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '45', TO_DATE('2017-06-09 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '0', TO_DATE('2017-06-09 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2017-05-19 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '45', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "TBL_CONTRACT" VALUES ('161', '13', '001', null, '43', '2', '456', 'HAND', null, null, '21', TO_DATE('2017-06-05 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '43', TO_DATE('2017-06-07 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2017-05-14 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '21', null, null, null, null, null, null, null, null, null, '43', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "TBL_CONTRACT" VALUES ('162', '10', '002', null, '45', '2', '456', 'HAND', null, null, '21', TO_DATE('2017-06-05 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '45', TO_DATE('2017-06-09 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2017-05-14 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '21', null, null, null, null, null, null, null, null, null, '43', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "TBL_CONTRACT" VALUES ('164', '1', '003', '666', '45', '2', '456', 'HAND', TO_DATE('2017-05-29 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2017-06-15 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '21', TO_DATE('2017-06-05 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '45', TO_DATE('2017-06-09 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2017-05-19 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '21', null, null, null, null, null, null, null, null, null, '43', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "TBL_CONTRACT" VALUES ('101', '6', '001', '666', '121', '2', '456', 'HAND', TO_DATE('2017-05-29 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2017-06-15 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '21', TO_DATE('2017-06-05 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '43', TO_DATE('2017-06-07 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2017-05-19 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '21', null, null, null, null, null, null, null, null, null, '43', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "TBL_CONTRACT" VALUES ('221', '28', 'CONTRACTYPE_TWO', '123456', '43', '2', '456', 'HAND', TO_DATE('2017-06-08 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2017-06-15 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '21', TO_DATE('2017-06-08 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '43', TO_DATE('2017-06-10 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2017-05-19 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '21', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "TBL_CONTRACT" VALUES ('242', '26', 'CONTRACTYPE_TWO', '45756', '43', '2', '456', 'HAND', TO_DATE('2017-05-29 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2017-06-23 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '21', TO_DATE('2017-06-09 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '43', TO_DATE('2017-06-10 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2017-05-19 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '21', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "TBL_CONTRACT" VALUES ('243', '26', 'CONTRACTYPE_TWO', '654', '43', '2', '456', 'HAND', TO_DATE('2017-05-30 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2017-06-22 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '21', TO_DATE('2017-06-09 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '43', TO_DATE('2017-06-10 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2017-05-19 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '21', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "TBL_CONTRACT" VALUES ('181', '12', '002', '23', '21', '2', '456', 'HAND', TO_DATE('2017-05-30 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2017-06-22 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '21', TO_DATE('2017-06-07 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '21', TO_DATE('2017-06-09 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2017-05-19 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '21', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "TBL_CONTRACT" VALUES ('171', '12', '003', '666', '43', '2', '456', 'HAND', TO_DATE('2017-05-29 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2017-06-15 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '21', TO_DATE('2017-06-05 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '43', TO_DATE('2017-06-09 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2017-05-19 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '21', null, null, null, null, null, null, null, null, null, '43', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "TBL_CONTRACT" VALUES ('172', '11', '002', 'ko sua truong trang thai contract', '21', '2', '456', 'HAND', TO_DATE('2017-05-29 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2017-06-15 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '21', TO_DATE('2017-06-05 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '21', TO_DATE('2017-06-12 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, '21', null, null, null, null, null, null, null, null, null, '43', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "TBL_CONTRACT" VALUES ('173', '11', '002', '666', '43', '2', '456', 'HAND', TO_DATE('2017-05-29 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2017-06-15 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '21', TO_DATE('2017-06-05 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '43', TO_DATE('2017-06-08 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2017-05-19 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '21', null, null, null, null, null, null, null, null, null, '43', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "TBL_CONTRACT" VALUES ('202', '12', '003', '1112', '21', '2', '456', 'HAND', TO_DATE('2017-06-07 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2017-06-15 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '21', TO_DATE('2017-06-08 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '0', TO_DATE('2017-06-08 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2017-05-19 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '21', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "TBL_CONTRACT" VALUES ('182', '12', '001', '56756', '21', '2', '456', 'HAND', TO_DATE('2017-05-30 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2017-06-22 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '21', TO_DATE('2017-06-07 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '0', TO_DATE('2017-06-07 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2017-05-19 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '21', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "TBL_CONTRACT" VALUES ('201', '9', '002', '1112', '21', '2', '456', 'HAND', TO_DATE('2017-06-07 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2017-06-15 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '21', TO_DATE('2017-06-08 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '0', TO_DATE('2017-06-08 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2017-05-19 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '21', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
>>>>>>> 4f1464f3ea01ff1d3e2fb0f514f6fda65674f3f6

-- ----------------------------
-- Table structure for TBL_CONTRACT_BREACH
-- ----------------------------
DROP TABLE "TBL_CONTRACT_BREACH";
CREATE TABLE "TBL_CONTRACT_BREACH" (
"ID" NUMBER(10) NOT NULL ,
"CONTRACT_ID" NUMBER NULL ,
"PARTNER_ID" NUMBER NULL ,
"START_DATE" DATE NULL ,
"END_DATE" DATE NULL ,
"IS_PUBLIC" NUMBER NULL ,
"CREATE_AT" DATE NULL ,
"CREATE_BY" NUMBER NULL ,
"UPDATE_AT" DATE NULL ,
"UPDATE_BY" NUMBER NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_CONTRACT_BREACH
-- ----------------------------

-- ----------------------------
-- Table structure for TBL_CONTRACT_BREACH_DISCIPLINE
-- ----------------------------
DROP TABLE "TBL_CONTRACT_BREACH_DISCIPLINE";
CREATE TABLE "TBL_CONTRACT_BREACH_DISCIPLINE" (
"ID" NUMBER(10) NOT NULL ,
"DISCIPLINE_CODE" VARCHAR2(256 BYTE) NULL ,
"PAUSE_START_DATE" DATE NULL ,
"PAUSE_END_DATE" DATE NULL ,
"DATE_OFF" DATE NULL ,
"CONTENT" VARCHAR2(2048 BYTE) NULL ,
"CONTENT_VALUE" VARCHAR2(2048 BYTE) NULL ,
"CONTRACT_BREACH_ID" NUMBER NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_CONTRACT_BREACH_DISCIPLINE
-- ----------------------------

-- ----------------------------
-- Table structure for TBL_CONTRACT_BREACH_FILES
-- ----------------------------
DROP TABLE "TBL_CONTRACT_BREACH_FILES";
CREATE TABLE "TBL_CONTRACT_BREACH_FILES" (
"ID" NUMBER(10) NOT NULL ,
"FILE_NAME" VARCHAR2(2048 BYTE) NULL ,
"DESCRIPTION" VARCHAR2(2048 BYTE) NULL ,
"PARTNER_BREACH_ID" NUMBER NULL ,
"UPLOAD_DATE" DATE NULL ,
"PATH_FILE" VARCHAR2(1024 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_CONTRACT_BREACH_FILES
-- ----------------------------

-- ----------------------------
-- Table structure for TBL_CONTRACT_BREACH_REASON
-- ----------------------------
DROP TABLE "TBL_CONTRACT_BREACH_REASON";
CREATE TABLE "TBL_CONTRACT_BREACH_REASON" (
"ID" NUMBER(10) NOT NULL ,
"REASON_CODE" VARCHAR2(256 BYTE) NULL ,
"CONTRACT_BREACH_ID" NUMBER NULL ,
"OTHER" VARCHAR2(2048 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_CONTRACT_BREACH_REASON
-- ----------------------------

-- ----------------------------
-- Table structure for TBL_CONTRACT_FILES
-- ----------------------------
DROP TABLE "TBL_CONTRACT_FILES";
CREATE TABLE "TBL_CONTRACT_FILES" (
"ID" NUMBER(10) NOT NULL ,
"FILE_CONTRACT_NUMBER" VARCHAR2(256 BYTE) NULL ,
"FILE_NAME" VARCHAR2(2048 BYTE) NULL ,
"FILE_STATUS" VARCHAR2(256 BYTE) DEFAULT 1  NULL ,
"FILE_TYPE" VARCHAR2(256 BYTE) NULL ,
"CONTRACT_ID" NUMBER NULL ,
"UPLOAD_DATE" DATE NULL ,
"START_DATE" DATE NULL ,
"END_DATE" DATE NULL ,
"CONTENT" VARCHAR2(2048 BYTE) NULL ,
"DESCRIPTION" VARCHAR2(2048 BYTE) NULL ,
"PATH_FILE" VARCHAR2(2048 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_CONTRACT_FILES
-- ----------------------------
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('1', 'eee', 'ttt', '1', '1', '5', TO_DATE('2017-06-01 10:34:37', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2017-06-01 10:34:39', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2017-06-01 10:34:41', 'YYYY-MM-DD HH24:MI:SS'), 'popo', '8909', '90909');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('5', null, '20170602093246_kkasgl_14zi9k5rjym5ttq8kz1h09m1mqji4tzlxsmwjzivzfh0390d6amjq2f6n.doc', null, 'TOTRINH', null, TO_DATE('2017-06-02 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, null, 'C:\uploaded-files\20170602093246_kkasgl_14zi9k5rjym5ttq8kz1h09m1mqji4tzlxsmwjzivzfh0390d6amjq2f6n.doc');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('6', null, '20170602093249ttwzn9_v1b4buu3dkmlx8_0o7hhd6tr__jxgevwbks8ds3t0if7tkiei5oi2bahbo.docx', null, 'TOTRINH', null, TO_DATE('2017-06-02 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, null, 'C:\uploaded-files\20170602093249ttwzn9_v1b4buu3dkmlx8_0o7hhd6tr__jxgevwbks8ds3t0if7tkiei5oi2bahbo.docx');
<<<<<<< HEAD
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('3', null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('4', null, '20170602093004uu7668_7xs994al3h54d2kjng9ur8j2he7h6eljk558mq22f4nug5i8x5l4twif79.docx', null, 'TOTRINH', null, TO_DATE('2017-06-02 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, null, 'C:\uploaded-files\20170602093004uu7668_7xs994al3h54d2kjng9ur8j2he7h6eljk558mq22f4nug5i8x5l4twif79.docx');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('9', null, '20170602094747xjdb2ouhvxrgdw5ksfihrzdc5cegqvhylgcqvug0993lqgq0v0x_g0f7ltky0ktmz.docx', null, 'TOTRINH', '61', TO_DATE('2017-06-02 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, null, 'C:\uploaded-files\20170602094747xjdb2ouhvxrgdw5ksfihrzdc5cegqvhylgcqvug0993lqgq0v0x_g0f7ltky0ktmz.docx');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('21', null, 'Thiet ke chi tiet_Web_FM&RA_1496398429191.docx', null, 'TOTRINH', '62', TO_DATE('2017-06-02 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, null, 'C:\uploaded-files\Thiet ke chi tiet_Web_FM&RA_1496398429191.docx');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('2', null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('7', null, '20170602094610vihu8a8zte1mcum_7hvj3fsmegf_0dl7bfm6nf7fd1zshqle3q8zlby2flbfqd1gx.docx', null, 'TOTRINH', null, TO_DATE('2017-06-02 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, null, 'C:\uploaded-files\20170602094610vihu8a8zte1mcum_7hvj3fsmegf_0dl7bfm6nf7fd1zshqle3q8zlby2flbfqd1gx.docx');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('8', null, '20170602094747xjdb2ouhvxrgdw5ksfihrzdc5cegqvhylgcqvug0993lqgq0v0x_g0f7ltky0ktmz.docx', null, 'TOTRINH', '34', TO_DATE('2017-06-02 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, null, 'C:\uploaded-files\20170602094747xjdb2ouhvxrgdw5ksfihrzdc5cegqvhylgcqvug0993lqgq0v0x_g0f7ltky0ktmz.docx');
=======
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('62', null, 'Thiet ke chi tiet_Web_FM&RA_1496736173179.docx', null, 'TOTRINH', '161', TO_DATE('2017-06-06 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, null, 'C:\uploaded-files\Thiet ke chi tiet_Web_FM&RA_1496736173179.docx');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('135', null, 'Thiet ke co so du lieu_Viettel_CSP_1497001673600.doc', null, null, null, TO_DATE('2017-06-09 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, null, 'Thiet ke co so du lieu_Viettel_CSP_1497001673600.doc');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('138', null, 'Thiet ke co so du lieu_Viettel_CSP_1497004900087.doc', null, 'PHULUCHOPDONG', null, TO_DATE('2017-06-09 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, 'sdf', 'D:\tool\uploaded-files\Thiet ke co so du lieu_Viettel_CSP_1497004900087.doc');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('3', null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('41', null, 'CSP_R726220_PTYC[136]_1496667912718.doc', null, 'TOTRINH', '170', TO_DATE('2017-06-05 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, 'uuuu', null, 'C:\uploaded-files\CSP_R726220_PTYC[136]_1496667912718.doc');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('42', null, '20052016_TKCT_VIETTEL_CSP_DANG KY VA DANG NHAP 2.0_Outsource_v1.0_1496667972269.doc', null, 'TOTRINH', '170', TO_DATE('2017-06-05 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, 'iiii', null, 'C:\uploaded-files\20052016_TKCT_VIETTEL_CSP_DANG KY VA DANG NHAP 2.0_Outsource_v1.0_1496667972269.doc');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('43', null, '20052016_TKCT_VIETTEL_CSP_DANG KY VA DANG NHAP 2.0_Outsource_v1.0_1496667997315.doc', null, 'TOTRINH', '170', TO_DATE('2017-06-05 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, 'oooo', null, 'C:\uploaded-files\20052016_TKCT_VIETTEL_CSP_DANG KY VA DANG NHAP 2.0_Outsource_v1.0_1496667997315.doc');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('45', null, 'Thiet ke chi tiet_Web_FM&RA_1496715526231.docx', null, 'TOTRINH', '170', TO_DATE('2017-06-06 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, null, 'C:\uploaded-files\Thiet ke chi tiet_Web_FM&RA_1496715526231.docx');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('46', 'tyty', 'Thiet ke chi tiet_Web_FM&RA_1496715708952.docx', null, 'TOTRINH', '62', TO_DATE('2017-06-06 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2017-05-30 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2017-06-23 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), 'tytyt', 'tyt', 'C:\uploaded-files\Thiet ke chi tiet_Web_FM&RA_1496715708952.docx');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('47', null, 'Thiet ke chi tiet_Web_FM&RA_1496718873431.docx', null, 'HOPDONG', '161', TO_DATE('2017-06-06 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, null, 'C:\uploaded-files\Thiet ke chi tiet_Web_FM&RA_1496718873431.docx');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('61', null, 'Thiet ke chi tiet_Web_FM&RA_1496736143906.docx', null, 'TOTRINH', '141', TO_DATE('2017-06-06 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, null, 'C:\uploaded-files\Thiet ke chi tiet_Web_FM&RA_1496736143906.docx');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('64', null, 'Quản Lý Hợp Đồng(1)aaa.pdf', null, 'TOTRINH', '161', TO_DATE('2017-06-06 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, null, 'C:\uploaded-files\Quản Lý Hợp Đồng(1)aaa.pdf');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('104', null, 'Qu-n Lư H-p --ng(1)aaa_1496841537473_1496887449577.pdf', null, 'HOPDONG', '164', TO_DATE('2017-06-08 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, null, 'C:\uploaded-files\Qu-n Lư H-p --ng(1)aaa_1496841537473_1496887449577.pdf');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('127', null, 'Qu-n Lư H-p --ng(1)aaa_1496841537473_1497000587472.pdf', null, 'HOPDONG', '181', TO_DATE('2017-06-09 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, null, 'C:\uploaded-files\Qu-n Lư H-p --ng(1)aaa_1496841537473_1497000587472.pdf');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('128', null, 'C##VIETTEL_CSP_1497000983634.sql', null, null, null, TO_DATE('2017-06-09 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, null, 'C##VIETTEL_CSP_1497000983634.sql');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('129', null, 'Thiet ke co so du lieu_Viettel_CSP_1497000983635.doc', null, null, null, TO_DATE('2017-06-09 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, null, 'Thiet ke co so du lieu_Viettel_CSP_1497000983635.doc');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('130', null, 'VIETTEL_CSP_1497000983635.sql', null, null, null, TO_DATE('2017-06-09 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, null, 'VIETTEL_CSP_1497000983635.sql');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('131', null, 'C##VIETTEL_CSP_1497000983634.sql', null, null, null, TO_DATE('2017-06-09 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, null, 'C##VIETTEL_CSP_1497000983634.sql');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('132', null, 'Thiet ke co so du lieu_Viettel_CSP_1497000983635.doc', null, null, null, TO_DATE('2017-06-09 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, null, 'Thiet ke co so du lieu_Viettel_CSP_1497000983635.doc');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('133', null, 'VIETTEL_CSP_1497000983635.sql', null, null, null, TO_DATE('2017-06-09 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, null, 'VIETTEL_CSP_1497000983635.sql');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('134', null, 'Thiet ke co so du lieu_Viettel_CSP_1497000983635.doc', null, null, null, TO_DATE('2017-06-09 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, null, 'Thiet ke co so du lieu_Viettel_CSP_1497000983635.doc');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('144', null, 'Thiet ke co so du lieu_Viettel_CSP_1497067386973.doc', null, 'PHULUCHOPDONG', '172', TO_DATE('2017-06-10 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, 'sdfsd', 'D:\tool\uploaded-files\Thiet ke co so du lieu_Viettel_CSP_1497067386973.doc');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('150', null, 'Thiet ke co so du lieu_Viettel_CSP_1497069370444.doc', null, 'PHULUCHOPDONG', '172', TO_DATE('2017-06-10 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, '345345345', 'D:\tool\uploaded-files\Thiet ke co so du lieu_Viettel_CSP_1497069370444.doc');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('4', null, '20170602093004uu7668_7xs994al3h54d2kjng9ur8j2he7h6eljk558mq22f4nug5i8x5l4twif79.docx', null, 'TOTRINH', null, TO_DATE('2017-06-02 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, null, 'C:\uploaded-files\20170602093004uu7668_7xs994al3h54d2kjng9ur8j2he7h6eljk558mq22f4nug5i8x5l4twif79.docx');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('9', null, '20170602094747xjdb2ouhvxrgdw5ksfihrzdc5cegqvhylgcqvug0993lqgq0v0x_g0f7ltky0ktmz.docx', null, 'TOTRINH', '61', TO_DATE('2017-06-02 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, null, 'C:\uploaded-files\20170602094747xjdb2ouhvxrgdw5ksfihrzdc5cegqvhylgcqvug0993lqgq0v0x_g0f7ltky0ktmz.docx');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('21', null, 'Thiet ke chi tiet_Web_FM&RA_1496398429191.docx', null, 'TOTRINH', '62', TO_DATE('2017-06-02 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, null, 'C:\uploaded-files\Thiet ke chi tiet_Web_FM&RA_1496398429191.docx');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('65', null, 'Thiet ke chi tiet_Web_FM&RA_1496738055448.docx', null, 'TOTRINH', '141', TO_DATE('2017-06-06 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, null, 'C:\uploaded-files\Thiet ke chi tiet_Web_FM&RA_1496738055448.docx');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('66', null, 'Thiet ke chi tiet_Web_FM&RA_1496738074250.docx', null, 'TOTRINH', '168', TO_DATE('2017-06-06 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, null, 'C:\uploaded-files\Thiet ke chi tiet_Web_FM&RA_1496738074250.docx');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('81', null, '20052016_TKCT_VIETTEL_CSP_DANG KY VA DANG NHAP 2.0_Outsource_v1.0_1496802445704.doc', null, 'TOTRINH', '161', TO_DATE('2017-06-07 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, null, 'C:\uploaded-files\20052016_TKCT_VIETTEL_CSP_DANG KY VA DANG NHAP 2.0_Outsource_v1.0_1496802445704.doc');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('102', null, 'Quản Lý Hợp Đồng(1)aaa_1496841537473.pdf', null, null, '164', TO_DATE('2017-06-07 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, null, 'C:\uploaded-files\Quản Lý Hợp Đồng(1)aaa_1496841537473.pdf');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('103', null, 'Qu-n Lư H-p --ng(1)aaa_1496841537473_1496887315681.pdf', null, 'HOPDONG', '164', TO_DATE('2017-06-08 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, null, 'C:\uploaded-files\Qu-n Lư H-p --ng(1)aaa_1496841537473_1496887315681.pdf');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('122', null, 'Qu-n Lư H-p --ng(1)aaa_1496841537473_1496980916838.pdf', null, 'HOSO', '162', TO_DATE('2017-06-09 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, null, 'C:\uploaded-files\Qu-n Lư H-p --ng(1)aaa_1496841537473_1496980916838.pdf');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('123', '534', 'Qu-n Lư H-p --ng(1)aaa_1496841537473_1496983630813.pdf', null, null, '221', TO_DATE('2017-06-09 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2017-05-29 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2017-06-23 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), 'eter', null, 'C:\uploaded-files\Qu-n Lư H-p --ng(1)aaa_1496841537473_1496983630813.pdf');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('124', '456', 'Qu-n Lư H-p --ng(1)aaa_1496841537473_1496983705682.pdf', null, null, '221', TO_DATE('2017-06-09 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2017-06-06 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2017-06-23 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), 'ryrt', null, 'C:\uploaded-files\Qu-n Lư H-p --ng(1)aaa_1496841537473_1496983705682.pdf');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('182', null, 'Thiet ke co so du lieu_Viettel_CSP_1497234241693.doc', null, 'PHULUCHOPDONG', '172', TO_DATE('2017-06-12 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, 'sdfsdf', 'C:\Users\admin\.IntelliJIdea2016.3\system\tomcat\uploaded-files\Thiet ke co so du lieu_Viettel_CSP_1497234241693.doc');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('146', null, 'Thiet ke co so du lieu_Viettel_CSP_1497068494429.doc', null, 'PHULUCHOPDONG', '172', TO_DATE('2017-06-10 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, 'BBBBBB', 'D:\tool\uploaded-files\Thiet ke co so du lieu_Viettel_CSP_1497068494429.doc');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('148', null, 'Thiet ke co so du lieu_Viettel_CSP_1497068718335.doc', null, 'PHULUCHOPDONG', '172', TO_DATE('2017-06-10 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, 'BBBBBB', 'D:\tool\uploaded-files\Thiet ke co so du lieu_Viettel_CSP_1497068718335.doc');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('161', null, 'Thiet ke co so du lieu_Viettel_CSP_1497089884266.doc', null, 'PHULUCHOPDONG', '172', TO_DATE('2017-06-10 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, 'dfg', 'C:\Users\admin\.IntelliJIdea2016.3\system\tomcat\uploaded-files\Thiet ke co so du lieu_Viettel_CSP_1497089884266.doc');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('181', null, 'Thiet ke co so du lieu_Viettel_CSP_1497234220214.doc', null, 'HOPDONG', '172', TO_DATE('2017-06-12 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, null, 'C:\Users\admin\.IntelliJIdea2016.3\system\tomcat\uploaded-files\Thiet ke co so du lieu_Viettel_CSP_1497234220214.doc');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('141', null, 'C:\uploaded-files\Qu-n Lư H-p --ng(1)aaa_1496841537473_1496887315681.pdf', null, 'HOPDONG', '172', TO_DATE('2017-06-10 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, null, 'D:\tool\uploaded-files\Thiet ke co so du lieu_Viettel_CSP_1496654057322.doc');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('44', null, 'Thiet ke chi tiet_Web_FM&RA_1496671606481.docx', null, 'TOTRINH', '170', TO_DATE('2017-06-05 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, null, 'C:\uploaded-files\Thiet ke chi tiet_Web_FM&RA_1496671606481.docx');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('63', null, 'Thiet ke chi tiet_Web_FM&RA_1496736349339.docx', null, 'TOTRINH', '164', TO_DATE('2017-06-06 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, null, 'C:\uploaded-files\Thiet ke chi tiet_Web_FM&RA_1496736349339.docx');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('84', null, '20052016_TKCT_VIETTEL_CSP_DANG KY VA DANG NHAP 2.0_Outsource_v1.0_1496804713368.doc', null, 'TOTRINH', '162', TO_DATE('2017-06-07 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, 'iiii', 'C:\uploaded-files\20052016_TKCT_VIETTEL_CSP_DANG KY VA DANG NHAP 2.0_Outsource_v1.0_1496804713368.doc');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('85', null, 'Thiet ke chi tiet_Web_FM&RA_1496804720157.docx', null, 'TOTRINH', '162', TO_DATE('2017-06-07 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, 'tytyitiu', 'C:\uploaded-files\Thiet ke chi tiet_Web_FM&RA_1496804720157.docx');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('87', null, 'Quản Lý Hợp Đồng(1)aaa_1496823736956.pdf', null, 'HOSO', '171', TO_DATE('2017-06-07 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, 'hhhh', 'C:\uploaded-files\Quản Lý Hợp Đồng(1)aaa_1496823736956.pdf');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('88', 'iiiii', 'Quản Lý Hợp Đồng(1)aaa_1496824155220.pdf', null, null, '170', TO_DATE('2017-06-07 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2017-05-29 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2017-06-23 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), 'tyut', 'tyutyutyuty', 'C:\uploaded-files\Quản Lý Hợp Đồng(1)aaa_1496824155220.pdf');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('105', null, 'Qu-n Lư H-p --ng(1)aaa_1496841537473_1496887659259.pdf', null, 'HOPDONG', '164', TO_DATE('2017-06-08 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, null, 'C:\uploaded-files\Qu-n Lư H-p --ng(1)aaa_1496841537473_1496887659259.pdf');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('136', null, 'Thiet ke co so du lieu_Viettel_CSP_1497001885981.doc', null, null, '172', TO_DATE('2017-06-09 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, null, 'Thiet ke co so du lieu_Viettel_CSP_1497001885981.doc');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('142', null, 'Thiet ke co so du lieu_Viettel_CSP_1497066500515.doc', null, 'PHULUCHOPDONG', '172', TO_DATE('2017-06-10 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, 'sadasd', 'D:\tool\uploaded-files\Thiet ke co so du lieu_Viettel_CSP_1497066500515.doc');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('7', null, '20170602094610vihu8a8zte1mcum_7hvj3fsmegf_0dl7bfm6nf7fd1zshqle3q8zlby2flbfqd1gx.docx', null, 'TOTRINH', null, TO_DATE('2017-06-02 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, null, 'C:\uploaded-files\20170602094610vihu8a8zte1mcum_7hvj3fsmegf_0dl7bfm6nf7fd1zshqle3q8zlby2flbfqd1gx.docx');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('8', null, '20170602094747xjdb2ouhvxrgdw5ksfihrzdc5cegqvhylgcqvug0993lqgq0v0x_g0f7ltky0ktmz.docx', null, 'TOTRINH', '34', TO_DATE('2017-06-02 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, null, 'C:\uploaded-files\20170602094747xjdb2ouhvxrgdw5ksfihrzdc5cegqvhylgcqvug0993lqgq0v0x_g0f7ltky0ktmz.docx');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('82', null, '20052016_TKCT_VIETTEL_CSP_DANG KY VA DANG NHAP 2.0_Outsource_v1.0_1496803118298.doc', null, 'HOPDONG', '161', TO_DATE('2017-06-07 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, null, 'C:\uploaded-files\20052016_TKCT_VIETTEL_CSP_DANG KY VA DANG NHAP 2.0_Outsource_v1.0_1496803118298.doc');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('83', null, 'CSP_R726220_PTYC[136]_1496803131781.doc', null, 'TOTRINH', '171', TO_DATE('2017-06-07 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, null, 'C:\uploaded-files\CSP_R726220_PTYC[136]_1496803131781.doc');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('101', '4123', 'Quản Lý Hợp Đồng(1)aaa_1496833538564.pdf', null, null, '181', TO_DATE('2017-06-07 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2017-05-31 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2017-06-23 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), 'tyuỷtu', null, 'C:\uploaded-files\Quản Lý Hợp Đồng(1)aaa_1496833538564.pdf');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('121', null, 'Qu-n Lư H-p --ng(1)aaa_1496841537473_1496980417213.pdf', null, 'HOSO', '162', TO_DATE('2017-06-09 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, null, 'C:\uploaded-files\Qu-n Lư H-p --ng(1)aaa_1496841537473_1496980417213.pdf');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('125', null, 'Qu-n Lư H-p --ng(1)aaa_1496841537473_1496990695479.pdf', null, null, '172', TO_DATE('2017-06-09 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, null, 'C:\uploaded-files\Qu-n Lư H-p --ng(1)aaa_1496841537473_1496990695479.pdf');
INSERT INTO "TBL_CONTRACT_FILES" VALUES ('126', null, 'Qu-n Lư H-p --ng(1)aaa_1496841537473_1496990850859.pdf', null, null, '164', TO_DATE('2017-06-09 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, null, 'C:\uploaded-files\Qu-n Lư H-p --ng(1)aaa_1496841537473_1496990850859.pdf');
>>>>>>> 4f1464f3ea01ff1d3e2fb0f514f6fda65674f3f6

-- ----------------------------
-- Table structure for TBL_CONTRACT_HIS
-- ----------------------------
DROP TABLE "TBL_CONTRACT_HIS";
CREATE TABLE "TBL_CONTRACT_HIS" (
"ID" NUMBER(10) NOT NULL ,
"CONTRACT_ID" NUMBER NULL ,
"CREATE_BY" NUMBER(10) DEFAULT NULL  NULL ,
"CREATE_AT" TIMESTAMP(6)  DEFAULT NULL  NULL ,
"STATUS_BEFORE" VARCHAR2(256 BYTE) NULL ,
"STATUS_AFTER" VARCHAR2(256 BYTE) NULL ,
"CONTENT" VARCHAR2(2048 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_CONTRACT_HIS
-- ----------------------------
INSERT INTO "TBL_CONTRACT_HIS" VALUES ('1', '5', '24', TO_TIMESTAMP(' 2017-05-30 00:00:00:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'), '8', '9', 'hhh');
INSERT INTO "TBL_CONTRACT_HIS" VALUES ('41', '42', '24', TO_TIMESTAMP(' 2017-06-01 00:00:00:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'), '8', '9', 'qwe Hồ sơ điền sai thông tin');
INSERT INTO "TBL_CONTRACT_HIS" VALUES ('23', '101', '24', TO_TIMESTAMP(' 2017-05-31 00:00:00:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'), '8', '9', 'hh');
INSERT INTO "TBL_CONTRACT_HIS" VALUES ('24', '42', '24', TO_TIMESTAMP(' 2017-05-31 00:00:00:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'), '8', '9', 'hh');
INSERT INTO "TBL_CONTRACT_HIS" VALUES ('25', '6', '24', TO_TIMESTAMP(' 2017-05-31 00:00:00:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'), '8', '9', 'hhh');
INSERT INTO "TBL_CONTRACT_HIS" VALUES ('26', '101', '24', TO_TIMESTAMP(' 2017-05-31 00:00:00:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'), '8', '9', 'hhh');
INSERT INTO "TBL_CONTRACT_HIS" VALUES ('22', '61', '24', TO_TIMESTAMP(' 2017-05-31 00:00:00:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'), '8', '9', 'hjhh');
<<<<<<< HEAD
INSERT INTO "TBL_CONTRACT_HIS" VALUES ('2', '5', '24', TO_TIMESTAMP(' 2017-05-31 00:00:00:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'), '8', '9', 'hhh');
INSERT INTO "TBL_CONTRACT_HIS" VALUES ('21', '6', '24', TO_TIMESTAMP(' 2017-05-31 00:00:00:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'), '8', '9', 'hh');
=======
INSERT INTO "TBL_CONTRACT_HIS" VALUES ('61', '172', '43', TO_TIMESTAMP(' 2017-06-07 00:00:00:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'), '8', '9', 'Thay đổi trạng thái tiếp nhận công việc');
INSERT INTO "TBL_CONTRACT_HIS" VALUES ('64', '161', '43', TO_TIMESTAMP(' 2017-06-07 00:00:00:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'), '8', '9', 'Thay đổi trạng thái tiếp nhận công việc');
INSERT INTO "TBL_CONTRACT_HIS" VALUES ('2', '5', '24', TO_TIMESTAMP(' 2017-05-31 00:00:00:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'), '8', '9', 'hhh');
INSERT INTO "TBL_CONTRACT_HIS" VALUES ('81', '164', '21', TO_TIMESTAMP(' 2017-06-08 00:00:00:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'), '8', '9', '-Hồ sơ điền sai thông tin');
INSERT INTO "TBL_CONTRACT_HIS" VALUES ('101', '173', '43', TO_TIMESTAMP(' 2017-06-08 00:00:00:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'), '8', '9', 'Thay đổi trạng thái tiếp nhận công việc');
INSERT INTO "TBL_CONTRACT_HIS" VALUES ('21', '6', '24', TO_TIMESTAMP(' 2017-05-31 00:00:00:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'), '8', '9', 'hh');
INSERT INTO "TBL_CONTRACT_HIS" VALUES ('62', '171', '43', TO_TIMESTAMP(' 2017-06-07 00:00:00:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'), '8', '9', 'Thay đổi trạng thái tiếp nhận công việc');
INSERT INTO "TBL_CONTRACT_HIS" VALUES ('63', '170', '43', TO_TIMESTAMP(' 2017-06-07 00:00:00:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'), '8', '9', 'Thay đổi trạng thái tiếp nhận công việc');
INSERT INTO "TBL_CONTRACT_HIS" VALUES ('121', '162', '45', TO_TIMESTAMP(' 2017-06-09 00:00:00:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'), '8', '9', 'q23-Hồ sơ điền sai thông tin');
>>>>>>> 4f1464f3ea01ff1d3e2fb0f514f6fda65674f3f6

-- ----------------------------
-- Table structure for TBL_CONTRACT_PAUSE
-- ----------------------------
DROP TABLE "TBL_CONTRACT_PAUSE";
CREATE TABLE "TBL_CONTRACT_PAUSE" (
"ID" NUMBER(10) NOT NULL ,
"CONTRACT_ID" NUMBER NULL ,
"CREATE_AT" DATE NULL ,
"CREATE_BY" NUMBER NULL ,
"UPDATE_AT" DATE NULL ,
"UPDATE_BY" NUMBER NULL ,
"START_DATE" DATE NULL ,
"END_DATE" DATE NULL ,
"REASON_CODE" VARCHAR2(256 BYTE) NULL ,
"FILE_PATH" VARCHAR2(2048 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_CONTRACT_PAUSE
-- ----------------------------

-- ----------------------------
-- Table structure for TBL_CONTRACT_PAUSE_FILES
-- ----------------------------
DROP TABLE "TBL_CONTRACT_PAUSE_FILES";
CREATE TABLE "TBL_CONTRACT_PAUSE_FILES" (
"ID" NUMBER(10) NOT NULL ,
"FILE_NAME" VARCHAR2(2048 BYTE) NULL ,
"DESCRIPTION" VARCHAR2(2048 BYTE) NULL ,
"CONTRACT_PAUSE_ID" NUMBER NULL ,
"UPLOAD_DATE" DATE NULL ,
"PATH_FILE" VARCHAR2(1024 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_CONTRACT_PAUSE_FILES
-- ----------------------------

-- ----------------------------
-- Table structure for TBL_CONTRACT_STATUS
-- ----------------------------
DROP TABLE "TBL_CONTRACT_STATUS";
CREATE TABLE "TBL_CONTRACT_STATUS" (
"CODE" VARCHAR2(256 BYTE) NOT NULL ,
"NAME" VARCHAR2(256 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_CONTRACT_STATUS
-- ----------------------------
<<<<<<< HEAD
INSERT INTO "TBL_CONTRACT_STATUS" VALUES ('8', 'Từ chối hồ sơ');
INSERT INTO "TBL_CONTRACT_STATUS" VALUES ('9', 'Duyệt hồ sơ');
INSERT INTO "TBL_CONTRACT_STATUS" VALUES ('10', 'Đã duyệt tờ trình');
INSERT INTO "TBL_CONTRACT_STATUS" VALUES ('1', 'Đang tạo hồ sơ');
INSERT INTO "TBL_CONTRACT_STATUS" VALUES ('2', 'Đã nộp hồ sơ - Hồ sơ đang chờ duyệt');
INSERT INTO "TBL_CONTRACT_STATUS" VALUES ('3', 'Hồ sơ không hợp lệ');
INSERT INTO "TBL_CONTRACT_STATUS" VALUES ('4', 'Đang chờ phê duyệt tờ trình');
=======
INSERT INTO "TBL_CONTRACT_STATUS" VALUES ('11', 'Đã gửi HĐ cho đối tác');
INSERT INTO "TBL_CONTRACT_STATUS" VALUES ('26', 'Có hiệu lực');
INSERT INTO "TBL_CONTRACT_STATUS" VALUES ('27', 'Tạm dừng HĐ');
INSERT INTO "TBL_CONTRACT_STATUS" VALUES ('8', 'Từ chối hồ sơ');
INSERT INTO "TBL_CONTRACT_STATUS" VALUES ('9', 'Duyệt hồ sơ');
INSERT INTO "TBL_CONTRACT_STATUS" VALUES ('25', 'Đồng ý tiếp nhận công việc');
INSERT INTO "TBL_CONTRACT_STATUS" VALUES ('10', 'Duyệt tờ trình');
INSERT INTO "TBL_CONTRACT_STATUS" VALUES ('12', 'Gửi HĐ cho Viettel');
INSERT INTO "TBL_CONTRACT_STATUS" VALUES ('13', 'Trình phê duyệt HĐ');
INSERT INTO "TBL_CONTRACT_STATUS" VALUES ('14', 'Từ chối phê duyệt HĐ');
INSERT INTO "TBL_CONTRACT_STATUS" VALUES ('15', 'Trình ký HĐ');
INSERT INTO "TBL_CONTRACT_STATUS" VALUES ('16', 'Từ chối ký HĐ');
INSERT INTO "TBL_CONTRACT_STATUS" VALUES ('17', 'Đã ký HĐ');
INSERT INTO "TBL_CONTRACT_STATUS" VALUES ('18', 'Gửi HĐ đã ký cho đối tác');
INSERT INTO "TBL_CONTRACT_STATUS" VALUES ('19', 'Đối tác đã ký HĐ');
INSERT INTO "TBL_CONTRACT_STATUS" VALUES ('20', 'Đối tác từ chối Ký HĐ');
INSERT INTO "TBL_CONTRACT_STATUS" VALUES ('21', 'Lưu và triển khai HĐ');
INSERT INTO "TBL_CONTRACT_STATUS" VALUES ('22', 'Hủy bỏ Hồ sơ dự thảo');
INSERT INTO "TBL_CONTRACT_STATUS" VALUES ('23', 'Hủy bỏ HĐ dự thảo');
INSERT INTO "TBL_CONTRACT_STATUS" VALUES ('24', 'Từ chối tiếp nhận công việc');
INSERT INTO "TBL_CONTRACT_STATUS" VALUES ('1', 'Đang tạo hồ sơ');
INSERT INTO "TBL_CONTRACT_STATUS" VALUES ('2', 'Đang chờ duyệt hồ sơ');
INSERT INTO "TBL_CONTRACT_STATUS" VALUES ('3', 'Hồ sơ không hợp lệ');
INSERT INTO "TBL_CONTRACT_STATUS" VALUES ('4', 'Đang chờ duyệt tờ trình');
>>>>>>> 4f1464f3ea01ff1d3e2fb0f514f6fda65674f3f6
INSERT INTO "TBL_CONTRACT_STATUS" VALUES ('5', 'Từ chối ký tờ trình');
INSERT INTO "TBL_CONTRACT_STATUS" VALUES ('6', 'Ký tờ trình');
INSERT INTO "TBL_CONTRACT_STATUS" VALUES ('7', 'Tạo hợp đồng dự thảo');

-- ----------------------------
-- Table structure for TBL_CONTRACT_TYPE
-- ----------------------------
DROP TABLE "TBL_CONTRACT_TYPE";
CREATE TABLE "TBL_CONTRACT_TYPE" (
"CODE" VARCHAR2(256 BYTE) NOT NULL ,
"NAME" VARCHAR2(256 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_CONTRACT_TYPE
-- ----------------------------
INSERT INTO "TBL_CONTRACT_TYPE" VALUES ('CONTRACTYPE_ONE', 'CONTRACTYPE_ONE');
INSERT INTO "TBL_CONTRACT_TYPE" VALUES ('CONTRACTYPE_TWO', 'CONTRACTYPE_TWO');
INSERT INTO "TBL_CONTRACT_TYPE" VALUES ('EDIT', 'Được sửa');
INSERT INTO "TBL_CONTRACT_TYPE" VALUES ('NO_EDIT', 'Không được sửa');

-- ----------------------------
-- Table structure for TBL_DEPARTMENT
-- ----------------------------
DROP TABLE "TBL_DEPARTMENT";
CREATE TABLE "TBL_DEPARTMENT" (
"ORGANIZATIONID" NUMBER(10) NULL ,
"CODE" VARCHAR2(256 BYTE) NULL ,
"NAME" VARCHAR2(1024 BYTE) NULL ,
"ORGPARENTID" NUMBER(10) NULL ,
"ISACTIVE" VARCHAR2(10 BYTE) NULL ,
"PATH" VARCHAR2(1024 BYTE) NULL ,
"ID" NUMBER NOT NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_DEPARTMENT
-- ----------------------------
INSERT INTO "TBL_DEPARTMENT" VALUES ('123465', '011', 'Phòng Kinh Doanh VIETTEL Hà Nội', '123464', '1', '/148842/123450/123461/123464/123465', '1');
INSERT INTO "TBL_DEPARTMENT" VALUES ('123466', '012', 'Trung tâm Chăm sóc Khách hàng Viettel', '123450', '1', '/148842/123450/123466', '2');
INSERT INTO "TBL_DEPARTMENT" VALUES ('148842', '001', 'Tập đoàn Viễn thông Quân đội', null, '1', '/148842/', '3');
INSERT INTO "TBL_DEPARTMENT" VALUES ('123450', '002', 'Khối đơn vị Hạch toán phụ thuộc', '148842', '1', '/148842/123450/', '4');
INSERT INTO "TBL_DEPARTMENT" VALUES ('123451', '003', 'Tổng công ty Viễn thông Viettel', '123450', '1', '/148842/123450/123451/', '5');
INSERT INTO "TBL_DEPARTMENT" VALUES ('123452', '004', 'Khối cơ quan Tổng công ty VTT', '123451', '1', '/148842/123450/123451/123452/', '6');
INSERT INTO "TBL_DEPARTMENT" VALUES ('123453', '005', 'Phòng Quản lý hợp tác CSP', '123452', '1', '/148842/123450/123451/123452/123453/', '7');
INSERT INTO "TBL_DEPARTMENT" VALUES ('123454', '006', 'Phòng Kiểm soát nội bộ', '123453', '1', '/148842/123450/123451/123452/123454/', '8');
INSERT INTO "TBL_DEPARTMENT" VALUES ('123461', '007', 'Khối Chi nhánh tỉnh/tp', '123450', '1', '/148842/123450/123461/', '9');
INSERT INTO "TBL_DEPARTMENT" VALUES ('123462', '008', 'VIETTEL Hà Nội', '123461', '1', '/148842/123450/123461/123462/', '10');
INSERT INTO "TBL_DEPARTMENT" VALUES ('123463', '010', 'VIETTEL TP.Hồ Chí Minh', '123461', '1', '/148842/123450/123461/123463/', '11');
INSERT INTO "TBL_DEPARTMENT" VALUES ('123464', '009', 'VIETTEL Đà Nẵng', '123461', '1', '/148842/123450/123461/123464/', '12');
INSERT INTO "TBL_DEPARTMENT" VALUES ('148843', '13', 'ROOT2', '148842', '1', '/148842/---/', '21');
INSERT INTO "TBL_DEPARTMENT" VALUES ('148844', '14', 'ROOT2', '148842', '1', '/148842/---/', '22');
INSERT INTO "TBL_DEPARTMENT" VALUES ('148845', '15', 'ROOT2', '148842', '1', '/148842/---/', '23');

-- ----------------------------
-- Table structure for TBL_DISCIPLINE
-- ----------------------------
DROP TABLE "TBL_DISCIPLINE";
CREATE TABLE "TBL_DISCIPLINE" (
"CODE" VARCHAR2(256 BYTE) NOT NULL ,
"NAME" VARCHAR2(256 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_DISCIPLINE
-- ----------------------------

-- ----------------------------
-- Table structure for TBL_FILE_TYPE
-- ----------------------------
DROP TABLE "TBL_FILE_TYPE";
CREATE TABLE "TBL_FILE_TYPE" (
"CODE" VARCHAR2(256 BYTE) NOT NULL ,
"NAME" VARCHAR2(256 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_FILE_TYPE
-- ----------------------------
INSERT INTO "TBL_FILE_TYPE" VALUES ('TOTRINH_HOSO', 'Tờ trình HĐ');
INSERT INTO "TBL_FILE_TYPE" VALUES ('TOTRINH_HOPDONG', 'Tờ trình Hồ sơ');
INSERT INTO "TBL_FILE_TYPE" VALUES ('HOPDONG', 'Hợp đồng');
INSERT INTO "TBL_FILE_TYPE" VALUES ('HOSO', 'Hồ sơ');
INSERT INTO "TBL_FILE_TYPE" VALUES ('PHULUC', 'Phụ lục');

-- ----------------------------
-- Table structure for TBL_LOCATION
-- ----------------------------
DROP TABLE "TBL_LOCATION";
CREATE TABLE "TBL_LOCATION" (
"PROVINCE_CODE" VARCHAR2(256 BYTE) NULL ,
"PROVINCE_NAME" VARCHAR2(256 BYTE) NULL ,
"DISTRICT_CODE" VARCHAR2(256 BYTE) NULL ,
"DISTRICT_NAME" VARCHAR2(256 BYTE) NULL ,
"VILLAGE_CODE" VARCHAR2(256 BYTE) NULL ,
"VILLAGE_NAME" VARCHAR2(256 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_LOCATION
-- ----------------------------
INSERT INTO "TBL_LOCATION" VALUES ('001', 'An Giang', '001001', 'Huyện An Phú
', '001001001', 'Xã Quốc Thái
');
INSERT INTO "TBL_LOCATION" VALUES ('001', 'An Giang', '001001', 'Huyện An Phú
', '001001002', 'Phường Trung Tâm');
INSERT INTO "TBL_LOCATION" VALUES ('001', 'An Giang', '001001', 'Huyện An Phú
', '001001003', 'Xã Phú Thọ');
INSERT INTO "TBL_LOCATION" VALUES ('001', 'An Giang', '001001', 'Huyện An Phú
', '001001004', 'Xã Long Giang
');
INSERT INTO "TBL_LOCATION" VALUES ('001', 'An Giang', '001001', 'Huyện An Phú
', '001001005', 'Xã Vĩnh Lợi
');
INSERT INTO "TBL_LOCATION" VALUES ('001', 'An Giang', '001002', 'Huyện Châu Phú
', '001002001', 'Xã Vĩnh Nhuận
');
INSERT INTO "TBL_LOCATION" VALUES ('001', 'An Giang', '001002', 'Huyện Châu Phú

', '001002002', 'Xã An Hòa');
INSERT INTO "TBL_LOCATION" VALUES ('001', 'An Giang', '001002', 'Huyện Châu Phú
', '001002003', 'Xã Bình Thạnh');
INSERT INTO "TBL_LOCATION" VALUES ('001', 'An Giang', '001003', 'Huyện Châu Thành
', '001003001', 'Xã Hòa Bình Thạnh');
INSERT INTO "TBL_LOCATION" VALUES ('001', 'An Giang', '001003', 'Huyện Châu Thành
', '001003002', 'Xã Bình Hòa
');
INSERT INTO "TBL_LOCATION" VALUES ('002', 'Hà Nội
', '002001', 'Huyện Hoài Đức
', '002001001', 'Xã Tiền Yên

');
INSERT INTO "TBL_LOCATION" VALUES ('002', 'Hà Nội
', '002001', 'Huyện Hoài Đức
', '002001002', 'Xã An Thượng
');
INSERT INTO "TBL_LOCATION" VALUES ('002', 'Hà Nội
', '002002', 'Huyện Mê Linh', '002002001', 'Xã Tự Lập
');
INSERT INTO "TBL_LOCATION" VALUES ('002', 'Hà Nội
', '002002', 'Huyện Mê Linh
', '002002002', 'Xã Liên Mạc
');
INSERT INTO "TBL_LOCATION" VALUES ('003', 'Hải Dương
', '003001', 'Huyện Ninh Giang
', '003001001', 'Thị trấn Ninh Giang
');
INSERT INTO "TBL_LOCATION" VALUES ('002', 'Hà Nội
', '002002', 'Huyện Mê Linh
', '002002003', 'Xã Chu Phan
');
INSERT INTO "TBL_LOCATION" VALUES ('002', 'Hà Nội
', '002002', 'Huyện Mê Linh
', '002002004', 'Xã Mê Linh

');
INSERT INTO "TBL_LOCATION" VALUES ('003', 'Hải Dương
', '003001', 'Huyện Ninh Giang
', '003001002', 'Xã ứng Hoè
');
INSERT INTO "TBL_LOCATION" VALUES ('003', 'Hải Dương
', '003001', 'Huyện Ninh Giang
', '003001003', 'Xã Hồng Đức
');

-- ----------------------------
-- Table structure for TBL_MESSAGE
-- ----------------------------
DROP TABLE "TBL_MESSAGE";
CREATE TABLE "TBL_MESSAGE" (
"ID" NUMBER(10) NOT NULL ,
"SEND_USER_ID" NUMBER NULL ,
"RECEIVE_USER_ID" NUMBER NULL ,
"CONTENT" VARCHAR2(2048 BYTE) NULL ,
"DESCRIPTION" VARCHAR2(2048 BYTE) NULL ,
"CREATE_BY" NUMBER NULL ,
"CREATE_AT" DATE NULL ,
"STATUS" NUMBER(10) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_MESSAGE
-- ----------------------------
INSERT INTO "TBL_MESSAGE" VALUES ('21', '24', '22', 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', '22', TO_DATE('2017-05-23 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '1');
<<<<<<< HEAD
INSERT INTO "TBL_MESSAGE" VALUES ('44', '26', '22', 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', '22', TO_DATE('2017-05-23 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '0');
INSERT INTO "TBL_MESSAGE" VALUES ('45', '26', '22', 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', '22', TO_DATE('2017-05-23 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '1');
=======
INSERT INTO "TBL_MESSAGE" VALUES ('44', '26', '26', 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', '22', TO_DATE('2017-05-23 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '1');
INSERT INTO "TBL_MESSAGE" VALUES ('45', '26', '26', 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', '22', TO_DATE('2017-05-23 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '1');
>>>>>>> 4f1464f3ea01ff1d3e2fb0f514f6fda65674f3f6
INSERT INTO "TBL_MESSAGE" VALUES ('121', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-24 06:59:44', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('122', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-24 07:00:01', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('144', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-24 19:24:48', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('203', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-27 18:41:45', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('207', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-27 19:11:29', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('208', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-27 19:13:58', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('214', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-27 21:37:57', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('216', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-27 21:50:08', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('253', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-28 01:20:00', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('254', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-28 01:20:17', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('255', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-28 01:20:25', 'YYYY-MM-DD HH24:MI:SS'), null);
<<<<<<< HEAD
=======
INSERT INTO "TBL_MESSAGE" VALUES ('321', null, null, 'Từ chối công việc', 'Từ chối công việc', null, TO_DATE('2017-06-07 21:49:16', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('322', null, null, 'Từ chối công việc', 'Từ chối công việc', null, TO_DATE('2017-06-07 21:52:34', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('362', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-06-10 22:28:16', 'YYYY-MM-DD HH24:MI:SS'), null);
>>>>>>> 4f1464f3ea01ff1d3e2fb0f514f6fda65674f3f6
INSERT INTO "TBL_MESSAGE" VALUES ('47', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-23 23:45:18', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('48', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-23 23:47:05', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('49', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-23 23:47:51', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('50', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-23 23:49:27', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('51', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-23 23:49:53', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('52', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-23 23:51:38', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('54', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-23 23:55:05', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('142', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-24 19:12:09', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('143', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-24 19:13:18', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('161', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-25 02:54:47', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('162', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-25 02:54:54', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('163', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-25 02:55:23', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('164', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-25 02:56:10', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('165', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-25 03:01:48', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('166', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-25 03:01:54', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('181', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-26 19:15:35', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('184', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-26 19:17:03', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('185', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-26 19:17:43', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('186', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-26 19:18:07', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('201', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-27 18:41:13', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('209', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-27 19:21:17', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('211', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-27 19:29:25', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('212', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-27 19:29:42', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('213', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-27 21:37:50', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('222', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-27 21:53:15', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('225', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-27 21:53:50', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('227', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-27 21:54:08', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('228', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-27 21:54:19', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('229', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-27 21:54:38', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('230', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-27 21:58:09', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('231', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-27 21:58:44', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('232', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-27 21:59:09', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('233', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-27 21:59:55', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('234', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-27 22:12:12', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('235', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-27 22:18:29', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('237', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-27 22:20:25', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('238', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-27 22:21:38', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('239', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-27 22:21:46', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('240', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-27 22:21:49', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('246', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-27 22:28:08', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('247', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-27 22:28:17', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('248', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-27 22:28:41', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('249', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-27 22:30:27', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('281', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-30 16:48:33', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('282', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-30 16:48:44', 'YYYY-MM-DD HH24:MI:SS'), null);
<<<<<<< HEAD
=======
INSERT INTO "TBL_MESSAGE" VALUES ('325', null, null, 'Từ chối công việc', 'Từ chối công việc', null, TO_DATE('2017-06-07 23:15:26', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('326', null, null, 'Từ chối công việc', 'Từ chối công việc', null, TO_DATE('2017-06-07 23:17:40', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('327', null, null, 'Từ chối công việc', 'Từ chối công việc', null, TO_DATE('2017-06-07 23:18:34', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('331', null, null, 'Từ chối công việc', 'Từ chối công việc', null, TO_DATE('2017-06-07 23:50:12', 'YYYY-MM-DD HH24:MI:SS'), null);
>>>>>>> 4f1464f3ea01ff1d3e2fb0f514f6fda65674f3f6
INSERT INTO "TBL_MESSAGE" VALUES ('1', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-23 03:05:50', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('2', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-23 03:26:30', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('3', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-23 03:28:16', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('4', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-23 03:30:40', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('8', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-23 03:47:15', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('10', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-23 03:57:09', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('11', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-23 03:57:18', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('6', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-23 03:46:28', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('7', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-23 03:46:33', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('9', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-23 03:48:57', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('5', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-23 03:46:22', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('22', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-23 18:46:02', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('46', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-23 23:42:30', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('81', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-24 02:19:51', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('141', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-24 19:02:32', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('168', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-25 18:13:04', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('182', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-26 19:16:07', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('183', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-26 19:16:23', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('202', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-27 18:41:22', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('215', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-27 21:50:02', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('217', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-27 21:50:14', 'YYYY-MM-DD HH24:MI:SS'), null);
<<<<<<< HEAD
=======
INSERT INTO "TBL_MESSAGE" VALUES ('301', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-06-05 17:05:28', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('328', null, null, 'Từ chối công việc', 'Từ chối công việc', null, TO_DATE('2017-06-07 23:26:39', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('361', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-06-10 22:02:37', 'YYYY-MM-DD HH24:MI:SS'), null);
>>>>>>> 4f1464f3ea01ff1d3e2fb0f514f6fda65674f3f6
INSERT INTO "TBL_MESSAGE" VALUES ('42', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-23 22:29:24', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('55', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-24 00:13:52', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('56', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-24 00:16:54', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('82', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-24 02:33:50', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('83', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-24 02:37:14', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('84', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-24 02:43:21', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('101', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-24 03:10:41', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('145', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-24 19:35:47', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('146', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-24 19:38:20', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('147', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-24 19:43:17', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('167', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-25 17:19:33', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('204', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-27 18:53:29', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('205', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-27 18:53:35', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('206', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-27 18:54:41', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('218', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-27 21:52:38', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('219', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-27 21:52:44', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('220', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-27 21:52:52', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('221', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-27 21:53:04', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('223', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-27 21:53:24', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('224', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-27 21:53:30', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('226', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-27 21:53:58', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('250', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-27 22:37:38', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('256', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-28 01:21:44', 'YYYY-MM-DD HH24:MI:SS'), null);
<<<<<<< HEAD
=======
INSERT INTO "TBL_MESSAGE" VALUES ('302', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-06-05 19:31:03', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('323', null, null, 'Từ chối công việc', 'Từ chối công việc', null, TO_DATE('2017-06-07 21:53:55', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('324', null, null, 'Từ chối công việc', 'Từ chối công việc', null, TO_DATE('2017-06-07 21:57:57', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('341', null, null, 'Từ chối công việc', 'Từ chối công việc', null, TO_DATE('2017-06-08 22:37:06', 'YYYY-MM-DD HH24:MI:SS'), null);
>>>>>>> 4f1464f3ea01ff1d3e2fb0f514f6fda65674f3f6
INSERT INTO "TBL_MESSAGE" VALUES ('41', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-23 22:29:05', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('43', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-23 22:37:20', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('53', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-23 23:54:37', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('57', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-24 00:17:05', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('58', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-24 00:19:48', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('59', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-24 00:20:04', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('60', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-24 00:20:14', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('61', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-24 00:20:27', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('62', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-24 00:21:15', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('63', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-24 00:22:13', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('85', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-24 02:54:30', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('148', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-24 19:45:22', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('210', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-27 19:29:10', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('236', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-27 22:19:10', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('241', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-27 22:21:58', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('242', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-27 22:23:12', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('243', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-27 22:23:28', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('244', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-27 22:23:44', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('245', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-27 22:23:52', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('251', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-27 23:52:51', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('252', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-27 23:52:57', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('261', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-29 17:49:33', 'YYYY-MM-DD HH24:MI:SS'), null);
<<<<<<< HEAD
=======
INSERT INTO "TBL_MESSAGE" VALUES ('329', null, null, 'Từ chối công việc', 'Từ chối công việc', null, TO_DATE('2017-06-07 23:37:39', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('330', null, null, 'Từ chối công việc', 'Từ chối công việc', null, TO_DATE('2017-06-07 23:47:38', 'YYYY-MM-DD HH24:MI:SS'), null);
INSERT INTO "TBL_MESSAGE" VALUES ('332', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-06-08 01:05:50', 'YYYY-MM-DD HH24:MI:SS'), null);
>>>>>>> 4f1464f3ea01ff1d3e2fb0f514f6fda65674f3f6

-- ----------------------------
-- Table structure for TBL_PARTNER
-- ----------------------------
DROP TABLE "TBL_PARTNER";
CREATE TABLE "TBL_PARTNER" (
"ID" NUMBER NOT NULL ,
"PARTNER_TYPE" VARCHAR2(50 BYTE) NULL ,
"PARTNER_STATUS" VARCHAR2(50 BYTE) NULL ,
"PARTNER_CODE" VARCHAR2(50 BYTE) NULL ,
"COMPANY_NAME" VARCHAR2(255 BYTE) NULL ,
"COMPANY_NAME_SHORT" VARCHAR2(255 BYTE) NULL ,
"ADDRESS_HEAD_OFFICE" VARCHAR2(255 BYTE) NULL ,
"ADDRESS_TRADING_OFFICE" VARCHAR2(255 BYTE) NULL ,
"PHONE" VARCHAR2(50 BYTE) NULL ,
"REP_NAME" VARCHAR2(255 BYTE) NULL ,
"REP_POSITION" VARCHAR2(255 BYTE) NULL ,
"EMAIL" VARCHAR2(255 BYTE) NULL ,
"TAX_CODE" VARCHAR2(50 BYTE) NULL ,
"BUSINESS_REGIS_NUMBER" VARCHAR2(255 BYTE) NULL ,
"BUSINESS_REGIS_DATE" DATE NULL ,
"BUSINESS_REGIS_COUNT" VARCHAR2(7 BYTE) NULL ,
"PARTNER_GROUP" VARCHAR2(255 BYTE) NULL ,
"PARTNER_GROUP_PROVINCE" VARCHAR2(255 BYTE) NULL ,
"CREATE_AT" TIMESTAMP(6)  DEFAULT NULL  NULL ,
"CREATE_BY" NUMBER NULL ,
"UPDATE_AT" TIMESTAMP(6)  DEFAULT NULL  NULL ,
"UPDATE_BY" NUMBER NULL ,
"IS_LOCK" NUMBER(1) DEFAULT NULL  NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_PARTNER
-- ----------------------------
<<<<<<< HEAD
INSERT INTO "TBL_PARTNER" VALUES ('81', null, null, 'HCM2', 'chăm sóc  hàng hồ chí minh', null, 'Hồ chí minh, Việt Nam', 'Hồ chí minh', '081234567', '1', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "TBL_PARTNER" VALUES ('103', null, null, 'HCM2', 'chăm sóc  hàng hồ chí minh', null, 'Hồ chí minh, Việt Nam', 'Hồ chí minh', '081234567', '1', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "TBL_PARTNER" VALUES ('123', null, null, 'HCM2', 'chăm sóc  hàng hồ chí minh', null, 'Hồ chí minh, Việt Nam', 'Hồ chí minh', '081234567', '1', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "TBL_PARTNER" VALUES ('122', null, null, 'HCM2', 'chăm sóc  hàng hồ chí minh', null, 'Hồ chí minh, Việt Nam', 'Hồ chí minh', '081234567', '1', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "TBL_PARTNER" VALUES ('5', 'Platform', '003', 'HCM1', 'Chăm sóc khách hàng Hồ Chí Minh', 'CSKH HCM', 'Hồ chí minh, Việt Nam', 'Hồ chí minh', '081234567', '1', '1', 'cskhhcm@viettel.com.vn', '12323', '1', TO_DATE('2018-12-22 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), 'ăerwer', 'INTERNAL', '002', TO_TIMESTAMP(' 2017-05-29 00:00:00:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, TO_TIMESTAMP(' 2017-05-29 00:00:00:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, null);
INSERT INTO "TBL_PARTNER" VALUES ('3', 'Platform', '003', 'HCM3', 'Chăm sóc khách hàng Hồ Chí Minh', 'CSKH HCM', 'Hồ chí minh, Việt Nam', 'Hồ chí minh', '081234567', '1', '1', 'cskhhcm@viettel.com.vn', '12323', '1', TO_DATE('2018-12-22 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), 'ăerwer', 'INTERNAL', '002', TO_TIMESTAMP(' 2017-05-25 00:00:00:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, TO_TIMESTAMP(' 2017-05-25 00:00:00:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, null);
INSERT INTO "TBL_PARTNER" VALUES ('61', null, null, 'HCM2', 'chăm sóc  hàng hồ chí minh', null, 'Hồ chí minh, Việt Nam', 'Hồ chí minh', '081234567', '1', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "TBL_PARTNER" VALUES ('6', 'Platform', '003', 'HCM2', 'Chăm sóc  hàng Hồ Chí Minh', 'CSKH HCM', 'Hồ chí minh, Việt Nam', 'Hồ chí minh', '081234567', '1', '1', 'cskhhcm@viettel.com.vn', '12323', '1', TO_DATE('2018-12-22 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), 'ăerwer', 'INTERNAL', '002', TO_TIMESTAMP(' 2017-05-30 09:01:54:193000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, TO_TIMESTAMP(' 2017-05-30 09:01:54:193000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, null);
INSERT INTO "TBL_PARTNER" VALUES ('101', null, null, 'HCM2', 'chăm sóc  hàng hồ chí minh', null, 'Hồ chí minh, Việt Nam', 'Hồ chí minh', '081234567', '1', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "TBL_PARTNER" VALUES ('24', 'Platform', '003', 'HCM2', 'Chăm sóc  hàng Hồ Chí Minh', 'CSKH HCM', 'Hồ chí minh, Việt Nam', 'Hồ chí minh', '081234567', '1', '1', 'cskhhcm@viettel.com.vn', '12323', '1', TO_DATE('2018-12-22 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), 'ăerwer', 'INTERNAL', '002', TO_TIMESTAMP(' 2017-05-27 00:00:00:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, TO_TIMESTAMP(' 2017-05-27 00:00:00:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, null);
INSERT INTO "TBL_PARTNER" VALUES ('102', null, null, 'HCM2', 'chăm sóc  hàng hồ chí minh', null, 'Hồ chí minh, Việt Nam', 'Hồ chí minh', '081234567', '1', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "TBL_PARTNER" VALUES ('104', null, null, 'HCM2', 'chăm sóc  hàng hồ chí minh', null, 'Hồ chí minh, Việt Nam', 'Hồ chí minh', '081234567', '1', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "TBL_PARTNER" VALUES ('121', null, null, 'HCM2', 'chăm sóc  hàng hồ chí minh', null, 'Hồ chí minh, Việt Nam', 'Hồ chí minh', '081234567', '1', null, null, null, null, null, null, null, null, null, null, null, null, null);
=======
INSERT INTO "TBL_PARTNER" VALUES ('151', null, null, '1', '1', null, null, '1', '16756756757', '1', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "TBL_PARTNER" VALUES ('152', null, null, 'testManyBank', '1', null, null, '1', '154534534534', '1', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "TBL_PARTNER" VALUES ('160', 'Platform', null, null, '1', '1', '1', '1', '343534534534', '1', '1', 'testBank11@gmail.com', '1', '1', TO_DATE('2017-06-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '1', 'INTERNAL', '002', TO_TIMESTAMP(' 2017-06-06 09:52:32:727000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, TO_TIMESTAMP(' 2017-06-06 09:52:32:727000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, null);
INSERT INTO "TBL_PARTNER" VALUES ('81', '0', null, 'HCM2', 'chăm sóc  hàng hồ chí minh', 'dfd', 'Hồ chí minh, Việt Nam', 'Hồ chí minh', '081234567', '1', 'dfdf', 'phamvanluong@gmail.com', '23', 'fdf', TO_DATE('2017-06-08 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), 'dfdf', 'FORENAL', null, TO_TIMESTAMP(' 2017-06-10 13:39:53:607000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, TO_TIMESTAMP(' 2017-06-10 13:39:53:607000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, null);
INSERT INTO "TBL_PARTNER" VALUES ('172', '1', null, null, '1', '1', '1', '1', '4534543543', '1', '1', 'tet25@gmail.com', '1', '1', TO_DATE('2017-06-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '1', 'FORENAL', null, TO_TIMESTAMP(' 2017-06-06 11:17:22:268000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, TO_TIMESTAMP(' 2017-06-06 11:17:22:268000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, null);
INSERT INTO "TBL_PARTNER" VALUES ('174', 'Platform', null, null, '1', '1', '1', '1', '13333333333', '1', '1', 'eeee@gmail.com', '1', '1', TO_DATE('2017-06-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '1', 'FORENAL', null, TO_TIMESTAMP(' 2017-06-06 11:35:51:392000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, TO_TIMESTAMP(' 2017-06-06 11:35:51:392000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, null);
INSERT INTO "TBL_PARTNER" VALUES ('186', '0', null, null, 'EE', 'E', 'HN', 'HN', '0123012124', 'Kien', 'Nhan vien', 'mr@gmail.com', '123455', '123', TO_DATE('2017-06-02 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '1', 'INTERNAL', '002', TO_TIMESTAMP(' 2017-06-09 17:25:04:795000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, TO_TIMESTAMP(' 2017-06-09 17:25:04:795000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, null);
INSERT INTO "TBL_PARTNER" VALUES ('103', null, null, 'HCM2', 'chăm sóc  hàng hồ chí minh', null, 'Hồ chí minh, Việt Nam', 'Hồ chí minh', '081234567', '1', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "TBL_PARTNER" VALUES ('123', null, null, 'HCM2', 'chăm sóc  hàng hồ chí minh', null, 'Hồ chí minh, Việt Nam', 'Hồ chí minh', '081234567', '1', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "TBL_PARTNER" VALUES ('141', null, null, '1', 'hà nội', null, null, '1', '123456789', '1', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "TBL_PARTNER" VALUES ('122', null, null, 'HCM2', 'chăm sóc  hàng hồ chí minh', null, 'Hồ chí minh, Việt Nam', 'Hồ chí minh', '081234567', '1', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "TBL_PARTNER" VALUES ('143', null, null, '1', '1', null, null, '1', '123456789', '1', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "TBL_PARTNER" VALUES ('146', null, null, 'HCM2', 'chăm sóc  hàng hồ chí minh', null, 'Hồ chí minh, Việt Nam', 'Hồ chí minh', '081234567', '1', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "TBL_PARTNER" VALUES ('163', '0', null, null, '1', '1', '1', '1', '43243242342', '1', '1', 'testBank15@gmail.com', '1', '1', TO_DATE('2017-06-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '11', 'INTERNAL', '001', TO_TIMESTAMP(' 2017-06-06 10:08:30:727000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, TO_TIMESTAMP(' 2017-06-06 10:08:30:727000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, null);
INSERT INTO "TBL_PARTNER" VALUES ('164', '1', null, null, '1', '1', '1', '1', '13542345345345', '1', '1', 'testBank17@gmail.com', '1', '1', TO_DATE('2017-06-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '1', 'FORENAL', null, TO_TIMESTAMP(' 2017-06-06 10:18:00:695000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, TO_TIMESTAMP(' 2017-06-06 10:18:00:695000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, null);
INSERT INTO "TBL_PARTNER" VALUES ('165', '1', null, null, '1', '1', '1', '1', '23333333333', '1', '1', 'testBank18@gmail.com', '1', '1', TO_DATE('2017-06-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '1', 'FORENAL', null, TO_TIMESTAMP(' 2017-06-06 10:20:28:508000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, TO_TIMESTAMP(' 2017-06-06 10:20:28:508000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, null);
INSERT INTO "TBL_PARTNER" VALUES ('173', 'Platform', null, null, '34564', '1', '1', '1', '6456546444', '1', '1', 'fdsf@giaf.com', '1', '1', TO_DATE('2017-06-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '1', 'FORENAL', null, TO_TIMESTAMP(' 2017-06-06 11:29:12:809000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, TO_TIMESTAMP(' 2017-06-06 11:29:12:809000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, null);
INSERT INTO "TBL_PARTNER" VALUES ('175', '1', null, null, '1', '1', '1', '1', '143453453453', '1', '1', 'rewrw@gmail.com', '1', '1', TO_DATE('2017-06-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '1', 'FORENAL', null, TO_TIMESTAMP(' 2017-06-06 11:39:32:718000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, TO_TIMESTAMP(' 2017-06-06 11:39:32:718000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, null);
INSERT INTO "TBL_PARTNER" VALUES ('184', '0', null, null, 'GME', 'GME', 'HN', 'HN', '099990999009', 'Kiên', 'nhan vien', 'giaodichdi@gmail.com', '1234', '1234454', TO_DATE('2017-06-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '1', 'INTERNAL', '002', TO_TIMESTAMP(' 2017-06-09 16:56:38:738000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, TO_TIMESTAMP(' 2017-06-09 16:56:38:738000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, null);
INSERT INTO "TBL_PARTNER" VALUES ('187', '0', null, null, 'Q', 'Q', 'HN', 'HN', '0123123444', 'Kien', 'Nhan vien', '123@gmail.com', '1234', '1235455', TO_DATE('2017-06-09 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '1', 'INTERNAL', '002', TO_TIMESTAMP(' 2017-06-09 17:28:43:062000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, TO_TIMESTAMP(' 2017-06-09 17:28:43:062000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, null);
INSERT INTO "TBL_PARTNER" VALUES ('5', 'Platform', '003', 'HCM1', 'Chăm sóc khách hàng Hồ Chí Minh', 'CSKH HCM', 'Hồ chí minh, Việt Nam', 'Hồ chí minh', '081234567', '1', '1', 'cskhhcm@viettel.com.vn', '12323', '1', TO_DATE('2018-12-22 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), 'ăerwer', 'INTERNAL', '002', TO_TIMESTAMP(' 2017-05-29 00:00:00:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, TO_TIMESTAMP(' 2017-05-29 00:00:00:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, null);
INSERT INTO "TBL_PARTNER" VALUES ('147', null, null, 'pepsi', 'pepsico', null, null, 'Ha Noi', '0933444555', 'Nguyen Pep', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "TBL_PARTNER" VALUES ('148', null, null, '1', '1', null, null, '1', '165786867867', '1', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "TBL_PARTNER" VALUES ('149', null, null, '6546456', '1', null, null, '1', '156456546546', '1', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "TBL_PARTNER" VALUES ('150', null, null, '1', '1', null, null, '1', '1778748484', '1', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "TBL_PARTNER" VALUES ('153', null, null, 'testManyBank2', '1', null, null, '1', '145345345345', '1', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "TBL_PARTNER" VALUES ('154', null, null, 'testManyBank3', '1', null, null, '1', '575467476', '1', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "TBL_PARTNER" VALUES ('171', 'Platform', null, null, '1', '1', '1', '1', '1423423424', '1', '1', 'testBank23@gmail.com', '1', '1', TO_DATE('2017-06-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '1', 'FORENAL', null, TO_TIMESTAMP(' 2017-06-06 11:13:41:108000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, TO_TIMESTAMP(' 2017-06-06 11:13:41:108000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, null);
INSERT INTO "TBL_PARTNER" VALUES ('176', '1', null, null, '1', '1', '1', '1', '6745677567', '1', '1', 'chin@gmail.com', '1', '1', TO_DATE('2017-06-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '2', 'FORENAL', null, TO_TIMESTAMP(' 2017-06-06 11:52:10:204000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, TO_TIMESTAMP(' 2017-06-06 11:52:10:204000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, null);
INSERT INTO "TBL_PARTNER" VALUES ('182', '0', null, null, 'g', 'g', 'g', 'g', '111111111111', 'c', 'd', 'ddd@d.com', '44', '44', TO_DATE('2018-06-09 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '2', 'INTERNAL', '001', TO_TIMESTAMP(' 2017-06-09 15:11:51:073000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, TO_TIMESTAMP(' 2017-06-09 15:11:51:073000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, null);
INSERT INTO "TBL_PARTNER" VALUES ('3', 'Platform', '003', 'HCM3', 'Chăm sóc khách hàng Hồ Chí Minh', 'CSKH HCM', 'Hồ chí minh, Việt Nam', 'Hồ chí minh', '081234567', '1', '1', 'cskhhcm@viettel.com.vn', '12323', '1', TO_DATE('2018-12-22 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), 'ăerwer', 'INTERNAL', '002', TO_TIMESTAMP(' 2017-05-25 00:00:00:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, TO_TIMESTAMP(' 2017-05-25 00:00:00:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, null);
INSERT INTO "TBL_PARTNER" VALUES ('61', null, null, 'HCM2', 'chăm sóc  hàng hồ chí minh', null, 'Hồ chí minh, Việt Nam', 'Hồ chí minh', '081234567', '1', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "TBL_PARTNER" VALUES ('144', null, null, '1', '1', null, null, '1', '4534534534', '1', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "TBL_PARTNER" VALUES ('145', null, null, 'cocacola', 'Cocacola Viet Nam', null, null, 'Viet Nam', '0922333444', 'Dong nam', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "TBL_PARTNER" VALUES ('155', null, null, 'testBank4', 'testBank4', null, null, '1', '156546546546', '1', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "TBL_PARTNER" VALUES ('167', '1', null, null, '1', '1', '1', '1', '134342342423', '1', '1', 'test@gmail.com', '1', '1', TO_DATE('2017-06-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '1', 'FORENAL', null, TO_TIMESTAMP(' 2017-06-06 10:35:38:425000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, TO_TIMESTAMP(' 2017-06-06 10:35:38:425000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, null);
INSERT INTO "TBL_PARTNER" VALUES ('168', '1', null, null, '1', '1', '1', '1', '13245234234', '1', '1', 'testBank20@gmail.com', '1', '1', TO_DATE('2017-06-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '1', 'FORENAL', null, TO_TIMESTAMP(' 2017-06-06 10:45:26:965000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, TO_TIMESTAMP(' 2017-06-06 10:45:26:965000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, null);
INSERT INTO "TBL_PARTNER" VALUES ('181', '0', null, null, 'GEM', 'GEM', 'Hà Nội', 'Hà Nội', '0123546977', 'Kiên', 'GĐ', 'giaodich@gmail.com', '12334456666', '12334', TO_DATE('2017-01-02 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '1', 'INTERNAL', '002', TO_TIMESTAMP(' 2017-06-09 15:04:42:861000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, TO_TIMESTAMP(' 2017-06-09 15:04:42:861000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, null);
INSERT INTO "TBL_PARTNER" VALUES ('6', 'Platform', '003', 'HCM2', 'Chăm sóc  hàng Hồ Chí Minh', 'CSKH HCM', 'Hồ chí minh, Việt Nam', 'Hồ chí minh', '081234567', '1', '1', 'cskhhcm@viettel.com.vn', '12323', '1', TO_DATE('2018-12-22 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), 'ăerwer', 'INTERNAL', '002', TO_TIMESTAMP(' 2017-05-30 09:01:54:193000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, TO_TIMESTAMP(' 2017-05-30 09:01:54:193000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, null);
INSERT INTO "TBL_PARTNER" VALUES ('101', null, null, 'HCM2', 'chăm sóc  hàng hồ chí minh', null, 'Hồ chí minh, Việt Nam', 'Hồ chí minh', '081234567', '1', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "TBL_PARTNER" VALUES ('24', 'Platform', '003', 'HCM2', 'Chăm sóc  hàng Hồ Chí Minh', 'CSKH HCM', 'Hồ chí minh, Việt Nam', 'Hồ chí minh', '081234567', '1', '1', 'cskhhcm@viettel.com.vn', '12323', '1', TO_DATE('2018-12-22 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), 'ăerwer', 'INTERNAL', '002', TO_TIMESTAMP(' 2017-05-27 00:00:00:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, TO_TIMESTAMP(' 2017-05-27 00:00:00:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, null);
INSERT INTO "TBL_PARTNER" VALUES ('21', null, null, 'HCM2', 'chăm sóc  hàng hồ chí minh', 'HCM', 'Hồ chí minh, Việt Nam', 'Hồ chí minh', '081234567', '1', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "TBL_PARTNER" VALUES ('45', null, null, 'HCM2', 'chăm sóc  hàng hồ chí minh', 'HN', 'Hồ chí minh, Việt Nam', 'Hồ chí minh', '081234567', '1', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "TBL_PARTNER" VALUES ('121', null, null, 'HCM2', 'chăm sóc  hàng hồ chí minh', null, 'Hồ chí minh, Việt Nam', 'Hồ chí minh', '081234567', '1', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "TBL_PARTNER" VALUES ('142', null, null, '1', '1', null, null, '1', '123456789', '1', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO "TBL_PARTNER" VALUES ('177', '1', null, null, '1', '1', '1', '1', '123456789', '1', '1', 'fgsdoj@gmail.com', '1', '1', TO_DATE('2017-06-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '1', 'FORENAL', null, TO_TIMESTAMP(' 2017-06-06 13:25:13:111000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, TO_TIMESTAMP(' 2017-06-06 13:25:13:111000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, null);
INSERT INTO "TBL_PARTNER" VALUES ('183', '1', null, null, 'KKKK', 'K', 'HCM', 'HCM', '02928374646', 'Kiên', 'Nhân Viên', 'nguyentrungkien277.ptit@gmail.com', '2222334334451', '234123', TO_DATE('2017-01-03 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '2', 'INTERNAL', '002', TO_TIMESTAMP(' 2017-06-09 16:07:55:912000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, TO_TIMESTAMP(' 2017-06-09 16:07:55:912000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, null);
INSERT INTO "TBL_PARTNER" VALUES ('185', '0', null, null, 'EMG', 'EMG', 'Hà Nội', 'Hà Nội', '012345677', 'Kiên', 'Nhân viên', '123abc@gmail.com', '1234566', '1233', TO_DATE('2017-06-02 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '2', 'INTERNAL', '002', TO_TIMESTAMP(' 2017-06-09 17:17:47:251000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, TO_TIMESTAMP(' 2017-06-09 17:17:47:251000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, null);
INSERT INTO "TBL_PARTNER" VALUES ('188', '0', null, null, 'Q', 'WQQ', 'Ha Noi', 'ha Noi', '0123456344', 'kien', 'nhan vien', 'mrkienbi277@gmail.com', '12345', '1123445', TO_DATE('2017-06-12 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '1', 'INTERNAL', '002', TO_TIMESTAMP(' 2017-06-09 17:39:11:457000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, TO_TIMESTAMP(' 2017-06-09 17:39:11:457000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null, null);
>>>>>>> 4f1464f3ea01ff1d3e2fb0f514f6fda65674f3f6

-- ----------------------------
-- Table structure for TBL_PARTNER_BANK
-- ----------------------------
DROP TABLE "TBL_PARTNER_BANK";
CREATE TABLE "TBL_PARTNER_BANK" (
"ID" NUMBER(10) NOT NULL ,
"PARTNER_ID" NUMBER NULL ,
"BANK_CODE" VARCHAR2(256 BYTE) NULL ,
"BANK_BRANCH" VARCHAR2(256 BYTE) NULL ,
"ACCOUNT_NUMBER" VARCHAR2(256 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_PARTNER_BANK
-- ----------------------------
<<<<<<< HEAD
INSERT INTO "TBL_PARTNER_BANK" VALUES ('234', '6', 'BIDV', null, '1');
INSERT INTO "TBL_PARTNER_BANK" VALUES ('235', '6', 'BIDV', null, '2');
INSERT INTO "TBL_PARTNER_BANK" VALUES ('282', '24', 'BIDV', null, '5324');
INSERT INTO "TBL_PARTNER_BANK" VALUES ('283', '24', 'VTB', null, '5432523');
INSERT INTO "TBL_PARTNER_BANK" VALUES ('261', '21', 'BIDV', null, '2');
INSERT INTO "TBL_PARTNER_BANK" VALUES ('262', '21', 'AGB', null, '1');
=======
INSERT INTO "TBL_PARTNER_BANK" VALUES ('319', '174', 'BIDV', null, '444444444444');
INSERT INTO "TBL_PARTNER_BANK" VALUES ('365', '186', 'BIDV', null, '1235467');
INSERT INTO "TBL_PARTNER_BANK" VALUES ('384', '21', 'AGB', null, 'dfdf');
INSERT INTO "TBL_PARTNER_BANK" VALUES ('385', '21', 'BIDV', null, '2dfd');
INSERT INTO "TBL_PARTNER_BANK" VALUES ('386', '21', 'BIDV', null, '1');
INSERT INTO "TBL_PARTNER_BANK" VALUES ('302', null, 'BIDV', null, '534534534534');
INSERT INTO "TBL_PARTNER_BANK" VALUES ('317', '172', 'TPB', null, '9999999999');
INSERT INTO "TBL_PARTNER_BANK" VALUES ('305', null, 'BIDV', null, '23423423423');
INSERT INTO "TBL_PARTNER_BANK" VALUES ('306', null, 'BIDV', null, '32423423423');
INSERT INTO "TBL_PARTNER_BANK" VALUES ('234', '6', 'BIDV', null, '1');
INSERT INTO "TBL_PARTNER_BANK" VALUES ('307', null, 'BIDV', null, '11111111111');
INSERT INTO "TBL_PARTNER_BANK" VALUES ('235', '6', 'BIDV', null, '2');
INSERT INTO "TBL_PARTNER_BANK" VALUES ('282', '24', 'BIDV', null, '5324');
INSERT INTO "TBL_PARTNER_BANK" VALUES ('283', '24', 'VTB', null, '5432523');
INSERT INTO "TBL_PARTNER_BANK" VALUES ('318', null, 'BIDV', null, '66666666666666666666');
INSERT INTO "TBL_PARTNER_BANK" VALUES ('320', '175', 'BIDV', null, '555555555555');
INSERT INTO "TBL_PARTNER_BANK" VALUES ('321', '175', 'TPB', null, '4444444444444');
INSERT INTO "TBL_PARTNER_BANK" VALUES ('363', '184', 'BIDV', null, '12312312313');
INSERT INTO "TBL_PARTNER_BANK" VALUES ('366', '187', 'VTB', null, '1231414141');
INSERT INTO "TBL_PARTNER_BANK" VALUES ('313', null, 'BIDV', null, '22222222222222');
INSERT INTO "TBL_PARTNER_BANK" VALUES ('314', null, 'BIDV', null, '3333333333333');
INSERT INTO "TBL_PARTNER_BANK" VALUES ('315', null, 'BIDV', null, '2222222222222');
INSERT INTO "TBL_PARTNER_BANK" VALUES ('316', null, 'BIDV', null, '33333333333');
INSERT INTO "TBL_PARTNER_BANK" VALUES ('322', '176', 'TPB', null, '33333');
INSERT INTO "TBL_PARTNER_BANK" VALUES ('309', null, 'BIDV', null, '2222222222222222');
INSERT INTO "TBL_PARTNER_BANK" VALUES ('310', null, 'BIDV', null, '33333333333333333333333');
INSERT INTO "TBL_PARTNER_BANK" VALUES ('361', '181', 'AGB', null, '123456566');
INSERT INTO "TBL_PARTNER_BANK" VALUES ('362', '183', 'DAB', null, '1231231313131');
INSERT INTO "TBL_PARTNER_BANK" VALUES ('364', '185', 'BIDV', null, '11112222');
INSERT INTO "TBL_PARTNER_BANK" VALUES ('323', '177', 'BIDV', null, 'kkkkk');
INSERT INTO "TBL_PARTNER_BANK" VALUES ('367', '188', 'BIDV', null, '123123413123112');
>>>>>>> 4f1464f3ea01ff1d3e2fb0f514f6fda65674f3f6

-- ----------------------------
-- Table structure for TBL_PARTNER_LOCK
-- ----------------------------
DROP TABLE "TBL_PARTNER_LOCK";
CREATE TABLE "TBL_PARTNER_LOCK" (
"ID" NUMBER(10) NOT NULL ,
"REASON_CODE" VARCHAR2(256 BYTE) NULL ,
"UPDATE_AT" TIMESTAMP(6)  NULL ,
"UPDATE_BY" NUMBER(10) NULL ,
"CREATE_BY" NUMBER(10) NULL ,
"CREATE_AT" TIMESTAMP(6)  NULL ,
"CONTENT" VARCHAR2(2048 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_PARTNER_LOCK
-- ----------------------------

-- ----------------------------
-- Table structure for TBL_PARTNER_LOCK_FILES
-- ----------------------------
DROP TABLE "TBL_PARTNER_LOCK_FILES";
CREATE TABLE "TBL_PARTNER_LOCK_FILES" (
"ID" NUMBER(10) NOT NULL ,
"FILE_NAME" VARCHAR2(2048 BYTE) NULL ,
"DESCRIPTION" VARCHAR2(2048 BYTE) NULL ,
"PARTNER_LOCK_ID" NUMBER NULL ,
"UPLOAD_DATE" DATE NULL ,
"PATH_FILE" VARCHAR2(2048 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_PARTNER_LOCK_FILES
-- ----------------------------

-- ----------------------------
-- Table structure for TBL_PARTNER_STATUS
-- ----------------------------
DROP TABLE "TBL_PARTNER_STATUS";
CREATE TABLE "TBL_PARTNER_STATUS" (
"CODE" VARCHAR2(256 BYTE) NOT NULL ,
"NAME" VARCHAR2(256 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_PARTNER_STATUS
-- ----------------------------
INSERT INTO "TBL_PARTNER_STATUS" VALUES ('001', 'Đang chờ xác thực tài khoản');
INSERT INTO "TBL_PARTNER_STATUS" VALUES ('002', 'Đang chờ xác nhận đối tác');
INSERT INTO "TBL_PARTNER_STATUS" VALUES ('003', 'Đang hoạt động');
INSERT INTO "TBL_PARTNER_STATUS" VALUES ('004', 'Đang khóa');

-- ----------------------------
-- Table structure for TBL_PARTNER_TYPE
-- ----------------------------
DROP TABLE "TBL_PARTNER_TYPE";
CREATE TABLE "TBL_PARTNER_TYPE" (
"CODE" VARCHAR2(256 BYTE) NOT NULL ,
"NAME" VARCHAR2(256 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_PARTNER_TYPE
-- ----------------------------
INSERT INTO "TBL_PARTNER_TYPE" VALUES ('0', 'CSP');
INSERT INTO "TBL_PARTNER_TYPE" VALUES ('1', 'CSKH');
INSERT INTO "TBL_PARTNER_TYPE" VALUES ('Platform', 'Platform');

-- ----------------------------
-- Table structure for TBL_PERSON_TYPE
-- ----------------------------
DROP TABLE "TBL_PERSON_TYPE";
CREATE TABLE "TBL_PERSON_TYPE" (
"CODE" VARCHAR2(256 BYTE) NOT NULL ,
"NAME" VARCHAR2(256 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_PERSON_TYPE
-- ----------------------------

-- ----------------------------
-- Table structure for TBL_REASON
-- ----------------------------
DROP TABLE "TBL_REASON";
CREATE TABLE "TBL_REASON" (
"CODE" VARCHAR2(256 BYTE) NOT NULL ,
"NAME" VARCHAR2(256 BYTE) NULL ,
"DESCRIPTION" VARCHAR2(2048 BYTE) NULL ,
"TYPE" VARCHAR2(256 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_REASON
-- ----------------------------
INSERT INTO "TBL_REASON" VALUES ('R001', 'Vi phạm Hợp đồng', null, 'LOCK_PARTNER');
INSERT INTO "TBL_REASON" VALUES ('R002', 'Phá vỡ Hợp đồng', null, 'LOCK_PARTNER');
INSERT INTO "TBL_REASON" VALUES ('R003', 'Chậm phản hồi PAKN', null, 'LOCK_PARTNER');
INSERT INTO "TBL_REASON" VALUES ('R004', 'Hồ sơ điền sai thông tin', null, 'CONTRACT_PROFILE');
INSERT INTO "TBL_REASON" VALUES ('R005', 'Hồ sơ không hợp lệ', null, 'CONTRACT_PROFILE');
INSERT INTO "TBL_REASON" VALUES ('R006', 'Hồ sơ thiếu thông tin', null, 'CONTRACT_PROFILE');
INSERT INTO "TBL_REASON" VALUES ('R007', 'Khách hàng chưa phù hợp', null, 'CONTRACT_PROFILE');
<<<<<<< HEAD
=======
INSERT INTO "TBL_REASON" VALUES ('R012', 'Hợp đồng lỗi', ' ', 'CONTRACT');
INSERT INTO "TBL_REASON" VALUES ('R013', 'Hợp đồng có điều khoản chưa hợp lệ', ' ', 'CONTRACT');
INSERT INTO "TBL_REASON" VALUES ('R014', 'Cần bổ sung thêm thông tin về Đối tác', null, 'CONTRACT');
INSERT INTO "TBL_REASON" VALUES ('R015', 'Điều khoản cần rõ ràng hơn', null, 'CONTRACT');
INSERT INTO "TBL_REASON" VALUES ('R016', 'Cần đàm phán lại', null, 'CONTRACT');
INSERT INTO "TBL_REASON" VALUES ('R017', 'Điều khoản bất lợi', null, 'CONTRACT');
>>>>>>> 4f1464f3ea01ff1d3e2fb0f514f6fda65674f3f6
INSERT INTO "TBL_REASON" VALUES ('R008', 'Vi phạm Hợp đồng', null, 'BREACH');
INSERT INTO "TBL_REASON" VALUES ('R009', 'Phá vỡ Hợp đồng', null, 'BREACH');
INSERT INTO "TBL_REASON" VALUES ('R010', 'Chậm phản hồi PAKN', null, 'BREACH');
INSERT INTO "TBL_REASON" VALUES ('R011', 'Hồ sơ điền sai thông tin', null, 'BREACH');
<<<<<<< HEAD
=======
INSERT INTO "TBL_REASON" VALUES ('REFLECT01', 'SPAM', 'Các trường hợp đối tác vi phạm tính cước quá 150.000vnd/1 ngày', 'REFLECT');
INSERT INTO "TBL_REASON" VALUES ('REFLECT02', 'Cú pháp sai', 'Các trường hợp đối tác vẫn tính cước khi thuê bao gửi cú pháp sai và không được hỗ trợ dịch vụ', 'REFLECT');
INSERT INTO "TBL_REASON" VALUES ('REFLECT03', 'Lỗi kết nối 1', 'CP không nhận được MO (Lỗi kết nối1) ', 'REFLECT');
INSERT INTO "TBL_REASON" VALUES ('REFLECT04', 'Lỗi kết nối 2', 'Có MO nhưng không trả MT (Lỗi kết nối2).', 'REFLECT');
INSERT INTO "TBL_REASON" VALUES ('REFLECT05', 'Lỗi kết nối 3', 'Trả MT lỗi (Lỗi kết nối3)', 'REFLECT');
INSERT INTO "TBL_REASON" VALUES ('REFLECT06', 'CSKH 1', 'KQ đối soát chính xác nhưng KH mong muốn chia sẻ% cước ( ghi chú: CSKH1)', 'REFLECT');
INSERT INTO "TBL_REASON" VALUES ('REFLECT07', 'CSKH 2', 'Thiết bị đầu cuối của KH không tương thích, KH  không dùng được dịch vụ (CSKH2)', 'REFLECT');
INSERT INTO "TBL_REASON" VALUES ('REFLECT08', 'CSKH 3', 'CP xác nhận hoàn cước do không có nội dung giải đáp rõ ràng, do không tìm ra nguyên nhân lỗi (CSKH3)', 'REFLECT');
INSERT INTO "TBL_REASON" VALUES ('REFLECT09', 'Lỗi khác', null, 'REFLECT');
INSERT INTO "TBL_REASON" VALUES ('REFLECT10', 'Không có lỗi dịch vụ ', 'Không do lỗi của dịch vụ  do KH chưa hiểu, chưa kiểm soát tốt thiết bị,,,', 'REFLECT');
>>>>>>> 4f1464f3ea01ff1d3e2fb0f514f6fda65674f3f6

-- ----------------------------
-- Table structure for TBL_REFLECT
-- ----------------------------
DROP TABLE "TBL_REFLECT";
CREATE TABLE "TBL_REFLECT" (
"ID" NUMBER(10) NOT NULL ,
"CODE" VARCHAR2(256 BYTE) NULL ,
"REFLECT_STATUS" VARCHAR2(256 BYTE) NULL ,
"REFLECT_TYPE" VARCHAR2(256 BYTE) NULL ,
"CUSTOMER_NAME" VARCHAR2(256 BYTE) NULL ,
"CUSTOMER_PHONE" VARCHAR2(256 BYTE) NULL ,
"SERVICE_CODE" VARCHAR2(256 BYTE) NULL ,
"HEAD_OF_SERVICE" VARCHAR2(256 BYTE) NULL ,
"CONTENT" VARCHAR2(2048 BYTE) NULL ,
"PARTNER_ID" NUMBER NULL ,
"CREATE_BY" NUMBER NULL ,
"CREATE_AT" DATE NULL ,
"UPDATE_BY" NUMBER NULL ,
"UPDATE_AT" DATE NULL ,
"CLOSE_DATE" DATE NULL ,
"CUSTOMER_CARE_STAFF_NAME" VARCHAR2(256 BYTE) NULL ,
"CUSTOMER_CARE_STAFF_PHONE" VARCHAR2(256 BYTE) NULL ,
"PRIORITY_LEVEL" VARCHAR2(256 BYTE) NULL ,
"REQUIRED_REPONSE_DATE" DATE NULL ,
"REASON_CODE" VARCHAR2(256 BYTE) NULL ,
"REASON_DESCRIPTION" VARCHAR2(2048 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_REFLECT
-- ----------------------------
<<<<<<< HEAD
INSERT INTO "TBL_REFLECT" VALUES ('80', 'BBBBBBBB', 'NEW', '1', '61', '88888', '3', '90909090', 'hhhhhh', null, null, null, null, null, null, null, null, 'VERY_IMPORTANT', TO_DATE('2017-07-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "TBL_REFLECT" VALUES ('79', 'KHONGDUOCXOA', '001', '2', '123', '13123123131', '1', '321312', '321313131332312', null, null, null, null, null, null, null, null, 'VERY_IMPORTANT', TO_DATE('2017-06-16 00:00:00', 'YYYY-MM-DD HH24:MI:SS'));
=======
INSERT INTO "TBL_REFLECT" VALUES ('79', 'KHONGDUOCXOA', '004', '1', '123', '13123123131', '1', '321312', '321313131332312', '21', '48', TO_DATE('2017-06-06 19:25:35', 'YYYY-MM-DD HH24:MI:SS'), '48', TO_DATE('2017-06-08 16:03:09', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2017-06-07 15:25:19', 'YYYY-MM-DD HH24:MI:SS'), '86786868', '86786868768', 'VERY_IMPORTANT', TO_DATE('2017-06-16 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), 'REFLECT05', '54235235');
INSERT INTO "TBL_REFLECT" VALUES ('141', '078', '004', '2', '078', '0982630749', '1', '08970', '089707079', '21', '48', null, '48', TO_DATE('2017-06-10 15:32:26', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('2017-06-10 15:32:26', 'YYYY-MM-DD HH24:MI:SS'), '68', '0982630749', 'VERY_IMPORTANT', TO_DATE('2017-06-02 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), 'REFLECT03', '078070');
>>>>>>> 4f1464f3ea01ff1d3e2fb0f514f6fda65674f3f6

-- ----------------------------
-- Table structure for TBL_REFLECT_FILES
-- ----------------------------
DROP TABLE "TBL_REFLECT_FILES";
CREATE TABLE "TBL_REFLECT_FILES" (
"ID" NUMBER(10) NOT NULL ,
"FILE_NAME" VARCHAR2(2048 BYTE) NULL ,
"DESCRIPTION" VARCHAR2(2048 BYTE) NULL ,
"REFLECT_ID" NUMBER NULL ,
"UPLOAD_DATE" DATE NULL ,
"PATH_FILE" VARCHAR2(2048 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_REFLECT_FILES
-- ----------------------------
<<<<<<< HEAD
INSERT INTO "TBL_REFLECT_FILES" VALUES ('1', 'a', 'ab', '80', TO_DATE('2017-06-30 04:26:16', 'YYYY-MM-DD HH24:MI:SS'), 'abc');
=======
INSERT INTO "TBL_REFLECT_FILES" VALUES ('8', 'Thiet ke co so du lieu_Viettel_CSP_1496660634733.doc', null, '79', null, 'D:\tool\uploaded-files\Thiet ke co so du lieu_Viettel_CSP_1496660634733.doc');
INSERT INTO "TBL_REFLECT_FILES" VALUES ('14', 'Thiet ke co so du lieu_Viettel_CSP_1496718133141.doc', null, '79', null, 'D:\tool\uploaded-files\Thiet ke co so du lieu_Viettel_CSP_1496718133141.doc');
INSERT INTO "TBL_REFLECT_FILES" VALUES ('20', 'Thiet ke co so du lieu_Viettel_CSP_1496722496695.doc', null, '79', null, 'D:\tool\uploaded-files\Thiet ke co so du lieu_Viettel_CSP_1496722496695.doc');
INSERT INTO "TBL_REFLECT_FILES" VALUES ('41', 'Thiet ke co so du lieu_Viettel_CSP_1496803219161.doc', 'Thiet ke co so du lieu_Viettel_CSP_1496653138815.doc', '79', TO_DATE('2017-06-07 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), 'D:\tool\uploaded-files\Thiet ke co so du lieu_Viettel_CSP_1496803219161.doc');
INSERT INTO "TBL_REFLECT_FILES" VALUES ('3', 'Thiet ke co so du lieu_Viettel_CSP_1496653138815.doc', 'Thiet ke co so du lieu_Viettel_CSP_1496653138815.doc', '79', null, 'D:\tool\uploaded-files\Thiet ke co so du lieu_Viettel_CSP_1496653138815.doc');
INSERT INTO "TBL_REFLECT_FILES" VALUES ('5', 'Thiet ke co so du lieu_Viettel_CSP_1496653480604.doc', 'Thiet ke co so du lieu_Viettel_CSP_1496653138815.doc', '79', null, 'D:\tool\uploaded-files\Thiet ke co so du lieu_Viettel_CSP_1496653480604.doc');
INSERT INTO "TBL_REFLECT_FILES" VALUES ('9', 'Thiet ke co so du lieu_Viettel_CSP_1496674176596.doc', null, '79', null, 'D:\tool\uploaded-files\Thiet ke co so du lieu_Viettel_CSP_1496674176596.doc');
INSERT INTO "TBL_REFLECT_FILES" VALUES ('13', 'Thiet ke co so du lieu_Viettel_CSP_1496717844184.doc', null, '79', null, 'D:\tool\uploaded-files\Thiet ke co so du lieu_Viettel_CSP_1496717844184.doc');
INSERT INTO "TBL_REFLECT_FILES" VALUES ('17', 'Thiet ke co so du lieu_Viettel_CSP_1496720679380.doc', 'Thiet ke co so du lieu_Viettel_CSP_1496653138815.doc', '79', null, 'D:\tool\uploaded-files\Thiet ke co so du lieu_Viettel_CSP_1496720679380.doc');
INSERT INTO "TBL_REFLECT_FILES" VALUES ('10', 'Thiet ke co so du lieu_Viettel_CSP_1496674846849.doc', 'Thiet ke co so du lieu_Viettel_CSP_1496653138815.doc', '79', null, 'D:\tool\uploaded-files\Thiet ke co so du lieu_Viettel_CSP_1496674846849.doc');
INSERT INTO "TBL_REFLECT_FILES" VALUES ('11', 'Thiet ke co so du lieu_Viettel_CSP_1496716244172.doc', 'Thiet ke co so du lieu_Viettel_CSP_1496653138815.doc', '79', null, 'D:\tool\uploaded-files\Thiet ke co so du lieu_Viettel_CSP_1496716244172.doc');
INSERT INTO "TBL_REFLECT_FILES" VALUES ('16', 'Thiet ke co so du lieu_Viettel_CSP_1496720521726.doc', 'Thiet ke co so du lieu_Viettel_CSP_1496653138815.doc', '79', null, 'D:\tool\uploaded-files\Thiet ke co so du lieu_Viettel_CSP_1496720521726.doc');
INSERT INTO "TBL_REFLECT_FILES" VALUES ('21', 'Thiet ke co so du lieu_Viettel_CSP_1496723155570.doc', 'Thiet ke co so du lieu_Viettel_CSP_1496653138815.doc', '79', null, 'D:\tool\uploaded-files\Thiet ke co so du lieu_Viettel_CSP_1496723155570.doc');
INSERT INTO "TBL_REFLECT_FILES" VALUES ('22', 'Thiet ke co so du lieu_Viettel_CSP_1496723236466.doc', null, '79', null, 'D:\tool\uploaded-files\Thiet ke co so du lieu_Viettel_CSP_1496723236466.doc');
INSERT INTO "TBL_REFLECT_FILES" VALUES ('23', 'Thiet ke co so du lieu_Viettel_CSP_1496731186883.doc', null, '79', null, 'D:\tool\uploaded-files\Thiet ke co so du lieu_Viettel_CSP_1496731186883.doc');
INSERT INTO "TBL_REFLECT_FILES" VALUES ('24', 'Thiet ke co so du lieu_Viettel_CSP_1496732975158.doc', null, '79', null, 'D:\tool\uploaded-files\Thiet ke co so du lieu_Viettel_CSP_1496732975158.doc');
INSERT INTO "TBL_REFLECT_FILES" VALUES ('4', 'Thiet ke co so du lieu_Viettel_CSP_1496653260964.doc', null, '79', null, 'D:\tool\uploaded-files\Thiet ke co so du lieu_Viettel_CSP_1496653260964.doc');
INSERT INTO "TBL_REFLECT_FILES" VALUES ('6', 'Thiet ke co so du lieu_Viettel_CSP_1496654051672.doc', null, '79', null, 'D:\tool\uploaded-files\Thiet ke co so du lieu_Viettel_CSP_1496654051672.doc');
INSERT INTO "TBL_REFLECT_FILES" VALUES ('7', 'Thiet ke co so du lieu_Viettel_CSP_1496654057322.doc', null, '79', null, 'D:\tool\uploaded-files\Thiet ke co so du lieu_Viettel_CSP_1496654057322.doc');
INSERT INTO "TBL_REFLECT_FILES" VALUES ('1', 'a', 'ab', '79', TO_DATE('2017-06-30 04:26:16', 'YYYY-MM-DD HH24:MI:SS'), 'abc');
INSERT INTO "TBL_REFLECT_FILES" VALUES ('2', 'Thiet ke co so du lieu_Viettel_CSP_1496652922650.doc', null, '79', null, 'D:\tool\uploaded-files\Thiet ke co so du lieu_Viettel_CSP_1496652922650.doc');
INSERT INTO "TBL_REFLECT_FILES" VALUES ('12', 'Thiet ke co so du lieu_Viettel_CSP_1496717636365.doc', null, '79', null, 'D:\tool\uploaded-files\Thiet ke co so du lieu_Viettel_CSP_1496717636365.doc');
INSERT INTO "TBL_REFLECT_FILES" VALUES ('15', 'Thiet ke co so du lieu_Viettel_CSP_1496718851357.doc', null, '79', null, 'D:\tool\uploaded-files\Thiet ke co so du lieu_Viettel_CSP_1496718851357.doc');
INSERT INTO "TBL_REFLECT_FILES" VALUES ('18', 'Thiet ke co so du lieu_Viettel_CSP_1496721144914.doc', null, '79', null, 'D:\tool\uploaded-files\Thiet ke co so du lieu_Viettel_CSP_1496721144914.doc');
INSERT INTO "TBL_REFLECT_FILES" VALUES ('19', 'Thiet ke co so du lieu_Viettel_CSP_1496721184324.doc', null, '79', null, 'D:\tool\uploaded-files\Thiet ke co so du lieu_Viettel_CSP_1496721184324.doc');
INSERT INTO "TBL_REFLECT_FILES" VALUES ('61', 'Vu-Xuan-Huong-TopCV.vn-140517.210208_1497067225622.pdf', null, '141', TO_DATE('2017-06-10 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), 'C:\Users\GEM\.IntelliJIdea14\system\tomcat\uploaded-files\Vu-Xuan-Huong-TopCV.vn-140517.210208_1497067225622.pdf');
>>>>>>> 4f1464f3ea01ff1d3e2fb0f514f6fda65674f3f6

-- ----------------------------
-- Table structure for TBL_REFLECT_STATUS
-- ----------------------------
DROP TABLE "TBL_REFLECT_STATUS";
CREATE TABLE "TBL_REFLECT_STATUS" (
"CODE" VARCHAR2(256 BYTE) NOT NULL ,
"NAME" VARCHAR2(256 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_REFLECT_STATUS
-- ----------------------------
INSERT INTO "TBL_REFLECT_STATUS" VALUES ('001', 'Chưa gửi CSP');
INSERT INTO "TBL_REFLECT_STATUS" VALUES ('002', 'Đang chờ trả lời');
INSERT INTO "TBL_REFLECT_STATUS" VALUES ('003', 'Đã trả lời');
INSERT INTO "TBL_REFLECT_STATUS" VALUES ('004', 'Hoàn thành');

-- ----------------------------
-- Table structure for TBL_REFLECT_TYPE
-- ----------------------------
DROP TABLE "TBL_REFLECT_TYPE";
CREATE TABLE "TBL_REFLECT_TYPE" (
"CODE" VARCHAR2(256 BYTE) NOT NULL ,
"NAME" VARCHAR2(256 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_REFLECT_TYPE
-- ----------------------------
INSERT INTO "TBL_REFLECT_TYPE" VALUES ('1', 'PRIMEFACE');
INSERT INTO "TBL_REFLECT_TYPE" VALUES ('2', 'ZK');

-- ----------------------------
-- Table structure for TBL_SERVICE
-- ----------------------------
DROP TABLE "TBL_SERVICE";
CREATE TABLE "TBL_SERVICE" (
"CODE" VARCHAR2(256 BYTE) NOT NULL ,
"NAME" VARCHAR2(256 BYTE) NULL ,
"PARENT_CODE" NUMBER NULL ,
"PATH_FILE" VARCHAR2(1024 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_SERVICE
-- ----------------------------
<<<<<<< HEAD
INSERT INTO "TBL_SERVICE" VALUES ('1', 'SPAM', null);
INSERT INTO "TBL_SERVICE" VALUES ('2', 'TIN NHAN', '1');
INSERT INTO "TBL_SERVICE" VALUES ('3', 'TEXT', '1');
INSERT INTO "TBL_SERVICE" VALUES ('4', 'Quang cao', '1');
=======
INSERT INTO "TBL_SERVICE" VALUES ('5', 'Nhan tin 360', '1', null);
INSERT INTO "TBL_SERVICE" VALUES ('1', 'SPAM', null, null);
INSERT INTO "TBL_SERVICE" VALUES ('2', 'TIN NHAN', '1', 'C:\uploaded-files\Thiet ke chi tiet_Web_FM&RA_1496671606481.docx');
INSERT INTO "TBL_SERVICE" VALUES ('3', 'TEXT', '1', 'C:\uploaded-files\Quản Lý Hợp Đồng(1)aaa_1496823518198.pdf');
INSERT INTO "TBL_SERVICE" VALUES ('4', 'Quang cao', '1', 'C:\uploaded-files\Qu-n Lư H-p --ng(1)aaa_1496841537473_1496980417213.pdf');
>>>>>>> 4f1464f3ea01ff1d3e2fb0f514f6fda65674f3f6

-- ----------------------------
-- Table structure for TBL_SERVICE_PARTNER
-- ----------------------------
DROP TABLE "TBL_SERVICE_PARTNER";
CREATE TABLE "TBL_SERVICE_PARTNER" (
"ID" NUMBER(10) NOT NULL ,
"SERVICE_CODE" VARCHAR2(256 BYTE) NULL ,
"PARTNER_ID" NUMBER NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_SERVICE_PARTNER
-- ----------------------------

-- ----------------------------
-- Table structure for TBL_SOLUTION
-- ----------------------------
DROP TABLE "TBL_SOLUTION";
CREATE TABLE "TBL_SOLUTION" (
"ID" NUMBER(10) NOT NULL ,
"REFLECT_ID" NUMBER NULL ,
"CREATE_BY" NUMBER NULL ,
"CREATE_AT" DATE NULL ,
"UPDATE_BY" NUMBER NULL ,
"UPDATE_AT" DATE NULL ,
"CONTENT" VARCHAR2(2048 BYTE) NULL ,
"REPONSE_DATE" DATE NULL ,
"SOLUTION_STATUS" VARCHAR2(256 BYTE) NULL ,
"REQUEST_DATE" DATE NULL ,
"PRIORITY_LEVEL" VARCHAR2(256 BYTE) NULL ,
"EVAL_REASON_CODE" VARCHAR2(256 BYTE) NULL ,
"EVAL_DESCRIPTION" VARCHAR2(256 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_SOLUTION
-- ----------------------------
<<<<<<< HEAD
INSERT INTO "TBL_SOLUTION" VALUES ('53', '79', '24', TO_DATE('2017-06-02 11:15:30', 'YYYY-MM-DD HH24:MI:SS'), '24', TO_DATE('2017-06-02 11:15:30', 'YYYY-MM-DD HH24:MI:SS'), null, TO_DATE('2017-06-16 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, null, null, null, null);
INSERT INTO "TBL_SOLUTION" VALUES ('61', '79', '24', TO_DATE('2017-06-02 17:03:06', 'YYYY-MM-DD HH24:MI:SS'), '24', TO_DATE('2017-06-02 17:03:06', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, 'SEND_PARTNER', null, TO_DATE('2017-06-02 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null);
INSERT INTO "TBL_SOLUTION" VALUES ('62', '79', null, null, null, null, null, TO_DATE('2017-06-16 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, 'PARTNER', null, null, null, null);
INSERT INTO "TBL_SOLUTION" VALUES ('49', '79', '24', TO_DATE('2017-06-01 19:14:34', 'YYYY-MM-DD HH24:MI:SS'), '24', TO_DATE('2017-06-01 19:14:34', 'YYYY-MM-DD HH24:MI:SS'), null, TO_DATE('2017-06-16 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, null, null, null, null);
INSERT INTO "TBL_SOLUTION" VALUES ('50', '80', null, null, null, null, null, TO_DATE('2017-07-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, 'PARTNER', null, null, null, null);
INSERT INTO "TBL_SOLUTION" VALUES ('51', '80', null, null, null, null, null, TO_DATE('2017-07-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, 'PARTNER', null, null, null, null);
INSERT INTO "TBL_SOLUTION" VALUES ('52', '80', null, null, null, null, null, TO_DATE('2017-07-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, 'PARTNER', null, null, null, null);
INSERT INTO "TBL_SOLUTION" VALUES ('47', '79', '24', TO_DATE('2017-06-01 17:23:40', 'YYYY-MM-DD HH24:MI:SS'), '24', TO_DATE('2017-06-01 17:23:40', 'YYYY-MM-DD HH24:MI:SS'), null, TO_DATE('2017-06-16 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, null, null, null, null);
INSERT INTO "TBL_SOLUTION" VALUES ('48', '79', '24', TO_DATE('2017-06-01 17:45:13', 'YYYY-MM-DD HH24:MI:SS'), '24', TO_DATE('2017-06-01 17:45:13', 'YYYY-MM-DD HH24:MI:SS'), null, TO_DATE('2017-06-16 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, null, null, null, null);
=======
INSERT INTO "TBL_SOLUTION" VALUES ('184', '141', '21', TO_DATE('2017-06-10 15:30:41', 'YYYY-MM-DD HH24:MI:SS'), '21', TO_DATE('2017-06-10 15:30:41', 'YYYY-MM-DD HH24:MI:SS'), '908', TO_DATE('2017-06-10 15:30:41', 'YYYY-MM-DD HH24:MI:SS'), 'REPONSE', TO_DATE('2017-06-10 15:30:41', 'YYYY-MM-DD HH24:MI:SS'), null, null, null);
INSERT INTO "TBL_SOLUTION" VALUES ('183', '141', '21', TO_DATE('2017-06-10 15:17:10', 'YYYY-MM-DD HH24:MI:SS'), '48', TO_DATE('2017-06-10 15:22:56', 'YYYY-MM-DD HH24:MI:SS'), '80978', TO_DATE('2017-06-10 15:17:10', 'YYYY-MM-DD HH24:MI:SS'), 'NOK', TO_DATE('2017-06-10 15:17:10', 'YYYY-MM-DD HH24:MI:SS'), null, null, '09');
>>>>>>> 4f1464f3ea01ff1d3e2fb0f514f6fda65674f3f6

-- ----------------------------
-- Table structure for TBL_SOLUTION_FILES
-- ----------------------------
DROP TABLE "TBL_SOLUTION_FILES";
CREATE TABLE "TBL_SOLUTION_FILES" (
"ID" NUMBER(10) NOT NULL ,
"FILE_NAME" VARCHAR2(2048 BYTE) NULL ,
"DESCRIPTION" VARCHAR2(2048 BYTE) NULL ,
"SOLUTION_ID" NUMBER NULL ,
"UPLOAD_DATE" DATE NULL ,
"PATH_FILE" VARCHAR2(2048 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_SOLUTION_FILES
-- ----------------------------
INSERT INTO "TBL_SOLUTION_FILES" VALUES ('1', 'Vu-Xuan-Huong-TopCV.vn-140517.210208.pdf', 'Vu-Xuan-Huong-TopCV.vn-140517.210208.pdf', '161', TO_DATE('2017-06-09 18:46:53', 'YYYY-MM-DD HH24:MI:SS'), 'C:\Vu-Xuan-Huong-TopCV.vn-140517.210208.pdf');

-- ----------------------------
-- Table structure for TBL_TASK_FILES
-- ----------------------------
DROP TABLE "TBL_TASK_FILES";
CREATE TABLE "TBL_TASK_FILES" (
"ID" NUMBER(10) NOT NULL ,
"FILE_NAME" VARCHAR2(2048 BYTE) NULL ,
"DESCRIPTION" VARCHAR2(2048 BYTE) NULL ,
"TASK_ID" NUMBER NULL ,
"UPLOAD_DATE" DATE NULL ,
"PATH_FILE" VARCHAR2(2048 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_TASK_FILES
-- ----------------------------

-- ----------------------------
-- Table structure for TBL_TASK_TYPE
-- ----------------------------
DROP TABLE "TBL_TASK_TYPE";
CREATE TABLE "TBL_TASK_TYPE" (
"CODE" VARCHAR2(256 BYTE) NULL ,
"NAME" VARCHAR2(256 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;
COMMENT ON TABLE "TBL_TASK_TYPE" IS 'danh sách loại công việc';

-- ----------------------------
-- Records of TBL_TASK_TYPE
-- ----------------------------

-- ----------------------------
-- Table structure for TBL_TASKS
-- ----------------------------
DROP TABLE "TBL_TASKS";
CREATE TABLE "TBL_TASKS" (
"ID" NUMBER(10) NOT NULL ,
"CONTRACT_ID" NUMBER NULL ,
"ASSIGN_BY" NUMBER NULL ,
"ASSIGN_AT" DATE NULL ,
"TASK_USER_ID" NUMBER NULL ,
"CONTENT" VARCHAR2(2048 BYTE) NULL ,
"CREATE_BY" NUMBER NULL ,
"CREATE_AT" DATE NULL ,
"UPDATE_BY" NUMBER NULL ,
"UPDATE_AT" DATE NULL ,
"REQUIRED_COMPLETE_DATE" DATE NULL ,
"COMPLETE_DATE" DATE NULL ,
"STATUS" VARCHAR2(256 BYTE) NULL ,
"BERFORE_TASK_ID" NUMBER NULL ,
"TASK_TYPE" VARCHAR2(256 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_TASKS
-- ----------------------------
INSERT INTO "TBL_TASKS" VALUES ('1', '5', '81', TO_DATE('2017-06-06 01:25:01', 'YYYY-MM-DD HH24:MI:SS'), null, null, '81', TO_DATE('2017-06-06 01:25:01', 'YYYY-MM-DD HH24:MI:SS'), '81', TO_DATE('2017-06-06 01:25:01', 'YYYY-MM-DD HH24:MI:SS'), null, null, '4', null, null);
INSERT INTO "TBL_TASKS" VALUES ('2', '5', '81', TO_DATE('2017-06-06 01:25:17', 'YYYY-MM-DD HH24:MI:SS'), null, null, '81', TO_DATE('2017-06-06 01:25:17', 'YYYY-MM-DD HH24:MI:SS'), '81', TO_DATE('2017-06-06 01:25:17', 'YYYY-MM-DD HH24:MI:SS'), null, null, '4', null, null);
INSERT INTO "TBL_TASKS" VALUES ('21', '6', '43', TO_DATE('2017-06-06 01:25:01', 'YYYY-MM-DD HH24:MI:SS'), null, null, '81', TO_DATE('2017-06-06 01:25:01', 'YYYY-MM-DD HH24:MI:SS'), '81', TO_DATE('2017-06-06 01:25:01', 'YYYY-MM-DD HH24:MI:SS'), null, null, '4', null, null);
INSERT INTO "TBL_TASKS" VALUES ('22', '7', '43', TO_DATE('2017-06-06 01:25:01', 'YYYY-MM-DD HH24:MI:SS'), null, null, '81', TO_DATE('2017-06-06 01:25:01', 'YYYY-MM-DD HH24:MI:SS'), '81', TO_DATE('2017-06-06 01:25:01', 'YYYY-MM-DD HH24:MI:SS'), null, null, '4', null, null);
INSERT INTO "TBL_TASKS" VALUES ('23', '8', '43', TO_DATE('2017-06-06 01:25:01', 'YYYY-MM-DD HH24:MI:SS'), null, null, '81', TO_DATE('2017-06-06 01:25:01', 'YYYY-MM-DD HH24:MI:SS'), '81', TO_DATE('2017-06-06 01:25:01', 'YYYY-MM-DD HH24:MI:SS'), null, null, '4', null, null);
INSERT INTO "TBL_TASKS" VALUES ('24', '9', '43', TO_DATE('2017-06-06 01:25:01', 'YYYY-MM-DD HH24:MI:SS'), null, null, '81', TO_DATE('2017-06-06 01:25:01', 'YYYY-MM-DD HH24:MI:SS'), '81', TO_DATE('2017-06-06 01:25:01', 'YYYY-MM-DD HH24:MI:SS'), null, null, '4', null, null);
INSERT INTO "TBL_TASKS" VALUES ('25', '10', '43', TO_DATE('2017-06-06 01:25:01', 'YYYY-MM-DD HH24:MI:SS'), null, null, '81', TO_DATE('2017-06-06 01:25:01', 'YYYY-MM-DD HH24:MI:SS'), '81', TO_DATE('2017-06-06 01:25:01', 'YYYY-MM-DD HH24:MI:SS'), null, null, '4', null, null);
INSERT INTO "TBL_TASKS" VALUES ('26', '11', '43', TO_DATE('2017-06-06 01:25:01', 'YYYY-MM-DD HH24:MI:SS'), null, null, '81', TO_DATE('2017-06-06 01:25:01', 'YYYY-MM-DD HH24:MI:SS'), '81', TO_DATE('2017-06-06 01:25:01', 'YYYY-MM-DD HH24:MI:SS'), null, null, '2', null, null);
INSERT INTO "TBL_TASKS" VALUES ('27', '12', '43', TO_DATE('2017-06-06 01:25:01', 'YYYY-MM-DD HH24:MI:SS'), null, null, '81', TO_DATE('2017-06-06 01:25:01', 'YYYY-MM-DD HH24:MI:SS'), '81', TO_DATE('2017-06-06 01:25:01', 'YYYY-MM-DD HH24:MI:SS'), null, null, '2', null, null);
INSERT INTO "TBL_TASKS" VALUES ('28', '13', '43', TO_DATE('2017-06-06 01:25:01', 'YYYY-MM-DD HH24:MI:SS'), null, null, '81', TO_DATE('2017-06-06 01:25:01', 'YYYY-MM-DD HH24:MI:SS'), '81', TO_DATE('2017-06-06 01:25:01', 'YYYY-MM-DD HH24:MI:SS'), null, null, '2', null, null);

-- ----------------------------
-- Table structure for TBL_USER
-- ----------------------------
DROP TABLE "TBL_USER";
CREATE TABLE "TBL_USER" (
"ID" NUMBER(10) NOT NULL ,
"FULLNAME" VARCHAR2(256 BYTE) NULL ,
"USERNAME" VARCHAR2(256 BYTE) NULL ,
"PASSWORD" VARCHAR2(256 BYTE) NULL ,
"EMAIL" VARCHAR2(256 BYTE) NULL ,
"TELEPHONE_NUMBER" VARCHAR2(256 BYTE) NULL ,
"PERSON_INFO_ID" NUMBER NULL ,
"PERSON_TYPE" VARCHAR2(256 BYTE) NULL ,
"STATUS" VARCHAR2(256 BYTE) NULL ,
"DATE_OF_BIRTH" DATE DEFAULT NULL  NULL ,
"GENDER" VARCHAR2(256 BYTE) NULL ,
"CURRENT_ADDRESS" VARCHAR2(20 BYTE) NULL ,
"MOBILE_NUMBER" VARCHAR2(256 BYTE) NULL ,
"ORGANIZATIONID" NUMBER(10) NULL ,
"EMPLOYEECODE" VARCHAR2(256 BYTE) NULL ,
"PASSWORD2" VARCHAR2(256 BYTE) NULL ,
"VERIFY_CODE" VARCHAR2(256 BYTE) NULL ,
"VERIFY_CODE_DATE" TIMESTAMP(6)  DEFAULT NULL  NULL ,
"RANDOM_KEY" VARCHAR2(256 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;
COMMENT ON COLUMN "TBL_USER"."VERIFY_CODE_DATE" IS '12 hour';

-- ----------------------------
-- Records of TBL_USER
-- ----------------------------
<<<<<<< HEAD
INSERT INTO "TBL_USER" VALUES ('22', 'nguyễn văn tiến', 'tien', '8i3nGBK4YFQ+mb+69AfELe0EPSrXe+3qaxjkbn93LGs=', 'tien@gmail.com', null, '22', 'CONTACT', 'ACTIVE', null, null, null, '093938388', null, null, '8i3nGBK4YFQ+mb+69AfELe0EPSrXe+3qaxjkbn93LGs=', '0.8534729293046364', TO_TIMESTAMP(' 2016-05-26 00:00:00:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'), '17df5d0e-915a-446b-9002-b2d02617e97a');
INSERT INTO "TBL_USER" VALUES ('24', 'lại thanh', 'thanhlt27', '8888', 'hoavt11@viettel.com.vn', null, '24', 'CONTACT', 'ACTIVE', null, null, null, '1234567891', null, null, null, null, null, '1');
INSERT INTO "TBL_USER" VALUES ('41', null, 'tien11111111111', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '1');
INSERT INTO "TBL_USER" VALUES ('26', '123123', 'abc123', null, '1232312@gmail.com', null, '26', 'CONTACT', 'ACTIVE', null, null, null, '123234567', null, null, null, null, null, '1');
INSERT INTO "TBL_USER" VALUES ('42', null, 'tien11111111111', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '1');
INSERT INTO "TBL_USER" VALUES ('3', null, '43223233', null, null, null, '81', 'PARTNER', 'ACTIVE', null, null, null, null, null, null, null, null, null, '1');
INSERT INTO "TBL_USER" VALUES ('27', '123123', 'aabc123', null, 'aaaar@gmail.com', null, '27', 'CONTACT', 'INACTIVE', null, null, null, '234234234', null, null, null, null, null, null);
=======
INSERT INTO "TBL_USER" VALUES ('86', null, 'kienkien', 'f8fed605b83be9279e2b0a9549aff3378de6844091309c02a4b1ec72369741fe', 'abc@gmail.com', null, null, null, null, null, null, null, '01223445455', null, null, null, null, null, 'JId8=>P6cO~SY,z>UPU@');
INSERT INTO "TBL_USER" VALUES ('22', 'nguyễn văn tiến', 'sub-doitac', '2926a2731f4b312c08982cacf8061eb14bf65c1a87cc5d70e864e079c6220731', 'tien@gmail.com', '0982630749', '22', 'CONTACT', 'ACTIVE', TO_DATE('1983-06-07 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), 'MALE', 'Bắc Ninh', '093938388', '148843', '001', '8i3nGBK4YFQ+mb+69AfELe0EPSrXe+3qaxjkbn93LGs=', '0.23096750556064416', TO_TIMESTAMP(' 2016-05-26 00:00:00:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'), null);
INSERT INTO "TBL_USER" VALUES ('24', 'Lại Thị Thanh', 'thanhlt27', '2926a2731f4b312c08982cacf8061eb14bf65c1a87cc5d70e864e079c6220731', 'hoavt11@viettel.com.vn', '0982630749', '24', 'CONTACT', 'ACTIVE', TO_DATE('1983-06-07 16:32:53', 'YYYY-MM-DD HH24:MI:SS'), 'MALE', 'Bắc Ninh', '1234567891', '148843', '002', null, null, null, '1');
INSERT INTO "TBL_USER" VALUES ('84', null, 'kien', '243611f31d989acc3e9150ea5f46fdacf1a8e618a9b366b99e78684d18c27ac6', '123aaa@gmail.com', null, null, null, null, null, null, null, '0123456556', null, null, null, null, null, 'J);sI4gMy~Oe4>UXlKGn');
INSERT INTO "TBL_USER" VALUES ('87', null, 'kien1995', '5a01c6f5015bd991f085a509fe678de0b54af06bceea7a19883c7b0f2c620c9e', '12123@gmail.com', null, null, null, null, null, null, null, '0000112311', null, null, null, '0.3252233466146134', TO_TIMESTAMP(' 2017-06-09 17:28:24:904000', 'YYYY-MM-DD HH24:MI:SS:FF6'), '=laV''2vna-DO5th=_)uA');
INSERT INTO "TBL_USER" VALUES ('82', null, 'aaa', '68c09338ac898602d1a4fdbca5a4a664c964dd2b01d3a5910f9f22ec29115ee3', ' abc@abc.com', null, null, null, null, null, null, null, '                            123123123', null, null, null, null, null, 'fw>RN:LX-9pqH@OX&<&@');
INSERT INTO "TBL_USER" VALUES ('81', null, 'mrkienbi277', 'f557bd486dcc1da097ee0bd5017b22a60c31f16daa1792332bdd4fc532773534', 'mrkienbi277@gmail.com', null, null, null, null, null, null, null, '01636245439', null, null, null, null, null, '~Ubb>/QMs)CQ$!^.0@Q/');
INSERT INTO "TBL_USER" VALUES ('83', null, 'kien277', '47730d78e47fd1b0a265d379bad82b06d49d57e53884e73949b9a9748011a6f3', 'nguyentrungkien277.ptit@gmail.com', null, null, null, null, null, null, null, '0122344522', null, null, null, null, null, '-#%p8~o@l@n@=q#IyBTC');
INSERT INTO "TBL_USER" VALUES ('21', 'Đối tác Viettel', 'doitac', '2926a2731f4b312c08982cacf8061eb14bf65c1a87cc5d70e864e079c6220731', 'doitac@gmail.com', '0982630749', '21', 'PARTNER', 'ACTIVE', TO_DATE('1983-06-07 16:32:53', 'YYYY-MM-DD HH24:MI:SS'), 'MALE', 'Bắc Ninh', '1234567891', '148843', null, '2926a2731f4b312c08982cacf8061eb14bf65c1a87cc5d70e864e079c6220731', null, null, null);
INSERT INTO "TBL_USER" VALUES ('43', 'Phòng CSP', 'vt_csp_dept', '2926a2731f4b312c08982cacf8061eb14bf65c1a87cc5d70e864e079c6220731', 'vt_csp_dept@gmail.com', '0982630749', null, 'VIETTEL', 'ACTIVE', TO_DATE('1983-06-07 16:32:53', 'YYYY-MM-DD HH24:MI:SS'), 'MALE', 'Bắc Ninh', '1234567891', '123453', null, '2926a2731f4b312c08982cacf8061eb14bf65c1a87cc5d70e864e079c6220731', null, null, null);
INSERT INTO "TBL_USER" VALUES ('44', 'Pháp chế', 'vt_csp_phapche', '2926a2731f4b312c08982cacf8061eb14bf65c1a87cc5d70e864e079c6220731', 'vt_csp_phapche@gmail.com', '0982630749', null, 'VIETTEL', 'ACTIVE', TO_DATE('1983-06-07 16:32:53', 'YYYY-MM-DD HH24:MI:SS'), 'MALE', 'Bắc Ninh', '1234567891', '123454', null, '2926a2731f4b312c08982cacf8061eb14bf65c1a87cc5d70e864e079c6220731', null, null, null);
INSERT INTO "TBL_USER" VALUES ('45', 'Tổng Giám Đốc', 'vt_csp_ceo', '2926a2731f4b312c08982cacf8061eb14bf65c1a87cc5d70e864e079c6220731', 'vt_csp_ceo@gmail.com', '0982630749', null, 'VIETTEL', 'ACTIVE', TO_DATE('1983-06-07 16:32:53', 'YYYY-MM-DD HH24:MI:SS'), 'MALE', 'Bắc Ninh', '1234567891', '123451', null, '2926a2731f4b312c08982cacf8061eb14bf65c1a87cc5d70e864e079c6220731', null, null, null);
INSERT INTO "TBL_USER" VALUES ('46', 'Giám đốc đơn vị Kinh doanh', 'vt_csp_dvtk_director', '2926a2731f4b312c08982cacf8061eb14bf65c1a87cc5d70e864e079c6220731', 'vt_csp_dvtk_director@gmail.com', '0982630749', null, 'VIETTEL', 'ACTIVE', TO_DATE('1983-06-07 16:32:53', 'YYYY-MM-DD HH24:MI:SS'), 'MALE', 'Bắc Ninh', '1234567891', '123462', null, '2926a2731f4b312c08982cacf8061eb14bf65c1a87cc5d70e864e079c6220731', null, null, null);
INSERT INTO "TBL_USER" VALUES ('47', 'Nhân viên triển khai', 'vt_csp_dvtk_staff', '2926a2731f4b312c08982cacf8061eb14bf65c1a87cc5d70e864e079c6220731', 'vt_csp_dvtk_staff@gmail.com', '0982630749', null, 'VIETTEL', 'ACTIVE', TO_DATE('1983-06-07 16:32:53', 'YYYY-MM-DD HH24:MI:SS'), 'MALE', 'Bắc Ninh', '1234567891', '123465', null, '2926a2731f4b312c08982cacf8061eb14bf65c1a87cc5d70e864e079c6220731', null, null, null);
INSERT INTO "TBL_USER" VALUES ('48', 'BO Chăm sóc KH', 'vt_csp_bo_cskh', '2926a2731f4b312c08982cacf8061eb14bf65c1a87cc5d70e864e079c6220731', 'vt_csp_bo_cskh@gmail.com', '0982630749', null, 'VIETTEL', 'ACTIVE', TO_DATE('1983-06-07 16:32:53', 'YYYY-MM-DD HH24:MI:SS'), 'MALE', 'Bắc Ninh', '1234567891', '123466', null, '2926a2731f4b312c08982cacf8061eb14bf65c1a87cc5d70e864e079c6220731', null, null, null);
INSERT INTO "TBL_USER" VALUES ('85', null, 'kien', 'ca40356623d7ac03af596bd1919fb1f9d5a0fdb7ecac28fe002e52b7cf44a250', '123abc@gmail.com', null, null, null, null, null, null, null, '012222331', null, null, null, null, null, 'xZw(B.Hh#''jSJgTIKmp~');
INSERT INTO "TBL_USER" VALUES ('88', null, 'kiennt', 'kienkien', 'mrkienbi277@gmail.com', null, null, null, null, null, null, null, '01553334445', null, null, 'kienkien', '0.19990172075045376', TO_TIMESTAMP(' 2017-06-09 17:44:22:062000', 'YYYY-MM-DD HH24:MI:SS:FF6'), 'k:aFm.*J:XR!G:40OxrD');
>>>>>>> 4f1464f3ea01ff1d3e2fb0f514f6fda65674f3f6

-- ----------------------------
-- Function structure for CUSTOM_AUTH
-- ----------------------------
CREATE OR REPLACE function "CUSTOM_AUTH" (p_username in VARCHAR2, p_password in VARCHAR2)
return BOOLEAN
is
  l_password varchar2(4000);
  l_stored_password varchar2(4000);
  l_expires_on date;
  l_count number;
begin
-- First, check to see if the user is in the user table
select count(*) into l_count from demo_users where user_name = p_username;
if l_count > 0 then
  -- First, we fetch the stored hashed password & expire date
  select password, expires_on into l_stored_password, l_expires_on
   from demo_users where user_name = p_username;

  -- Next, we check to see if the user's account is expired
  -- If it is, return FALSE
  if l_expires_on > sysdate or l_expires_on is null then

    -- If the account is not expired, we have to apply the custom hash
    -- function to the password
    l_password := custom_hash(p_username, p_password);

    -- Finally, we compare them to see if they are the same and return
    -- either TRUE or FALSE
    if l_password = l_stored_password then
      return true;
    else
      return false;
    end if;
  else
    return false;
  end if;
else
  -- The username provided is not in the DEMO_USERS table
  return false;
end if;
end;
/

-- ----------------------------
-- Function structure for CUSTOM_HASH
-- ----------------------------
CREATE OR REPLACE function "CUSTOM_HASH" (p_username in varchar2, p_password in varchar2)
return varchar2
is
  l_password varchar2(4000);
  l_salt varchar2(4000) := 'TRJKVE0NGR1ZPCVNYNE02C7QOUKRKY';
begin

-- This function should be wrapped, as the hash algorhythm is exposed here.
-- You can change the value of l_salt or the method of which to call the
-- DBMS_OBFUSCATOIN toolkit, but you much reset all of your passwords
-- if you choose to do this.

l_password := utl_raw.cast_to_raw(dbms_obfuscation_toolkit.md5
  (input_string => p_password || substr(l_salt,10,13) || p_username ||
    substr(l_salt, 4,10)));
return l_password;
end;
/

-- ----------------------------
-- Sequence structure for TBL_CONTACT_POINT_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_CONTACT_POINT_SEQ";
CREATE SEQUENCE "TBL_CONTACT_POINT_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 181
 CACHE 20;

-- ----------------------------
-- Sequence structure for TBL_CONTACT_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_CONTACT_SEQ";
CREATE SEQUENCE "TBL_CONTACT_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 41
 CACHE 20;

-- ----------------------------
-- Sequence structure for TBL_CONTACT_TYPE_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_CONTACT_TYPE_SEQ";
CREATE SEQUENCE "TBL_CONTACT_TYPE_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 1
 CACHE 20;

-- ----------------------------
-- Sequence structure for TBL_CONTRACT_BREACH_DIS_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_CONTRACT_BREACH_DIS_SEQ";
CREATE SEQUENCE "TBL_CONTRACT_BREACH_DIS_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 1
 CACHE 20;

-- ----------------------------
-- Sequence structure for TBL_CONTRACT_BREACH_FILES_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_CONTRACT_BREACH_FILES_SEQ";
CREATE SEQUENCE "TBL_CONTRACT_BREACH_FILES_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 1
 CACHE 20;

-- ----------------------------
-- Sequence structure for TBL_CONTRACT_BREACH_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_CONTRACT_BREACH_SEQ";
CREATE SEQUENCE "TBL_CONTRACT_BREACH_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 1
 CACHE 20;

-- ----------------------------
-- Sequence structure for TBL_CONTRACT_FILES_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_CONTRACT_FILES_SEQ";
CREATE SEQUENCE "TBL_CONTRACT_FILES_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 201
 CACHE 20;

-- ----------------------------
-- Sequence structure for TBL_CONTRACT_HIS_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_CONTRACT_HIS_SEQ";
CREATE SEQUENCE "TBL_CONTRACT_HIS_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 141
 CACHE 20;

-- ----------------------------
-- Sequence structure for TBL_CONTRACT_LOCK_FILES_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_CONTRACT_LOCK_FILES_SEQ";
CREATE SEQUENCE "TBL_CONTRACT_LOCK_FILES_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 1
 CACHE 20;

-- ----------------------------
-- Sequence structure for TBL_CONTRACT_LOCK_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_CONTRACT_LOCK_SEQ";
CREATE SEQUENCE "TBL_CONTRACT_LOCK_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 1
 CACHE 20;

-- ----------------------------
-- Sequence structure for TBL_CONTRACT_PAUSE_FILES_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_CONTRACT_PAUSE_FILES_SEQ";
CREATE SEQUENCE "TBL_CONTRACT_PAUSE_FILES_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 1
 CACHE 20;

-- ----------------------------
-- Sequence structure for TBL_CONTRACT_PAUSE_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_CONTRACT_PAUSE_SEQ";
CREATE SEQUENCE "TBL_CONTRACT_PAUSE_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 1
 CACHE 20;

-- ----------------------------
-- Sequence structure for TBL_CONTRACT_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_CONTRACT_SEQ";
CREATE SEQUENCE "TBL_CONTRACT_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 261
 CACHE 20;

-- ----------------------------
-- Sequence structure for TBL_CT_BREACH_REASON_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_CT_BREACH_REASON_SEQ";
CREATE SEQUENCE "TBL_CT_BREACH_REASON_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 1
 CACHE 20;

-- ----------------------------
-- Sequence structure for TBL_DEPARTMENT_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_DEPARTMENT_SEQ";
CREATE SEQUENCE "TBL_DEPARTMENT_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 41
 CACHE 20;

-- ----------------------------
-- Sequence structure for TBL_DISCIPLINE_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_DISCIPLINE_SEQ";
CREATE SEQUENCE "TBL_DISCIPLINE_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 1
 CACHE 20;

-- ----------------------------
-- Sequence structure for TBL_MESSAGE_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_MESSAGE_SEQ";
CREATE SEQUENCE "TBL_MESSAGE_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 381
 CACHE 20;

-- ----------------------------
-- Sequence structure for TBL_PARTNER_BANK_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_PARTNER_BANK_SEQ";
CREATE SEQUENCE "TBL_PARTNER_BANK_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 401
 CACHE 20;

-- ----------------------------
-- Sequence structure for TBL_PARTNER_LOCK_FILES_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_PARTNER_LOCK_FILES_SEQ";
CREATE SEQUENCE "TBL_PARTNER_LOCK_FILES_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 1
 CACHE 20;

-- ----------------------------
-- Sequence structure for TBL_PARTNER_LOCK_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_PARTNER_LOCK_SEQ";
CREATE SEQUENCE "TBL_PARTNER_LOCK_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 1
 CACHE 20;

-- ----------------------------
-- Sequence structure for TBL_PARTNER_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_PARTNER_SEQ";
CREATE SEQUENCE "TBL_PARTNER_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 201
 CACHE 20;

-- ----------------------------
-- Sequence structure for TBL_REFLECT_FILES_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_REFLECT_FILES_SEQ";
CREATE SEQUENCE "TBL_REFLECT_FILES_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 81
 CACHE 20;

-- ----------------------------
-- Sequence structure for TBL_REFLECT_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_REFLECT_SEQ";
CREATE SEQUENCE "TBL_REFLECT_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 161
 CACHE 20;

-- ----------------------------
-- Sequence structure for TBL_SERVICE_PARTNER_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_SERVICE_PARTNER_SEQ";
CREATE SEQUENCE "TBL_SERVICE_PARTNER_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 1
 CACHE 20;

-- ----------------------------
-- Sequence structure for TBL_SOLUTION_FILES_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_SOLUTION_FILES_SEQ";
CREATE SEQUENCE "TBL_SOLUTION_FILES_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 1
 CACHE 20;

-- ----------------------------
-- Sequence structure for TBL_SOLUTION_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_SOLUTION_SEQ";
CREATE SEQUENCE "TBL_SOLUTION_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 201
 CACHE 20;

-- ----------------------------
-- Sequence structure for TBL_TASK_FILES_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_TASK_FILES_SEQ";
CREATE SEQUENCE "TBL_TASK_FILES_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 1
 CACHE 20;

-- ----------------------------
-- Sequence structure for TBL_TASKS_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_TASKS_SEQ";
CREATE SEQUENCE "TBL_TASKS_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 41
 CACHE 20;

-- ----------------------------
-- Sequence structure for TBL_USER_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_USER_SEQ";
CREATE SEQUENCE "TBL_USER_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 101
 CACHE 20;

-- ----------------------------
-- Checks structure for table TBL_BANK
-- ----------------------------
ALTER TABLE "TBL_BANK" ADD CHECK ("CODE" IS NOT NULL);
ALTER TABLE "TBL_BANK" ADD CHECK ("CODE" IS NOT NULL);

-- ----------------------------
-- Indexes structure for table TBL_CONTACT
-- ----------------------------

-- ----------------------------
-- Checks structure for table TBL_CONTACT
-- ----------------------------
ALTER TABLE "TBL_CONTACT" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "TBL_CONTACT" ADD CHECK ("ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TBL_CONTACT
-- ----------------------------
ALTER TABLE "TBL_CONTACT" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Indexes structure for table TBL_CONTACT_POINT
-- ----------------------------

-- ----------------------------
-- Checks structure for table TBL_CONTACT_POINT
-- ----------------------------
ALTER TABLE "TBL_CONTACT_POINT" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "TBL_CONTACT_POINT" ADD CHECK ("ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TBL_CONTACT_POINT
-- ----------------------------
ALTER TABLE "TBL_CONTACT_POINT" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Checks structure for table TBL_CONTACT_TYPE
-- ----------------------------
ALTER TABLE "TBL_CONTACT_TYPE" ADD CHECK ("CODE" IS NOT NULL);
ALTER TABLE "TBL_CONTACT_TYPE" ADD CHECK ("CODE" IS NOT NULL);

-- ----------------------------
-- Indexes structure for table TBL_CONTRACT
-- ----------------------------

-- ----------------------------
-- Checks structure for table TBL_CONTRACT
-- ----------------------------
ALTER TABLE "TBL_CONTRACT" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "TBL_CONTRACT" ADD CHECK ("ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TBL_CONTRACT
-- ----------------------------
ALTER TABLE "TBL_CONTRACT" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Indexes structure for table TBL_CONTRACT_BREACH
-- ----------------------------

-- ----------------------------
-- Checks structure for table TBL_CONTRACT_BREACH
-- ----------------------------
ALTER TABLE "TBL_CONTRACT_BREACH" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "TBL_CONTRACT_BREACH" ADD CHECK ("ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TBL_CONTRACT_BREACH
-- ----------------------------
ALTER TABLE "TBL_CONTRACT_BREACH" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Indexes structure for table TBL_CONTRACT_BREACH_DISCIPLINE
-- ----------------------------

-- ----------------------------
-- Checks structure for table TBL_CONTRACT_BREACH_DISCIPLINE
-- ----------------------------
ALTER TABLE "TBL_CONTRACT_BREACH_DISCIPLINE" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "TBL_CONTRACT_BREACH_DISCIPLINE" ADD CHECK ("ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TBL_CONTRACT_BREACH_DISCIPLINE
-- ----------------------------
ALTER TABLE "TBL_CONTRACT_BREACH_DISCIPLINE" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Indexes structure for table TBL_CONTRACT_BREACH_FILES
-- ----------------------------

-- ----------------------------
-- Checks structure for table TBL_CONTRACT_BREACH_FILES
-- ----------------------------
ALTER TABLE "TBL_CONTRACT_BREACH_FILES" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "TBL_CONTRACT_BREACH_FILES" ADD CHECK ("ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TBL_CONTRACT_BREACH_FILES
-- ----------------------------
ALTER TABLE "TBL_CONTRACT_BREACH_FILES" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Indexes structure for table TBL_CONTRACT_BREACH_REASON
-- ----------------------------

-- ----------------------------
-- Checks structure for table TBL_CONTRACT_BREACH_REASON
-- ----------------------------
ALTER TABLE "TBL_CONTRACT_BREACH_REASON" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "TBL_CONTRACT_BREACH_REASON" ADD CHECK ("ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TBL_CONTRACT_BREACH_REASON
-- ----------------------------
ALTER TABLE "TBL_CONTRACT_BREACH_REASON" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Indexes structure for table TBL_CONTRACT_FILES
-- ----------------------------

-- ----------------------------
-- Checks structure for table TBL_CONTRACT_FILES
-- ----------------------------
ALTER TABLE "TBL_CONTRACT_FILES" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "TBL_CONTRACT_FILES" ADD CHECK ("ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TBL_CONTRACT_FILES
-- ----------------------------
ALTER TABLE "TBL_CONTRACT_FILES" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Indexes structure for table TBL_CONTRACT_HIS
-- ----------------------------

-- ----------------------------
-- Checks structure for table TBL_CONTRACT_HIS
-- ----------------------------
ALTER TABLE "TBL_CONTRACT_HIS" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "TBL_CONTRACT_HIS" ADD CHECK ("ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TBL_CONTRACT_HIS
-- ----------------------------
ALTER TABLE "TBL_CONTRACT_HIS" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Indexes structure for table TBL_CONTRACT_PAUSE
-- ----------------------------

-- ----------------------------
-- Checks structure for table TBL_CONTRACT_PAUSE
-- ----------------------------
ALTER TABLE "TBL_CONTRACT_PAUSE" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "TBL_CONTRACT_PAUSE" ADD CHECK ("ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TBL_CONTRACT_PAUSE
-- ----------------------------
ALTER TABLE "TBL_CONTRACT_PAUSE" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Indexes structure for table TBL_CONTRACT_PAUSE_FILES
-- ----------------------------

-- ----------------------------
-- Checks structure for table TBL_CONTRACT_PAUSE_FILES
-- ----------------------------
ALTER TABLE "TBL_CONTRACT_PAUSE_FILES" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "TBL_CONTRACT_PAUSE_FILES" ADD CHECK ("ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TBL_CONTRACT_PAUSE_FILES
-- ----------------------------
ALTER TABLE "TBL_CONTRACT_PAUSE_FILES" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Checks structure for table TBL_CONTRACT_STATUS
-- ----------------------------
ALTER TABLE "TBL_CONTRACT_STATUS" ADD CHECK ("CODE" IS NOT NULL);
ALTER TABLE "TBL_CONTRACT_STATUS" ADD CHECK ("CODE" IS NOT NULL);

-- ----------------------------
-- Checks structure for table TBL_CONTRACT_TYPE
-- ----------------------------
ALTER TABLE "TBL_CONTRACT_TYPE" ADD CHECK ("CODE" IS NOT NULL);
ALTER TABLE "TBL_CONTRACT_TYPE" ADD CHECK ("CODE" IS NOT NULL);
<<<<<<< HEAD
=======

-- ----------------------------
-- Indexes structure for table TBL_DEPARTMENT
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table TBL_DEPARTMENT
-- ----------------------------
ALTER TABLE "TBL_DEPARTMENT" ADD PRIMARY KEY ("ID");
>>>>>>> 4f1464f3ea01ff1d3e2fb0f514f6fda65674f3f6

-- ----------------------------
-- Checks structure for table TBL_DISCIPLINE
-- ----------------------------
ALTER TABLE "TBL_DISCIPLINE" ADD CHECK ("CODE" IS NOT NULL);
ALTER TABLE "TBL_DISCIPLINE" ADD CHECK ("CODE" IS NOT NULL);

-- ----------------------------
-- Checks structure for table TBL_FILE_TYPE
-- ----------------------------
ALTER TABLE "TBL_FILE_TYPE" ADD CHECK ("CODE" IS NOT NULL);
ALTER TABLE "TBL_FILE_TYPE" ADD CHECK ("CODE" IS NOT NULL);

-- ----------------------------
-- Indexes structure for table TBL_MESSAGE
-- ----------------------------

-- ----------------------------
-- Checks structure for table TBL_MESSAGE
-- ----------------------------
ALTER TABLE "TBL_MESSAGE" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "TBL_MESSAGE" ADD CHECK ("ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TBL_MESSAGE
-- ----------------------------
ALTER TABLE "TBL_MESSAGE" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Checks structure for table TBL_PARTNER
-- ----------------------------
ALTER TABLE "TBL_PARTNER" ADD CHECK ("ID" IS NOT NULL);

-- ----------------------------
-- Checks structure for table TBL_PARTNER_BANK
-- ----------------------------
ALTER TABLE "TBL_PARTNER_BANK" ADD CHECK ("ID" IS NOT NULL);

-- ----------------------------
-- Indexes structure for table TBL_PARTNER_LOCK
-- ----------------------------

-- ----------------------------
-- Checks structure for table TBL_PARTNER_LOCK
-- ----------------------------
ALTER TABLE "TBL_PARTNER_LOCK" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "TBL_PARTNER_LOCK" ADD CHECK ("ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TBL_PARTNER_LOCK
-- ----------------------------
ALTER TABLE "TBL_PARTNER_LOCK" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Indexes structure for table TBL_PARTNER_LOCK_FILES
-- ----------------------------

-- ----------------------------
-- Checks structure for table TBL_PARTNER_LOCK_FILES
-- ----------------------------
ALTER TABLE "TBL_PARTNER_LOCK_FILES" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "TBL_PARTNER_LOCK_FILES" ADD CHECK ("ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TBL_PARTNER_LOCK_FILES
-- ----------------------------
ALTER TABLE "TBL_PARTNER_LOCK_FILES" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Checks structure for table TBL_PARTNER_STATUS
-- ----------------------------
ALTER TABLE "TBL_PARTNER_STATUS" ADD CHECK ("CODE" IS NOT NULL);
ALTER TABLE "TBL_PARTNER_STATUS" ADD CHECK ("CODE" IS NOT NULL);

-- ----------------------------
-- Checks structure for table TBL_PARTNER_TYPE
-- ----------------------------
ALTER TABLE "TBL_PARTNER_TYPE" ADD CHECK ("CODE" IS NOT NULL);
ALTER TABLE "TBL_PARTNER_TYPE" ADD CHECK ("CODE" IS NOT NULL);

-- ----------------------------
-- Checks structure for table TBL_PERSON_TYPE
-- ----------------------------
ALTER TABLE "TBL_PERSON_TYPE" ADD CHECK ("CODE" IS NOT NULL);
ALTER TABLE "TBL_PERSON_TYPE" ADD CHECK ("CODE" IS NOT NULL);

-- ----------------------------
-- Checks structure for table TBL_REASON
-- ----------------------------
ALTER TABLE "TBL_REASON" ADD CHECK ("CODE" IS NOT NULL);
ALTER TABLE "TBL_REASON" ADD CHECK ("CODE" IS NOT NULL);

-- ----------------------------
-- Indexes structure for table TBL_REFLECT
-- ----------------------------

-- ----------------------------
-- Checks structure for table TBL_REFLECT
-- ----------------------------
ALTER TABLE "TBL_REFLECT" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "TBL_REFLECT" ADD CHECK ("ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TBL_REFLECT
-- ----------------------------
ALTER TABLE "TBL_REFLECT" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Indexes structure for table TBL_REFLECT_FILES
-- ----------------------------

-- ----------------------------
-- Checks structure for table TBL_REFLECT_FILES
-- ----------------------------
ALTER TABLE "TBL_REFLECT_FILES" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "TBL_REFLECT_FILES" ADD CHECK ("ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TBL_REFLECT_FILES
-- ----------------------------
ALTER TABLE "TBL_REFLECT_FILES" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Checks structure for table TBL_REFLECT_STATUS
-- ----------------------------
ALTER TABLE "TBL_REFLECT_STATUS" ADD CHECK ("CODE" IS NOT NULL);
ALTER TABLE "TBL_REFLECT_STATUS" ADD CHECK ("CODE" IS NOT NULL);

-- ----------------------------
-- Checks structure for table TBL_REFLECT_TYPE
-- ----------------------------
ALTER TABLE "TBL_REFLECT_TYPE" ADD CHECK ("CODE" IS NOT NULL);
ALTER TABLE "TBL_REFLECT_TYPE" ADD CHECK ("CODE" IS NOT NULL);

-- ----------------------------
-- Checks structure for table TBL_SERVICE
-- ----------------------------
ALTER TABLE "TBL_SERVICE" ADD CHECK ("CODE" IS NOT NULL);
ALTER TABLE "TBL_SERVICE" ADD CHECK ("CODE" IS NOT NULL);

-- ----------------------------
-- Indexes structure for table TBL_SERVICE_PARTNER
-- ----------------------------

-- ----------------------------
-- Checks structure for table TBL_SERVICE_PARTNER
-- ----------------------------
ALTER TABLE "TBL_SERVICE_PARTNER" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "TBL_SERVICE_PARTNER" ADD CHECK ("ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TBL_SERVICE_PARTNER
-- ----------------------------
ALTER TABLE "TBL_SERVICE_PARTNER" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Indexes structure for table TBL_SOLUTION
-- ----------------------------

-- ----------------------------
-- Checks structure for table TBL_SOLUTION
-- ----------------------------
ALTER TABLE "TBL_SOLUTION" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "TBL_SOLUTION" ADD CHECK ("ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TBL_SOLUTION
-- ----------------------------
ALTER TABLE "TBL_SOLUTION" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Indexes structure for table TBL_SOLUTION_FILES
-- ----------------------------

-- ----------------------------
-- Checks structure for table TBL_SOLUTION_FILES
-- ----------------------------
ALTER TABLE "TBL_SOLUTION_FILES" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "TBL_SOLUTION_FILES" ADD CHECK ("ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TBL_SOLUTION_FILES
-- ----------------------------
ALTER TABLE "TBL_SOLUTION_FILES" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Indexes structure for table TBL_TASK_FILES
-- ----------------------------

-- ----------------------------
-- Checks structure for table TBL_TASK_FILES
-- ----------------------------
ALTER TABLE "TBL_TASK_FILES" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "TBL_TASK_FILES" ADD CHECK ("ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TBL_TASK_FILES
-- ----------------------------
ALTER TABLE "TBL_TASK_FILES" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Indexes structure for table TBL_TASKS
-- ----------------------------

-- ----------------------------
-- Checks structure for table TBL_TASKS
-- ----------------------------
ALTER TABLE "TBL_TASKS" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "TBL_TASKS" ADD CHECK ("ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TBL_TASKS
-- ----------------------------
ALTER TABLE "TBL_TASKS" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Indexes structure for table TBL_USER
-- ----------------------------

-- ----------------------------
-- Checks structure for table TBL_USER
-- ----------------------------
ALTER TABLE "TBL_USER" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "TBL_USER" ADD CHECK ("ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TBL_USER
-- ----------------------------
ALTER TABLE "TBL_USER" ADD PRIMARY KEY ("ID");
