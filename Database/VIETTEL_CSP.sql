/*
Navicat Oracle Data Transfer
Oracle Client Version : 10.2.0.5.0

Source Server         : localhost
Source Server Version : 110200
Source Host           : localhost:1521
Source Schema         : VIETTEL_CSP

Target Server Type    : ORACLE
Target Server Version : 110200
File Encoding         : 65001

Date: 2017-05-23 08:50:41
*/


-- ----------------------------
-- Table structure for TBL_BANK
-- ----------------------------
DROP TABLE "TBL_BANK";
CREATE TABLE "TBL_BANK" (
"CODE" VARCHAR2(256 BYTE) NOT NULL ,
"NAME" VARCHAR2(256 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_BANK
-- ----------------------------
INSERT INTO "TBL_BANK" VALUES ('BIDV', 'Ngân hàng Đầu tư và Phát triển Việt Nam');
INSERT INTO "TBL_BANK" VALUES ('AGB', 'Ngân hàng nông nghiệp và phát triển nông thôn');
INSERT INTO "TBL_BANK" VALUES ('VTB', 'Ngân hàng công thương Việt Nam');
INSERT INTO "TBL_BANK" VALUES ('TPB', 'Ngân hàng Tiên Phong');
INSERT INTO "TBL_BANK" VALUES ('DAB', 'Ngân hàng Đông Á');

-- ----------------------------
-- Table structure for TBL_CONTACT
-- ----------------------------
DROP TABLE "TBL_CONTACT";
CREATE TABLE "TBL_CONTACT" (
"ID" NUMBER NOT NULL ,
"CONTACT_TYPE" VARCHAR2(256 BYTE) NULL ,
"PARTNER_ID" NUMBER NULL ,
"NAME" VARCHAR2(256 BYTE) NULL ,
"EMAIL" VARCHAR2(256 BYTE) NULL ,
"FIXED_PHONE_NUMBER" VARCHAR2(256 BYTE) NULL ,
"MOBILE_PHONE_NUMBER" VARCHAR2(256 BYTE) NULL ,
"SEX" VARCHAR2(256 BYTE) NULL ,
"POSITION" VARCHAR2(256 BYTE) NULL ,
"DEPARTMENT" VARCHAR2(256 BYTE) NULL ,
"OTHER_INFO" VARCHAR2(256 BYTE) NULL ,
"CONTACT_STATUS" VARCHAR2(256 BYTE) NULL ,
"CREATE_AT" DATE NULL ,
"CREATE_BY" NUMBER NULL ,
"UPDATE_AT" DATE NULL ,
"UPDATE_BY" NUMBER NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_CONTACT
-- ----------------------------
INSERT INTO "TBL_CONTACT" VALUES ('3', '002', null, 'rwqer', 'd@gmail.com', '423423423', '424234244', 'MALE', null, null, null, 'ACTIVE', TO_DATE('2017-05-23 02:08:02', 'YYYY-MM-DD HH24:MI:SS'), '0', TO_DATE('2017-05-23 02:08:33', 'YYYY-MM-DD HH24:MI:SS'), '0');

-- ----------------------------
-- Table structure for TBL_CONTACT_POINT
-- ----------------------------
DROP TABLE "TBL_CONTACT_POINT";
CREATE TABLE "TBL_CONTACT_POINT" (
"ID" NUMBER(10) NOT NULL ,
"CONTACT_ID" NUMBER NULL ,
"CONTRACT_ID" NUMBER NULL ,
"CONTACT_TYPE" VARCHAR2(256 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_CONTACT_POINT
-- ----------------------------

-- ----------------------------
-- Table structure for TBL_CONTACT_TYPE
-- ----------------------------
DROP TABLE "TBL_CONTACT_TYPE";
CREATE TABLE "TBL_CONTACT_TYPE" (
"CODE" VARCHAR2(256 BYTE) NOT NULL ,
"NAME" VARCHAR2(256 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_CONTACT_TYPE
-- ----------------------------
INSERT INTO "TBL_CONTACT_TYPE" VALUES ('001', 'Đầu mối liên hệ về Kinh doanh');
INSERT INTO "TBL_CONTACT_TYPE" VALUES ('002', 'Đầu mối liên hệ về Kỹ thuật');
INSERT INTO "TBL_CONTACT_TYPE" VALUES ('003', 'Đầu mối liên hệ về Đối soát');
INSERT INTO "TBL_CONTACT_TYPE" VALUES ('004', 'Đầu mối liên hệ về Thanh toán');
INSERT INTO "TBL_CONTACT_TYPE" VALUES ('005', 'Đầu mối liên hệ về Đối soát');
INSERT INTO "TBL_CONTACT_TYPE" VALUES ('006', 'Đầu mối liên hệ về Hợp đồng');

-- ----------------------------
-- Table structure for TBL_CONTRACT
-- ----------------------------
DROP TABLE "TBL_CONTRACT";
CREATE TABLE "TBL_CONTRACT" (
"ID" NUMBER(10) NOT NULL ,
"CONTRACT_STATUS" VARCHAR2(256 BYTE) NULL ,
"CONTRACT_TYPE" VARCHAR2(256 BYTE) NULL ,
"CONTRACT_NUMBER" VARCHAR2(256 BYTE) NULL ,
"PARTNER_ID" NUMBER NULL ,
"SERVICE_CODE" VARCHAR2(256 BYTE) NULL ,
"HEAD_OF_SERVICE" VARCHAR2(256 BYTE) NULL ,
"SIGN_FORM" VARCHAR2(256 BYTE) DEFAULT NULL  NULL ,
"START_DATE" DATE NULL ,
"END_DATE" DATE NULL ,
"CREATE_BY" NUMBER NULL ,
"CREATE_AT" DATE DEFAULT NULL  NULL ,
"UPDATE_BY" NUMBER NULL ,
"UPDATE_AT" DATE DEFAULT NULL  NULL ,
"FILING_DATE" DATE NULL ,
"FILING_BY" NUMBER NULL ,
"CT_USER_CSP" NUMBER NULL ,
"SIGN_CT_APPROVE" NUMBER NULL ,
"SIGN_CT_APPROVE_DATE" DATE NULL ,
"SIGN_CT_EXPERTISE_LAW" NUMBER NULL ,
"SIGN_CT_EXPERTISE_LAW_DATE" DATE NULL ,
"SIGN_CT_COMPLETE_VIETTEL" NUMBER NULL ,
"SIGN_CT_COMPLETE_VIETTEL_DATE" DATE NULL ,
"SIGN_CT_COMPLETE_PARTNER" NUMBER NULL ,
"SIGN_CT_COMPLETE_PARTNER_DATE" DATE NULL ,
"DEPLOY_USER_ID" NUMBER NULL ,
"DEPLOY_ASSIGN_DATE" DATE NULL ,
"DEPLOY_CLOSE_DATE" DATE NULL ,
"IS_LOCK" NUMBER NULL ,
"LI_USER_CSP" NUMBER NULL ,
"SIGN_LI" NUMBER NULL ,
"SIGN_LI_DATE" DATE NULL ,
"SIGN_LI_EXPERTISE_LAW" NUMBER NULL ,
"SIGN_LI_EXPERTISE_LAW_DATE" DATE NULL ,
"SIGN_LI_COMPLETE_VIETTEL" NUMBER NULL ,
"SIGN_LI_COMPLETE_VIETTEL_DATE" DATE NULL ,
"SIGN_LI_PARTNER" NUMBER NULL ,
"SIGN_LI_PARTNER_DATE" DATE NULL ,
"SINGLE_LI" NUMBER NULL ,
"SINGLE_LI_DATE" DATE NULL ,
"SINGLE_LI_START_DATE" DATE NULL ,
"LI_STATUS" NUMBER(10) NULL ,
"SINGLE_LIQUIDATION" NUMBER(10) NULL ,
"SINGLE_LIQUIDATION_DATE" DATE NULL ,
"SINGLE_LIQUIDATION_START_DATE" DATE NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_CONTRACT
-- ----------------------------

-- ----------------------------
-- Table structure for TBL_CONTRACT_BREACH
-- ----------------------------
DROP TABLE "TBL_CONTRACT_BREACH";
CREATE TABLE "TBL_CONTRACT_BREACH" (
"ID" NUMBER(10) NOT NULL ,
"CONTRACT_ID" NUMBER NULL ,
"PARTNER_ID" NUMBER NULL ,
"START_DATE" DATE NULL ,
"END_DATE" DATE NULL ,
"IS_PUBLIC" NUMBER NULL ,
"CREATE_AT" DATE NULL ,
"CREATE_BY" NUMBER NULL ,
"UPDATE_AT" DATE NULL ,
"UPDATE_BY" NUMBER NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_CONTRACT_BREACH
-- ----------------------------

-- ----------------------------
-- Table structure for TBL_CONTRACT_BREACH_DISCIPLINE
-- ----------------------------
DROP TABLE "TBL_CONTRACT_BREACH_DISCIPLINE";
CREATE TABLE "TBL_CONTRACT_BREACH_DISCIPLINE" (
"ID" NUMBER(10) NOT NULL ,
"DISCIPLINE_CODE" VARCHAR2(256 BYTE) NULL ,
"PAUSE_START_DATE" DATE NULL ,
"PAUSE_END_DATE" DATE NULL ,
"DATE_OFF" DATE NULL ,
"CONTENT" VARCHAR2(2048 BYTE) NULL ,
"CONTENT_VALUE" VARCHAR2(2048 BYTE) NULL ,
"CONTRACT_BREACH_ID" NUMBER NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_CONTRACT_BREACH_DISCIPLINE
-- ----------------------------

-- ----------------------------
-- Table structure for TBL_CONTRACT_BREACH_FILES
-- ----------------------------
DROP TABLE "TBL_CONTRACT_BREACH_FILES";
CREATE TABLE "TBL_CONTRACT_BREACH_FILES" (
"ID" NUMBER(10) NOT NULL ,
"FILE_NAME" VARCHAR2(2048 BYTE) NULL ,
"DESCRIPTION" VARCHAR2(2048 BYTE) NULL ,
"PARTNER_BREACH_ID" NUMBER NULL ,
"UPLOAD_DATE" DATE NULL ,
"PATH_FILE" VARCHAR2(1024 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_CONTRACT_BREACH_FILES
-- ----------------------------

-- ----------------------------
-- Table structure for TBL_CONTRACT_BREACH_REASON
-- ----------------------------
DROP TABLE "TBL_CONTRACT_BREACH_REASON";
CREATE TABLE "TBL_CONTRACT_BREACH_REASON" (
"ID" NUMBER(10) NOT NULL ,
"REASON_CODE" VARCHAR2(256 BYTE) NULL ,
"CONTRACT_BREACH_ID" NUMBER NULL ,
"OTHER" VARCHAR2(2048 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_CONTRACT_BREACH_REASON
-- ----------------------------

-- ----------------------------
-- Table structure for TBL_CONTRACT_FILES
-- ----------------------------
DROP TABLE "TBL_CONTRACT_FILES";
CREATE TABLE "TBL_CONTRACT_FILES" (
"ID" NUMBER(10) NOT NULL ,
"FILE_CONTRACT_NUMBER" VARCHAR2(256 BYTE) NULL ,
"FILE_NAME" VARCHAR2(2048 BYTE) NULL ,
"FILE_STATUS" VARCHAR2(256 BYTE) NULL ,
"FILE_TYPE" VARCHAR2(256 BYTE) NULL ,
"CONTRACT_ID" NUMBER NULL ,
"UPLOAD_DATE" DATE NULL ,
"START_DATE" DATE NULL ,
"END_DATE" DATE NULL ,
"CONTENT" VARCHAR2(2048 BYTE) NULL ,
"DESCRIPTION" VARCHAR2(2048 BYTE) NULL ,
"PATH_FILE" VARCHAR2(2048 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_CONTRACT_FILES
-- ----------------------------

-- ----------------------------
-- Table structure for TBL_CONTRACT_HIS
-- ----------------------------
DROP TABLE "TBL_CONTRACT_HIS";
CREATE TABLE "TBL_CONTRACT_HIS" (
"ID" NUMBER(10) NOT NULL ,
"CONTRACT_ID" NUMBER NULL ,
"CREATE_BY" VARCHAR2(256 BYTE) NULL ,
"CREATE_AT" DATE NULL ,
"STATUS_BEFORE" VARCHAR2(256 BYTE) NULL ,
"STATUS_AFTER" VARCHAR2(256 BYTE) NULL ,
"CONTENT" VARCHAR2(2048 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_CONTRACT_HIS
-- ----------------------------

-- ----------------------------
-- Table structure for TBL_CONTRACT_PAUSE
-- ----------------------------
DROP TABLE "TBL_CONTRACT_PAUSE";
CREATE TABLE "TBL_CONTRACT_PAUSE" (
"ID" NUMBER(10) NOT NULL ,
"CONTRACT_ID" NUMBER NULL ,
"CREATE_AT" DATE NULL ,
"CREATE_BY" NUMBER NULL ,
"UPDATE_AT" DATE NULL ,
"UPDATE_BY" NUMBER NULL ,
"START_DATE" DATE NULL ,
"END_DATE" DATE NULL ,
"REASON_CODE" VARCHAR2(256 BYTE) NULL ,
"FILE_PATH" VARCHAR2(2048 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_CONTRACT_PAUSE
-- ----------------------------

-- ----------------------------
-- Table structure for TBL_CONTRACT_PAUSE_FILES
-- ----------------------------
DROP TABLE "TBL_CONTRACT_PAUSE_FILES";
CREATE TABLE "TBL_CONTRACT_PAUSE_FILES" (
"ID" NUMBER(10) NOT NULL ,
"FILE_NAME" VARCHAR2(2048 BYTE) NULL ,
"DESCRIPTION" VARCHAR2(2048 BYTE) NULL ,
"CONTRACT_PAUSE_ID" NUMBER NULL ,
"UPLOAD_DATE" DATE NULL ,
"PATH_FILE" VARCHAR2(1024 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_CONTRACT_PAUSE_FILES
-- ----------------------------

-- ----------------------------
-- Table structure for TBL_CONTRACT_STATUS
-- ----------------------------
DROP TABLE "TBL_CONTRACT_STATUS";
CREATE TABLE "TBL_CONTRACT_STATUS" (
"CODE" VARCHAR2(256 BYTE) NOT NULL ,
"NAME" VARCHAR2(256 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_CONTRACT_STATUS
-- ----------------------------

-- ----------------------------
-- Table structure for TBL_CONTRACT_TYPE
-- ----------------------------
DROP TABLE "TBL_CONTRACT_TYPE";
CREATE TABLE "TBL_CONTRACT_TYPE" (
"CODE" VARCHAR2(256 BYTE) NOT NULL ,
"NAME" VARCHAR2(256 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_CONTRACT_TYPE
-- ----------------------------

-- ----------------------------
-- Table structure for TBL_DEPARTMENT
-- ----------------------------
DROP TABLE "TBL_DEPARTMENT";
CREATE TABLE "TBL_DEPARTMENT" (
"ORGANIZATIONID" NUMBER(10) NULL ,
"CODE" VARCHAR2(256 BYTE) NULL ,
"NAME" VARCHAR2(1024 BYTE) NULL ,
"ORGPARENTID" NUMBER(10) NULL ,
"ISACTIVE" VARCHAR2(10 BYTE) NULL ,
"PATH" VARCHAR2(1024 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_DEPARTMENT
-- ----------------------------

-- ----------------------------
-- Table structure for TBL_DISCIPLINE
-- ----------------------------
DROP TABLE "TBL_DISCIPLINE";
CREATE TABLE "TBL_DISCIPLINE" (
"CODE" VARCHAR2(256 BYTE) NOT NULL ,
"NAME" VARCHAR2(256 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_DISCIPLINE
-- ----------------------------

-- ----------------------------
-- Table structure for TBL_FILE_TYPE
-- ----------------------------
DROP TABLE "TBL_FILE_TYPE";
CREATE TABLE "TBL_FILE_TYPE" (
"CODE" VARCHAR2(256 BYTE) NOT NULL ,
"NAME" VARCHAR2(256 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_FILE_TYPE
-- ----------------------------

-- ----------------------------
-- Table structure for TBL_LOCATION
-- ----------------------------
DROP TABLE "TBL_LOCATION";
CREATE TABLE "TBL_LOCATION" (
"PROVINCE_CODE" VARCHAR2(256 BYTE) NULL ,
"PROVINCE_NAME" VARCHAR2(256 BYTE) NULL ,
"DISTRICT_CODE" VARCHAR2(256 BYTE) NULL ,
"DISTRICT_NAME" VARCHAR2(256 BYTE) NULL ,
"VILLAGE_CODE" VARCHAR2(256 BYTE) NULL ,
"VILLAGE_NAME" VARCHAR2(256 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_LOCATION
-- ----------------------------
INSERT INTO "TBL_LOCATION" VALUES ('001', 'An Giang', '001001', 'Huyện An Phú
', '001001001', 'Xã Quốc Thái
');
INSERT INTO "TBL_LOCATION" VALUES ('001', 'An Giang', '001001', 'Huyện An Phú
', '001001002', 'Phường Trung Tâm');
INSERT INTO "TBL_LOCATION" VALUES ('001', 'An Giang', '001001', 'Huyện An Phú
', '001001003', 'Xã Phú Thọ');
INSERT INTO "TBL_LOCATION" VALUES ('001', 'An Giang', '001001', 'Huyện An Phú
', '001001004', 'Xã Long Giang
');
INSERT INTO "TBL_LOCATION" VALUES ('001', 'An Giang', '001001', 'Huyện An Phú
', '001001005', 'Xã Vĩnh Lợi
');
INSERT INTO "TBL_LOCATION" VALUES ('001', 'An Giang', '001002', 'Huyện Châu Phú
', '001002001', 'Xã Vĩnh Nhuận
');
INSERT INTO "TBL_LOCATION" VALUES ('001', 'An Giang', '001002', 'Huyện Châu Phú

', '001002002', 'Xã An Hòa');
INSERT INTO "TBL_LOCATION" VALUES ('001', 'An Giang', '001002', 'Huyện Châu Phú
', '001002003', 'Xã Bình Thạnh');
INSERT INTO "TBL_LOCATION" VALUES ('001', 'An Giang', '001003', 'Huyện Châu Thành
', '001003001', 'Xã Hòa Bình Thạnh');
INSERT INTO "TBL_LOCATION" VALUES ('001', 'An Giang', '001003', 'Huyện Châu Thành
', '001003002', 'Xã Bình Hòa
');
INSERT INTO "TBL_LOCATION" VALUES ('002', 'Hà Nội
', '002001', 'Huyện Hoài Đức
', '002001001', 'Xã Tiền Yên

');
INSERT INTO "TBL_LOCATION" VALUES ('002', 'Hà Nội
', '002001', 'Huyện Hoài Đức
', '002001002', 'Xã An Thượng
');
INSERT INTO "TBL_LOCATION" VALUES ('002', 'Hà Nội
', '002002', 'Huyện Mê Linh', '002002001', 'Xã Tự Lập
');
INSERT INTO "TBL_LOCATION" VALUES ('002', 'Hà Nội
', '002002', 'Huyện Mê Linh
', '002002002', 'Xã Liên Mạc
');
INSERT INTO "TBL_LOCATION" VALUES ('003', 'Hải Dương
', '003001', 'Huyện Ninh Giang
', '003001001', 'Thị trấn Ninh Giang
');
INSERT INTO "TBL_LOCATION" VALUES ('002', 'Hà Nội
', '002002', 'Huyện Mê Linh
', '002002003', 'Xã Chu Phan
');
INSERT INTO "TBL_LOCATION" VALUES ('002', 'Hà Nội
', '002002', 'Huyện Mê Linh
', '002002004', 'Xã Mê Linh

');
INSERT INTO "TBL_LOCATION" VALUES ('003', 'Hải Dương
', '003001', 'Huyện Ninh Giang
', '003001002', 'Xã ứng Hoè
');
INSERT INTO "TBL_LOCATION" VALUES ('003', 'Hải Dương
', '003001', 'Huyện Ninh Giang
', '003001003', 'Xã Hồng Đức
');

-- ----------------------------
-- Table structure for TBL_MESSAGE
-- ----------------------------
DROP TABLE "TBL_MESSAGE";
CREATE TABLE "TBL_MESSAGE" (
"ID" NUMBER(10) NOT NULL ,
"SEND_USER_ID" NUMBER NULL ,
"RECEIVE_USER_ID" NUMBER NULL ,
"CONTENT" VARCHAR2(2048 BYTE) NULL ,
"DESCRIPTION" VARCHAR2(2048 BYTE) NULL ,
"CREATE_BY" NUMBER NULL ,
"CREATE_AT" DATE NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_MESSAGE
-- ----------------------------
INSERT INTO "TBL_MESSAGE" VALUES ('1', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-23 03:05:50', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "TBL_MESSAGE" VALUES ('2', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-23 03:26:30', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "TBL_MESSAGE" VALUES ('3', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-23 03:28:16', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "TBL_MESSAGE" VALUES ('4', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-23 03:30:40', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "TBL_MESSAGE" VALUES ('8', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-23 03:47:15', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "TBL_MESSAGE" VALUES ('10', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-23 03:57:09', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "TBL_MESSAGE" VALUES ('11', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-23 03:57:18', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "TBL_MESSAGE" VALUES ('6', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-23 03:46:28', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "TBL_MESSAGE" VALUES ('7', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-23 03:46:33', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "TBL_MESSAGE" VALUES ('9', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-23 03:48:57', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "TBL_MESSAGE" VALUES ('5', null, null, 'Thay đổi thông tin đối tác', 'Thay đổi thông tin đối tác', null, TO_DATE('2017-05-23 03:46:22', 'YYYY-MM-DD HH24:MI:SS'));

-- ----------------------------
-- Table structure for TBL_PARTNER
-- ----------------------------
DROP TABLE "TBL_PARTNER";
CREATE TABLE "TBL_PARTNER" (
"ID" NUMBER(10) NOT NULL ,
"PARTNER_TYPE" VARCHAR2(256 BYTE) NULL ,
"PARTNER_STATUS" VARCHAR2(256 BYTE) NULL ,
"PARTNER_CODE" VARCHAR2(256 BYTE) NULL ,
"COMPANY_NAME" VARCHAR2(256 BYTE) NULL ,
"COMPANY_NAME_SHORT" VARCHAR2(256 BYTE) NULL ,
"ADDRESS_HEAD_OFFICE" VARCHAR2(256 BYTE) NULL ,
"ADDRESS_TRADING_OFFICE" VARCHAR2(256 BYTE) NULL ,
"PHONE" VARCHAR2(256 BYTE) NULL ,
"REP_NAME" VARCHAR2(256 BYTE) NULL ,
"REP_POSITION" VARCHAR2(256 BYTE) NULL ,
"EMAIL" VARCHAR2(256 BYTE) NULL ,
"TAX_CODE" VARCHAR2(256 BYTE) NULL ,
"BUSINESS_REGIS_NUMBER" VARCHAR2(256 BYTE) NULL ,
"BUSINESS_REGIS_DATE" DATE NULL ,
"BUSINESS_REGIS_COUNT" VARCHAR2(7 BYTE) NULL ,
"PARTNER_GROUP" VARCHAR2(256 BYTE) NULL ,
"PARTNER_GROUP_PROVINCE" VARCHAR2(256 BYTE) NULL ,
"CREATE_AT" DATE NULL ,
"CREATE_BY" NUMBER NULL ,
"UPDATE_AT" DATE NULL ,
"UPDATE_BY" NUMBER NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_PARTNER
-- ----------------------------
INSERT INTO "TBL_PARTNER" VALUES ('1', 'CSKH', '001', '001', '1', '1', '1', '1', '432344343', '1', '1', '3@gmail.com', '1', '1', TO_DATE('2017-05-06 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '1', 'INTERNAL', '001', TO_DATE('2017-05-23 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null, TO_DATE('2017-05-23 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), null);

-- ----------------------------
-- Table structure for TBL_PARTNER_BANK
-- ----------------------------
DROP TABLE "TBL_PARTNER_BANK";
CREATE TABLE "TBL_PARTNER_BANK" (
"ID" NUMBER(10) NOT NULL ,
"PARTNER_ID" NUMBER NULL ,
"BANK_CODE" VARCHAR2(256 BYTE) NULL ,
"BANK_BRANCH" VARCHAR2(256 BYTE) NULL ,
"ACCOUNT_NUMBER" VARCHAR2(256 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_PARTNER_BANK
-- ----------------------------

-- ----------------------------
-- Table structure for TBL_PARTNER_LOCK
-- ----------------------------
DROP TABLE "TBL_PARTNER_LOCK";
CREATE TABLE "TBL_PARTNER_LOCK" (
"ID" NUMBER(10) NOT NULL ,
"REASON_CODE" VARCHAR2(256 BYTE) NULL ,
"COLUMN1" VARCHAR2(2048 BYTE) NULL ,
"COLUMN4" TIMESTAMP(6)  NULL ,
"UPDATE_AT" TIMESTAMP(6)  NULL ,
"UPDATE_BY" NUMBER(10) NULL ,
"CREATE_BY" NUMBER(10) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_PARTNER_LOCK
-- ----------------------------

-- ----------------------------
-- Table structure for TBL_PARTNER_LOCK_FILES
-- ----------------------------
DROP TABLE "TBL_PARTNER_LOCK_FILES";
CREATE TABLE "TBL_PARTNER_LOCK_FILES" (
"ID" NUMBER(10) NOT NULL ,
"FILE_NAME" VARCHAR2(2048 BYTE) NULL ,
"DESCRIPTION" VARCHAR2(2048 BYTE) NULL ,
"PARTNER_LOCK_ID" NUMBER NULL ,
"UPLOAD_DATE" DATE NULL ,
"PATH_FILE" VARCHAR2(2048 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_PARTNER_LOCK_FILES
-- ----------------------------

-- ----------------------------
-- Table structure for TBL_PARTNER_STATUS
-- ----------------------------
DROP TABLE "TBL_PARTNER_STATUS";
CREATE TABLE "TBL_PARTNER_STATUS" (
"CODE" VARCHAR2(256 BYTE) NOT NULL ,
"NAME" VARCHAR2(256 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_PARTNER_STATUS
-- ----------------------------
INSERT INTO "TBL_PARTNER_STATUS" VALUES ('001', 'Đang chờ xác thực tài khoản');
INSERT INTO "TBL_PARTNER_STATUS" VALUES ('002', 'Đang chờ xác nhận đối tác');
INSERT INTO "TBL_PARTNER_STATUS" VALUES ('003', 'Đang hoạt động');
INSERT INTO "TBL_PARTNER_STATUS" VALUES ('004', 'Đang khóa');

-- ----------------------------
-- Table structure for TBL_PARTNER_TYPE
-- ----------------------------
DROP TABLE "TBL_PARTNER_TYPE";
CREATE TABLE "TBL_PARTNER_TYPE" (
"CODE" VARCHAR2(256 BYTE) NOT NULL ,
"NAME" VARCHAR2(256 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_PARTNER_TYPE
-- ----------------------------
INSERT INTO "TBL_PARTNER_TYPE" VALUES ('CSP', 'CSP');
INSERT INTO "TBL_PARTNER_TYPE" VALUES ('CSKH', 'CSKH');
INSERT INTO "TBL_PARTNER_TYPE" VALUES ('Platform', 'Platform');

-- ----------------------------
-- Table structure for TBL_PERSON_TYPE
-- ----------------------------
DROP TABLE "TBL_PERSON_TYPE";
CREATE TABLE "TBL_PERSON_TYPE" (
"CODE" VARCHAR2(256 BYTE) NOT NULL ,
"NAME" VARCHAR2(256 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_PERSON_TYPE
-- ----------------------------

-- ----------------------------
-- Table structure for TBL_REASON
-- ----------------------------
DROP TABLE "TBL_REASON";
CREATE TABLE "TBL_REASON" (
"CODE" VARCHAR2(256 BYTE) NOT NULL ,
"NAME" VARCHAR2(256 BYTE) NULL ,
"DESCRIPTION" VARCHAR2(2048 BYTE) NULL ,
"TYPE" VARCHAR2(256 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_REASON
-- ----------------------------

-- ----------------------------
-- Table structure for TBL_REFLECT
-- ----------------------------
DROP TABLE "TBL_REFLECT";
CREATE TABLE "TBL_REFLECT" (
"ID" NUMBER(10) NOT NULL ,
"CODE" VARCHAR2(256 BYTE) NULL ,
"REFLECT_STATUS" VARCHAR2(256 BYTE) NULL ,
"REFLECT_TYPE" VARCHAR2(256 BYTE) NULL ,
"CUSTOMER_NAME" VARCHAR2(256 BYTE) NULL ,
"CUSTOMER_PHONE" VARCHAR2(256 BYTE) NULL ,
"SERVICE_CODE" VARCHAR2(256 BYTE) NULL ,
"HEAD_OF_SERVICE" VARCHAR2(256 BYTE) NULL ,
"CONTENT" VARCHAR2(2048 BYTE) NULL ,
"PARTNER_ID" NUMBER NULL ,
"CREATE_BY" NUMBER NULL ,
"CREATE_AT" DATE NULL ,
"UPDATE_BY" NUMBER NULL ,
"UPDATE_AT" DATE NULL ,
"CLOSE_DATE" DATE NULL ,
"CUSTOMER_CARE_STAFF_NAME" VARCHAR2(256 BYTE) NULL ,
"CUSTOMER_CARE_STAFF_PHONE" VARCHAR2(256 BYTE) NULL ,
"PRIORITY_LEVEL" VARCHAR2(256 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_REFLECT
-- ----------------------------

-- ----------------------------
-- Table structure for TBL_REFLECT_FILES
-- ----------------------------
DROP TABLE "TBL_REFLECT_FILES";
CREATE TABLE "TBL_REFLECT_FILES" (
"ID" NUMBER(10) NOT NULL ,
"FILE_NAME" VARCHAR2(2048 BYTE) NULL ,
"DESCRIPTION" VARCHAR2(2048 BYTE) NULL ,
"REFLECT_ID" NUMBER NULL ,
"UPLOAD_DATE" DATE NULL ,
"PATH_FILE" VARCHAR2(2048 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_REFLECT_FILES
-- ----------------------------

-- ----------------------------
-- Table structure for TBL_REFLECT_STATUS
-- ----------------------------
DROP TABLE "TBL_REFLECT_STATUS";
CREATE TABLE "TBL_REFLECT_STATUS" (
"CODE" VARCHAR2(256 BYTE) NOT NULL ,
"NAME" VARCHAR2(256 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_REFLECT_STATUS
-- ----------------------------

-- ----------------------------
-- Table structure for TBL_REFLECT_TYPE
-- ----------------------------
DROP TABLE "TBL_REFLECT_TYPE";
CREATE TABLE "TBL_REFLECT_TYPE" (
"CODE" VARCHAR2(256 BYTE) NOT NULL ,
"NAME" VARCHAR2(256 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_REFLECT_TYPE
-- ----------------------------

-- ----------------------------
-- Table structure for TBL_SERVICE
-- ----------------------------
DROP TABLE "TBL_SERVICE";
CREATE TABLE "TBL_SERVICE" (
"CODE" VARCHAR2(256 BYTE) NOT NULL ,
"NAME" VARCHAR2(256 BYTE) NULL ,
"PARENT_CODE" NUMBER NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_SERVICE
-- ----------------------------

-- ----------------------------
-- Table structure for TBL_SERVICE_PARTNER
-- ----------------------------
DROP TABLE "TBL_SERVICE_PARTNER";
CREATE TABLE "TBL_SERVICE_PARTNER" (
"ID" NUMBER(10) NOT NULL ,
"SERVICE_CODE" VARCHAR2(256 BYTE) NULL ,
"PARTNER_ID" NUMBER NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_SERVICE_PARTNER
-- ----------------------------

-- ----------------------------
-- Table structure for TBL_SOLUTION
-- ----------------------------
DROP TABLE "TBL_SOLUTION";
CREATE TABLE "TBL_SOLUTION" (
"ID" NUMBER(10) NOT NULL ,
"REFLECT_ID" NUMBER NULL ,
"CREATE_BY" NUMBER NULL ,
"CREATE_AT" DATE NULL ,
"UPDATE_BY" NUMBER NULL ,
"UPDATE_AT" DATE NULL ,
"CONTENT" VARCHAR2(2048 BYTE) NULL ,
"REQUIRED_REPONSE_DATE" DATE NULL ,
"REPONSE_DATE" DATE NULL ,
"SOLUTION_STATUS" VARCHAR2(256 BYTE) NULL ,
"SOLUTION_TYPE" VARCHAR2(256 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_SOLUTION
-- ----------------------------

-- ----------------------------
-- Table structure for TBL_SOLUTION_FILES
-- ----------------------------
DROP TABLE "TBL_SOLUTION_FILES";
CREATE TABLE "TBL_SOLUTION_FILES" (
"ID" NUMBER(10) NOT NULL ,
"FILE_NAME" VARCHAR2(2048 BYTE) NULL ,
"DESCRIPTION" VARCHAR2(2048 BYTE) NULL ,
"SOLUTION_ID" NUMBER NULL ,
"UPLOAD_DATE" DATE NULL ,
"PATH_FILE" VARCHAR2(2048 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_SOLUTION_FILES
-- ----------------------------

-- ----------------------------
-- Table structure for TBL_TASK_FILES
-- ----------------------------
DROP TABLE "TBL_TASK_FILES";
CREATE TABLE "TBL_TASK_FILES" (
"ID" NUMBER(10) NOT NULL ,
"FILE_NAME" VARCHAR2(2048 BYTE) NULL ,
"DESCRIPTION" VARCHAR2(2048 BYTE) NULL ,
"TASK_ID" NUMBER NULL ,
"UPLOAD_DATE" DATE NULL ,
"PATH_FILE" VARCHAR2(2048 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_TASK_FILES
-- ----------------------------

-- ----------------------------
-- Table structure for TBL_TASKS
-- ----------------------------
DROP TABLE "TBL_TASKS";
CREATE TABLE "TBL_TASKS" (
"ID" NUMBER(10) NOT NULL ,
"CONTRACT_ID" NUMBER NULL ,
"ASSIGN_BY" NUMBER NULL ,
"ASSIGN_AT" DATE NULL ,
"TASK_USER_ID" NUMBER NULL ,
"CONTENT" VARCHAR2(2048 BYTE) NULL ,
"CREATE_BY" NUMBER NULL ,
"CREATE_AT" DATE NULL ,
"UPDATE_BY" NUMBER NULL ,
"UPDATE_AT" DATE NULL ,
"REQUIRED_COMPLETE_DATE" DATE NULL ,
"COMPLETE_DATE" DATE NULL ,
"STATUS" VARCHAR2(256 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_TASKS
-- ----------------------------

-- ----------------------------
-- Table structure for TBL_USER
-- ----------------------------
DROP TABLE "TBL_USER";
CREATE TABLE "TBL_USER" (
"ID" NUMBER(10) NOT NULL ,
"FULLNAME" VARCHAR2(256 BYTE) NULL ,
"USERNAME" VARCHAR2(256 BYTE) NULL ,
"PASSWORD" VARCHAR2(256 BYTE) NULL ,
"EMAIL" VARCHAR2(256 BYTE) NULL ,
"TELEPHONE_NUMBER" VARCHAR2(256 BYTE) NULL ,
"PERSON_INFO_ID" NUMBER NULL ,
"PERSON_TYPE" VARCHAR2(256 BYTE) NULL ,
"STATUS" VARCHAR2(256 BYTE) NULL ,
"DATE_OF_BIRTH" DATE DEFAULT NULL  NULL ,
"GENDER" VARCHAR2(256 BYTE) NULL ,
"CURRENT_ADDRESS" VARCHAR2(20 BYTE) NULL ,
"MOBILE_NUMBER" VARCHAR2(256 BYTE) NULL ,
"ORGANIZATIONID" NUMBER(10) NULL ,
"EMPLOYEECODE" VARCHAR2(256 BYTE) NULL ,
"PASSWORD2" VARCHAR2(256 BYTE) NULL ,
"VERIFY_CODE" VARCHAR2(256 BYTE) NULL ,
"VERIFY_CODE_DATE" DATE DEFAULT NULL  NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TBL_USER
-- ----------------------------
INSERT INTO "TBL_USER" VALUES ('3', null, '43223233', null, null, null, '1', 'PARTNER', 'ACTIVE', null, null, null, null, null, null, null, null, null);
INSERT INTO "TBL_USER" VALUES ('2', 'rwqer', 'asdfa', null, 'd@gmail.com', null, '3', 'CONTACT', 'ACTIVE', null, null, null, '424234244', null, null, null, null, null);

-- ----------------------------
-- Function structure for CUSTOM_AUTH
-- ----------------------------
CREATE OR REPLACE function "CUSTOM_AUTH" (p_username in VARCHAR2, p_password in VARCHAR2)
return BOOLEAN
is
  l_password varchar2(4000);
  l_stored_password varchar2(4000);
  l_expires_on date;
  l_count number;
begin
-- First, check to see if the user is in the user table
select count(*) into l_count from demo_users where user_name = p_username;
if l_count > 0 then
  -- First, we fetch the stored hashed password & expire date
  select password, expires_on into l_stored_password, l_expires_on
   from demo_users where user_name = p_username;

  -- Next, we check to see if the user's account is expired
  -- If it is, return FALSE
  if l_expires_on > sysdate or l_expires_on is null then

    -- If the account is not expired, we have to apply the custom hash
    -- function to the password
    l_password := custom_hash(p_username, p_password);

    -- Finally, we compare them to see if they are the same and return
    -- either TRUE or FALSE
    if l_password = l_stored_password then
      return true;
    else
      return false;
    end if;
  else
    return false;
  end if;
else
  -- The username provided is not in the DEMO_USERS table
  return false;
end if;
end;
/

-- ----------------------------
-- Function structure for CUSTOM_HASH
-- ----------------------------
CREATE OR REPLACE function "CUSTOM_HASH" (p_username in varchar2, p_password in varchar2)
return varchar2
is
  l_password varchar2(4000);
  l_salt varchar2(4000) := 'TRJKVE0NGR1ZPCVNYNE02C7QOUKRKY';
begin

-- This function should be wrapped, as the hash algorhythm is exposed here.
-- You can change the value of l_salt or the method of which to call the
-- DBMS_OBFUSCATOIN toolkit, but you much reset all of your passwords
-- if you choose to do this.

l_password := utl_raw.cast_to_raw(dbms_obfuscation_toolkit.md5
  (input_string => p_password || substr(l_salt,10,13) || p_username ||
    substr(l_salt, 4,10)));
return l_password;
end;
/

-- ----------------------------
-- Sequence structure for TBL_CONTACT_POINT_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_CONTACT_POINT_SEQ";
CREATE SEQUENCE "TBL_CONTACT_POINT_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 1
 CACHE 20;

-- ----------------------------
-- Sequence structure for TBL_CONTACT_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_CONTACT_SEQ";
CREATE SEQUENCE "TBL_CONTACT_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 21
 CACHE 20;

-- ----------------------------
-- Sequence structure for TBL_CONTACT_TYPE_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_CONTACT_TYPE_SEQ";
CREATE SEQUENCE "TBL_CONTACT_TYPE_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 1
 CACHE 20;

-- ----------------------------
-- Sequence structure for TBL_CONTRACT_BREACH_DIS_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_CONTRACT_BREACH_DIS_SEQ";
CREATE SEQUENCE "TBL_CONTRACT_BREACH_DIS_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 1
 CACHE 20;

-- ----------------------------
-- Sequence structure for TBL_CONTRACT_BREACH_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_CONTRACT_BREACH_SEQ";
CREATE SEQUENCE "TBL_CONTRACT_BREACH_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 1
 CACHE 20;

-- ----------------------------
-- Sequence structure for TBL_CONTRACT_FILES_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_CONTRACT_FILES_SEQ";
CREATE SEQUENCE "TBL_CONTRACT_FILES_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 1
 CACHE 20;

-- ----------------------------
-- Sequence structure for TBL_CONTRACT_HIS_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_CONTRACT_HIS_SEQ";
CREATE SEQUENCE "TBL_CONTRACT_HIS_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 1
 CACHE 20;

-- ----------------------------
-- Sequence structure for TBL_CONTRACT_LOCK_FILES_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_CONTRACT_LOCK_FILES_SEQ";
CREATE SEQUENCE "TBL_CONTRACT_LOCK_FILES_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 1
 CACHE 20;

-- ----------------------------
-- Sequence structure for TBL_CONTRACT_LOCK_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_CONTRACT_LOCK_SEQ";
CREATE SEQUENCE "TBL_CONTRACT_LOCK_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 1
 CACHE 20;

-- ----------------------------
-- Sequence structure for TBL_CONTRACT_PAUSE_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_CONTRACT_PAUSE_SEQ";
CREATE SEQUENCE "TBL_CONTRACT_PAUSE_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 1
 CACHE 20;

-- ----------------------------
-- Sequence structure for TBL_CONTRACT_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_CONTRACT_SEQ";
CREATE SEQUENCE "TBL_CONTRACT_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 1
 CACHE 20;

-- ----------------------------
-- Sequence structure for TBL_CT_BREACH_REASON_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_CT_BREACH_REASON_SEQ";
CREATE SEQUENCE "TBL_CT_BREACH_REASON_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 1
 CACHE 20;

-- ----------------------------
-- Sequence structure for TBL_DISCIPLINE_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_DISCIPLINE_SEQ";
CREATE SEQUENCE "TBL_DISCIPLINE_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 1
 CACHE 20;

-- ----------------------------
-- Sequence structure for TBL_MESSAGE_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_MESSAGE_SEQ";
CREATE SEQUENCE "TBL_MESSAGE_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 21
 CACHE 20;

-- ----------------------------
-- Sequence structure for TBL_PARTNER_BANK_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_PARTNER_BANK_SEQ";
CREATE SEQUENCE "TBL_PARTNER_BANK_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 1
 CACHE 20;

-- ----------------------------
-- Sequence structure for TBL_PARTNER_LOCK_FILES_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_PARTNER_LOCK_FILES_SEQ";
CREATE SEQUENCE "TBL_PARTNER_LOCK_FILES_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 1
 CACHE 20;

-- ----------------------------
-- Sequence structure for TBL_PARTNER_LOCK_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_PARTNER_LOCK_SEQ";
CREATE SEQUENCE "TBL_PARTNER_LOCK_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 1
 CACHE 20;

-- ----------------------------
-- Sequence structure for TBL_PARTNER_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_PARTNER_SEQ";
CREATE SEQUENCE "TBL_PARTNER_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 1
 CACHE 20;

-- ----------------------------
-- Sequence structure for TBL_REFLECT_FILES_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_REFLECT_FILES_SEQ";
CREATE SEQUENCE "TBL_REFLECT_FILES_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 1
 CACHE 20;

-- ----------------------------
-- Sequence structure for TBL_REFLECT_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_REFLECT_SEQ";
CREATE SEQUENCE "TBL_REFLECT_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 1
 CACHE 20;

-- ----------------------------
-- Sequence structure for TBL_SERVICE_PARTNER_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_SERVICE_PARTNER_SEQ";
CREATE SEQUENCE "TBL_SERVICE_PARTNER_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 1
 CACHE 20;

-- ----------------------------
-- Sequence structure for TBL_SOLUTION_FILES_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_SOLUTION_FILES_SEQ";
CREATE SEQUENCE "TBL_SOLUTION_FILES_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 1
 CACHE 20;

-- ----------------------------
-- Sequence structure for TBL_SOLUTION_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_SOLUTION_SEQ";
CREATE SEQUENCE "TBL_SOLUTION_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 1
 CACHE 20;

-- ----------------------------
-- Sequence structure for TBL_TASK_FILES_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_TASK_FILES_SEQ";
CREATE SEQUENCE "TBL_TASK_FILES_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 1
 CACHE 20;

-- ----------------------------
-- Sequence structure for TBL_TASKS_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_TASKS_SEQ";
CREATE SEQUENCE "TBL_TASKS_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 1
 CACHE 20;

-- ----------------------------
-- Sequence structure for TBL_USER_SEQ
-- ----------------------------
DROP SEQUENCE "TBL_USER_SEQ";
CREATE SEQUENCE "TBL_USER_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 21
 CACHE 20;

-- ----------------------------
-- Checks structure for table TBL_BANK
-- ----------------------------
ALTER TABLE "TBL_BANK" ADD CHECK ("CODE" IS NOT NULL);

-- ----------------------------
-- Indexes structure for table TBL_CONTACT
-- ----------------------------

-- ----------------------------
-- Checks structure for table TBL_CONTACT
-- ----------------------------
ALTER TABLE "TBL_CONTACT" ADD CHECK ("ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TBL_CONTACT
-- ----------------------------
ALTER TABLE "TBL_CONTACT" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Indexes structure for table TBL_CONTACT_POINT
-- ----------------------------

-- ----------------------------
-- Checks structure for table TBL_CONTACT_POINT
-- ----------------------------
ALTER TABLE "TBL_CONTACT_POINT" ADD CHECK ("ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TBL_CONTACT_POINT
-- ----------------------------
ALTER TABLE "TBL_CONTACT_POINT" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Checks structure for table TBL_CONTACT_TYPE
-- ----------------------------
ALTER TABLE "TBL_CONTACT_TYPE" ADD CHECK ("CODE" IS NOT NULL);

-- ----------------------------
-- Indexes structure for table TBL_CONTRACT
-- ----------------------------

-- ----------------------------
-- Checks structure for table TBL_CONTRACT
-- ----------------------------
ALTER TABLE "TBL_CONTRACT" ADD CHECK ("ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TBL_CONTRACT
-- ----------------------------
ALTER TABLE "TBL_CONTRACT" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Indexes structure for table TBL_CONTRACT_BREACH
-- ----------------------------

-- ----------------------------
-- Checks structure for table TBL_CONTRACT_BREACH
-- ----------------------------
ALTER TABLE "TBL_CONTRACT_BREACH" ADD CHECK ("ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TBL_CONTRACT_BREACH
-- ----------------------------
ALTER TABLE "TBL_CONTRACT_BREACH" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Indexes structure for table TBL_CONTRACT_BREACH_DISCIPLINE
-- ----------------------------

-- ----------------------------
-- Checks structure for table TBL_CONTRACT_BREACH_DISCIPLINE
-- ----------------------------
ALTER TABLE "TBL_CONTRACT_BREACH_DISCIPLINE" ADD CHECK ("ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TBL_CONTRACT_BREACH_DISCIPLINE
-- ----------------------------
ALTER TABLE "TBL_CONTRACT_BREACH_DISCIPLINE" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Indexes structure for table TBL_CONTRACT_BREACH_FILES
-- ----------------------------

-- ----------------------------
-- Checks structure for table TBL_CONTRACT_BREACH_FILES
-- ----------------------------
ALTER TABLE "TBL_CONTRACT_BREACH_FILES" ADD CHECK ("ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TBL_CONTRACT_BREACH_FILES
-- ----------------------------
ALTER TABLE "TBL_CONTRACT_BREACH_FILES" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Indexes structure for table TBL_CONTRACT_BREACH_REASON
-- ----------------------------

-- ----------------------------
-- Checks structure for table TBL_CONTRACT_BREACH_REASON
-- ----------------------------
ALTER TABLE "TBL_CONTRACT_BREACH_REASON" ADD CHECK ("ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TBL_CONTRACT_BREACH_REASON
-- ----------------------------
ALTER TABLE "TBL_CONTRACT_BREACH_REASON" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Indexes structure for table TBL_CONTRACT_FILES
-- ----------------------------

-- ----------------------------
-- Checks structure for table TBL_CONTRACT_FILES
-- ----------------------------
ALTER TABLE "TBL_CONTRACT_FILES" ADD CHECK ("ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TBL_CONTRACT_FILES
-- ----------------------------
ALTER TABLE "TBL_CONTRACT_FILES" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Indexes structure for table TBL_CONTRACT_HIS
-- ----------------------------

-- ----------------------------
-- Checks structure for table TBL_CONTRACT_HIS
-- ----------------------------
ALTER TABLE "TBL_CONTRACT_HIS" ADD CHECK ("ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TBL_CONTRACT_HIS
-- ----------------------------
ALTER TABLE "TBL_CONTRACT_HIS" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Indexes structure for table TBL_CONTRACT_PAUSE
-- ----------------------------

-- ----------------------------
-- Checks structure for table TBL_CONTRACT_PAUSE
-- ----------------------------
ALTER TABLE "TBL_CONTRACT_PAUSE" ADD CHECK ("ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TBL_CONTRACT_PAUSE
-- ----------------------------
ALTER TABLE "TBL_CONTRACT_PAUSE" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Indexes structure for table TBL_CONTRACT_PAUSE_FILES
-- ----------------------------

-- ----------------------------
-- Checks structure for table TBL_CONTRACT_PAUSE_FILES
-- ----------------------------
ALTER TABLE "TBL_CONTRACT_PAUSE_FILES" ADD CHECK ("ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TBL_CONTRACT_PAUSE_FILES
-- ----------------------------
ALTER TABLE "TBL_CONTRACT_PAUSE_FILES" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Checks structure for table TBL_CONTRACT_STATUS
-- ----------------------------
ALTER TABLE "TBL_CONTRACT_STATUS" ADD CHECK ("CODE" IS NOT NULL);

-- ----------------------------
-- Checks structure for table TBL_CONTRACT_TYPE
-- ----------------------------
ALTER TABLE "TBL_CONTRACT_TYPE" ADD CHECK ("CODE" IS NOT NULL);

-- ----------------------------
-- Checks structure for table TBL_DISCIPLINE
-- ----------------------------
ALTER TABLE "TBL_DISCIPLINE" ADD CHECK ("CODE" IS NOT NULL);

-- ----------------------------
-- Checks structure for table TBL_FILE_TYPE
-- ----------------------------
ALTER TABLE "TBL_FILE_TYPE" ADD CHECK ("CODE" IS NOT NULL);

-- ----------------------------
-- Indexes structure for table TBL_MESSAGE
-- ----------------------------

-- ----------------------------
-- Checks structure for table TBL_MESSAGE
-- ----------------------------
ALTER TABLE "TBL_MESSAGE" ADD CHECK ("ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TBL_MESSAGE
-- ----------------------------
ALTER TABLE "TBL_MESSAGE" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Indexes structure for table TBL_PARTNER
-- ----------------------------

-- ----------------------------
-- Checks structure for table TBL_PARTNER
-- ----------------------------
ALTER TABLE "TBL_PARTNER" ADD CHECK ("ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TBL_PARTNER
-- ----------------------------
ALTER TABLE "TBL_PARTNER" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Indexes structure for table TBL_PARTNER_BANK
-- ----------------------------

-- ----------------------------
-- Checks structure for table TBL_PARTNER_BANK
-- ----------------------------
ALTER TABLE "TBL_PARTNER_BANK" ADD CHECK ("ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TBL_PARTNER_BANK
-- ----------------------------
ALTER TABLE "TBL_PARTNER_BANK" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Indexes structure for table TBL_PARTNER_LOCK
-- ----------------------------

-- ----------------------------
-- Checks structure for table TBL_PARTNER_LOCK
-- ----------------------------
ALTER TABLE "TBL_PARTNER_LOCK" ADD CHECK ("ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TBL_PARTNER_LOCK
-- ----------------------------
ALTER TABLE "TBL_PARTNER_LOCK" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Indexes structure for table TBL_PARTNER_LOCK_FILES
-- ----------------------------

-- ----------------------------
-- Checks structure for table TBL_PARTNER_LOCK_FILES
-- ----------------------------
ALTER TABLE "TBL_PARTNER_LOCK_FILES" ADD CHECK ("ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TBL_PARTNER_LOCK_FILES
-- ----------------------------
ALTER TABLE "TBL_PARTNER_LOCK_FILES" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Checks structure for table TBL_PARTNER_STATUS
-- ----------------------------
ALTER TABLE "TBL_PARTNER_STATUS" ADD CHECK ("CODE" IS NOT NULL);

-- ----------------------------
-- Checks structure for table TBL_PARTNER_TYPE
-- ----------------------------
ALTER TABLE "TBL_PARTNER_TYPE" ADD CHECK ("CODE" IS NOT NULL);

-- ----------------------------
-- Checks structure for table TBL_PERSON_TYPE
-- ----------------------------
ALTER TABLE "TBL_PERSON_TYPE" ADD CHECK ("CODE" IS NOT NULL);

-- ----------------------------
-- Checks structure for table TBL_REASON
-- ----------------------------
ALTER TABLE "TBL_REASON" ADD CHECK ("CODE" IS NOT NULL);

-- ----------------------------
-- Indexes structure for table TBL_REFLECT
-- ----------------------------

-- ----------------------------
-- Checks structure for table TBL_REFLECT
-- ----------------------------
ALTER TABLE "TBL_REFLECT" ADD CHECK ("ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TBL_REFLECT
-- ----------------------------
ALTER TABLE "TBL_REFLECT" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Indexes structure for table TBL_REFLECT_FILES
-- ----------------------------

-- ----------------------------
-- Checks structure for table TBL_REFLECT_FILES
-- ----------------------------
ALTER TABLE "TBL_REFLECT_FILES" ADD CHECK ("ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TBL_REFLECT_FILES
-- ----------------------------
ALTER TABLE "TBL_REFLECT_FILES" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Checks structure for table TBL_REFLECT_STATUS
-- ----------------------------
ALTER TABLE "TBL_REFLECT_STATUS" ADD CHECK ("CODE" IS NOT NULL);

-- ----------------------------
-- Checks structure for table TBL_REFLECT_TYPE
-- ----------------------------
ALTER TABLE "TBL_REFLECT_TYPE" ADD CHECK ("CODE" IS NOT NULL);

-- ----------------------------
-- Checks structure for table TBL_SERVICE
-- ----------------------------
ALTER TABLE "TBL_SERVICE" ADD CHECK ("CODE" IS NOT NULL);

-- ----------------------------
-- Indexes structure for table TBL_SERVICE_PARTNER
-- ----------------------------

-- ----------------------------
-- Checks structure for table TBL_SERVICE_PARTNER
-- ----------------------------
ALTER TABLE "TBL_SERVICE_PARTNER" ADD CHECK ("ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TBL_SERVICE_PARTNER
-- ----------------------------
ALTER TABLE "TBL_SERVICE_PARTNER" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Indexes structure for table TBL_SOLUTION
-- ----------------------------

-- ----------------------------
-- Checks structure for table TBL_SOLUTION
-- ----------------------------
ALTER TABLE "TBL_SOLUTION" ADD CHECK ("ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TBL_SOLUTION
-- ----------------------------
ALTER TABLE "TBL_SOLUTION" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Indexes structure for table TBL_SOLUTION_FILES
-- ----------------------------

-- ----------------------------
-- Checks structure for table TBL_SOLUTION_FILES
-- ----------------------------
ALTER TABLE "TBL_SOLUTION_FILES" ADD CHECK ("ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TBL_SOLUTION_FILES
-- ----------------------------
ALTER TABLE "TBL_SOLUTION_FILES" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Indexes structure for table TBL_TASK_FILES
-- ----------------------------

-- ----------------------------
-- Checks structure for table TBL_TASK_FILES
-- ----------------------------
ALTER TABLE "TBL_TASK_FILES" ADD CHECK ("ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TBL_TASK_FILES
-- ----------------------------
ALTER TABLE "TBL_TASK_FILES" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Indexes structure for table TBL_TASKS
-- ----------------------------

-- ----------------------------
-- Checks structure for table TBL_TASKS
-- ----------------------------
ALTER TABLE "TBL_TASKS" ADD CHECK ("ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TBL_TASKS
-- ----------------------------
ALTER TABLE "TBL_TASKS" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Indexes structure for table TBL_USER
-- ----------------------------

-- ----------------------------
-- Checks structure for table TBL_USER
-- ----------------------------
ALTER TABLE "TBL_USER" ADD CHECK ("ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TBL_USER
-- ----------------------------
ALTER TABLE "TBL_USER" ADD PRIMARY KEY ("ID");
